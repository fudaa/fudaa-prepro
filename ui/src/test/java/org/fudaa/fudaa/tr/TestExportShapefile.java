/*
 *  @creation     28 sept. 2005
 *  @modification $Date: 2007-01-19 13:14:33 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr;

import junit.framework.TestCase;
import org.fudaa.ctulu.gis.shapefile.TestJShapefile;
import org.fudaa.fudaa.sig.wizard.FSigDataModelStoreAdapter;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;

import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TestExportShapefile.java,v 1.6 2007-01-19 13:14:33 deniger Exp $
 */
public class TestExportShapefile extends TestCase {
  public void testAdapter() {

    try {
      final File f = TestJShapefile.testLineWrite();
      final FSigDataModelStoreAdapter adapter = FSigDataModelStoreAdapter.create(
          new ShapefileDataStoreFactory(), f, null);
      assertEquals(1, adapter.getNumGeometries());
      f.delete();
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }
}
