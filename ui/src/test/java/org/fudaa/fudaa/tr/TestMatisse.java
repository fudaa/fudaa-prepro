/*
 * @creation 3 mai 07
 * @modification $Date: 2007-05-04 14:07:12 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr;

import com.memoire.bu.BuPreferences;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import org.fudaa.fudaa.tr.common.TrPreferences;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.telemac.TrMatisseConvertGUI;

/**
 * @author fred deniger
 * @version $Id: TestMatisse.java,v 1.1 2007-05-04 14:07:12 deniger Exp $
 */
public final class TestMatisse {
  
  private TestMatisse() {}

  /**
   * @param _args non utilisee
   */
  public static void main(final String[] _args) {
    final JFrame f = new JFrame(TrResource.getS("Convertisseur Matisse"));
    f.setContentPane(new TrMatisseConvertGUI(true));
    f.addWindowListener(new WindowListener() {
  
      @Override
      public void windowActivated(final WindowEvent _e) {}
  
      @Override
      public void windowClosed(final WindowEvent _e) {
        BuPreferences.BU.writeIniFile();
        TrPreferences.TR.writeIniFile();
        System.exit(0);
      }
  
      @Override
      public void windowClosing(final WindowEvent _e) {}
  
      @Override
      public void windowDeactivated(final WindowEvent _e) {}
  
      @Override
      public void windowDeiconified(final WindowEvent _e) {}
  
      @Override
      public void windowIconified(final WindowEvent _e) {}
  
      @Override
      public void windowOpened(final WindowEvent _e) {}
    });
    f.pack();
    f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    f.setVisible(true);
  }

}
