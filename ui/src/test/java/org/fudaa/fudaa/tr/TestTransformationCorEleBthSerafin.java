/**
 *  @creation     8 ao�t 2003
 *  @modification $Date: 2007-01-19 13:07:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr;
import java.io.File;
import java.util.Map;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.commun.ProgressionTestAdapter;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.impl.EfLibImpl;
import org.fudaa.dodico.ef.io.corelebth.CorEleBthFileFormat;
import org.fudaa.dodico.ef.io.corelebth.CorEleBthInterface;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinMaillageAdapter;
import org.fudaa.dodico.ef.io.serafin.SerafinWriter;
/**
 * @author deniger
 * @version $Id: TestTransformationCorEleBthSerafin.java,v 1.1 2007-01-19 13:07:19 deniger Exp $
 */
public final class TestTransformationCorEleBthSerafin {
  private TestTransformationCorEleBthSerafin() {
    super();
  }
  /**
   * @param _args -file=monFichier.cor
   */
  public static void main(final String[] _args) {
    final Map arg= CtuluLibString.parseArgs(_args);
    String fileName= (String)arg.get("-f");
    if (fileName == null) {
      fileName= (String)arg.get("-file");
    }
    if (fileName == null) {
      System.err.println("Usage: -file=monFichier.cor");
      return;
    }
    final File f= new File(fileName);
    if (!f.exists()) {
      System.err.println("Fichier inexistant " + f.getAbsolutePath());
      return;
    }
    final ProgressionTestAdapter progres= new ProgressionTestAdapter();
    final CtuluIOOperationSynthese synth=
      CorEleBthFileFormat.getInstance().read(f, progres);
    synth.printAnalyze();
    final CorEleBthInterface inter= (CorEleBthInterface)synth.getSource();
    final EfGridInterface mail= inter.getGrid();
    long t1= System.currentTimeMillis();
    mail.computeBord(null,null);
    long t2= System.currentTimeMillis();
    System.out.println(t2 - t1);
    final EfGrid maillageSimpleT3= EfLibImpl.maillageT6enT3(mail,progres, null);
    t1= System.currentTimeMillis();
    maillageSimpleT3.computeBord(null,null);
    t2= System.currentTimeMillis();
    System.out.println(t2 - t1);
    final File parent= f.getParentFile();
    final File serafinSimple= new File(parent, f.getName() + "T3.ser");
    final SerafinMaillageAdapter interT3Simple=
      new SerafinMaillageAdapter(
        SerafinFileFormat.getInstance(),
        maillageSimpleT3);
    interT3Simple.setFormatColonne(true);
    System.out.println("Ecriture simple T3");
    final SerafinWriter w=
      SerafinFileFormat
        .getInstance()
        .createSerafinWriter();
    w.setMachineSPARC();
    w.setFile(serafinSimple);
    w.setProgressReceiver(progres);
    w.write(interT3Simple).printAnalyze();
  }
}
