/*
 *  @creation     23 ao�t 2005
 *  @modification $Date: 2006-10-05 12:50:52 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Query;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import java.io.File;
import junit.framework.TestCase;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISZoneAttributeFactory;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneListener;
import org.fudaa.fudaa.commun.save.FudaaSaveLib;

/**
 * @author Fred Deniger
 * @version $Id: TestJSave.java,v 1.7 2006-10-05 12:50:52 deniger Exp $
 */
public class TestJSave extends TestCase {

  ObjectContainer container_;
  File file_;

  @Override
  protected void setUp() throws Exception{
    super.setUp();
    file_ = File.createTempFile("fudaaSave", ".tmp");
    assertNotNull(file_);
  }

  @Override
  protected void tearDown() throws Exception{
    super.tearDown();
    closeDb();
    file_.delete();
  }

  protected void wakeUpDb(){
    container_ = Db4o.openFile(file_.getAbsolutePath());
    FudaaSaveLib.configureDb4o();
    assertNotNull(container_);
  }

  protected void closeDb(){
    if (container_ != null) {
      container_.commit();
      container_.close();
      container_ = null;
    }

  }

  public void testLigneSave(){
    final GISZoneListener listener = new GISZoneListener() {

      @Override
      public void attributeAction(Object source, int indexAtt, GISAttributeInterface att, int action) {}

      @Override
      public void attributeValueChangeAction(Object source, int indexAtt, GISAttributeInterface att, int indexObj,
          Object newValue) {}

      @Override
      public void objectAction(Object source, int indexObj, Object obj, int action) {}
      


    };
    //on construit une zone de ligne brisee
    final GISZoneCollectionLigneBrisee lb = GISZoneAttributeFactory.createLigneBrisee(listener,
        new GISAttributeInterface[] { GISAttributeConstants.TITRE, GISAttributeConstants.BATHY});
    final Coordinate[] cs = new Coordinate[20];
    double x = 0;
    final Double[] z = new Double[20];
    for (int i = cs.length - 1; i >= 0; i--) {
      cs[i] = new Coordinate(x++, x++, x++);
      z[i] = new Double(Math.random() * 100);
    }
    final CoordinateSequence seq = GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(cs);
    lb.addCoordinateSequence(seq, new Object[] { "toto", z}, null);
    assertEquals(1, lb.getNumGeometries());
    //on enregistre la zone dans le bdd et on la reinitialise
    wakeUpDb();
    container_.set(lb);
    closeDb();
    wakeUpDb();
    //on remplace les r�f�rences titre et bathy
    final Query q = container_.query();
    q.constrain(GISZoneCollectionLigneBrisee.class);
    final ObjectSet s = q.execute();
    assertEquals(1, s.size());
    final GISZoneCollectionLigneBrisee lbSave = (GISZoneCollectionLigneBrisee) s.next();
    container_.activate(lbSave, Integer.MAX_VALUE);
    assertEquals(1, lbSave.getNumGeometries());
    assertTrue(lbSave.getAttribute(0) == GISAttributeConstants.TITRE);
    assertTrue(lbSave.getAttribute(1) == GISAttributeConstants.BATHY);
    assertEquals(1, lbSave.getDataModel(0).getSize());
    assertEquals("toto", lbSave.getDataModel(0).getObjectValueAt(0));
    closeDb();

  }
}
