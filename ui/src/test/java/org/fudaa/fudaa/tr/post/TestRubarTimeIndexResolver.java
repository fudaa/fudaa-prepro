/**
 * 
 */
package org.fudaa.fudaa.tr.post;

import junit.framework.TestCase;

/**
 * @author CANEL Christophe (Genesis)
 */
public class TestRubarTimeIndexResolver extends TestCase {
  private static final double[] TPC_TIME_STEP_COMPATIBLE = new double[] { 0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8 };
  private static final double[] TPS_TIME_STEP_COMPATIBLE = new double[] { 0.0, 0.4, 0.6, 0.8, 1.2, 1.5, 1.8 };
  private static final double[] ZFN_TIME_STEP_COMPATIBLE = new double[] { 0.0, 0.1, 0.4, 0.5, 0.8, 0.9, 1.2, 1.3, 1.5, 1.6, 1.7 };

  private static final double[] TIME_STEP_ALL = new double[] { 0.0, 0.4, 0.8, 1.2 };
  private static final int[] TPC_TIME_INDEX_ALL = new int[] { 0, 2, 4, 6 };
  private static final int[] TPS_TIME_INDEX_ALL = new int[] { 0, 1, 3, 4 };
  private static final int[] ZFN_TIME_INDEX_ALL = new int[] { 0, 2, 4, 6 };

  private static final double[] TIME_STEP_TPC_TPS = new double[] { 0.0, 0.4, 0.6, 0.8, 1.2, 1.8 };
  private static final int[] TPC_TIME_INDEX_TPC_TPS = new int[] { 0, 2, 3, 4, 6, 9 };
  private static final int[] TPS_TIME_INDEX_TPC_TPS = new int[] { 0, 1, 2, 3, 4, 6 };
  private static final int[] ZFN_TIME_INDEX_TPC_TPS = null;

  private static final double[] TIME_STEP_TPC_ZFN = new double[] { 0.0, 0.4, 0.8, 1.2, 1.6 };
  private static final int[] TPC_TIME_INDEX_TPC_ZFN = new int[] { 0, 2, 4, 6, 8 };
  private static final int[] TPS_TIME_INDEX_TPC_ZFN = null;
  private static final int[] ZFN_TIME_INDEX_TPC_ZFN = new int[] { 0, 2, 4, 6, 9 };

  private static final double[] TIME_STEP_TPS_ZFN = new double[] { 0.0, 0.4, 0.8, 1.2, 1.5 };
  private static final int[] TPC_TIME_INDEX_TPS_ZFN = null;
  private static final int[] TPS_TIME_INDEX_TPS_ZFN = new int[] { 0, 1, 3, 4, 5 };
  private static final int[] ZFN_TIME_INDEX_TPS_ZFN = new int[] { 0, 2, 4, 6, 8 };

  private static final double[] TIME_STEP_TPC = new double[] { 0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8 };
  private static final int[] TPC_TIME_INDEX_TPC = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  private static final int[] TPS_TIME_INDEX_TPC = null;
  private static final int[] ZFN_TIME_INDEX_TPC = null;
  
  private static final double[] TIME_STEP_TPS = new double[] { 0.0, 0.4, 0.6, 0.8, 1.2, 1.5, 1.8 };
  private static final int[] TPC_TIME_INDEX_TPS = null;
  private static final int[] TPS_TIME_INDEX_TPS = new int[] { 0, 1, 2, 3, 4, 5, 6 };
  private static final int[] ZFN_TIME_INDEX_TPS = null;

  private static final double[] TIME_STEP_ZFN = new double[] { 0.0, 0.1, 0.4, 0.5, 0.8, 0.9, 1.2, 1.3, 1.5, 1.6, 1.7 };
  private static final int[] TPC_TIME_INDEX_ZFN = null;
  private static final int[] TPS_TIME_INDEX_ZFN = null;
  private static final int[] ZFN_TIME_INDEX_ZFN = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

  private static final double[] TPC_TIME_STEP_INCOMPATIBLE = new double[] { 0.0, 0.2, 1.0, 1.2, 1.4, 1.9 };
  private static final double[] TPS_TIME_STEP_INCOMPATIBLE = new double[] { 0.4, 0.6, 0.8, 1.1, 1.5, 1.8 };
  private static final double[] ZFN_TIME_STEP_INCOMPATIBLE = new double[] { 0.1, 0.5, 0.9, 1.3, 1.6, 1.7 };

  private static final double[] TIME_STEP_INCOMPATIBLE = new double[0];
  private static final int[] TPC_TIME_INDEX_INCOMPATIBLE = null;
  private static final int[] TPS_TIME_INDEX_INCOMPATIBLE = null;
  private static final int[] ZFN_TIME_INDEX_INCOMPATIBLE = null;

  public void testResolver() {
    RubarTimeIndexResolver resolver = new RubarTimeIndexResolver();

    resolver.setTpcTimeStep(TPC_TIME_STEP_COMPATIBLE);
    resolver.setTpsTimeStep(TPS_TIME_STEP_COMPATIBLE);
    resolver.setZfnTimeStep(ZFN_TIME_STEP_COMPATIBLE);

    assertTrue(resolver.computeTimeSteps());

    assertTimeStepCorrect(TIME_STEP_ALL, resolver.getTimeStep());
    assertTimeIndexCorrect(TPC_TIME_INDEX_ALL, TPS_TIME_INDEX_ALL, ZFN_TIME_INDEX_ALL, resolver);

    resolver = new RubarTimeIndexResolver();

    resolver.setTpcTimeStep(TPC_TIME_STEP_COMPATIBLE);
    resolver.setTpsTimeStep(TPS_TIME_STEP_COMPATIBLE);

    assertTrue(resolver.computeTimeSteps());

    assertTimeStepCorrect(TIME_STEP_TPC_TPS, resolver.getTimeStep());
    assertTimeIndexCorrect(TPC_TIME_INDEX_TPC_TPS, TPS_TIME_INDEX_TPC_TPS, ZFN_TIME_INDEX_TPC_TPS, resolver);

    resolver = new RubarTimeIndexResolver();

    resolver.setTpcTimeStep(TPC_TIME_STEP_COMPATIBLE);
    resolver.setZfnTimeStep(ZFN_TIME_STEP_COMPATIBLE);

    assertTrue(resolver.computeTimeSteps());

    assertTimeStepCorrect(TIME_STEP_TPC_ZFN, resolver.getTimeStep());
    assertTimeIndexCorrect(TPC_TIME_INDEX_TPC_ZFN, TPS_TIME_INDEX_TPC_ZFN, ZFN_TIME_INDEX_TPC_ZFN, resolver);

    resolver = new RubarTimeIndexResolver();

    resolver.setTpsTimeStep(TPS_TIME_STEP_COMPATIBLE);
    resolver.setZfnTimeStep(ZFN_TIME_STEP_COMPATIBLE);

    assertTrue(resolver.computeTimeSteps());

    assertTimeStepCorrect(TIME_STEP_TPS_ZFN, resolver.getTimeStep());
    assertTimeIndexCorrect(TPC_TIME_INDEX_TPS_ZFN, TPS_TIME_INDEX_TPS_ZFN, ZFN_TIME_INDEX_TPS_ZFN, resolver);

    resolver = new RubarTimeIndexResolver();

    resolver.setTpcTimeStep(TPC_TIME_STEP_COMPATIBLE);

    assertTrue(resolver.computeTimeSteps());

    assertTimeStepCorrect(TIME_STEP_TPC, resolver.getTimeStep());
    assertTimeIndexCorrect(TPC_TIME_INDEX_TPC, TPS_TIME_INDEX_TPC, ZFN_TIME_INDEX_TPC, resolver);

    resolver = new RubarTimeIndexResolver();

    resolver.setTpsTimeStep(TPS_TIME_STEP_COMPATIBLE);

    assertTrue(resolver.computeTimeSteps());

    assertTimeStepCorrect(TIME_STEP_TPS, resolver.getTimeStep());
    assertTimeIndexCorrect(TPC_TIME_INDEX_TPS, TPS_TIME_INDEX_TPS, ZFN_TIME_INDEX_TPS, resolver);

    resolver = new RubarTimeIndexResolver();

    resolver.setZfnTimeStep(ZFN_TIME_STEP_COMPATIBLE);

    assertTrue(resolver.computeTimeSteps());

    assertTimeStepCorrect(TIME_STEP_ZFN, resolver.getTimeStep());
    assertTimeIndexCorrect(TPC_TIME_INDEX_ZFN, TPS_TIME_INDEX_ZFN, ZFN_TIME_INDEX_ZFN, resolver);

    resolver = new RubarTimeIndexResolver();

    resolver.setTpcTimeStep(TPC_TIME_STEP_INCOMPATIBLE);
    resolver.setTpsTimeStep(TPS_TIME_STEP_INCOMPATIBLE);
    resolver.setZfnTimeStep(ZFN_TIME_STEP_INCOMPATIBLE);

    assertTrue(resolver.computeTimeSteps());

    assertTimeStepCorrect(TIME_STEP_INCOMPATIBLE, resolver.getTimeStep());
    assertTimeIndexCorrect(TPC_TIME_INDEX_INCOMPATIBLE, TPS_TIME_INDEX_INCOMPATIBLE, ZFN_TIME_INDEX_INCOMPATIBLE, resolver);
  }

  private static void assertTimeStepCorrect(double[] expectedTimeStep, double[] actualTimeStep) {
    assertNotNull(actualTimeStep);

    final int nbTimeStep = actualTimeStep.length;

    assertEquals(expectedTimeStep.length, nbTimeStep);

    for (int i = 0; i < nbTimeStep; i++) {
      assertEquals(expectedTimeStep[i], actualTimeStep[i]);
    }
  }

  private static void assertTimeIndexCorrect(int[] expectedTPCTimeIndex, int[] expectedTPSTimeIndex,
      int[] expectedZFNTimeIndex, RubarTimeIndexResolver resolver) {
    if (expectedTPCTimeIndex != null) {
      for (int i = 0; i < expectedTPCTimeIndex.length; i++) {
        assertEquals(expectedTPCTimeIndex[i], resolver.getTPCTimeStep(i));
      }
    }

    if (expectedTPSTimeIndex != null) {
      for (int i = 0; i < expectedTPSTimeIndex.length; i++) {
        assertEquals(expectedTPSTimeIndex[i], resolver.getTPSTimeStep(i));
      }
    }

    if (expectedZFNTimeIndex != null) {
      for (int i = 0; i < expectedZFNTimeIndex.length; i++) {
        assertEquals(expectedZFNTimeIndex[i], resolver.getZFNTimeStep(i));
      }
    }
  }
}
