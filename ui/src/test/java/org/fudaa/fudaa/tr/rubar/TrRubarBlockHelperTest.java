package org.fudaa.fudaa.tr.rubar;

import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.junit.Test;

import static org.junit.Assert.*;

public class TrRubarBlockHelperTest {
  @Test
  public void getVar() {
    assertEquals(H2dVariableType.DEBIT_NORMAL, TrRubarBlockHelper.getVar(2));

    assertEquals(H2dVariableTransType.CONCENTRATION, TrRubarBlockHelper.getVar(5));
    assertEquals(H2dVariableTransType.CONCENTRATION, TrRubarBlockHelper.getVar(8));
    assertEquals(H2dVariableTransType.CONCENTRATION, TrRubarBlockHelper.getVar(11));

    assertEquals(H2dVariableTransType.DIAMETRE, TrRubarBlockHelper.getVar(6));
    assertEquals(H2dVariableTransType.DIAMETRE, TrRubarBlockHelper.getVar(9));
    assertEquals(H2dVariableTransType.DIAMETRE, TrRubarBlockHelper.getVar(12));

    assertEquals(H2dVariableTransType.ETENDUE, TrRubarBlockHelper.getVar(7));
    assertEquals(H2dVariableTransType.ETENDUE, TrRubarBlockHelper.getVar(10));
    assertEquals(H2dVariableTransType.ETENDUE, TrRubarBlockHelper.getVar(13));
  }

  @Test
  public void getBlockIdx() {

    int idx = 5;
    assertEquals(0, TrRubarBlockHelper.getBlockIdx(idx++));
    assertEquals(0, TrRubarBlockHelper.getBlockIdx(idx++));
    assertEquals(0, TrRubarBlockHelper.getBlockIdx(idx++));

    assertEquals(1, TrRubarBlockHelper.getBlockIdx(idx++));
    assertEquals(1, TrRubarBlockHelper.getBlockIdx(idx++));
    assertEquals(1, TrRubarBlockHelper.getBlockIdx(idx++));

    assertEquals(2, TrRubarBlockHelper.getBlockIdx(idx++));
    assertEquals(2, TrRubarBlockHelper.getBlockIdx(idx++));
    assertEquals(2, TrRubarBlockHelper.getBlockIdx(idx++));

    assertEquals(3, TrRubarBlockHelper.getBlockIdx(idx++));
    assertEquals(3, TrRubarBlockHelper.getBlockIdx(idx++));
    assertEquals(3, TrRubarBlockHelper.getBlockIdx(idx++));
  }
}
