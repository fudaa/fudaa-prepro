/*
 *  @file         TestExec.java
 *  @creation     1 d�c. 2003
 *  @modification $Date: 2006-09-19 15:06:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr;


/**
 * @author deniger
 * @version $Id: TestExec.java,v 1.2 2006-09-19 15:06:55 deniger Exp $
 */
public class TestExec {
  // The default system browser under windows.
  private static final String WIN_PATH = "rundll32";

  // The flag to display a url.
  private static final String WIN_FLAG = "url.dll,FileProtocolHandler";

  /**
   *
   */
  public TestExec() {
    super();
  }

  public static void main(final String[] args) {
    final String cmd=WIN_PATH+" "+WIN_FLAG+" "+args[0];
    try{
      Runtime.getRuntime().exec(cmd);

    } catch (final Exception e){
      e.printStackTrace();
    }
  }
}
