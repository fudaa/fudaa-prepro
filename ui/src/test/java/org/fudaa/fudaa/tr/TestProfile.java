/*
 * @creation 24 nov. 06
 * 
 * @modification $Date: 2007-06-13 12:58:12 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuPanel;
import org.locationtech.jts.geom.Coordinate;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javax.swing.JDialog;
import org.fudaa.ctulu.CtuluDurationFormatter;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluUIDefault;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;
import org.fudaa.ebli.courbe.EGDialog;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourceBuilder;
import org.fudaa.fudaa.tr.post.profile.MvProfileBuilderFromLine;
import org.fudaa.fudaa.tr.post.profile.MvProfileFillePanel;
import org.fudaa.fudaa.tr.post.profile.MvProfileTarget;

/**
 * @author fred deniger
 * @version $Id: TestProfile.java,v 1.5 2007-06-13 12:58:12 deniger Exp $
 */
public final class TestProfile {

  private TestProfile() {
  }

  protected static class ProfileTarget implements MvProfileTarget {

    public TrPostSource getDataSource() {
      return null;
    }
    final TrPostSource src_;
    EGDialog d_;

    public ProfileTarget(final TrPostSource _src) {
      super();
      src_ = _src;
    }

    @Override
    public TrPostSource getData() {
      return src_;
    }

    @Override
    public EfGridDataInterpolator getInterpolator() {
      return src_.getInterpolator();
    }

    @Override
    public FudaaCourbeTimeListModel getTimeModel() {
      return src_.getNewTimeListModel();
    }

    @Override
    public CtuluVariable[] getVars() {
      return src_.getAvailableVar();
    }

    @Override
    public void profilPanelCreated(final MvProfileFillePanel _panel, ProgressionInterface _prog, String _title) {
      BuPanel pn = new BuPanel(new BuBorderLayout(5, 5));

      d_ = new EGDialog(null, _panel, true, false);
      d_.pack();
      d_.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      d_.addWindowListener(new WindowAdapter() {

        @Override
        public void windowClosed(WindowEvent _e) {
          System.exit(1);
        }
      });
      d_.setModal(false);
      d_.setSize(1000, 600);
      BuLib.invokeLater(new Runnable() {

        @Override
        public void run() {
          _panel.getGraphe().restore();
        }
      });
      d_.setVisible(true);

    }
  }

  public static void main(String[] _args) {
    System.out.println(FortranLib.getFormater(8, 3, false).format(408.245));
    if (CtuluLibArray.isEmpty(_args)) {
      System.out.println("arg is empty");
      return;
    }
    final File f = new File(_args[0]);
    System.out.println("profile for " + f.getAbsolutePath());
    if (!f.exists()) {
      return;
    }
    CtuluUIDefault ui = new CtuluUIDefault();
    TrPostSource src = TrPostSourceBuilder.activeSerafinSourceAction(f, ui, null, null, false);
    src.getTime().setTimeFormat(new CtuluDurationFormatter(true, false));
    EfGridInterface grid = src.getGrid();
    Coordinate[] cs = new Coordinate[2];
    cs[0] = new Coordinate(grid.getMinX() - 20, (grid.getMaxY() + grid.getMinY()) / 2);
    cs[1] = new Coordinate(grid.getMaxX() + 20, (grid.getMaxY() + grid.getMinY()) / 2);
    MvProfileBuilderFromLine fromLine = new MvProfileBuilderFromLine(new ProfileTarget(src), ui,
            GISGeometryFactory.INSTANCE.createLineString(cs), null);
    fromLine.start();
  }
}
