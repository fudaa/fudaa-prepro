/*
 * @creation 24 juil. 06
 * @modification $Date: 2007-01-19 13:14:33 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr;

import com.memoire.bu.BuApplication;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuPreferences;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gui.CtuluWizardInternalFrame;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.ZEbliFilleCalques;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeDefault;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.sig.wizard.FSigImageWizardTask;
import org.fudaa.fudaa.tr.common.TrPreferences;

/**
 * @author fred deniger
 * @version $Id: TestImportImage.java,v 1.6 2007-01-19 13:14:33 deniger Exp $
 */
public final class TestImportImage {

  private TestImportImage() {}

  public static void main(final String[] _args) {
    final FSigImageWizardTask image = new FSigImageWizardTask();
    final CtuluWizardInternalFrame wizard = new CtuluWizardInternalFrame(image);
    final FudaaCommonImplementation imp = new FudaaCommonImplementation() {

      @Override
      public BuInformationsSoftware getInformationsSoftware() {
        return new BuInformationsSoftware();
      }

      @Override
      public BuPreferences getApplicationPreferences() {
        return TrPreferences.TR;
      }

    };
    final BuApplication app = new BuApplication();
    app.setImplementation(imp);
    image.setParentRootPane(app.getRootPane());
    app.init();
    app.start();
    wizard.doLayout();
    // wizard.setSize(400, 450);
    wizard.pack();
    final GISZoneCollectionLigneBrisee pol = new GISZoneCollectionLigneBrisee();
    pol.addGeometry(GISGeometryFactory.INSTANCE.createLinearRing(0, 100, 0, 100), null, null);
    final ZEbliFilleCalques pn = new ZEbliFilleCalques(imp, null);
    final ZCalqueLigneBrisee calqueLigneBrisee = new ZCalqueLigneBrisee(new ZModeleLigneBriseeDefault(pol));
    pn.addCalque(calqueLigneBrisee);
    pn.getArbreCalqueModel().setSelectionCalque(calqueLigneBrisee);
    imp.addInternalFrame(pn);
    imp.addInternalFrame(wizard);
    image.setCqDest(pn.getVisuPanel());
    BuLib.invokeLater(new Runnable() {

      @Override
      public void run() {
        pn.pack();
        pn.restaurer();
      }

    });

  }

}
