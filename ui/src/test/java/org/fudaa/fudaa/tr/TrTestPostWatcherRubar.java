/*
 * @creation 19 avr. 2006
 * @modification $Date: 2007-02-07 09:56:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluUIDefault;
import org.fudaa.dodico.commun.ProgressionTestAdapter;
import org.fudaa.fudaa.tr.post.TrPostInspectorReaderRubar;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostRubarLoader;
import org.fudaa.fudaa.tr.post.TrPostSourceBuilder;
import org.fudaa.fudaa.tr.post.TrPostSourceRubar;

/**
 * @author fred deniger
 * @version $Id: TrTestPostWatcherRubar.java,v 1.6 2007-02-07 09:56:20 deniger Exp $
 */
public final class TrTestPostWatcherRubar {
  private TrTestPostWatcherRubar() {}

  public static void main(final String[] _args) {
    final Timer t = new Timer();
    if (CtuluLibArray.isEmpty(_args)) {
      System.out.println("arg is empty");
      return;
    }
    final File f = new File(_args[0]);
    System.out.println("watch " + f.getAbsolutePath());
    if (!f.exists()) {
      System.out.println("ERROR: le fichier n'existe pas");
      return;
    }
    System.out.println("DEBUT chargement des fichiers");
    final CtuluUIDefault ui = new CtuluUIDefault();
    final TrPostSourceRubar src = TrPostSourceBuilder.activeRubarSrcAction(f, new TrPostImplementation(),
        new ProgressionTestAdapter());
    final TrPostRubarLoader loader = new TrPostRubarLoader(src, null, true);
    loader.setLoadAll();
    loader.loadFiles(ui.getMainState(), ui);
    final TrPostInspectorReaderRubar rub = new TrPostInspectorReaderRubar(new TrPostProjet(src, null), src);
    t.schedule(new TimerTask() {
      @Override
      public void run() {
        rub.read();
      }

    }, 1 * 1000, 1 * 1000);

  }

}
