/*
 *  @file         TestExecLinux.java
 *  @creation     1 d�c. 2003
 *  @modification $Date: 2006-09-19 15:06:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr;

import java.io.IOException;

/**
 * @author deniger
 * @version $Id: TestExecLinux.java,v 1.2 2006-09-19 15:06:55 deniger Exp $
 */
public class TestExecLinux {
  /**
   * 
   */
  public TestExecLinux() {
    super();
  }
  
  public static void main(final String[] args) {
    final String[] com=new String[] {"xterm","-e","/home/deniger/bin/FDnetstat"};
    try {
      System.out.println("lancer");
      Runtime.getRuntime().exec(com);
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }
}
