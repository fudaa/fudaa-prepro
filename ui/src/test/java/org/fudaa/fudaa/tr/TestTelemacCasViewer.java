/*
 *  @file         TestTelemacCasViewer.java
 *  @creation     12 mai 2003
 *  @modification $Date: 2006-09-19 15:06:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.net.URL;
import java.util.Map;
import javax.swing.JFrame;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.telemac.Telemac2dFileFormat;
import org.fudaa.fudaa.fdico.FDicoEntitePanel;
import org.fudaa.fudaa.fdico.FDicoEntiteTableModel;
import org.fudaa.fudaa.tr.common.TrPreferences;
import org.fudaa.fudaa.tr.telemac.TrTelemac2dProject;
import org.fudaa.fudaa.tr.telemac.TrTelemacProjectFactory;
/**
 * @author deniger
 * @version $Id: TestTelemacCasViewer.java,v 1.2 2006-09-19 15:06:55 deniger Exp $
 */
public class TestTelemacCasViewer extends JFrame {
  public TestTelemacCasViewer(final File _fileCas) {
    final TrTelemac2dProject p=
      (TrTelemac2dProject)TrTelemacProjectFactory.createTelemac2dProject(
        _fileCas,
        (Telemac2dFileFormat.TelemacVersion)Telemac2dFileFormat
          .getInstance()
          .getLastVersionImpl(),
        null,
        null);
    initFrame(p);
  }
  private final void initFrame(final TrTelemac2dProject _param) {
    if (_param == null) {
      System.err.println("params nuls");
    }
    if (_param != null) {
      final FDicoEntitePanel pn=
        new FDicoEntitePanel(new FDicoEntiteTableModel(_param, null));
      setContentPane(pn);
    }
  }
  public static void main(final String[] args) {
    final Map arg= CtuluLibString.parseArgs(args);
    final String fileName= (String)arg.get("-f");
    File f= null;
    if (fileName == null) {
      System.err.println("fichier null -> fichier par defaut");
      final URL url= TestTelemacCasViewer.class.getResource("cas1.txt");
      if (url != null) {
        f= new File(url.getPath());
      }
    } else {
      f= new File(fileName);
    }
    if ((f == null) || (!f.exists())) {
      System.err.println("Fichier non trouv�");
      return;
    }
    final TestTelemacCasViewer frame= new TestTelemacCasViewer(f);
    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(final WindowEvent _e) {
        TrPreferences.TR.writeIniFile();
        System.exit(0);
      }
    });
    frame.pack();
    frame.setSize(800, 800);
    frame.setVisible(true);
  }
}
