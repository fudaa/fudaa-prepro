/*
 * @creation 8 d?c. 2005
 * 
 * @modification $Date: 2007-06-13 12:58:13 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr;

import java.io.File;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluAnalyzeGroup;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluUIDefault;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.commun.ProgressionTestAdapter;
import org.fudaa.dodico.ef.*;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.impl.EfLibImpl;
import org.fudaa.dodico.ef.io.corelebth.CorEleBthAdapter;
import org.fudaa.dodico.ef.io.corelebth.CorEleBthFileFormat;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.export.TrExportRefluxSovAct;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourceBuilder;
import org.fudaa.fudaa.tr.post.TrPostSourceDefault;
import org.fudaa.fudaa.tr.post.TrPostSourceProjected;

/**
 * @author Fred Deniger
 * @version $Id: TestJTrPostProject.java,v 1.2 2007-06-13 12:58:13 deniger Exp $
 */
public class TestJTrPostProject extends TestIO {

  public TestJTrPostProject() {
    super("");
  }

  EfGridInterface buildGridProjection() {
    final EfNode[] nds = new EfNode[9];
    for (int i = 0; i < nds.length; i++) {
      nds[i] = new EfNode(i % 3 + 0.5, Math.floor(i / 3) + 0.5, 0);
    }
    final EfElement[] elt = new EfElement[8];
    createElts(elt);
    return new EfGrid(nds, elt);
  }

  private void createElts(final EfElement[] _elt) {
    int idx = 0;
    // 0
    _elt[idx++] = new EfElement(new int[] { 0, 1, 4 });
    // 1
    _elt[idx++] = new EfElement(new int[] { 1, 5, 4 });
    // 2
    _elt[idx++] = new EfElement(new int[] { 1, 2, 5 });
    // 3
    _elt[idx++] = new EfElement(new int[] { 2, 6, 5 });
    // 4
    _elt[idx++] = new EfElement(new int[] { 2, 3, 6 });
    // 5
    _elt[idx++] = new EfElement(new int[] { 3, 7, 6 });
    // 6
    _elt[idx++] = new EfElement(new int[] { 4, 5, 8 });
    // 7
    _elt[idx++] = new EfElement(new int[] { 5, 9, 8 });
  }

  EfGridInterface buildGridProjectionRubar() {
    final EfNode[] nds = new EfNode[9];
    for (int i = 0; i < nds.length; i++) {
      nds[i] = new EfNode(i % 3 + 0.5, Math.floor(i / 3) + 0.5, 0);
    }
    final EfElement[] elt = new EfElement[4];
    int idx = 0;
    // 0
    elt[idx++] = new EfElement(new int[] { 0, 1, 4, 3 });
    // 1
    elt[idx++] = new EfElement(new int[] { 1, 2, 5, 4 });
    // 2
    elt[idx++] = new EfElement(new int[] { 2, 4, 7, 6 });
    // 7
    elt[idx++] = new EfElement(new int[] { 4, 5, 8, 7 });
    return new EfGrid(nds, elt);
  }

  EfDataElement[][] buildElementData(final EfData[][] _nodes, final EfGridInterface _rubarGrid) {
    final EfDataElement[][] res = new EfDataElement[_nodes.length][_nodes[0].length];
    for (int i = 0; i < res.length; i++) {
      for (int j = 0; j < res[i].length; j++) {
        final double[] eltValues = new double[_rubarGrid.getEltNb()];
        for (int elt = 0; elt < _rubarGrid.getEltNb(); elt++) {
          eltValues[elt] = _rubarGrid.getElement(elt).getAverageDanger(_nodes[i][j]);
        }
        res[i][j] = new EfDataElement(eltValues);
      }
    }
    return res;
  }

  /**
   * @param _elt les valeurs aux elements
   * @param _rubarGrid le maillage
   * @return les valeurs au noeuds: moyenne sur les elements adjacents
   */
  EfDataNode[][] buildNodeData(final EfData[][] _elt, final EfGridInterface _rubarGrid) {
    final EfDataNode[][] res = new EfDataNode[_elt.length][_elt[0].length];
    final EfNeighborMesh mesh = EfNeighborMesh.compute(_rubarGrid, null);
    for (int i = 0; i < res.length; i++) {
      for (int j = 0; j < res[i].length; j++) {
        final double[] ndValues = new double[_rubarGrid.getPtsNb()];
        for (int nd = 0; nd < _rubarGrid.getPtsNb(); nd++) {
          ndValues[nd] = mesh.getAverageForNodeDanger(nd, _elt[i][j]);
        }
        res[i][j] = new EfDataNode(ndValues);
      }
    }
    return res;
  }

  private double[] buildValues(final int _offset, final int _nbvalues) {
    final double[] res = new double[_nbvalues];
    for (int i = 0; i < res.length; i++) {
      res[i] = _offset + i;
    }
    return res;
  }

  public void testToT6() {
    final EfGridInterface initT3 = buildGrid();
    assertEquals(EfElementType.T3, initT3.getEltType());
    final EfDataNode data = new EfDataNode(buildValues(100, initT3.getPtsNb()));
    final TrPostSourceDefault src = new TrPostSourceDefault(null, "test", initT3, new double[] { 10 },
        new H2dVariableType[] { H2dVariableType.BATHYMETRIE }, new EfData[][] { { data } }, null);
    final EfData test = src.getData(H2dVariableType.BATHYMETRIE, 0);
    assertNotNull(test);
    assertTrue(Arrays.equals(test.getValues(), data.getValues()));
    final TrExportRefluxSovAct act = new TrExportRefluxSovAct();
    act.setExportCorEleBth(true);
    act.setSrc(src, new InterpolationVectorContainer());
    final File tmp = createTempFile(".sot");
    act.actExport(new ProgressionTestAdapter(), new File[] { tmp }, new CtuluAnalyzeGroup(null), new String[4]);
    act.setExportCorEleBth(true);
    final File cor = CtuluLibFile.changeExtension(tmp, "cor");
    assertTrue(cor.exists());
    final TrPostSource exportedSrc = TrPostSourceBuilder.activeINPSource(cor, tmp, new CtuluUIDefault(), null);
    final EfGridInterface compareSrc = EfLibImpl.maillageT6enT3(EfLibImpl.maillageT3enT6(initT3, null, null), null,
        null);
    assertNotNull(compareSrc);
    assertNotNull(exportedSrc);
    assertNotNull(exportedSrc.getGrid());
    assertTrue(compareSrc.isEquivalent(exportedSrc.getGrid(), true, 1E-15));
    EfData newData = null;
    newData = exportedSrc.getData(H2dVariableType.BATHYMETRIE, 0);
    assertNotNull(newData);
    assertEquals(compareSrc.getPtsNb(), newData.getSize());
    for (int i = data.getSize() - 1; i >= 0; i--) {
      assertEquals(data.getValue(i), newData.getValue(i), 1E-15);
    }
    // test de cor ele bth

    assertNotNull(tmp);
    final CorEleBthAdapter adapter = new CorEleBthAdapter();
    adapter.setMaillage(exportedSrc.getGrid());
    CorEleBthFileFormat.getInstance().write(tmp, adapter, null);
    final CtuluIOOperationSynthese op = CorEleBthFileFormat.getInstance().readGrid(tmp, null);
    if (op.containsSevereError()) {
      op.printAnalyze();
    }
    assertFalse(op.containsSevereError());
    final EfGridSource srcRead = (EfGridSource) op.getSource();
    assertNotNull(srcRead);
    tmp.delete();
    assertTrue(exportedSrc.getGrid().isSameStrict(srcRead.getGrid(), null, 1E-15));

  }

  private void buildProjectedData(final EfData[][] _dataToModify, final EfData[][] _refData) {
    final int nbPt = 9;
    final int[][] srcIdx = new int[9][];
    int idx = 0;
    srcIdx[idx++] = new int[] { 0, 1, 4, 5 };
    srcIdx[idx++] = new int[] { 1, 2, 5, 6 };
    srcIdx[idx++] = new int[] { 2, 3, 6, 7 };
    srcIdx[idx++] = new int[] { 4, 5, 8, 9 };
    srcIdx[idx++] = new int[] { 5, 6, 9, 10 };
    srcIdx[idx++] = new int[] { 6, 7, 10, 11 };
    srcIdx[idx++] = new int[] { 8, 9, 12, 13 };
    srcIdx[idx++] = new int[] { 9, 10, 13, 14 };
    srcIdx[idx++] = new int[] { 10, 11, 14, 15 };
    for (int i = 0; i < _dataToModify.length; i++) {
      for (int j = 0; j < _dataToModify[i].length; j++) {
        final EfData ref = _refData[i][j];
        final double[] res = new double[nbPt];
        for (int pt = 0; pt < nbPt; pt++) {
          final int[] idxPt = srcIdx[pt];
          for (int k = 0; k < idxPt.length; k++) {
            res[pt] += ref.getValue(idxPt[k]);
          }
          res[pt] = res[pt] / idxPt.length;
        }
        _dataToModify[i][j] = new EfDataNode(res);

      }
    }
  }

  public void testProjectedSimple() {
    final EfGridInterface grid = buildGrid();
    final double[] time = new double[5];
    for (int i = 0; i < time.length; i++) {
      time[i] = i * 10;
    }
    final H2dVariableType[] vars = new H2dVariableType[] { H2dVariableType.BATHYMETRIE, H2dVariableType.COTE_EAU,
        H2dVariableType.VITESSE_U, H2dVariableType.VITESSE_V };
    final EfData[][] datas = new EfData[vars.length][time.length];
    for (int i = 0; i < datas.length; i++) {
      for (int t = 0; t < time.length; t++) {
        datas[i][t] = new EfDataNode(buildValues(i * 100, grid.getPtsNb()));
      }
    }
    final TrPostSourceDefault src = new TrPostSourceDefault(null, "test", grid, time, vars, datas, null);
    final CtuluAnalyze analyze = new CtuluAnalyze();
    src.openDatas(null, analyze, new CtuluUIDefault());
    // la variable vitesse doit etre construite
    src.buildDefaultVarUpdateLists();
    analyze.printResume();
    internTest(src, datas, vars, time.length, 1E-15);
    // on projete sur le meme maillage
    TrPostSourceProjected srcProj = new TrPostSourceProjected(src, grid, null, false, null, "");
    srcProj.openDatas(null, analyze, null);
    analyze.printResume();
    assertTrue(CtuluLibArray.isDoubleEquals(time, srcProj.getTime().getInitTimeSteps(), 1E-10));
    for (int i = 0; i < vars.length; i++) {
      assertTrue(srcProj.isDefined(vars[i]));
    }
    internTest(srcProj, datas, vars, time.length, 1E-15);
    // sur un maillage interne
    final EfGridInterface gridProjected = buildGridProjection();
    final EfData[][] datasProjected = new EfData[vars.length][time.length];
    buildProjectedData(datasProjected, datas);
    srcProj = new TrPostSourceProjected(src, gridProjected, null, false, null, "");
    srcProj.openDatas(null, analyze, null);
    analyze.printResume();
    assertTrue(CtuluLibArray.isDoubleEquals(time, srcProj.getTime().getInitTimeSteps(), 1E-10));
    for (int i = 0; i < vars.length; i++) {
      assertTrue(srcProj.isDefined(vars[i]));
    }
    internTest(srcProj, datasProjected, vars, time.length, 1E-2);

    final int[] idx = new int[] { 1, 2 };
    double[] time2 = new double[] { time[idx[0]], time[idx[1]] };
    // on projete sur des pas de temps differents
    srcProj = new TrPostSourceProjected(src, grid, time2, false, null, "");
    srcProj.openDatas(null, analyze, null);
    analyze.printResume();
    assertTrue(CtuluLibArray.isDoubleEquals(time2, srcProj.getTime().getInitTimeSteps(), 1E-10));
    final EfData[][] datas2 = new EfData[vars.length][time2.length];
    final EfData[][] datas2Projected = new EfData[vars.length][time2.length];
    for (int i = 0; i < datas2.length; i++) {
      for (int j = 0; j < time2.length; j++) {
        datas2[i][j] = datas[i][idx[j]];
        datas2Projected[i][j] = datasProjected[i][idx[j]];
      }
    }
    // interpolation sur le temps
    internTest(srcProj, datas2, vars, time2.length, 1E-15);
    // interpolation sur le temps et maillage
    srcProj = new TrPostSourceProjected(src, gridProjected, time2, false, null, "");
    srcProj.openDatas(null, analyze, null);
    analyze.printResume();
    assertTrue(CtuluLibArray.isDoubleEquals(time2, srcProj.getTime().getInitTimeSteps(), 1E-10));
    internTest(srcProj, datas2Projected, vars, time2.length, 1E-2);

    time2 = new double[] { (time[idx[0]] + time[idx[1]]) / 2 };
    srcProj = new TrPostSourceProjected(src, grid, time2, false, null, "");
    srcProj.openDatas(null, analyze, null);
    analyze.printResume();
    assertTrue(CtuluLibArray.isDoubleEquals(time2, srcProj.getTime().getInitTimeSteps(), 1E-10));
    final EfData[][] datas3 = new EfData[vars.length][1];
    final EfData[][] datas3projected = new EfData[vars.length][1];
    for (int i = 0; i < datas3.length; i++) {
      final double[] res = new double[datas2[i][0].getSize()];
      final double[] resProjected = new double[datas2Projected[i][0].getSize()];
      for (int idxPt = 0; idxPt < res.length; idxPt++) {
        res[idxPt] = (datas2[i][0].getValue(idxPt) + datas2[i][1].getValue(idxPt)) / 2;
      }
      for (int idxPt = 0; idxPt < resProjected.length; idxPt++) {
        resProjected[idxPt] = (datas2Projected[i][0].getValue(idxPt) + datas2Projected[i][1].getValue(idxPt)) / 2;
      }
      datas3[i][0] = new EfDataNode(res);
      datas3projected[i][0] = new EfDataNode(resProjected);
    }
    internTest(srcProj, datas3, vars, 1, 1E-15);
    // sur le maillage projete
    srcProj = new TrPostSourceProjected(src, gridProjected, time2, false, null, "");
    srcProj.openDatas(null, analyze, null);
    analyze.printResume();
    assertTrue(CtuluLibArray.isDoubleEquals(time2, srcProj.getTime().getInitTimeSteps(), 1E-10));
    internTest(srcProj, datas3projected, vars, 1, 1E-2);
    // test pour rubar

  }

  private void internTest(final TrPostSource _src, final EfData[][] _datas, final H2dVariableType[] _vars,
      final int _nbTime, final double _prec) {
    // on v?rifie que les valeurs sont correctement initialis?es
    for (int var = 0; var < _vars.length; var++) {
      for (int t = 0; t < _nbTime; t++) {
        final EfData data = _src.getData(_vars[var], t);
        assertEquals(_datas[var][t].getSize(), data.getSize());
        for (int i = data.getSize() - 1; i >= 0; i--) {
          assertEquals(_datas[var][t].getValue(i), data.getValue(i), _prec);
        }
      }
    }
    assertTrue(_src.isDefined(H2dVariableType.VITESSE));
    final int idxVx = 2;
    final int idxVy = 3;
    for (int t = 0; t < _nbTime; t++) {
      final EfData data = _src.getData(H2dVariableType.VITESSE, t);
      for (int i = data.getSize() - 1; i >= 0; i--) {
        final double vx = _datas[idxVx][t].getValue(i);
        final double vy = _datas[idxVy][t].getValue(i);
        assertEquals(Math.hypot(vx, vy), data.getValue(i), _prec);
      }
    }

  }

  EfGridInterface buildGrid() {
    final EfNode[] nds = new EfNode[16];
    for (int i = 0; i < nds.length; i++) {
      nds[i] = new EfNode(i % 4, Math.floor(i / 4), 0);
    }
    final EfElement[] elt = new EfElement[18];
    createElts(elt);
    int idx = 8;
    // 8
    elt[idx++] = new EfElement(new int[] { 5, 6, 9 });
    // 9
    elt[idx++] = new EfElement(new int[] { 6, 10, 9 });
    // 10
    elt[idx++] = new EfElement(new int[] { 6, 7, 10 });
    // 11
    elt[idx++] = new EfElement(new int[] { 7, 11, 10 });
    // 12
    elt[idx++] = new EfElement(new int[] { 8, 9, 12 });
    // 13
    elt[idx++] = new EfElement(new int[] { 9, 13, 12 });
    // 14
    elt[idx++] = new EfElement(new int[] { 9, 10, 13 });
    // 15
    elt[idx++] = new EfElement(new int[] { 10, 14, 13 });
    // 16
    elt[idx++] = new EfElement(new int[] { 10, 11, 14 });
    // 17
    elt[idx++] = new EfElement(new int[] { 11, 15, 14 });
    return new EfGrid(nds, elt);
  }
}
