/*
 * @creation 28 avr. 2006
 * @modification $Date: 2006-11-14 09:08:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPanel;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.IndexedGeometryArray;
import javax.swing.JFrame;
import javax.swing.JPopupMenu;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.animation.EbliAnimationAdapterInterface;
import org.fudaa.ebli.controle.BSelecteurTargetColorInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.palette.BPalettePlageTarget;
import org.fudaa.ebli.volume.BCalqueBoite;
import org.fudaa.ebli.volume.BGrilleIrreguliere;
import org.fudaa.ebli.volume.BGroupeLumiere;
import org.fudaa.ebli.volume.BGroupeStandard;
import org.fudaa.ebli.volume.BGroupeVolume;
import org.fudaa.ebli.volume.BLumiereDirectionnelle;
import org.fudaa.ebli.volume.BUnivers;
import org.fudaa.ebli.volume.ZVue3DPanel;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourceBuilder;

/**
 * @author fred deniger
 * @version $Id: Test3D.java,v 1.7 2006-11-14 09:08:08 deniger Exp $
 */
public final class Test3D {
  private Test3D() {}

  static Color color = new Color(255, 200, 100);
  static Color colorEau = Color.BLUE;

  private static class Source extends BGrilleIrreguliere implements EbliAnimationAdapterInterface, BPalettePlageTarget,
      BSelecteurTargetColorInterface {
    final EfGridData src_;
    final TrPostSource initSrc_;
    H2dVariableType var_;
    int idx_;
    BUnivers u_;

    protected Source(final H2dVariableType _var, final EfGridData _src, final TrPostSource _initSrc) {
      super(_var.getName());
      src_ = _src;
      initSrc_ = _initSrc;
      var_ = _var;
    }

    @Override
    public boolean getRange(final CtuluRange _b) {
      initSrc_.getExtrema(_b, var_, null, null);
      return true;
    }

    @Override
    public boolean getTimeRange(final CtuluRange _b) {
      try {
        EfLib.getMinMax(src_.getData(var_, idx_), _b);
      } catch (final IOException _evt) {
        FuLog.error(_evt);

      }
      return true;
    }

    @Override
    public boolean isDonneesBoiteAvailable() {
      return true;
    }

    @Override
    public boolean isDonneesBoiteTimeAvailable() {
      return true;
    }

    @Override
    public int getNbTimeStep() {
      return initSrc_.getNbTimeStep();
    }

    @Override
    public String getTimeStep(final int _idx) {
      return initSrc_.getTimeListModel().getElementAt(_idx).toString();
    }

    @Override
    public double getTimeStepValueSec(int _idx) {
      return initSrc_.getTime().getTimeStep(_idx);
    }

    @Override
    public String getTitle() {
      return super.getName();
    }

    @Override
    public void setTimeStep(final int _idx) {
      idx_ = _idx;
      affiche();
    }

    @Override
    public void actualise(final long _t) {
      idx_++;
      affiche();
    }

    private void affiche() {
      // u_.getCanvas3D().stopRenderer();
      // u_.getCanvas3D().startRenderer();
      if (idx_ >= initSrc_.getNbTimeStep()) {
        idx_ = 0;
      }
      try {
        final EfData cote = src_.getData(var_, idx_);
        final EfData bathy = src_.getData(H2dVariableType.BATHYMETRIE, idx_);
        final Point3d[] nextFigure = new Point3d[src_.getGrid().getPtsNb()];
        final Color[] c = new Color[nextFigure.length];
        for (int i = 0; i < nextFigure.length; i++) {
          final Point3d ancien = new Point3d();
          ((IndexedGeometryArray) shape_.getGeometry()).getCoordinate(i, ancien);
          final double value = cote.getValue(i);
          nextFigure[i] = new Point3d(ancien.x, ancien.y, value);
          if (bathy != null && (Math.abs(value - bathy.getValue(i)) < 1E-3)) {
            c[i] = color;
          } else if (isColorUsed_ || palette_ == null) {
            c[i] = getCouleur();
          } else {
            c[i] = palette_.getColorFor(value);
            if (c[i] == null) {
              c[i] = palette_.getNearestPlage(value).getCouleur();
              if (c[i] == null) {}
            }

          }

        }
        ((IndexedGeometryArray) shape_.getGeometry()).setCoordinates(0, nextFigure);
        setCouleurs(c);
      } catch (final IOException _evt) {
        FuLog.error(_evt);
      }
      // u_.getCanvas3D().swap();
      // u_.getCanvas3D().startRenderer();
      // u_.getCanvas3D().repaint();
    }

    public BUnivers getU() {
      return u_;
    }

    public void setU(final BUnivers _u) {
      u_ = _u;
    }

  }

  public static void main(final String[] _args) {
    JPopupMenu.setDefaultLightWeightPopupEnabled(false);
    File f = new File("/home/genesis/Devel/tests/Fudaa-Prepro/telemac/export.ser");
    if (!CtuluLibArray.isEmpty(_args)) {
      f = new File(_args[0]);
    }
    if (!f.exists()) {
      System.err.println("le fichier n'existe pas " + f.getAbsolutePath());
      return;
    }
    final TrPostSource src = TrPostSourceBuilder.activeSerafinSourceAction(f, null, null, null,false);
    if (src == null) {
      System.err.println("la source n'est pas valide");
      return;
    }

    final int t = 0;
    EfData data = null;
    data = src.getData(H2dVariableType.BATHYMETRIE, t);
    BGrilleIrreguliere bathy = null;
    final EfGridInterface grid = src.getGrid();
    if (grid.getEltType() != EfElementType.T3) {
      System.err.println("mauvais type");
      return;
    }
    Point3d[] pts = new Point3d[grid.getPtsNb()];
    for (int i = 0; i < grid.getPtsNb(); i++) {
      pts[i] = new Point3d(grid.getPtX(i), grid.getPtY(i), data.getValue(i));
    }
    final int nbPtParEle = grid.getEltType().getNbPt();
    final int[] connect = new int[grid.getEltNb() * nbPtParEle];

    for (int i = 0; i < grid.getEltNb(); i++) {
      final EfElement element = grid.getElement(i);
      for (int k = 0; k < nbPtParEle; k++) {
        connect[nbPtParEle * i + k] = element.getPtIndex(k);
      }
    }
    bathy = new BGrilleIrreguliere("fond");
    bathy.setGeometrie(grid.getPtsNb(), pts, connect.length, connect);
    bathy.setCouleur(color);
    bathy.setEclairage(true);
    GrBoite boite = new GrBoite(grid.getEnvelope(null));
    CtuluRange r = EfLib.getMinMax(data, new CtuluRange());
    boite.e_.z_ = r.max_;
    boite.o_.z_ = r.min_;
    bathy.setBoite(new GrBoite(boite));
    bathy.setVisible(true);
    bathy.setRapide(false);
    bathy.compile();
    final BGroupeVolume gv = new BGroupeVolume();
    gv.setName("Essai");
    final BCalqueBoite cqboite = new BCalqueBoite("Axe");
    cqboite.setGeometrie(boite);
    // cqboite.setCouleur(Color.LIGHT_GRAY);
    gv.add(cqboite);
    // cqboite.setVisible(true);
    final Source sol = new Source(H2dVariableType.COTE_EAU, src, src);
    // final BGrilleIrreguliere sol = new BGrilleIrreguliere("toto");
    data = src.getData(H2dVariableType.COTE_EAU, t);
    pts = new Point3d[grid.getPtsNb()];
    for (int i = pts.length - 1; i >= 0; i--) {
      pts[i] = new Point3d(grid.getPtX(i), grid.getPtY(i), data.getValue(i));
    }
    sol.setGeometrie(grid.getPtsNb(), pts, connect.length, connect);
    sol.setVisible(true);
    sol.setRapide(false);
    sol.setTransparence(0.1f);
    sol.setEclairage(true);
    boite = new GrBoite(grid.getEnvelope(null));
    r = EfLib.getMinMax(data, new CtuluRange());
    boite.e_.z_ = r.max_;
    boite.o_.z_ = r.min_;

    sol.setBoite(boite);
    gv.add(sol);
    sol.setCouleur(colorEau);
    gv.add(bathy);
    final BGroupeLumiere gl = new BGroupeLumiere();
    gl.setName("Lumi�res");
    boite = gv.getBoite();
    final double dx = Math.abs(boite.getDeltaX());
    final double dz = Math.abs(boite.getDeltaZ());
    final double dy = Math.abs(boite.getDeltaY());
    final BLumiereDirectionnelle l1 = new BLumiereDirectionnelle(new Vector3f(0, 0, -1), Color.white);
    double dist = (dx * dx + dy * dy) / 4;
    dist = Math.sqrt(dz * dz + dist);
    final BoundingSphere sphere = new BoundingSphere(new Point3d(boite.o_.x_ + dx / 2, boite.o_.y_ + dy / 2,
        boite.e_.z_), dist);
    // BoundingSphere sphere = new BoundingSphere(new Point3d(0,0,0), Double.MAX_VALUE);
    l1.setBounds(sphere);
    l1.setIntensite(0.6);
    l1.setName("Principale");
    gl.add(l1);
    // gl.add(l2);
    l1.setRapide(false);
    l1.setVisible(true);
    final BGroupeStandard gs = new BGroupeStandard();
    gs.setName("Univers");
    gs.add(gv);
    gs.add(gl);
    gs.setVisible(true);
    gv.setAllVisible();
    final ZVue3DPanel fille = new ZVue3DPanel(null);
    fille.setRoot(gs);
    sol.setU(fille.getUnivers());
    fille.build(true);
    sol.setTimeStep(0);
    fille.getUnivers().init();
    // fille.getUnivers().getCanvas3D().startRenderer();
    final JFrame frame = new JFrame();
    fille.updateFrame(frame);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    final BuPanel content = new BuPanel();
    content.setLayout(new BuBorderLayout());
    content.add(fille, BuBorderLayout.CENTER);
    frame.setContentPane(content);
    frame.pack();
    frame.setVisible(true);
    fille.getUnivers().getCanvas3D().requestFocusInWindow();

  }
}
