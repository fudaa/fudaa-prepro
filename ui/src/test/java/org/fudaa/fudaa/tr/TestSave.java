/*
 * @creation 30 ao�t 06
 * @modification $Date: 2007-01-19 13:14:33 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr;

import java.io.File;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalquePersistenceGroupe;
import org.fudaa.ebli.calque.BCalquePoint;
import org.fudaa.ebli.calque.BCalqueSaverInterface;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.fudaa.commun.save.FudaaSaveLib;
import org.fudaa.fudaa.commun.save.FudaaSaveZipLoader;
import org.fudaa.fudaa.commun.save.FudaaSaveZipLoaderAdapter;
import org.fudaa.fudaa.commun.save.FudaaSaveZipWriter;
import org.fudaa.fudaa.sig.layer.FSigLayerLineEditable;

/**
 * @author fred deniger
 * @version $Id: TestSave.java,v 1.5 2007-01-19 13:14:33 deniger Exp $
 */
public final class TestSave {

  private TestSave() {}

  public static void main(final String[] _args) {
    FudaaSaveLib.configureDb4o();

    final GISZoneCollectionLigneBrisee zone = new GISZoneCollectionLigneBrisee();
    zone.setAttributes(new GISAttributeInterface[] { GISAttributeConstants.BATHY, GISAttributeConstants.TITRE }, null);
    zone.addGeometry(GISGeometryFactory.INSTANCE.createLinearRing(0, 10, 1, 11), new Object[] {
        new CtuluArrayDouble(new double[] { 1, 2, 3, 4 }), "ligne1" }, null);

    final File f = new File("C:\\reflux\\test.zip");
    if(f.exists()) {
      f.delete();
    }
    try {
      final BGroupeCalque cq = new BGroupeCalque();
      cq.setTitle("toto");
      final BCalque cq1 = new BCalquePoint();
      cq1.setTitle("toto1");
      cq.enPremier(cq1);
      final BGroupeCalque cq2 = new BGroupeCalque();
      cq2.setTitle("toto2");
      final BCalque cq3 = new BCalquePoint();
      final String string = "toto &��3";
      cq3.setTitle(string);
      cq2.enPremier(cq3);
      cq.enDernier(cq2);
      final FSigLayerLineEditable cq4 = new FSigLayerLineEditable(new ZModeleLigneBriseeEditable(zone), null);
      cq4.setTitle("toto &��4");
      cq.enDernier(cq4);
      final FudaaSaveZipWriter writer = new FudaaSaveZipWriter(f);

      final BCalquePersistenceGroupe saver = cq.getGroupePersistenceMng();
      saver.setTop(true);
      writer.createDir("data", cq.getTousCalques().length);
      final BCalqueSaverInterface savedData = saver.saveIn(cq, writer, "data/", "data");
      writer.close();
      System.out.println("FIN de la sauvegarde");
      cq4.getModelEditable().getGeomData().addGeometry(
          GISGeometryFactory.INSTANCE.createLinearRing(100, 1000, 200, 2000), null, null);
      System.out.println("nb geom in model " + cq4.getModelEditable().getGeomData().getNumGeometries());
      cq3.setTitle("new");
      final FudaaSaveZipLoaderAdapter.FilleCalque fille = new FudaaSaveZipLoaderAdapter.FilleCalque(new FudaaSaveZipLoader(f), savedData);
      final BCalquePersistenceGroupe groupePersistenceMng = cq.getGroupePersistenceMng();
      groupePersistenceMng.setTop(true);
      groupePersistenceMng.restoreFrom(fille, null, cq, "data/", null, null);
      System.out.println(cq3.getTitle() + " == " + string);
    } catch (final Exception _e) {
      _e.printStackTrace();
    } finally {}

  }
}
