/*
 * @creation 18 avr. 2006
 * @modification $Date: 2006-09-19 15:06:55 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.fudaa.tr.post.TrPostInspectorReaderSerafin;

/**
 * @author fred deniger
 * @version $Id: TrTestPostWatchSerafin.java,v 1.3 2006-09-19 15:06:55 deniger Exp $
 */
public final class TrTestPostWatchSerafin {
  private TrTestPostWatchSerafin() {}

  public static void main(final String[] _args) {
    final Timer t = new Timer();
    if (CtuluLibArray.isEmpty(_args)) {
      System.out.println("arg is empty");
      return;
    }
    final File f = new File(_args[0]);
    System.out.println("watch " + f.getAbsolutePath());
    if (!f.exists()) {
      return;
    }
    final TrPostInspectorReaderSerafin ser = new TrPostInspectorReaderSerafin(f,null);
    t.schedule(new TimerTask() {

      @Override
      public void run() {
        ser.read();
      }

    }, 1 * 1000, 1 * 1000);

  }

}
