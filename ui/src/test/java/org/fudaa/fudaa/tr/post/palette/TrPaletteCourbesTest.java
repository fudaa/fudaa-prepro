package org.fudaa.fudaa.tr.post.palette;

import junit.framework.TestCase;
import org.fudaa.ebli.courbe.PaletteCourbes;
/**
 * 
 * @author Adrien Hadoux
 *
 */
public class TrPaletteCourbesTest extends TestCase{
	
	
	
	public void testSaveColorPalette() {
		 PaletteCourbes tr = new PaletteCourbes("prepro.xml");
		//-- try to load palettes --//
		tr.initTraceurs();
		assertNotNull(tr.getTraceurs());
		tr.savePalette();
		assertNotNull(tr.fileName());
		assertTrue(new java.io.File(tr.fileName()).exists());
		PaletteCourbes tr2 = new PaletteCourbes("prepro.xml");
		tr2.initTraceurs();

		assertEquals(tr2.getTraceurs().size(), tr.getTraceurs().size());
		for(int i=0;i<tr2.getTraceurs().size();i++) {
			assertEquals(tr2.getTraceurs().get(i).size(), tr.getTraceurs().get(i).size());
		}
		
	}

}
