/*
 *  @file         TestTrMaillage.java
 *  @creation     30 juin 2003
 *  @modification $Date: 2006-10-27 10:24:42 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr;

import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuSplit2Pane;
import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.util.Map;
import javax.swing.JFrame;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUIDefault;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinInterface;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.BVueCalque;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.calque.ZCalquePolygone;
import org.fudaa.ebli.calque.ZEbliFilleCalques;
import org.fudaa.fudaa.meshviewer.layer.MvGridLayerGroup;

/**
 * @author deniger
 * @version $Id: TestTrMaillage.java,v 1.5 2006-10-27 10:24:42 deniger Exp $
 */
public class TestTrMaillage {
  ZEbliFilleCalques fc_;
  BVueCalque vue_;
  BArbreCalque arbre_;

  /**
   *
   */
  public TestTrMaillage(final MvGridLayerGroup _maillage) {
    final BGroupeCalque gc = new BGroupeCalque();
    gc.setTitle("Mailleur");
    final ZCalquePoint zpt = _maillage.getPointLayer();
    final ZCalquePolygone zpoly = _maillage.getPolygonLayer();
    zpt.setName("cqPOINTMAILLEUR");
    zpt.setTitle("points");
    gc.add(zpt);
    gc.add(zpoly);
    fc_ = new ZEbliFilleCalques(new CtuluUIDefault(), gc);
    vue_ = fc_.getVueCalque();
    vue_.setBackground(Color.white);
    vue_.setPreferredSize(new Dimension(500, 400));
    arbre_ = new BArbreCalque();
    arbre_.setModel(fc_.getArbreCalqueModel());
    // fc_.setSelectionVisible(true);
    fc_.setName("MAILLEUR");
    fc_.setTitle("Modele");
    fc_.setCalque(gc);
    fc_.setCalqueSelectionActif();
    fc_.restaurer();
  }

  public static void main(final String[] args) {
    final Map arg = CtuluLibString.parseArgs(args);
    String fileName = (String) arg.get("-f");
    if (fileName == null) {
      fileName = (String) arg.get("-file");
    }
    if (fileName == null) {
      System.err.println("Fichier non trouve");
      System.exit(1);
    }
    final File f = new File(fileName);
    System.out.println("Debut Lecture");
    final CtuluIOOperationSynthese op = SerafinFileFormat.getInstance().read(f, null);
    final SerafinInterface inter = (SerafinInterface) op.getSource();
    System.out.println("fin Lecture");
    if (op.containsSevereError()) {
      op.getAnalyze().printResume();
      System.exit(1);
    } else if (op.containsMessages()) {
      op.getAnalyze().printResume();
    }
    // TrMaillage maillage =
    // new TrMaillage(
    // H2dLib.createFor(inter.getMaillage(), inter.getPtsFrontiere(), null));
    System.out.println(inter.getIpoboFr()[0]);
    final long t1 = System.currentTimeMillis();
    inter.getGrid().computeBord(null, null);
    final long t2 = System.currentTimeMillis();
    System.out.println("temps " + (t2 - t1));
    final MvGridLayerGroup maillage = new MvGridLayerGroup(inter.getGrid());
    final TestTrMaillage test = new TestTrMaillage(maillage);
    final JFrame frame = new JFrame("Essai");
    final BuDesktop desk = new BuDesktop();
    desk.setPreferredSize(new Dimension(300, 300));
    test.arbre_.setPreferredSize(new Dimension(50, 300));
    desk.add(test.fc_);
    test.fc_.setVisible(true);
    final BuSplit2Pane split = new BuSplit2Pane(desk, test.arbre_);
    frame.setContentPane(split);
    test.fc_.restaurer();
    test.vue_.changeRepere(test.fc_, maillage.getDomaine());
    frame.pack();
    frame.setVisible(true);
    test.fc_.restaurer();
  }
}
