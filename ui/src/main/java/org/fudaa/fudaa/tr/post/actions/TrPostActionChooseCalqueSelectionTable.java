package org.fudaa.fudaa.tr.post.actions;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.calque.action.CalqueActionTable;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.commun.EbliTableInfoTarget;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrIsoLayerDefault;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Surcharge de l'action calque Action Table qui propose 2 modes: - affiche dans le cas 'classique' les noeuds
 * s�lectionn�es - affiche dans le cas 'interpol�' les selection interpol�es. le mode est activ� automatiquement en
 * fonction de la s�lection.
 *
 * @author Adrien Hadoux
 *
 */
public class TrPostActionChooseCalqueSelectionTable extends CalqueActionTable implements EbliTableInfoTarget {

    private static final long serialVersionUID = 1L;
    BArbreCalqueModel model_;
    CalqueGISTreeModel treeModel;

    public List<GrPoint> transform(LineString datas) {
        List<GrPoint> data = new ArrayList<GrPoint>();

        for (int i = 0; i < datas.getNumPoints(); i++) {
            Point p = datas.getPointN(i);
            GrPoint point = new GrPoint(p.getX(), p.getY(), 0);
            data.add(point);
        }
        return data;
    }

    public class TableModelInterpolated implements TableModel {

        List<GrPoint> data;
        TrPostVisuPanel panel;
        H2dVariableType variable;
        int time;
        String[] title = new String[]{"Element", "X", "Y", "value"};

        public TableModelInterpolated(List<GrPoint> datas, TrPostVisuPanel p, H2dVariableType var, int t) {
            this.data = datas;
            this.panel = p;
            this.variable = var;
            this.time = t;
            String indicTime = p.getSource().getTime().getTimeListModel().getElementAt(this.time).toString();
                    
            title[3] = variable.getName() + " " + indicTime;
        }

        @Override
        public int getRowCount() {
            return this.data.size();
        }

        @Override
        public int getColumnCount() {
            return title.length;
        }

        @Override
        public String getColumnName(int columnIndex) {
            return title[columnIndex];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {

            if (columnIndex == 0) {
                final int idxElt = TrIsoLayerDefault.sondeSelection(data.get(rowIndex), panel.getIsoLayer().getIsoModel());
                return idxElt;
            } else if (columnIndex == 1) {
                return data.get(rowIndex).x_;
            } else if (columnIndex == 2) {
                return data.get(rowIndex).y_;
            } else {
                try {

                    final int idxElt = TrIsoLayerDefault.sondeSelection(data.get(rowIndex), panel.getIsoLayer().getIsoModel());
                    return panel.getSource().getInterpolator().interpolate(idxElt, data.get(rowIndex).x_, data.get(rowIndex).y_, variable, time);
                } catch (IOException ex) {
                    FuLog.error(ex);
                    return "";
                }
            }
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        }

        @Override
        public void addTableModelListener(TableModelListener l) {

        }

        @Override
        public void removeTableModelListener(TableModelListener l) {

        }

    }

    public TrPostActionChooseCalqueSelectionTable(BArbreCalqueModel model, CtuluUI ui,
            ZEbliCalquesPanel calque) {
        super(model, ui, calque);
        model_ = model; //new BArbreCalqueModel((BGroupeCalque) model.getRootCalque());
        treeModel = new CalqueGISTreeModel(model_,(BGroupeCalque) model.getRootCalque());
        treeModel.setMask(GISLib.MASK_POINT);
        setIcon(BuResource.BU.getToolIcon("tache"));
        putValue(Action.NAME, TrResource.getS("Tableau des valeurs issues d'un autre calque"));
    }

    public BGroupeCalque buildGroupCalque(BGroupeCalque groupeCalque) {
        BGroupeCalque res = groupeCalque;
        boolean endOfCalque = false;
        while (!endOfCalque) {
            BCalque[] calques = groupeCalque.getCalques();
            for (BCalque calque : calques) {
                if (calque instanceof BGroupeCalque) {
                    BGroupeCalque groupeCalculated = buildGroupCalque((BGroupeCalque) calque);
                    res.add(groupeCalculated);
                } else if (calque instanceof ZCalqueAffichageDonneesInterface) {
                    ZCalqueAffichageDonneesInterface zc = (ZCalqueAffichageDonneesInterface) calque;
                    if (zc.modeleDonnees() instanceof ZModelePoint) {
                        res.add(calque);
                    }
                }
            }
        }

        return res;
    }

    public List<GrPoint> getSelectedLine() {
        List<GrPoint> selection = new ArrayList<GrPoint>();
        final TreePath p = tree_.getSelectionPath();
        if(p != null) {
        BCalque calque = (BCalque)((CalqueGISTreeModel.LayerNode) p.getLastPathComponent()).getUserObject(); // 

        // pas de calque s�lectionn�
        if (calque instanceof ZCalqueAffichageDonneesInterface) { //-- on essaie de recuperer la ligne selectionnee:--//
            LineString ln = ((ZCalqueAffichageDonneesInterface) calque).getSelectedLine();

            if (ln == null) {
                if (((ZCalqueAffichageDonneesInterface) calque).modeleDonnees() instanceof ZModelePoint) {
                    ZModelePoint modelLayer = ((ZModelePoint) ((ZCalqueAffichageDonneesInterface)calque).modeleDonnees());
                    selection = new ArrayList<GrPoint>();
                    for (int i = 0; i < modelLayer.getNombre(); i++) {
                        selection.add(new GrPoint(modelLayer.getX(i), modelLayer.getY(i), 0));
                    }
                }
            } else {
                selection = transform(ln);
            }
        }
      }  
        return selection;
    
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {

        buildSelectionCalqueIhm();
    }
    JTree tree_ = null;

    private void buildSelectionCalqueIhm() {
        BuPanel panel = new BuPanel(new BorderLayout());
        final Frame f = CtuluLibSwing.getFrameAncestorHelper(getUi().getParentComponent());
        final JDialog dialog_ = new JDialog(f);

        tree_ = treeModel.createView(false, false);

        //panel.add(arbreCalque, BorderLayout.CENTER);
        panel.add(new JScrollPane(tree_), BorderLayout.CENTER);
        BuPanel buttonPanel = new BuPanel(new FlowLayout(FlowLayout.CENTER));
        BuButton ok = new BuButton(TrResource.getS("ok"),
                BuResource.BU.getIcon("valider_22"));
        buttonPanel.add(ok);
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog_.dispose();
                buildUi();

            }
        });
        panel.add(buttonPanel, BorderLayout.SOUTH);

        dialog_.setResizable(true);
        dialog_.setContentPane(panel);
        dialog_.setModal(true);
        dialog_.pack();
        dialog_.setPreferredSize(new Dimension(600,400));
        dialog_.setMinimumSize(new Dimension(600,400));
        dialog_.setMaximumSize(new Dimension(600,400));
        dialog_.setTitle(TrResource.getS("Choisir le calque qui contient la s�lection de points"));
        dialog_.setLocationRelativeTo(CtuluLibSwing.getFrameAncestor(getUi().getParentComponent()));
        dialog_.setVisible(true);
        
    }

    private void buildUi() {
        EbliTableInfoPanel panel = new EbliTableInfoPanel(getUi(), this, null);
        panel.showInDialog();
    }

    @Override
    public BuTable createValuesTable() {
        List<GrPoint> result = getSelectedLine();
        TrPostVisuPanel panel = (TrPostVisuPanel) getCalque();
        H2dVariableType variable = panel.getSelectedVarInCalqueActif();
        int time = panel.getSelectedTimeInCalqueActif();
        TableModelInterpolated model = new TableModelInterpolated(result, panel, variable, time);
        BuTable table = new BuTable(model);

        return table;

    }

    @Override
    public int[] getSelectedObjectInTable() {
        return null;
    }

    @Override
    public boolean isValuesTableAvailable() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
