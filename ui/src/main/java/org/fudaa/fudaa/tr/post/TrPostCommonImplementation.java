/**
 * @creation 19 oct. 2004
 * @modification $Date: 2007-05-04 14:01:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.courbe.PaletteCourbes;
import org.fudaa.ebli.visuallibrary.calque.CalqueLegendeWidgetAdapter;
import org.fudaa.fudaa.tr.common.TrCommonImplementation;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.actions.TrPostActionChooseAndCreateCalque;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostWizardCreateScope;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: TrPostCommonImplementation.java,v 1.24 2007-05-04 14:01:51 deniger Exp $
 */
public abstract class TrPostCommonImplementation extends TrCommonImplementation {
  TrPostProjet project;

  public final TrPostProjet getCurrentProject() {
    return project;
  }

  public final void setCurrentProject(final TrPostProjet _c) {
    project = _c;
  }

  /**
   * @return toutes les fenetres de PostLayout
   */
  public List<TrPostLayoutFille> getAllLayoutFille() {
    final JInternalFrame[] allInternalFrames = getAllInternalFrames();
    if (allInternalFrames == null) {
      return Collections.emptyList();
    }
    final List<TrPostLayoutFille> res = new ArrayList<TrPostLayoutFille>(allInternalFrames.length);
    for (int i = 0; i < allInternalFrames.length; i++) {
      final JInternalFrame internalFrame = allInternalFrames[i];
      if (internalFrame instanceof TrPostLayoutFille) {
        res.add((TrPostLayoutFille) internalFrame);
      }
    }
    return res;
  }

  public void elimineDoublonNomsLayout() {
    final List<TrPostLayoutFille> liste = getAllLayoutFille();

    for (int i = 0; i < liste.size(); i++) {
      int cpt = 1;
      for (int j = i; j < liste.size(); j++) {
        if (liste.get(j).previousTitleFrame.equals(liste.get(i).previousTitleFrame)) {

          liste.get(j).previousTitleFrame += "_" + (cpt++);
        }
      }
    }
  }

  public void display(final TrPostVisuPanel pnVisu, final BCalque calqueActif) {
    display(pnVisu, calqueActif, TrPostSourceComparatorBuilder.getComparaisonTitle(), true);
  }

  public void display(final TrPostVisuPanel pnVisu, final BCalque calqueActif, final String title, final boolean addCounter) {
    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {
        final CalqueLegendeWidgetAdapter legendeCalque = (CalqueLegendeWidgetAdapter) pnVisu.getCqLegend();
        final BCalque calqueAct = calqueActif;
        pnVisu.setCalqueActif(calqueAct);
        final TrPostLayoutFille layoutFille = getCurrentLayoutFille();
        String titleTouse = title;
        if (addCounter) {
          titleTouse += " " + layoutFille.getScene().getAllVue2d().size() + 1;
        }
        layoutFille.controller_.addCalque(titleTouse, null, pnVisu.getPreferredSize(), pnVisu);

        pnVisu.restaurer();
      }
    });
  }

  /**
   * @return la fenetre layout active.
   */
  public TrPostLayoutFille getCurrentLayoutFille() {
    final JInternalFrame currentInternalFrame = getCurrentInternalFrame();
    if (currentInternalFrame instanceof TrPostLayoutFille) {
      return (TrPostLayoutFille) currentInternalFrame;
    } else {
      // -- on recherche la premiere file layout qu'on a et on l'envoie --//
      // -- ACHTUNG: CAS A TRAITER:EXEMPLE SI ON EST DANS LA FRAME GESTION MULTI SOURCE; ON EST PAS DANS LA CURRENT
      // LAYOUT MAIS ON S EN FOUT--//
      return getAllLayoutFille().get(0);
    }
  }

  /**
   * @return la fenetre layout active ou la premiere des LayoutFille de cette application
   */
  public TrPostLayoutFille getCurrentLayoutFilleOrFirst() {
    TrPostLayoutFille current = getCurrentLayoutFille();
    if (current == null) {
      final List<TrPostLayoutFille> allLayoutFille = getAllLayoutFille();
      if (!allLayoutFille.isEmpty()) {
        current = allLayoutFille.get(0);
      }
    }
    return current;
  }

  @Override
  public void init() {
    super.init();

    final BuMenu menuFile = (BuMenu) getMainMenuBar().getMenu("MENU_FICHIER");
    BuMenuItem openAndRecompute = new BuMenuItem(TrResource.getS("Ouvrir et recalculer"));
    openAndRecompute.setActionCommand("OPEN_AND_RELOAD");
    openAndRecompute.addActionListener(this);
    openAndRecompute.setIcon(BuResource.BU.getIcon("ouvrir"));
    openAndRecompute.setToolTipText(TrResource.getS("Toutes les variables, trajectoires,... vont �tre recalcul�es au lancement"));
    menuFile.add(openAndRecompute, 2);

    final BuMenu menu = (BuMenu) getMainMenuBar().getMenu("IMPORTER");
    final JMenuItem menuImportImage = BuMenuBar.getMenuItem(menu, "IMPORT_IMAGE");
    if (menuImportImage != null) {
      menu.remove(menuImportImage);
    }
    final JMenuItem menuImportProjet = BuMenuBar.getMenuItem(menu, "IMPORT_PROJECT");
    if (menuImportProjet != null) {
      menuImportProjet.setText(TrResource.getS("Importer un fichier source"));
    }

    menu.addMenuItem(TrResource.getS("Fichiers scope"), getImportResultsAct());
    setEnabledForAction("OPEN_AND_RELOAD", true);

    final BuMenu menuGraphe = new BuMenu();
    menuGraphe.setText(TrResource.getS("Graphe"));
    menuGraphe.setIcon(null);
    menuGraphe.setIconTextGap(0);
    menuGraphe.setSize(100, 0);
    menuGraphe.addMenuItem(TrResource.getS("Configuration couleurs"), "CONFIG_GRAPH_MODEL", null, this);
    getMainMenuBar().add(menuGraphe, 3);
  }

  String getImportResultsAct() {
    return "IMPORT_RESULTS";
  }

  @Override
  public void close() {
    if (project != null) {
      project.close();
    }
    removeInternalFrames(getAllInternalFrames());
  }

  public void setProjet(final TrPostProjet _c) {
    if (project == _c) {
      return;
    }
    new CtuluTaskOperationGUI(null, CtuluLibString.EMPTY_STRING) {
      @Override
      public void act() {
        saveAndCloseProjet(createProgressionInterface(this));
        project = _c;
        if (project != null) {
          project.active(TrPostCommonImplementation.this);
          BuLib.invokeNowOrLater(new Runnable() {
            @Override
            public void run() {
              setEnabledForAction("CREER", true);
              setEnabledForAction("OPEN_AND_RELOAD", true);
              setEnabledForAction("ENREGISTRER", true);
              setEnabledForAction("ENREGISTRERSOUS", true);
              setEnabledForAction("IMPORTER", true);
              setEnabledForAction("RECHERCHER", true);
              setEnabledForAction(getImportResultsAct(), true);
              setEnabledForAction("IMPORT_PROJECT", true);
            }
          });
        }
      }
    }.start();
  }

  public boolean saveAndCloseProjet(final ProgressionInterface _interface) {
    if (project != null) {

      // -- enregistrement sauce widget --//
      if (project.isModified()) {
        final int i = CtuluLibDialog.confirmExitIfProjectisModified(getFrame());
        if (i == JOptionPane.CANCEL_OPTION) {
          return false;
        }
        if (i != JOptionPane.NO_OPTION) {
          project.getManager().saveProject(false);
          // c_.save(this, _interface);
        }
      }
      project.close();
      project = null;
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          setEnabledForAction("CREER", true);
          setEnabledForAction("OPEN_AND_RELOAD", true);
          setEnabledForAction("ENREGISTRER", false);
          setEnabledForAction("IMPORTER", false);
          setEnabledForAction(getImportResultsAct(), false);
          setEnabledForAction("IMPORT_PROJECT", false);
        }
      });
    }

    return true;
  }

  @Override
  public boolean confirmExit() {
    final boolean b = super.confirmExit();
    if (b && project != null && !saveAndCloseProjet(null)) {
      return false;
    }
    return b;
  }

  @Override
  public void actionPerformed(final ActionEvent event) {
    final String com = event.getActionCommand();

    if ("CREER".equals(com)) {

      if (project != null) {
        final int reponse = JOptionPane.showConfirmDialog(this.getParentComponent(),
            TrResource.getS("Le projet courant va �tre ferm�. \n Voulez vous sauvegarder le projet courant avant sa fermeture?"));
        if (reponse == JOptionPane.CANCEL_OPTION) {
          return;
        }
        if (reponse == JOptionPane.OK_OPTION) {
          project.getManager().saveProject(false);
        }
        // -- nettoyage du projet pr�c�dent --//

        project.getManager().clearProject();
      } else {
        project = new TrPostProjet(this);
        project.setMenuPost();
      }
      // -- ouverture d'un fichier r�sultat qui sera automatiquement ajout� dans la layout courante --//
      project.createNewLayoutFrame();
      // -- ouverture d'un fichier r�sultat --//
      final TrPostActionChooseAndCreateCalque chooserSource = new TrPostActionChooseAndCreateCalque(project, project.getImpl()
          .getCurrentLayoutFille().controller_);
      chooserSource.actionPerformed(null);
    } else if ("OUVRIR".equals(com)) {
      doOpenProject(false);
    } else if ("OPEN_AND_RELOAD".equals(com)) {
      doOpenProject(true);
    } else if ("FERMER".equals(com)) {
      new CtuluTaskOperationGUI(this, TrResource.getS("Fermer")) {
        @Override
        public void act() {
          saveAndCloseProjet(createProgressionInterface(this));
        }
      }.start();
    } else if ("ENREGISTRER".equals(com) && project != null) {
      new CtuluTaskOperationGUI(this, TrResource.getS("Enregistrement")) {
        @Override
        public void act() {

          // -- enregistrement sauce widget --//
          project.getManager().saveProject(false);
        }
      }.start();
    } else if ("ENREGISTRERSOUS".equals(com) && project != null) {
      new CtuluTaskOperationGUI(this, TrResource.getS("Enregistrement Sous")) {
        @Override
        public void act() {

          // -- enregistrement sauce widget --//
          project.getManager().saveProject(true);
        }
      }.start();
    } else if ("CONFIG_GRAPH_MODEL".equals(com)) {
      PaletteCourbes.getInstance("prepro.xml").displayUiManager(this);
    } else if (getImportResultsAct().equals(com) && project != null) {
      File scopeFile = ouvrirFileChooser("Scope", null);
      if (scopeFile != null) {
        doImportScopeFile(scopeFile, false);
      }
    } else {
      super.actionPerformed(event);
    }
  }

  private void doOpenProject(boolean forceReload) {
    // -- ouverture d un nouveau projet --//
    if (project != null) {
      final int reponse = JOptionPane.showConfirmDialog(this.getParentComponent(),
          TrResource.getS("Le projet courant va �tre ferm�. \n Voulez vous sauvegarder le projet courant avant sa fermeture?"));
      if (reponse == JOptionPane.CANCEL_OPTION) {
        return;
      }
      if (reponse == JOptionPane.OK_OPTION) {
        project.getManager().saveProject(false);
      }

      // -- nettoyage du projet pr�c�dent --//
      project.getManager().clearProject();
    } else {
      project = new TrPostProjet(this);
      project.setMenuPost();
    }

    project.getManager().loadProject(false, null, forceReload);
  }

  @Override
  protected void ouvrir() {
    cmdOuvrirFile(super.ouvrirFileChooser(TrResource.getS("Fichier post"), null));
  }

  @Override
  public String getHelpDir() {
    return getInformationsSoftware().man + "post/";
  }

  /**
   * Methode appelee pour creer un projet .POST
   *
   * @param _f
   * @param forceRecompute TODO
   */
  public void cmdOuvrirLayoutFile(final File _f, final boolean forceRecompute) {

    if (project == null) {
      project = new TrPostProjet(TrPostCommonImplementation.this);
      project.active(TrPostCommonImplementation.this);
      BuLib.invokeNowOrLater(new Runnable() {
        @Override
        public void run() {
          setEnabledForAction("ENREGISTRER", true);
          setEnabledForAction("ENREGISTRERSOUS", true);
          setEnabledForAction("IMPORTER", true);
          setEnabledForAction(getImportResultsAct(), true);
          setEnabledForAction("IMPORT_PROJECT", true);
          setEnabledForAction("RECHERCHER", true);
        }
      });
    }
    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {
        project.getManager().loadProject(true, _f, forceRecompute);
      }
    });
  }

  public void cmdOuvrirScopFile(final File scopeFile) {

    if (project == null) {
      // setProjet(new TrPostProjet(TrPostCommonImplementation.this));
      project = new TrPostProjet(TrPostCommonImplementation.this);
      project.active(TrPostCommonImplementation.this);
      BuLib.invokeNowOrLater(new Runnable() {
        @Override
        public void run() {
          setEnabledForAction("ENREGISTRER", true);
          setEnabledForAction("ENREGISTRERSOUS", true);
          setEnabledForAction("IMPORTER", true);
          setEnabledForAction(getImportResultsAct(), true);
          setEnabledForAction("IMPORT_PROJECT", true);
          setEnabledForAction("RECHERCHER", true);
        }
      });
    }
    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {
        doImportScopeFile(scopeFile, true);
      }
    });
  }

  private void doImportScopeFile(final File scopeFile, final boolean inNewLayout) {
    // -- creation de la trpostlayoutFille --//
    if (inNewLayout) {
      final TrPostLayoutFille postFrame = new TrPostLayoutFille(getCurrentProject());
      // -- sauvegarde du persitant --//
      TrPostCommonImplementation.this.addInternalFrame(postFrame);
    } else {
      TrPostLayoutFille current = getCurrentLayoutFilleOrFirst();
      if (current == null) {
        doImportScopeFile(scopeFile, true);
      } else {
        current.toFront();
      }
    }
    // -- creation du wizard scop --//
    final TrPostScopCourbeTreeModel modelGraphe = new TrPostScopCourbeTreeModel();
    final TrPostWizardCreateScope wizard = new TrPostWizardCreateScope(TrPostCommonImplementation.this.getCurrentProject(), modelGraphe, scopeFile);
    final BuWizardDialog DialogWizard = new BuWizardDialog(TrPostCommonImplementation.this.getFrame(), wizard);
    // --affichage du wizard --//
    DialogWizard.setSize(600, 600);
    DialogWizard.setLocationRelativeTo(TrPostCommonImplementation.this.getFrame());
    DialogWizard.setVisible(true);
  }

  @Override
  public void cmdOuvrirFile(final File _f) {
    final TrPostCommonImplementation alreadyOpen = getLauncher().findPostWithOpenedFile(_f);
    if (alreadyOpen == null) {
      TrPostMultiSourceActivator.activeFile(_f, this);
    } else {
      error(TrLib.getMessageAlreadyOpen(_f));
      alreadyOpen.getFrame().toFront();
    }
  }

  /**
   * @return the project
   */
  public TrPostProjet getProject() {
    return project;
  }
}
