/**
 * @file         PrertResource.java
 * @creation     2002-09-25
 * @modification $Date: 2006-09-19 15:07:30 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuIcon;
import com.memoire.bu.BuResource;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.fdico.FDicoResource;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * @version $Id: TrResource.java,v 1.8 2006-09-19 15:07:30 deniger Exp $
 * @author Fred Deniger
 */
public final class TrResource extends FudaaResource {

  public final static TrResource TR = new TrResource(FDicoResource.FD);

  private TrResource(final BuResource _b) {
    super(_b);
  }

  public static String getS(final String _s) {
    return TR.getString(_s);
  }

  public static String getS(final String _s, final String _v0) {
    return TR.getString(_s, _v0);
  }

  public static String getS(final String _s, final String _v0, final String _v1) {
    return TR.getString(_s, _v0, _v1);
  }
  
  public static String getS(final String _s, final String _v0, final String _v1,final String _v3) {
    return TR.getString(_s, _v0, _v1,_v3);
  }

  public static BuIcon getMeshIcon() {
    return TrResource.TR.getIcon("prepro-mesh");
  }

  public static String getMeshName() {
    return TrResource.getS("Meshview");
  }

  public static BuIcon getSupervisorIcon() {
    return TrResource.TR.getIcon("prepro-super");
  }

  public static String getSupervisorName() {
    return TrResource.getS("Superviseur");
  }

  public static BuIcon getPostIcon() {
    return TrResource.TR.getIcon("prepro-post");
  }

  public static BuIcon getPostLayoutIcon() {
	    return TrResource.TR.getIcon("crystal16_browser.png");
	  }
  
  public static BuIcon getScopIcon() {
	    return EbliResource.EBLI.getIcon("curves");
	  }
  
  public static String getPostName() {
    return TrResource.getS("Post");
  }

  public static BuIcon getEditorIcon() {
    return TrResource.TR.getIcon("prepro-ed");
  }

  public static String getEditorName() {
    return TrResource.getS("Editeur");
  }
  
  @Override
  public  BuIcon getIcon(final String path) {
	  
	  BuIcon  res = super.getIcon("/resources/org/fudaa/fudaa/tr/common/"+path);
	  if(res == null || "default".equals(res.getDescription()))
		  res = super.getIcon(path);
	  if(res == null || "default".equals(res.getDescription()))
		  res = super.getIcon("/org/fudaa/fudaa/tr/common/"+path);
	  
	  return res;
  }

}
