package org.fudaa.fudaa.tr.post.persist;

import java.io.Serializable;

/**
 * classe utiliseer pour donner des noms potable poru le xml parser
 *
 * @author Adrien Hadoux
 */
public class LayoutDescriptor implements Serializable {
  public String pathRelatifLayout;

  public LayoutDescriptor(String pathRelatifLayout) {
    super();
    this.pathRelatifLayout = pathRelatifLayout;
  }
}
