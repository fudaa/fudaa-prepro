/*
 * @creation 5 ao�t 2005
 * 
 * @modification $Date: 2007-05-04 14:01:51 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.fu.FuLog;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.dodico.h2d.H2dVariableProviderContainerInterface;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.fudaa.meshviewer.model.MvElementModel;
import org.fudaa.fudaa.meshviewer.model.MvExpressionSupplierElement;
import org.fudaa.fudaa.meshviewer.model.MvExpressionSupplierNode;
import org.fudaa.fudaa.meshviewer.model.MvNodeModel;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id: TrExpressionSupplierForData.java,v 1.11 2007-05-04 14:01:51 deniger Exp $
 */
public class TrExpressionSupplierForData implements EbliFindExpressionContainerInterface {

  Map varList_;

  private final H2dVariableProviderContainerInterface varContainer_;

  /**
   * @param _container
   */
  public TrExpressionSupplierForData(final H2dVariableProviderContainerInterface _container) {
    super();
    varContainer_ = _container;
  }

  @Override
  public void initialiseExpr(final CtuluExpr _exp) {
    final H2dVariableProviderInterface prov = varContainer_.getVariableDataProvider();
    if (prov == null) { return; }
    final Collection l = prov.getUsableVariables();
    if (l != null) {
      varList_ = new HashMap(l.size());
      for (final Iterator it = l.iterator(); it.hasNext();) {
        final H2dVariableType t = (H2dVariableType) it.next();
        varList_.put(_exp.addVar(t.getShortName(), t.getName()), t);
      }
    }
  }

  @Override
  public void majVariable(final int _idx, final Variable[] _varToUpdate) {
    if (_varToUpdate != null) {
      final H2dVariableProviderInterface prov = varContainer_.getVariableDataProvider();
      for (int i = _varToUpdate.length - 1; i >= 0; i--) {
        final H2dVariableType t = (H2dVariableType) varList_.get(_varToUpdate[i]);
        if (t != null) {
          _varToUpdate[i].setValue(prov.getViewedModel(t).getObjectValueAt(_idx));
        }
      }
    }

  }

  /**
   * @author Fred Deniger
   * @version $Id: TrExpressionSupplierForData.java,v 1.11 2007-05-04 14:01:51 deniger Exp $
   */
  public static class Node extends MvExpressionSupplierNode {

    TrExpressionSupplierForData data_;

    /**
     * @param _container le container de donnees
     * @param _nodes le modele des noeuds
     */
    public Node(final H2dVariableProviderContainerInterface _container, final MvNodeModel _nodes) {
      super(_nodes);
      if (FuLog.isDebug()) {
        FuLog.debug("build with special node supplier");
      }
      data_ = new TrExpressionSupplierForData(_container);
    }

    @Override
    public void initialiseExpr(final CtuluExpr _expr) {
      super.initialiseExpr(_expr);
      data_.initialiseExpr(_expr);
    }

    @Override
    public void majVariable(final int _idx, final Variable[] _varToUpdate) {
      super.majVariable(_idx, _varToUpdate);
      data_.majVariable(_idx, _varToUpdate);
    }

  }

  /**
   * @author Fred Deniger
   * @version $Id: TrExpressionSupplierForData.java,v 1.11 2007-05-04 14:01:51 deniger Exp $
   */
  public static class Element extends MvExpressionSupplierElement {

    final TrExpressionSupplierForData trExpressionSupplierForData;

    /**
     * @param _nodes
     */
    public Element(final H2dVariableProviderContainerInterface _container, final MvElementModel _nodes) {
      super(_nodes);
      trExpressionSupplierForData = new TrExpressionSupplierForData(_container);
    }

    @Override
    public void initialiseExpr(final CtuluExpr _expr) {
      super.initialiseExpr(_expr);
      trExpressionSupplierForData.initialiseExpr(_expr);
    }

    @Override
    public void majVariable(final int _idx, final Variable[] _varToUpdate) {
      super.majVariable(_idx, _varToUpdate);
      trExpressionSupplierForData.majVariable(_idx, _varToUpdate);
    }

  }

  /**
   * @author Fred Deniger
   * @version $Id: TrExpressionSupplierForData.java,v 1.11 2007-05-04 14:01:51 deniger Exp $
   */
  public static class Elements extends MvExpressionSupplierElement {

    TrExpressionSupplierForData[] exprData_;

    /**
     * @param _nodes
     */
    public Elements(final H2dVariableProviderContainerInterface[] _container, final MvElementModel _nodes) {
      super(_nodes);
      exprData_ = new TrExpressionSupplierForData[_container.length];
      for (int i = 0; i < _container.length; i++) {
        exprData_[i] = new TrExpressionSupplierForData(_container[i]);
      }
    }

    @Override
    public void initialiseExpr(final CtuluExpr _expr) {
      super.initialiseExpr(_expr);
      for (int i = 0; i < exprData_.length; i++) {
        exprData_[i].initialiseExpr(_expr);
      }
    }

    @Override
    public void majVariable(final int _idx, final Variable[] _varToUpdate) {
      super.majVariable(_idx, _varToUpdate);
      for (int i = 0; i < exprData_.length; i++) {
        exprData_[i].majVariable(_idx, _varToUpdate);
      }
    }

  }

}
