package org.fudaa.fudaa.tr.post.replay;

import com.memoire.bu.BuButton;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.concurrent.Callable;

/**
 * Classe qui se charge d'afficher et rejouer les donn�es d'origine des courbes TR. C'set a dire les courbes spatiales et evolutions temporelles.
 *
 * @author Adrien Hadoux
 */
public class TrReplayerTimeEvolution extends AbstractTrReplayer {
  public TrReplayerTimeEvolution(TrPostSource source,
                                 final TrPostCommonImplementation implementation) {
    super(source, implementation);
  }

  /**
   * Constructeur d'une dialog qui affiche la vue 2d avec le point initial qui a servi a la creation du modele.
   *
   * @param modele
   */
  public void displayTimeEvolutionOrigin(final TrPostCourbeModel modele, final TrPostCommonImplementation implementation) {

    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    final TrReplayTemporelleVisuCustomizer customizer = new TrReplayTemporelleVisuCustomizer(modele);
    final TrPostVisuPanel vue2d = TrReplayVue2dBuilder.constructVue2d(implementation, modele.source_, customizer);
    vue2d.removeCalqueLegend();

    // -- on affiche le contenu dans une dialog--//
    final TrReplayGenereVue2DInSceneAction action = new TrReplayGenereVue2DInSceneAction(implementation, modele.source_, customizer, modele.getTitle());
    final CtuluDialogPanel panel = constructDialog(getTrueCallable(), vue2d, modele, null, false, "<html><body>"
        + TrResource.getS("Origine {0}. <br /> Le point est encadr� en rouge", modele.getTitle()), action);

    // -- on positionne la bonne variable et le bon pas de temps --//
    comboVar_.getCb().setSelectedItem(modele.getVar());
    panel.afficheModale(implementation.getParentComponent(), TrResource.getS("Origine ") + " " + modele.getTitle(),
        createRestaurerRunnable(vue2d));
  }

  /**
   * Methode qui v�rifie les saisies sur la vue 2d et r�alise les cr�ations/ecrasements avec les nouveaut�s des profils temporels.
   */
  private boolean replayEvolutionFor(final TrPostSource source, final TrPostVisuPanel vue2d,
                                     final TrPostCourbeTreeModel tree, final TrPostCourbeModel modele, final TrPostCommonImplementation implementation) {
    TrPostInterpolatePoint newPointInterpole = null;
    if (vue2d.isSelectionOkForEvolution()) {
      final ZCalqueAffichageDonnees calque = (ZCalqueAffichageDonnees) vue2d.getCalqueActif();

      final int[] idx = calque.getLayerSelection().getSelectedIndex();
      if (idx.length > 1) {
        implementation.error(
            TrLib.getString("Il faut choisir un seul point afin de recalculer les donn�es de la courbe"));
        return false;
      }
      H2dVariableType selectedVar = null;
      if (calque instanceof TrIsoLayer) {
        selectedVar = ((TrIsoLayer) calque).getIsoModel().getVariableSelected();
      } else if (calque instanceof TrPostFlecheLayer) {
        selectedVar = ((TrPostFlecheLayer) calque).getSelectedVar();
      }

      // -- cas point reel
      H2dVariableType newVariable = selectedVar;
      int newPointReel = idx[0];

      // -- on rejoue les donn�es avec les nouvelles valeurs --//
      final String name = newVariable == null ? "?" : newVariable.getName();
      if (this.ecraser_.isSelected()) {
        String string = TrResource.getS(
            "La courbe actuelle de mod�le: variable:{0}, point N� {1} va �tre recalcul�e pour les donn�es suivantes",
            modele.getVar().getName(), modele.getSelectionPointName());
        String string2 = TrResource.getS("Variable: {0}, point N� {1}", name, Integer.toString(newPointReel + 1));
        implementation.message(string + "\n" + string2);
      } else {
        implementation.message(TrResource.getS(
            "Une nouvelle courbe �volution temporelle va �tre calcul�e pour la variable: {0}, et le point N� {1}.\nLa courbe actuelle ne sera pas �cras�e.",
            name, Integer.toString(newPointReel + 1)));
      }

      // -- on rejoue les donn�es avec ou sans ecrasement --//
      tree.replayPoints(modele, source, newPointReel, vue2d.getCmdMng(), implementation.getMainProgression(),
          newVariable, this.ecraser_.isSelected());

      return true;
    } else if (vue2d.isSelectionOkForEvolutionSonde()) {
      // -- cas point interpol� --//
      H2dVariableType newVariable = vue2d.getSelectedVarInCalqueActif();
      newPointInterpole = vue2d.getInterpolePointForEvol();

      // -- on rejoue les donn�es avec les nouvelles valeurs --//
      if (this.ecraser_.isSelected()) {
        String string = TrResource.getS(
            "La courbe actuelle de mod�le variable {0}, point N� {1} va �tre recalcul�e pour les donn�es suivantes",
            modele.getVar().getName(), modele.getSelectionPointName());
        String forData = TrResource.getS("Variable: {0}\n Point interpol�: {1}", newVariable.getName(),
            CtuluNumberFormatDefault.formatPoint(newPointInterpole.getX(), newPointInterpole.getY(), source.getPrecisionModel().getFormatter()));
        implementation.message(string + "\n" + forData);
      } else {
        String message = TrResource.getS(
            "Une nouvelle courbe �volution temporelle va �tre calcul�e pour la variable: {0}, point interpol�  {1}\nLa courbe actuelle ne sera pas supprim�e",
            newVariable.getName(), CtuluNumberFormatDefault.formatPoint(newPointInterpole.getX(), newPointInterpole.getY(),
                source.getPrecisionModel().getFormatter()));
        implementation.message(message);
      }
      // -- on rejoue les donn�es avec ou sans ecrasement --//
      tree.replayPoints(modele, source, newPointInterpole, vue2d.getCmdMng(), implementation.getMainProgression(),
          newVariable, this.ecraser_.isSelected());

      return true;
    } else {
      implementation.error(TrLib.getString(
          "Il faut choisir un seul point afin de recalculer les donn�es de la courbe"));
      return false;
    }
  }

  /**
   * Constructeur d'une dialog qui affiche la vue 2d avec le point initial qui a servi a la creation du modele. Ajoute �galement la liste des
   * variables � choisir. Il est possible dans la vue de s�lectionner un autre point ou plusieurs. LA validation de la frame permettra de r�cup�rer
   * les nouveaux point ainsi que les variables qui seront utilis�es pour refaire le calcul.
   *
   * @param modele
   */
  @SuppressWarnings("serial")
  public void displayTimeEvolutionReplayData(final TrPostSource source, final TrPostCourbeTreeModel treeModel,
                                             final TrPostCourbeModel modele, final TrPostCommonImplementation implementation) {

    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    final TrPostVisuPanel vue2d = buildVue2d(implementation, source, new TrReplayTemporelleEditCustomizer(modele));
    vue2d.removeCalqueLegend();

    // -- creation du panel de choix des variables a selectionner --//
    rejouerSrc_ = new BuButton();
    // -- on affiche le contenu dans une dialog--//
    final String title = "<html><body>"
        + TrResource.getS("Choisir le point et la variable qui seront utilis�s pour refaire le calcul (interpolation possible).<br /> Le carr� rouge d�signe le point d'origine.");
    Callable<Boolean> okAction = new Callable<Boolean>() {
      @Override
      public Boolean call() throws Exception {
        return replayEvolutionFor(source, vue2d, treeModel, modele, implementation);
      }
    };
    final TrReplayDialog dialog = constructEditionDialog(okAction, vue2d, modele, null, title);
    rejouerSrc_.setAction(new AbstractAction(TrLib.getString("Choisir un autre fichier r�sultat")) {
      @Override
      public void actionPerformed(final ActionEvent e) {
        final TrPostSource selectedSource = selectSource(implementation.getCurrentProject(),
            (String) getValue(Action.NAME), implementation.getFrame(), source);
        if (selectedSource != null) {
//          replayWithAnotherSource = true;
          // -- on ferme l'interface courante --//
          if (dialog != null) {
            dialog.getDialog().cancel();
          }
          // -- on rappelle l'interface --//
          new TrReplayerTimeEvolution(selectedSource, implementation).displayTimeEvolutionReplayData(selectedSource, treeModel, modele, implementation);
        }
      }
    });
    if (implementation.getCurrentProject().getSources().getSrcSize() < 2) {
      rejouerSrc_.setEnabled(false);
      rejouerSrc_.setToolTipText(TrLib.getString("Un seul r�sultat est charg�"));
    }

    // -- on positionne la bonne variable et le bon pas de temps --//
    comboVar_.getCb().setSelectedItem(modele.getVar());

    dialog.afficheModale(implementation.getParentComponent(), TrResource.getS("Rejouer")
        + " " + modele.getTitle(), createRestaurerRunnable(vue2d));
  }
}
