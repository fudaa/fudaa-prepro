package org.fudaa.fudaa.tr.post.persist;

/**
 * Format de fichier unique propre au trpost.
 * Le fichier est un repertoire .POST
 * @author Adrien Hadoux
 *
 */

import com.memoire.bu.BuFileFilter;
import java.io.File;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: SerafinFileFormat.java,v 1.37 2007-05-04 13:47:27 deniger Exp $
 */
public final class TrPostFileFormat extends FileFormatUnique {

  private static final TrPostFileFormat INSTANCE = new TrPostFileFormat();

  /**
   * @return le singleton
   */
  public static TrPostFileFormat getInstance() {
    return INSTANCE;
  }

  protected TrPostFileFormat() {
    super(2);
    extensions_ = new String[] { "post", "POST" };
    id_ = "POST";
    nom_ = "post";
    description_ = TrResource.getS("Projet int�gral post avec layout");
    software_ = null;
    type_ = null;
  }

  @Override
  public BuFileFilter createFileFilter() {
    return new TrPostFileFilter(this);
  }

  @Override
  public String getVersionName() {
    return CtuluLibString.UN;
  }

  @Override
  public CtuluIOOperationSynthese read(final File _f, final ProgressionInterface _prog) {
    return read(this, _f, _prog);
  }

  @Override
  public CtuluIOOperationSynthese write(final File _f, final Object _source, final ProgressionInterface _prog) {
    return write(this, _f, _source, _prog);
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return null;
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return null;
  }
  
  
  public  File getSetupFromMainDirectory(File mainDir){
	  
	  if(mainDir!=null && mainDir.isDirectory() && mainDir.exists()){
		
		  if(TrPostFileFilter.acceptDirectoryPOST(mainDir)){
			  
			  String path=mainDir.getAbsolutePath();
			  path+=File.separator+TrPostPersistenceFileFinder.MAIN_SETUP_FILENAME;
			  File setup=new File(path);
			  
			  if(setup!=null && setup.exists() && setup.isFile())
				  return setup;
		  }
		  
	  }
	  return null;
	  
  }
  
}
