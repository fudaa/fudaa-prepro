/*
 * @creation 19 sept. 06
 * @modification $Date: 2007-01-19 13:14:10 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuInformationsDocument;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrGrapheSimpleTimeFille;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author fred deniger
 * @version $Id: TrPostRubarLiminFilleSimple.java,v 1.2 2007-01-19 13:14:10 deniger Exp $
 */
public class TrPostRubarLiminFilleSimple extends TrGrapheSimpleTimeFille {

  public TrPostRubarLiminFilleSimple(final EGGraphe _g, final String _titre, final FudaaCommonImplementation _appli,
      final BuInformationsDocument _id) {
    super(_g, _titre, _appli, _id, new EGTableGraphePanel());
    setName("ifPostLimniSimple");
  }

  @Override
  protected String getMenuTitle() {
    return TrPostRubarLiminFilleSimple.getFilleTitle();
  }

  @Override
  public String getShortHtmlHelp() {
    return TrPostRubarLiminFilleSimple.getShortHtmlHelp(getTitle(), impl_);
  }

  public static String getShortHtmlHelp(final String _title, final FudaaCommonImplementation _impl) {
    final CtuluHtmlWriter buf = new CtuluHtmlWriter();
    buf.h2Center(_title);
    buf.close(buf.para(), TrResource.getS("Affiche les limnigrammes au noeud s�lectionn�"));
    buf.h3(TrResource.getS("Documents associ�s"));
    buf.addTag("lu");
    buf.close(buf.addTag("li"), _impl.buildLink(TrResource.getS("Description du composant d'affichage des courbes"),
        "common-curves"));
    buf.close(buf.addTag("li"), _impl.buildLink(TrResource.getS("Comment utiliser la vue 2D"), "post-vue2d"));
    return buf.toString();
  }

  public static String getFilleTitle() {
    return H2dResource.getS("Limnigrammes");
  }
}