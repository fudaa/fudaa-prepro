/**
 * @creation 5 janv. 2005 @modification $Date: 2007-03-30 15:40:05 $ @license GNU General Public License 2 @copyright (c)1998-2001
 *     CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluCellButtonEditor;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageTypeClient;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarOuvrageEditorPanel.java,v 1.19 2007-03-30 15:40:05 deniger Exp $
 */
public class TrRubarOuvrageElementaireEditorPanel implements H2dRubarOuvrageTypeClient {
  protected class OuvrageEditor implements H2dRubarOuvrageTypeClient {
    boolean result_;

    @Override
    public void visitApport(final Object _o) {
      result_ = TrRubarOuvrageEditorFactory.editApport(_o, ui.getParentComponent(), evolManager, tarageManager);
    }

    @Override
    public void visitBreche(final Object _o) {
      result_ = TrRubarOuvrageEditorFactory.editBreche(_o, ui.getParentComponent());
    }

    @Override
    public void visitComposite(Object _o) {
      result_ = TrRubarOuvrageCompositeEditorPanel.edit(grid, (H2dRubarOuvrageElementaireComposite) _o, ouvrage, ui, evolManager, tarageManager);
    }

    @Override
    public void visitDeversoir(final Object _o) {
      result_ = TrRubarOuvrageEditorFactory.editValuablesOuvrages((H2dRubarOuvrageElementaireValuablesInterface) _o,
          ui.getParentComponent());
    }

    @Override
    public void visitDeversoirHydraulique(Object _o) {
      result_ = TrRubarOuvrageEditorFactory.editValuablesOuvrages((H2dRubarOuvrageElementaireValuablesInterface) _o,
          ui.getParentComponent());
    }

    @Override
    public void visitOrificeCirculaire(Object _o) {
      result_ = TrRubarOuvrageEditorFactory.editValuablesOuvrages((H2dRubarOuvrageElementaireValuablesInterface) _o,
          ui.getParentComponent());
    }

    @Override
    public void visitTransfert(final Object _o) {
      result_ = TrRubarOuvrageEditorFactory.editTransfert(_o, ui.getParentComponent(), evolManager, tarageManager);
    }
  }

  CtuluCommandContainer cmd_;
  private CtuluListEditorPanel editor;
  final H2dRubarGridAreteSource grid;
  CtuluListEditorModel model_;
  private H2dRubarOuvrage ouvrage;
  private Object[] ouvrageTypes;
  final TrRubarEvolutionManager evolManager;
  final TrRubarTarageCourbesManager tarageManager;
  CtuluUI ui;

  /**
   * @param _o l'ouvrage a editer
   * @param _cmd le receveur de commande
   * @param _impl l'impl parente
   */
  public TrRubarOuvrageElementaireEditorPanel(final H2dRubarOuvrageElementaireInterface[] _o, final H2dRubarGridAreteSource _grid,
                                              final CtuluCommandContainer _cmd, final CtuluUI _impl,
                                              final TrRubarEvolutionManager evolManager, TrRubarTarageCourbesManager tarageManager) {
    this.grid = _grid;
    ui = _impl;
    this.evolManager = evolManager;
    this.tarageManager = tarageManager;
    cmd_ = _cmd;
    ouvrageTypes = H2dRubarOuvrageType.LIST.toArray();
    final BuPanel pnTop = new BuPanel();
    pnTop.setLayout(new BuGridLayout(2, 5, 5));
    model_ = new CtuluListEditorModel(true) {
      @Override
      public Object createNewObject() {
        return addOuvrageElementaire();
      }

      @Override
      public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      }
    };
    model_.setMaxValueNb(5);
    model_.setData(_o);
    editor = new CtuluListEditorPanel(model_, true, true, false, false, false);
    editor.setValueListCellEditor(new CtuluCellButtonEditor(null) {
      @Override
      protected void doAction() {
        editOuvrage(model_.getIndexOf(value_));
      }
    });
  }

  public CtuluListEditorPanel getEditor() {
    return editor;
  }

  protected Object addOuvrageElementaire() {
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuGridLayout(2, 5, 5));
    pn.addLabel(H2dResource.getS("Type ouvrage élémentaire"));
    final BuComboBox cb = new BuComboBox();

    cb.setModel(new DefaultComboBoxModel(ouvrageTypes));
    pn.add(cb);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(ui.getParentComponent(), TrResource.getS("Choix ouvrage élémentaire")))) {
      final H2dRubarOuvrageTypeCreator c = new H2dRubarOuvrageTypeCreator(evolManager.getProjectType(), evolManager.getRubarParam().getBcMng().getNbConcentrationBlocks());
      c.setGridAreteSource(this.grid);
      final H2dRubarOuvrageType t = (H2dRubarOuvrageType) cb.getItemAt(cb.getSelectedIndex());
      t.visit(c, null);
      if (H2dRubarOuvrageType.BRECHE.equals(t)) {
        H2dRubarOuvrageElementaireBreche breach = (H2dRubarOuvrageElementaireBreche) c.result_;
        double za1 = ouvrage.getZaAmont();
        breach.setElevationOfTheTopOfTheDike(za1 + 5);
        breach.setElevationAtTheBasementOfTheDike(za1);
        breach.setBreachBottomElevation(za1);
      }
      final OuvrageEditor e = new OuvrageEditor();
      t.visit(e, c.result_);
      if (e.result_) {
        return c.result_;
      }
    }
    return null;
  }

  void editOuvrage(final int _r) {
    final H2dRubarOuvrageElementaireInterface ouv = (H2dRubarOuvrageElementaireInterface) model_.getValueAt(_r);
    ouv.getType().visit(this, ouv);
  }

  public H2dRubarOuvrage getOuvrage() {
    return ouvrage;
  }

  public H2dRubarOuvrageElementaireAbstract[] getOuvrages() {
    final H2dRubarOuvrageElementaireAbstract[] newOuv = new H2dRubarOuvrageElementaireAbstract[model_.getRowCount()];
    for (int i = newOuv.length - 1; i >= 0; i--) {
      newOuv[i] = (H2dRubarOuvrageElementaireAbstract) model_.getValueAt(i);
    }
    return newOuv;
  }

  public Object[] getOuvrageTypes() {
    return ouvrageTypes;
  }

  public void setOuvrage(H2dRubarOuvrage ouvrage) {
    this.ouvrage = ouvrage;
  }

  public void setOuvrageTypes(Object[] ouvrageTypes) {
    this.ouvrageTypes = ouvrageTypes;
  }

  @Override
  public void visitApport(final Object _o) {
    TrRubarOuvrageEditorFactory.editApport(_o, ui.getParentComponent(), evolManager, tarageManager);
  }

  @Override
  public void visitBreche(final Object _o) {
    TrRubarOuvrageEditorFactory.editBreche(_o, ui.getParentComponent());
  }

  @Override
  public void visitComposite(Object _o) {
    TrRubarOuvrageCompositeEditorPanel.edit(grid, (H2dRubarOuvrageElementaireComposite) _o, ouvrage, ui, evolManager, tarageManager);
  }

  @Override
  public void visitDeversoir(final Object _o) {
    TrRubarOuvrageEditorFactory.editValuablesOuvrages((H2dRubarOuvrageElementaireValuablesInterface) _o, ui.getParentComponent());
  }

  @Override
  public void visitDeversoirHydraulique(Object _o) {
    TrRubarOuvrageEditorFactory.editValuablesOuvrages((H2dRubarOuvrageElementaireValuablesInterface) _o, ui.getParentComponent());
  }

  @Override
  public void visitOrificeCirculaire(Object _o) {
    TrRubarOuvrageEditorFactory.editValuablesOuvrages((H2dRubarOuvrageElementaireValuablesInterface) _o, ui.getParentComponent());
  }

  @Override
  public void visitTransfert(final Object _o) {
    TrRubarOuvrageEditorFactory.editTransfert(_o, ui.getParentComponent(), evolManager, tarageManager);
  }
}
