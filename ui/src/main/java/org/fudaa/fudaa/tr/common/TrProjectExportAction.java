package org.fudaa.fudaa.tr.common;

import org.fudaa.fudaa.tr.TrEditorImplementation;

public class TrProjectExportAction {
  private final TrEditorImplementation editor;

  public TrProjectExportAction(TrEditorImplementation editor) {
    this.editor = editor;
  }

  public void manageExport() {
    editor.currentProject_.exportMaillage();
  }
}
