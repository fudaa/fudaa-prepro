/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.fudaa.tr.post;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author deniger
 */
interface TrPostSourceReaderInterface {

  /**
   * @return les fichiers utilis�s pour les resultats
   */
  Collection<File> getFiles();

  /**
   * @return la liste des variables lues dans le fichier source
   */
  List<H2dVariableType> getInitVar();

  /**
   * @param _varIdx l'indice de la variable
   * @param _timeIdx le pas de temps
   * @return les donnees
   */
  EfData getInitData(CtuluVariable _varIdx, int _timeIdx);

  /**
   * @param _varIdx l'indice de la variable
   * @param _timeIdx le pas de temps
   * @param _ptIdx le point/element demande
   * @return la valeur
   * @throws IOException
   */
  double getInitData(CtuluVariable _varIdx, int _timeIdx, int _ptIdx) throws IOException;

  /**
   * @return true si donn�es definies aux �l�ments
   */
  boolean isVolumique();

  // TrPostInspectorReader createWatcher(TrPostProjet _proj, boolean _auto);

  int getNbTimeStep();

  double getTimeStep(int _idx);

  void close();

  public String getTitle();

  public String getId();

  public void SetId(String id);

}
