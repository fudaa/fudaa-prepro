/**
 * @creation 24 mars 2004
 * @modification $Date: 2007-05-04 14:01:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author Fred Deniger
 * @version $Id: TrPostSourceSerafin.java,v 1.15 2007-05-04 14:01:52 deniger Exp $
 */
public final class TrPostSourceSerafin extends TrPostSourceFromReader {

  protected TrPostSourceSerafin(final TrPostSourceReaderSerafin _reader, final String _titre, final EfGridInterface _g, final CtuluUI _impl) {
    super(_reader, _titre, _g, _impl);
  }

  @Override
  public boolean containsElementVar() {
    return super.getReader().isVolumique();
  }

  public TrPostSourceReaderSerafin getReaderSerafin() {
    return (TrPostSourceReaderSerafin) super.reader_;
  }

  @Override
  public long getReferenceDateInMillis() {
    return ((TrPostSourceReaderSerafin) super.getReader()).getReferenceDateInMillis();
  }

}