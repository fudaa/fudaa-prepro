/*
 * @creation 8 ao�t 2005
 * 
 * @modification $Date: 2007-04-16 16:35:34 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.calque.ZModeleSegment;
import org.fudaa.ebli.calque.find.CalqueFindExpression;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.fudaa.tr.common.TrResource;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id: TrAreteExprSupplier.java,v 1.8 2007-04-16 16:35:34 deniger Exp $
 */
public class TrAreteExprSupplier extends CalqueFindExpression {

  /**
   * le x du milieu.
   */
  Variable xMid_;
  /**
   * le y du milieu.
   */
  Variable yMid_;

  /**
   * le max x.
   */
  Variable xMax_;
  /**
   * le max y.
   */
  Variable yMax_;

  /**
   * le min x.
   */
  Variable xMin_;
  /**
   * le min y.
   */
  Variable yMin_;

  /**
   * le x du point 1.
   */
  Variable x1_;
  /**
   * le y du point 1.
   */
  Variable y1_;
  /**
   * le x du point 1.
   */
  Variable x2_;
  /**
   * le y du point 1.
   */
  Variable y2_;

  /**
   * @param _data
   */
  public TrAreteExprSupplier(final ZModeleSegment _data) {
    super(_data);
  }

  @Override
  public void initialiseExpr(final CtuluExpr _expr) {
    super.initialiseExpr(_expr);
    x1_ = _expr.addVar("edgeX1", TrResource.getS("L'abscisse du point {0} de l'ar�te", CtuluLibString.getString(1)));
    x2_ = _expr.addVar("edgeX2", TrResource.getS("L'abscisse du point {0} de l'ar�te", CtuluLibString.getString(2)));
    y1_ = _expr.addVar("edgeY1", TrResource.getS("L'ordonn�e du point {0} de l'ar�te", CtuluLibString.getString(1)));
    y2_ = _expr.addVar("edgeY2", TrResource.getS("L'ordonn�e du point {0} de l'ar�te", CtuluLibString.getString(2)));
    xMid_ = _expr.addVar("edgeMidX", TrResource.getS("L'abscisse du milieu de l'ar�te"));
    yMid_ = _expr.addVar("edgeMidY", TrResource.getS("L'ordonn�e du milieu de l'ar�te"));

    xMin_ = _expr.addVar("edgeMinX", TrResource.getS("L'abscisse minimale de l'ar�te"));
    xMax_ = _expr.addVar("edgeMaxX", TrResource.getS("L'abscisse maximale de l'ar�te"));
    yMin_ = _expr.addVar("edgeMinY", TrResource.getS("L'ordonn�e minimale de l'ar�te"));
    yMax_ = _expr.addVar("edgeMaxY", TrResource.getS("L'ordonn�e maximale de l'ar�te"));

  }

  @Override
  public void majVariable(final int _idx, final Variable[] _varToUpdate) {
    super.majVariable(_idx, _varToUpdate);
    final ZModeleSegment segM = (ZModeleSegment) super.data_;
    final GrSegment seg = new GrSegment(new GrPoint(), new GrPoint());
    segM.segment(seg, _idx, true);
    double x1 = seg.o_.x_;
    double x2 = seg.e_.x_;
    x1_.setValue(CtuluLib.getDouble(x1));
    x2_.setValue(CtuluLib.getDouble(x2));
    xMid_.setValue(CtuluLib.getDouble((x1 + x2) / 2));
    xMax_.setValue(CtuluLib.getDouble(x1 < x2 ? x2 : x1));
    xMin_.setValue(CtuluLib.getDouble(x1 > x2 ? x2 : x1));
    x1 = seg.o_.y_;
    x2 = seg.e_.y_;
    y1_.setValue(CtuluLib.getDouble(x1));
    y2_.setValue(CtuluLib.getDouble(x2));
    yMid_.setValue(CtuluLib.getDouble((x1 + x2) / 2));
    yMax_.setValue(CtuluLib.getDouble(x1 < x2 ? x2 : x1));
    yMin_.setValue(CtuluLib.getDouble(x1 > x2 ? x2 : x1));
  }
}
