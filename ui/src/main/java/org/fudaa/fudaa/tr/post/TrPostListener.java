/*
 *  @creation     3 mai 2005
 *  @modification $Date: 2006-09-19 15:07:27 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: TrPostListener.java,v 1.3 2006-09-19 15:07:27 deniger Exp $
 */
public interface TrPostListener {

  /**
   * @param _t la variable modifier
   * @param _src la source a l'origine
   * @return true si un changement a ete effectue
   */
  boolean valueChanged(final H2dVariableType _t, TrPostSource _src);

}
