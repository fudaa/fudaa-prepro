/**
 *  @creation     19 janv. 2005
 *  @modification $Date: 2007-02-15 17:10:48 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.rubar;

import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.ebli.calque.BCalqueConfigureSectionAbstract;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * Boite de dialogue utilisee pour configurer le calque ouvrage.
 * 
 * @author Fred Deniger
 * @version $Id: TrRubarOuvrageConfiguration.java,v 1.13 2007-02-15 17:10:48 deniger Exp $
 */
public class TrRubarOuvrageConfiguration extends BCalqueConfigureSectionAbstract implements ZSelectionListener {

  final BSelecteurCheckBox cbShowEdgeDesc_;
  final BSelecteurCheckBox cbShowMeshes_;
  final BSelecteurCheckBox cbDisplayOnlySelected_;
  final BSelecteurCheckBox cbShowMeshesDesc_;
  final BSelecteurCheckBox cbShowStructureNumber_;
  final BSelecteurCheckBox cbShowEdges_;
  final BSelecteurCheckBox cbShowMeshesCenter_;

  private final String keyShowEdes_ = "rubarShowEdges";
  private final String keyShowMeshesCenter_ = "rubarShowMeshesCenter";
  private final String keyShowStructuresNumber_ = "rubarShowStructuresNumber";
  private final String keyShowMeshesDesc_ = "rubarShowMeshesDesc";
  private final String keyShowEdgesDesc_ = "rubarShowEdesDesc";
  private final String keyShowMeshes_ = "rubarShowMeshes";
  private final String keyShowSelected_ = "rubarShowSelected";

  /**
   * @param _target le calque cible
   */
  public TrRubarOuvrageConfiguration(final TrRubarOuvrageLayer _target) {
    super(_target, H2dResource.getS("Ouvrages"));
    cbShowStructureNumber_ = new BSelecteurCheckBox(keyShowStructuresNumber_);
    cbShowStructureNumber_.setTitle(TrResource.getS("Afficher les num�ros des ouvrages"));

    // Ajout du 7 novembre: demande de A.Paquier
    cbShowEdges_ = new BSelecteurCheckBox(keyShowEdes_);
    cbShowEdges_.setTitle(TrResource.getS("Afficher les ar�tes aval/amont"));
    cbShowMeshesCenter_ = new BSelecteurCheckBox(keyShowMeshesCenter_);
    cbShowMeshesCenter_.setTitle(TrResource.getS("Afficher les centres des �l�ments"));
    cbShowMeshes_ = new BSelecteurCheckBox(keyShowMeshes_);
    cbShowMeshes_.setTitle(TrResource.getS("Afficher les �l�ments supports"));
    cbDisplayOnlySelected_ = new BSelecteurCheckBox(keyShowSelected_);
    cbDisplayOnlySelected_.setTitle(TrResource.getS("Afficher uniquement les ouvrages s�lectionn�s"));
    cbShowMeshesDesc_ = new BSelecteurCheckBox(keyShowMeshesDesc_);
    cbShowMeshesDesc_.setTitle(TrResource.getS("Afficher la description des �l�ments"));
    cbShowEdgeDesc_ = new BSelecteurCheckBox(keyShowEdgesDesc_);
    cbShowEdgeDesc_.setTitle(TrResource.getS("Afficher la description des ar�tes"));
    // ce genre d'evt est envoy� quand on passe du mode elt au mode ouvrage.
    target_.addSelectionListener(this);
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    return new BSelecteurInterface[] { cbShowEdges_, cbShowMeshesCenter_, cbShowStructureNumber_, cbShowMeshesDesc_,
        cbShowEdgeDesc_, cbShowMeshes_, cbDisplayOnlySelected_ };
  }

  @Override
  public Object getProperty(final String _key) {
    final TrRubarOuvrageLayer target = (TrRubarOuvrageLayer) target_;
    if (keyShowEdes_ == _key) { return getV(target.showEdges_); }
    if (keyShowMeshesCenter_ == _key) { return getV(target.showMeshesCenter_); }
    if (keyShowStructuresNumber_ == _key) { return getV(target.showWorkNumber_); }
    if (keyShowMeshesDesc_ == _key) { return getV(target.showElementDesc_); }
    if (keyShowEdgesDesc_ == _key) { return getV(target.showEdgeDesc_); }
    if (keyShowMeshes_ == _key) { return getV(target.isPaintElt()); }
    if (keyShowSelected_ == _key) { return getV(target.isOnlySelectedShown()); }
    return null;
  }

  private Boolean getV(final boolean _b) {
    return Boolean.valueOf(_b);
  }

  private boolean getV(final Object _b) {
    return _b == null ? false : ((Boolean) _b).booleanValue();
  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    final TrRubarOuvrageLayer target = (TrRubarOuvrageLayer) target_;
    final boolean v = getV(_newProp);
    if (keyShowEdes_ == _key) {
      target.showEdges_ = v;
    } else if (keyShowMeshesCenter_ == _key) {
      target.showMeshesCenter_ = v;
    } else if (keyShowStructuresNumber_ == _key) {
      target.setShowWorkNumber(v);
    } else if (keyShowMeshesDesc_ == _key) {
      target.setShowElementDesc(v);
    } else if (keyShowEdgesDesc_ == _key) {
      target.setShowEdgeDesc(v);
    } else if (keyShowMeshes_ == _key) {
      target.setPaintElt(v);
    } else if (keyShowSelected_ == _key) {
      target.setDisplayOnlySelected(v);
    }
    target.repaint();
    return true;
  }

  @Override
  public void stopConfiguration() {
    target_.removeSelectionListener(this);

  }

  @Override
  public void selectionChanged(final ZSelectionEvent _evt) {
    cbDisplayOnlySelected_.setEnabled(((TrRubarOuvrageLayer) target_).isOnlySelectedShownEnable());
  }
}