/**
 * @creation 27 mai 2004
 * @modification $Date: 2007-01-19 13:14:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr;

import com.memoire.bu.BuIcon;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuTask;
import java.awt.Frame;
import java.io.File;
import javax.swing.JInternalFrame;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.fudaa.meshviewer.MvActLoad;
import org.fudaa.fudaa.meshviewer.MvControllerSrc;
import org.fudaa.fudaa.meshviewer.MvParentInterface;
import org.fudaa.fudaa.meshviewer.impl.MvActCheckDefault;
import org.fudaa.fudaa.meshviewer.impl.MvActLoadDefault;
import org.fudaa.fudaa.meshviewer.impl.MvInternalFrame;
import org.fudaa.fudaa.tr.common.TrCommonImplementation;
import org.fudaa.fudaa.tr.common.TrProjectPersistence;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrMeshImplementation.java,v 1.19 2007-01-19 13:14:38 deniger Exp $
 */
public class TrMeshImplementation extends TrCommonImplementation implements MvParentInterface {

  private MvActLoad loader_;

  @Override
  public ProgressionInterface createProgression(final BuTask _t) {
    return super.createProgressionInterface(_t);
  }

  @Override
  public void doImportProject() {
    final JInternalFrame currentFrame = getCurrentInternalFrame();
    if (currentFrame instanceof MvInternalFrame) {
      TrProjectPersistence.importForFrame(currentFrame, this);
    }

  }

  @Override
  public Frame getMvParentComponent() {
    return getFrame();
  }

  // private MvActSave save_;

  /**
   *
   */
  public TrMeshImplementation() {
    super();
  }

  @Override
  public void setActionRunning(final boolean _b) {}

  @Override
  public void setMessage(final String _s) {
    setMainMessage(_s);
  }

  @Override
  public void setProgression(final int _v) {
    setMainProgression(_v);
  }

  private void loadFile(final File _f) {
    final MvControllerSrc s = new MvControllerSrc();
    s.setChecker(new MvActCheckDefault());
    if (loader_ == null) {
      loader_ = new MvActLoadDefault();
    }
    s.setParent(this);
    loader_.load(_f, s, this);
  }

  @Override
  public BuIcon getSpecificIcon() {
    return TrResource.getMeshIcon();
  }

  @Override
  public String getSpecificName() {
    return TrResource.getMeshName();
  }

  @Override
  protected void ouvrir() {
    loadFile(null);
  }

  @Override
  public void cmdOuvrirFile(final File _f) {
    if (_f != null) {
      loadFile(_f);
    }
  }

  @Override
  public BuInformationsSoftware getInformationsSoftware() {
    return TrLauncherDefault.getInfosSoftware();
  }

  @Override
  public void init() {
    getFrame().setVisible(false);
    super.init();
    buildTaskView();
  }

  @Override
  public void start() {
    super.start();
    setEnabledForAction("OUVRIR", true);
  }
}