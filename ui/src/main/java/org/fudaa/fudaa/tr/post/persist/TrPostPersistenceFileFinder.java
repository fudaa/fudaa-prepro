package org.fudaa.fudaa.tr.post.persist;

import com.memoire.fu.FuLog;
import org.fudaa.fudaa.tr.post.TrPostSource;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tentative de simplificattion de {@link TrPostPersistenceManager}
 *
 * @author deniger
 */
public class TrPostPersistenceFileFinder {
  /**
   * Peut etre modifi� par l'appli appelabt si l'on veut une extension diff�rente
   */
  static final String EXTENSION = "." + TrPostFileFilter.DEFAULT_EXTENSION;
  /**
   * Point d'entr�e poru charger un projet repertoire. Continet le path du directory. Cela permet de placer ce fichier n'importe ou sur le ddur et de
   * pouvoir charger le projet. Peut etre g�r� comem un raccourcis.
   */
  static final String MAIN_SETUP_FILENAME = "setup";// + EXTENSION;
  /**
   * Fichier qui d�crit tous les contenu layout sauvegard�s Et noms des balises importantes
   */
  private final static String MAIN_LAYOUTS_CONFIG_FILENAME = "info_layout";// "InfoLayout" + EXTENSION;
  private final static String VERSION_FILE = "version.txt";// "InfoLayout" + EXTENSION;
  /**
   * Fichier qui d�crit int�gralement le contenu d'une scene
   */
  final static String EXTENSION_REPLAYOUT = ".LAY";
  final static String LAYOUT_CONFIG_FILENAME = "desc_frame";
  private final static String REP_VAR = "VARIABLES";
  final static String REP_CALQUES = "CALQUES";
  final static String REP_GRAPHES = "GRAPHES";
  final static String REP_TEXTES = "TEXTES";
  private final static String REP_CHECKSUM = "checksum";
  final static String MAIN_WIDGET_NAMES_FILENAME = "nom_frame";// "FramesNames"+ EXTENSION;
  private final static String MAIN_COMMON = "common";// "FramesNames"+ EXTENSION;
  private final File projectBaseDir;

  /**
   * @param projectBaseDir the root directory
   */
  public TrPostPersistenceFileFinder(File projectBaseDir) {
    super();
    this.projectBaseDir = projectBaseDir;
  }

  public File getMainSetupFile() {
    return new File(projectBaseDir, MAIN_SETUP_FILENAME);
  }

  public void saveVersion(String version) {
    File versionFile = new File(projectBaseDir, VERSION_FILE);
    try {
      Files.deleteIfExists(versionFile.toPath());
    } catch (IOException e) {
      FuLog.error(e);
    }
    try (FileWriter writer = new FileWriter(versionFile)) {
      Properties properties = new Properties();
      properties.put("version", version);
      properties.store(writer, "file Version");
    } catch (Exception ex) {
      Logger.getLogger(TrPostPersistenceFileFinder.class.getName()).log(Level.INFO, "message");
    }
  }

  public String getVersion() {
    File versionFile = new File(projectBaseDir, VERSION_FILE);
    String version = null;
    if (versionFile.exists()) {
      try (FileReader reader = new FileReader(versionFile)) {
        Properties properties = new Properties();
        properties.load(reader);
        version = properties.getProperty("version");
      } catch (Exception ex) {
        Logger.getLogger(TrPostPersistenceFileFinder.class.getName()).log(Level.INFO, "message");
      }
    }
    return version;
  }

  public File getBaseDir() {
    return projectBaseDir;
  }

  public File getPath(String relativePath) {
    return new File(projectBaseDir, relativePath);
  }

  public File getVariablesDir() {
    return getPath(TrPostPersistenceFileFinder.REP_VAR);
  }

  public File getMainWidgetDir() {
    return getPath(MAIN_WIDGET_NAMES_FILENAME);
  }

  public File getCommonDir() {
    return getPath(MAIN_COMMON);
  }

  public File getMainLayoutDir() {
    return getPath(MAIN_LAYOUTS_CONFIG_FILENAME);
  }

  public File getMainChecksumDir() {
    return getPath(REP_CHECKSUM);
  }

  public File getChecksumFileForSetup() {
    return new File(getMainChecksumDir(), TrPostPersistenceFileFinder.MAIN_SETUP_FILENAME.toLowerCase() + ".checksum");
  }

  public File getChecksumFileForVariables() {
    return new File(getMainChecksumDir(), TrPostPersistenceFileFinder.REP_VAR.toLowerCase() + ".checksum");
  }

  public File getChecksumFileForSources(TrPostSource src) {
    return new File(getMainChecksumDir(), "sources_" + src.getId() + ".checksum");
  }
}
