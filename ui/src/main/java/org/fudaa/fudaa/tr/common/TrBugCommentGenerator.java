/*
 *  @creation     4 mars 2005
 *  @modification $Date: 2007-05-04 14:01:46 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuPreferences;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.fudaa.commun.FudaaBrowserControl;
import org.fudaa.fudaa.commun.FudaaTee;
import org.fudaa.fudaa.tr.TrLauncherDefault;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.*;

/**
 * @author Fred Deniger
 * @version $Id: TrBugCommentGenerator.java,v 1.20 2010-01-27 12:00:00 fargeix Exp $
 */
public class TrBugCommentGenerator {
  private String defaultText = buildDefaultText();

  private String buildDefaultText() {
    return TrResource.getS("(Veuillez ajouter votre commentaire ici)") +
        "\n\n\n-----------------------------------------------------------------------" +
        "\n*General info*\n" +
        "OS: " + System.getProperty("os.name") + CtuluLibString.ESPACE +
        System.getProperty("os.arch") + System.getProperty("os.version") +
        "\nJava: " + System.getProperty("java.vendor") + CtuluLibString.ESPACE +
        System.getProperty("java.version") +
        "\nPrepro: " + TrLauncherDefault.getInfosSoftware().name + CtuluLibString.ESPACE +
        TrLauncherDefault.getInfosSoftware().version;
  }

  private void activeFrame() {
    FudaaBrowserControl.displayURL("https://fudaa-project.atlassian.net/projects/PREPRO/issues");
  }

  /**
   * Copie les infos du log dans le presse-papiers.
   * Copies the infos from the log in the clipboard
   */
  private void copyLog() {
    Toolkit toolKit = Toolkit.getDefaultToolkit();
    Clipboard cb = toolKit.getSystemClipboard();
    cb.setContents(new StringSelection(defaultText), null);
  }

  /**
   * ajoute le fichier ts.log aux commentaires.
   * adds the list ts.log to the comments
   */
  private void addTsLog() {
    if (FudaaTee.isCreated()) {
      final File ts = FudaaTee.getFudaaTee().getFile();
      if (ts.exists()) {
        try (BufferedReader reader = new LineNumberReader(new FileReader(ts))) {
          final StringBuilder buffer = new StringBuilder(defaultText);
          buffer.append("\n\n*File log*:");
          String lu;
          while ((lu = reader.readLine()) != null) {
            buffer.append(CtuluLibString.LINE_SEP).append(lu);
          }
          defaultText = buffer.toString();
        } catch (final IOException e) {
          FuLog.warning(e);
        }
      }
    }
  }

  /**
   * Active la saisie d'un commentaire.
   * Activates the capture of a comment
   */
  static void sendAComment() {
    final TrBugCommentGenerator gen = new TrBugCommentGenerator();
    gen.activeFrame();
  }

  /**
   * Active la saisie d'un commentaire.
   * Activates the capture of a comment
   */
  public static void sendACommentWithBugTitle() {
    final TrBugCommentGenerator gen = new TrBugCommentGenerator();
    gen.copyLog();
    gen.activeFrame();
  }

  /**
   * Test.
   *
   * @param args non utilise
   */
  public static void main(final String[] args) {
    BuPreferences.BU.applyLanguage(TrLauncherDefault.getInfosSoftware().languages);
    FudaaTee.createFudaaTee();
    final TrBugCommentGenerator gen = new TrBugCommentGenerator();
    gen.addTsLog();
    gen.activeFrame();
  }
}
