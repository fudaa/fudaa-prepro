/**
 * @creation 26 mars 2004
 * @modification $Date: 2007-05-04 14:01:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;
import javax.swing.ListModel;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfFilterTime;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.find.EbliFindExpressionComposite;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.layer.MvIsoModelInterface;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;
import org.fudaa.fudaa.meshviewer.model.MvExpressionSupplierElement;
import org.fudaa.fudaa.meshviewer.model.MvExpressionSupplierNode;
import org.fudaa.fudaa.meshviewer.model.MvInfoDelegate;
import org.fudaa.fudaa.meshviewer.model.MvNodeModel;

/**
 * @author Fred Deniger
 * @version $Id: TrIsoModelAbstract.java,v 1.24 2007-05-04 14:01:51 deniger Exp $
 */
public abstract class TrIsoModelAbstract extends MvElementModelDefault implements MvNodeModel, MvIsoModelInterface {

    TrPostSource s_;
    ListModel varModel_;
    EfData oldData_;
    H2dVariableType var_;
    TrPostFilterLayer cond_;

    public TrPostFilterLayer getCond() {
        if (cond_ == null) {
            cond_ = new TrPostFilterLayer(s_);
        }
        return cond_;
    }

    public void saveFilter(final EbliUIProperties _res) {
        if (cond_ != null) {
            _res.put("iso.filter", cond_.createSaver());
        }
    }

    public void restoreFilter(final EbliUIProperties _res) {
        if (_res != null) {
            final Object o = _res.get("iso.filter");
            if (o != null) {
                getCond().restoreFromSaver(o, s_);
            }
        }
    }

  @Override
    public boolean getDataRange(final CtuluRange _b) {
        if (var_ == null) {
            return false;
        }
        getPaletteExtremaForSelectedValue(_b);
        return true;
    }

    public int getVarIdx(final String _varIdx) {
        return getVarIdx(_varIdx, varModel_);
    }

    public static int getVarIdx(final String _varIdx, final ListModel _model) {
        if (_model != null) {
            for (int i = _model.getSize() - 1; i >= 0; i--) {
                final Object oi = _model.getElementAt(i);
                H2dVariableType var = null;
                if (oi instanceof H2dVariableType) {
                    var = (H2dVariableType) oi;
                } else if (oi instanceof TrPostFlecheContent) {
                    var = ((TrPostFlecheContent) oi).getVar();
                }
                if (var != null && var.getID().equals(_varIdx)) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * @param _s la source
     * @param _varModel le modele contenant les variables
     * @param _d le delegue pour les infos
     */
    public TrIsoModelAbstract(final TrPostSource _s, final ListModel _varModel, final MvInfoDelegate _d) {
        super(_s.getGrid(), _d);
        s_ = _s;
        varModel_ = _varModel;
    }

    public EfFilterTime getFilterForCurrentVar() {
        if (cond_ != null && cond_.isVarModifiedByFilter(getVariableSelected())) {
            return cond_;
        }
        return null;
    }

    public CtuluRange getPaletteExtremaForSelectedValue(final CtuluRange _r) {
        CtuluRange r = _r;
        if (r == null) {
            r = new CtuluRange();
        }
        // le filtre a appliquer reellement
        final EfFilterTime cond = getFilterForCurrentVar();
        r = EfLib.getMinMax(oldData_, r, cond);
        return r;
    }

    /**
     * @return la valeur max pour la valeur selectionnee
     */
    public CtuluRange getPaletteExtremaForSelectedValue() {
        return getPaletteExtremaForSelectedValue(null);
    }

    public double updateMaxValue(final double _init) {
        if ((var_ != null) && ("froude".equals(var_.getShortName())) && _init < 1) {
            return 1;
        }
        return _init;

    }

    public double updateMinValue(final double _init) {
        if ((var_ != null) && (s_.isDefaultPaletteMinValueDefined(var_))) {
            final double min = s_.getDefaultPaletteMinValueFor(var_);
            if (_init < min) {
                return min;
            }
        }
        return _init;

    }

    /**
     * @param _t la variable a chercher
     * @return l'indice de la variable
     */
    public int getIndexOf(final H2dVariableType _t) {
        if (varModel_ == null) {
            return -1;
        }
        for (int i = varModel_.getSize() - 1; i >= 0; i--) {
            if (varModel_.getElementAt(i) == _t) {
                return i;
            }
        }
        return -1;
    }


    /**
     * @return true si la variable courante est de type par element
     */
    public final boolean isCurrentDataElementType() {
        return oldData_ == null ? false : oldData_.isElementData();
    }

    public abstract void updateCurrentValueCache();

    /**
     * Pour la rendre abstraite.
     */
    @Override
    public abstract BuTable createValuesTable(ZCalqueAffichageDonneesInterface _layer);

    public abstract void setVar(int _idx);

    public abstract void fillInterpolateInfo(InfoData _m, int _element, double _x, double _y, String _layerTitle);

    /**
     * @param _idxElement l'indice de l'element
     * @param _l la liste a remplir
     * @return true
     */
  @Override
    public final double[] fillWithData(final int _idxElement, final double[] _l) {
        return fillWithData(s_.getGrid().getElement(_idxElement), oldData_, _l);
    }

  @Override
    public double getDatatFor(int idxElt, int idxPtOnElt) {
        return oldData_.getValue(s_.getGrid().getElement(idxElt).getPtIndex(idxPtOnElt));
    }


    public static double[] fillWithData(final EfElement _idxElement, final EfData _nodeData, final double[] _l) {
        if (_nodeData == null) {
            return null;
        }
        final EfElement el = _idxElement;
        final int n = el.getPtNb();
        double[] r = _l;
        if ((r == null) || (r.length != n)) {
            r = new double[n];
        }
        for (int i = 0; i < n; i++) {
            r[i] = _nodeData.getValue(el.getPtIndex(i));
        }
        return r;

    }

    public boolean isAccepted(final int _i, final boolean _force) {
        return (_force || cond_ == null || !cond_.isVarModifiedByFilter(var_) || cond_.isActivatedElt(_i));
    }

  @Override
    public boolean isPainted(int idxElt) {
        return isAccepted(idxElt, false);
    }


    @Override
    public boolean getCentre(GrPoint pt, int i, boolean force) {
        if (isAccepted(i, force)) {
            return cente(g_, pt, i);
        }
        return false;
    }

    public final String getVariableNameSelected() {
        if (var_ == null) {
            return CtuluLibString.EMPTY_STRING;
        }
        return var_.getName();
    }

    public final H2dVariableType getVariableSelected() {
        return var_;
    }

    public abstract double getValueFor(H2dVariableType _var, int _idx);

    /**
     * @return l'identifiant pour la variable courante
     */
    public final String getCompleteVariableNameId() {
        if (var_ == null) {
            return CtuluLibString.EMPTY_STRING;
        }
        return createString(var_, var_.getCommonUnit());
    }

    @Override
    public final EbliFindExpressionContainerInterface getExpressionContainer() {
        return new EbliFindExpressionComposite(
               oldData_!=null && oldData_.isElementData()? (EbliFindExpressionContainerInterface) new MvExpressionSupplierElement(this)
                : new MvExpressionSupplierNode(this), new TrPostExprDataSupplier(this));
    }

    /**
     * @param _t la variable
     * @param _unit l'unite
     * @return la chaine _t.getNom() (unit)
     */
    public final static String createString(final H2dVariableType _t, final Object _unit) {
        if (_t == null) {
            return CtuluLibString.EMPTY_STRING;
        }
        if (_unit != null && _unit.toString().length() > 0) {
            return _t.getName() + " (" + _unit + ")";
        }
        return _t.getName();
    }

    /**
     * @param _t la variable
     * @return la chaine a utiliser pour la palette
     */
    public final String getVarDescriptionForPalette(final H2dVariableType _t) {
        return createString(_t, getCurrentUnitUsedFor(_t));
    }

    public final String getVarDescriptionForPalette() {
        return getVarDescriptionForPalette(var_);
    }

    public final String getCurrentUnitUsedFor(final H2dVariableType _t) {
        if (_t != null && (_t == H2dVariableType.TEMPS || (_t.getParentVariable() == H2dVariableType.TEMPS))) {
            final CtuluNumberFormatI f = s_.getTimeFormatter();
            if (f == null) {
                return H2dVariableType.TEMPS.getCommonUnitString();
            }
            return null;
        }
        return _t == null ? null : _t.getCommonUnitString();
    }

    /**
     * @return le nombre d'element
     */
    public final int getEltNb() {
        return s_.getGrid().getEltNb();
    }

    @Override
    public final EfGridInterface getGrid() {
        return s_.getGrid();
    }
    
    
    public final TrPostSource getSource(){
      return s_;
    }

    @Override
    public final int getNombre() {
        return isCurrentDataElementType() ? getEltNb() : getNbPoint();
    }

    public final double getValue(final int _idx) {
        return oldData_ == null ? 0 : oldData_.getValue(_idx);
    }

    public final String getV(final int _i) {
        return s_.getVariable(_i).getName();
    }

    public final ListModel getVariableList() {
        return varModel_;
    }

    /**
     * @return le nombre de variables utilisee
     */
    public final int getVariableNb() {
        return varModel_.getSize();
    }

  @Override
    public final double getX(final int _i) {
        return s_.getGrid().getPtX(_i);
    }

  @Override
    public final double getY(final int _i) {
        return s_.getGrid().getPtY(_i);
    }

  @Override
    public final double getZ(final int _i) {
        return s_.getGrid().getPtZ(_i);
    }

    public boolean isValuesTableAvailableFromModel() {
        return true;
    }

  @Override
    public final boolean point(final GrPoint _p, final int _i, final boolean _force) {
        final EfGridInterface g = s_.getGrid();
        _p.setCoordonnees(g.getPtX(_i), g.getPtY(_i), g.getPtZ(_i));
        return true;
    }

    /**
     * Ne pas utiliser.
     */
    @Override
    public final void setGrid(final EfGrid _g) {
        FuLog.warning(new Throwable());
        // super.setGrid(_g);
    }
}
