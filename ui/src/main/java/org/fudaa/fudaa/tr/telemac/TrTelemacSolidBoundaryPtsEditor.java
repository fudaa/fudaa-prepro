/**
 * @creation 3 nov. 2003
 * @modification $Date: 2007-05-22 14:20:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcManager;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBordParFrontiere;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBoundary;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBoundaryCondition;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrTelemacSolidBoundaryPtsEditor.java,v 1.7 2007-05-22 14:20:38 deniger Exp $
 */
public class TrTelemacSolidBoundaryPtsEditor extends CtuluDialogPanel {

  CtuluCommandContainer cmdMng_;
  BuComboBox vitesses_;
  TIntIntHashMap idxVit_;
  int[] idx_;

  H2dTelemacBordParFrontiere frontier_;
  H2dTelemacBcManager mng_;
  final BuComboBox cb_;
  final int initIdx_;

  public TrTelemacSolidBoundaryPtsEditor(final CtuluCommandContainer _cmdMng, final H2dTelemacBordParFrontiere _frIdx,
      final H2dTelemacBoundary _b, final int[] _idxPt, final H2dTelemacBcManager _mng) {
    mng_ = _mng;
    cmdMng_ = _cmdMng;
    frontier_ = _frIdx;
    this.idx_ = _idxPt;
    setLayout(new BuGridLayout(2, 10, 10));
    addEmptyBorder(10);
    if (_b.getName() != null) {
      addLabel(TrResource.getS("Bord"));
      addStringText(_b.getName()).setEditable(false);
    }
    addLabel(H2dResource.getS("Vitesses"));
    final int i = _frIdx.isSolidTypeConstantOnPoint(_idxPt);
    final String[] names = H2dTelemacBoundaryCondition.getSolidUVDesc();
    if (H2dTelemacBoundaryCondition.isSolidIdxMixte(i)) {
      final String[] namesWithMixte = new String[names.length + 1];
      namesWithMixte[namesWithMixte.length - 1] = H2dResource.getS("Mixte");
      System.arraycopy(names, 0, namesWithMixte, 0, names.length);
      cb_ = new BuComboBox(namesWithMixte);
      cb_.setSelectedIndex(names.length);
    } else {
      cb_ = new BuComboBox(names);
      cb_.setSelectedIndex(i);
    }
    add(cb_);
    initIdx_ = cb_.getSelectedIndex();

  }

  @Override
  public boolean apply() {
    final int newUv = cb_.getSelectedIndex();
    if (initIdx_ != newUv && !H2dTelemacBoundaryCondition.isSolidIdxMixte(newUv)) {
      frontier_.setVitesseForSelectedPoints(idx_, newUv, cmdMng_);

    }
    return true;
  }
}
