/**
 * @file TrImplHelper.java
 * @creation 29 avr. 2003
 * @modification $Date: 2006-11-16 08:10:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuDynamicMenu;
import com.memoire.bu.BuMenuItem;
import java.io.File;
import java.util.Map;
import java.util.Properties;
import javax.swing.Icon;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * @author deniger
 * @version $Id: TrImplHelperAbstract.java,v 1.19 2006-11-16 08:10:28 deniger Exp $
 */
public abstract class TrImplHelperAbstract {

  /**
   * @author fred deniger
   * @version $Id: TrImplHelperAbstract.java,v 1.19 2006-11-16 08:10:28 deniger Exp $
   */
  static final class OpenOtherMenu extends BuDynamicMenu {
    /**
     * 
     */
    private final TrImplementationEditorAbstract impl_;
    /**
     * 
     */
    private final BuMenuItem[] items_;

    /**
     * @param _s
     * @param _cmd
     * @param _impl
     * @param _items
     */
    OpenOtherMenu(final String _s, final String _cmd, final TrImplementationEditorAbstract _impl,
        final BuMenuItem[] _items) {
      super(_s, _cmd);
      impl_ = _impl;
      items_ = _items;
    }

    @Override
    public void build() {
      if (this.getMenuComponentCount() == 0) {
        // addMenuItem("Projet T�l�mac", PREF_OUVRIR + TrProjet.TELEMAC_ID);
        final int n = items_.length;
        for (int i = 0; i < n; i++) {
          add(items_[i]);
          items_[i].addActionListener(impl_);
        }
      }
    }

    @Override
    public boolean isActive() {
      return true;
    }
  }

  TrApplicationManager mng_;

  /**
   * @param _mng l'appli manager
   */
  public TrImplHelperAbstract(final TrApplicationManager _mng) {
    mng_ = _mng;
  }

  /**
   * Activation de la chaine.
   * Activation of the chain
   * @param _impl l'impl cible
   */
  public void active(final TrImplementationEditorAbstract _impl) {
    _impl.getFrame().setTitle(TrResource.getS("Editeur"));
    final Icon ic = FudaaResource.FUDAA.getIcon("appli/" + getSoftwareID());
    if (ic != null) {
      _impl.getMainPanel().getDesktop().setLogo(ic);
    }
    // addOuvrirAutre(_impl);
  }

  /**
   * @param _impl l'impl
   * @param _op la barre de progress
   * @param _initGrid le maillage d'origine (peut etre null) / the origin mesh (can be null)
   * @return un nouveau projet / a new project
   */
  public abstract TrProjet creer(TrImplementationEditorAbstract _impl, ProgressionInterface _op, File _initGrid,
      Properties _options);

  /**
   * @return les menus a ajouter a Ouvrir autre / the menus to add to open other
   */
  public abstract BuMenuItem[] getMenuItemsOuvrir(TrImplementationEditorAbstract _impl);

  /**
   * @return l'id de la chaine utilisee / the id from the used chain
   */
  public abstract String getSoftwareID();

  /**
   * Boite de dialogue pour choisir le fichier.
   * Dialog box to choose the file
   * @param _op la barre de progress / the progress bar
   * @return le projet ouvert / the opened project 
   */
  public abstract TrProjet ouvrir(final TrImplementationEditorAbstract _impl, ProgressionInterface _op);

  /**
   * @param _op la barre de progress / the progress bar
   * @param _f le fichier a ouvrir / the file to open
   * @param _params TODO
   * @return le projet ouvert / the open project
   */
  public abstract TrProjet ouvrir(final TrImplementationEditorAbstract _impl, ProgressionInterface _op, File _f,
      Map _params);

  /**
   * Appele pour ferme l'impl helper.
   * Called to close the impl helper
   */
  public void unactive(final TrImplementationEditorAbstract _impl) {
  // removeOuvrirAutre(_impl);
  }

  /**
   * @return le manager d'appli / the manager of application
   */
  public final TrApplicationManager getAppliMng() {
    return mng_;
  }
}