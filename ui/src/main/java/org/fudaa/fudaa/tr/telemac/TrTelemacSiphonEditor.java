/**
 * @creation 1 d�c. 2004
 * @modification $Date: 2007-05-04 14:01:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;
import com.memoire.bu.BuVerticalLayout;
import gnu.trove.TIntDoubleHashMap;
import java.awt.Color;
import javax.swing.BorderFactory;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSourceMng;
import org.fudaa.dodico.h2d.telemac.H2dtelemacSiphon;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacSiphonEditor.java,v 1.7 2007-05-04 14:01:50 deniger Exp $
 */
public class TrTelemacSiphonEditor extends CtuluDialogPanel {

  H2dTelemacSourceMng mng_;
  CtuluCommandContainer cmd_;
  int[] selectedIdx_;
  int[] varIdxGeneral;
  BuTextField[] generalValues_;
  BuTextField[] s1Values_;
  BuTextField[] s2Values_;
  int[] varIdxS1;
  int[] varIdxS2;

  /**
   * @param _mng le manager des sources
   * @param _cmd
   * @param _selectedIdx
   */
  public TrTelemacSiphonEditor(final H2dTelemacSourceMng _mng, final CtuluCommandContainer _cmd,
      final int[] _selectedIdx) {
    super(true);
    setLayout(new BuVerticalLayout(5));
    mng_ = _mng;
    cmd_ = _cmd;
    selectedIdx_ = _selectedIdx;
    final BuValueValidator val = BuValueValidator.MIN(0);
    // on ne gere ce genre de truc
    if (selectedIdx_ == null) { return; }
    // le panneau comportant les donn�es generales
    BuPanel general = new BuPanel();
    general.setLayout(new BuGridLayout(2, 5, 5));
    boolean latotal = true;
    if (selectedIdx_.length > 1) {
      latotal = false;
      addLabel(general, TrResource.getS("Nombre de siphons �dit�s"));
      addLabel(general, CtuluLibString.getString(selectedIdx_.length));
    }
    varIdxGeneral = mng_.getSiphonCommonIndexVar();
    generalValues_ = new BuTextField[varIdxGeneral.length];
    final Double[] commonVal = mng_.getSiphonCommonValuesFor(selectedIdx_, varIdxGeneral);
    for (int i = 0; i < varIdxGeneral.length; i++) {
      addLabel(general, mng_.getSiphonVariableName(varIdxGeneral[i]));
      generalValues_[i] = addDoubleText(general, commonVal[i] == null ? CtuluLibString.EMPTY_STRING : commonVal[i]
          .toString());
      generalValues_[i].setValueValidator(val);
    }
    add(general);
    // on cree des panneaux pour les donnees spec aux sources
    if (latotal) {
      varIdxS1 = mng_.getSiphonSource1IndexVar();
      varIdxS2 = mng_.getSiphonSource2IndexVar();
      s1Values_ = new BuTextField[varIdxS1.length];
      s2Values_ = new BuTextField[varIdxS2.length];
      final H2dtelemacSiphon sip = mng_.getSiphon(selectedIdx_[0]);
      general = new BuPanel();
      general.setLayout(new BuGridLayout(2, 5, 5));
      general.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TrResource.getS(
          "Source {0}", CtuluLibString.UN)));
      addLabel(general, TrResource.getS("Indice global de la source"));
      addLabel(general, CtuluLibString.getString(sip.getI1() + 1));
      for (int i = 0; i < varIdxS1.length; i++) {
        addLabel(general, mng_.getSiphonVariableName(varIdxS1[i]));
        s1Values_[i] = addDoubleText(general, sip.getValue(varIdxS1[i]));
        s1Values_[i].setValueValidator(val);
      }
      add(general);
      general = new BuPanel();
      general.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TrResource.getS(
          "Source {0}", CtuluLibString.DEUX)));
      general.setLayout(new BuGridLayout(2, 5, 5));
      addLabel(general, TrResource.getS("Indice global de la source"));
      addLabel(general, CtuluLibString.getString(sip.getI2() + 1));
      for (int i = 0; i < varIdxS2.length; i++) {
        addLabel(general, mng_.getSiphonVariableName(varIdxS2[i]));
        s2Values_[i] = addDoubleText(general, sip.getValue(varIdxS2[i]));
        s2Values_[i].setValueValidator(val);
      }

      add(general);
    }
  }

  public static boolean valideCulvertEndValues(final BuTextField[] _fs) {
    if (_fs == null) { return true; }
    boolean r = true;
    for (int i = _fs.length - 1; i >= 0; i--) {
      final String s = _fs[i].getText();
      if ((!CtuluLibString.isEmpty(s)) && (Double.parseDouble(s) < 0)) {
        _fs[i].setForeground(Color.RED);
        r = false;
      } else {
        _fs[i].setForeground(Color.BLACK);
      }
    }
    return r;

  }

  private boolean valide(final BuTextField[] _fs) {
    return valideCulvertEndValues(_fs);
  }

  /**
   * @param _fs les textes comportants les valeurs
   * @return tableau de double contenant les valeurs. une ligne est null si la saisie est vide
   */
  public static Double[] getValues(final BuTextField[] _fs) {
    if (_fs == null) { return null; }
    final Double[] r = new Double[_fs.length];
    for (int i = _fs.length - 1; i >= 0; i--) {
      final String s = _fs[i].getText();
      if (!CtuluLibString.isEmpty(s)) {
        r[i] = new Double(s);
      }
    }
    return r;
  }

  @Override
  public boolean apply() {
    final TIntDoubleHashMap map = new TIntDoubleHashMap();
    Double[] d = getValues(generalValues_);
    if (d != null) {
      for (int i = d.length - 1; i >= 0; i--) {
        if (d[i] != null) {
          map.put(varIdxGeneral[i], d[i].doubleValue());
        }
      }
    }
    d = getValues(s1Values_);
    if (d != null) {
      for (int i = d.length - 1; i >= 0; i--) {
        if (d[i] != null) {
          map.put(varIdxS1[i], d[i].doubleValue());
        }
      }
    }
    d = getValues(s2Values_);
    if (d != null) {
      for (int i = d.length - 1; i >= 0; i--) {
        if (d[i] != null) {
          map.put(varIdxS2[i], d[i].doubleValue());
        }
      }
    }
    mng_.modifySiphon(selectedIdx_, map.keys(), map.getValues(), cmd_);
    return true;
  }

  @Override
  public boolean isDataValid() {
    boolean r = valide(generalValues_);
    if (!valide(s1Values_)) {
      r = false;
    }
    if (!valide(s2Values_)) {
      r = false;
    }
    if (!r) {
      setErrorText(TrResource.getS("Des valeurs sont erron�es"));
    }

    return r;
  }
}
