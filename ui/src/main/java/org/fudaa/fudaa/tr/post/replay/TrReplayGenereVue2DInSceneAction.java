/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.replay;

import com.memoire.bu.BuResource;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.calque.CalqueLegendeWidgetAdapter;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostLayoutFille;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;

/**
 *
 * @author Frederic Deniger
 */
@SuppressWarnings(value = "serial")
public final class TrReplayGenereVue2DInSceneAction extends EbliActionSimple {

  private final TrReplayVisuCustomizer customizer;
  private final TrPostCommonImplementation implementation;
  private final TrPostSource source;
  private final String vue2dTitle;

  public TrReplayGenereVue2DInSceneAction(final TrPostCommonImplementation implementation, final TrPostSource source, final TrReplayVisuCustomizer customizer, final String vue2dTitle) {
    super(EbliResource.EBLI.getString("G�n�rer la frame dans le layout"), BuResource.BU.getToolIcon("crystal_graphe"), "WIDGETRECALQUE");
    this.implementation = implementation;
    this.customizer = customizer;
    this.vue2dTitle = vue2dTitle;
    this.source = source;
  }

  @Override
  public void actionPerformed(final ActionEvent e) {
    final TrPostLayoutFille fille = implementation.getCurrentLayoutFille();
    if (fille == null) {
      implementation.error(TrResource.getS("Il n'existe pas de layout"));
      return;
    }
    CalqueLegendeWidgetAdapter legende = new CalqueLegendeWidgetAdapter(fille.getScene());
    final TrPostVisuPanel vue2d = TrReplayVue2dBuilder.constructVue2d(implementation, source, customizer, legende);
    fille.addCalque(TrResource.getS("Origine courbe: {0}", vue2dTitle), null, new Dimension(300, 300), vue2d, legende);
    setEnabled(false);
  }
}
