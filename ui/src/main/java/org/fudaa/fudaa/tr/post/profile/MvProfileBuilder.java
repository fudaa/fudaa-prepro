/*
 * @creation 21 nov. 06
 * @modification $Date: 2007-06-13 12:58:10 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import com.memoire.bu.BuLib;
import org.locationtech.jts.geom.LineString;
import java.awt.Color;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ListModel;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsCorrectionTester;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsBuilder;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsMng;
import org.fudaa.dodico.ef.operation.EfLineIntersectorActivity;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.export.MvExportChooseVarAndTime;

/**
 * @author fred deniger
 * @version $Id: MvProfileBuilder.java,v 1.14 2007-06-13 12:58:10 deniger Exp $
 */
public abstract class MvProfileBuilder implements CtuluActivity {
  public static int idx;

  public static String getProfileName(final String _name) {
    return MvResource.getS("Profils spatiaux")
        + (_name == null ? CtuluLibString.EMPTY_STRING : (CtuluLibString.ESPACE + _name));
  }

  final EfLineIntersectorActivity act_;
  final MvProfileTarget data_;
  // BuDesktop dest_;
  public final ListModel initVar_;
  // GrBoite initZoom_;
  // boolean isZoomChanged_;
  EfLineIntersectionsResultsMng meshesResults_;
  public String name_;
  EfLineIntersectionsResultsMng nodesResults_;
  public LineString selectedLine_;
  final FudaaCourbeTimeListModel timeModel_;
  final EfLineIntersectionsCorrectionTester tester_;
  // ZCalqueLigneBrisee tmp_;
  public final CtuluUI ui_;
  /**
   * La ligne originelle qui a servie a creer lea courbe
   */
  private final LineString initLine_;

  public MvProfileBuilder(final MvProfileTarget _data, final CtuluUI _ui,
                          final EfLineIntersectionsCorrectionTester _tester, final LineString _selected) {
    super();
    data_ = _data;
    tester_ = _tester;

    initVar_ = CtuluLibSwing.createListModel(data_.getVars());
    act_ = new EfLineIntersectorActivity(data_.getInterpolator());
    ui_ = _ui;
    timeModel_ = _data.getTimeModel();
    initLine_ = _selected;
  }

  MvExportChooseVarAndTime createVarTimeChooser(final String _help) {
    final MvExportChooseVarAndTime res = new MvExportChooseVarAndTime(initVar_, timeModel_, _help);
    if (timeModel_ != null) {
      final int i = timeModel_.getSize() - 1;
      res.getTimeSelectionModel().setSelectionInterval(i, i);
    }
    return res;
  }

  public MvProfileFillePanel actBuildGroup(final ProgressionInterface _prog, final MvExportChooseVarAndTime _selected) {
    return actBuildGroup(_prog, _selected, true, null, -1);
  }

  /**
   * methode qui construit le nouveau graphe de profil spatial.
   *
   * @param _prog
   * @param _selected
   */
  public MvProfileFillePanel actBuildGroup(final ProgressionInterface _prog, final MvExportChooseVarAndTime _selected,
                                           final boolean buildIHM, final H2dVariableType variable, final int indiceTime) {
    int[] time = _selected.getSelectedTimeStepIdx();
    if (indiceTime != -1) {
      time = new int[1];
      time[0] = indiceTime;
    }
    CtuluVariable[] vs = _selected.getSelectedVar();
    if (variable != null) {
      vs = new CtuluVariable[1];
      vs[0] = variable;
    }

    final int nbVar = vs == null ? 0 : vs.length;
    final boolean isTime = timeModel_ != null;
    if (time == null) {
      time = new int[]{0};
    }
    boolean addXYForNode = false;
    boolean addXYForMesh = false;
    for (int i = initVar_.getSize() - 1; i >= 0; i--) {
      if (data_.getData().isElementVar((CtuluVariable) initVar_.getElementAt(i))) {
        addXYForMesh = true;
      } else {
        addXYForNode = true;
      }
      // les 2 sont a true
      if (addXYForMesh && addXYForNode) {
        break;
      }
    }
    final int nbTIme = time.length;
    final MvProfileTreeModel tree = new MvProfileTreeModel(this.data_, tester_);
//    if (addXYForNode) {
//      tree.setNodesResults(getNodeResultsMng(_prog), getNodeResultBuilder(_prog));
//    }
//    if (addXYForMesh) {
//      tree.setMeshesResults(getMeshResultsMng(_prog), getMeshesResultBuilder(_prog));
//
//    }
    final EGGroup grCoord = new EGGroup();
    final String s = EbliLib.getS("Coordonn�es");
    grCoord.setTitle(s);
    final EGAxeVertical yCoordonnees = new EGAxeVertical();
    yCoordonnees.setTitre(s);
    yCoordonnees.setUnite("m");
    grCoord.setAxeY(yCoordonnees);
    final String sep = " - ";
    if (addXYForNode) {
      final EGCourbeChild courbeChild = new MVProfileCourbeCoordinate(grCoord, new MvProfileCoordinatesModel(data_.getData(),
          initLine_, getNodeResultsMng(_prog).getDefaultRes(), true));
      final EGCourbeChild courbeChild2 = new MVProfileCourbeCoordinate(grCoord, new MvProfileCoordinatesModel(data_.getData(),
          initLine_, getNodeResultsMng(_prog).getDefaultRes(), false));
      if (addXYForNode && addXYForMesh) {
        courbeChild.setTitle(courbeChild.getTitle() + sep + MvResource.getS("Noeuds"));
        courbeChild2.setTitle(courbeChild2.getTitle() + sep + MvResource.getS("Noeuds"));
      }

      grCoord.addEGComponent(courbeChild);

      grCoord.addEGComponent(courbeChild2);
    }
    if (addXYForMesh) {
      final EGCourbeChild courbeChild = new MVProfileCourbeCoordinate(grCoord, new MvProfileCoordinatesModel(data_.getData(),
          initLine_, getMeshResultsMng(_prog).getDefaultRes(), true));
      final EGCourbeChild courbeChild2 = new MVProfileCourbeCoordinate(grCoord, new MvProfileCoordinatesModel(data_.getData(),
          initLine_, getMeshResultsMng(_prog).getDefaultRes(), false));
      if (addXYForNode && addXYForMesh) {
        courbeChild.setTitle(courbeChild.getTitle() + sep + MvResource.getS("El�ments"));
        courbeChild2.setTitle(courbeChild2.getTitle() + sep + MvResource.getS("El�ments"));
      }
      grCoord.addEGComponent(courbeChild);

      grCoord.addEGComponent(courbeChild2);
    }
    grCoord.setVisible(false);
    tree.add(grCoord);
    // map permettant de stocker la correspondance var -> groupe
    final Map varGroup = new HashMap();
    if (vs != null) {
      for (int i = 0; i < nbVar; i++) {
        // pas null
        final CtuluVariable var = vs[i];
        MvProfileCourbeGroup gri = null;
        // dans ce cas, on va g�rer les variables parents: inutile de creer des groupe different
        // pour des variables de meme type
        Color c = null;
        boolean mustAddGr = true;
        if (var instanceof H2dVariableType) {
          H2dVariableType parent = ((H2dVariableType) var).getParentVariable();
          if (parent == null) {
            parent = (H2dVariableType) var;
          }
          c = MvProfileFillePanel.getColorFor(i, parent);
          gri = (MvProfileCourbeGroup) varGroup.get(parent);
          if (gri == null) {
            gri = MvProfileFillePanel.createGroupFor(parent);
            varGroup.put(parent, gri);
          } else {
            mustAddGr = false;
          }
        }
        if (gri == null) {
          gri = MvProfileFillePanel.createGroupFor(var);
        }
        if (c == null) {
          c = MvProfileFillePanel.getColorFor(i, var);
        }
        // on doit construire les courbes
        for (int t = 0; t < nbTIme; t++) {

          EfLineIntersectionsResultsBuilder builder = getResBuilder(var, _prog);
          if (selectedLine_ != null) {
            builder.setInitLine(selectedLine_);
          } else {
            builder.setInitLine(initLine_);
          }
          final MVProfileCourbeModel model = new MVProfileCourbeModel(var, data_.getData(), time[t], builder, _prog);
          EGCourbeChild child;
          if (isTime) {
            String title = var.toString() + CtuluLibString.ESPACE;
            if (var.getCommonUnit() != null) {
              title += "(" + var.getCommonUnit() + ")" + CtuluLibString.ESPACE;
            }
            title += timeModel_.getElementAt(time[t]);
            // -- on ajoute les extremites du segment choisi --//
            if (selectedLine_ != null) {
              title += ", P1(" + format(this.selectedLine_.getCoordinateN(0).x, 2) + ";"
                  + format(this.selectedLine_.getCoordinateN(0).y, 2) + ")" + sep + "P"
                  + this.selectedLine_.getNumPoints() + "("
                  + format(this.selectedLine_.getCoordinateN(this.selectedLine_.getNumPoints() - 1).x, 2) + ";"
                  + format(this.selectedLine_.getCoordinateN(this.selectedLine_.getNumPoints() - 1).y, 2) + ")";
            }

            model.setTitle(title);
            child = new MvProfileCourbeTime(gri, model, timeModel_);
          } else {
            child = new MvProfileCourbe(gri, model);
          }

          gri.addEGComponent(child);
        }
        if (mustAddGr) {
          tree.add(gri);
        }
      }
    }
    MvProfileFillePanel panel = new MvProfileFillePanel(tree, ui_);
    if (buildIHM) {
      data_.profilPanelCreated(panel, _prog, name_);
    }

    return panel;
  }

  public String format(double value, int nbChiffresSignificatifs) {
    String forme = "#.";
    for (int i = 0; i < nbChiffresSignificatifs; i++) {
      forme += "0";
    }
    DecimalFormat df = new DecimalFormat(forme);
    return df.format(value);
  }

  /**
   * Methode qui permet d ajouter une courbe spatiale poru un grapeh deja existant.
   *
   * @param _prog
   * @param _selected
   * @author Adrien Hadoux
   */
  public MvProfileFillePanel actAddGroupForExistingModel(final ProgressionInterface _prog,
                                                         final MvExportChooseVarAndTime _selected) {
    int[] time = _selected.getSelectedTimeStepIdx();
    final CtuluVariable[] vs = _selected.getSelectedVar();

    final int nbVar = vs == null ? 0 : vs.length;
    final boolean isTime = timeModel_ != null;
    if (time == null) {
      time = new int[]{0};
    }
    boolean addXYForNode = false;
    boolean addXYForMesh = false;
    for (int i = initVar_.getSize() - 1; i >= 0; i--) {
      if (data_.getData().isElementVar((CtuluVariable) initVar_.getElementAt(i))) {
        addXYForMesh = true;
      } else {
        addXYForNode = true;
      }
      // les 2 sont a true
      if (addXYForMesh && addXYForNode) {
        break;
      }
    }
    final int nbTIme = time.length;
    final MvProfileTreeModel tree = new MvProfileTreeModel(this.data_, tester_);
//    if (addXYForNode) {
//      tree.setNodesResults(getNodeResultsMng(_prog), getNodeResultBuilder(_prog));
//    }
//    if (addXYForMesh) {
//      tree.setMeshesResults(getMeshResultsMng(_prog), getMeshesResultBuilder(_prog));
//
//    }
    final EGGroup grCoord = new EGGroup();
    final String s = EbliLib.getS("Coordonn�es");
    grCoord.setTitle(s);
    final EGAxeVertical yCoordonnees = new EGAxeVertical();
    yCoordonnees.setTitre(s);
    yCoordonnees.setUnite("m");
    grCoord.setAxeY(yCoordonnees);
    final String sep = " - ";
    if (addXYForNode) {
      final EGCourbeChild courbeChild = new MVProfileCourbeCoordinate(grCoord, new MvProfileCoordinatesModel(data_.getData(),
          initLine_, getNodeResultsMng(_prog).getDefaultRes(), true));
      final EGCourbeChild courbeChild2 = new MVProfileCourbeCoordinate(grCoord, new MvProfileCoordinatesModel(data_.getData(),
          initLine_, getNodeResultsMng(_prog).getDefaultRes(), false));
      if (addXYForNode && addXYForMesh) {
        courbeChild.setTitle(courbeChild.getTitle() + sep + MvResource.getS("Noeuds"));
        courbeChild2.setTitle(courbeChild2.getTitle() + sep + MvResource.getS("Noeuds"));
      }

      grCoord.addEGComponent(courbeChild);

      grCoord.addEGComponent(courbeChild2);
    }
    if (addXYForMesh) {
      final EGCourbeChild courbeChild = new MVProfileCourbeCoordinate(grCoord, new MvProfileCoordinatesModel(data_.getData(),
          initLine_, getMeshResultsMng(_prog).getDefaultRes(), true));
      final EGCourbeChild courbeChild2 = new MVProfileCourbeCoordinate(grCoord, new MvProfileCoordinatesModel(data_.getData(),
          initLine_, getMeshResultsMng(_prog).getDefaultRes(), false));
      if (addXYForNode && addXYForMesh) {
        courbeChild.setTitle(courbeChild.getTitle() + sep + MvResource.getS("Elements"));
        courbeChild2.setTitle(courbeChild2.getTitle() + sep + MvResource.getS("Elements"));
      }

      grCoord.addEGComponent(courbeChild);

      grCoord.addEGComponent(courbeChild2);
    }
    grCoord.setVisible(false);
    tree.add(grCoord);
    // map permettant de stocker la correspondance var -> groupe
    final Map varGroup = new HashMap();
    if (vs != null) {
      for (int i = 0; i < nbVar; i++) {
        // pas null
        final CtuluVariable var = vs[i];
        MvProfileCourbeGroup gri = null;
        // dans ce cas, on va g�rer les variables parents: inutile de creer des groupe different
        // pour des variables de meme type
        Color c = null;
        boolean mustAddGr = true;
        if (var instanceof H2dVariableType) {
          H2dVariableType parent = ((H2dVariableType) var).getParentVariable();
          if (parent == null) {
            parent = (H2dVariableType) var;
          }
          c = MvProfileFillePanel.getColorFor(i, parent);
          gri = (MvProfileCourbeGroup) varGroup.get(parent);
          if (gri == null) {
            gri = MvProfileFillePanel.createGroupFor(parent);
            varGroup.put(parent, gri);
          } else {
            mustAddGr = false;
          }
        }
        if (gri == null) {
          gri = MvProfileFillePanel.createGroupFor(var);
        }
        if (c == null) {
          c = MvProfileFillePanel.getColorFor(i, var);
        }
        // on doit construire les courbes
        for (int t = 0; t < nbTIme; t++) {
          final MVProfileCourbeModel model = new MVProfileCourbeModel(var, data_.getData(), time[t], getResBuilder(var,
              _prog), _prog);
          EGCourbeChild child;
          if (isTime) {
            model.setTitle(var.toString() + CtuluLibString.ESPACE + timeModel_.getElementAt(time[t]));
            child = new MvProfileCourbeTime(gri, model, timeModel_);
          } else {
            child = new MvProfileCourbe(gri, model);
          }
          //child.setAspectContour(c);

          gri.addEGComponent(child);
        }
        if (mustAddGr) {
          tree.add(gri);
        }
      }
    }

    return new MvProfileFillePanel(tree, ui_);
  }

  public void close() {
  }

  protected String getHelpForVarTime() {
    return timeModel_ == null ? CtuluLib.getS("S�lectionner les variables") : CtuluLib.getS("S�lectionner les variables et les pas de temps");
  }

  protected EfLineIntersectionsResultsI getRes(final CtuluVariable _o, final int _tidx, final ProgressionInterface _prog) {
    if (data_.getData().isElementVar(_o)) {
      return getMeshResults(_prog, _tidx);
    }
    return getNodeResults(_prog, _tidx);
  }

  protected EfLineIntersectionsResultsBuilder getResBuilder(final CtuluVariable _o, final ProgressionInterface _prog) {
    if (data_.getData().isElementVar(_o)) {
      return getMeshesResultBuilder(_prog);
    }
    return getNodeResultBuilder(_prog);
  }

  public EfLineIntersectionsResultsI getDefaultRes(final CtuluVariable _o, final ProgressionInterface _prog) {
    if (data_.getData().isElementVar(_o)) {
      return getMeshResultsMng(_prog).getDefaultRes();
    }
    return getNodeResultsMng(_prog).getDefaultRes();
  }

  public void showNoIntersectionFound() {
    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {
        ui_.error(DodicoLib.getS("Aucune intersection trouv�e"));
      }
    });
  }

  protected abstract void stepOne();

  public EfLineIntersectionsResultsMng getMeshResultsMng(final ProgressionInterface _prog) {
    if (meshesResults_ == null) {
      meshesResults_ = act_.computeFor(selectedLine_, _prog, true);
    }
    return meshesResults_;
  }

  EfLineIntersectionsResultsBuilder meshesResultBuilder_;
  EfLineIntersectionsResultsBuilder nodesResultBuilder_;

  public EfLineIntersectionsResultsI getMeshResults(final ProgressionInterface _prog, final int _tidx) {
    getMeshesResultBuilder(_prog);
    return meshesResultBuilder_.createResults(_tidx, _prog);
  }

  public EfLineIntersectionsResultsBuilder getMeshesResultBuilder(final ProgressionInterface _prog) {
    final EfLineIntersectionsResultsMng meshes = getMeshResultsMng(_prog);

    if (meshesResultBuilder_ == null) {
      meshesResultBuilder_ = new EfLineIntersectionsResultsBuilder(initLine_, meshes.getDefaultRes(), tester_);
    }
    return meshesResultBuilder_;
  }

  public EfLineIntersectionsResultsI getNodeResults(final ProgressionInterface _prog, final int _tidx) {
    getNodeResultBuilder(_prog);
    return nodesResultBuilder_.createResults(_tidx, _prog);
  }

  public EfLineIntersectionsResultsBuilder getNodeResultBuilder(final ProgressionInterface _prog) {
    final EfLineIntersectionsResultsMng nodes = getNodeResultsMng(_prog);
    if (nodesResultBuilder_ == null) {
      nodesResultBuilder_ = new EfLineIntersectionsResultsBuilder(initLine_, nodes.getDefaultRes(), tester_);
    }
    return nodesResultBuilder_;
  }

  public EfLineIntersectionsResultsMng getNodeResultsMng(final ProgressionInterface _prog) {
    if (nodesResults_ == null) {
      nodesResults_ = act_.computeFor(selectedLine_, _prog, false);
    }
    return nodesResults_;
  }

  public ListModel getTimeModel() {
    return timeModel_;
  }

  public void start() {
    if (initVar_.getSize() == 0) {
      ui_.error(CtuluLib.getS("Il n'y a pas de variables � traiter"));
      return;
    }
    stepOne();
  }

  @Override
  public void stop() {
    if (act_ != null) {
      act_.stop();
    }
  }
}
