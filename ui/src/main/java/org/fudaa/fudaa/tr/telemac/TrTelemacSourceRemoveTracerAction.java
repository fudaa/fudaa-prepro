package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuLabel;
import gnu.trove.TIntArrayList;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.telemac.H2dTelemacTracerMng;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.tr.common.TrLib;
import org.jdesktop.swingx.VerticalLayout;

/**
 * @author genesis
 */
public class TrTelemacSourceRemoveTracerAction extends EbliActionSimple {
  private final H2dTelemacTracerMng source;
  private final CtuluCommandManager cmdManager;

  public TrTelemacSourceRemoveTracerAction(H2dTelemacTracerMng source, CtuluCommandManager cmdManager) {
    super(TrLib.getString("Supprimer des traceurs"), null, "REMOVE_TRACER");
    this.source = source;
    this.cmdManager = cmdManager;
  }

  @Override
  public String getEnableCondition() {
    return TrLib.getString("Au moins un traceur doit �tre d�fini");
  }

  @Override
  public void updateStateBeforeShow() {
    setEnabled(source.getNbTraceurs() > 0);
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    String[] names = source.getTracerNames();
    JCheckBox[] cbs = new JCheckBox[names.length];
    CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BorderLayout());
    JPanel centerPanel = new JPanel(new VerticalLayout());
    if (names.length > 6) {
      pn.add(new JScrollPane(centerPanel));
    } else {
      pn.add(centerPanel);
    }
    centerPanel.setLayout(new VerticalLayout());
    for (int i = 0; i < names.length; i++) {
      cbs[i] = new JCheckBox(names[i]);
      centerPanel.add(cbs[i]);
    }
    String string = TrLib.getString("Choisir les traceurs � supprimer");
    pn.setHelpText(string);
    pn.add(new BuLabel(string), BorderLayout.NORTH);
    if (pn.afficheModaleOk(null, getTitle())) {
      TIntArrayList idxToDelete = new TIntArrayList();
      for (int i = 0; i < cbs.length; i++) {
        if (cbs[i].isSelected()) {
          idxToDelete.add(i);
        }
      }
      if (!idxToDelete.isEmpty()) {
        int[] idx = idxToDelete.toNativeArray();
        source.removeTracers(idx, cmdManager);
      }

    }

  }
}
