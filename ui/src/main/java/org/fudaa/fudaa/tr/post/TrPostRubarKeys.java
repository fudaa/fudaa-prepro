package org.fudaa.fudaa.tr.post;

import org.fudaa.fudaa.tr.post.data.TrPostKey;

public class TrPostRubarKeys {
  public static final String TPS = "TPS";
  public static final String TPC = "TPC";
  public static final String ZFN = "ZFN";
  public static final String DZF = "DZF";
  public static final String SED = "SED";

  public static TrPostKey createTPCKey(int varIndex, int fileIndex) {
    return new TrPostKey(TPC, varIndex, fileIndex);
  }

  public static TrPostKey createTPSKey(int varIndex) {
    return new TrPostKey(TPS, varIndex);
  }

  public static TrPostKey createSEDKey(int varIndex) {
    return new TrPostKey(SED, varIndex);
  }

  public static TrPostKey createDZFKey(int varIndex) {
    return new TrPostKey(DZF, varIndex);
  }

  public static TrPostKey createZFNKey(int varIndex) {
    return new TrPostKey(ZFN, varIndex);
  }
}
