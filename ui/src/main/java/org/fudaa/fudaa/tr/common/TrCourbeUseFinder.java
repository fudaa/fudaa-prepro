/*
 * @creation 14 mars 07
 * @modification $Date: 2007-03-19 13:28:03 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * @author fred deniger
 * @version $Id: TrCourbeUseFinder.java,v 1.2 2007-03-19 13:28:03 deniger Exp $
 */
public interface TrCourbeUseFinder extends CtuluActivity {

  TrCourbeUseResultsI findAll(ProgressionInterface _prog);

  TrCourbeUseResultsI findFor(EvolutionReguliereInterface _evol, ProgressionInterface _prog);

  EvolutionReguliereInterface[] getSelectedEvolutions();

  FudaaCommonImplementation getImpl();

}
