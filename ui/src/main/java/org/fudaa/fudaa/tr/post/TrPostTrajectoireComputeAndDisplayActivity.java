package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuGridLayout;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.ef.operation.EfTrajectoireActivity;
import org.fudaa.dodico.ef.operation.EfTrajectoireParameters;
import org.fudaa.dodico.ef.operation.EfTrajectoireResultBuilder;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeDefault;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetVueCalque;
import org.fudaa.fudaa.sig.FSigAttibuteTypeManager;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.profile.MvProfileCoteTester;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Algorithme de Calcul les lignes de courants TODO mettre la classe dans EF il y a la classe qui est charge de faire le
 * calcul qui doit implementer CtulActivity et la classe qui implemente EfLineIntersectionParent.
 *
 * @author Adrien Hadoux
 */
public class TrPostTrajectoireComputeAndDisplayActivity {
  static final CtuluVariable MARK = new CtuluVariableDefault("MARK");
  private static final String TYPE_ID = "data.type";

  private static GISAttributeInterface getAttributeFor(final Map<GISAttributeInterface, CtuluVariable> _var,
                                                       final FSigLayerGroup _group, final CtuluVariable _variable) {
    if (_var.containsValue(_variable)) {
      return null;
    }
    GISAttributeInterface att = _group.getAtt().getAttribute(_variable.getName());
    if (att == null || (att.getDataClass() != Double.class || !att.isAtomicValue())) {
      att = _group.getAtt().addDoubleAttribute(_variable.getName(), true);
      if (att == null) {
        att = _group.getAtt().addDoubleAttribute("T-" + _variable.getName(), true);
      }
      if (att == null) {
        FuLog.error(new Throwable());
      }
    }
    _var.put(att, _variable);
    return att;
  }

  private static GISAttributeInterface getBooleanAttributeFor(final Map<GISAttributeInterface, CtuluVariable> _var,
                                                              final FSigLayerGroup _group, final CtuluVariable _variable) {
    if (_var.containsValue(_var)) {
      return null;
    }
    GISAttributeInterface att = _group.getAtt().getAttribute(_variable.getName());
    if (att == null || (att.getDataClass() != Boolean.class || !att.isAtomicValue())) {
      att = _group.getAtt().addBooleanAttribute(_variable.getName(), true);
      if (att == null) {
        att = _group.getAtt().addBooleanAttribute("T-" + _variable.getName(), true);
      }
    }
    _var.put(att, _variable);
    return att;
  }

  public static String getCalqueId(final boolean _trajectoire) {
    return _trajectoire ? "trajectoire.type" : "ligne.type";
  }

  public static String getCalqueName(final boolean _trajectoire) {
    return TrLib.getString(_trajectoire ? "Trajectoires" : "Lignes de courant");
  }

  static int idxTraj;
  static int idxLigne;

  public static String createNewCalqueName(final boolean _trajectoire) {
    int suffixe = _trajectoire ? (++idxTraj) : (++idxLigne);
    return getCalqueName(_trajectoire) + " " + Integer.toString(suffixe);
  }

  EfTrajectoireActivity activity_;
  FSigLayerGroup ligneDeCourant_;
  final TrPostVisuPanel pn_;
  boolean stop_;
  FSigLayerGroup traj_;
  TrPostTrajectoireLineLayer calqueAecraser_;

  public TrPostTrajectoireComputeAndDisplayActivity(final TrPostVisuPanel calque, TrPostTrajectoireLineLayer c) {
    this.pn_ = calque;
    calqueAecraser_ = c;
  }

  public TrPostTrajectoireComputeAndDisplayActivity(final TrPostVisuPanel calque) {
    this(calque, null);
  }

  public void reloadLinesIn(TrPostTrajectoireLineLayer calque, EfTrajectoireParameters data,
                            List<EfTrajectoireResultBuilder> result, CtuluAnalyze log) {
    final FSigLayerGroup gr = pn_.getGroupGIS();
    final Map<GISAttributeInterface, CtuluVariable> varAtttributes = new HashMap<GISAttributeInterface, CtuluVariable>(
        20);
    final List<GISAttributeInterface> attDansOrdreIns = fillCorrespondantAttributeVariable(data, gr, varAtttributes);
    GISDataModel model = TrPostTrajectoireGisDataModel.build(result, attDansOrdreIns, varAtttributes, log);
    if (model == null || log.containsFatalError()) {
      return;
    }
    final GISZoneCollectionLigneBrisee lb = new GISZoneCollectionLigneBrisee();
    lb.setAttributes(attDansOrdreIns.toArray(new GISAttributeInterface[attDansOrdreIns.size()]), null);
    lb.addAll(model, null, false);
    calque.modele(new ZModeleLigneBriseeDefault(lb));
    calque.dataMemory_ = data;
    calque.markAttr_ = findAttributeForVariable(varAtttributes, MARK);
    calque.updateVariablesList();
    calque.repaint();
    calque.firePropertyChange(EbliWidgetVueCalque.CLEAR_CACHE, Boolean.FALSE, Boolean.TRUE);
  }

  /**
   * Methode appel�e par l'interface de calcul des lignes de courant.
   *
   * @param init point de depart
   * @param dureeIntegration duree totale de l'integration en secondes
   * @param indicePdt indice du pas de temps
   * @param finesse coefficient de finesse double
   * @param vx la variable a utiliser
   * @param _prog l'interface de progression, peut etre null
   */
  public void computeLigneCourant(final TrPostVisuPanel vue2d, final EfTrajectoireParameters _data,
                                  final CtuluAnalyze _analyse, final ProgressionInterface _prog, final String[] _messages) {
    _messages[0] = CtuluLibString.EMPTY_STRING;
    stop_ = false;
    // on recupere les lignes de courant
    activity_ = new EfTrajectoireActivity(pn_.getSource().getInterpolator(), new MvProfileCoteTester(), pn_.getSource()
        .getTime().getTimesInSec());

    final List<EfTrajectoireResultBuilder> computeLigneCourant = activity_.computeLigneCourant(_data, _prog, _analyse);
    if (FuLog.isDebug()) {
      FuLog.debug("TrTrajectoireComputeAndDisplayActivity activity is finished");
    }
    if (computeLigneCourant == null || computeLigneCourant.isEmpty()) {
      return;
    }
    final FSigLayerGroup gr = pn_.getGroupGIS();
    final Map<GISAttributeInterface, CtuluVariable> varAtttributes = new HashMap<GISAttributeInterface, CtuluVariable>(
        20);
    final List<GISAttributeInterface> attDansOrdreIns = fillCorrespondantAttributeVariable(_data, gr, varAtttributes);
    final GISDataModel model = TrPostTrajectoireGisDataModel.build(computeLigneCourant, attDansOrdreIns,
        varAtttributes, _analyse);
    if (model == null || _analyse.containsFatalError()) {
      return;
    }
    try {
      EventQueue.invokeAndWait(new Runnable() {
        @Override
        public void run() {
          if (calqueAecraser_ != null) {
            reloadLinesIn(calqueAecraser_, _data, computeLigneCourant, _analyse);
            _messages[0] = TrResource.TR.getString("Calque {0} mis � jour", calqueAecraser_.getTitle());
          } else {
            final FSigLayerGroup dest = getDestGroup(_data.isTrajectoire());
            final GISZoneCollectionLigneBrisee lb = new GISZoneCollectionLigneBrisee();
            lb.setAttributes(attDansOrdreIns.toArray(new GISAttributeInterface[attDansOrdreIns.size()]), null);
            lb.addAll(model, null, false);
            final String titreCalque = getCalqueTitle(_data);

            final ZCalqueLigneBrisee la = createLayer(vue2d, _data, dest, lb, titreCalque, findAttributeForVariable(
                varAtttributes, MARK));
            la.setDestructible(true);
            dest.enPremier(la);
            _messages[0] = TrResource.TR.getString("Calque {0} cr��", titreCalque);
          }
        }
      });
    } catch (Exception e) {
      FuLog.error(e);
    }
  }

  private String getCalqueTitle(final EfTrajectoireParameters _data) {
    String titreCalque;
    if (calqueAecraser_ == null) {
      CtuluDialogPanel pn = new CtuluDialogPanel(false);
      pn.setLayout(new BuGridLayout(2));
      pn.addLabel(TrLib.getString("Indiquer le nom du calque � cr�er"));
      JTextField tf = pn.addStringText();
      tf.setColumns(10);
      pn.afficheModale(CtuluLibSwing.getActiveWindow(), getCalqueName(_data.isTrajectoire()), CtuluDialog.OK_OPTION);
      titreCalque = tf.getText();
    } else {
      titreCalque = calqueAecraser_.getTitle();
      // -- on detruit le calque apres rejoue de donn�es --//
      calqueAecraser_.setDestructible(true);
      this.pn_.detruireCalque(calqueAecraser_);
    }
    return titreCalque;
  }

  GISAttributeInterface findAttributeForVariable(Map<GISAttributeInterface, CtuluVariable> _varAtttributes,
                                                 CtuluVariable _v) {
    for (Map.Entry<GISAttributeInterface, CtuluVariable> e : _varAtttributes.entrySet()) {
      if (e.getValue() == _v) {
        return e.getKey();
      }
    }
    return null;
  }

  private ZCalqueLigneBrisee createLayer(TrPostVisuPanel vue2d, final EfTrajectoireParameters _data,
                                         final FSigLayerGroup dest, final GISZoneCollectionLigneBrisee lb, String _title, GISAttributeInterface _mark) {
    final ZCalqueLigneBrisee la = new TrPostTrajectoireLineLayer(vue2d, _data, new ZModeleLigneBriseeDefault(lb), _mark);
    la.setName(BGroupeCalque.findUniqueChildName(dest, getCalqueId(_data.isTrajectoire())));
    la.setTitle(CtuluLibString.isEmpty(_title) ? createNewCalqueName(_data.isTrajectoire()) : _title);
    la.setDestructible(true);
    return la;
  }

  public static List<GISAttributeInterface> fillCorrespondantAttributeVariable(final EfTrajectoireParameters _data,
                                                                               final FSigLayerGroup _gr, final Map<GISAttributeInterface, CtuluVariable> _varAtttributes) {
    final List<GISAttributeInterface> attDansOrdreIns = new ArrayList<GISAttributeInterface>(20);
    H2dVariableType temps = H2dVariableType.TEMPS;
    attDansOrdreIns.add(getAttributeFor(_varAtttributes, _gr, temps));
    attDansOrdreIns.add(getAttributeFor(_varAtttributes, _gr, _data.vx));
    attDansOrdreIns.add(getAttributeFor(_varAtttributes, _gr, _data.vy));
    if (_data.varsASuivre_ != null) {
      for (final CtuluVariable v : _data.varsASuivre_) {
        // on fait cela car vx et vy sont deja ajoute
        addIfNotNull(attDansOrdreIns, getAttributeFor(_varAtttributes, _gr, v));
      }
    }
    // pour le marqueur on utilise la variable specifique MARK
    attDansOrdreIns.add(getBooleanAttributeFor(_varAtttributes, _gr, MARK));
    return attDansOrdreIns;
  }

  protected static void addIfNotNull(List<GISAttributeInterface> _dest, GISAttributeInterface _toAdd) {
    if (_toAdd != null) {
      _dest.add(_toAdd);
    }
  }

  private FSigLayerGroup createDestGroup(final boolean _trajectoire) {
    final FSigLayerGroup existGroup = findExistGroup(_trajectoire);
    if (existGroup != null) {
      return existGroup;
    }
    final FSigLayerGroup gr = pn_.getGroupGIS();
    final FSigLayerGroup res = gr.addGroupAct(getCalqueName(_trajectoire), gr, true, null);
    res.setActions(null);
    res.setAtt(new FSigAttibuteTypeManager());
    res.putClientProperty(TYPE_ID, getCalqueId(_trajectoire));
    res.setDestructible(true);
    return res;
  }

  private FSigLayerGroup findExistGroup(final boolean _trajectoire) {
    final FSigLayerGroup gr = pn_.getGroupGIS();
    final BCalque[] cq = gr.getCalques();
    final String toFind = getCalqueId(_trajectoire);
    if (cq != null) {
      for (int i = 0; i < cq.length; i++) {
        if (toFind.equals(cq[i].getClientProperty(TYPE_ID))) {
          return (FSigLayerGroup) cq[i];
        }
      }
    }
    return null;
  }

  private FSigLayerGroup getDestGroup(final boolean _trajectoire) {
    if (_trajectoire && traj_ != null) {
      return traj_;
    }
    if (!_trajectoire && ligneDeCourant_ != null) {
      return ligneDeCourant_;
    }
    final FSigLayerGroup res = createDestGroup(_trajectoire);
    if (_trajectoire) {
      traj_ = res;
    } else {
      ligneDeCourant_ = res;
    }
    return res;
  }

  /**
   * fonction appel�e en cas de stop de thread
   */
  public void stop() {
    stop_ = true;
    if (activity_ != null) {
      activity_.stop();
    }
  }
}
