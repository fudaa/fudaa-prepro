/**
 * @creation 10 juin 2004 @modification $Date: 2007-06-14 12:01:42 $ @license GNU General Public License 2 @copyright (c)1998-2001 CETMEF 2 bd
 *     Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.*;
import org.apache.commons.collections.Predicate;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluNumberFormater;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluValuesEditorPanel;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridVolumeInterface;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryFlowrateGroupType;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryTarageGroupType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.commun.EbliActionChangeState;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.meshviewer.model.MvEdgeModelDefault;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.*;
import org.fudaa.fudaa.tr.post.profile.MvVolumeAction;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Collection;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarVisuPanel.java,v 1.48 2007-06-14 12:01:42 deniger Exp $
 */
public final class TrRubarVisuPanel extends TrVisuPanelEditor implements H2dRubarBcListener, H2dRubarTarageListener {
  protected class AddDiffusionAction extends EbliActionSimple {
    protected AddDiffusionAction() {
      super(TrResource.getS("Utiliser un fichier des coefficients de diffusion"), null, "DIFF_ADD");
      super.putValue(EbliActionInterface.UNABLE_TOOLTIP, TrResource.getS("Le fichier est d�j� cr��"));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if (getRubarParams().isDiffusionCreated()) {
        getRubarParams().setUsedDiffusion(getCmdMng());
      } else {
        double val = getRubarParams().getDIfValueInDico().doubleValue();
        if (val < 0) {
          final Double newVal = askDiffusionValue(TrResource.getS("Indiquez une valeur par d�faut:"));
          if (newVal == null) {
            return;
          }
          val = newVal.doubleValue();
        }
        if (val >= 0) {
          getRubarParams().createDiffusion(val, getCmdMng());
        }
      }
    }

    @Override
    public void updateStateBeforeShow() {
      super.setEnabled(!getRubarParams().isDiffusionCreated() || getRubarParams().getDIfValueInDico().doubleValue() >= 0);
    }
  }

  protected class EditFrcStormAction extends EbliActionSimple {
    protected EditFrcStormAction() {
      super(TrResource.getS("Editer le frottement, chroniques d'apport, vent"), null, "FRC_STORM");
      super.putValue(EbliActionInterface.DEFAULT_TOOLTIP, TrResource.getS("Editer les �l�ments s�lectionn�s"));
      super.putValue(EbliActionInterface.UNABLE_TOOLTIP, TrResource.getS("S�lectionner au moins un �l�ment"));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final MvElementLayer l = getGridGroup().getPolygonLayer();
      new TrRubarElementEditor(l.getSelectedIndex(), proj_.getEvolMng(), proj_.getH2dRubarParameters(), getCmdMng()).afficheModale(getFrame(),
          TrResource.getS("propri�t�s �l�mentaires"));
    }

    @Override
    public void updateStateBeforeShow() {
      super.setEnabled(!getGridGroup().getPolygonLayer().isSelectionElementEmpty());
    }
  }

  protected class RemoveDiffusionAction extends EbliActionSimple {
    protected RemoveDiffusionAction() {
      super(TrResource.getS("Ne plus utiliser un fichier des coefficients de diffusion"), null, "DIFF_REMOVE");
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      double val = getRubarParams().getDIfValueInDico().doubleValue();
      if (val < 0) {
        final Double newVal = askDiffusionValue(TrResource.getS("Indiquer la valeur � utiliser dans le fichier PAR"));
        if (newVal == null) {
          return;
        }
        val = newVal.doubleValue();
      }
      getRubarParams().removeDiffusion(getCmdMng(), Double.toString(val));
    }

    @Override
    public void updateStateBeforeShow() {
      super.setEnabled(getRubarParams().isDiffusionCreated() || getRubarParams().getDIfValueInDico().doubleValue() < 0);
    }
  }

  @Override
  protected void addCqEdgeNumber(final EfGridVolumeInterface _g, final BGroupeCalque _c) {
    final TrEdgeNumberLayer pt = new TrEdgeNumberLayer(new MvEdgeModelDefault(_g), this.getAreteLayer());
    pt.setName("cqEdgeNumber");
    _c.add(pt);
  }

  protected static String br() {
    return "<br>";
  }

  public static String getDatErrorMsg() {
    return TrResource.getS("Le fichier DAT doit �tre charg�");
  }

  TrRubarInfoSenderDelegate infoDelegate_;
  TrRubarProject proj_;

  /**
   * @param _proj le projet en question
   */
  public TrRubarVisuPanel(final TrRubarProject _proj) {
    super(_proj.getImpl());
    proj_ = _proj;
    proj_.getH2dRubarParameters().getBcMng().addListener(this);
    proj_.getH2dRubarParameters().getTarageMng().addTarageListener(this);
    final EfGridVolumeInterface g = proj_.getH2dRubarParameters().getGridVolume();
    infoDelegate_ = new TrRubarInfoSenderDelegate(_proj, getEbliFormatter());

    final TrRubarBcAreteDefaultModel bcModel = new TrRubarBcAreteDefaultModel(_proj.getH2dRubarParameters());
    final TrRubarBcAreteLayer bcL = new TrRubarBcAreteLayer(bcModel, getCqLegend());
    bcL.setTitle(TrResource.getS("Ar�tes externes"));
    bcL.setName(TrBcLayerGroup.getBoundaryGroupName());
    addCalque(TrRubarDonneesBrutesLayerGroup.createDonneesBrutesGroup(proj_.getH2dRubarParameters().createDonneesBrutes(), this));
    final TrRubarLimniModel m = new TrRubarLimniModel(proj_.getH2dRubarParameters().getLimniMng());
    final ZCalquePoint limniLayer = m.buildLayer();
    limniLayer.putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Permet d'�diter les points ou seront cr��s des limnigrammes"));
    limniLayer.putClientProperty(CtuluLib.getHelpProperty(), getImpl().buildHelpPathFor("rubar-editor-limni"));
    limniLayer.setName("cqLimni");
    limniLayer.setTitle(H2dResource.getS("Positions des limnigrammes"));
    final TrRubarOuvrageLayer ouvLayer = new TrRubarOuvrageLayer(new TrRubarOuvrageLayerModel(proj_.getH2dRubarParameters(), infoDelegate_));
    ouvLayer.setName("cqOuv");
    ouvLayer.putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Edition des ouvrages"));
    ouvLayer.putClientProperty(CtuluLib.getHelpProperty(), getImpl().buildHelpPathFor("rubar-editor-ouv"));
    ouvLayer.setTitle(H2dResource.getS("Ouvrages"));
    ouvLayer.setLegende(getCqLegend());
    addCalque(limniLayer);
    addCalque(ouvLayer);
    final TrBcLayerGroup cl = buildGroupConlim();
    cl.putClientProperty(CtuluLib.getHelpProperty(), _proj.getImpl().buildHelpPathFor("rubar-editor-bc"));
    cl.add(bcL);

    addCqGroupeMaillage(new TrRubarGridLayerGroup(_proj.getH2dRubarParameters(), infoDelegate_, this));
    addCqInfos(g);

    final BGroupeCalque gsi = new BGroupeCalque();
    gsi.setTitle(TrResource.getS("Conditions initiales"));
    final TrRubarSILayer s = new TrRubarSILayer(new TrRubarElementSIModel(g, infoDelegate_), proj_.getH2dRubarParameters());
    s.setTitle(MvResource.getS("El�ments") + (CtuluLib.isFrenchLanguageSelected() ? " (CI)" : " (IC)"));
    s.setName(TrSiNodeLayer.getDefaultName());
    s.putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Permet d'�diter les conditions initiales"));
    s.putClientProperty(CtuluLib.getHelpProperty(), "rubar-editor-ic");
    gsi.add(s);
    gsi.setName(getSiGroupName());
    addCalque(gsi);
    // ajout des actions
    addActionsForBcAreteLayer();
    addActionsForSiLayer();
    addActionsForElementLayer();
    addActionsForLimni();
    addActionsForOuvrage();
    addActionsForPtLayer();
    addActionsForAreteLayer();
    getGridGroup().getPolygonLayer().putClientProperty(Action.SHORT_DESCRIPTION,
        TrResource.getS("Permet d'�diter les propri�t�s �l�mentaires et les chroniques d'apport"));
    getGridGroup().getPolygonLayer().putClientProperty(CtuluLib.getHelpProperty(), getImpl().buildHelpPathFor("rubar-editor-chronique"));
    super.addIsoLayer();
    new TrRubarVisuPanelModelListener(this);
  }

  @Override
  protected void fireGridPointModified() {
    getRubarParams().getBcMng().fireGridPointChanged();
  }

  @Override
  public Double getUndefinedValueForZ() {
    return Double.valueOf(9999.99);
  }

  private void addActionsForAreteLayer() {
    final EbliActionSimple a = new EbliActionSimple(TrResource.getS("Editer ar�te(s)"), null, "EDIT_ARETE") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        editArete();
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("S�lectionner au moins une ar�te");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(!getAreteLayer().isSelectionEmpty());
      }
    };
    a.setDefaultToolTip(TrResource.getS("Editer les ar�tes s�lectionn�es"));
    final EbliActionSimple selectEdgesOfType = new EbliActionSimple(TrResource.getS("S�lectionner les ar�tes du m�me type"), null, "SELECT_BC_ARETE") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        getAreteLayer().selectEdgesFromSameType();
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("S�lectionner au moins une ar�te");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(true);
        //        super.setEnabled(!getBcAreteLayer().isSelectionEmpty());
      }
    };
    selectEdgesOfType.setDefaultToolTip(TrResource.getS("S�lectionner les ar�tes du m�me type"));
    addCalqueActions(getAreteLayer(), new EbliActionInterface[]{a, selectEdgesOfType});
  }

  private void addActionsForBcAreteLayer() {
    final EbliActionSimple a = new EbliActionSimple(TrResource.getS("Editer ar�te(s)"), null, "EDIT_BC_ARETE") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        editBcArete();
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("S�lectionner au moins une ar�te");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(!getBcAreteLayer().isSelectionEmpty());
      }
    };
    a.setDefaultToolTip(TrResource.getS("Editer les ar�tes s�lectionn�es"));
    final EbliActionSimple selectEdgesOfType = new EbliActionSimple(TrResource.getS("S�lectionner les ar�tes du m�me type"), null, "SELECT_BC_ARETE") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        getBcAreteLayer().selectEdgesFromSameType();
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("S�lectionner au moins une ar�te");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(true);
        //        super.setEnabled(!getBcAreteLayer().isSelectionEmpty());
      }
    };
    selectEdgesOfType.setDefaultToolTip(TrResource.getS("S�lectionner les ar�tes du m�me type"));
    addCalqueActions(getBcAreteLayer(), new EbliActionInterface[]{a, selectEdgesOfType});
  }

  Double askDiffusionValue(final String _title) {
    final BuTextField tf = BuTextField.createDoubleField();
    final CtuluNumberFormater fmt = getRubarParams().getFmt().getFormatterFor(H2dVariableType.COEF_DIFFUSION);
    fmt.initTextField(tf);
    final CtuluDialogPanel pn = new CtuluDialogPanel(true) {
      @Override
      public boolean isDataValid() {
        final boolean res = tf.getValue() != null;
        if (!res) {
          setErrorText(fmt.getValueValidator().getDescription());
        }
        return res;
      }
    };
    pn.setLayout(new BuBorderLayout(5, 5));

    final BuPanel val = new BuPanel();
    val.setLayout(new BuGridLayout(2, 5, 5));
    pn.addLabel(val, H2dVariableType.COEF_DIFFUSION.getName());
    val.add(tf);
    pn.add(new BuLabel(_title), BuBorderLayout.NORTH);
    pn.add(val);
    Double valFinal = null;
    if (pn.afficheModaleOk(getFrame(), H2dVariableType.COEF_DIFFUSION.getName())) {
      valFinal = ((Double) tf.getValue());
    }
    return valFinal;
  }

  H2dRubarParameters getRubarParams() {
    return proj_.getTrRubarParams().getH2dRubarParametres();
  }

  /**
   * Ajoute les actions pour les si.
   */
  protected void addActionsForElementLayer() {
    addCalqueActions(getGridGroup().getPolygonLayer(), new EbliActionInterface[]{new ElementEditAction(), new EditFrcStormAction(),
        new ElementEditGeomAction(), null, new AddDiffusionAction(), new RemoveDiffusionAction()});
  }

  protected void addActionsForLimni() {
    final EbliActionInterface[] r = new EbliActionInterface[3];
    int idx = 0;

    r[idx++] = new TrSaisiePointAction(TrResource.getS("Ajouter des limnigrammes"), null, "ADD_LIMNI") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        if (isEnCours()) {
          super.selectPalette();
          return;
        }
        new TrRubarLimniSaisie(TrRubarVisuPanel.this, getLimniLayer()).activeSaisiePoint(this);
      }
    };
    r[idx] = new EbliActionSimple(TrResource.getS("Enlever les limnigrammes"), null, "REMOVE_LIMNI") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        final int[] idxSelected = getLimniLayer().getSelectedIndex();
        if (idxSelected != null) {
          getLimniLayer().clearSelection();
          proj_.getH2dRubarParameters().getLimniMng().removeLimni(idxSelected, getCmdMng());
        }
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("S�lectionner au moins un limnigramme");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(!getLimniLayer().isSelectionEmpty());
      }
    };
    r[idx++].setDefaultToolTip(TrResource.getS("Enlever les limnigrammes s�lectionn�s"));
    r[idx++] = new EbliActionSimple(H2dResource.getS("Limnigrammes") + ": " + TrResource.getS("pas de temps de stockage"), null, "TIME_LIMNI") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        final BuTextField ft = BuTextField.createDoubleField();
        ft.setColumns(8);
        ft.setText(Double.toString(proj_.getH2dRubarParameters().getLimniMng().getStockage()));
        final CtuluDialogPanel pn = new CtuluDialogPanel() {
          @Override
          public boolean apply() {
            final String txt = ft.getText().trim();
            if (txt.length() > 0) {
              final double d = Double.parseDouble(txt);
              proj_.getH2dRubarParameters().getLimniMng().setTimeStepStockage(d, getCmdMng());
            }
            return true;
          }

          @Override
          public boolean isDataValid() {
            final String txt = ft.getText().trim();
            if (txt.length() > 0 && Double.parseDouble(txt) < 0) {
              setErrorText(TrResource.getS("Valeur positive attendue"));
              return false;
            }
            return true;
          }
        };
        pn.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        pn.setLayout(new BuGridLayout(2, 5, 5));
        pn.add(new BuLabel(TrResource.getS("pas de temps de stockage")));
        pn.add(ft);
        pn.afficheModale(getImpl().getFrame(), (String) super.getValue(Action.NAME));
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("Au moins un limnigramme doit �tre d�fini");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(!proj_.getH2dRubarParameters().getLimniMng().isEmpty());
      }
    };
    super.addCalqueActions(getLimniLayer(), r);
  }

  protected void addActionsForOuvrage() {
    final EbliActionInterface[] ebliActionInterfaces = new EbliActionInterface[10];
    int idx = 0;
    ebliActionInterfaces[idx] = new EbliActionSimple(TrResource.getS("Editer ouvrage"), null, "EDIT_WORK") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        final CtuluListSelectionInterface s = getOuvrageLayer().getLayerSelection();
        if (s != null && s.isOnlyOnIndexSelected()) {
          final TrRubarOuvrageEditorPanel pn = new TrRubarOuvrageEditorPanel(
              proj_.getH2dRubarParameters().getOuvrageMng().getRubarOuvrage(s.getMaxIndex()),
              proj_.getH2dRubarParameters().getOuvrageMng().getGrid(), getCmdMng(), getImpl(), (TrRubarEvolutionManager) proj_.getEvolMng(),
              proj_.getTrRubarParams().getTarageMng());
          pn.afficheModale(getImpl().getFrame(), H2dResource.getS("Ouvrage {0}", CtuluLibString.getString(s.getMaxIndex() + 1)));
        }
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("D�sactiver le mode \"�l�ment\"") + br() + TrResource.getS("S�lectionner un seul ouvrage");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(proj_.getH2dRubarParameters().getOuvrageMng().isInitialized() && !getOuvrageLayer().getM().isModeElt()
            && getOuvrageLayer().isOnlyOneObjectSelected());
      }
    };
    ebliActionInterfaces[idx++].setDefaultToolTip(TrResource.getS("Editer l'ouvrage s�lectionn�"));

    ebliActionInterfaces[idx++] = new EbliActionSimple(TrResource.getS("Enlever ouvrages s�lectionn�s"), null, "REMOVE_WORKS") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        final int[] idxSelected = getOuvrageLayer().getSelectedIndex();
        if (idxSelected != null && idxSelected.length > 0) {
          getOuvrageLayer().clearSelection();
          proj_.getH2dRubarParameters().getOuvrageMng().removeOuvrage(idxSelected, getCmdMng());
        }
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("D�sactiver le mode \"�l�ment\"") + br() + TrResource.getS("S�lectionner au moins un ouvrage");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(proj_.getH2dRubarParameters().getOuvrageMng().isInitialized() && !getOuvrageLayer().getM().isModeElt()
            && !getOuvrageLayer().isSelectionEmpty());
      }
    };
    ebliActionInterfaces[idx++] = null;
    ebliActionInterfaces[idx++] = new EbliActionSimple(TrResource.getS("S�lectionner les ouvrages avec r�f�rence -1"), null, "SELECT_WORKS_REF_1") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        getOuvrageLayer().selectWorksWithRubarRef(-1);
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("D�sactiver le mode \"�l�ment\"");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(proj_.getH2dRubarParameters().getOuvrageMng().isInitialized() && !getOuvrageLayer().getM().isModeElt());
      }
    };
    ebliActionInterfaces[idx++] = new EbliActionSimple(TrResource.getS("S�lectionner les ouvrages avec r�f�rence -2"), null, "SELECT_WORKS_REF_2") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        getOuvrageLayer().selectWorksWithRubarRef(-2);
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("D�sactiver le mode \"�l�ment\"");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(proj_.getH2dRubarParameters().getOuvrageMng().isInitialized() && !getOuvrageLayer().getM().isModeElt());
      }
    };
    ebliActionInterfaces[idx++] = new EbliActionSimple(TrResource.getS("S�lectionner les ouvrages avec une maille avale"), null, "SELECT_WORKS_DOWNSTREAM_MESH") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        getOuvrageLayer().selectWorksWithDownstreamMesh(true);
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("D�sactiver le mode \"�l�ment\"");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(proj_.getH2dRubarParameters().getOuvrageMng().isInitialized() && !getOuvrageLayer().getM().isModeElt());
      }
    };
    ebliActionInterfaces[idx++] = new EbliActionSimple(TrResource.getS("S�lectionner les ouvrages sans maille avale"), null, "SELECT_WORKS_NODOWNSTREAM_MESH") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        getOuvrageLayer().selectWorksWithDownstreamMesh(false);
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("D�sactiver le mode \"�l�ment\"");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(proj_.getH2dRubarParameters().getOuvrageMng().isInitialized() && !getOuvrageLayer().getM().isModeElt());
      }
    };

    ebliActionInterfaces[idx++] = null;
    ebliActionInterfaces[idx++] = new EbliActionChangeState(TrResource.getS("Travailler sur les �l�ments"), null, "MODE_WORKS") {
      @Override
      public void changeAction() {
        getOuvrageLayer().changeModeElt();
      }

      @Override
      public String getEnableCondition() {
        return getDatErrorMsg();
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(proj_.getH2dRubarParameters().getOuvrageMng().isInitialized());
      }
    };
    ebliActionInterfaces[idx++] = new TrSaisiePointAction(TrResource.getS("Ajouter un ouvrage"), null, "ADD_WORK") {
      int[] idxSelected_;

      @Override
      public void actionPerformed(final ActionEvent _arg) {
        if (isEnCours()) {
          super.selectPalette();
          return;
        }
        if (idxSelected_ == null) {
          getImpl().error((String) super.getValue(Action.NAME), H2dResource.getS("Les �l�ments ne sont pas adjacents"), false);
        } else {
          new TrRubarOuvrageSaisie(TrRubarVisuPanel.this, getRubarParams().getOuvrageMng().getGrid(), getOuvrageLayer(), idxSelected_,
              getRubarParams().getOuvrageMng().getUsedEdge(), (TrRubarEvolutionManager) proj_.getEvolMng(), proj_.getTrRubarParams().getTarageMng()).activeSaisiePoint(this);
        }
      }

      @Override
      public String getEnableCondition() {
        return getDatErrorMsg() + br() + TrResource.getS("Activer le mode \"�l�ments\"")
            + br() + TrResource.getS("S�lectionner des �l�ments adjacents")
            + br() + TrResource.getS("Ou 2 �l�ments non adjacents")

            ;
      }

      @Override
      public void updateStateBeforeShow() {
        final TrRubarOuvrageLayer l = getOuvrageLayer();
        idxSelected_ = l.getSelectedIndex();
        if (!l.getM().isSelectionValide(idxSelected_)) {
          idxSelected_ = null;
        }
        boolean enable = false;
        String userMessage = null;
        if (!proj_.getH2dRubarParameters().getOuvrageMng().isInitialized()) {
          userMessage = getDatErrorMsg();
        } else if (!getOuvrageLayer().getM().isModeElt()) {
          userMessage = TrResource.getS("Activer le mode \"�l�ments\"");
        } else if (!l.isSelectionEmpty() && idxSelected_ == null) {
          userMessage = TrResource.getS("Les �l�ments s�lectionn�s ne sont pas adjacents");
        } else {
          enable = true;
        }
        super.setUserMessage(userMessage);
        super.setEnabled(enable);
      }
    };

    addCalqueActions(getOuvrageLayer(), ebliActionInterfaces);
  }

  protected void addActionsForPtLayer() {
    final String condit = TrResource.getS("S�lectionner au moins un point") + br() + getDatErrorMsg();
    final EbliActionSimple nodalEditAction = new NodalEditAction() {
      @Override
      public Predicate getVariableFilter() {
        return new SedimentPredicate(false);
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(proj_.isDatLoaded() && !getGridGroup().getPointLayer().isSelectionEmpty());
      }
    };
    final EbliActionSimple nodalSedimentEditAction = new NodalEditAction() {
      {
        super.putValue(Action.NAME, TrLib.getString("Editer les couches s�dimentaires"));
      }

      @Override
      public void actionPerformed(ActionEvent _e) {
        editSedProperties(this.getVariables(), this.getLayer(), getVariableFilter());
      }

      @Override
      public Predicate getVariableFilter() {
        return new SedimentPredicate(true);
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(proj_.isDatLoaded() && proj_.getTrRubarParams().getH2dRubarParametres().getProjetType().equals(H2dRubarProjetType.TRANSPORT)
            && !getGridGroup().getPointLayer().isSelectionEmpty());
      }
    };
    nodalEditAction.putValue(EbliActionInterface.UNABLE_TOOLTIP, condit);
    final EbliActionSimple nodalEditGeomAction = new NodalEditGeomAction() {
      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(proj_.isDatLoaded());
      }
    };
    nodalEditGeomAction.putValue(EbliActionInterface.UNABLE_TOOLTIP, getDatErrorMsg());
    addCalqueActions(getGridGroup().getPointLayer(), new EbliActionInterface[]{nodalEditAction, nodalSedimentEditAction, nodalEditGeomAction,
        new TrEditPointAction(this), new TrValidGridAction(this)});
  }

  private static class SedimentPredicate implements Predicate {
    private final boolean acceptSediment;

    public SedimentPredicate(boolean acceptSediment) {
      super();
      this.acceptSediment = acceptSediment;
    }

    @Override
    public boolean evaluate(Object object) {
      if (object instanceof H2dRubarSedimentVariableType) {
        return acceptSediment;
      }
      return !acceptSediment;
    }
  }

  @Override
  public Predicate getNodalVariableFilter() {
    return new SedimentPredicate(false);
  }

  /**
   * Ajoute les actions pour les si.
   */
  protected void addActionsForSiLayer() {
    final EbliActionSimple a1 = new EbliActionSimple(TrResource.getS("Editer le temps initial"), null, "EDIT_TIME_CL") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        editSITime();
      }
    };
    final EbliActionChangeState aChangeCote = new EbliActionChangeState(TrResource.getS("Travailler sur le cote d'eau"), null, "EDIT_TIME_CL") {
      @Override
      public void changeAction() {
        getRubarParams().getSi().setCoteViewed(!getRubarParams().getSi().isCoteViewed());
      }

      @Override
      public void updateStateBeforeShow() {
        setSelected(getRubarParams().getSi().isCoteViewed());
      }
    };
    addCalqueActions((BCalque) getSiPointLayer(), new EbliActionInterface[]{new SiEditAction(), new SiEditGeomAction(), aChangeCote, a1, null,
        new SiClearHAction(), new SiCheckHauteurEau(), null, new MvVolumeAction(getParams().createSiGridDataAdapter(), TrRubarVisuPanel.this)});
  }

  protected void editArete(final int[] _selectedArete) {
    if (_selectedArete == null) {
      return;
    }
    boolean isIntern = false;
    final H2dRubarParameters p = proj_.getH2dRubarParameters();
    for (int i = _selectedArete.length - 1; i >= 0; i--) {
      if (!((H2dRubarArete) p.getGridVolume().getArete(_selectedArete[i])).isExtern()) {
        isIntern = true;
        break;
      }
    }
    if (isIntern) {
      getImpl().message(TrResource.getS("Seules les ar�tes externes peuvent �tre �dit�es"));
      return;
    }
    new TrRubarAreteEditor(_selectedArete, proj_.getH2dRubarParameters(), getCmdMng(), proj_.getEvolMng(), proj_.getTrRubarParams().getTarageMng()).afficheModale(getFrame());
  }


  /**
   * A appeler lorsque le type d'une arete est modifie.
   */
  @Override
  public void areteTypeChanged() {
    updateInfoComponent();
    getBcAreteLayer().clearCache();
    getVueCalque().repaint();
    getBcAreteLayer().updateLegende();
  }

  @Override
  public void bathyChanged() {
    updateInfoAndIso();
  }

  @Override
  public void nodeInGridChanged() {
    updateInfoAndIso();
  }

  @Override
  public void fondDurChanged() {
    updateInfoAndIso();
  }

  /**
   * Methode a appeler pour �diter les aretes.
   */
  public void editArete() {
    editArete(getAreteLayer().getSelectedIndex());
  }

  /**
   * Methode a appeler pour editer les aretes de bord.
   */
  public void editBcArete() {
    editArete(getBcAreteLayer().getSelectedGlobalIndex());
  }

  /**
   * Ne fait rien.
   */
  @Override
  public String editBcPoint() {
    return null;
  }

  @Override
  public String editGridPoly() {
    final MvElementLayer l = getGridGroup().getPolygonLayer();
    if (l.isSelectionEmpty()) {
      return MvResource.getS("S�lection vide");
    }
    getGridGroup().getPolygonLayer().getActions()[0].actionPerformed(null);
    return null;
  }

  /**
   * Edit le temps initial pour les solutions initiales.
   */
  public void editSITime() {
    final CtuluNumberFormater fm = proj_.getH2dRubarParameters().getFmt().getTime();
    final BuTextField f = BuTextField.createDoubleField();
    fm.initTextField(f, getRubarParams().getSi().getT0());
    final CtuluDialogPanel pn = new CtuluDialogPanel(true) {
      @Override
      public boolean isDataValid() {
        final boolean r = fm.getValueValidator().isValueValid(f.getValue());
        if (!r) {
          setErrorText(fm.getValueValidator().getDescription());
        }
        return r;
      }
    };
    pn.setLayout(new BuBorderLayout());
    pn.add(f, BuBorderLayout.CENTER);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(getFrame()))) {
      getRubarParams().getSi().setBeginTime(((Double) f.getValue()).doubleValue(), getCmdMng());
    }
  }

  @Override
  public void flowrateGroupChanged(final H2dRubarBoundaryFlowrateGroupType _t) {
  }

  /**
   * @return le calque des aretes utilise dans le groupe maillage
   */
  public TrRubarAreteLayer getAreteLayer() {
    return getRubarGridLayerGroup().getAreteLayer();
  }

  /**
   * @return le calque des aretes de bord
   */
  public TrRubarBcAreteLayer getBcAreteLayer() {
    return (TrRubarBcAreteLayer) getGroupBoundary().getBcBoundaryLayer();
  }

  @Override
  public EfGridInterface getGrid() {
    return getRubarParams().getMaillage();
  }

  @Override
  public void getDiscreteIsoPainters(final Collection _setToFill) {
    _setToFill.add(new TrRubarApportIsoPainter(getRubarParams()));
    _setToFill.add(new TrRubarVentIsoPainter(getRubarParams()));
    _setToFill.add(new TrRubarVentIsoXOrYPainter(getRubarParams(), false));
    _setToFill.add(new TrRubarVentIsoXOrYPainter(getRubarParams(), true));
  }

  /**
   * @return le calque gerant les limnigrammes
   */
  public ZCalquePoint getLimniLayer() {
    return (ZCalquePoint) super.getDonneesCalqueByName("cqLimni");
  }

  /**
   * @return le calque gerant les ouvrages
   */
  public TrRubarOuvrageLayer getOuvrageLayer() {
    return (TrRubarOuvrageLayer) super.getDonneesCalqueByName("cqOuv");
  }

  @Override
  public H2dRubarParameters getParams() {
    return proj_.getH2dRubarParameters();
  }

  @Override
  public String getProjectName() {
    return proj_.getParamsFile().getAbsolutePath();
  }

  /**
   * @return le groupe maillage special rubar
   */
  public TrRubarGridLayerGroup getRubarGridLayerGroup() {
    return (TrRubarGridLayerGroup) getGridGroup();
  }

  /**
   * @return false
   */
  @Override
  public boolean isBcPointEditable() {
    return false;
  }

  /**
   * @return true
   */
  @Override
  public boolean isGridElementEditable() {
    return true;
  }

  @Override
  public boolean isGridPointEditable() {
    return true;
  }

  @Override
  public void projectTypeChanged() {
    updateInfoAndIso();
  }

  @Override
  public void numberOfConcentrationChanged() {
    updateInfoAndIso();
  }

  @Override
  public void restaurer() {
    final GrBoite b = getGridGroup().getDomaine();
    if ((b == null) || b.isIndefinie()) {
      return;
    }
    vc_.changeRepere(this, b);
  }

  public String editSedProperties(final H2dVariableProviderInterface _provider, final ZCalqueAffichageDonneesInterface _cq,
                                  Predicate variablePredicate) {
    String r = null;
    final H2dVariableType[] var = _provider == null ? null : _provider.getVarToModify();
    if (_provider != null && var != null && var.length > 0) {
      if (_cq.isSelectionEmpty()) {
        r = MvResource.getS("S�lection vide");
      } else {
        final CtuluValuesEditorPanel pn = buildValuesPanel(_cq, _provider, variablePredicate);
        pn.displayOnlyCommonPanel();
        String title = null;

        if (_cq.isOnlyOneObjectSelected()) {
          title = MvResource.getS(_provider.isElementVar() ? "El�ment n� {0}" : "Noeud n� {0}",
              CtuluLibString.getString(getGridGroup().getPointLayer().getLayerSelection().getMaxIndex() + 1));
        } else {
          title = _cq.getTitle();
        }
        afficheValuesPanel(pn, title);
      }
    } else {
      TrResource.getS("Les propri�t�s ne sont pas modifiables");
    }
    return r;
  }

  /**
   * A appeler lorsque les conditions limites ont change.
   */
  public void siChanged() {
    updateInfoComponent();
  }

  @Override
  public void tarageCourbeChanged(final H2dRubarTarageMng _mng, final EvolutionReguliereInterface _e) {
  }

  @Override
  public void TarageGroupeUsedChanged(H2dRubarTarageMng _mng, H2dRubarBoundaryTarageGroupType tarageGroupeType) {
    updateInfoComponent();
  }

  @Override
  public void tarageUsedChanged(final H2dRubarTarageMng _mng) {
    updateInfoComponent();
  }

  @Override
  public void timeClChanged() {
  }

  /**
   * Les variables a afficher dans les iso ont ete modifiees.
   */
  public void varToDisplayInIsoChanged() {
    super.updateIsoVariables();
  }
}
