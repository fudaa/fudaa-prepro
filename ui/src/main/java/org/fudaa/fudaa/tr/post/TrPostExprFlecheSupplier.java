/*
 *  @creation     9 ao�t 2005
 *  @modification $Date: 2007-04-30 14:22:38 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import gnu.trove.TObjectIntHashMap;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.data.TrPostDataVecteur;
import org.nfunk.jep.Variable;

/**
 * A voir s'il ne faut pas optimiser cela: cache pour les variables.
 * 
 * @author Fred Deniger
 * @version $Id: TrPostExprFlecheSupplier.java,v 1.10 2007-04-30 14:22:38 deniger Exp $
 */
public class TrPostExprFlecheSupplier implements EbliFindExpressionContainerInterface {

  final TrPostFlecheModel model_;
  TObjectIntHashMap varFleche_;
  TObjectIntHashMap varFlecheX_;
  TObjectIntHashMap varFlecheY_;

  /**
   * @param _model
   */
  public TrPostExprFlecheSupplier(final TrPostFlecheModel _model) {
    super();
    model_ = _model;
  }

  @Override
  public void initialiseExpr(final CtuluExpr _exp) {
    if (varFleche_ == null) {
      varFleche_ = new TObjectIntHashMap();
    } else {
      varFleche_.clear();
    }
    if (varFlecheX_ == null) {
      varFlecheX_ = new TObjectIntHashMap();
    } else {
      varFlecheX_.clear();
    }
    if (varFlecheY_ == null) {
      varFlecheY_ = new TObjectIntHashMap();
    } else {
      varFlecheY_.clear();
    }
    if (model_ != null) {
      for (int i = model_.fleche_.getSize() - 1; i >= 0; i--) {
        final H2dVariableType vi = ((TrPostFlecheContent) model_.fleche_.getElementAt(i)).getVar();
        varFleche_.put(_exp.addVar(vi.getShortName(), vi.getName()), i);
        varFlecheX_.put(_exp.addVar(vi.getShortName() + "_x", vi.getName() + CtuluLibString.ESPACE
            + TrResource.getS("selon x")), i);
        varFlecheY_.put(_exp.addVar(vi.getShortName() + "_y", vi.getName() + CtuluLibString.ESPACE
            + TrResource.getS("selon y")), i);
      }
    }

  }

  @Override
  public void majVariable(final int _idx, final Variable[] _varToUpdate) {
    if (_varToUpdate != null) {
      // ces trois variables permettent de garder des valeurs interm�diaires
      int lastIdx = -1;
      TrPostFlecheContent newFleche = null;
      TrPostDataVecteur vect = null;
      for (int i = _varToUpdate.length - 1; i >= 0; i--) {
        final Variable vari = _varToUpdate[i];
        if (varFleche_.containsKey(vari)) {
          final int var = varFleche_.get(vari);
          if (var != lastIdx) {
            lastIdx = var;
            newFleche = (TrPostFlecheContent) model_.getListModel().getElementAt(lastIdx);
            vect = null;
          }
          if (newFleche != null) {
            _varToUpdate[i].setValue(CtuluLib.getDouble(newFleche.getValue(model_.timeIdx_, _idx)));
          }
        }
        if (varFlecheX_.containsKey(vari)) {
          final int varX = varFlecheX_.get(vari);
          if (varX != lastIdx) {
            lastIdx = varX;
            newFleche = (TrPostFlecheContent) model_.getListModel().getElementAt(lastIdx);
            vect = null;
          }
          if (vect == null && newFleche != null) {
            vect = newFleche.getValues(model_.timeIdx_);
          }
          if (vect != null) {
            _varToUpdate[i].setValue(CtuluLib.getDouble(vect.getVx(_idx)));
          }

        }
        if (varFlecheY_.containsKey(vari)) {
          final int varY = varFlecheY_.get(vari);
          if (varY != lastIdx) {
            lastIdx = varY;
            newFleche = (TrPostFlecheContent) model_.getListModel().getElementAt(lastIdx);
            vect = null;
          }
          if (vect == null && newFleche != null) {
            vect = newFleche.getValues(model_.timeIdx_);
          }
          if (vect != null) {
            _varToUpdate[i].setValue(CtuluLib.getDouble(vect.getVy(_idx)));
          }

        }
      }
    }

  }
}
