/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.dialogSpec;

import javax.swing.AbstractListModel;
import javax.swing.JTextField;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;

/**
 *
 * @author Frederic Deniger
 */
public interface AbstractTrPostDialogAddToSceneDialogContent {

  EbliScene getScene();

  H2dVariableType getSelectedVariable();

  double getSeuil();

  JTextField getTxtSeuil();

  AbstractListModel getModelVariables();

  TrPostSource getSource();

  TrPostProjet getProject();

  TrPostVisuPanel getVisuPanel();
  
}
