package org.fudaa.fudaa.tr.data;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.tr.common.TrResource;

import java.awt.event.ActionEvent;

public class TrEditPointAction extends EbliActionSimple {
  private TrVisuPanelEditor visuPanelEditor;

  public TrEditPointAction(TrVisuPanelEditor visuPanelEditor) {
    super(TrResource.getS("Modifier le point sélectionné"), null, "MODIFIE_POINT");
    this.visuPanelEditor = visuPanelEditor;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final int selectedPt = visuPanelEditor.getGridGroup().getPointLayer().getSelectedPtIdx()[0];
    final EfGridInterface grid = visuPanelEditor.getGrid();
    TrEditNodesPanel.Data data = new TrEditNodesPanel.Data(new int[]{selectedPt});
    data.oldX[0] = grid.getPtX(selectedPt);
    data.oldY[0] = grid.getPtY(selectedPt);
    data.oldZ[0] = grid.getPtZ(selectedPt);
    data.newX[0] = grid.getPtX(selectedPt);
    data.newY[0] = grid.getPtY(selectedPt);
    data.newZ[0] = grid.getPtZ(selectedPt);

    TrEditNodesPanel panel = TrEditNodesPanel.build(this.visuPanelEditor, data,
        visuPanelEditor.getParams().getNodalData().getModifiableModel(H2dVariableType.BATHYMETRIE), visuPanelEditor.getUndefinedValueForZ());
    String title;
    title = TrResource.getS("Modifier le point sélectionné: {0}", CtuluLibString.getString(selectedPt + 1));
    if (CtuluDialogPanel
        .isOkResponse(panel.afficheModale(visuPanelEditor.getFrame(), title))) {
      panel.checkAndApplyModifications(visuPanelEditor.getCmdMng());
    }
  }

  @Override
  public void updateStateBeforeShow() {
    final int[] selectedPts = visuPanelEditor.getGridGroup().getPointLayer().getSelectedPtIdx();

    if (selectedPts == null) {
      this.setEnabled(false);
    } else {
      this.setEnabled(selectedPts.length == 1);
    }
  }
}
