/**
 *  @creation     10 juin 2004
 *  @modification $Date: 2007-01-19 13:14:12 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.rubar;

import java.util.List;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfSegment;
import org.fudaa.dodico.h2d.rubar.H2dRubarBcMng;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;

/**
 * Un modele pour les aretes de bords.
 * 
 * @author Fred Deniger
 * @version $Id: TrRubarBcAreteDefaultModel.java,v 1.14 2007-01-19 13:14:12 deniger Exp $
 */
public class TrRubarBcAreteDefaultModel extends TrRubarAreteModelProject implements TrRubarBcAreteModel {

  private final H2dRubarBcMng bcMng_;

  /**
   * @param _p les parametres
   */
  public TrRubarBcAreteDefaultModel(final H2dRubarParameters _p) {
    super(_p);
    bcMng_ = _p.getBcMng();
  }

  @Override
  public EfFrontierInterface getFr() {
    return bcMng_.getGridVolume().getFrontiers();
  }

  /**
   * @return la liste des bords utilises
   */
  @Override
  public List getUsedBoundaryType() {
    return bcMng_.getUsedBoundaryType();
  }

  @Override
  public List getBordList() {
    return bcMng_.getBordList();
  }

  /**
   * @param _frIdx l'indice de la maille sur la frontiere
   * @return l'indice global
   */
  @Override
  public int getGlobalIdx(final int _frIdx) {
    return bcMng_.getGlobalIdx(_frIdx);
  }

  @Override
  public int[] getGlobalIdx(final int[] _selectedIdx) {
    if (_selectedIdx == null || _selectedIdx.length == 0) { return _selectedIdx; }
    final int[] idx = new int[_selectedIdx.length];
    for (int i = idx.length - 1; i >= 0; i--) {
      idx[i] = getGlobalIdx(_selectedIdx[i]);
    }
    return idx;
  }

  @Override
  public EfSegment getArete(final int _idx) {
    return bcMng_.getBcArete(_idx);
  }

  @Override
  public int getNombre() {
    return bcMng_.getNbBcArete();
  }

  /**
   * @return le nombre de bord diff utilise
   */
  @Override
  public int getNbTypeBord() {
    return bcMng_.getBordList().size();
  }

}
