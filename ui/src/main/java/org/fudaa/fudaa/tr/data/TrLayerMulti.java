/*
 *  @creation     19 d�c. 2003
 *  @modification $Date: 2007-01-19 13:14:08 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.data;

import org.fudaa.ebli.calque.ZCalqueAffichageDonneesMultiSpecial;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;

/**
 * @author deniger
 * @version $Id: TrLayerMulti.java,v 1.11 2007-01-19 13:14:08 deniger Exp $
 */
public abstract class TrLayerMulti extends ZCalqueAffichageDonneesMultiSpecial implements MvLayerGrid {

  public boolean isConfigurable() {
    return false;
  }

  @Override
  protected final void select(final int _i, final int _idx1, final int _idx2) {
    if (_i > 0) {
      super.select(_i, _idx2, _idx1);
    } else {
      super.select(_i, _idx1, _idx2);
    }
  }

}
