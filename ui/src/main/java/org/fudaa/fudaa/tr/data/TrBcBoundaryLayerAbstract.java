/**
 * @creation 20 nov. 2003
 * @modification $Date: 2007-06-05 09:01:15 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.tr.data;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesConfigure;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrBcBoundaryLayerAbstract.java,v 1.27 2007-06-05 09:01:15 deniger Exp $
 */
public abstract class TrBcBoundaryLayerAbstract extends TrLayerMulti implements TrBcBoundaryLayer {

  TrBoundaryLineEditor editor_;

  protected TrBordTraceLigneData bdTlData_;

  /**
   * Met a jour le titre et la legende.
   * 
   * @param _l la legende.
   */
  public TrBcBoundaryLayerAbstract(final BCalqueLegende _l) {
    setTitle(TrResource.getS("Bords"));
    setLegende(_l);
  }

  @Override
  protected BConfigurableInterface getAffichageConf() {
    if (editor_ == null) {
      editor_ = new TrBoundaryLineEditor(this);
    }
    final BConfigurableInterface[] sect = new BConfigurableInterface[2];
    sect[0] = new ZCalqueAffichageDonneesConfigure(this);
    sect[1] = editor_;
    return new BConfigurableComposite(sect, EbliLib.getS("Affichage"));
  }

  protected void initLegende() {
    if (!getLegende().containsLegend(this)) {
      getLegende().ajouteLegendPanel(new TrBcBoundaryLegendPanel(this));
    }
  }

  @Override
  public void boundaryTypeRendererChanged() {
    updateLegende();
    repaint();
  }

  @Override
  public TrBordTraceLigneData getBordTrace() {
    if (bdTlData_ == null) {
      bdTlData_ = new TrBordTraceLigneData();
    }
    return bdTlData_;
  }

  /**
   * @return le nombre TOTAL de bord a utiliser
   */
  public abstract int getNbBoundaryType();

  @Override
  public int[] getSelectedEdgeIdx() {
    return null;
  }

  @Override
  public TraceLigneModel getTlData(final H2dBoundaryType _t) {
    if (bdTlData_ == null) {
      bdTlData_ = new TrBordTraceLigneData();
    }
    final TraceLigneModel res = bdTlData_.getTlData(_t);
    if (EbliLib.isAlphaChanged(alpha_)) {
      res.setColor(EbliLib.getAlphaColor(res.getCouleur(), alpha_));
    }
    return res;
  }

  /**
   * return true.
   */
  @Override
  public final boolean isConfigurable() {
    return true;
  }

  public boolean isFontModifiable() {
    return false;
  }

  public boolean isForegroundColorModifiable() {
    return false;
  }

  @Override
  public boolean isPaletteModifiable() {
    return false;
  }

  @Override
  public boolean isSelectionEdgeEmpty() {
    return true;
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    final Color solide = new Color(164, 95, 30);
    final TraceLigne l = new TraceLigne();
    l.setEpaisseur(2);
    final int h = getIconHeight();
    final int w = getIconWidth();
    int x1 = _x + 3;
    int y1 = _y + h / 2;
    int x2 = _y + w / 4;
    int y2 = _y + 3;
    l.setCouleur(Color.BLUE);
    l.dessineTrait((Graphics2D) _g, x1, y1, x2, y2);
    x1 = x2;
    y1 = y2;
    y2 = _y + h / 4;
    x2 = _x + w / 2;
    l.setCouleur(solide);
    l.dessineTrait((Graphics2D) _g, x1, y1, x2, y2);
    x1 = x2;
    y1 = y2;
    y2 = _y + 3;
    x2 = _x + w / 2 + 2;
    l.dessineTrait((Graphics2D) _g, x1, y1, x2, y2);
    l.setCouleur(Color.BLUE);
    x1 = x2;
    y1 = y2;
    y2 = _y + h / 2;
    x2 = _x + w - 2;
    l.dessineTrait((Graphics2D) _g, x1, y1, x2, y2);
    l.setCouleur(solide);
    x1 = x2;
    y1 = y2;
    y2 = _y + h - 2;
    x2 = _x + w - 5;
    l.dessineTrait((Graphics2D) _g, x1, y1, x2, y2);
    x1 = x2;
    y1 = y2;
    x2 = _x + 3;
    y2 = _y + h / 2;
    l.dessineTrait((Graphics2D) _g, x1, y1, x2, y2);
  }

  /**
   * Met a jour la legende en fonction des bords utilises.
   */
  public void updateLegende() {
    final BCalqueLegende l = getLegende();
    if (l != null) {
      final TrBcBoundaryLegendPanel p = (TrBcBoundaryLegendPanel) l.getLegende(this);
      p.init(this);
      l.revalidate();
    }
  }

}