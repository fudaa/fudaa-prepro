/*
 * @creation 1 sept. 06
 * @modification $Date: 2007-02-15 17:10:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.fu.FuLog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.ListSelectionModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostFlecheContentDefaut;
import org.fudaa.fudaa.tr.post.TrPostUserVariableSaver;

class TrPostDataUserVarModel extends CtuluListEditorModel {

  /**
   * 
   */
  private final TrPostDataCreationPanel panel_;

  /**
   * @param _o
   * @param _showNumber
   * @param _panel
   */
  public TrPostDataUserVarModel(final TrPostDataCreationPanel _panel, final Object[] _o, final boolean _showNumber) {
    super(_o, _showNumber);
    panel_ = _panel;
  }

  @Override
  public boolean actionAdd() {
    return createVar(new TrPostDataEditExprPanel(panel_.src_, null));
  }

  private boolean createVar(final TrPostDataEditCommonPanel _pn) {
    if (CtuluDialogPanel.isOkResponse(_pn.afficheModale(panel_.ui_.getParentComponent(), TrResource
        .getS("Ajouter une variable")))) {
      panel_.src_.addUserVar(new H2dVariableTypeCreated[] { _pn.getNewVar() }, new TrPostDataCreated[] { _pn
          .createData() }, panel_.cmd_);
    }
    return false;
  }

  public boolean actionAddCst() {
    return createVar(new TrPostDataEditCstPanel(panel_.src_, null));
  }

  public boolean actionAddFleche() {
    return createVar(new TrPostDataEditFlechePanel(panel_.src_, null));
  }

  public boolean actionAddStatistic() {
    return createVar(new TrPostDataEditStatisticPanel(panel_.src_, panel_.ui_, null));
  }

  @Override
  public void edit(final int _r) {
    final H2dVariableTypeCreated cr = (H2dVariableTypeCreated) v_.get(_r);
    if (!panel_.src_.isUserVarEditable(cr)) { return; }
    TrPostDataEditCommonPanel pn;
    final TrPostDataCreated dataCreated = panel_.src_.getUserCreatedVar(cr);
    if (dataCreated instanceof TrPostDataCreatedExpr) {
      pn = new TrPostDataEditExprPanel(panel_.src_, cr);
    } else if (dataCreated instanceof TrPostDataCreatedStatistic) {
      pn = new TrPostDataEditStatisticPanel(panel_.src_, panel_.ui_, cr);
    } else if (dataCreated instanceof TrPostFlecheContentDefaut) {
      pn = new TrPostDataEditFlechePanel(panel_.src_, cr);

    } else {
      pn = new TrPostDataEditCstPanel(panel_.src_, cr);
    }
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(panel_.ui_.getParentComponent(), CtuluLib
        .getS("Editer une variable")))) {
      if (FuLog.isDebug()) {
        FuLog.debug("TRP: " + panel_.getClass().getName() + " change var= " + pn.isModified());
      }
      if (pn.isModified()) {
        panel_.src_.updateUserValue(cr, pn.getNewVar(), pn.createData(), panel_.cmd_);
      }
    }
  }

  @Override
  public Class getColumnClass(final int _columnIndex) {
    return String.class;
  }

  @Override
  public int getColumnCount() {
    return 2;
  }

  @Override
  public String getColumnName(final int _column) {
    if (_column == 0) { return TrResource.getS("Nom"); }
    return TrResource.getS("Formule");
  }

  public String getFormule(final int _row) {
    final TrPostDataCreated cr = panel_.src_.getUserCreatedVar((H2dVariableType) getValueAt(_row));
    if (cr instanceof TrPostDataCreatedExpr) { return ((TrPostDataCreatedExpr) cr).getFormule(); }
    return CtuluLibString.EMPTY_STRING;

  }

  @Override
  public Object getValueAt(final int _row, final int _col) {
    if (_col == 1) {
      final TrPostDataCreated cr = panel_.src_.getUserCreatedVar(getVariable(_row));
      return cr == null ? CtuluLibString.EMPTY_STRING : cr.getDescription();
    }
    return super.getValueAt(_row, _col);
  }

  public boolean isExpr(final int _row) {
    return panel_.src_.getUserCreatedVar((H2dVariableType) getValueAt(_row)) instanceof TrPostDataCreatedExpr;
  }

  public H2dVariableTypeCreated getVariable(final int _row) {
    return (H2dVariableTypeCreated) getValueAt(_row);
  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    return false;
  }

  @Override
  public void remove(final ListSelectionModel _m) {
    final int min = _m.getMinSelectionIndex();
    int max = _m.getMaxSelectionIndex();
    max = max >= v_.size() ? v_.size() - 1 : max;
    final List r = new ArrayList(max);
    final List otherVarRemoved = new ArrayList();
    // on enleve egalement les variables dependantes de celles enlevees
    for (int i = max; i >= min; i--) {
      if (_m.isSelectedIndex(i)) {
        final H2dVariableType removed = getVariable(i);
        otherVarRemoved.addAll(TrPostDataHelper.getAllVarDependingOn(removed, panel_.src_));
        r.add(removed);
      }
    }
    // des variables dependent des supprim�es
    if (otherVarRemoved.size() > 0) {
      // on annule si l'utilisateur refuse de tout enlever
      if (!panel_.ui_.question(TrResource.getS("Suppression de variables"), TrResource
          .getS("Des variables d�pendantes seront �galement supprim�es. Voulez-vous continuer ?")
          + CtuluLibString.LINE_SEP
          + TrResource.getS("Liste:")
          + TrPostUserVariableSaver.getListString(otherVarRemoved))) { return; }
      r.addAll(otherVarRemoved);
    }
    panel_.src_.removeUserVar((H2dVariableTypeCreated[]) r.toArray(new H2dVariableTypeCreated[r.size()]), panel_.cmd_);
  }

  /**
   * Reconstruit les donn�es.
   */
  public void restore() {
    super.v_.clear();
    v_.addAll(Arrays.asList(panel_.src_.getUserCreatedVar()));
    super.fireTableDataChanged();

  }

}