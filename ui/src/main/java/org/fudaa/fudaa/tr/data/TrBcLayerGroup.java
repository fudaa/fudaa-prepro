/**
 *  @creation     11 sept. 2003
 *  @modification $Date: 2008-01-17 11:39:01 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.data;

import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.fudaa.meshviewer.layer.MvFrontierPointLayer;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * Un groupe de calque pour les conditions limites sur les bords. Il contient un
 * calque pour les conditions limites sur les noeuds, un pour les frontières. 
 * 
 * @author deniger
 * @version $Id: TrBcLayerGroup.java,v 1.15.4.1 2008-01-17 11:39:01 bmarchan Exp $
 */
public class TrBcLayerGroup extends BGroupeCalque {
  public TrBcLayerGroup() {
    setTitle(TrResource.getS("Conditions limites"));
    setDestructible(false);
  }

  public final MvFrontierPointLayer getBcPointLayer() {
    return (MvFrontierPointLayer) getCalqueParNom("cqCL");
  }

  public final TrBcBoundaryLayer getBcBoundaryLayer() {
    return (TrBcBoundaryLayer) getCalqueParNom("cqBord");
  }

  public final MvFrontierPointLayer addBcPointLayer(final MvFrontierPointLayer _c) {
    _c.setName("cqCL");
    _c.setDestructible(false);
    add(_c);
    return _c;
  }

  public final void addBcBoundaryLayer(final TrBcBoundaryLayerAbstract _c) {
    _c.setName(getBoundaryGroupName());
    _c.setDestructible(false);
    add(_c);
  }

  public final static String getBoundaryGroupName() {
    return "cqBord";
  }

  public final void clearSelection() {
    getBcBoundaryLayer().clearSelection();
    getBcPointLayer().clearSelection();
  }
}
