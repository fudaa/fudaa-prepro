/*
 * @creation 18 avr. 2006
 * @modification $Date: 2006-09-19 15:07:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.ProgressionInterface;

/**
 * @author fred deniger
 * @version $Id: TrPostInspectorReader.java,v 1.2 2006-09-19 15:07:27 deniger Exp $
 */
public interface TrPostInspectorReader {

  /**
   * @return true si la lecture a donne quelque chose. Si true la maj de l'interface peut etre effectuee
   */
  int read();

  boolean isPostActivated();

  boolean isPostActivating();

  TrPostProjet getPostActivatedProject();

  void setImpl(TrPostCommonImplementation _impl);

  void close();

  void setProgression(ProgressionInterface _prog);

}
