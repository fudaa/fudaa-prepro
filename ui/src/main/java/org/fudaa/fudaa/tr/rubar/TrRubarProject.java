/*
 * @creation 9 juin 2004
 * @modification $Date: 2007-06-14 12:01:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.*;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluFileChooserFileTester;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.calcul.CalculExecBatch;
import org.fudaa.dodico.calcul.CalculLauncher;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.type.H2dRubarFileType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.rubar.RubarExecAbstract;
import org.fudaa.dodico.rubar.RubarRubarExec;
import org.fudaa.dodico.rubar.RubarRubarSedimentExec;
import org.fudaa.dodico.rubar.RubarVF2MExec;
import org.fudaa.dodico.rubar.io.RubarCLIFileFormat;
import org.fudaa.dodico.rubar.io.RubarDATFileFormat;
import org.fudaa.dodico.rubar.io.RubarTARFileFormat;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.courbe.EGFilleSimple;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.fudaa.commun.FudaaUI;
import org.fudaa.fudaa.commun.calcul.FudaaCalculAction;
import org.fudaa.fudaa.commun.calcul.FudaaCalculOp;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.*;
import org.fudaa.fudaa.tr.data.TrFilleVisu;
import org.fudaa.fudaa.tr.post.TrPostInspector;
import org.fudaa.fudaa.tr.post.TrPostInspectorReaderRubar;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarProject.java,v 1.45 2007-06-14 12:01:42 deniger Exp $
 */

/**
 * @author Fred Deniger
 * @version $Id: TrRubarProject.java,v 1.45 2007-06-14 12:01:42 deniger Exp $
 */
public final class TrRubarProject extends TrProjetCommon {
  protected static class RubarLauncher extends FudaaCalculOp {
    private final File f_;
    final CalculExecBatch[] batch_;
    final TrLauncher launcher_;

    protected RubarLauncher(final CalculExecBatch[] _batch, final File _f, final TrLauncher _l) {
      super(_batch[0]);
      f_ = _f;
      launcher_ = _l;
      batch_ = _batch;
    }

    @Override
    public void execute() {
      if (launcher_ != null) {
        final LaunchOption op = getLaunchOptions(batch_, super.ui_, super.getTaskName());
        if (op == null) {
          return;
        }
        File toFoll = null;
        if (op.isRubarAluv()) {
          super.exe_ = batch_[1];
        }
        if (op.isTPSInspected()) {
          toFoll = new File(CtuluLibFile.getSansExtension(f_) + ".tps");
        } else if (op.isTPCInspected()) {
          toFoll = new File(CtuluLibFile.getSansExtension(f_) + ".tpc");
        }
        if (toFoll != null) {
          // ne pas effacer car peut etre r�utiliser dans le cas d'une suite de calcul
          // if (toFoll.exists()) {
          // toFoll.delete();
          // }
          new TrPostInspector(new TrPostInspectorReaderRubar(toFoll), getTaskName(), launcher_).start();
        }
      }
      super.execute();
    }

    @Override
    public File proceedParamFile(final ProgressionInterface _inter) {
      return f_;
    }
  }

  protected static class RubarVFMLauncher extends FudaaCalculOp {
    private final File f_;

    protected RubarVFMLauncher(final CalculExecBatch _batch, final File _f) {
      super(_batch);
      f_ = _f;
    }

    @Override
    public File proceedParamFile(final ProgressionInterface _inter) {
      return f_;
    }
  }

  public static class LaunchOption extends CtuluDialogPanel {
    final BuRadioButton cbInspectNone_ = new BuRadioButton(getS("Ne pas suivre les fichiers r�sultats"));
    final BuRadioButton cbInspectTpc_ = new BuRadioButton(getS("Suivre en temps r�el le fichier {0}", "TPC"));
    final BuRadioButton cbInspectTps_ = new BuRadioButton(getS("Suivre en temps r�el le fichier {0}", "TPS"));
    final BuCheckBox cbUseVersionAluv_ = new BuCheckBox(getS("Utiliser l'ex�cutable avec S�diment"));

    public LaunchOption(final CalculExecBatch[] _batch) {
      super(false);
      setLayout(new BuVerticalLayout(2));
      final ButtonGroup bg = new ButtonGroup();
      add(cbUseVersionAluv_);
      add(new BuSeparator());
      bg.add((AbstractButton) add(cbInspectNone_));
      bg.add((AbstractButton) add(cbInspectTps_));
      bg.add((AbstractButton) add(cbInspectTpc_));
      cbInspectTpc_.setToolTipText(getTltip("TPC"));
      cbInspectTps_.setToolTipText(getTltip("TPS"));
      cbInspectNone_.setSelected(true);
      if (!_batch[0].isExecFileExists()) {
        cbUseVersionAluv_.setSelected(true);
        cbUseVersionAluv_.setEnabled(false);
      }
      if (!_batch[1].isExecFileExists()) {
        cbUseVersionAluv_.setSelected(false);
        cbUseVersionAluv_.setEnabled(false);
      }
    }

    private String getTltip(final String _file) {
      return getS("Si s�lectionn�, le contenu du fichier de r�sutats {0} sera affich� et mis � jour automatiquement",
          _file);
    }

    @Override
    public String getS(final String _s) {
      return TrResource.getS(_s);
    }

    public String getS(final String _s, final String _v0) {
      return TrResource.getS(_s, _v0);
    }

    public boolean isRubarAluv() {
      return cbUseVersionAluv_.isSelected();
    }

    public boolean isTPCInspected() {
      return cbInspectTpc_.isSelected();
    }

    public boolean isTPSInspected() {
      return cbInspectTps_.isSelected();
    }
  }

  public static LaunchOption getLaunchOptions(final CalculExecBatch[] _batch, final CtuluUI _ui, final String _title) {
    final LaunchOption op = new LaunchOption(_batch);
    return op.afficheModaleOk(_ui.getParentComponent(), _title) ? op : null;
  }

  public static CalculLauncher launchCalcul(final CalculExecBatch _batch, final FudaaUI _ui, final File _f) {
    if (!_batch.isExecFileExists()) {
      _ui.error(_batch.getExecName(), getS("L'ex�cutable '{0}' est introuvable. Pr�ciser le chemin de '{0}' "
          + "dans le panneau des pr�f�rences (Alt+F2)", "rubar")
          + CtuluLibString.ESPACE + getS("Section") + ": Rubar", false);
      return null;
    }
    final FudaaCalculOp op = new RubarVFMLauncher(_batch, _f);
    op.setUi(_ui);
    return op;
  }

  /**
   * @param _batch l'exe a lancer
   * @param _ui l'ui
   * @param _f le fichier de destination
   * @param _impl l'implementation
   * @return le lancer d'appli: null si l'execution ne correspond pas a Rubar20
   */
  public static CalculLauncher launchCalcul(final CalculExecBatch[] _batch, final FudaaUI _ui, final File _f,
                                            final TrLauncher _impl) {
    if (!_batch[0].isExecFileExists() && !_batch[1].isExecFileExists()) {
      _ui.error(_batch[0].getExecName(), getS("L'ex�cutable '{0}' est introuvable. Pr�ciser le chemin de '{0}' "
          + "dans le panneau des pr�f�rences (Alt+F2)", "rubar")
          + CtuluLibString.ESPACE + getS("Section") + ": Rubar", false);
      return null;
    }
    final FudaaCalculOp op = new RubarLauncher(_batch, _f, _impl);
    op.setUi(_ui);
    return op;
  }

  TrRubarFileStateMng fileStates_;
  TrRubarParameters params_;
  BuMenu projectMenu_;
  EbliActionSimple tarageAction_;
  EGFilleSimple tarageFille_;
  EbliActionInterface vf2mAction_;
  /**
   * true if the x,y and index should use the new length ( 12 for coordinates and 9 for indices).
   */
  private boolean newFormatForNumberOfDigits;

  /**
   * @param _p les parametres
   * @param _fileStates le gestionnaire des fichiers
   */
  public TrRubarProject(final TrRubarParameters _p, final TrRubarFileStateMng _fileStates) {
    params_ = _p;
    fileStates_ = _fileStates;
    buildFilleActions();
  }

  public boolean isNewFormatForNumberOfDigits() {
    return newFormatForNumberOfDigits;
  }

  public void setNewFormatForNumberOfDigits(boolean newFormatForNumberOfDigits, boolean fireEvent, CtuluCommandContainer cmd) {
    if (this.newFormatForNumberOfDigits != newFormatForNumberOfDigits) {
      this.newFormatForNumberOfDigits = newFormatForNumberOfDigits;
      params_.getH2dRubarParametres().setXyFormat(newFormatForNumberOfDigits ? H2dRubarFileType.NEW_FORMAT_9_DIGITS : H2dRubarFileType.OLD_FORMAT_6_DIGITS);
      if (fireEvent) {
        fileStates_.fileFormatDigitsChanged();
      }
    }
  }

  @Override
  protected void buildFilleActions() {
    super.buildFilleActions();
    if (vf2mAction_ == null) {
      vf2mAction_ = new EbliActionSimple("vf2m", TrResource.TR.getToolIcon("executer-vf2m"), "LAUNCH_FV2M") {
        @Override
        public void actionPerformed(final ActionEvent _ae) {
          actionVF2MCalcul().execute();
        }
      };
      vf2mAction_.putValue(Action.SHORT_DESCRIPTION, DodicoLib.getS("Ex�cuter {0}", "vf2m"));
    }
    if (tarageAction_ == null) {
      tarageAction_ = new EbliActionSimple(H2dResource.getS("Loi de tarage"), TrResource.TR.getToolIcon("tarage"),
          "TARAGE_CURVES") {
        @Override
        public void actionPerformed(final ActionEvent _ae) {
          showTarageFille();
        }
      };
      tarageAction_.putValue(Action.SHORT_DESCRIPTION, TrResource.getS("Afficher la fen�tre des lois de tarage"));
      tarageAction_.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.ALT_MASK
          + InputEvent.CTRL_MASK));
    }
  }

  @Override
  protected BuInternalFrame buildGeneralFille() {
    return new TrRubarFilleProjet(this);
  }

  @Override
  protected TrFilleVisu buildVisuFille() {
    return new TrFilleVisu(new TrRubarVisuPanel(this));
  }

  protected File chooseFile() {
    final CtuluFileChooserFileTester tester = new CtuluFileChooserFileTester() {
      @Override
      public boolean isFileOk(final File _selectedFile, final CtuluFileChooser _f) {
        if (_selectedFile == null) {
          return false;
        }
        if (_selectedFile.isDirectory()) {
          getImpl().error(getS("Le nouveau nom du projet est vide"));
          return false;
        }
        if (CtuluLibFile.getSansExtension(_selectedFile.getName()).length() > 6) {
          getImpl().error(getS("Le nom du fichier est trop long. Maximum autoris�: 6"));
          return false;
        }
        return true;
      }
    };
    return getImpl().ouvrirFileChooser(getS("Choisir le r�pertoire et le nom du projet (sans extension)"), null, true,
        tester);
  }

  @Override
  protected String[] getLinksToCurvesRelatedDoc() {
    return new String[]{getImpl().buildLink(H2dResource.getS("Les conditions limites"), "rubar-editor-bc"),
        getImpl().buildLink(H2dResource.getS("Les chroniques d'apport"), "rubar-editor-chronique")};
  }

  protected RubarExecAbstract getVF2MExec() {
    return new RubarVF2MExec();
  }

  protected TrRubarVisuPanel getVisuPanel() {
    return visu_ == null ? null : (TrRubarVisuPanel) visu_.getVisuPanel();
  }

  protected boolean isReloadFileSyntheseOk(final CtuluIOOperationSynthese _s) {
    return _s != null && !getImpl().manageErrorOperationAndIsFatal(_s);
  }

  protected void reloadAfterVF2M() {
    new CtuluTaskOperationGUI(null, CtuluLibString.EMPTY_STRING) {
      @Override
      public void act() {
        reloadAfterVF2MAction(getImpl().createProgressionInterface(this));
      }
    }.start();
  }

  protected void reloadAfterVF2MAction(final ProgressionInterface _prog) {
    TrRubarProjectReloader.reloadAfterVF2MAction(_prog, this);
  }

  @Override
  public CalculLauncher actionCalcul() {
    final FudaaCalculOp op = new RubarLauncher(new CalculExecBatch[]{new RubarRubarExec(),
        new RubarRubarSedimentExec()}, getParamsFile(), ((TrCommonImplementation) getImpl()).getLauncher()) {
      @Override
      protected String getTaskName() {
        return getS("lancement de {0}", "Rubar");
      }

      @Override
      public void actionToDoJustAfterLaunching() {
        getImpl().message(super.exe_.getExecTitle(), DodicoLib.getS("Calcul termin�"), false);
      }

      @Override
      public File proceedParamFile(final ProgressionInterface _inter) {
        save(_inter);
        return getParamsFile();
      }
    };
    op.setUi(getImpl());
    return op;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
  }

  /**
   * @return le launcher de vf2m
   */
  public CalculLauncher actionVF2MCalcul() {
    final FudaaCalculOp op = new FudaaCalculOp(getVF2MExec()) {
      @Override
      public void actionToDoJustAfterLaunching() {
        reloadAfterVF2M();
      }

      @Override
      public File proceedParamFile(final ProgressionInterface _inter) {
        save(_inter);
        return getParamsFile();
      }
    };
    op.setUi(getImpl());
    return op;
  }

  // TrRubarFilleProjet fille_;

  @Override
  public void applicationPreferencesChanged() {
  }

  @Override
  public FudaaCalculAction getCalculActions() {
    final FudaaCalculAction r = super.getCalculActions();
    r.setEnableCalcul(fileStates_.isContained(RubarDATFileFormat.getInstance()));
    return r;
  }

  @Override
  public String getCodeExecDir() {
    return null;
  }

  @Override
  public DicoParams getDicoParams() {
    return getH2dRubarParameters().getDicoParams();
  }

  @Override
  public File getDirBase() {
    return params_.getDirBase();
  }

  /**
   * @return l'impl parente
   */
  @Override
  public TrImplementationEditorAbstract getEditorImpl() {
    return params_.getImpl();
  }

  /**
   * @return la frame parente
   */
  public Frame getFrame() {
    return params_.getFrame();
  }

  /**
   * @return l'action de vf2m
   */
  public EbliActionInterface getFV2MAction() {
    return vf2mAction_;
  }

  /**
   * @return les parametres hydrauliques
   */
  public H2dRubarParameters getH2dRubarParameters() {
    return params_.getH2dRubarParametres();
  }

  @Override
  public String getID() {
    return getSoftwareID();
  }

  @Override
  public FudaaCommonImplementation getImpl() {
    return params_.getImpl();
  }

  @Override
  public File getParamsFile() {
    return params_.getPARFile();
  }

  @Override
  public BuMenu getProjectMenu() {
    if (projectMenu_ == null) {
      projectMenu_ = new BuMenu(getS("Projet"), "PROJET");
      projectMenu_.setName("mnProject");
      projectMenu_.setIcon(null);

      projectMenu_.addSeparator();
      projectMenu_.add(vf2mAction_.buildMenuItem(EbliComponentFactory.INSTANCE));
      projectMenu_.addSeparator();
      projectMenu_.add(projectAction_.buildMenuItem(EbliComponentFactory.INSTANCE));
      projectMenu_.add(visuAction_.buildMenuItem(EbliComponentFactory.INSTANCE));
      projectMenu_.add(courbeAction_.buildMenuItem(EbliComponentFactory.INSTANCE));
      projectMenu_.add(tarageAction_.buildMenuItem(EbliComponentFactory.INSTANCE));
    }
    return projectMenu_;
  }

  /**
   * @return le path du projet
   */
  public String getProjectPath() {
    return params_.getProjectPath();
  }

  @Override
  public String getSoftwareID() {
    return TrRubarImplHelper.getID();
  }

  @Override
  public FileFormatSoftware getSystemVersion() {
    return params_.getH2dRubarParametres().getDicoParams().getDicoFileFormatVersion().getSoftVersion();
  }

  /**
   * @return l'action permettant d'afficher la fenetre du maillage
   */
  public EbliActionInterface getTarageFilleAction() {
    return tarageAction_;
  }

  @Override
  public String getTitle() {
    return params_.getTitle();
  }

  @Override
  public TrParametres getTrParams() {
    return params_;
  }

  public TrRubarParameters getTrRubarParams() {
    return params_;
  }

  @Override
  public TrCourbeUseFinder getUsedCourbeFinder(final EGGrapheModel _model) {
    return new TrRubarCourbeUseFinder(getTrRubarParams().getH2dRubarParametres(), getVisuPanel(), _model);
  }

  public boolean isDatLoaded() {
    return fileStates_.contains(RubarDATFileFormat.getInstance()) != null;
  }

  @Override
  public boolean loadAll(final ProgressionInterface _inter) {
    return true;
  }

  @Override
  public boolean save(final ProgressionInterface _prog) {
    if (fileStates_.save(this, getDirBase(), params_.getTitle(), _prog, true)) {
      getState().setParamsModified(false);
      if (TrProjectPersistence.saveProject(getImpl(), this, getParamsFile(), _prog)) {
        getState().setUIModified(false);
      }
      return true;
    }
    return false;
  }

  @Override
  public boolean saveAs(final ProgressionInterface _prog) {
    final File f = chooseFile();
    if (f == null) {
      return false;
    }
    if (f.equals(getParamsFile())) {
      return save(_prog);
    }
    if (getEditorImpl().getLauncher().isAlreadyUsed(f)) {
      errorFileAlreadyUsed(f.getName());
      return saveAs(_prog);
    }
    final File dir = f.getParentFile();
    final String projectName = CtuluLibFile.getSansExtension(f.getName());
    if (fileStates_.save(this, dir, projectName, _prog, true)) {
      getState().setParamsModified(false);
      params_.dir_ = dir;
      params_.title_ = projectName;
      if (TrProjectPersistence.saveProject(getImpl(), this, getParamsFile(), _prog)) {
        getState().setUIModified(false);
      }
      return true;
    }
    return saveAs(_prog);
  }

  private void errorFileAlreadyUsed(final String _f) {
    getImpl().error(CtuluResource.CTULU.getString("Le fichier {0} est d�ja ouvert et ne peut pas �tre �cras�", _f));
  }

  @Override
  public File saveCopy(final ProgressionInterface _prog, final File _f) {
    File f = _f == null ? chooseFile() : _f;
    if (f == null) {
      return null;
    }
    f = CtuluLibFile.changeExtension(f, "par");
    if (getEditorImpl().getLauncher().isAlreadyUsed(f)) {
      errorFileAlreadyUsed(f.getName());
      return _f == null ? saveCopy(_prog, _f) : null;
    }
    if (fileStates_.save(this, f.getParentFile(), CtuluLibFile.getSansExtension(f.getName()), _prog, false)) {
      TrProjectPersistence.saveProject(getImpl(), this, f, _prog);
      return f;
    }
    // si cela se passe mal on recommence
    return saveCopy(_prog, _f);
  }

  @Override
  public void active() {
    super.active();
    importTarIfNeeded();
    importCliIfNeeded();
  }

  public void importTarIfNeeded() {
    final File tarFile = RubarTARFileFormat.getInstance().getFileFor(getDirBase(), CtuluLibFile.getSansExtension(getParamsFile().getName()));
    if (tarFile.exists()) {
      int nbEvol = getTrRubarParams().getTarageMng().getNbEvol();
      boolean isEmptyEvol = nbEvol == 0;
      if (nbEvol == 1) {
        final EvolutionReguliereInterface evol = getTrRubarParams().getTarageMng().getEvol(0);
        isEmptyEvol = evol.getNbValues() == 0;
      }
      if (isEmptyEvol) {
        final String title = TrLib.getString("Importer les courbes de tarage");
        boolean importCurves = getUI().question(
            title,
            TrResource.getS("Il semble que les courbes de tarage du fichier {0} n'ont pas �t� charg�es. Voulez-vous les importer ?", tarFile.getName())
        );
        if (importCurves) {
          new CtuluRunnable(title, getUI()) {
            @Override
            public boolean run(ProgressionInterface progression) {
              final CtuluIOOperationSynthese operation = RubarTARFileFormat.getInstance().readEvolutions(tarFile, progression, null);
              if (operation.containsSevereError()) {
                EventQueue.invokeLater(new Runnable() {
                  @Override
                  public void run() {
                    getImpl().manageAnalyzeAndIsFatal(operation.getAnalyze());
                  }
                });
              } else {
                getTrRubarParams().getTarageMng().importCourbes((EvolutionReguliereInterface[]) operation.getSource(), null, progression);
              }
              return true;
            }
          }.run();
        }
      }
    }
  }

  public void importCliIfNeeded() {
    final File cliFile = RubarCLIFileFormat.getInstance().getFileFor(getDirBase(), CtuluLibFile.getSansExtension(getParamsFile().getName()));
    if (cliFile.exists()) {
      int nbEvol = getTrRubarParams().getEvolMng().getNbEvol();
      boolean isEmptyEvol = nbEvol == 0;
      if (nbEvol == 1) {
        final EvolutionReguliereInterface evol = getTrRubarParams().getTarageMng().getEvol(0);
        isEmptyEvol = evol.getNbValues() == 0;
      }
      if (isEmptyEvol) {
        final String title = TrLib.getString("Importer les courbes temporelles");
        boolean importCurves = getUI().question(
            title,
            TrResource.getS("Il semble que les courbes temporelles du fichier {0} n'ont pas �t� charg�es. Voulez-vous les importer ?", cliFile.getName())
        );
        if (importCurves) {
          new CtuluRunnable(title, getUI()) {
            @Override
            public boolean run(ProgressionInterface progression) {
              final CtuluIOOperationSynthese operation = RubarCLIFileFormat.getInstance().readEvolutions(cliFile, progression, null);
              if (operation.containsSevereError()) {
                EventQueue.invokeLater(new Runnable() {
                  @Override
                  public void run() {
                    getImpl().manageAnalyzeAndIsFatal(operation.getAnalyze());
                  }
                });
              } else {
                getTrRubarParams().getEvolMng().importCourbes((EvolutionReguliereInterface[]) operation.getSource(), null, progression);
              }
              return true;
            }
          }.run();
        }
      }
    }
  }

  public synchronized void showTarageFille() {
    if ((getImpl() == null) || (getTrRubarParams().getTarageMng() == null)) {
      return;
    }
    if (this.tarageFille_ == null) {
      tarageAction_.setEnabled(true);
      tarageFille_ = getTrRubarParams().getTarageMng().createFille(this);
      if (tarageFille_ == null) {
        return;
      }
      tarageFille_.setVisible(true);
      tarageFille_.pack();
      getImpl().addInternalFrame(tarageFille_);
      tarageFille_.getGraphe().restore();
    } else if (tarageFille_.isClosed()) {
      getImpl().addInternalFrame(tarageFille_);
    } else {
      getImpl().activateInternalFrame(tarageFille_);
    }
  }
}
