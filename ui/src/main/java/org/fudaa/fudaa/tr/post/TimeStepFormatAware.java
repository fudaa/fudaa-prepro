package org.fudaa.fudaa.tr.post;

public interface TimeStepFormatAware {
  
  public void timeStepFormatChanged();

}
