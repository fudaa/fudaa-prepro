/*
 * @creation 9 sept. 2005
 *
 * @modification $Date: 2007-06-20 12:23:42 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.export;

import com.memoire.bu.BuFileFilter;
import com.memoire.fu.FuComparator;
import org.apache.commons.lang.ArrayUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.io.corelebth.CorEleBthFileFormat;
import org.fudaa.dodico.h2d.rubar.H2dRubarBcMng;
import org.fudaa.dodico.h2d.rubar.H2dRubarGrid;
import org.fudaa.dodico.h2d.rubar.H2dRubarGridModifier;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.reflux.io.RefluxRefondeSolutionFileFormat;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.commun.impl.FudaaPanelTaskModel;
import org.fudaa.fudaa.meshviewer.MvSelectionNodeOrEltData;
import org.fudaa.fudaa.meshviewer.export.*;
import org.fudaa.fudaa.meshviewer.export.MvExportTaskSkeleton.ExportPanelCustom;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;
import org.fudaa.fudaa.tr.data.TrVisuPanel;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourceRubar;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.reflux.TrRefluxImplHelper;
import org.fudaa.fudaa.tr.rubar.TrRubarVisuPanel;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: TrExportFactory.java,v 1.23 2007-06-20 12:23:42 deniger Exp $
 */
public final class TrExportFactory extends MvExportFactory {
  public static void startExport(String softId, final CtuluUI ui, final EfGridData _src, final H2dVariableType[] _vars,
                                 final MvSelectionNodeOrEltData _selection, final InterpolationVectorContainer _vects, final FSigVisuPanel visuPanel, String additionalText
      , int nbBlockConcentration) {
    MvExportFactory.startExport(new TrExportFactory(ui, softId, _vars, _src, _vects, visuPanel, additionalText, nbBlockConcentration), ui, _selection);
  }

  BuFileFilter fmtSov_;
  final String softId;

  public TrExportFactory(CtuluUI ui, String softId, final H2dVariableType[] _vars, final EfGridData _data, final InterpolationVectorContainer _vects,
                         FSigVisuPanel panel, String additionalText, int nbBlockConcentration) {
    super(ui, CtuluLibSwing.createListModel(_vars), null, _data, _vects, panel, nbBlockConcentration);
    this.softId = softId;
    super.setAdditionalText(additionalText);
  }

  public TrExportFactory(CtuluUI ui, String softId, final ListModel _vars, final FudaaCourbeTimeListModel _time, final EfGridData _datas,
                         final InterpolationVectorContainer _vects, FSigVisuPanel panel, String additionalText, int nbBlockConcentration) {
    super(ui, _vars, _time, _datas, _vects, panel, nbBlockConcentration);
    this.softId = softId;
    super.setAdditionalText(additionalText);
  }

  @Override
  protected BuFileFilter getPreferredFileFilter() {
    if (softId == TrRefluxImplHelper.getID()) {
      return fmtSov_;
    }
    return super.getPreferredFileFilter();
  }

  /**
   * @param _src
   * @param panel panel: peut etre null.
   */
  public TrExportFactory(CtuluUI ui, String softId, final TrPostSource _src, TrVisuPanel panel) {
    super(ui, _src.getNewVarListModel(), _src.getNewTimeListModel(), _src, _src.getInterpolator().getVect(), panel, getNbConcentrationBlocks(_src));
    this.softId = softId;
  }

  public static int getNbConcentrationBlocks(TrPostSource _src) {
    if (_src == null) {
      return 0;
    }
    return getNbConcentrationBlocks(_src.getAllVariablesNonVec());
  }

  public static int getNbConcentrationBlocks(H2dVariableType[] allVariablesNonVec) {
    int nb = 0;
    if (ArrayUtils.contains(allVariablesNonVec, H2dVariableTransType.CONCENTRATION)) {
      nb = 1;
      for (H2dVariableType variableType : allVariablesNonVec) {
        nb = Math.max(nb, H2dRubarBcMng.getIndexBlock(variableType));
      }
    }
    return nb;
  }

  @Override
  protected MvExportPanelFilter buildFilter(final MvSelectionNodeOrEltData _selection, final ListSelectionModel _timeSelection) {
    return TrExportPanelFilterFactory.buildFilter(getUi(), getGrid(), _selection, _timeSelection,
        time_ != null && datas_.isDefined(H2dVariableType.HAUTEUR_EAU), (TrVisuPanel) getPanel());
  }

  protected FudaaPanelTaskModel createRefluxSovTaskModel(final File _initFile, final MvSelectionNodeOrEltData _selection) {
    if (!RefluxRefondeSolutionFileFormat.isOk2D(datas_)) {
      return null;
    }

    final TrExportRefluxSovAct ser = new TrExportRefluxSovAct();
    final TrExportRefluxSovAct.CustomPanel custom = ser.createCustom();
    final MvExportTaskSkeleton skeleton = new MvExportTaskSkeleton(datas_, vects_, ser) {
      @Override
      public File[] getDestFiles() {
        File[] init = super.getDestFiles();
        if (custom.cbCor_.isSelected()) {
          final List l = new ArrayList(Arrays.asList(init));
          final String[] ext = CorEleBthFileFormat.getInstance().getExtensions();
          for (int i = ext.length - 1; i >= 0; i--) {
            l.add(CtuluLibFile.changeExtension(init[0], ext[i]));
          }
          init = (File[]) l.toArray(new File[l.size()]);
        }
        return init;
      }
    };
    final RefluxRefondeSolutionFileFormat fmt = RefluxRefondeSolutionFileFormat.getInstance();
    addSingleFileChooser(skeleton, fmt, _initFile);
    ListSelectionModel listSelection = null;
    if (time_ != null && time_.getSize() > 0) {
      final MvExportTaskSkeleton.ExportPanelVarTimeChooser selection = new MvExportTaskSkeleton.ExportPanelVarTimeChooser(null, time_);
      skeleton.setPnVar(selection);
      listSelection = selection.getTimeSelectionModel();
    }
    skeleton.setPnFilter(buildFilter(_selection, listSelection));
    skeleton.setCustom(new ExportPanelCustom[]{custom});
    return skeleton;
  }

  @Override
  public FudaaPanelTaskModel createExporter(final File _initFile, final BuFileFilter _filter, final MvSelectionNodeOrEltData _selection) {
    FudaaPanelTaskModel res = super.createExporter(_initFile, _filter, _selection);
    if (res == null && _filter != null) {
      if (_filter == fmtSov_) {
        res = createRefluxSovTaskModel(_initFile, _selection);
      }
    }
    H2dRubarGrid rubarGrid = null;
    if (getPanel() instanceof TrRubarVisuPanel) {
      rubarGrid = ((TrRubarVisuPanel) getPanel()).getParams().getGridVolume();
    } else if (getPanel() instanceof TrPostVisuPanel) {
      final TrPostSource source = ((TrPostVisuPanel) getPanel()).getSource();
      if (source instanceof TrPostSourceRubar) {
        rubarGrid = ((TrPostSourceRubar) source).getGrid();
      }
    } else if (datas_ instanceof TrPostSourceRubar) {
      rubarGrid = ((TrPostSourceRubar) datas_).getGrid();
    }
    if (rubarGrid != null) {
      if (res instanceof MvExportRubarTaskSkeleton && (((MvExportRubarTaskSkeleton) res).getActInterface()) instanceof MvExportRubarMAI) {
        MvExportRubarMAI rubarMAI = (MvExportRubarMAI) ((MvExportRubarTaskSkeleton) res).getActInterface();
        rubarMAI.setRubarGridModifier(new H2dRubarGridModifier(rubarGrid));
      }
    }
    return res;
  }

  @Override
  public BuFileFilter[] createFilter() {
    final List r = new ArrayList(Arrays.asList(super.createFilter()));
    if (RefluxRefondeSolutionFileFormat.isOk(datas_)) {
      if (fmtSov_ == null) {
        fmtSov_ = RefluxRefondeSolutionFileFormat.getInstance().createFileFilter();
      }
      r.add(fmtSov_);
    }
    final BuFileFilter[] res = (BuFileFilter[]) r.toArray(new BuFileFilter[r.size()]);
    Arrays.sort(res, FuComparator.STRING_COMPARATOR);
    return res;
  }
}
