package org.fudaa.fudaa.tr.post.persist;

import com.memoire.fu.FuLog;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import org.fudaa.ctulu.CtuluLibFile;

public class TrPostCheckSumGenerator {

  private static byte[] createChecksum(File file) throws Exception {
    MessageDigest complete = MessageDigest.getInstance("MD5");
    InputStream fis = null;
    try {
      fis = new FileInputStream(file);
      byte[] buffer = new byte[1024];
      int numRead;
      do {
        numRead = fis.read(buffer);
        if (numRead > 0) {
          complete.update(buffer, 0, numRead);
        }
      }
      while (numRead != -1);
    } catch (Exception e) {
      FuLog.error(e);
    } finally {
      CtuluLibFile.close(fis);
    }
    return complete.digest();
  }

  public static String getMD5Checksum(File file) {
    String result = "";
    try {
      byte[] b = createChecksum(file);
      for (int i = 0; i < b.length; i++) {
        result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
      }
    } catch (Exception e) {
      FuLog.error(e);
    }
    return result;
  }
}
