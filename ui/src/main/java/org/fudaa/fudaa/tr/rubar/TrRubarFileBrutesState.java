/**
 * @creation 1 oct. 2004
 * @modification $Date: 2006-09-19 15:07:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.dodico.h2d.rubar.H2dRubarDonneesBrutes;
import org.fudaa.dodico.h2d.rubar.H2dRubarDonneesBrutesListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.rubar.H2dRubarVF2MResultInterface;
import org.fudaa.dodico.rubar.io.RubarDonneesBrutesFileFormat;
import org.fudaa.fudaa.commun.FudaaUI;

import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFileBrutesState.java,v 1.14 2006-09-19 15:07:26 deniger Exp $
 */
public final class TrRubarFileBrutesState extends TrRubarFileState implements H2dRubarDonneesBrutesListener {
  protected static void readFile(final File _dir, final String _projectName, final H2dRubarParameters _p,
                                 final RubarDonneesBrutesFileFormat _ft, final String _id, final TrRubarFileStateMng _l, final FudaaUI _ui,
                                 final ProgressionInterface _progress) {
    final File grid = new File(_dir, CtuluLibFile.getFileName(_projectName, _ft.getExtensions()[0]));

    final TrRubarFileBrutesState gridFileState = new TrRubarFileBrutesState(_id, _ft, _l);
    if (grid.exists()) {
      final CtuluIOOperationSynthese s = _ft.read(grid, _progress);
      if (!_ui.manageErrorOperationAndIsFatal(s)) {
        final H2dRubarVF2MResultInterface i = (H2dRubarVF2MResultInterface) s.getSource();
        gridFileState.setInitFile(grid);
        final H2dRubarDonneesBrutes b = _p.createDonneesBrutes().get(_id);
        b.setFirstLine(i.getFirstLine());
        if (i.isParall()) {
          b.initDonneesBrutes(i);
        } else {
          b.initDonneesBrutes(i.getGridData());
        }
        if (!b.isOrganisationEmpty()) {
          _l.add(gridFileState);
        }
      }
    }
    _p.createDonneesBrutes().addListener(gridFileState);
  }

  @Override
  public void fileFormatDigitsChanged() {
    setModified(true);
  }

  String brutesId_;

  private TrRubarFileBrutesState(final String _brutesID, final RubarDonneesBrutesFileFormat _f,
                                 final TrRubarFileStateMng _l) {
    super(_f, _l);
    brutesId_ = _brutesID;
  }

  private void eventModified(final H2dRubarDonneesBrutes _source) {
    if (_source.getID() == brutesId_) {
      final TrRubarFileStateMng mng = (TrRubarFileStateMng) l_;
      if (_source.isOrganisationEmpty()) {
        if (mng.contains(this)) {
          setMarkRemoved(true);
        }
      } else {
        setMarkRemoved(false);
        setModified(true);
        if (mng.contains(this)) {
          l_.fileStateChanged(this);
        } else {
          mng.add(this);
        }
      }
    }
  }

  @Override
  public void donneesBrutesDataChanged(final H2dRubarDonneesBrutes _source) {
    eventModified(_source);
  }

  @Override
  public void donneesBrutesDataNombreChanged(final H2dRubarDonneesBrutes _source) {
    eventModified(_source);
  }

  @Override
  public void donneesBrutesNuageDataChanged(final H2dRubarDonneesBrutes _source) {

  }

  @Override
  public void donneesBrutesNuageDataNombreChanged(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesNuageSupportAddedOrRemoved(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesNuageSupportChanged(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesSupportAddedOrRemoved(final H2dRubarDonneesBrutes _source) {
    eventModified(_source);
  }

  @Override
  public void donneesBrutesSupportChanged(final H2dRubarDonneesBrutes _source) {
    eventModified(_source);
  }

  @Override
  public void donneesBrutesTypeChanged(final H2dRubarDonneesBrutes _source) {
    eventModified(_source);
  }

  @Override
  public CtuluIOOperationSynthese save(final File _dir, final String _projectName,
                                       final ProgressionInterface _progression, final TrRubarProject _projet) {
    final H2dRubarParameters param = _projet.getH2dRubarParameters();
    if (param.getDonneesBrutes() == null) {
      return null;
    }
    final H2dRubarDonneesBrutes db = param.getDonneesBrutes().get(brutesId_);
    if (db == null) {
      return null;
    }
    if (db.getTypeOrganisation() != null) {
      final File dest = new File(_dir, CtuluLibFile.getFileName(_projectName, fmt_.getExtensions()[0]));
      ((RubarDonneesBrutesFileFormat) fmt_).setNewFormat(_projet.isNewFormatForNumberOfDigits());
      return ((FileFormatUnique) fmt_).write(dest, db.getFileModel(), _progression);
    }
    return null;
  }
}
