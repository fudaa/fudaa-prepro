/**
 * @creation 1999-08-10
 * @modification $Date: 2007-06-05 09:01:14 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuResource;
import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.operation.EfIndexHelper;
import org.fudaa.dodico.ef.operation.EfIsoRestructuredGridActivity;
import org.fudaa.dodico.ef.operation.EfIsoRestructuredGridResult;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueCacheManager;
import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesConfigure;
import org.fudaa.ebli.calque.ZCalquePolygone;
import org.fudaa.ebli.calque.ZCalqueSondeInterface;
import org.fudaa.ebli.calque.ZSelectionTrace;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.controle.BSelecteurListTarget;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageAbstract;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.ebli.palette.BPalettePlageProperties;
import org.fudaa.ebli.palette.PaletteManager;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIsoInterface;
import org.fudaa.ebli.trace.TraceIsoLignesAvecPlages;
import org.fudaa.ebli.trace.TraceIsoSurfacesAvecPlages;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetVueCalque;
import org.fudaa.fudaa.meshviewer.layer.MvIsoModelInterface;
import org.fudaa.fudaa.meshviewer.layer.MvNodeLayer;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.persistence.TrPostIsoLayerPersistence;
import org.fudaa.fudaa.tr.post.data.TrPostDataListener;

/**
 * Un calque de trace de cartes avec un nouvel algorithme de trace.
 *
 * @author Bertrand Marchand
 * @version $Id: TrIsoLayerDefault.java,v 1.48 2007-06-05 09:01:14 deniger Exp $
 */
public class TrIsoLayerDefault extends MvNodeLayer implements ListSelectionListener, BSelecteurListTarget, ZCalqueSondeInterface, TrPostDataListener,
    TimeStepFormatAware {
  static int nameIdx;

  public static TraceIcon getSondeIcone() {
    final TraceIcon r = new TraceIcon(TraceIcon.RIEN, 6) {
      @Override
      public void paintIconCentre(final Component _c, final Graphics _g, final int _x, final int _y) {
        if (_g == null) {
          return;
        }
        Color old = null;
        if (getCouleur() != null) {
          old = _g.getColor();
          _g.setColor(getCouleur());
        }
        final int taille = super.getTaille();
        final int demiTaille = taille / 2;
        _g.drawLine(_x - demiTaille, _y, _x - taille, _y + 1);
        _g.drawLine(_x - demiTaille, _y, _x - taille, _y - 1);
        _g.drawLine(_x + demiTaille, _y, _x + taille, _y + 1);
        _g.drawLine(_x + demiTaille, _y, _x + taille, _y - 1);
        _g.drawLine(_x, _y - demiTaille, _x + 1, _y - taille);
        _g.drawLine(_x, _y - demiTaille, _x - 1, _y - taille);
        _g.drawLine(_x, _y + demiTaille, _x + 1, _y + taille);
        _g.drawLine(_x, _y + demiTaille, _x - 1, _y + taille);
        _g.drawLine(_x, _y - taille, _x, _y + taille);
        _g.drawLine(_x - taille, _y, _x + taille, _y);
        if (super.getCouleur() != null) {
          _g.setColor(old);
        }
      }
    };
    r.setCouleur(Color.BLACK);
    return r;
  }

  public static boolean restorePalette(final EbliUIProperties _p, final Map _dest) {
    final String[] names = (String[]) _p.get("post.paletteNames");
    if (names != null && names.length > 0) {
      final BPalettePlageProperties[] props = (BPalettePlageProperties[]) _p.get("post.paletteProps");
      if (_dest.size() > 0) {
        _dest.clear();
      }
      for (int i = 0; i < names.length; i++) {
        if (props[i] != null && names[i] != null) {
          BPalettePlage plage = new BPalettePlage(props[i]);
          initSubTitleLabel(plage);
          _dest.put(names[i], plage);
        }
      }
      return true;
    }
    return false;
  }

  public static void restoreVar(final EbliUIProperties _p, final BSelecteurListTarget _target, final String _defaultId) {
    if (_p == null || _target == null) {
      return;
    }
    final int size = _target.getListModel().getSize();
    if (size == 0) {
      return;
    }
    String varId = _p.getString("post.varId");
    if (varId == null) {
      varId = _defaultId;
    }
    if (varId != null) {
      int idx = TrIsoModelAbstract.getVarIdx(varId, _target.getListModel());

      if (idx < 0 || idx >= size) {
        idx = size - 1;
      }
      if (idx >= 0) {
        _target.getListSelectionModel().setSelectionInterval(idx, idx);
      }
    }
  }

  public static EbliUIProperties savePalettes(final Map _nameBPalette, final EbliUIProperties _prop) {
    if (_prop == null) {
      return null;
    }
    if (_nameBPalette == null || _nameBPalette.size() == 0) {
      return _prop;
    }
    final List paletteNames = new ArrayList(_nameBPalette.size());
    final List palettes = new ArrayList(_nameBPalette.size());
    for (final Iterator it = _nameBPalette.entrySet().iterator(); it.hasNext(); ) {
      final Map.Entry e = (Map.Entry) it.next();
      final String key = (String) e.getKey();
      if (!CtuluLibString.isEmpty(key)) {

        paletteNames.add(key);
        palettes.add(((BPalettePlage) e.getValue()).save());
      }
    }
    _prop.put("post.paletteNames", paletteNames.toArray(new String[paletteNames.size()]));
    _prop.put("post.paletteProps", palettes.toArray(new BPalettePlageProperties[palettes.size()]));
    return _prop;
  }

  public static EbliUIProperties saveVar(final EbliUIProperties _prop, final BSelecteurListTarget _list) {
    if (_prop == null) {
      return null;
    }
    if (_list == null || _list.getListSelectionModel().isSelectionEmpty()) {
      return _prop;
    }
    Object oi = getCurrentSelection(_list);

    H2dVariableType variableSelected = null;
    if (oi instanceof H2dVariableType) {
      variableSelected = (H2dVariableType) oi;
    } else if (oi instanceof TrPostFlecheContent) {
      variableSelected = ((TrPostFlecheContent) oi).getVar();
    }
    if (variableSelected != null) {
      final String varName = variableSelected.getID();
      if (varName != null) {
        _prop.put("post.varId", varName);
      }
    }
    return _prop;
  }

  protected static Object getCurrentSelection(final BSelecteurListTarget _list) {
    final int currentSelection = _list.getListSelectionModel().getMaxSelectionIndex();
    final Object oi = _list.getListModel().getElementAt(currentSelection);
    return oi;
  }

  public static int selectionElement(final GrPoint _pt, final int _tolerance, final GrMorphisme _versReel,
                                     final MvIsoModelInterface _model,
                                     final GrBoite _clipReel) {
    GrBoite bClip = _model.getDomaine();
    final double distanceReel = GrMorphisme.convertDistanceXY(_versReel, _tolerance);
    if ((!bClip.contientXY(_pt)) && (bClip.distanceXY(_pt) > distanceReel)) {
      return -1;
    }
    bClip = _clipReel;
    final GrPolygone poly = new GrPolygone();
    int r = -1;
    for (int i = _model.getNbElt() - 1; i >= 0; i--) {
      _model.polygone(poly, i, true);
      if (bClip.intersectXY(poly.boite())) {
        if (poly.contientXY(_pt)) {
          return i;
        } else if (poly.distanceXY(_pt) <= distanceReel) {
          r = i;
        }
      }
    }
    return r;
  }

  /**
   * Methode qui permet de savoir si le point appartient a la sonde
   *
   * @param _prReel
   * @param _model
   */
  public static int sondeSelection(final GrPoint _prReel, final MvIsoModelInterface _model) {
    final EfGridInterface grid = _model.getGrid();
    if (grid.getIndex() != null) {
      return EfIndexHelper.getElementEnglobant(grid, _prReel.x_, _prReel.y_, null);
    }
    return sondeSelectionLong(_prReel, _model);
  }

  /**
   * Recherche l'�l�ment qui peut contenir la sonde pour bien initialiser la var.
   *
   * @param _prReel
   * @param grid
   */
  public static int sondeSelection(final GrPoint _prReel, final EfGridInterface grid) {
    if (grid.getIndex() != null) {
      return EfIndexHelper.getElementEnglobant(grid, _prReel.x_, _prReel.y_, null);
    }
    return -1;
  }

  private static int sondeSelectionLong(final GrPoint _prReel, final MvIsoModelInterface _model) {
    final GrPolygone poly = new GrPolygone();
    for (int i = _model.getNbElt() - 1; i >= 0; i--) {
      _model.polygone(poly, i, true);
      if (poly.contientXY(_prReel)) {
        return i;
      }
    }
    return -1;
  }

  private boolean sonde_;
  ListSelectionModel mainVariableSelectionModel_;
  Map namePalette_;
  boolean oldDataTypeIsElement_;
  TraceIcon sondeIcone_;
  List<GrPoint> sondePt_;
  int sondeSelectedElement_ = -1;
  JMenuItem[] spec_;
  double[] v_;

  /**
   * Contructeur du calque.
   *
   * @param _m le modele de donnees
   */
  public TrIsoLayerDefault(final TrIsoModelAbstract _m) {
    super(_m);
    paletteCouleur_ = new BPalettePlage();
    // on choisit la hauteur d'eau
    int i = _m.getIndexOf(H2dVariableType.HAUTEUR_EAU);
    if (i < 0) {
      i = 0;
    }
    setV(i);
    sondePt_ = new ArrayList<GrPoint>();
    // Test de perf
    BCalqueCacheManager.installDefaultCacheManager(this);
  }

  public final TrPostSource getSource() {
    return getIsoModelAbstract().getSource();
  }

  void initPaletteMap() {
    if (namePalette_ == null) {
      namePalette_ = new HashMap(getIsoModelAbstract().getVariableNb() + 1);
    }
  }

  protected TrIsoLayerDefault buildLayer() {
    return new TrIsoLayerDefault(getIsoModelAbstract());
  }

  @Override
  protected void buildSpecificMenuItemsForAction(final List _l) {
    _l.add(createDuplicateItem());
  }

  protected BPalettePlage createPlageForSelectedVar(final H2dVariableType _t) {
    if (_t == null) {
      return null;
    }
    BPalettePlage s = new BPalettePlage();
    initSubTitleLabel(s);
    final TrIsoModelAbstract model = getIsoModelAbstract();
    if (_t.getParentVariable() == H2dVariableType.TEMPS) {
      s = new TrPostPaletteTime(model.s_);
    }
    s.setTitre(model.getVarDescriptionForPalette(_t));
    final CtuluRange r = getRangeForPalette();
    if (r.max_ - r.min_ < 0.01) {
      s.initPlages(1, r.min_, r.max_);
    } else {
      s.initPlages(10, r.min_, r.max_);
    }
    s.initCouleurs(PaletteManager.INSTANCE);
    updateSavedPalBeforeSet(s);
    return s;
  }

  /**
   * Initialize the palette with the subtitle Time.
   *
   * @param s
   */
  public static void initSubTitleLabel(BPalettePlageAbstract s) {
    s.setSubTitleLabel(TrLib.getString("Temps"));
  }

  public final TrIsoModelAbstract getIsoModelAbstract() {
    return (TrIsoModelAbstract) modele_;
  }

  protected CtuluRange getRangeForPalette() {
    return getIsoModelAbstract().getPaletteExtremaForSelectedValue();
  }

  protected final void paintSonde(final Graphics2D _g2d, final GrMorphisme _versEcran) {
    if (sondeSelectedElement_ >= 0 && _g2d != null) {
      GrPoint oldPoint = null;
      for (final GrPoint point : sondePt_) {

        final GrPoint p = point.applique(_versEcran);
        if (sondeIcone_ == null) {
          sondeIcone_ = getSondeIcone();
        }
        // _g2d.setXORMode(Color.WHITE);
        sondeIcone_.paintIconCentre(this, _g2d, p.x_, p.y_);

        // -- on trace la ligne entre les sondes --//
        if (oldPoint != null) {
          _g2d.drawLine((int) oldPoint.x_, (int) oldPoint.y_, (int) p.x_, (int) p.y_);
        }
        oldPoint = p;
      }
    }
  }

  @Override
  public void timeStepFormatChanged() {
    if (getIsoModelAbstract().getVariableSelected() != null
        && getIsoModelAbstract().getVariableSelected().getParentVariable() == H2dVariableType.TEMPS) {
      updateTimeLegende();
      updateLegende();
    } else {
      firePropertyChange("paletteCouleur", null, paletteCouleur_);
    }
  }

  protected void updateCalqueInfo() {
    final String var = getIsoModelAbstract().getVariableNameSelected();
    putCalqueInfo(var);
  }

  protected void updateLegende() {
    updateLegende(false);
  }

  protected void updateTimeLegende() {
    for (final Iterator it = namePalette_.values().iterator(); it.hasNext(); ) {
      final BPalettePlage p = (BPalettePlage) it.next();
      if (p instanceof TrPostPaletteTime) {
        p.updatePlageLegendes();
      }
    }
  }

  protected void updateLegende(final boolean _forceUpdate) {
    if (getIsoModelAbstract().getVariableNb() == 0 || getListSelectionModel().isSelectionEmpty()) {
      return;
    }
    initPaletteMap();
    final String v = getIsoModelAbstract().getCompleteVariableNameId();
    BPalettePlage s = (BPalettePlage) namePalette_.get(v);
    if (s == null || _forceUpdate) {
      final H2dVariableType t = getIsoModelAbstract().getVariableSelected();
      s = createPlageForSelectedVar(t);
      namePalette_.put(v, s);
    } else {
      updateSavedPalBeforeSet(s);
    }
    setPaletteCouleur(s);
    construitLegende();
  }

  protected void updateSavedPalBeforeSet(final BPalettePlage _p) {
  }

  @Override
  public final boolean changeSonde(final GrPoint _ptReel, final boolean _add) {
    if (!isSondeEnable()) {
      return false;
    }
    if (sondePt_ == null) {
      // sondePt_ = new GrPoint();
      sondePt_ = new ArrayList<GrPoint>();
    }

    final GrPoint point = new GrPoint();

    point.initialiseAvec(_ptReel);

    if (!_add) {
      sondePt_.clear();
    }
    sondePt_.add(point);

    final int i = sondeSelection(point, getIsoModelAbstract());
    final boolean oldIsDraw = sondeSelectedElement_ >= 0;
    sondeSelectedElement_ = i;
    if (sondeSelectedElement_ >= 0) {
      paintSonde((Graphics2D) getGraphics(), getVersEcran());
    } else if (oldIsDraw) {
      repaint(0);
    }
    // pour le panel d'info
    fireSelectionEvent();
    return sondeSelectedElement_ >= 0;
  }

  public JMenuItem createDuplicateItem() {
    final BuMenuItem it = new BuMenuItem(BuResource.BU.getIcon("dupliquer"), BuResource.BU.getString("dupliquer")) {
      @Override
      protected void fireActionPerformed(final ActionEvent _event) {
        super.fireActionPerformed(_event);
        final BCalque c = (BCalque) TrIsoLayerDefault.this.getParent();
        final Component[] cs = c.getCalques();
        int i = -1;
        for (i = 0; i < cs.length; i++) {
          if (TrIsoLayerDefault.this.equals(cs[i])) {
            break;
          }
        }
        if (i < 0 || i == (cs.length - 1)) {
          c.enDernier(duplicate());
        } else if (i == 0) {
          c.enPremier(duplicate());
        } else {
          c.add(duplicate(), i + 1);
        }
      }
    };
    return it;
  }

  @Override
  public BPalettePlageInterface createPaletteCouleur() {
    if (getIsoModelAbstract().getVariableSelected() != null
        && getIsoModelAbstract().getVariableSelected().getParentVariable() == H2dVariableType.TEMPS) {
      return new TrPostPaletteTime(getIsoModelAbstract().s_);
    }
    return super.createPaletteCouleur();
  }

  @Override
  public void dataAdded(final boolean _isFleche) {
    H2dVariableType currentVar = getIsoModelAbstract().var_;
    //we must update the selection
    if (currentVar != getCurrentSelection(this)) {
      int idx = TrIsoModelAbstract.getVarIdx(currentVar.getID(), getListModel());
      if (idx < 0) {
        idx = 0;
      }
      if (idx >= 0) {
        getListSelectionModel().setSelectionInterval(idx, idx);
      }
    }
  }

  @Override
  public void dataChanged(final H2dVariableType _old, final H2dVariableType _new, final boolean _contentChanged,
                          final boolean _isFleche,
                          final Set _varDepending) {
    if (_isFleche) {
      return;
    }
    if (_old == getIsoModelAbstract().var_ || _varDepending.contains(getIsoModelAbstract().var_)) {
      // la variable en cours d'affichage est modifi�e
      if (_old != _new && _old == getIsoModelAbstract().var_) {
        getListSelectionModel().clearSelection();
        setV(getIsoModelAbstract().getIndexOf(_new));
      } // si le contenu de la variable en cours d'affichage est concern�, on met a jour le cache
      else {
        getIsoModelAbstract().updateCurrentValueCache();
        fireSelectionEvent();
        repaint();
      }
      firePropertyChange(EbliWidgetVueCalque.CLEAR_CACHE, Boolean.TRUE, Boolean.FALSE);
    }
  }

  @Override
  public void dataRemoved(final H2dVariableType[] _vars, final boolean _isFleche) {
    // si la variable en cours a ete supprimee, on deselectionne l'affichage....
    if (FuLog.isDebug()) {
      FuLog.debug("TRP: " + getClass().getName() + " var removed");
    }
    if (getIsoModelAbstract().var_ == null) {
      return;
    }
    if (_vars != null && CtuluLibArray.findObject(_vars, getIsoModelAbstract().var_) >= 0) {
      if (FuLog.isDebug()) {
        FuLog.debug("TRP: " + getClass().getName() + " var was used ...");
      }
      mainVariableSelectionModel_.clearSelection();
      firePropertyChange(EbliWidgetVueCalque.CLEAR_CACHE, Boolean.TRUE, Boolean.FALSE);
    }
  }

  public final ZCalqueAffichageDonnees duplicate() {
    final TrIsoLayerDefault r = buildLayer();
    r.setName(getName() + "-" + nameIdx++);
    r.initFrom(saveUIProperties());
    r.setTitle(r.getTitle() + CtuluLibString.ESPACE + CtuluLib.getS("copie"));
    r.setLegende(getLegende());
    r.setActions(getActions());
    r.setDestructible(true);
    return r;
  }

  @Override
  public void fillWithInfo(final InfoData _m) {
    _m.setTitle(getTitle());
    if (isSondeActive()) {
      fillWithInterpolateInfo(_m);
    } else {
      modele_.fillWithInfo(_m, this);
    }
  }

  @Override
  public void fillWithInterpolateInfo(final InfoData _m) {
    final int i = sondePt_.size() - 1;
    getIsoModelAbstract().fillInterpolateInfo(_m, sondeSelectedElement_, sondePt_.get(i).x_, sondePt_.get(i).y_, getTitle());
  }

  @Override
  public final String getDataDescription() {
    return getTitle();
  }

  @Override
  public final int getElementSonde() {
    return sondeSelectedElement_;
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return getIsoModelAbstract().getExpressionContainer();
  }

  @Override
  public final ListModel getListModel() {
    return getIsoModelAbstract().getVariableList();
  }

  @Override
  public final ListSelectionModel getListSelectionModel() {
    if (mainVariableSelectionModel_ == null) {
      mainVariableSelectionModel_ = new DefaultListSelectionModel();
      mainVariableSelectionModel_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      final H2dVariableType t = getIsoModelAbstract().getVariableSelected();
      if (t != null) {
        for (int i = getListModel().getSize() - 1; i >= 0; i--) {
          if (getListModel().getElementAt(i) == t) {
            mainVariableSelectionModel_.setSelectionInterval(i, i);
            break;
          }
        }
      }
      mainVariableSelectionModel_.addListSelectionListener(this);
    }
    return mainVariableSelectionModel_;
  }

  @Override
  public int getNbSet() {
    return 1;
  }

  @Override
  public BCalquePersistenceInterface getPersistenceMng() {
    return new TrPostIsoLayerPersistence();
  }

  @Override
  public int[] getSelectedElementIdx() {
    if (getIsoModelAbstract().isCurrentDataElementType()) {
      return getSelectedIndex();
    }
    return null;
  }

  @Override
  public LineString getSelectedLine() {
    if (getIsoModelAbstract().isCurrentDataElementType()) {
      int[] twoFirstSelected = super.getTwoFirstSelected();
      if (twoFirstSelected.length == 2) {
        return ZCalquePolygone.getSelectedLine(twoFirstSelected[0], twoFirstSelected[1], getIsoModelAbstract());
      }
      return ZCalquePolygone.getSelectedLine(this, getIsoModelAbstract());
    }
    return super.getSelectedLine();
  }

  @Override
  public int[] getSelectedObjectInTable() {
    return getSelectedIndex();
  }

  @Override
  public int[] getSelectedPtIdx() {
    if (getIsoModelAbstract().isCurrentDataElementType()) {
      return null;
    }
    return super.getSelectedPtIdx();
  }

  @Override
  protected BConfigurableInterface getAffichageConf() {
    final BConfigurableInterface[] sect = new BConfigurableInterface[2];
    final ZCalqueAffichageDonneesConfigure calqueAffichageDonneesConfigure = new ZCalqueAffichageDonneesConfigure(this);
    calqueAffichageDonneesConfigure.setAddColor(false);
    sect[0] = calqueAffichageDonneesConfigure;
    sect[1] = new TrIsoLayerConfigure(this);

    return new BConfigurableComposite(sect, EbliLib.getS("Affichage"));
  }

  @Override
  public BConfigurableInterface getSingleConfigureInterface() {
    return new BConfigurableComposite(new BConfigurableInterface[]{getAffichageConf(),
        new TrPostFilterConfigure(getIsoModelAbstract().s_, this,
            getIsoModelAbstract().getCond())},
        null);
  }

  @Override
  public final double getSondeX() {
    return sondePt_ == null ? 0 : sondePt_.get(sondePt_.size() - 1).x_;
  }

  @Override
  public final double getSondeY() {
    return sondePt_ == null ? 0 : sondePt_.get(sondePt_.size() - 1).y_;
  }

  public final JComponent getTargetComponent() {
    return this;
  }

  @Override
  public GrBoite getDomaineOnSelected() {
    if (isSelectionEmpty()) {
      return null;
    }
    int m = selection_.getMaxIndex();
    if (m > modele_.getNombre()) {
      m = modele_.getNombre() - 1;
    }
    final GrBoite r = new GrBoite();
    if (getIsoModelAbstract().isCurrentDataElementType()) {
      final GrPolygone p = new GrPolygone();
      final GrBoite b = new GrBoite();
      for (int i = selection_.getMinIndex(); i <= m; i++) {
        if (selection_.isSelected(i)) {
          getIsoModelAbstract().polygone(p, i, true);
          p.boite(b);
          r.ajuste(b);
        }
      }
    } else {
      final GrPoint p = new GrPoint();
      for (int i = selection_.getMinIndex(); i <= m; i++) {
        if (selection_.isSelected(i)) {
          modele_.point(p, i, true);
          r.ajuste(p);
        }
      }
    }
    ajusteZoomOnSelected(r);
    return r;
  }

  @Override
  public void initFrom(final EbliUIProperties _p) {
    if (_p != null) {
      getListSelectionModel().clearSelection();
      super.initFrom(_p);
      initPaletteMap();
      if (restorePalette(_p, namePalette_)) {
        updateLegende();
      }
      restoreVar(_p, this, H2dVariableType.BATHYMETRIE.getID());
      getIsoModelAbstract().restoreFilter(_p);
      traceIsoLine_ = _p.getBoolean("calque.draw.iso");
    }
  }

  @Override
  public boolean isAntialiasSupported() {
    return true;
  }

  public boolean isBase() {
    return !isDestructible();
  }

  @Override
  public boolean isConfigurable() {
    return true;
  }

  @Override
  public final boolean isDonneesBoiteAvailable() {
    return true;
  }

  @Override
  public boolean isPointSondable(final GrPoint _reel) {
    return sondeSelection(_reel, getIsoModelAbstract()) >= 0;
  }

  @Override
  public boolean isSelectionElementEmpty() {
    if (getIsoModelAbstract().isCurrentDataElementType()) {
      return isSelectionEmpty();
    }
    return true;
  }

  @Override
  public boolean isSelectionPointEmpty() {
    return getIsoModelAbstract().isCurrentDataElementType() ? true : super.isSelectionPointEmpty();
  }

  @Override
  public final boolean isSondeActive() {
    return isSondeEnable() && (sondeSelectedElement_ >= 0 || (sondePt_ != null && !sondePt_.isEmpty()));
  }

  /**
   * @return true si la sonde est active
   */
  @Override
  public final boolean isSondeEnable() {
    return sonde_;
  }

  @Override
  public boolean isTitleModifiable() {
    return true;
  }

  boolean traceIsoLine_;
  TrIsoModelEltDataAdapter eltAdapter_;

  private boolean isLineAllPaintedByNode(int minX, int maxX, int minY, int maxY, int[] x, int[] y, int nbPt) {
    final int deltaX = maxX - minX;
    final int deltaY = maxY - minY;
    if (deltaX <= 1 && deltaY <= 1) {
      return true;
    }
    if (deltaX == 0 && deltaY < nbPt) {
      boolean[] dots = new boolean[deltaY + 1];
      for (int i = 0; i < nbPt; i++) {
        dots[y[i] - minY] = true;
      }
      return isAllTrue(dots);
    }
    if (deltaX < nbPt && deltaY == 0) {
      boolean[] dots = new boolean[deltaX + 1];
      for (int i = 0; i < nbPt; i++) {
        dots[x[i] - minX] = true;
      }
      return isAllTrue(dots);
    }
    return false;
  }

  private boolean isAllTrue(boolean[] dots) {
    for (int i = 0; i < dots.length; i++) {
      if (!dots[i]) {
        return false;
      }
    }
    return true;
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel, final GrBoite _clipReel) {

    if (namePalette_ == null) {
      updateLegende();
    }
    final boolean rapide = isRapide();
    if (rapide) {
      super.paintDonnees(_g, getIsoModelAbstract().getNbPoint(), _versEcran, _clipReel);
      return;
    }
    int idxElt;
    TraceIsoInterface isos = null;
    final boolean isElement = getIsoModelAbstract().isCurrentDataElementType();
    if (getIsoModelAbstract().getVariableSelected() == null) {
      return;
    }
    MvIsoModelInterface modelToUse = getIsoModelAbstract();
    if (traceIsoLine_) {
      isos = new TraceIsoLignesAvecPlages(paletteCouleur_, alpha_, ligneModel_);
      if (isElement) {
        // TODO:a modifier null?
        if (eltAdapter_ == null) {
          final EfIsoRestructuredGridResult res = new EfIsoRestructuredGridActivity().restructure(getIsoModelAbstract().getGrid(),
              null,
              new CtuluAnalyze());
          eltAdapter_ = new TrIsoModelEltDataAdapter(res, getIsoModelAbstract());
        }
        updateEltAdapter();
        modelToUse = eltAdapter_;
      }
    } else if (!isElement) {
      isos = new TraceIsoSurfacesAvecPlages(paletteCouleur_, alpha_);
    }
    if (isos != null) {
      isos.setRapide(isRapide());
      isos.setClipEcran(_clipReel.applique(_versEcran));
    }

    // pour les surfaces, on voudrait eviter d'utiliser l'anticrenelage
    final RenderingHints renderingHints = _g.getRenderingHints();
    renderingHints.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
    renderingHints.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
    _g.setRenderingHints(renderingHints);

    final int n = modelToUse.getNbElt();
    int[] x = null;
    int[] y = null;
    GrPoint pt = new GrPoint();
    int w = getWidth() + 1;
    CtuluListSelection memory = new CtuluListSelection(w * getHeight());
    if (isos != null) {
      isos.setDimension(getWidth(), getHeight(), memory);
    }
    Envelope envReel = _clipReel.getEnv();
    for (idxElt = 0; idxElt < n; idxElt++) {
      if (!modelToUse.isPainted(idxElt)) {
        continue;
      }

      final int nbPt = modelToUse.getNbPt(idxElt);
      Envelope envelopeElement = modelToUse.getEnvelopeForElement(idxElt);

      if (!envReel.intersects(envelopeElement)) {
        continue;
      }
      if (x == null || x.length < nbPt) {
        x = new int[nbPt];
      }
      if (y == null || y.length < nbPt) {
        y = new int[nbPt];
      }
      int maxX = -1;
      int minX = Integer.MAX_VALUE;
      int maxY = -1;
      int minY = Integer.MAX_VALUE;
      for (int iPt = 0; iPt < nbPt; iPt++) {
        pt.x_ = modelToUse.getX(idxElt, iPt);
        pt.y_ = modelToUse.getY(idxElt, iPt);
        pt.z_ = 0;
        pt.autoApplique2D(_versEcran);
        final int xi = (int) pt.x_;
        final int yi = (int) pt.y_;
        x[iPt] = xi;
        y[iPt] = yi;
        maxX = Math.max(maxX, xi);
        maxY = Math.max(maxY, yi);
        minX = Math.min(minX, xi);
        minY = Math.min(minY, yi);
      }
      final int deltaX = maxX - minX;
      final int deltaY = maxY - minY;
      boolean isOnlyOnePoint = deltaX == 0 && deltaY == 0;
      // Trace des isosurfaces / Isolignes (dans la couleur du fond si les isocouleurs sont
      // tracees).
      if (isElement && !traceIsoLine_) {
        final Polygon p = new Polygon(x, y, nbPt);
        // on utilise getIsoModelAbstract car on est bien dans le cas ou il n'y a pas d'adapteur
        final Color c = paletteCouleur_ == null ? null : ((BPalettePlage) paletteCouleur_).getColorFor(getIsoModelAbstract().getValue(
            idxElt));
        if (c != null) {
          _g.setColor(rapide ? c : EbliLib.getAlphaColor(c, alpha_));
          _g.fillPolygon(p);
        }
      } else if (isos != null) {
        if (isOnlyOnePoint) {
          int idxDone = x[0] + y[0] * w;
          if (idxDone >= 0 && !memory.isSelected(idxDone)) {
            memory.add(idxDone);
            double value = modelToUse.getDatatFor(idxElt, 0);
            final Color c = paletteCouleur_ == null ? null : ((BPalettePlage) paletteCouleur_).getColorFor(value);
            if (c != null) {
              _g.setColor(rapide ? c : EbliLib.getAlphaColor(c, alpha_));
              _g.drawLine(x[0], y[0], x[0], y[0]);
            }
          }
        } else if (isLineAllPaintedByNode(minX, maxX, minY, maxY, x, y, nbPt)) {
          boolean painted = false;
          if (deltaX == 0 || deltaY == 0) {
            double valueToUse = modelToUse.getDatatFor(idxElt, 0);
            boolean constant = true;
            //pour les points sur 2 pixels on ne fait pas le tests pour savoir si meme valeur.
            if (deltaX >= 1 || deltaY >= 1) {
              for (int iPt = 1; iPt < nbPt; iPt++) {
                double val = modelToUse.getDatatFor(idxElt, iPt);
                if (constant) {
                  constant = CtuluLib.isEquals(val, valueToUse, 1e-10);
                  if (!constant) {
                    break;
                  }
                }
              }
            }
            if (constant) {
              painted = true;
              final Color color = paletteCouleur_ == null ? null : ((BPalettePlage) paletteCouleur_).getColorFor(
                  valueToUse);
              final Color c = rapide ? color : EbliLib.getAlphaColor(color, alpha_);
              if (c != null) {
                _g.setColor(c);
                _g.drawLine(minX, minY, maxX, maxY);
              }
              for (int iPt = 0; iPt < nbPt; iPt++) {
                int idxDone = x[iPt] + y[iPt] * w;
                if (idxDone >= 0) {
                  memory.add(idxDone);
                }
              }
            }
          }
          if (!painted) {
            for (int iPt = 0; iPt < nbPt; iPt++) {
              int idxDone = x[iPt] + y[iPt] * w;
              if (idxDone >= 0 && !memory.isSelected(idxDone)) {
                memory.add(idxDone);
                double value = modelToUse.getDatatFor(idxElt, iPt);
                final Color color = paletteCouleur_ == null ? null : ((BPalettePlage) paletteCouleur_).getColorFor(
                    value);
                final Color c = rapide ? color : EbliLib.getAlphaColor(color, alpha_);
                if (c != null) {
                  _g.setColor(c);
                  _g.drawLine(x[iPt], y[iPt], x[iPt], y[iPt]);
                }
              }
            }
          }
        } else {
          v_ = modelToUse.fillWithData(idxElt, v_);
          if (v_ != null) {
            if (rapide && deltaX <= 5 && deltaY <= 5) {
              double moyenne = CtuluLibArray.getMoyenne(v_);
              final Color c = paletteCouleur_ == null ? null : ((BPalettePlage) paletteCouleur_).getColorFor(
                  moyenne);
              _g.setColor(c);
              _g.fillRect(minX, minY, deltaX + 1, deltaY + 1);
            } else {
              final Polygon p = new Polygon(x, y, nbPt);
              isos.draw(_g, p, v_);
            }
          }
        }
      }
    }
  }

  @Override
  public void paintTransient(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
    paintSonde(_g, _versEcran);
  }

  protected void updateEltAdapter() {
    eltAdapter_.setCurrentEltData(getIsoModelAbstract().var_, 0, getIsoModelAbstract().oldData_);
  }

  /**
   * Ne dessine que la selection.
   *
   * @param _g le graphics cible
   */
  public final void paintElementSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
                                          final GrBoite _clipReel) {
    if (isSelectionEmpty()) {
      return;
    }
    final GrBoite domaine = modele_.getDomaine();

    if (!domaine.intersectXY(_clipReel)) {
      return;
    }
    Color cs = _trace.getColor();

    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    final TraceLigne tlSelection = _trace.getLigne();
    final TraceIcon ic = _trace.getIcone();

    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    _g.setColor(cs);
    final int nb = selection_.getMaxIndex();
    final int min = selection_.getMinIndex();
    final GrPolygone poly = new GrPolygone();

    for (int i = nb; i >= min; i--) {
      if (!selection_.isSelected(i)) {
        continue;
      }
      getIsoModelAbstract().polygone(poly, i, true);

      if (_clipReel.intersectXY(poly.boite())) {
        poly.autoApplique(_versEcran);
        final int nbPoints = poly.nombre();
        GrPoint ptOri = poly.sommets_.renvoie(0);
        GrPoint ptDest;

        for (int j = nbPoints - 1; j >= 0; j--) {
          // le point de dest est initialise
          ptDest = poly.sommets_.renvoie(j);
          ic.paintIconCentre(this, _g, ptDest.x_, ptDest.y_);
          tlSelection.dessineTrait(_g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
          ptOri = ptDest;
        }
      }
    }
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    // super.paintIcon(_c,_g,_x,_y);
    _g.translate(_x, _y);
    final boolean attenue = isAttenue();
    final int w = getIconWidth();
    final int h = getIconHeight();
    Color c;
    c = Color.blue;

    if (attenue) {
      c = attenueCouleur(c);
    }
    _g.setColor(c);
    _g.fillRect(1, 1, w - 1, h - 1);
    c = Color.red;

    if (attenue) {
      c = attenueCouleur(c);
    }
    _g.setColor(c);
    _g.fillOval(3, 3, w - 5, h - 5);
    c = Color.yellow;

    if (attenue) {
      c = attenueCouleur(c);
    }
    _g.setColor(c);
    _g.fillOval(7, 7, w - 14, h - 14);
    _g.translate(-_x, -_y);
  }

  @Override
  public final void doPaintSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
                                     final GrBoite _clipReel) {
    if (getIsoModelAbstract().isCurrentDataElementType()) {
      paintElementSelection(_g, _trace, _versEcran, _clipReel);
    } else {
      super.doPaintSelection(_g, _trace, _versEcran, _clipReel);
    }
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    // c'est pour que la variable en cours soit enregistr�e
    updateLegende(false);
    final EbliUIProperties res = super.saveUIProperties();
    savePalettes(namePalette_, res);
    saveVar(res, this);
    getIsoModelAbstract().saveFilter(res);
    if (res != null) {
      res.put("calque.draw.iso", this.traceIsoLine_);
    }

    return res;
  }

  @Override
  public final CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    if (getIsoModelAbstract().isCurrentDataElementType()) {
      return selectionElement(_pt, _tolerance);
    }
    return super.selection(_pt, _tolerance);
  }

  @Override
  public final CtuluListSelection selection(final LinearRing _poly, final int _mode) {
    if (getIsoModelAbstract().isCurrentDataElementType()) {
      return selectionElement(_poly, _mode);
    }
    return super.selection(_poly, _mode);
  }

  public final CtuluListSelection selectionElement(final GrPoint _pt, final int _tolerance) {
    if (!isVisible()) {
      return null;
    }
    final int i = selectionElement(_pt, _tolerance, getVersReel(), getIsoModelAbstract(), getClipReel(getGraphics()));

    if (i >= 0) {
      final CtuluListSelection r = creeSelection();
      r.add(i);

      return r;
    }
    return null;
  }

  public final CtuluListSelection selectionElement(final LinearRing _poly, final int _mode) {
    if (!isVisible()) {
      return null;
    }
    return ZCalquePolygone.selection(_poly, _mode, getIsoModelAbstract());
  }

  @Override
  public void setForeground(final Color _v) {
    // super.setForeground(_v);
  }

  @Override
  public void setPaletteCouleurPlages(final BPalettePlageInterface _newPlage) {
    if (getListSelectionModel().isSelectionEmpty()) {
      return;
    }
    super.setPaletteCouleurPlages(_newPlage);
  }

  /**
   * @param _enable nouvel etat de l'outil sonde
   */
  @Override
  public void setSondeEnable(final boolean _enable) {
    if (_enable != sonde_) {
      sonde_ = _enable;

      if (sonde_) {
        clearSelection();
      } else {
        clearSonde();
      }
    }
  }

  @Override
  public boolean changeSelection(final CtuluListSelection s, final int action) {
    clearSonde();

    return super.changeSelection(s, action);
  }

  @Override
  public boolean changeSelection(final LinearRing poly, final int action, final int mode) {
    clearSonde();

    return super.changeSelection(poly, action, mode);
  }

  @Override
  public boolean changeSelection(final GrPoint pt, final int tolerancePixel, final int action) {
    clearSonde();

    return super.changeSelection(pt, tolerancePixel, action);
  }

  @Override
  public boolean changeSelection(final LinearRing[] p, final int action, final int mode) {
    clearSonde();

    return super.changeSelection(p, action, mode);
  }

  @Override
  public void clearSonde() {
    if (sondePt_ != null) {
      this.sondePt_.clear();
    }
    sondeSelectedElement_ = -1;
    // pour mettre a jour le panel d'info
    fireSelectionEvent();
  }

  public final void setV(final int _i) {
    if (getIsoModelAbstract().getVariableNb() == 0) {
      return;
    }
    getListSelectionModel().setSelectionInterval(_i, _i);
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    if (_e.getValueIsAdjusting()) {
      return;
    }
    if (_e.getSource() == mainVariableSelectionModel_) {
      H2dVariableType oldVariableSelected = getIsoModelAbstract().getVariableSelected();
      getIsoModelAbstract().setVar(this.mainVariableSelectionModel_.getMinSelectionIndex());
      firePropertyChange("variable", oldVariableSelected, getIsoModelAbstract().getVariableSelected());
      varUpdated();
    }
  }

  public final void varUpdated() {
    updateLegende();
    // si on passe de donn�es sur des elements a des donn�es sur des points ou inve

    if (oldDataTypeIsElement_ != getIsoModelAbstract().isCurrentDataElementType()) {
      clearSelection();
      oldDataTypeIsElement_ = getIsoModelAbstract().isCurrentDataElementType();
    }
    updateCalqueInfo();
    fireSelectionEvent();
    repaint(0);
  }

  public boolean isTraceIsoLine() {
    return traceIsoLine_;
  }

  public void setTraceIsoLine(final boolean _traceIsoLine) {
    if (_traceIsoLine != traceIsoLine_) {
      traceIsoLine_ = _traceIsoLine;

      if (traceIsoLine_ && ligneModel_ == null) {
        ligneModel_ = new TraceLigneModel();
      }
      if (!traceIsoLine_) {
        eltAdapter_ = null;
      }
      firePropertyChange("traceIsoLine", !traceIsoLine_, traceIsoLine_);
      repaint();
    }
  }

  @Override
  public List<GrPoint> getLigneBriseeFromSondes() {
    return this.sondePt_;
  }
}
