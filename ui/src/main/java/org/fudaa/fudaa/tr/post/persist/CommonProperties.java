package org.fudaa.fudaa.tr.post.persist;

import java.io.Serializable;

/**
 * Contient des common properties du projet.
 * @author Adrien
 *
 */
public class CommonProperties implements Serializable {

	public CubaturePropertie cubatureProperties;
	public TrajectoireProperties trajectoireProperties;
	public BilanProperties bilanProperties;
	
	/**
	 * Properties de l'ihm de cubature.
	 * @author Adrien
	 *
	 */
	public static class CubaturePropertie implements Serializable {
		public boolean allDomain = true;
		public boolean selectedObject = false;
		public boolean  lineObject = false;
		public int variable;
		public int pdt;
		public String seuil;
		
		public boolean createCurve1 = false;
		public boolean createCurve2 = false;
		public boolean createCurve3 = false;
		public int pdtDepart;
		public int pdtFin;
		public int grapheCible;
	}
	
	public static class BilanProperties implements Serializable{
		public boolean createLine = true;
		public boolean selectedLine = false;
		
		public String p1x,p1y,p2x,p2y;
		public int variable;
		public int pdt;
		public String seuil;
		public boolean calculVectoriel = false;
		
		public boolean createCurve1 = false;
		public boolean createCurve2 = false;
		public boolean createCurve3 = false;
		public int pdtDepart;
		public int pdtFin;
		public int grapheCible;
	}
	
	public static class TrajectoireProperties implements Serializable{
		public int mode;
		public int vecteur;
		public int pdt;
		public String duree;
		public String precision;
		public String p1x,p1y,p2x,p2y;
		public String nbTrajectoires;
		public int[]  choixVariables;
		public int type;
		public String delta;
		
	}
	
}
