/**
 * @creation 24 mars 2004
 * @modification $Date: 2007-06-05 09:01:13 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuMenu;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.swing.ListModel;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISPrecision;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfFilter;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.commun.save.FudaaSavable;
import org.fudaa.fudaa.tr.common.TrDataSourceNomme;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreated;
import org.fudaa.fudaa.tr.post.data.TrPostDataListener;
import org.fudaa.fudaa.tr.post.data.TrPostDataMinMaxGlobalItem;
import org.nfunk.jep.Variable;

/**
 * Une interface de base pour les fenetre de post?
 *
 * @author Fred Deniger
 * @version $Id: TrPostSource.java,v 1.31 2007-06-05 09:01:13 deniger Exp $
 */
public interface TrPostSource extends EfGridData, FudaaSavable, TrDataSourceNomme {

  /**
   *
   * @return the date in millis used as the beginning of the computation
   */
  public long getReferenceDateInMillis();

  public String getId();

  public void setId(String id);

  public void activate();

  EfGridDataInterpolator getInterpolator();

  /**
   * @param _var la var a enlever
   */
  void removeMinMax(String _var);

  TrPostTimeModel getTime();
  
  String getFormattedTime(int i);

  TrPostFlecheContent getFlecheContent(H2dVariableType _t);

  void buildDefaultVarUpdateLists();

  boolean isInspected();

  /**
   * @return toutes les variables. Les variables represent�es sous forme de vecteur et de norme ne sont mise qu'une fois.
   */
  H2dVariableType[] getAvailableVar();

  double getDefaultPaletteMinValueFor(H2dVariableType _t);

  boolean isDefaultPaletteMinValueDefined(H2dVariableType _t);

  TrPostMinPaletteTableModel getMinPaletteModel();

  Variable getGravity();

  GISPrecision getPrecisionModel();

  /**
   * @param _m le receveur des infos
   * @param _element l'element selectionne
   * @param _x le x du point a interpoler
   * @param _y le y du point a interpoler
   * @param _time le pas de temps
   * @param _layerTitle le titre du calque
   */
  void fillInterpolateInfo(InfoData _m, int _element, double _x, double _y, int _time, String _layerTitle);

  /**
   * @return les variables qui ne representent pas un vecteur
   */
  H2dVariableType[] getAllVariablesNonVec();

  boolean isMinMaxCompute(H2dVariableType _varIdx);

  boolean isMinMaxCompute(H2dVariableType _varIdx, int _timeStep);

  /**
   * @return true si rubar
   */
  boolean containsElementVar();

  /**
   * Fermeture du projet.
   */
  void close();

  boolean isClosed();

  /**
   * La premiere methode a appeler: la database sera fermee. Ouvre toutes les valeurs
   *
   * @param _int la progression
   * @return true si la bd a ete modifiee
   */
  boolean openDatas(ProgressionInterface _int, CtuluAnalyze _analyze, CtuluUI _ui);

  /**
   * @param _variable la variable voulu
   * @param _timeStep l'indice du pas de temps
   * @return les donnees correspondantes ou null
   */
  EfData getData(H2dVariableType _variable, int _timeStep);

  /**
   * @param _variable la variable voulu
   * @param _timeStep l'indice du pas de temps
   * @param _idxPt l'indice du point voulu
   * @return la valeur demandee
   * @throws IOException
   */
  double getData(H2dVariableType _variable, int _timeStep, int _idxPt) throws IOException;

  /**
   * @return les fichiers qui a permis le chargement
   */
  Collection<File> getFiles();

  /**
   * @return le fichier principal des fichiers de res
   */
  File getMainFile();

  public boolean isOpened(final File _f);

  /**
   * Attention: les tableaux, listes, ... qui utilise ce modele doivent se desenregistrer a la fin (sinon le tableau sera toujours considere comme
   * listener du modele)
   *
   * @return la liste des TrPostFlecheContent proposees par le modele
   */
  ListModel getFlecheListModel();

  public ListModel getNewFlecheListModel();

  /**
   * @return le maillage de l'objet source
   */
  @Override
  EfGridInterface getGrid();

  /**
   * @param _variable la variable voulue
   * @return la valeur min sur tous les pas de temps
   */
  TrPostDataMinMaxGlobalItem getGlobalExtrema(H2dVariableType _variable, ProgressionInterface _prog);

  CtuluRange getExtrema(CtuluRange _r, H2dVariableType _variable, EfFilter _cond, ProgressionInterface _prog);

  /**
   * @param _variable la variable demandee
   * @param _time le pas de temps
   * @return le min pour la variable sur le pas de temps demandee
   */
  CtuluRange getExtremaForTimeStep(CtuluRange _r, H2dVariableType _variable, int _time, EfFilter _cond);

  /**
   * @return le nombre de pas de temps
   */
  int getNbTimeStep();

  /**
   * Attention: les tableaux, listes, ... qui utilise ce modele doivent se desenregistrer a la fin (sinon le tableau sera toujours considere comme
   * listener du modele)
   *
   * @return la liste des pas de temps proposees par le modele
   */
  ListModel getTimeListModel();

  /**
   * @param _fmt le nouveau format
   * @return true si changement
   */
  boolean setTimeFormat(CtuluNumberFormatI _fmt);

  CtuluNumberFormatI getTimeFormatter();

  /**
   * @param _i l'indice voulu
   * @return la valeur du pas de temps i. temps en secondes
   */
  double getTimeStep(int _i);

  /**
   * @return le titre du post
   */
  String getTitle();

  String getFormatedTitle();

  /**
   * @param _idx l'indice dans le tableau des variables
   * @return le nom de la variable
   */
  H2dVariableType getVariable(int _idx);

  /**
   * @return le nombre de variable
   */
  int getVariableNb();

  /**
   * Attention: les tableaux, listes, ... qui utilise ce modele doivent se desenregistrer a la fin (sinon le tableau sera toujours considere comme
   * listener du modele)
   *
   * @return la liste des variables (H2DVariable) proposees par le modele
   */
  ListModel getVarListModel();

  /**
   * @return une liste non mise a jour des variables
   */
  ListModel getNewVarListModel();

  /**
   * @return une liste non mise a jour des pas de temps
   */
  FudaaCourbeTimeListModel getNewTimeListModel();

  /**
   * @param _t la variable a tester
   * @return true si creee par l'utilisateur
   */
  boolean isUserCreatedVar(H2dVariableType _t);

  /**
   * @param _t la variable a tester
   * @return true si definie dans les sources du projet
   */
  boolean isInitVar(H2dVariableType _t);

  /**
   * @param _t la variable
   * @return les donn�es
   */
  TrPostDataCreated getUserCreatedVar(H2dVariableType _t);

  boolean isUserVarEditable(H2dVariableTypeCreated _var);

  H2dVariableType[] getUserCreatedVar();

  boolean isDefined(final H2dVariableType _variable);

  void addVariableListener(TrPostDataListener _listener);

  void removeVariableListener(TrPostDataListener _listener);

  /**
   * @return le tableau nom court -> variable
   */
  Map getShortNameCreateVar();

  void fillWithConstantVar(CtuluExpr _expr);

  Variable[] getConstantVar();

  /**
   * @param _t les variables a creer
   * @param _cr les fournisseurs de valeurs
   * @param _cmd le receveur de commandes
   */
  void addUserVar(final H2dVariableTypeCreated[] _t, final TrPostDataCreated[] _cr, CtuluCommandContainer _cmd);

  /**
   * @param _t les variables a enlever
   * @param _cmd le receveur de commandes
   */
  void removeUserVar(final H2dVariableTypeCreated[] _t, CtuluCommandContainer _cmd);

  /**
   * @param _old l'ancienne variable
   * @param _new la nouvelle == si la meme
   * @param _newData la nouvelle expression
   * @param _cmd le receveur de commande
   */
  void updateUserValue(final H2dVariableTypeCreated _old, final H2dVariableTypeCreated _new,
          final TrPostDataCreated _newData, final CtuluCommandContainer _cmd);

  /**
   * @param _analyze permet d'initialise la base temporaire.
   */
  void initializeTempDb(CtuluAnalyze _analyze);

  void addSpecificItemInMainMenu(final BuMenu _m, TrPostCommonImplementation _impl);

  TrPostVisuPanel buildVisuPanel(TrPostProjet _parent, BCalqueLegende _legende);

  /**
   * @return
   */
  H2dVariableType[] getVarToDefinedMinPalette();

  /**
   * @param _v
   * @return
   */
  boolean[] getMinPaletteActived(H2dVariableType[] _v);

  /**
   * @param _v
   * @return
   */
  double[] getMinPaletteValues(H2dVariableType[] _v);

  /**
   * @param _v
   * @param _actived
   * @param _d
   */
  void setDefaultPaletteMinPalette(H2dVariableType[] _v, boolean[] _actived, double[] _d);

  public void fillWithSourceCreationInfo(String _pref, Map _table);

  /**
   * @param src
   * @return true if this source uses the src.
   */
  public List<TrPostSource> getUsedSources();
}