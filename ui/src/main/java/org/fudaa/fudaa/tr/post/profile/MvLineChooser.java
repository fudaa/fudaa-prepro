/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.fudaa.tr.post.profile;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuToolBar;
import com.memoire.bu.BuVerticalLayout;
import org.locationtech.jts.geom.LineString;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Observable;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.CalqueGISTreeModel;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.edition.BPaletteEdition;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.layer.MvNodeLayer;
import org.fudaa.fudaa.meshviewer.layer.MvVisuPanel;
import org.fudaa.fudaa.meshviewer.model.MvNodeModelDefault;
import org.fudaa.fudaa.sig.layer.FSigTempLineInLayer;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * Permet de choisir ou cr�er une ligne. Par d�faut, permet la cr�ation de ligne ferm�e uniquement.
 *
 * @author deniger
 */
public class MvLineChooser extends Observable {

  public BuRadioButton rdSelectedLine_;
  public BuRadioButton rdSelectInTree_;
  public BuRadioButton rdCreateNew_;
  public BuButton buttonCreateNew_;
  // public final MvVisuPanel calque_;
  public final LineString initSelected_;
  public LineString createdLine_;
  CalqueGISTreeModel lineTreeModel_;
  BuPanel panelTreeIsoLignes;
  private FSigTempLineInLayer tmpLine;
  public JTree tree_;
  MvVisuPanel visuPanel;
  final CtuluUI ui;
  final EfGridInterface grid;
  int mask;
  ZCalqueLigneBriseeEditable calqueLigneBrisee;

  public MvLineChooser(final LineString _initSelected, final EfGridInterface _grid, final CtuluUI ui) {
    this(_initSelected, _grid, ui, GISLib.MASK_POLYLINE | GISLib.MASK_POLYGONE);
  }

  public MvLineChooser(final LineString _initSelected, final EfGridInterface _grid, final CtuluUI ui, final int mask) {
    super();
    this.ui = ui;
    this.grid = _grid;
    initSelected_ = _initSelected;
  }

  private void addActionToToolBar(final EbliActionInterface[] navigationActions, final BuToolBar tp) {
    for (int i = 0; i < navigationActions.length; i++) {
      final EbliActionInterface ebliActionInterface = navigationActions[i];
      tp.add(ebliActionInterface.buildToolButton(EbliComponentFactory.INSTANCE));
    }
  }

  protected JTree buildTree() {
    return lineTreeModel_ == null ? null : lineTreeModel_.createView(true, false);
  }

  public FSigTempLineInLayer getTmpLine() {
    return tmpLine;
  }

  public void setTmpLine(FSigTempLineInLayer tmpLine) {
    this.tmpLine = tmpLine;
  }

  public void close() {
    if (tmpLine != null) {
      tmpLine.close();
    }
  }

  public String getError() {
    if (rdCreateNew_ != null && rdCreateNew_.isSelected() && createdLine_ == null) {
      return TrResource
              .getS("Une ligne doit �tre cr��e");
    }

    if (tree_ != null && tree_.isEnabled() && tree_.isSelectionEmpty()) {
      return EbliLib
              .getS("S�lectionner au moins une courbe");
    }
    return null;
  }

  /**
   * @return the initSelected
   */
  public LineString getInitSelected() {
    return initSelected_;
  }

  public JComponent getPanel(final String title) {
    if (panelTreeIsoLignes != null) {
      return panelTreeIsoLignes;
    }
    panelTreeIsoLignes = new BuPanel(new BuVerticalLayout(3));
    tree_ = buildTree();
    panelTreeIsoLignes.add(new BuLabel(title));
    final boolean isTreeEmpty = tree_ == null || tree_.getModel().getChildCount(tree_.getModel().getRoot()) == 0;
    rdSelectedLine_ = new BuRadioButton(MvResource.getS("Utiliser La ligne s�lectionn�e"));
    rdSelectInTree_ = new BuRadioButton(MvResource.getS("Utiliser une ligne disponible:"));
    final ButtonGroup bg = new ButtonGroup();
    bg.add(rdSelectedLine_);
    panelTreeIsoLignes.add(rdSelectedLine_);
    rdCreateNew_ = new BuRadioButton(MvResource.getS("Cr�er une nouvelle ligne"));
    bg.add(rdCreateNew_);
    final JPanel pn = new JPanel(new BorderLayout());
    pn.add(rdCreateNew_);
    buttonCreateNew_ = new BuButton(MvResource.getS("Cr�er/Modifier"));
    buttonCreateNew_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        showCreateNewPanel();

      }
    });
    pn.add(buttonCreateNew_, BorderLayout.EAST);
    buttonCreateNew_.setEnabled(false);
    panelTreeIsoLignes.add(pn);
    if (!isTreeEmpty) {
      bg.add(rdSelectInTree_);
      final BuScrollPane buScrollPane = new BuScrollPane(tree_);
      buScrollPane.setPreferredHeight(200);
      panelTreeIsoLignes.add(rdSelectInTree_);
      buScrollPane.setBorder(BuBorders.EMPTY2500);
      panelTreeIsoLignes.add(buScrollPane);
    }
    if (initSelected_ == null) {
      rdSelectedLine_.setEnabled(false);
      rdSelectInTree_.setSelected(true);
    } else {
      rdSelectedLine_.setSelected(true);
      if (tree_ != null) {
        tree_.setEnabled(false);
      }
      this.update(initSelected_, false);
    }

    if (isTreeEmpty && initSelected_ == null && rdCreateNew_ != null) {
      rdCreateNew_.setSelected(true);
      buttonCreateNew_.setEnabled(true);
    }
    if (tree_ != null) {
      tree_.addTreeSelectionListener(new TreeSelectionListener() {
        @Override
        public void valueChanged(final TreeSelectionEvent _e) {
          // updateError(pn, tree_, varTime_);
          update(getSelectedLineInTree(tree_), true);
        }
      });
    }
    // le selectionneur pour le bouton
    final ItemListener itemListener = new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent _e) {
        itemStateChangedInBt();
      }
    };
    rdSelectedLine_.addItemListener(itemListener);
    if (rdCreateNew_ != null) {
      rdCreateNew_.addItemListener(itemListener);
    }
    return panelTreeIsoLignes;
  }

  public LineString getSelectedLine() {
    if (rdSelectedLine_.isSelected()) {
      return initSelected_;
      // MvProfileBuilderFromTree.this.name_ = EbliLib.getS("S�lection");
    } else if (rdCreateNew_ != null && rdCreateNew_.isSelected()) {
      return createdLine_;
    } else {
      return getSelectedLineInTree(tree_);
      // MvProfileBuilderFromTree.this.name_ = getSelectedLineTitle(tree_);
    }
  }

  public LineString getSelectedLineInTree(final JTree _tree) {
    if (_tree == null || _tree.isSelectionEmpty()) {
      return null;
    }
    final CalqueGISTreeModel.LayerNode node = (CalqueGISTreeModel.LayerNode) _tree.getSelectionPath()
            .getLastPathComponent();
    final ZCalqueLigneBrisee cq = (ZCalqueLigneBrisee) node.getUserObject();
    return (LineString) cq.modeleDonnees().getGeomData().getGeometry(node.getIdxGeom());
  }

  public String getSelectedLineName() {
    if (rdSelectedLine_.isSelected()) {
      return EbliLib.getS("S�lection");
    } else {
      return getSelectedLineTitle(tree_);
    }
  }

  public String getSelectedLineTitle(final JTree _tree) {
    if (_tree == null || _tree.isSelectionEmpty()) {
      return null;
    }
    final CalqueGISTreeModel.LayerNode node = (CalqueGISTreeModel.LayerNode) _tree.getSelectionPath()
            .getLastPathComponent();
    return node.toString();
  }

  protected void itemStateChangedInBt() {
    if (tree_ != null) {
      tree_.setEnabled(rdSelectInTree_.isSelected());
    }
    if (rdCreateNew_ != null) {
      buttonCreateNew_.setEnabled(rdCreateNew_.isSelected());
    }
    if (rdSelectedLine_.isSelected()) {
      update(initSelected_, tmpLine != null && tmpLine.isZoomChanged());
    } else if (rdCreateNew_ != null && rdCreateNew_.isSelected()) {
    } else if (tree_ == null || tree_.isSelectionEmpty()) {
      update(null, false);
    } else {
      update(getSelectedLineInTree(tree_), true);
    }
  }

  public void restaurer() {
    if (tmpLine != null) {
      tmpLine.restaurer();
    }
  }

  public void setCanCreateNewLine(final BCalque[] cqsToDisplay) {
    buildVisuPanel(cqsToDisplay);
  }

  private void buildVisuPanel(final BCalque[] cqsToDisplay) {
    visuPanel = new MvVisuPanel((FudaaCommonImplementation) ui);
    for (int i = 0; i < cqsToDisplay.length; i++) {
      visuPanel.getGcDonnees().add(cqsToDisplay[i]);
    }
    calqueLigneBrisee = (ZCalqueLigneBriseeEditable) visuPanel.getGroupGIS().addLigneBriseeLayerAct("saisie",
            new GISZoneCollectionLigneBrisee());
    visuPanel.getScene().setCalqueActif(calqueLigneBrisee);
  }
  private boolean createLigneOuverte;

  public void setCreateLigneOuverte(final boolean b) {
    createLigneOuverte = b;
  }

  private void buildDefaultVisuPanel() {
    final MvNodeLayer mvNodeLayer = new MvNodeLayer(new MvNodeModelDefault(grid));
    mvNodeLayer.setIconModel(0, new TraceIconModel(TraceIcon.CROIX, 2, Color.BLACK));
    buildVisuPanel(new BCalque[]{mvNodeLayer});

  }

  @SuppressWarnings("serial")
  private void showCreateNewPanel() {
    if (visuPanel == null) {
      buildDefaultVisuPanel();
    }
    final CtuluDialogPanel pn = new CtuluDialogPanel() {
      @Override
      public boolean isDataValid() {
        final int nbLigneBrisee = calqueLigneBrisee.modeleDonnees().getNombre();
        if (nbLigneBrisee == 0) {
          setErrorText(TrResource.getS("Une ligne doit �tre cr��e"));
          return false;
        } else if (nbLigneBrisee > 1) {

          setErrorText(TrResource.getS("Une seule ligne ferm�e doit �tre cr��e"));
          return false;
        }
        return true;
      }
    };
    pn.setLayout(new BorderLayout());
    final BuToolBar tp = new BuToolBar();
    tp.add(visuPanel.getController().getSelectionRectangleAction());
    addActionToToolBar(visuPanel.getController().getNavigationActions(), tp);
    tp.addSeparator();
    visuPanel.getArbreCalqueModel().setSelectionCalque(calqueLigneBrisee);
    final BPaletteEdition palette = new BPaletteEdition(visuPanel.getScene());
    palette.buildComponents();
    palette.setTargetClient(visuPanel.getEditor());

    visuPanel.getEditor().setActivated(calqueLigneBrisee, palette);
    if (createLigneOuverte) {
      final AbstractButton globalButton = visuPanel.getEditor().getEditionPalette().getGlobalButton(
              "GLOBAL_ADD_POLYLIGNE");
      tp.add(globalButton);
      visuPanel.getEditor().changeState("GLOBAL_ADD_POLYLIGNE");
      globalButton.setSelected(true);
    } else {
      tp.add(visuPanel.getEditor().getEditionPalette().getGlobalButton("GLOBAL_ADD_RECTANGLE"));
      final AbstractButton globalButton = visuPanel.getEditor().getEditionPalette().getGlobalButton(
              "GLOBAL_ADD_POLYGONE");
      tp.add(globalButton);
      visuPanel.getEditor().changeState("GLOBAL_ADD_POLYGONE");
      globalButton.setSelected(true);
    }
    visuPanel.getEditor().setActivated(calqueLigneBrisee, palette);
    tp.add(visuPanel.getEditor().getEditionPalette().getGlobalButton("ATOME_ADD"));
    tp.add(visuPanel.getEditor().getEditionPalette().getGlobalButton("MODE_ATOME"));
    tp.addSeparator();
    tp.add(visuPanel.getEditor().getActionDelete());
    tp.addSeparator();
    tp.add(visuPanel.getEditor().getEditorPanel().getBtReprise());
    pn.add(tp, BorderLayout.NORTH);
    pn.add(visuPanel);
    final Window activeWindow = CtuluLibSwing.getActiveWindow();
    final CtuluDialog ctuluDialog = (activeWindow instanceof JDialog) ? new CtuluDialog((JDialog) activeWindow, pn)
            : new CtuluDialog((JFrame) activeWindow, pn);
    ctuluDialog.setPreferredSize(new Dimension(500, 500));
    ctuluDialog.setLocationRelativeTo(panelTreeIsoLignes);
    final int res = ctuluDialog.afficheDialogModal(new Runnable() {
      @Override
      public void run() {
        visuPanel.restaurer();
      }
    });
    if (CtuluDialogPanel.isOkResponse(res)) {
      createdLine_ = (LineString) calqueLigneBrisee.modeleDonnees().getGeomData().getGeometry(0);
      this.update(createdLine_, false);
    }

  }

  public void update(final LineString _s, final boolean _zoom) {
    if (tmpLine != null) {
      tmpLine.display(_s, _zoom);
    }
    setChanged();
    super.notifyObservers(_s);
  }

  public void zoomInitial() {
    if (tmpLine != null) {
      tmpLine.zoomInitial();
    }
  }
}
