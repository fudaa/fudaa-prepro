/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuEmptyArrays;
import gnu.trove.TIntHashSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGGroup;

/**
 *
 * @author Frederic Deniger
 */
public class TrPostCourbeTreeModelBuilder {

  List<TrPostInterpolatePoint> intepolPt_ = new ArrayList<TrPostInterpolatePoint>();
  int[] objIdx_ = FuEmptyArrays.INT0;
  Set<H2dVariableType> varSupported_ = new HashSet<H2dVariableType>();
  TrPostCourbeTreeModel treeModel;

  public TrPostCourbeTreeModelBuilder(TrPostCourbeTreeModel treeModel) {
    this.treeModel = treeModel;
  }

  TrPostCourbeTreeModelBuilder(TrPostCourbeTreeModel treeModel, int[] _idxPtOrMeshesArray, H2dVariableType[] _vars) {
    this(treeModel);
    this.objIdx_ = _idxPtOrMeshesArray;
    this.varSupported_.addAll(Arrays.asList(_vars));
  }

  TrPostCourbeTreeModelBuilder(TrPostCourbeTreeModel treeModel, TrPostInterpolatePoint[] pts, H2dVariableType[] _vars) {
    this(treeModel);
    intepolPt_.addAll(Arrays.asList(pts));
    this.varSupported_.addAll(Arrays.asList(_vars));
  }

  protected void addAllCourbes(final TrPostSource _src, final ProgressionInterface _prog, final CtuluCommandContainer _cmd) {
    treeModel.startUpdating();
    final List modelToUpdate = new ArrayList();
    final List courbeToMem = _cmd == null ? null : new ArrayList();
    final List groupToMem = _cmd == null ? null : new ArrayList();
    // double[] timesInSec = _src.getTime().getTimeListModel().getTimesInSec();
    int cptGroupe =-1;
    for (H2dVariableType t : varSupported_) {
      cptGroupe++;
      final EGGroup g = treeModel.getGroupFor(t);
      final int nb = objIdx_.length;
      for (int i = 0; i < nb; i++) {
        final int idx = objIdx_[i];
        final TrPostCourbeModel c = treeModel.containsCourbeFor(t, g, idx);
        if (c == null) {

          final EGCourbeChild courbe = TrPostCourbeModelBuilder.buildCourbeModel(t, idx, g, _src);
           courbe.setAspectContour(courbe.getLigneModel().getCouleur());
          if (courbeToMem != null) {
            courbeToMem.add(courbe);
            groupToMem.add(g);
          }
          modelToUpdate.add(courbe.getModel());
        } else if (c.isYNull()) {
          modelToUpdate.add(c);
        }
      }
      int cptInterpol =0;
      for (final Iterator iter = intepolPt_.iterator(); iter.hasNext();) {
        final TrPostInterpolatePoint element = (TrPostInterpolatePoint) iter.next();
        final TrPostCourbeModel c = treeModel.containsCourbeFor(t, g, element);
        if (c == null) {
          final EGCourbeChild courbe = TrPostCourbeModelBuilder.buildCourbeModel(t, element, g, _src);
            courbe.setAspectContour(courbe.getLigneModel().getCouleur());
          if (courbeToMem != null) {
            courbeToMem.add(courbe);
            groupToMem.add(g);
          }
          modelToUpdate.add(courbe.getModel());
        } else if (c.isYNull()) {
          modelToUpdate.add(c);
        }
      }
    }
    if (courbeToMem != null && _cmd != null) {
      _cmd.addCmd(treeModel.new CommandAddCourbesMulti((EGCourbeChild[]) courbeToMem.toArray(new EGCourbeChild[courbeToMem.size()]), (EGGroup[]) groupToMem
              .toArray(new EGGroup[groupToMem.size()])));
    }
    treeModel.updateCurves((TrPostCourbeModel[]) modelToUpdate.toArray(new TrPostCourbeModel[modelToUpdate.size()]), _prog);
    treeModel.reupdateGroup();
    treeModel.updateTimeFmt(_src);
  }

  /**
   * Refonte de la methode addpoints pour gerer les fusions de graphes. fusionne les variables des 2 graphes model afin de recuperer toutes les
   * variabels.
   *
   * @author Adrien Hadoux
   * @param _idxToAdd
   * @param _cmd
   * @param _prog
   * @param variablesGrapheMerge
   */
  public void addPoints(final TrPostSource _src, final int[] _idxToAdd, final CtuluCommandContainer _cmd, final ProgressionInterface _prog,
          final Set variablesGrapheMerge) {
    // pour le undo
    final int[] old = CtuluLibArray.copy(objIdx_);
    final TIntHashSet hasset = new TIntHashSet(objIdx_);
    hasset.addAll(_idxToAdd);
    objIdx_ = hasset.toArray();
    Arrays.sort(objIdx_);
    Arrays.sort(_idxToAdd);
    // buildTimeSteps();
    treeModel.startUpdating();
    final List modelToUpdate = new ArrayList();
    final List courbeToMem = _cmd == null ? null : new ArrayList();
    final List groupToMem = _cmd == null ? null : new ArrayList();

    // -- completer les variables du graphe avec celles du graphe merg� --//
    if (variablesGrapheMerge != null) {
      for (final Iterator it = variablesGrapheMerge.iterator(); it.hasNext();) {
        final H2dVariableType t = (H2dVariableType) it.next();
        varSupported_.add(t);
      }
    }

    for (final Iterator it = varSupported_.iterator(); it.hasNext();) {
      final H2dVariableType t = (H2dVariableType) it.next();
      final EGGroup g = treeModel.getGroupFor(t);
      final int nb = _idxToAdd.length;
      for (int i = 0; i < nb; i++) {
        final int idx = _idxToAdd[i];
        final TrPostCourbeModel c = treeModel.containsCourbeFor(t, g, idx);
        if (c == null) {
          final EGCourbeChild courbe = TrPostCourbeModelBuilder.buildCourbeModel(t, idx, g, _src);
          if (courbeToMem != null) {
            courbeToMem.add(courbe);
            groupToMem.add(g);
          }
          modelToUpdate.add(courbe.getModel());
        } else if (c.isYNull()) {
          modelToUpdate.add(c);
        }
      }
    }
    if (courbeToMem != null && _cmd != null) {
      _cmd.addCmd(treeModel.new CommandAddCourbesMulti((EGCourbeChild[]) courbeToMem.toArray(new EGCourbeChild[courbeToMem.size()]), (EGGroup[]) groupToMem
              .toArray(new EGGroup[groupToMem.size()])) {
        @Override
        public void redo() {
          super.redo();
          final TIntHashSet hassetRedo = new TIntHashSet(objIdx_);
          hassetRedo.addAll(_idxToAdd);
          objIdx_ = hassetRedo.toArray();
          Arrays.sort(objIdx_);
          treeModel.reupdateGroup();
        }

        @Override
        public void undo() {
          super.undo();
          objIdx_ = old;
          treeModel.reupdateGroup();
        }
      });
    }
    treeModel.updateCurves((TrPostCourbeModel[]) modelToUpdate.toArray(new TrPostCourbeModel[modelToUpdate.size()]), _prog);
    treeModel.reupdateGroup();
    treeModel.updateTimeFmt(_src);

  }
}
