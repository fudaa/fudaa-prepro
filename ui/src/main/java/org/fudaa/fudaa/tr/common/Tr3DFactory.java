/*
 * @creation 30 mai 2006
 * 
 * @modification $Date: 2007-06-11 13:08:20 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuCheckBox;
import org.locationtech.jts.geom.Envelope;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.media.j3d.RenderingAttributes;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import javax.vecmath.Point3d;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurListComboBox;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.volume.BCalqueBoite;
import org.fudaa.ebli.volume.BGrille;
import org.fudaa.ebli.volume.BGroupeVolume;
import org.fudaa.ebli.volume.ZVue3DPanel;
import org.fudaa.ebli.volume.controles.BArbreVolume;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.layer.MvFrontierLayerAbstract;
import org.fudaa.fudaa.meshviewer.threedim.Mv3DBathy;
import org.fudaa.fudaa.meshviewer.threedim.Mv3DCalque;
import org.fudaa.fudaa.meshviewer.threedim.Mv3DFactory;
import org.fudaa.fudaa.meshviewer.threedim.Mv3DImage;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourceTelemac3D;

/**
 * @author fred deniger
 * @version $Id: Tr3DFactory.java,v 1.20 2007-06-11 13:08:20 deniger Exp $
 */
public final class Tr3DFactory extends Mv3DFactory {

  public Tr3DFactory() {
  }

  //TODO Gard�e pour retro-compatibilit�, voir si � virer.
  public final void afficheFrame(final JFrame _f, final EfGridData _grid, final InterpolationVectorContainer _vects,
          final CtuluUI _ui,
          final BGroupeCalque _fond, final MvFrontierLayerAbstract _fr) {
    afficheFrame(_f, _grid, _vects, _ui, _fond, _fr, new ZCalqueLigneBrisee[0]);
  }

  public final void afficheFrame(final JFrame _f, final EfGridData _grid, final InterpolationVectorContainer _vects,
          final CtuluUI _ui,
          final BGroupeCalque _fond, final MvFrontierLayerAbstract _fr,
          final ZCalqueLigneBrisee[] _buildingLayers) {
    if (_grid == null) {
      return;
    }
    final CtuluTaskDelegate task = _ui.createTask("3D");
    task.start(new Runnable() {
      @Override
      public void run() {
        afficheFrameAction(_f, _grid, _vects, _ui, _fond, _fr, _buildingLayers, task.getStateReceiver());
      }
    });
  }

  private void afficheFrameAction(final JFrame _f, final EfGridData _initSrc, final InterpolationVectorContainer _vects,
          final CtuluUI _ui,
          final BGroupeCalque _cqFond, final MvFrontierLayerAbstract _building,
          final ZCalqueLigneBrisee[] _buildingLayers,
          final ProgressionInterface _prog) {
    // on doit avoir des T3
    //we must get T3
    H2dVariableType varBathy = H2dVariableType.BATHYMETRIE;
    boolean containsBathy = _initSrc.isDefined(varBathy);
    H2dVariableType[] varZe = new H2dVariableType[]{H2dVariableType.COTE_EAU};
    boolean containsZe = _initSrc.isDefined(varZe[0]);
    boolean isTelemac3D = false;
    if (!containsBathy && !containsZe) {
      boolean ok = false;
      if (_initSrc instanceof TrPostSourceTelemac3D) {
        isTelemac3D = true;
        final TrPostSourceTelemac3D src = (TrPostSourceTelemac3D) _initSrc;
        final CtuluPermanentList vars = src.getCotes();
        final int size = vars.size();
        ok = size > 0;
        if (ok) {
          containsBathy = true;
          varBathy = (H2dVariableType) vars.get(0);
          containsZe = size > 1;
          varZe = new H2dVariableType[size - 1];
          for (int i = 0; i < size - 1; i++) {
            varZe[i] = (H2dVariableType) vars.get(i + 1);
          }
        }
      }
      if (!ok) {
        _ui.error(Tr3DInitialiser.get3dName(), TrResource.getS("Pas de variables 3D"), false);
        return;
      }

    }
    final EfGridData endData = isTelemac3D ? _initSrc : convertGrid(_initSrc, _ui, _prog, new CtuluVariable[]{varBathy, varZe[0]},
            _vects);
    if (endData == null) {
      return;
    }
    if (_prog != null) {
      _prog.setDesc(TrResource.getS("Conversion 3D"));
    }
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    final EfGridInterface grid = endData.getGrid();
    up.setValue(5, grid.getPtsNb() * 2, 0, 50);
    Point3d[] ptsBathy = null;
    // pour telemac 3d, il peut y avoir plusieur couche
    //for telemac 3d, there can get several layers
    final Point3d[][] ptsCote = new Point3d[varZe.length][];
    int idx = 0;
    boolean isAnim = _initSrc instanceof TrPostSource;
    if (isAnim) {
      idx = ((TrPostSource) _initSrc).getNbTimeStep() - 1;
      if (idx <= 0) {
        idx = 0;
        isAnim = false;
      }
    }
    final GrBoite[] boiteZ = new GrBoite[varZe.length];
    final Envelope envelope = grid.getEnvelope(null);
    for (int i = 0; i < boiteZ.length; i++) {
      boiteZ[i] = new GrBoite(envelope);
    }
    final GrBoite boiteBathy = new GrBoite(envelope);
    final GrBoite boiteAll = new GrBoite(envelope);
    if (containsBathy) {
      ptsBathy = create3d(endData, varBathy, idx, _ui, up, boiteBathy);
      // Erreur non g�r�e
      // Error not managed
      if (ptsBathy == null) {
        return;
      }
      boiteAll.ajuste(boiteBathy);
    }
    if (containsZe) {
      for (int i = 0; i < varZe.length; i++) {
        ptsCote[i] = create3d(endData, varZe[i], idx, _ui, up, boiteZ[i]);
        // Erreur non g�r�e
        // Error not managed
        if (ptsCote[i] == null) {
          return;
        }
        boiteAll.ajuste(boiteZ[i]);
      }

    }
    final int[] connect = buildConnections(up, grid);
    // on construit les calques 3D
    // We build the 3D layers
    final BGroupeVolume gv = new BGroupeVolume();
    gv.setName(EbliLib.getS("Calques"));
    // les axes
    // the axis
    final BCalqueBoite chp = new BCalqueBoite(EbliLib.getS("Axes"));
    boiteAll.factor(1.2);
    chp.setGeometrie(boiteAll);

    gv.add(chp);
    // la boite englobante xy
    // the box including xy 
    final Mv3DCalque[] cqZe = new Mv3DCalque[varZe.length];
    if (containsZe) {
      for (int i = 0; i < varZe.length; i++) {
        cqZe[i] = createGrille(varZe[i], _initSrc, endData, idx);
        cqZe[i].setBoite(boiteZ[i]);
        cqZe[i].setGeometrie(grid.getPtsNb(), ptsCote[i], connect.length, connect);
        updateWaterLayer(cqZe[i]);
      }
    }
    if (containsBathy) {
      final Mv3DCalque cq = createGrille(varBathy, _initSrc, endData, idx);
      cq.setBoite(boiteBathy);
      cq.setGeometrie(grid.getPtsNb(), ptsBathy, connect.length, connect);
      cq.setCouleur(Mv3DCalque.getBathyColor());
      cq.setBrillance(0);
      Mv3DImage.addTexture(_cqFond, cq, endData.getGrid());
      // pour les projets 2d uniquement
      //  only for the 2d projects
      if (containsZe && cqZe.length == 1 && cqZe[0] != null) {
        cqZe[0].setBathy(cq);
      }
      gv.add(cq);
      final CtuluCollectionDouble height = _building == null ? null : _building.getBuildingHeight();
      if (_building != null && height != null) {
        final BGrille g = Mv3DBathy.addBuilding(endData, _building.get3DType(), height);
        if (g != null) {
          gv.add(g);
        }
      }
      if (_buildingLayers != null) {
        //TODO Voir si code doit �tre sorti du bloc if.
        for (ZCalqueLigneBrisee layer : _buildingLayers) {
          layer.modeleDonnees().prepareExport();
          final BGrille g = Mv3DBathy.addBuilding(endData, layer);
          if (g != null) {
            gv.add(g);
          }
        }
      }
    }
    if (containsZe) {
      for (int i = 0; i < varZe.length; i++) {
        gv.add(cqZe[i]);
      }

    }

    afficheFrame(_f, isAnim, gv, _initSrc);
    chp.setVisible(false);

  }

  protected final void updateWaterLayer(final Mv3DCalque _cqZe) {
    _cqZe.setCouleur(Color.BLUE);
    _cqZe.setTransparence(0.1F);
    RenderingAttributes rendering = _cqZe.getRenderingAttributes();
    if (rendering == null) {
      rendering = new RenderingAttributes();
      _cqZe.setRenderingAttributes(rendering);
    }
    rendering.setAlphaTestFunction(RenderingAttributes.GREATER_OR_EQUAL);
    rendering.setAlphaTestValue(0.9f);
  }

  @Override
  protected void initFille(final ZVue3DPanel fille, final EfGridData _grid, final boolean _isAnim) {
    initUseH(fille, _grid);
    if (_isAnim) {
      initAnim(fille);
    }
  }

  protected final void initUseH(final ZVue3DPanel _fille, final EfGridData _src) {
    if (!_src.isDefined(H2dVariableType.HAUTEUR_EAU)) {
      return;
    }
    final BArbreVolume arbre = _fille.getArbreVolume();
    final JCheckBox cbUseH = new BuCheckBox();
    cbUseH.setEnabled(false);
    cbUseH.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        final TreePath path = arbre.getSelectionPath();
        if (path != null && path.getLastPathComponent() instanceof Mv3DCalque) {
          final Mv3DCalque cq = (Mv3DCalque) path.getLastPathComponent();
          cq.setUseH(cbUseH.isSelected());
          arbre.propertyChangedForSelectedLayer();
        }

      }
    });
    arbre.addTreeSelectionListener(new TreeSelectionListener() {
      @Override
      public void valueChanged(final TreeSelectionEvent _e) {
        final TreePath path = arbre.getSelectionPath();
        boolean enable = path != null && path.getLastPathComponent() instanceof Mv3DCalque;
        if (enable && path != null) {
          final Mv3DCalque cq = (Mv3DCalque) path.getLastPathComponent();
          enable = cq.isHUsable();
          cbUseH.setEnabled(enable);
          cbUseH.setSelected(enable && cq.isUseH());
        }
      }
    });
    _fille.addUserComponent(EbliLib.getS("Utiliser la hauteur d'eau pour la palette de couleurs"), cbUseH);
  }

  protected final void initAnim(final ZVue3DPanel _fille) {
    final BArbreVolume arbre = _fille.getArbreVolume();
    final BSelecteurListComboBox palette = new BSelecteurListComboBox();
    arbre.getSelectionModel().addTreeSelectionListener(palette);
    _fille.addUserComponent(MvResource.getS("Pas de temps"), palette.getCb());

  }

  @Override
  protected final Mv3DCalque createGrille(final H2dVariableType _var, final EfGridData _init, final EfGridData _end, final int _idx) {
    Mv3DCalque r = null;
    if (_init instanceof TrPostSource) {
      r = new Tr3DCalqueTime(_var, (TrPostSource) _init, _end, _idx);
    } else {
      r = new Mv3DCalque(_var, _end);
    }
    r.setVisible(true);
    r.setRapide(false);
    r.setEclairage(true);
    // r.setCollidable(false);
    return r;

  }
}
