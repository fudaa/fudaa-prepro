/*
 * @creation 9 ao�t 2005
 * 
 * @modification $Date: 2007-04-16 16:35:32 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.fu.FuLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluParser;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.TrPostFlecheContent;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id: TrPostDataCreatedExpr.java,v 1.6 2007-04-16 16:35:32 deniger Exp $
 */
public final class TrPostDataCreatedExpr implements TrPostDataCreated {

  private static final ExecutorService EXECUTOR_SERVICE = Executors.newCachedThreadPool();
  String formule;
  private final transient TrPostSource src_;
  String[] usedVar_;
  H2dVariableType[] var_;
  Map<String, Object> constantVars = new HashMap<String, Object>();

  /**
   * @param _expr l'expression
   * @param _var le tableau des variables de l'expression a utiliser
   * @param _var2 leur correspondance en Variable2d.
   */
  TrPostDataCreatedExpr(final TrPostSource _src, final String formule, final String[] _var, final H2dVariableType[] _var2) {
    super();
    src_ = _src;
    this.formule = formule;
    usedVar_ = _var;
    if (_var2 != null) {
      var_ = Arrays.copyOf(_var2, _var2.length);
      for (H2dVariableType h2dVariableType : _var2) {
        assert h2dVariableType != null;
      }
    }
  }

  public TrPostSource getSrc() {
    return src_;
  }

  @Override
  public void restore() {
  }

  private TrPostDataCreatedExpr(final TrPostSource _src, final TrPostDataCreatedExpr _from) {
    super();
    src_ = _src;
    initFrom(_from);
  }

  public TrPostDataCreatedExpr createCopy(final TrPostSource _src) {
    return new TrPostDataCreatedExpr(_src, this);
  }

  protected void initFrom(final TrPostDataCreatedExpr _from) {
    formule = _from.formule;
    usedVar_ = _from.usedVar_;
    var_ = _from.var_;
    for (H2dVariableType h2dVariableType : var_) {
      assert h2dVariableType != null;
    }
  }

  @Override
  public void clearCache() {
  }

  public int compareTo(final Object _o) {
    if (_o == this) {
      return 0;
    }
    if (_o == null) {
      return 1;
    }
    final int i = getDescription().compareTo(((TrPostDataCreated) _o).getDescription());
    // normalement il sont egaux
    if (i == 0) {
      return hashCode() - _o.hashCode();
    }
    return i;
  }

  @Override
  public TrPostDataCreatedSaver createSaver() {
    return null;
  }

  @Override
  public boolean equals(final Object _obj) {
    if (_obj == this) {
      return true;
    }
    if (_obj == null) {
      return false;
    }
    if (_obj.getClass().equals(getClass())) {
      return getFormule().equals(((TrPostDataCreatedExpr) _obj).getFormule());
    }
    return false;
  }

  /**
   * @param _res la liste a remplir avec toutes les variables utilisee par cette var et les var dependantes
   */
  @Override
  public void fillWhithAllUsedVar(final Set _res) {
    if (var_ != null) {
      for (int i = var_.length - 1; i >= 0; i--) {
        final H2dVariableType vi = var_[i];
        if (!_res.contains(vi)) {
          _res.add(vi);
          TrPostDataHelper.addDependingVarOf(vi, src_, _res);
        }
      }
    }
  }

  public Set getAllUsedVars() {
    final HashSet r = new HashSet();
    fillWhithAllUsedVar(r);
    return r;
  }

  private static class PartialResultCallable implements Callable<PartialResult> {

    private final PartialResult init;
    private final CtuluParser parser;
    private final Variable[] variable;
    final EfData[] ds;

    public PartialResultCallable(PartialResult init, CtuluParser parser, Variable[] variable, EfData[] ds) {
      super();
      this.init = init;
      this.parser = parser;
      this.variable = variable;
      this.ds = ds;
    }

    @Override
    public PartialResult call() throws Exception {
      for (int localIdx = 0; localIdx < init.getSize(); localIdx++) {
        for (int variableIdx = variable.length - 1; variableIdx >= 0; variableIdx--) {
          variable[variableIdx].setValue(CtuluLib.getDouble(ds[variableIdx].getValue(init.getGlobalIdx(localIdx))));
        }
        init.setValued(localIdx, parser.getValue());
      }
      return init;
    }
  }

  private static class PartialResult {

    private final int firstIndex;
    private final int lastIndex;
    private final double[] values;

    public PartialResult(int firstIndex, int lastIndex) {
      super();
      this.firstIndex = firstIndex;
      this.lastIndex = lastIndex;
      assert lastIndex > firstIndex;
      values = new double[getSize()];
    }

    public final int getSize() {
      return lastIndex - firstIndex + 1;
    }

    public int getGlobalIdx(int local) {
      return firstIndex + local;
    }

    protected void setValued(int localIdx, double value) {
      values[localIdx] = value;
    }

    public double getValue(int localIdx) {
      return values[localIdx];

    }
  }

  @Override
  public EfData getDataFor(final int _idxTime) {

    final EfData[] ds = new EfData[usedVar_.length];
    // on enregistre les donnees pour chaque variable necessaire.
    boolean isElem = false;
    for (int i = usedVar_.length - 1; i >= 0; i--) {
      ds[i] = src_.getData(var_[i], _idxTime);
      if (ds[i].isElementData()) {
        isElem = true;
      }
    }
    if (isElem) {
      for (int i = 0; i < ds.length; i++) {
        if (!ds[i].isElementData()) {
          ds[i] = EfLib.getElementDataDanger(ds[i], src_.getGrid());
        }
      }
    }

    final double[] res = new double[isElem ? src_.getGrid().getEltNb() : src_.getGrid().getPtsNb()];
    if (res.length < 100) {
      return getDataSimpleFor(_idxTime, res, ds);
    }
    //    //we create as thread as availables:
    int nbThread = 3;
    int length = (int) Math.ceil(Math.max(res.length / nbThread, 2));
    List<PartialResult> chunck = new ArrayList<TrPostDataCreatedExpr.PartialResult>();
    int lastIndex = res.length - 1;
    for (int i = 0; i < res.length; i += length) {
      int lastPartialIndex = Math.min(i + length - 1, lastIndex);
      if (lastPartialIndex >= lastIndex - 2) {
        lastPartialIndex = lastIndex;
      }
      chunck.add(new PartialResult(i, lastPartialIndex));
      if (lastPartialIndex == lastIndex) {
        break;
      }

    }
    List<PartialResultCallable> callables = new ArrayList<PartialResultCallable>();
    for (PartialResult partialResult : chunck) {
      CtuluParser expression = createExpression();
      Variable[] variables = createVariables(expression);
      callables.add(new PartialResultCallable(partialResult, expression, variables, ds));
    }
    try {
      List<Future<PartialResult>> invokeAll = EXECUTOR_SERVICE.invokeAll(callables);
      for (Future<PartialResult> future : invokeAll) {
        PartialResult partialResult = future.get();
        for (int localIdx = 0; localIdx < partialResult.getSize(); localIdx++) {
          int globalIdx = partialResult.getGlobalIdx(localIdx);
          res[globalIdx] = partialResult.getValue(localIdx);
        }

      }
    } catch (Exception e) {
      FuLog.error(e);
    }

    return isElem? (EfData) new EfDataElement(res) : new EfDataNode(res);

  }

  public EfData getDataSimpleFor(final int _idxTime, final double[] res, final EfData[] ds) {
    CtuluParser expression = createExpression();
    Variable[] variables = createVariables(expression);
    for (int i = res.length - 1; i >= 0; i--) {
      // mis a jour des variables
      for (int j = usedVar_.length - 1; j >= 0; j--) {
        variables[j].setValue(CtuluLib.getDouble(ds[j].getValue(i)));
      }
      //      String errorInfo = expression.getErrorInfo();
      res[i] = expression.getValue();
    }
    return src_.containsElementVar() ? (EfData) new EfDataElement(res) : new EfDataNode(res);

  }

  private Variable[] createVariables(CtuluParser expression) {
    Variable[] variables = new Variable[usedVar_.length];
    for (int i = 0; i < usedVar_.length; i++) {
      variables[i] = expression.getSymbolTable().makeVarIfNeeded(usedVar_[i]);
    }
    expression.parseExpression(formule);
    return variables;
  }

  private CtuluParser createExpression() {
    CtuluParser expression = new CtuluExpr().getParser();
    for (Entry<String, Object> entry : constantVars.entrySet()) {
      Variable var = expression.getVar(entry.getKey());
      if (var == null) {
        expression.addConstant(entry.getKey(), entry.getValue());
      } else {
        var.setValue(entry.getValue());
      }
    }

    return expression;
  }

  @Override
  public String getDescription() {
    return getFormule();
  }

  public String getFormule() {
    return formule;
  }

  public void getUsedVar(final Set _dest) {
    if (usedVar_ != null) {
      _dest.addAll(Arrays.asList(var_));
    }
  }

  @Override
  public double getValue(final int _idxTime, final int _idxObject) {
    CtuluParser expression = createExpression();
    Variable[] variables = createVariables(expression);
    // on enregistre les donnees pour chaque variable necessaire.
    for (int i = usedVar_.length - 1; i >= 0; i--) {
      try {
        variables[i].setValue(CtuluLib.getDouble(src_.getData(var_[i], _idxTime, _idxObject)));
      } catch (final IOException _e) {
        FuLog.warning(_e);
      }
    }
    return expression.getValue();

  }

  @Override
  public int hashCode() {
    return getFormule().hashCode();
  }

  @Override
  public boolean isEditable() {
    return true;
  }

  @Override
  public boolean isExpr() {
    return true;
  }

  @Override
  public TrPostFlecheContent isFleche() {
    return null;
  }

  /**
   * @param _var la constante modifiee
   * @return true si une telle constante est utils�e
   */
  @Override
  public boolean updateConstantVar(final Variable _var) {
    final String name = _var.getName();
    if (!constantVars.containsKey(name)) {
      return false;
    }
    constantVars.put(name, _var.getValue());
    return true;
  }
}
