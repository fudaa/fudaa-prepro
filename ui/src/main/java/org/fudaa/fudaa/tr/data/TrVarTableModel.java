/*
 * @creation 20 mars 2006
 * @modification $Date: 2007-04-16 16:35:33 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.data;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author fred deniger
 * @version $Id: TrVarTableModel.java,v 1.5 2007-04-16 16:35:33 deniger Exp $
 */
public class TrVarTableModel extends AbstractTableModel {
  final H2dVariableProviderInterface src_;

  final H2dVariableType[] vars_;

  public H2dVariableType getVarAt(final int _idxVar) {
    return vars_[_idxVar];
  }

  public TrVarTableModel(final H2dVariableProviderInterface _src, final List _varToAdd) {
    src_ = _src;
    final Set r = new HashSet(src_.getUsableVariables());
    if (_varToAdd != null) {
      r.addAll(_varToAdd);
    }
    vars_ = (H2dVariableType[]) r.toArray(new H2dVariableType[r.size()]);
    Arrays.sort(vars_);
  }

  @Override
  public int getColumnCount() {
    return 3 + vars_.length;
  }

  @Override
  public String getColumnName(final int _columnIndex) {
    switch (_columnIndex) {
    case 0:
      return MvResource.getS("Indice");
    case 1:
      return "X";
    case 2:
      return "Y";
    default:
    }
    final int var = _columnIndex - 3;
    if (var >= 0) { return vars_[var].getName(); }
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public int getRowCount() {
    return src_.isElementVar() ? src_.getGrid().getEltNb() : src_.getGrid().getPtsNb();
  }

  @Override
  public Class getColumnClass(final int _columnIndex) {
    return _columnIndex == 0 ? Integer.class : Double.class;
  }

  @Override
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {
    switch (_columnIndex) {
    case 0:
      return new Integer(_rowIndex + 1);
    case 1:
      if (src_.isElementVar()) { return CtuluLib.getDouble(src_.getGrid().getMoyCentreXElement(_rowIndex)); }
      return CtuluLib.getDouble(src_.getGrid().getPtX(_rowIndex));
    case 2:
      if (src_.isElementVar()) { return CtuluLib.getDouble(src_.getGrid().getMoyCentreYElement(_rowIndex)); }
      return CtuluLib.getDouble(src_.getGrid().getPtY(_rowIndex));
    default:
      // ne fait rien
    }
    final int var = _columnIndex - 3;
    if (var >= 0) { return CtuluLib.getDouble(src_.getViewedModel(vars_[var]).getValue(_rowIndex)); }
    return CtuluLibString.EMPTY_STRING;
  }

  public H2dVariableProviderInterface getSrc() {
    return src_;
  }

}
