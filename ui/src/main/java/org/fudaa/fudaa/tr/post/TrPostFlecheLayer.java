/**
 * @creation 18 nov. 2004 @modification $Date: 2007-06-13 12:58:13 $ @license GNU General Public License 2 @copyright (c)1998-2001
 *     CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuResource;
import org.locationtech.jts.geom.LinearRing;
import org.fudaa.ctulu.*;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.animation.EbliAnimationAdapterInterface;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.calque.find.CalqueFindFlecheExpression;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.controle.BSelecteurListTarget;
import org.fudaa.ebli.controle.BSelecteurListTimeTarget;
import org.fudaa.ebli.find.EbliFindExpressionComposite;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageAbstract;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.ebli.palette.PaletteManager;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetControllerCalque;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetVueCalque;
import org.fudaa.fudaa.meshviewer.layer.MVFlecheLayer;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.persistence.TrPostFlecheLayerPersistence;
import org.fudaa.fudaa.tr.post.data.TrPostDataListener;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.*;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: TrPostFlecheLayer.java,v 1.36 2007-06-13 12:58:13 deniger Exp $
 */
@SuppressWarnings("serial")
public class TrPostFlecheLayer extends MVFlecheLayer implements BSelecteurListTarget, BSelecteurListTimeTarget, ListSelectionListener,
    EbliAnimationAdapterInterface, ZCalqueSondeInterface, TrPostDataListener, TimeStepFormatAware {
  static int nameIdx;
  private boolean sonde_;
  private int sondeSelectedElement_ = -1;
  ListSelectionModel flecheSelection_;
  Map namePalette_;
  TraceIcon sondeIcone_;
  List<GrPoint> sondePt_;
  JMenuItem[] spec_;
  ListSelectionModel timeSelection_;
  TrPostFlecheModel initModel_;

  /**
   * @param _s la source utilisee pour construire le modele
   */
  public TrPostFlecheLayer(final TrPostSource _s) {
    super(new TrPostFlecheModel(_s));
    initModel_ = (TrPostFlecheModel) super.modele_;
    setTitle(TrResource.getS("Vecteurs"));
    sondePt_ = new ArrayList<GrPoint>();
    BCalqueCacheManager.installDefaultCacheManager(this);
    EbliWidgetControllerCalque.setLegendAddedIfVisible(this);
  }

  public final TrPostSource getSource() {
    return ((TrPostFlecheModel) modele_).s_;
  }

  public H2dVariableType getSelectedVar() {
    return initModel_.getSelectedVar();
  }

  @Override
  public BCalquePersistenceInterface getPersistenceMng() {
    return new TrPostFlecheLayerPersistence();
  }

  @Override
  protected String getFlecheUnit() {
    final H2dVariableType var = initModel_.getSelectedVar();
    return var == null ? CtuluLibString.EMPTY_STRING : var.getCommonUnitString();
  }

  protected void changeTimeStep() {
    if (timeSelection_ == null) {
      return;
    }
    final int i = timeSelection_.getMinSelectionIndex();
    initModel_.setTimeIdx(i);
    if (paletteCouleur_ != null) {
      paletteCouleur_.setSousTitre((String) initModel_.getTimeListModel().getElementAt(i));
    }
    updateLegendeTitre();
    fireSelectionEvent();
    firePropertyChange("t", true, false);
    updateCalqueInfo();
    repaint();
  }

  protected void updateCalqueInfo() {
    final String var = initModel_.getSelectedVarName();
    if (var == null) {
      putCalqueInfo(null);
    } else {
      final String time = initModel_.getSelectedTime();
      putCalqueInfo(time == null ? var : (var + CtuluLibString.ESPACE + time));
    }
  }

  protected void changeVarStep() {
    final int i = flecheSelection_.getMinSelectionIndex();
    if (isPaletteCouleurUsed_) {
      initPaletteMap();
      final String v = initModel_.getSelectedVarName();
      if (CtuluLibString.isEmpty(v)) {
        final BPalettePlage s = (BPalettePlage) namePalette_.get(v);
        if (s != paletteCouleur_) {
          namePalette_.put(v, paletteCouleur_);
        }
      }
    }
    initModel_.setVarIdx(i);
    if (getTimeListSelectionModel().isSelectionEmpty() && getTimeListModel().getSize() > 0) {
      getTimeListSelectionModel().setSelectionInterval(0, 0);
    }
    updateLegende();
    updateCalqueInfo();
    // pour mettre a jour le panneau d'info
    fireSelectionEvent();
    repaint();
  }

  /*
   * protected TrPostFlecheModel getF() { return (TrPostFlecheModel) modele_; }
   */
  protected void paintSonde(final Graphics2D _g2d, final GrMorphisme _versEcran) {
    if (sondeSelectedElement_ >= 0) {

      for (final GrPoint point : sondePt_) {

        final GrPoint p = point.applique(_versEcran);
        if (sondeIcone_ == null) {
          sondeIcone_ = TrIsoLayerDefault.getSondeIcone();
        }
        // _g2d.setXORMode(Color.WHITE);
        sondeIcone_.paintIconCentre(this, _g2d, p.x_, p.y_);
      }
    }
  }

  @Override
  protected void setPaletteCouleur(final BPalettePlageAbstract _paletteCouleur) {
    if (_paletteCouleur == null) {
      return;
    }
    super.setPaletteCouleur(_paletteCouleur);
    updateLegendeTitre();
  }

  protected void setSelectedVar(final H2dVariableType _t) {
    getListSelectionModel().clearSelection();
    for (int i = getListModel().getSize() - 1; i >= 0; i--) {
      final TrPostFlecheContent newFleche = (TrPostFlecheContent) getListModel().getElementAt(i);
      if (newFleche.getVar() == _t) {
        getListSelectionModel().setSelectionInterval(i, i);
        return;
      }
    }
  }

  @Override
  public void timeStepFormatChanged() {
    changeTimeStep();
  }

  @Override
  protected void construitLegende() {
    super.construitLegende();
  }

  protected void updateLegende() {
    if (!isPaletteCouleurUsed_) {
      if (paletteCouleur_ != null) {
        TrIsoLayerDefault.initSubTitleLabel(paletteCouleur_);
        updateTitre(paletteCouleur_);
        updateLegendeTitre();
      }
      return;
    }
    initPaletteMap();
    final String v = initModel_.getSelectedVarName();
    if (CtuluLibString.isEmpty(v)) {
      return;
    }
    BPalettePlage s = (BPalettePlage) namePalette_.get(v);
    if (s == null) {
      s = new BPalettePlage();
      updateTitre(s);
      TrIsoLayerDefault.initSubTitleLabel(s);
      final CtuluRange r = initModel_.getExtremaForTimeStep();
      if (r.max_ - r.min_ < 0.01) {
        s.initPlages(1, r.min_, r.max_);
      } else {
        s.initPlages(10, r.min_, r.max_);
      }
      s.initCouleurs(PaletteManager.INSTANCE);
      namePalette_.put(v, s);
    } else {
      s.setSousTitre(initModel_.getSelectedTime());
      // updateLegendeTitre();
    }
    setPaletteCouleur(s);
  }

  protected void updateTitre(final BPalettePlageAbstract _plage) {
    _plage.setSousTitre(initModel_.getSelectedTime());
    _plage.setTitre(initModel_.getSelectedVarName());
  }

  void initPaletteMap() {
    if (namePalette_ == null) {
      namePalette_ = new HashMap(initModel_.getListModel().getSize() + 1);
    }
  }

  @Override
  public boolean changeSonde(final GrPoint _pt, final boolean add) {
    if (!isSondeEnable()) {
      return false;
    }
    if (sondePt_ == null) {
      sondePt_ = new ArrayList<GrPoint>();
      // sondePt_ = new GrPoint();
    }
    final GrPoint point = new GrPoint();

    point.initialiseAvec(_pt);

    if (!add) {
      sondePt_.clear();
    }
    sondePt_.add(point);

    final int i = TrIsoLayerDefault.sondeSelection(point, initModel_);
    final boolean oldIsDraw = sondeSelectedElement_ >= 0;
    sondeSelectedElement_ = i;
    if (sondeSelectedElement_ >= 0) {
      paintSonde((Graphics2D) getGraphics(), getVersEcran());
    } else if (oldIsDraw) {
      repaint();
    }
    // pour le panel d'info
    fireSelectionEvent();
    return sondeSelectedElement_ >= 0;
  }

  public JMenuItem createDuplicateItem() {
    final BuMenuItem it = new BuMenuItem(BuResource.BU.getIcon("dupliquer"), BuResource.BU.getString("dupliquer")) {
      @Override
      protected void fireActionPerformed(final ActionEvent _event) {
        super.fireActionPerformed(_event);
        final BCalque c = (BCalque) TrPostFlecheLayer.this.getParent();
        final Component[] cs = c.getCalques();
        int i = -1;
        for (i = 0; i < cs.length; i++) {
          if (TrPostFlecheLayer.this.equals(cs[i])) {
            break;
          }
        }
        if (i < 0 || i == (cs.length - 1)) {
          c.enDernier(duplicate());
        } else if (i == 0) {
          c.enPremier(duplicate());
        } else {
          c.add(duplicate(), i + 1);
        }
      }
    };
    return it;
  }

  @Override
  public void dataAdded(final boolean _isFleche) {
    H2dVariableType currentVar = ((TrPostFlecheModel) modele_).getSelectedVar();
    //we must update the selection
    if (currentVar != TrIsoLayerDefault.getCurrentSelection(this)) {
      int idx = TrIsoModelAbstract.getVarIdx(currentVar.getID(), getListModel());
      if (idx < 0) {
        idx = 0;
      }
      if (idx >= 0) {
        getListSelectionModel().setSelectionInterval(idx, idx);
      }
    }
  }

  @Override
  public void dataChanged(final H2dVariableType _old, final H2dVariableType _new, final boolean _contentChanged,
                          final boolean _isFleche,
                          final Set _varDepending) {
    if (!_isFleche) {
      return;
    }
    if (_old == initModel_.current_.getVar() || _varDepending.contains(initModel_.current_.getVar())) {
      // la variable en cours d'affichage est modifi�e
      if (_old != _new && _old == initModel_.current_.getVar()) {
        setSelectedVar(_new);
      } else {
        // pour remettre a jour les valeurs
        initModel_.setVarIdx(getListSelectionModel().getMaxSelectionIndex());
        updateLegendeTitre();
        fireSelectionEvent();
      }
      firePropertyChange(EbliWidgetVueCalque.CLEAR_CACHE, Boolean.TRUE, Boolean.FALSE);
    }
  }

  @Override
  public void dataRemoved(final H2dVariableType[] _vars, final boolean _isFleche) {
    // si la variable en cours a ete supprimee, on deselectionne l'affichage....
    if (_vars != null && initModel_.current_ != null && CtuluLibArray.findObjectEgalEgal(_vars, initModel_.current_.getVar()) >= 0) {
      getListSelectionModel().clearSelection();
      firePropertyChange(EbliWidgetVueCalque.CLEAR_CACHE, Boolean.TRUE, Boolean.FALSE);
    }
  }

  public MVFlecheLayer duplicate() {
    final TrPostFlecheLayer r = new TrPostFlecheLayer(initModel_.s_);
    r.initFrom(saveUIProperties());
    if (!getTimeListSelectionModel().isSelectionEmpty()) {
      r.initModel_.setTimeIdx(initModel_.getTimeIdx());
    }
    if (!getListSelectionModel().isSelectionEmpty()) {
      r.initModel_.setVarIdx(getListSelectionModel().getMaxSelectionIndex());
    }
    r.setName(getName() + '-' + nameIdx++);
    r.setTitle(getTitle() + CtuluLibString.ESPACE + CtuluLib.getS("Copie"));
    r.setLegende(getLegende());
    r.setDestructible(true);
    r.setActions(getActions());
    return r;
  }

  @Override
  public void fillWithInfo(final InfoData _m) {
    if (isSondeActive()) {
      final int i = sondePt_.size() - 1;
      initModel_.fillInterpolateInfo(_m, sondeSelectedElement_, sondePt_.get(i).x_, sondePt_.get(i).y_, getTitle());
    } else if (!isGrilleActivated()) {

      initModel_.fillWithInfo(_m, this);
    }
    super.fillWithInfo(_m);
  }

  @Override
  public BConfigurableInterface getSingleConfigureInterface() {
    return new BConfigurableComposite(new BConfigurableInterface[]{getAffichageConf(),
        new TrPostFilterConfigure(initModel_.s_, this,
            initModel_.getCond())}, null);
  }

  @Override
  public void setVisible(final boolean _v) {
    if (_v && getListSelectionModel().isSelectionEmpty() && getListModel().getSize() > 0) {
      int last = getTimeListModel().getSize() - 1;
      getTimeListSelectionModel().setSelectionInterval(last, last);
      last = initModel_.getIdxOfVelocity();
      if (last < 0) {
        last = 0;
      }
      getListSelectionModel().setSelectionInterval(last, last);
    }
    super.setVisible(_v);
    if (_v) {
      boolean shouldCreateAndUpdateLegend = (paletteCouleur_ == null);
      if (shouldCreateAndUpdateLegend) {
        paletteCouleur_ = new BPalettePlage();
      }
      construitLegende();
      if (shouldCreateAndUpdateLegend) {
        updateLegende();
      }
    }
  }

  @Override
  public int getElementSonde() {
    return sondeSelectedElement_;
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    if (isGrilleActivated()) {
      return super.getExpressionContainer();
    }
    return new EbliFindExpressionComposite(new CalqueFindFlecheExpression(initModel_, true), new TrPostExprFlecheSupplier(
        initModel_));
  }

  @Override
  public ListModel getListModel() {
    return initModel_.getListModel();
  }

  @Override
  public ListSelectionModel getListSelectionModel() {
    if (flecheSelection_ == null) {
      flecheSelection_ = new DefaultListSelectionModel();
      flecheSelection_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      flecheSelection_.addListSelectionListener(this);
    }
    return flecheSelection_;
  }

  @Override
  public int getNbTimeStep() {
    return initModel_.getTimeListModel().getSize();
  }

  @Override
  public boolean getRange(final CtuluRange _b) {
    return initModel_.getDataRange(_b);
  }

  @Override
  public double getSondeX() {
    return sondePt_ == null ? 0 : sondePt_.get(sondePt_.size() - 1).x_;
  }

  @Override
  public double getSondeY() {
    return sondePt_ == null ? 0 : sondePt_.get(sondePt_.size() - 1).y_;
  }

  @Override
  protected void buildSpecificMenuItemsForAction(final List _l) {
    _l.add(createDuplicateItem());
  }

  public JComponent getTargetComponent() {
    return this;
  }

  public int getTimeIdx() {
    return initModel_.timeIdx_;
  }

  @Override
  public ListModel getTimeListModel() {
    return initModel_.getTimeListModel();
  }

  private DefaultListSelectionModel createTimeSelectionModel() {
    return TrIsoLayer.createTimeSelectionModel(this);
  }

  @Override
  public ListSelectionModel getTimeListSelectionModel() {
    if (timeSelection_ == null) {
      timeSelection_ = createTimeSelectionModel();
      timeSelection_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      timeSelection_.addListSelectionListener(this);
    }
    return timeSelection_;
  }

  @Override
  public boolean getTimeRange(final CtuluRange _b) {
    return initModel_.getTimeDataRange(_b);
  }

  @Override
  public String getTimeStep(final int _idx) {
    return (String) initModel_.getTimeListModel().getElementAt(_idx);
  }

  @Override
  public double getTimeStepValueSec(final int _idx) {
    return initModel_.getTimeStep(_idx);
  }

  public JComponent getTimeTargetComponent() {
    return this;
  }

  public boolean isBase() {
    return !isDestructible();
  }

  @Override
  public boolean isDonneesBoiteTimeAvailable() {
    return true;
  }

  @Override
  public boolean isPointSondable(final GrPoint _reel) {
    return TrIsoLayerDefault.sondeSelection(_reel, initModel_) >= 0;
  }

  @Override
  public boolean isSondeActive() {
    return isSondeEnable() && sondeSelectedElement_ >= 0;
  }

  /**
   * @return true si la sonde est active
   */
  @Override
  public boolean isSondeEnable() {
    return sonde_;
  }

  @Override
  public boolean isTitleModifiable() {
    return true;
  }

  @Override
  public int[] getSelectedElementIdx() {
    if (isSelectionElementEmpty()) {
      return null;
    }
    return getSelectedIndex();
  }

  @Override
  public int[] getSelectedPtIdx() {
    if (isSelectionPointEmpty()) {
      return null;
    }
    return getSelectedIndex();
  }

  @Override
  public boolean isSelectionElementEmpty() {
    boolean isElement = initModel_.getData() != null && initModel_.getData().isElementData();
    return !isElement || isGrilleActivated() || isSelectionEmpty();
  }

  @Override
  public boolean isSelectionPointEmpty() {
    boolean isElement = initModel_.getData() != null && initModel_.getData().isElementData();
    return isElement || isSelectionEmpty() || isGrilleActivated();
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel, final GrBoite _clipReel) {
    if (modele_ != null) {
      modele_.prepare();
    }
    super.paintDonnees(_g, _versEcran, _versReel, _clipReel);
  }

  @Override
  public void paintTransient(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
    paintSonde(_g, _versEcran);
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    final EbliUIProperties res = super.saveUIProperties();
    TrIsoLayerDefault.savePalettes(namePalette_, res);
    TrIsoLayerDefault.saveVar(res, this);
    TrIsoLayer.saveTime(res, this, initModel_.s_);
    return res;
  }

  public int getVarIdx(final String _varIdx) {
    return TrIsoModelAbstract.getVarIdx(_varIdx, getListModel());
  }

  @Override
  public void initFrom(final EbliUIProperties _p) {
    if (_p != null) {
      getListSelectionModel().clearSelection();
      super.initFrom(_p);
      initPaletteMap();
      if (TrIsoLayerDefault.restorePalette(_p, namePalette_)) {
        updateLegende();
      }
      TrIsoLayerDefault.restoreVar(_p, this, H2dVariableType.VITESSE.getID());
      TrIsoLayer.restoreTime(_p, this, initModel_.s_);
    }
  }

  /**
   * Ne pas utiliser.
   */
  @Override
  public void setModele(final ZModeleSegment _s) {
    throw new IllegalArgumentException("Do not use");
  }

  /*
   * public EbliListeSelection selectionElement(GrPoint _pt,int _tolerance){ if (!isVisible()) return null; int i =
   * TrIsoLayerDefault.selectionElement(_pt, _tolerance, getVersEcran(), getF(), getClipReel(getGraphics())); if (i >= 0) {
   * EbliListeSelection r = creeSelection(); r.add(i); return r; } return null; }
   */
  @Override
  public void setPaletteCouleurPlages(final BPalettePlageInterface _newPlage) {
    if (getListSelectionModel().isSelectionEmpty()) {
      return;
    }
    if (_newPlage == null) {
      return;
    }
    // final boolean old = isPaletteCouleurUsed_;
    super.setPaletteCouleurPlages(_newPlage);
    // si c'est la premiere fois, il faut mettre � jour les titres
    if (paletteCouleur_ != null) {
      updateTitre(paletteCouleur_);
      updateLegende();
      TrIsoLayerDefault.initSubTitleLabel(paletteCouleur_);
    }
  }

  /**
   * @param _enable nouvel etat de l'outil sonde
   */
  @Override
  public void setSondeEnable(final boolean _enable) {
    if (_enable != sonde_) {
      sonde_ = _enable;

      if (sonde_) {
        clearSelection();
      } else {
        clearSonde();
      }
    }
  }

  @Override
  public void clearSonde() {
    sondeSelectedElement_ = -1;
    if (sondePt_ != null) {
      sondePt_.clear();
    }
    // pour mettre a jour le panel d'info
    fireSelectionEvent();
  }

  @Override
  public boolean changeSelection(CtuluListSelection s, int action) {
    clearSonde();
    return super.changeSelection(s, action);
  }

  @Override
  public boolean changeSelection(GrPoint pt, int tolerancePixel, int action) {
    clearSonde();
    return super.changeSelection(pt, tolerancePixel, action);
  }

  @Override
  public boolean changeSelection(LinearRing poly, int action, int mode) {
    clearSonde();
    return super.changeSelection(poly, action, mode);
  }

  @Override
  public boolean changeSelection(LinearRing[] p, int action, int mode) {
    clearSonde();
    return super.changeSelection(p, action, mode);
  }

  @Override
  public void setTimeStep(final int _idx) {
    getTimeListSelectionModel().setSelectionInterval(_idx, _idx);
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    if (_e.getValueIsAdjusting()) {
      return;
    }
    if (_e.getSource() == timeSelection_) {
      changeTimeStep();
    } else {
      changeVarStep();
    }
  }

  @Override
  public List<GrPoint> getLigneBriseeFromSondes() {
    return this.sondePt_;
  }
}
