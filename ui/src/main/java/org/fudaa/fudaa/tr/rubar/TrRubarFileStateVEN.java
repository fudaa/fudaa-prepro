/**
 *
 */
package org.fudaa.fudaa.tr.rubar;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.rubar.H2dRubarVentListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarVentMng;
import org.fudaa.dodico.mesure.EvolutionListener;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.rubar.io.RubarVENFileFormat;

import java.io.File;

/**
 * @author CANEL Christophe (Genesis)
 */
//TODO Voir comment d�t�cter les modifications.
public class TrRubarFileStateVEN extends TrRubarFileState implements H2dRubarVentListener, EvolutionListener {
  private final H2dRubarVentMng ventMng;

  /**
   * @param _l le listener
   * @param _p les parametres
   */
  public TrRubarFileStateVEN(final TrRubarFileStateMng _l, final H2dRubarParameters _p) {
    super(new RubarVENFileFormat(), _l);
    ventMng = _p.getVentMng();
    ventMng.addVentListener(this);
    if (_p.getVentMng().isSet()) {
      _l.add(this);
    }
    _p.getBcMng().addEvolutionListener(this);
  }

  @Override
  public void evolutionChanged(EvolutionReguliereInterface _e) {
    if (ventMng.isUsed(_e)) {
      setModified(true);
    }
  }

  @Override
  public void evolutionUsedChanged(EvolutionReguliereInterface _e, int _old, int _new, boolean _isAdjusting) {

  }

  @Override
  public void ventChanged(final H2dRubarVentMng _mng) {

    if (_mng.isSet()) {
      setMarkRemoved(false);
      if (((TrRubarFileStateMng) l_).contains(fmt_) == null) {
        ((TrRubarFileStateMng) l_).add(this);
      }
    } else {
      setMarkRemoved(true);
    }
    setModified(true);
  }

  @Override
  public void ventEvolutionContentChanged(final H2dRubarVentMng _mng, final EvolutionReguliereInterface _dest) {
    setModified(true);
  }

  @Override
  public void ventEvolutionUsedChanged(final H2dRubarVentMng _mng, final EvolutionReguliereInterface _dest) {

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CtuluIOOperationSynthese save(File _dir, String _projectName, ProgressionInterface _progression, TrRubarProject _projet) {
    final File f = fmt_.getFileFor(_dir, _projectName);
    return ((FileFormatUnique) fmt_).write(f, _projet.getH2dRubarParameters().getVentMng().getSavedInterface(), _progression);
  }
}
