/*
 * @creation 24 mars 2004
 * 
 * @modification $Date: 2007-06-20 12:23:39 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuLib;
import java.io.File;
import org.fudaa.ctulu.CtuluLibGenerator;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinVolumeFileFormat;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.reflux.io.RefluxRefondeSolutionSequentielResult;
import org.fudaa.fudaa.tr.common.TrFileFormatManager;

/**
 * refonte de TrPostSourceActivator poru gerer plusieurs source au sein du meme post. La difference avec le
 * sourceActivator c'est qu'ici on ajoute toutes les sources a un meme TrPostProjet afin de gere le multi source au sein
 * du trpojet.
 * 
 * @author Adrien Hadoux
 */
public final class TrPostMultiSourceActivator {

  public TrPostProjet projetCourant_;

  public TrPostMultiSourceActivator(final TrPostProjet _projet) {
    super();
    projetCourant_ = _projet;

  }

  protected static void afficheFrame(final TrPostProjet _p, final TrPostCommonImplementation _impl) {
    afficheFrame(_p, _impl, null);
  }

  /**
   * @param _impl l'impl parente
   */
  protected static void afficheFrame(final TrPostProjet _p, final TrPostCommonImplementation _impl, final Runnable runnable) {
    BuLib.invokeNow(new Runnable() {

      @Override
      public void run() {
        _impl.setProjet(_p);
        if (runnable != null) {
          runnable.run();
        }
      }
    });
  }

  public static void activeFile(final File _f, final TrPostCommonImplementation _impl) {
    TrPostProjet pro = _impl.getCurrentProject();
    if (pro == null) {
      pro = new TrPostProjet(_impl);
    }
    new TrPostMultiSourceActivator(pro).active(_f, _impl);

  }

  /**
   * @param _f le fichier a ouvrir
   * @param _impl l'impl parent
   */
  public void activeSerafin(final File _f, final TrPostCommonImplementation _impl, final String id, final boolean isVolume) {
    new CtuluTaskOperationGUI(_impl, CtuluLibString.ESPACE) {

      @Override
      public void act() {
        final TrPostSource source = activeSerafinAction(_f, _impl, _impl.createProgressionInterface(this), isVolume);
        if (source == null) {
          return;
        }
        source.setId(id);
        if (SerafinFileFormat.is3DGrid(source.getGrid())) {
          BuLib.invokeLater(new Runnable() {

            @Override
            public void run() {
              TrPostSourceBuilder.affiche3D(source, _impl);
            }

          });
        } else {
          afficheFrame(projetCourant_, _impl, null);
        }
      }
    }.start();
  }

  public void activeGrid(final File _f, final FileFormatGridVersion _fmt, final TrPostCommonImplementation _impl, final String id) {
    new CtuluTaskOperationGUI(_impl, CtuluLibString.ESPACE) {

      @Override
      public void act() {
        activeGridAction(_f, _fmt, _impl, _impl.createProgressionInterface(this), id);
        if (projetCourant_ != null) {
          afficheFrame(projetCourant_, _impl);
        }
      }
    }.start();
  }

  public void active(final File _f, final TrPostCommonImplementation _impl) {

    String uniqueId;
    if (_impl.project != null) {
      uniqueId = _impl.project.deliverSourceId(_f.getName());
    } else {
      uniqueId = CtuluLibGenerator.getInstance().deliverUniqueStringId();
    }

    active(_f, _impl, uniqueId);
  }

  /**
   * Ouvre le fichier en fonction de l'extension.
   * 
   * @param _f le fichier correct
   * @param _impl l'impl parent
   */
  public void active(final File _f, final TrPostCommonImplementation _impl, final String IdSourceToActive) {
    if (_f == null) {
      return;
    }
    final TrFileFormatManager mng = TrFileFormatManager.INSTANCE;
    final String name = _f.getName();
    if (TrPostSourceBuilder.isReflux(name)) {
      activeINP(_f, _impl, IdSourceToActive);
      return;
    }
    if (mng.isRubarTPSFile(name)) {
      activeRubar(_f, _impl, IdSourceToActive);
      return;
    }

    // -- test format volumique --//
    if (SerafinVolumeFileFormat.getInstance().createFileFilter().accept(_f)) {
      activeSerafin(_f, _impl, IdSourceToActive, true);
      return;
    }

    // mettre serafin en dernier car le test est tres large ....
    if (SerafinFileFormat.getInstance().createFileFilter().accept(_f)) {
      activeSerafin(_f, _impl, IdSourceToActive, false);
      return;
    }
    final FileFormat ft = FileFormat.findFileFormat(TrFileFormatManager.getAllGridFormat(), _f);
    if (ft == null) {
      final String id = _impl.getLauncher().getCurrentPrefHydId();
      if (FileFormatSoftware.REFLUX_IS.name.equals(id)) {
        activeINP(_f, _impl, IdSourceToActive);
      } else if (FileFormatSoftware.TELEMAC_IS.name.equals(id)) {
        activeSerafin(_f, _impl, IdSourceToActive, false);
      } else if (FileFormatSoftware.RUBAR_IS.name.equals(id)) {
        activeRubar(_f, _impl, IdSourceToActive);
      }
    } else {
      activeGrid(_f, (FileFormatGridVersion) ft, _impl, IdSourceToActive);
    }
  }

  /**
   * Ouvir une boite de dialogue pour determiner le fichier inp et le fichier sov.
   * 
   * @param _f le fichier xinp,sov ou inp
   * @param _impl l'impl parent
   */
  public void activeINP(final File _f, final TrPostCommonImplementation _impl, final String id) {
    if (RefluxRefondeSolutionSequentielResult.isRefonde(_f)) {
      final File inp = TrPostSourceBuilder.getInpFileForRefonde(_f, _impl);
      if (inp != null) {
        activeINP(inp, _f, _impl, id);
      }
      return;
    }
    final TrPostSourceBuilder.INPChooseFile pn = new TrPostSourceBuilder.INPChooseFile(_f);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_impl.getFrame()))) {
      activeINP(pn.getInp(), pn.getsov(), _impl, id);
    }
  }

  /**
   * @param _f le fichier tps
   * @param _impl l'impl parente
   */
  public void activeRubar(final File _f, final TrPostCommonImplementation _impl, final String id) {
    new CtuluTaskOperationGUI(_impl, CtuluLibString.ESPACE) {

      @Override
      public void act() {
        final ProgressionInterface prog = _impl.createProgressionInterface(this);
        final TrPostSourceRubar src = activeRubarAction(_f, _impl, prog);
        if (src != null) {
          src.setId(id);
        }
        final TrPostRubarLoader loader = new TrPostRubarLoader(src, _f, true);
        loader.active(projetCourant_, _impl, prog);
        afficheFrame(projetCourant_, _impl);

      }
    }.start();
  }

  /**
   * @param _f le fichier inp
   * @param _sov le fichier sov
   * @param _impl l'impl parente
   */
  public void activeINP(final File _f, final File _sov, final TrPostCommonImplementation _impl, final String id) {
    new CtuluTaskOperationGUI(_impl, CtuluLibString.ESPACE) {

      @Override
      public void act() {
        final TrPostSource activeINPAction = activeINPAction(_f, _sov, _impl, _impl.createProgressionInterface(this), id);
        if (projetCourant_ != null) {
          afficheFrame(projetCourant_, _impl);
        }
      }
    }.start();
  }

  /**
   * @param _f le fichier serafin
   * @param _impl l'impl parent
   * @param _inter la barre de progress
   * @return le source post
   */
  public TrPostSource activeSerafinAction(final File _f, final TrPostCommonImplementation _impl, final ProgressionInterface _inter,
      final boolean isVolume) {
    final TrPostSource r = TrPostSourceBuilder.activeSerafinSourceAction(_f, _impl, _inter, null, isVolume);
    if (r != null) {

      this.projetCourant_.addSource(r);
    }
    return r;
  }

  /**
   * @param _f le fichier serafin
   * @param _impl l'impl parent
   * @param _inter la barre de progress
   * @return le source post
   */
  public TrPostSourceRubar activeRubarAction(final File _f, final TrPostCommonImplementation _impl, final ProgressionInterface _inter) {
    final TrPostSourceRubar src = TrPostSourceBuilder.activeRubarSrcAction(_f, _impl, _inter);
    if (src == null) {
      return null;
    }
    TrPostSourcesManager sources = projetCourant_.getSources();
    if (!sources.isOneSourceLoaded(src.getFiles())) {
      this.projetCourant_.addSource(src);
    }

    return src;
  }

  public static void loadData(final CtuluUI _impl, final ProgressionInterface _inter, final TrPostProjet _proj) {
    if (_proj != null && _proj.openSrcDataAndIsModified(_impl, _inter)) {
      _proj.setProjectModified();
    }
  }

  /**
   * @param _gridFile le fichier inp ou ximp
   * @param _sovFile le fichier contenant les solutions
   * @param _impl l'impl parent
   * @param _inter la barre de progression
   * @return le source
   */
  public TrPostSource activeINPAction(final File _gridFile, final File _sovFile, final CtuluUI _impl, final ProgressionInterface _inter,
      final String id) {

    final TrPostSourceReflux retour = (TrPostSourceReflux) TrPostSourceBuilder.activeINPSource(_gridFile, _sovFile, _impl, _inter);
    if (retour == null) {
      return retour;
    }
    retour.setId(id);

    projetCourant_.addSource(retour);

    loadData(_impl, _inter, projetCourant_);
    return retour;
  }

  public TrPostSource activeGridAction(final File _f, final FileFormatGridVersion _ftGrid, final TrPostCommonImplementation _impl,
      final ProgressionInterface _inter, final String id) {

    final TrPostSource retour = TrPostSourceBuilder.activeGridSource(_f, _ftGrid, _impl, _inter);
    if (retour == null) {
      return null;
    }
    retour.setId(id);
    this.projetCourant_.addSource(retour);

    loadData(_impl, _inter, projetCourant_);
    return retour;
  }

}
