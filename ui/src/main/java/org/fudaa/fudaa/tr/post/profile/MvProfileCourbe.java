/*
 * @creation 21 nov. 06
 * @modification $Date: 2007-02-02 11:22:11 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGCourbePersistBuilder;
import org.fudaa.ebli.courbe.EGGrapheDuplicator;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.courbe.EGModelDecorator;
import org.fudaa.ebli.courbe.EGParent;

/**
 * @author fred deniger
 * @version $Id: MvProfileCourbe.java,v 1.4 2007-02-02 11:22:11 deniger Exp $
 */
public class MvProfileCourbe extends EGCourbeChild implements MvProfileCourbeInterface {

  public MvProfileCourbe(final EGGroup _m, final MVProfileCourbeModel _model) {
    super(_m, _model);
  }
  
  @Override
  protected EGCourbePersistBuilder<? extends EGCourbeChild> createPersistBuilder() {
    return super.createPersistBuilder();
  }

  @Override
  public MvProfileCourbeModelInterface getProfileModel() {
    return (MVProfileCourbeModel) super.getModel();
  }

  protected MVProfileCourbeModel getM() {
    return (MVProfileCourbeModel) super.getModel();
  }
  
  @Override
  public EGCourbe duplicate(EGParent newParent, EGGrapheDuplicator duplicator) {
    MvProfileCourbe courbe=new MvProfileCourbe((MvProfileCourbeGroup) getParent(), getM().duplicate());
    courbe.initGraphicConfigurationFrom(this);
    return courbe;
  }

  /**
   * POur l'instant: comment mettre a jour les pas de temps.
   */
  @Override
  public boolean isTitleModifiable() {
    return true;
  }

  @Override
  public void setModel(final EGModel _model) {
    if (!(_model instanceof MVProfileCourbeModel)  ){
    	
    	if(!(_model instanceof EGModelDecorator) )
    		{ throw new IllegalAccessError(); }
    }
    super.setModel(_model);
  }

  public void setVar(final CtuluVariable _v, final boolean _contentChanged) {
    getM().setVariable(_v, _contentChanged);
    if (_v != getM().getVariable()) {
      setTitle(_v.getName());
    }
    fireCourbeContentChanged();

  }

}
