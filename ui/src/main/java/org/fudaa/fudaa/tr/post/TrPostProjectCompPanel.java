/*
 *  @creation     8 d�c. 2005
 *  @modification $Date: 2007-05-04 14:01:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.*;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.ButtonGroup;
import javax.swing.JDialog;
import javax.swing.JLabel;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.common.TrSuffixeDocListener;

/**
 * @author Fred Deniger
 * @version $Id: TrPostProjectCompPanel.java,v 1.13 2007-05-04 14:01:51 deniger Exp $
 */
public class TrPostProjectCompPanel extends CtuluDialogPanel {

  private final BuCheckBox cbAfficheCompGrid_;
  private BuRadioButton cbInverse_;
  BuButton btVerifyGrid_;
  BuCheckBox cbGridEquals_;
  BuCheckBox cbOnImpGrid_;

  BuCheckBox cbOnImpTime_;

  JDialog parent_;

  final EfGridInterface proj_;

  final EfGridInterface ref_;

  BuTextField tfSuff_;

  final CtuluUI ui_;

  List usedVar_;

  BuList varToImport_;

  /**
   * @param _ref
   * @param _proj
   */
  public TrPostProjectCompPanel(final EfGridInterface _ref, final EfGridInterface _proj, final String _isGridDiff,
      final CtuluUI _ui) {
    super();
    proj_ = _proj;
    ui_ = _ui;
    ref_ = _ref;
    // maillage egaux
    setLayout(new BuVerticalLayout(5));
    final BuPanel pnGrid = new BuPanel();
    pnGrid.setBorder(CtuluLibSwing.createTitleBorder(MvResource.getS("Maillages"), BuBorders.EMPTY2222));
    if (_isGridDiff == null) {
      pnGrid.setLayout(new BuGridLayout(2, 2, 2, false, true));
      cbGridEquals_ = new BuCheckBox(MvResource.getS("Les maillages sont identiques"));
      pnGrid.add(cbGridEquals_);
      btVerifyGrid_ = new BuButton(TrResource.getS("V�rifier"));
      btVerifyGrid_.setToolTipText(MvResource.getS("Permet de v�rifier que les maillages sont bien identiques"));
      btVerifyGrid_.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent _e) {
          verif();
        }
      });
      pnGrid.add(btVerifyGrid_);
    } else {
      pnGrid.setLayout(new BuVerticalLayout(2));
      addLabel(pnGrid, new CtuluHtmlWriter(true).addTagAndText("u", MvResource.getS("Les maillages sont diff�rents:"))
          .getHtml());
      addLabel(pnGrid, CtuluLibString.ESPACE + CtuluLibString.ESPACE + _isGridDiff);
    }
    cbAfficheCompGrid_ = new BuCheckBox(TrResource.getS("Afficher les 2 maillages"));
    pnGrid.add(cbAfficheCompGrid_);
    add(pnGrid);
  }

  public static BuPanel createVarPn(final String _s) {
    final BuPanel pnVar = new BuPanel();
    pnVar.setBorder(CtuluLibSwing.createTitleBorder(_s, BuBorders.EMPTY2222));
    return pnVar;
  }

  List verifNameUsed(final H2dVariableType[][] _selectedVar) {
    if (_selectedVar == null) { return null; }
    if (usedVar_ == null || usedVar_.size() == 0) { return Collections.EMPTY_LIST; }
    final Set usedName = new HashSet(usedVar_.size());
    final Set usedShortName = new HashSet(usedVar_.size());
    TrPostUserVariableSaver.fillWithForbidden(usedVar_, usedName, usedShortName);
    return TrPostUserVariableSaver.getAlreadyUsedVar(Arrays.asList(_selectedVar[0]), usedName, usedShortName);
  }

  public  final void setParent(final JDialog _parent) {
    parent_ = _parent;
  }

  protected void verif() {
    if (cbGridEquals_ != null && cbGridEquals_.isEnabled()) {
      final BuGlassPaneStop stop = new BuGlassPaneStop();
      if (parent_ != null) {
        parent_.setGlassPane(stop);
      }
      final CtuluTaskDelegate task = ui_.createTask(TrResource.getS("V�rifier"));
      final Runnable r = new Runnable() {

        @Override
        public void run() {
          final boolean res = ref_.isSameStrict(proj_, task.getStateReceiver(), 0);
          final Runnable swing = new Runnable() {

            @Override
            public void run() {
              ui_.message(TrResource.getS("V�rifier"), MvResource.getS(res ? "Les maillages sont identiques"
                  : "Les maillages ne sont pas identiques"), false);
              btVerifyGrid_.setEnabled(false);
              cbGridEquals_.setEnabled(false);
              cbGridEquals_.setSelected(res);
              if (parent_ != null) {
                parent_.getRootPane().remove(stop);
              }
            }
          };
          BuLib.invokeLater(swing);

        }
      };
      task.start(r);

    }
  }

  public void addComparePanel() {
    final BuPanel pnCompare = createVarPn(CtuluLib.getS("Comparer"));
    pnCompare.setLayout(new BuVerticalLayout(3));
    cbOnImpGrid_ = new BuCheckBox(TrResource.getS("Comparer sur le maillage du projet import�"));
    cbOnImpTime_ = new BuCheckBox(TrResource.getS("Comparer sur les pas de temps du projet import�"));
    pnCompare.add(cbOnImpGrid_);
    pnCompare.add(cbOnImpTime_);
    add(pnCompare);
    final BuPanel pnVar = createVarPn(CtuluLib.getS("Variables"));
    pnVar.setLayout(new BuVerticalLayout(2));
    final ButtonGroup bg = new ButtonGroup();
    final BuRadioButton normal = new BuRadioButton(TrResource.getS("Calculer 'projet import�' - 'projet courant'"));
    cbInverse_ = new BuRadioButton(TrResource.getS("Calculer 'projet courant' - 'projet import�'"));
    final Insets insets = new Insets(0, 5, 0, 2);
    normal.setMargin(insets);
    cbInverse_.setMargin(insets);
    bg.add(normal);
    bg.add(cbInverse_);
    normal.setSelected(true);
    addLabel(pnVar, TrResource.getS("Les variables communes aux 2 projets vont �tre compar�es."));
    addLabel(pnVar, TrResource.getS("Choisir la mani�re de calculer les diff�rences:"));
    pnVar.add(normal);
    pnVar.add(cbInverse_);
    add(pnVar);

  }

  public void addImportPanel(final H2dVariableType[] _varsToImport, final String _defSuff,
      final H2dVariableType[] _usedVar) {
    final BuPanel pnVar = createVarPn(CtuluLib.getS("Variables � importer"));
    usedVar_ = Arrays.asList(_usedVar);
    pnVar.setLayout(new BuBorderLayout(3, 5));

    varToImport_ = CtuluLibSwing.createBuList(_varsToImport);
    varToImport_.setSelectionInterval(0, _varsToImport.length - 1);
    pnVar.add(new BuScrollPane(varToImport_));
    final BuPanel pnSuff = new BuPanel(new BuGridLayout(2));
    addLabel(pnSuff, TrResource.getS("Suffixe ajout� au nom de la variable import�e"));
    tfSuff_ = new BuTextField(_defSuff);
    tfSuff_.setColumns(8);
    pnSuff.add(tfSuff_);
    final JLabel lb = addLabel(pnSuff, "<html><body>" + TrResource.getS("Exemple:") + "<br>'"
        + H2dVariableType.HAUTEUR_EAU + "' -> '" + H2dVariableType.HAUTEUR_EAU + tfSuff_.getText() + "'</body></html>");
    tfSuff_.getDocument().addDocumentListener(new TrSuffixeDocListener(lb));
    final BuPanel pnVarCheck = new BuPanel(new BuBorderLayout(2, 5));
    pnVarCheck.add(pnSuff);
    final BuButton btVerifyUsedName = new BuButton(TrResource
        .getS("V�rifier que les noms des variables import�es ne sont pas utilis�s"));
    pnVarCheck.add(btVerifyUsedName);
    btVerifyUsedName.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _e) {
        final String err = getVarUsedError();
        if (err == null) {
          CtuluLibDialog.showMessage(parent_, parent_.getTitle(), FSigLib.getS("Aucun conflit"));
        } else {
          CtuluLibDialog.showWarn(parent_, parent_.getTitle(), err);
        }
      }
    });
    pnVarCheck.add(btVerifyUsedName, BuBorderLayout.SOUTH);
    pnVar.add(pnVarCheck, BuBorderLayout.SOUTH);
    pnVar.add(new BuLabel(CtuluLib.getS("S�lectionner les variables � importer")), BuBorderLayout.NORTH);
    add(pnVar);
  }

  public H2dVariableType[][] createVarToImport() {
    final Object[] o = varToImport_.getSelectedValues();
    final H2dVariableType[][] res = new H2dVariableType[2][o.length];
    final String stuff = tfSuff_.getText();
    for (int i = 0; i < o.length; i++) {
      res[0][i] = H2dVariableType.createTempVar(o[i].toString() + stuff, null);
      res[1][i] = (H2dVariableType) o[i];
    }
    return res;
  }

  public String getVarUsedError() {
    return getVarUsedError(verifNameUsed(createVarToImport()));
  }

  public String getVarUsedError(final List _varToIgnore) {
    if (_varToIgnore != null && _varToIgnore.size() > 0) {
      final StringBuffer err = new StringBuffer(100);
      err.append(TrResource.getS("Les variables suivantes ne seront pas import�es\n car leur nom est d�j� utilis�:"));
      err.append(CtuluLibString.LINE_SEP);
      final int nb = _varToIgnore.size();
      for (int i = 0; i < nb; i++) {
        err.append(_varToIgnore.get(i).toString()).append(CtuluLibString.LINE_SEP);
      }
      err.append(MvResource.getS("Nombre de variables r�ellement import�es:")).append(
          CtuluLibString.getString(varToImport_.getModel().getSize() - _varToIgnore.size()));
      return err.toString();
    }
    return null;
  }

  public boolean isComparedGridDisplayed() {
    return cbAfficheCompGrid_.isSelected();
  }

  public boolean isCompareOnImportedGrid() {
    return cbOnImpGrid_.isSelected();
  }

  public boolean isCompareOnImportedTime() {
    return cbOnImpTime_.isSelected();
  }

  public boolean isGridEquals() {
    return cbGridEquals_ != null && cbGridEquals_.isSelected();
  }

  public boolean isInv() {
    return cbInverse_.isSelected();
  }

  @Override
  public boolean isDataValid() {
    cancelErrorText();
    if (varToImport_ != null) {
      if (varToImport_.isSelectionEmpty()) {
        setErrorText(CtuluLib.getS("S�lectionner les variables � importer"));
        return false;
      }
      String used = getVarUsedError();
      if (used != null) {
        used += CtuluLibString.LINE_SEP + CtuluLibString.LINE_SEP + BuResource.BU.getString("Voulez-vous continuer ?");
        return CtuluLibDialog.showConfirmation(parent_, parent_.getTitle(), used);
      }
    }
    return true;
  }

}
