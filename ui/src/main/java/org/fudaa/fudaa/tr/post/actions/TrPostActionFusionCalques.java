package org.fudaa.fudaa.tr.post.actions;

import com.memoire.bu.BuDialog;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuScrollPane;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.*;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionAbstract;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionUngroup;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetControllerCalque;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetCreatorVueCalque;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetFusionCalques;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetVueCalque;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetGroupCreator;
import org.fudaa.ebli.visuallibrary.graphe.GrapheCellRenderer;
import org.fudaa.ebli.visuallibrary.layout.OverlayLayoutGap;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.netbeans.api.visual.widget.Widget;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.util.*;

/**
 * Action qui realise la fusion des calques
 *
 * @author Adrien Hadoux
 */
public class TrPostActionFusionCalques extends EbliWidgetActionAbstract {
  TrPostProjet projet_;
  EbliWidgetVueCalque widgetCalque_;
  EbliNode nodeCalque_;
  JList jListeCalques_;
  ArrayList<JLabel> listeObjetsCalques;
  // -- les node possibles a choisir --//
  ArrayList<EbliNode> listeCalquesPossibles;
  JComponent content_;
  BuDialog dialog_;
  DefaultListModel modelCalquesPossibles_;

  /**
   * constructeur pour le cas de widget calque classiques.
   *
   * @param widget
   * @param nodeGraphe
   * @param projet
   */
  public TrPostActionFusionCalques(final EbliWidgetVueCalque widget, final EbliNode nodeGraphe,
                                   final TrPostProjet projet) {
    super(widget.getEbliScene(), TrResource.getS("Fusion avec un autre calque"), CtuluResource.CTULU.getIcon("cible"),
        "ADDPOINTWIDGET");

    widgetCalque_ = widget;
    nodeCalque_ = nodeGraphe;
    projet_ = projet;

    // -- ajout de la combo dans la toolbar de la widget --//
    if (!((EbliWidgetControllerCalque) widgetCalque_.getController()).hasAlreadyFusion) {
      widgetCalque_.getController().getPopup().add(this);
      ((EbliWidgetControllerCalque) widgetCalque_.getController()).hasAlreadyFusion = true;
    }
  }

  /**
   * Constructeur minimaliste qui r�alise directement la fusion sans passer par un �v�nement. Il faut appeler la methode
   * performGroupFusion avec la liste des claques a fusionner
   *
   * @param scene
   * @param projet
   */
  public TrPostActionFusionCalques(final EbliScene scene, final TrPostProjet projet) {
    super(scene, TrResource.getS("Fusion avec un autre calque"), CtuluResource.CTULU.getIcon("cible"), "ADDPOINTWIDGET");
    projet_ = projet;
  }

  /**
   * remplissage de la combo avec les graphes disponibles et compatibles
   */
  private void remplirCombo() {
    final Map params = new HashMap();
    CtuluLibImage.setCompatibleImageAsked(params);
    listeCalquesPossibles = new ArrayList<>();
    listeObjetsCalques = new ArrayList<>();
    EbliNode thisCurrentNodeCalque = nodeCalque_;
    if (isAlreadyInFusionWidget()) {
      thisCurrentNodeCalque = scene_.findNodeById(((EbliWidgetFusionCalques) nodeCalque_.getWidget().getParentWidget()).getId());
    }

    // -- remplissage de la liste a partir de l ecoute de la scene--//
    final Set<EbliNode> listeNode = (Set<EbliNode>) scene_.getObjects();
    Set<String> addedGroupOrLayer = new HashSet<>();
    for (final Iterator<EbliNode> it = listeNode.iterator(); it.hasNext(); ) {
      EbliNode currentNode = it.next();

      if (currentNode != thisCurrentNodeCalque && currentNode.getCreator() instanceof EbliWidgetCreatorVueCalque) {
        String idToUse = currentNode.getWidget().getId();
        final Widget parentWidget = currentNode.getWidget().getParentWidget();
        EbliWidgetImageProducer producer = (EbliWidgetImageProducer) currentNode.getWidget().getIntern();
        if (parentWidget instanceof EbliWidgetFusionCalques) {
          idToUse = ((EbliWidgetFusionCalques) parentWidget).getId();
          producer = (EbliWidgetImageProducer) parentWidget;
          currentNode = scene_.findNodeById(idToUse);
          if (currentNode == thisCurrentNodeCalque) {
            continue;
          }
        }
        if (addedGroupOrLayer.contains(idToUse)) {
          continue;
        }
        addedGroupOrLayer.add(idToUse);

        final JLabel label = new JLabel();
        final BufferedImage image = producer.produceImage(70, 50, params);
        final Icon icone = new ImageIcon(image);

        label.setIcon(icone);
        label.setText(TrResource.TR.getString("Fusionner avec ") + currentNode.getTitle());
        listeObjetsCalques.add(label);
        listeCalquesPossibles.add(currentNode);
      }
    }

    // -- creation graphique --//
    if (jListeCalques_ == null) {
      jListeCalques_ = new JList();
    }

    modelCalquesPossibles_ = new DefaultListModel();

    jListeCalques_.setModel(modelCalquesPossibles_);

    for (final Iterator<JLabel> it = listeObjetsCalques.iterator(); it.hasNext(); ) {

      modelCalquesPossibles_.addElement(it.next());
    }
    jListeCalques_.setSize(250, 350);
    jListeCalques_.setBorder(BorderFactory.createTitledBorder(TrResource.TR.getString("Calques possibles")));
    jListeCalques_.setCellRenderer(new GrapheCellRenderer());
  }

  private boolean isAlreadyInFusionWidget() {
    return nodeCalque_.getWidget().getParentWidget() instanceof EbliWidgetFusionCalques;
  }

  JComponent constructPanel() {
    final JPanel content = new JPanel(new BorderLayout());

    content.add(new JLabel(TrResource.getS("Fusion avec un autre calque")), BorderLayout.NORTH);
    content.add(new BuScrollPane(jListeCalques_), BorderLayout.CENTER);
    final JButton valide = new JButton(TrResource.getS("R�aliser la fusion"), EbliResource.EBLI
        .getIcon("crystal_valider"));
    valide.addActionListener(_e -> {
      mergeCalques();
      dialog_.dispose();
    });
    jListeCalques_.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

    final JPanel operations = new JPanel(new FlowLayout(FlowLayout.CENTER));

    operations.add(valide);
    content.add(operations, BorderLayout.SOUTH);

    return content;
  }

  /**
   * Methode qui: merge le calque choisi dans la combo avec le node actuel degage le node choisi de la scene remet a
   * jour la combo actuelle
   */
  private void mergeCalques() {

    final int[] selected = jListeCalques_.getSelectedIndices();
    if (selected == null || selected.length == 0) {
      return;
    }

    final ArrayList<EbliNode> listeToMerge = new ArrayList<EbliNode>();

    // -- parcours de la liste des indices selectionnes --//
    for (int i = 0; i < selected.length; i++) {
      final EbliNode nodeToMerge = listeCalquesPossibles.get(selected[i]);
      // --Ajout du noeud --//
      listeToMerge.add(nodeToMerge);
    }
    // -- ajout du node initial --//
    if (isAlreadyInFusionWidget()) {
      listeToMerge.add(scene_.findNodeById(((EbliWidgetFusionCalques) nodeCalque_.getWidget().getParentWidget()).getId()));
    } else {
      listeToMerge.add(nodeCalque_);
    }
    performGroupFusion(listeToMerge);
  }

  public EbliNode performGroupFusion(final Collection<EbliNode> listeToMerge) {
    final EbliNode fusion = groupWidgetsFusion(listeToMerge);

    // -- undo/redo --//
    getScene().getCmdMng().addCmd(new CtuluCommand() {
      @Override
      public void undo() {
        // -- degroupe et desynchronise le tout --//
        EbliWidgetActionUngroup.degroupObjects(getScene(), fusion);
      }

      @Override
      public void redo() {
        groupWidgetsFusion(listeToMerge);
      }
    });

    return fusion;
  }

  private EbliNode groupWidgetsFusion(final Collection<EbliNode> _selectedObjects) {
    final EbliWidgetFusionCalques parent = new EbliWidgetFusionCalques(scene_);

    // -- il faut des gaps suffisants pour deplacer la widget et avoir le menu
    // du groupe --//
    parent.setLayout(new OverlayLayoutGap(new Insets(-EbliLookFeel.getBorderThickness(), -EbliLookFeel
        .getBorderThickness(), -EbliLookFeel.getBorderThickness(), -EbliLookFeel.getBorderThickness())));

    final Point min = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
    final Point max = new Point(-Integer.MAX_VALUE, -Integer.MAX_VALUE);
    final Rectangle sizeMax = new Rectangle(0, 0);
    for (final Object object : _selectedObjects) {
      final Widget findWidget = scene_.findWidget(object);
      final Rectangle rec = findWidget.convertLocalToScene(findWidget.getBounds());
      min.x = Math.min(min.x, rec.x);
      min.y = Math.min(min.y, rec.y);
      max.x = Math.max(max.x, rec.x + rec.width);
      max.y = Math.max(max.y, rec.y + rec.height);

      sizeMax.height = Math.max(sizeMax.height, rec.height);
      sizeMax.width = Math.max(sizeMax.width, rec.width);
    }
    parent.setPreferredBounds(sizeMax);
    parent.setPreferredSize(new Dimension(sizeMax.width, sizeMax.height));

    for (final EbliNode object : _selectedObjects) {
      final EbliWidget findWidget = (EbliWidget) scene_.findWidget(object);
      findWidget.removeFromParent();
      if (findWidget instanceof EbliWidgetFusionCalques) {
        ArrayList<EbliWidgetVueCalque> layers = ((EbliWidgetFusionCalques) findWidget).listeWidgetCalque_;
        for (EbliWidgetVueCalque layer : layers) {
          layer.getParentWidget().removeFromParent();
          parent.addChildCalque((EbliWidget) layer.getParentWidget());
        }
      } else {
        parent.addChildCalque(findWidget);
      }
    }

    parent.setGroup(true);
    parent.setController(new EbliWidgetControllerForGroup(parent, true));
    parent.getController().setProportional(true);
    parent.synchronyseZoom();

    final EbliWidgetGroupCreator creator = new EbliWidgetGroupCreator();
    creator.setWidget(parent);
    creator.setPreferredLocation(min);

    final EbliNodeDefault node = new EbliNodeDefault();
    node.setTitle(EbliLib.getS("Fusion calques"));
    node.setCreator(creator);
    scene_.addNode(node);
    scene_.setSelectedObjects(new HashSet(Arrays.asList(node)));
    return node;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {

    // -- remplisage combo --//
    remplirCombo();

    dialog_ = new BuDialogMessage(projet_.impl_.getApp(), projet_.impl_.getInformationsSoftware(), TrResource
        .getS("Fusion avec un autre graphe"));
    if (content_ == null) {
      content_ = constructPanel();
    }
    dialog_.setContentPane(content_);
    dialog_.setSize(400, 250);
    dialog_.setModal(true);
    dialog_.setTitle(TrResource.getS("Fusion avec un autre graphe"));
    dialog_.activate();
  }
}
