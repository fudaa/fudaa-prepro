/*
 * @creation 29 janv. 07
 * @modification $Date: 2007-02-02 11:22:14 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.reflux;

import org.fudaa.dodico.h2d.reflux.H2dRefluxBcManager;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBoundaryCondition;
import org.fudaa.ebli.calque.ZModeleSegment;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.fudaa.meshviewer.model.MvFrontierModelDefault;

/**
 * Une classe permettant de tracer les normales aux noeuds frontières.
 *
 * @author fred deniger
 * @version $Id: TrRefluxNormaleModel.java,v 1.2 2007-02-02 11:22:14 deniger Exp $
 */
public class TrRefluxNormaleModel extends MvFrontierModelDefault implements ZModeleSegment {

  final H2dRefluxBcManager bc_;
  final int nbPtFrontier_;

  public TrRefluxNormaleModel(final H2dRefluxBcManager _bc) {
    super(_bc.getGrid());
    bc_ = _bc;
    nbPtFrontier_ = _bc.getNbTotalPoint();
  }

  @Override
  public void prepare() {
  }

  H2dRefluxBoundaryCondition getBc(final int _idxOnFr) {
    final int[] idxFrIdxPt = new int[2];
    getIdxFrIdxOnFrFromFrontier(_idxOnFr, idxFrIdxPt);
    return (H2dRefluxBoundaryCondition) (bc_.getBcFrontier(idxFrIdxPt[0]).getCl(idxFrIdxPt[1]));
  }

  int getIdxGlobal(final int _idxOnFr) {
    return bc_.getGrid().getFrontiers().getIdxGlobalFrom(_idxOnFr);
  }

  @Override
  public double getNorme(final int _i) {
    return 1;
  }

  @Override
  public double getVy(final int _i) {
    final H2dRefluxBoundaryCondition bc = getBc(_i);
    return Math.sin(Math.toRadians(bc.getNormale()));
  }

  @Override
  public double getVx(final int _i) {
    final H2dRefluxBoundaryCondition bc = getBc(_i);
    return Math.cos(Math.toRadians(bc.getNormale()));
  }

  @Override
  public double getX(final int _i) {
    return bc_.getGrid().getPtX(getIdxGlobal(_i));
  }

  @Override
  public double getY(final int _i) {
    return bc_.getGrid().getPtY(getIdxGlobal(_i));
  }

  @Override
  public double getZ1(final int _i) {
    return bc_.getGrid().getPtZ(getIdxGlobal(_i));
  }

  @Override
  public double getZ2(final int _i) {
    return bc_.getGrid().getPtZ(getIdxGlobal(_i));
  }

  @Override
  public boolean segment(final GrSegment _s, final int _i, final boolean _force) {
    // le depart du vecteur
    if (_s.o_ == null) {
      _s.o_ = new GrPoint(getX(_i), getY(_i), 0);
    } else {
      _s.o_.setCoordonnees(getX(_i), getY(_i), 0);
    }
    // la fin
    if (_s.e_ == null) {
      _s.e_ = new GrPoint(_s.o_.x_ + getVx(_i), _s.o_.y_ + getVy(_i), 0);
    } else {
      _s.e_.setCoordonnees(_s.o_.x_ + getVx(_i), _s.o_.y_ + getVy(_i), 0);
    }

    return true;
  }

  @Override
  public int getNombre() {
    return nbPtFrontier_;
  }

  @Override
  public boolean isValuesTableAvailable() {
    return false;
  }
}
