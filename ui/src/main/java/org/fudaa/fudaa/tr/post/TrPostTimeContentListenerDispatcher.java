/**
 * 
 */
package org.fudaa.fudaa.tr.post;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

public class TrPostTimeContentListenerDispatcher implements ListDataListener {
  private final TrPostProjet projet;

  public List<WeakReference<ListDataListener>> weakListener = new ArrayList<WeakReference<ListDataListener>>();

  /**
   * @param projet
   */
  public TrPostTimeContentListenerDispatcher(TrPostProjet projet) {
    super();
    this.projet = projet;
  }

  public void addWeakListener(ListDataListener listener) {
    weakListener.add(new WeakReference<ListDataListener>(listener));
  }

  public void clear() {
    weakListener.clear();
  }

  @Override
  public void contentsChanged(final ListDataEvent _event) {
    for (ListDataListener listDataListener : getListeners()) {
      listDataListener.contentsChanged(_event);
    }
    updateAll();
  }

  private Collection<ListDataListener> getListeners() {
    List<ListDataListener> res = new ArrayList<ListDataListener>();
    boolean mustClean = false;
    for (WeakReference<ListDataListener> weakRef : weakListener) {
      ListDataListener timeListener = weakRef.get();
      if (timeListener != null) {
        res.add(timeListener);
      } else {
        mustClean = true;
      }
    }
    if (mustClean) {
      for (Iterator<WeakReference<ListDataListener>> iterator = weakListener.iterator(); iterator.hasNext();) {
        if (iterator.next().get() == null) {
          iterator.remove();
        }
      }

    }
    return res;

  }

  @Override
  public void intervalAdded(final ListDataEvent _event) {
    updateAll();
    for (ListDataListener listDataListener : getListeners()) {
      listDataListener.intervalAdded(_event);
    }
  }

  @Override
  public void intervalRemoved(final ListDataEvent _event) {
    for (ListDataListener listDataListener : getListeners()) {
      listDataListener.intervalRemoved(_event);
    }
    updateAll();
  }

  public void removeWeakListener(ListDataListener listener) {
    for (Iterator<WeakReference<ListDataListener>> iterator = weakListener.iterator(); iterator.hasNext();) {
      if (iterator.next().get() == listener) {
        iterator.remove();
      }
    }
  }

  protected void updateAll() {
    projet.setProjectModified();
  }

}