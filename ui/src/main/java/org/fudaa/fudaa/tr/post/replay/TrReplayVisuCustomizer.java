/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.replay;

import org.fudaa.fudaa.tr.post.TrPostVisuPanel;

/**
 *
 * @author Frederic Deniger
 */
public interface TrReplayVisuCustomizer {

  void customizeVisu(TrPostVisuPanel panel);
  
}
