/*
 * @creation 29 sept. 2003 
 * @modification $Date: 2007-06-28 09:28:19 $ 
 * @license GNU General Public License 2 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne 
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.tr.telemac;

import java.io.File;
import java.util.Map;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.telemac.H2dTelemacDicoParams;
import org.fudaa.dodico.telemac.Telemac2dFileFormat;
import org.fudaa.dodico.telemac.TelemacDicoFileFormatVersion;
import org.fudaa.dodico.telemac.TelemacDicoManager;
import org.fudaa.fudaa.fdico.FDicoParams;
import org.fudaa.fudaa.tr.common.TrImplementationEditorAbstract;
import org.fudaa.fudaa.tr.common.TrProjet;

/**
 * @author deniger
 * @version $Id: TrTelemacProjectFactory.java,v 1.23 2007-06-28 09:28:19 deniger Exp $
 */
public final class TrTelemacProjectFactory {

  private TrTelemacProjectFactory() {
    super();
  }

  /**
   * @param _f le fichier cas a creer / the project file to create
   * @param _v la version voulu / the wanted version
   * @param _ui l'imp parent / the parent implementation
   * @param _gridFile le maillage (peut etre nul) / the mesh (can be null)
   * @return le projet / the project
   */
  public static TrProjet createNewTelemacProjet(final File _f, final TelemacDicoFileFormatVersion _v,
      final TrImplementationEditorAbstract _ui, final File _gridFile) {
    return createNewTelemacProjet(_f, _v, _ui, null, null, _gridFile);
  }

  /**
   * Cree un nouveau projet ( 2d si necessaire).
   * Create a new project (2d if necessary)
   * @param _f le fichier / file
   * @param _v la version / version
   * @param _ui l'impl parent / parent impl
   * @param _m les mot-cl�s (peut etre null) / keywords (can be null)
   * @param _commentaire les commentaires (peut etre null) / comments (can be null)
   * @param _gridFile le fichier de maillage (peut etre null) / mesh file (can be null)
   * @return le nouveau projet / new project
   */
  public static TrProjet createNewTelemacProjet(final File _f, final TelemacDicoFileFormatVersion _v,
      final TrImplementationEditorAbstract _ui, final Map _m, final Map _commentaire, final File _gridFile) {
    if (TelemacDicoManager.isTelemac2d(_v.getDicoFileFormat())) { return createNewTelemac2dProjet(_f, _v, _ui, _m,
        _commentaire, _gridFile); }
    if (TelemacDicoManager.isStbtel(_v.getFileFormat())) { return createNewStbtelProjet(_f, _v, _ui, _m, _commentaire,
        _gridFile); }
    final TrTelemacCommunProjectListener listener = new TrTelemacCommunProjectListener();
    final TrTelemacCommunProjet r = new TrTelemacCommunProjet(new TrTelemacCommunParametres(_ui,
        new H2dTelemacDicoParams(_m, _commentaire, _v), _f, _gridFile, listener));
    listener.setProj(r);
    return r;
  }

  /**
   * Creer un projet telemac2D.
   * Create a telemac2D project
   * @param _f le fichier / file
   * @param _v la version / version
   * @param _ui l'impl parent / parent implementation
   * @param _m les mot-cl�s (peut etre null) / keywords (can be null)
   * @param _com les commentaires (peut etre null) / comments (can be null)
   * @param _gridFile le fichier de maillage (peut etre null) / mesh file (can be null)
   * @return le nouveau projet / new project
   */
  public static TrTelemac2dProject createNewTelemac2dProjet(final File _f, final TelemacDicoFileFormatVersion _v,
      final TrImplementationEditorAbstract _ui, final Map _m, final Map _com, final File _gridFile) {
    final TrTelemacCommunProjectListener l = new TrTelemacCommunProjectListener();
    final TrTelemac2dProject r = new TrTelemac2dProject(new TrTelemac2dParametres(_ui, new H2dTelemacDicoParams(_m,
        _com, _v), _f, _gridFile, l));
    l.setProj(r);
    return r;
  }

  /**
   * Creer un projet telemac2D.
   * Create a telemac2D project
   * @param _f le fichier / file
   * @param _v la version / version
   * @param _ui l'impl parent / parent implementation
   * @param _m les mot-cl�s (peut etre null) / keywords (can be null)
   * @param _com les commentaires (peut etre null) / comments (can be null)
   * @param _gridFile le fichier de maillage (peut etre null) / mesh file (can be null)
   * @return le nouveau projet / new project
   */
  public static TrTelemacCommunProjet createNewStbtelProjet(final File _f, final TelemacDicoFileFormatVersion _v,
      final TrImplementationEditorAbstract _ui, final Map _m, final Map _com, final File _gridFile) {
    final TrTelemacCommunProjectListener l = new TrTelemacCommunProjectListener();
    final TrTelemacCommunProjet r = new TrTelemacCommunProjet(new TrTelemacStbtelParameters(_ui,
        new H2dTelemacDicoParams(_m, _com, _v), _f, _gridFile, l));
    l.setProj(r);
    return r;
  }

  /**
   * @param _file le fichier cas / project file
   * @param _fileFormat le format / format
   * @param _progress la barre de progression / progress bar
   * @param _ui l'impl / implementation
   * @return le nouveau projet ou nul si erreur / the new project or null if error
   */
  public static TrProjet createTelemacProjet(final File _file, final TelemacDicoFileFormatVersion _fileFormat,
      final ProgressionInterface _progress, final TrImplementationEditorAbstract _ui) {
    if (TelemacDicoManager.isTelemac2d(_fileFormat.getDicoFileFormat())) { return createTelemac2dProject(_file,
        (Telemac2dFileFormat.TelemacVersion) _fileFormat, _progress, _ui); }
    if (TelemacDicoManager.isStbtel(_fileFormat.getFileFormat())) { return createStbtelProject(_file, _fileFormat,
        _progress, _ui); }
    final FDicoParams.DicoReadData p = FDicoParams.createData(_file, _fileFormat, _progress, _ui, TelemacDicoManager
        .getINSTANCE());
    if ((p == null) || (p.src_ == null)) { return null; }
    final TrTelemacCommunProjectListener listener = new TrTelemacCommunProjectListener();
    final TrTelemacCommunProjet r = new TrTelemacCommunProjet(new TrTelemacCommunParametres(_ui,
        new H2dTelemacDicoParams(p.src_, (TelemacDicoFileFormatVersion) p.version_), _file, null, listener));
    listener.setProj(r);
    return r;
  }

  /**
   * @param _file le fichier cas / project file
   * @param _fileFormat le format voulu / wanted format
   * @param _progress la barre de progress / progress bar
   * @param _ui l'impl / implementation
   * @return le nouveau projet ou nul / new project or null
   */
  public static TrProjet createTelemac2dProject(final File _file, final Telemac2dFileFormat.TelemacVersion _fileFormat,
      final ProgressionInterface _progress, final TrImplementationEditorAbstract _ui) {
    final FDicoParams.DicoReadData p = FDicoParams.createData(_file, _fileFormat, _progress, _ui, TelemacDicoManager
        .getINSTANCE());
    if ((p == null) || (p.src_ == null)) { return null; }
    final TrTelemacCommunProjectListener l = new TrTelemacCommunProjectListener();
    final TrTelemac2dProject r = new TrTelemac2dProject(new TrTelemac2dParametres(_ui, new H2dTelemacDicoParams(p.src_,
        (TelemacDicoFileFormatVersion) p.version_), _file, null, l));
    l.setProj(r);
    return r;
  }

  /**
   * @param _file le fichier cas / project file
   * @param _fileFormat le format voulu / wanted format
   * @param _progress la barre de progress / progress bar
   * @param _ui l'impl / implementation
   * @return le nouveau projet ou nul / new project or null
   */
  public static TrProjet createStbtelProject(final File _file, final TelemacDicoFileFormatVersion _fileFormat,
      final ProgressionInterface _progress, final TrImplementationEditorAbstract _ui) {
    final FDicoParams.DicoReadData p = FDicoParams.createData(_file, _fileFormat, _progress, _ui, TelemacDicoManager
        .getINSTANCE());
    if ((p == null) || (p.src_ == null)) { return null; }
    final TrTelemacCommunProjectListener l = new TrTelemacCommunProjectListener();
    final TrTelemacCommunProjet r = new TrTelemacCommunProjet(new TrTelemacStbtelParameters(_ui,
        new H2dTelemacDicoParams(p.src_, (TelemacDicoFileFormatVersion) p.version_), _file, null, l));
    l.setProj(r);
    return r;
  }
}