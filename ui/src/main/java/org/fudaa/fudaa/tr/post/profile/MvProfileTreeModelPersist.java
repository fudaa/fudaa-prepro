package org.fudaa.fudaa.tr.post.profile;

import org.locationtech.jts.geom.LineString;
import java.util.Map;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.actions.TrPostProfileAction;
import org.fudaa.fudaa.tr.post.persist.TrPostReloadParameter;


/**
 * Donn�es persistantes de l'objet TrPostCourbeTreeModel.
 * Ces donn�es serviront a charger/sauvegarder les don�nes sp�cifique au trpostcourbe.
 * Cette classe doit etre s�rializable.
 * @author Adrien Hadoux
 *
 */
public class MvProfileTreeModelPersist {

	public String type;

	public String idSource;

	LineString initLineNode;
	LineString initLineMesh;

	public MvProfileTreeModelPersist(){}


	/**
	 * Methode qui remplit le model en param d'entr�es avec les datas de cette classe.
	 * @param model
	 */
	public void fillModelWithData(MvProfileTreeModel model, Map param){
		type="Courbe spatiale";
		model.tester_=new MvProfileCoteTester();
//		model.initLineNode=initLineNode;
//		model.initLineMesh=initLineMesh;


		//-- donnees specifiques propres au projet charg� --//
		TrPostProjet projet=(TrPostProjet) param.get(TrPostReloadParameter.POST_PROJET);

		if(projet!=null){
			if(this.idSource!=null){
				TrPostSource src=projet.getSources().findSourceById(this.idSource);
				if(src!=null){
					//-- creation du mvprofiletarget --//
					model.target_=TrPostProfileAction.createProfileAdapter(src, projet)  ;
					model.timeModel_ = model.target_.getTimeModel();

				}
			}

		}
	}

	/**
	 * TODO a enlever car ne doit pas dependre de Tr
	 */
	/**
	 * Methode qui remplit cetet classe a partir du modele fourni en entree.
	 */
	public void fillDataWithModel(MvProfileTreeModel model){
		type=TrLib.getString("Courbe spatiale");
		//-- recuperer la source du mvprofile --//
		if(model.target_.getData() instanceof TrPostSource){
			TrPostSource src=(TrPostSource) model.target_.getData();
			idSource=src.getId();
		}
//		initLineNode=model.initLineNode;
//		initLineMesh=model.initLineMesh;
	}
}

