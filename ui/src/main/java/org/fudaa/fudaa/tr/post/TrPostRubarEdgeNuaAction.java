package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuResource;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluDialogPreferences;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluTableExportAction;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.rubar.io.RubarNUAResult;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.tr.common.TrPreferences;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class TrPostRubarEdgeNuaAction extends EbliActionSimple {
  private final RubarNUAResult nuaResult;
  private final CtuluUI ctuluUI;

  public TrPostRubarEdgeNuaAction(TrPostRubarEdgesResults edgesResults, CtuluUI ctuluUI) {
    super(TrResource.getS("Voir") + " " + H2dVariableTransType.COEF_DIFFUSION.getName() + " (NUA)", BuResource.BU.getIcon("crystal_liste"), "EXPORT_NUA");
    this.nuaResult = edgesResults.getNuaResult();
    this.ctuluUI = ctuluUI;
  }

  public TrPostRubarEdgesNuaTableModel getNuaTableModel(int tidx) {
    return new TrPostRubarEdgesNuaTableModel(nuaResult, tidx);
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    final JComboBox comboBox = new JComboBox();
    comboBox.setModel(new TimeComboBoxModel(this.nuaResult));
    final CtuluTable table = new CtuluTable();
    comboBox.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        final int selectedIndex = comboBox.getSelectedIndex();
        if (selectedIndex >= 0) {
          table.setModel(getNuaTableModel(selectedIndex));
        }
      }
    });
    JPanel panel = new JPanel(new BorderLayout());
    JToolBar toolBar = new JToolBar();
    toolBar.add(new JLabel(TrResource.getS("Pas de temps:") + "  "));
    toolBar.add(comboBox);
    toolBar.add(new JSeparator());
    toolBar.add(new JButton(new CtuluTableExportAction(ctuluUI, table)));

    panel.add(new JScrollPane(table));
    panel.add(toolBar, BorderLayout.NORTH);
    final Frame f = CtuluLibSwing.getFrameAncestorHelper(ctuluUI.getParentComponent());
    JDialog dialog = new JDialog(f);
    dialog.setResizable(true);
    dialog.setContentPane(panel);
    CtuluDialogPreferences.loadComponentLocationAndDimension(dialog, TrPreferences.TR, getClass().getName());
    CtuluDialogPreferences.ensureComponentWillBeVisible(dialog, dialog.getLocation());
    dialog.setTitle(H2dVariableTransType.COEF_DIFFUSION.getName() + " (NUA)");
    dialog.setModal(true);
    comboBox.setSelectedIndex(0);
    dialog.show();
    dialog.dispose();
    CtuluDialogPreferences.saveComponentLocationAndDimension(dialog, TrPreferences.TR, getClass().getName());
    TrPreferences.TR.writeIniFile();
  }

  private static class TimeComboBoxModel extends AbstractListModel implements ComboBoxModel {
    private transient final RubarNUAResult edgesResults;

    public TimeComboBoxModel(RubarNUAResult edgesResults) {
      this.edgesResults = edgesResults;
    }

    transient int idx_ = -1;
    transient Object select_;

    @Override
    public Object getElementAt(final int _index) {
      return edgesResults.getBloc(_index).getTime();
    }

    @Override
    public Object getSelectedItem() {
      return select_;
    }

    @Override
    public int getSize() {
      return edgesResults.getNbBlocSize();
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != select_) {
        select_ = _anItem;
      }
      fireContentsChanged(this, -1, -1);
    }
  }
}
