/**
 * @creation 7 d�c. 2004
 * @modification $Date: 2007-01-19 13:14:31 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Dimension;
import java.beans.PropertyVetoException;
import javax.swing.event.InternalFrameEvent;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluHelpComponent;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuil;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuilListener;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuilMng;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.courbe.EGFilleSimple;
import org.fudaa.ebli.courbe.EGGrapheSimpleModel;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacWeirCourbeFille.java,v 1.13 2007-01-19 13:14:31 deniger Exp $
 */
public final class TrTelemacWeirCourbeFille extends EGFilleSimple implements H2dTelemacSeuilListener,
    CtuluHelpComponent {

  private static class CourbeModel extends EGGrapheSimpleModel {

    public CourbeModel() {
      super();
    }

    public CourbeModel(final EGCourbeSimple[] _s) {
      super(_s);
    }

    @Override
    public boolean canAddCourbe() {
      return false;
    }

    @Override
    public boolean isContentModifiable() {
      return true;
    }

    @Override
    public boolean isStructureModifiable() {
      return false;
    }
  }

  /**
   * Pour avoir des noms differents.
   */
  static int idx_;
  TrTelemacWeirCourbeModel coteModel_;
  TrTelemacWeirCourbeModel debitModel_;

  int idxSeuil_;
  H2dTelemacSeuilMng mng_;
  H2dTelemacSeuil seuilVu_;

  public static TrTelemacWeirCourbeFille createCourbeFille(final H2dTelemacSeuilMng _mng, final int _i,
      final FudaaCommonImplementation _impl) {
    final double[] x = _mng.getCurvAbs(_mng.getTelemacSeuil(_i));
    final TrTelemacWeirCourbeModel coteModel = new TrTelemacWeirCourbeModel(_i, _mng, true, x);
    final TrTelemacWeirCourbeModel debitModel = new TrTelemacWeirCourbeModel(_i, _mng, false, x);
    final EGCourbeSimple[] cs = new EGCourbeSimple[2];
    EGAxeVertical v = new EGAxeVertical();
    v.setTitre(coteModel.getTitle());
    cs[0] = new EGCourbeSimple(v, coteModel);
    cs[0].setAspectContour(Color.darkGray);
    v = new EGAxeVertical();
    v.setTitre(debitModel.getTitle());
    cs[1] = new EGCourbeSimple(v, debitModel);
    cs[1].setAspectContour(Color.lightGray);
    final TrTelemacWeirCourbeFille r = new TrTelemacWeirCourbeFille(new CourbeModel(cs), _mng, _i, _impl, x);
    r.coteModel_ = coteModel;
    r.debitModel_ = debitModel;
    r.setPreferredSize(new Dimension(300, 300));
    return r;

  }

  FudaaCommonImplementation impl_;

  /**
   * @param _mng le manager de seuil
   * @param _i l'indice du seuil a visualiser
   * @param _impl l'implemenation parente.
   */
  private TrTelemacWeirCourbeFille(final CourbeModel _m, final H2dTelemacSeuilMng _mng, final int _i,
      final FudaaCommonImplementation _impl, final double[] _x) {
    super(_m, TrResource.getS("Seuil {0}", CtuluLibString.getString(_i + 1)), _impl, null, new EGTableGraphePanel());
    setName("filleWeir" + (idx_++));
    mng_ = _mng;
    impl_ = _impl;
    idxSeuil_ = _i;
    seuilVu_ = mng_.getTelemacSeuil(_i);
    final EGAxeHorizontal axe = new EGAxeHorizontal(true);
    final TrTelemacFrontierNumberIteratorAbstract axeIt = new TrTelemacWeirNumberIterator(seuilVu_, _x);
    axe.setSpecificFormat(axeIt);
    axe.setTitre(EbliLib.getS("Indices"));
    axe.setAxisIterator(axeIt);
    axe.setValueEditor(axeIt);
    axe.setGraduations(true);
    getGraphe().setXAxe(axe);
    mng_.addListener(this);
  }

  @Override
  public String getShortHtmlHelp() {
    final CtuluHtmlWriter r = new CtuluHtmlWriter();
    r
        .h2Center(
            TrResource.getS("Edition de la cote et du d�bit le long du seuil {0}", CtuluLibString
                .getString(idxSeuil_ + 1))).h3(TrResource.getS("Documents associ�s")).addTag("ul");
    r.close(r.addTag("li"),
        impl_.buildLink(TrResource.getS("Description du composant d'affichage des courbes"), "common-curves")).close(
        r.addTag("li"), impl_.buildLink(TrResource.getS("Edition des seuils"), "telemac-editor-seuils"));
    return r.toString();
  }

  @Override
  protected void fireInternalFrameEvent(final int _id) {
    super.fireInternalFrameEvent(_id);
    if (_id == InternalFrameEvent.INTERNAL_FRAME_CLOSING) {
      TrLib.saveFrameDimensionInPref("weirTelemac", this);
      mng_.removeListener(this);
    }
  }

  @Override
  public void seuilAdded() {
    seuilRemoved();
  }

  @Override
  public void seuilChanged(final boolean _xyChanged) {
    final double[] x = mng_.getCurvAbs(mng_.getTelemacSeuil(idxSeuil_));
    coteModel_.setX(x);
    debitModel_.setX(x);
    ((TrTelemacWeirNumberIterator) getGraphe().getTransformer().getXAxe().getAxisIterator()).setX(x);
    getGraphe().fullRepaint();
  }

  @Override
  public void seuilRemoved() {
    final int i = mng_.containsSeuil(seuilVu_);
    if (i < 0) {
      try {
        setClosed(true);
      } catch (final PropertyVetoException _e) {
        FuLog.warning(_e);
      }
    } else if (i != idxSeuil_) {
      idxSeuil_ = i;
      setTitle(TrResource.getS("Seuil {0}", CtuluLibString.getString(idxSeuil_ + 1)));
      coteModel_.i_ = idxSeuil_;
      debitModel_.i_ = idxSeuil_;
    }
  }
}