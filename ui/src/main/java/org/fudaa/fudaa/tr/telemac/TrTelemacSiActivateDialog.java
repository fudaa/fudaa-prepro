/*
 * @creation 16 mars 2006
 * @modification $Date: 2006-09-19 15:07:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import javax.swing.ButtonGroup;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author fred deniger
 * @version $Id: TrTelemacSiActivateDialog.java,v 1.5 2006-09-19 15:07:29 deniger Exp $
 */
public class TrTelemacSiActivateDialog extends CtuluDialogPanel {

  final TrTelemacSiActivator act_;

  final BuRadioButton rdFromH_;

  final BuRadioButton rdFromFile_;

  final BuTextField tfCi_;

  final CtuluFileChooserPanel pnFile_;

  public boolean isInitFromH() {
    return rdFromH_.isSelected();
  }

  public double getCoteInitiale() {
    return ((Double) tfCi_.getValue()).doubleValue();
  }

  public File getRepFile() {
    return pnFile_.getFile();
  }

  public TrTelemacSiActivateDialog(final TrTelemacSiActivator _act, final boolean _allowFile, final File _f,
      final boolean _isFirst) {
    act_ = _act;
    final File destFile = act_.getFileSetInDico();
    rdFromH_ = new BuRadioButton(TrResource.getS("Initialiser depuis une surface libre plane:"));
    rdFromFile_ = new BuRadioButton(TrResource.getS("Initialiser depuis un fichier de r�sultats"));
    final CtuluHtmlWriter wr = new CtuluHtmlWriter(true);
    wr.setDefaultResource(TrResource.TR);
    rdFromFile_.setToolTipText(wr.close(wr.addStyledTag("u", "color:red"),
        "Le maillage du fichier r�sultat doit �tre identique � celui utilis�.").nl().addi18n(
        "Sinon, il est possible d'interpoler ces valeurs sur le maillage du projet.").getHtml());
    final ButtonGroup bg = new ButtonGroup();
    bg.add(rdFromH_);
    bg.add(rdFromFile_);

    tfCi_ = BuTextField.createDoubleField();
    tfCi_.setColumns(10);
    tfCi_.setText(Double.toString(act_.getCoteInitial()));
    pnFile_ = new CtuluFileChooserPanel(TrResource.getS("Fichier de reprise"));
    pnFile_.setWriteMode(true);
    pnFile_.setFilter(new BuFileFilter[] { SerafinFileFormat.getInstance().createFileFilter() });
    setLayout(new BuBorderLayout(1, 5));
    final BuPanel ciPanel = new BuPanel();
    ciPanel.setLayout(new BuGridLayout(2, 4, 4));
    ciPanel.add(rdFromH_);
    ciPanel.add(tfCi_);
    if (_allowFile) {
      ciPanel.add(rdFromFile_);
      ciPanel.add(pnFile_);
    }
    rdFromFile_.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(final ItemEvent _e) {
        final boolean fileOk = rdFromFile_.isSelected();
        pnFile_.setEnabled(fileOk);
        tfCi_.setEnabled(!fileOk);
      }

    });
    if (_f != null && _allowFile) {
      rdFromFile_.setSelected(true);
      pnFile_.setFile(_f);
    } else {
      rdFromH_.setSelected(true);
    }
    add(ciPanel, BuBorderLayout.CENTER);
    final CtuluHtmlWriter writer = new CtuluHtmlWriter(true);
    writer.setDefaultResource(TrResource.TR);
    final CtuluHtmlWriter.Tag t = writer.addStyledTag("p", "font-weight:bold");
    writer
        .addi18n(
            "Lorsque la reprise de calcul est activ�e, toutes les modifications concernant <br>&nbsp;des propri�t�s nodales (bathym�trie, frottement, ...) sont sauvegard�es dans le fichier de reprise")
        .nl().nl();
    if (destFile == null) {
      writer.addi18n("Un nouveau fichier de reprise de reprise sera cr�e.").nl();
    } else {
      writer.append(TrResource.getS(
          "Le fichier {0}, pr�cis� dans le fichier cas, contiendra les donn�es sauvegard�es.", destFile.getName()));
    }
    t.close();
    writer.addTag("p");
    if (!_allowFile) {
      writer.addi18n("Le maillage du projet a �t� optimis�. Il ne sera pas possible d'utiliser<br>un fichier de "
          + "r�sultats pour initialiser les conditions initiales.<br>Pour "
          + "r�cup�rer des donn�es, utiliser les actions d'�dition du calque.");
    } else if (_isFirst && _f != null) {
      writer.addi18n("Le fichier sp�cifi� ci-dessous est celui du fichier cas.");
    }
    add(new BuLabel(writer.toString()), BuBorderLayout.NORTH);
  }

  @Override
  public boolean isDataValid() {
    setErrorText(null);
    if (rdFromH_.isSelected()) {
      final boolean ok = (tfCi_.getText() != null) && tfCi_.getText().trim().length() > 0;
      if (!ok) {
        setErrorText(CtuluLib.getS("Valeur incorrecte"));
      }
      return ok;
    }
    final String err = CtuluLibFile.canWrite(pnFile_.getFile());
    if (err != null) {
      setErrorText(err);
      return false;
    }
    return true;
  }

}
