/*
 * @creation 18 avr. 2006
 * @modification $Date: 2007-06-05 09:01:14 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuColumn;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuLib;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrLauncher;

import javax.swing.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 * TODO a revoir entierement
 *
 * @author fred deniger
 * @version $Id: TrPostInspector.java,v 1.10 2007-06-05 09:01:14 deniger Exp $
 */
public class TrPostInspector {
  public class TimerTaskAdapter extends TimerTask {
    UIRunnable uiRun_;

    @Override
    public void run() {
      if (pause_) {
        if (Fu.DEBUG && FuLog.isDebug()) {
          FuLog.debug("FTR: inspector is paused");
        }
        return;
      }
      // le projet a �t� ferm�: on ferme les timers
      if (proj_ != null && src_ != null) {
        if (src_.isClosed()) {
          stop();
          return;
        }
        // src_.setInspected(true);
      }

      if (reader_ != null) {
        // en train d'ouvrir le post
        if (reader_.isPostActivating()) {
          if (Fu.DEBUG && FuLog.isDebug()) {
            FuLog.debug("FTR: inspector post is activating");
          }
          return;
        }
        final int res = reader_.read();
        if (!isPostActivated() && reader_.isPostActivated()) {
          if (Fu.DEBUG && FuLog.isDebug()) {
            FuLog.debug("FTR: inspector post is activated ");
          }
          proj_ = reader_.getPostActivatedProject();
          try {

            Thread.sleep(5 * 1000L);
          } catch (final InterruptedException _evt) {
            FuLog.error(_evt);
            Thread.currentThread().interrupt();
          }

          return;
        }
        if (uiRun_ == null) {
          uiRun_ = new UIRunnable();
        }
        uiRun_.setRes(res);
        BuLib.invokeLater(uiRun_);
      }
    }
  }

  class UIRunnable implements Runnable {
    int res_;

    protected int getRes() {
      return res_;
    }

    protected void setRes(final int _res) {
      res_ = _res;
    }

    @Override
    public void run() {
      if (panel_ != null) {
        panel_.update(res_);
        panel_.reset();
      }
      if (!projUpdated_) {
        updateRubarAction(false);
      }
      if (res_ > 0) {
        // if (proj_ != null) proj_.getImpl().getFrame().toFront();
        updateGraphs();
      }
    }
  }

  int nbSec_ = 3;
  final TrPostInspectorPanelBuilder panel_;
  boolean pause_;
  TrPostProjet proj_;
  TrPostSource src_;
  TrPostInspectorReader reader_;
  Timer timer_;

  private TrPostInspector() {
    panel_ = new TrPostInspectorPanelBuilder(this);
  }

  public TrPostInspector(final TrPostInspectorReader _reader, final String _titre, final TrLauncher _launcher) {
    this();
    reader_ = _reader;
    final TrPostCommonImplementation impl = _launcher.openPost(null);
    impl.setTitle(_titre);
    _reader.setImpl(impl);
    updateImpl(impl);
  }

  public TrPostInspector(final TrPostProjet _proj, final TrPostSource _src) {
    this();
    proj_ = _proj;
    src_ = _src;
    updateImpl(proj_.getImpl());
  }

  void updateGraphs() {
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FTR: inspector udpate Graphs");
    }
    if (proj_ == null || proj_.getImpl() == null) {
      return;
    }
    // pour activer la maj auto des pas de temps dans le calque iso
    // if (proj_.fille_ != null) {
    // final int idx = src_.getNbTimeStep() - 1;
    // final TrPostVisuPanel trPostVisuPanel = ((TrPostVisuPanel) proj_.fille_.getVisuPanel());
    // trPostVisuPanel.getIsoLayer().getMainTimeModel().setSelectionInterval(idx, idx);
    // final TrPostFlecheLayer layer = trPostVisuPanel.getFlecheLayer();
    // if (layer.getSelectedVar() != null) {
    // layer.getTimeListSelectionModel().setSelectionInterval(idx, idx);
    // }
    // }
    final JInternalFrame[] fr = proj_.getImpl().getAllInternalFrames();
    int[] idx = null;
    if (CtuluLibArray.isEmpty(fr)) {
      return;
    }
    for (int i = fr.length - 1; i >= 0; i--) {
      if (fr[i] instanceof TrPostTimeContentListener) {
        if (idx == null) {
          idx = new int[src_.getNbTimeStep()];
          for (int k = idx.length - 1; k >= 0; k--) {
            idx[k] = k;
          }
        }
        ((TrPostTimeContentListener) fr[i]).updateTimeStep(idx, panel_);
      }
    }
    panel_.reset();
  }

  void updateImpl(final BuCommonImplementation _impl) {
    final BuColumn rightColumn = _impl.getMainPanel().getRightColumn();
    rightColumn.add(panel_.getPn(), 0);
    rightColumn.revalidate();
    _impl.setEnabledForAction("MAJ_DATA_ALWAYS", false);
    _impl.setEnabledForAction("MAJ_DATA", false);
    updateRubarAction(false);
  }

  boolean projUpdated_;

  void updateRubarAction(final boolean _enable) {
    if (proj_ != null && src_.containsElementVar()) {
      projUpdated_ = true;
      final FudaaCommonImplementation impl = proj_.getImpl();
      if (impl == null) {
        return;
      }
      impl.setEnabledForAction("LOAD_FILE", _enable);
      impl.setEnabledForAction("ENV_TIMESTEPS", _enable);
    }
  }

  protected int getNbSec() {
    return nbSec_;
  }

  protected boolean isPause() {
    return pause_;
  }

  protected void setNbSec(final int _nbSec) {
    if (nbSec_ != _nbSec) {
      nbSec_ = _nbSec;
      if (timer_ != null) {
        timer_.cancel();
        start();
      }
    }
  }

  public boolean isPostActivated() {
    return proj_ != null;
  }

  public void pause() {
    pause_ = !pause_;
  }

  public void start() {
    if (timer_ == null) {
      timer_ = new Timer();
    }
    // TODO a revoir
//    if (reader_ == null) {
//      reader_ = proj_.createWatcher(src_);
//    }
    if (reader_ == null) {
      return;
    }
    if (proj_ != null && src_ != null) {
      // src_.setInspected(true);
    }

    reader_.setProgression(panel_);
    timer_.schedule(new TimerTaskAdapter(), 500, 1000L * nbSec_);
  }

  public void stop() {
    timer_.cancel();
    reader_.close();
    reader_ = null;
    if (proj_ != null && src_ != null) {
      // src_.setInspected(false);
    }
    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {
        if (panel_ != null && proj_ != null && proj_.getImpl() != null) {
          final BuColumn rightColumn = proj_.getImpl().getMainPanel().getRightColumn();
          rightColumn.remove(panel_.getPn());
          // on remet a jour la colonne
          rightColumn.revalidate();
          updateRubarAction(true);
          proj_.getImpl().setEnabledForAction("MAJ_DATA_ALWAYS", true);
          proj_.getImpl().setEnabledForAction("MAJ_DATA", true);
        }
      }
    });
  }
}
