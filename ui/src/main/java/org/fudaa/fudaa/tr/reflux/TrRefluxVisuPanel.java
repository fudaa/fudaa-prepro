/**
 * @creation 21 nov. 2003
 * @modification $Date: 2007-06-14 12:01:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.h2d.H2dParameters;
import org.fudaa.dodico.h2d.reflux.*;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dRefluxBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.reflux.io.RefluxSolutionInitReader;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.meshviewer.layer.MvFrontierPointLayer;
import org.fudaa.fudaa.meshviewer.layer.MvGridLayerGroup;
import org.fudaa.fudaa.meshviewer.layer.MvNodeLayer;
import org.fudaa.fudaa.meshviewer.model.MvElementModel;
import org.fudaa.fudaa.meshviewer.model.MvNodeModel;
import org.fudaa.fudaa.meshviewer.model.MvNodeModelDefault;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.*;
import org.fudaa.fudaa.tr.post.profile.MvVolumeAction;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.*;

/**
 * @author deniger
 * @version $Id: TrRefluxVisuPanel.java,v 1.44 2007-06-14 12:01:39 deniger Exp $
 */
public final class TrRefluxVisuPanel extends TrVisuPanelEditor {
  TrRefluxInfoSenderDefault infoDelegate_;
  TrRefluxProjet proj_;

  /**
   * @param _impl
   * @param _proj
   */
  public TrRefluxVisuPanel(final FudaaCommonImplementation _impl, final TrRefluxProjet _proj) {
    super(_impl);
    proj_ = _proj;
    infoDelegate_ = new TrRefluxInfoSenderDefault(proj_.getH2dRefluxParametres(), getEbliFormatter());
    addCqInfos(proj_.getH2dRefluxParametres().getMaillage());
    final TrRefluxBcBoundaryMiddleLayer cqBord = new TrRefluxBoundaryMiddleLayer(getBcMng(), getCqLegend(), infoDelegate_);
    final TrBcLayerGroup cl = buildGroupConlim();
    cl.putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Ce groupe de calques permet de g�rer les conditions limites"));
    cl.putClientProperty(CtuluLib.getHelpProperty(), getImpl().buildHelpPathFor("reflux-editor-bc"));
    cl.addBcBoundaryLayer(cqBord);
    cqBord.putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS(TrResource.getS("Gestion des segments fronti�res")));
    cl.addBcPointLayer(createBoundaryPointLayer()).putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Gestion des noeuds fronti�res"));
    final EfGridInterface g = proj_.getRefluxParametres().getMaillage();
    addLayerSiPoints();
    addCqMaillage(g, new MvNodeModelDefault(g, infoDelegate_) {
      @Override
      public EbliFindExpressionContainerInterface getExpressionContainer() {
        return new TrExpressionSupplierForData.Node(proj_.getH2dRefluxParametres().getNodalProvider(), this);
      }
    }, new TrRefluxElementModel(g, infoDelegate_) {
      @Override
      public EbliFindExpressionContainerInterface getExpressionContainer() {
        return new TrExpressionSupplierForData.Element(proj_.getH2dRefluxParametres().getElementProvider(), this);
      }
    });

    // Actions
    addCalqueActions(cqBord, this.buildBoundaryAction());
    addCalqueActions(cl.getBcPointLayer(), this.buildBcPointAction());
    addCalqueActions(getGridGroup().getPolygonLayer(), this.buildElementAction());
    addActionsForNodeLayer();
    getGridGroup().getPointLayer().putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Permet d'editer les propri�t�s nodales"));
    getGridGroup().getPolygonLayer().putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Permet d'�diter les propri�t�s �l�mentaires"));
    getGridGroup().getPolygonLayer().putClientProperty(CtuluLib.getHelpProperty(), getImpl().buildHelpPathFor("reflux-editor-prop"));
    addIsoLayer();
    new TrRefluxVisuPanelModelListener(this);
  }

  @Override
  protected void addCqMaillage(EfGridInterface _m, MvNodeModel _ptModel, MvElementModel _eltModel) {
    if (getGridGroup() != null) {
      return;
    }
    addCqGroupeMaillage(new MvGridLayerGroup(_m, new TrNodeLayerEditable(_ptModel,this), new MvElementLayer(_eltModel)));
  }

  @Override
  protected void fireGridPointModified() {
    proj_.getH2dRefluxParametres().fireNodeXYChanged();
  }

  private EbliActionInterface[] buildBcPointAction() {
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("build bcPointLayer actions");
    }
    final EbliActionInterface[] r = new EbliActionInterface[3];
    r[0] = new EbliActionSimple(TrResource.getS("Editer noeuds"), null, "POINTS_EDIT") {
      @Override
      public void actionPerformed(final ActionEvent _arg0) {
        editBcPoint();
      }
    };
    r[1] = new EbliActionSimple(TrResource.getS("Editer bords"), null, "POINTS_SEGMENT_EDIT") {
      @Override
      public void actionPerformed(final ActionEvent _arg0) {
        editPtBcPointSegmentLayer();
      }
    };
    r[1].putValue(Action.SHORT_DESCRIPTION, TrResource.getS("Les segments dont les noeuds milieux sont s�lectionn�s seront �dit�s"));
    r[2] = getLayerBcPoint().create3DAction(getImpl(), getCmdMng());
    return r;
  }

  private EbliActionInterface[] buildBoundaryAction() {
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("build bcBoundaryLayer actions");
    }
    final EbliActionInterface[] r = new EbliActionInterface[1];
    r[0] = new EbliActionSimple(TrResource.getS("Editer bords"), null, "FRONTIER_EDIT") {
      @Override
      public void actionPerformed(final ActionEvent _arg0) {
        editBoundaryMiddleLayer();
      }
    };
    return r;
  }

  private EbliActionInterface[] buildElementAction() {
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("build bcElement actions");
    }
    final EbliActionInterface[] r = new EbliActionInterface[2];
    r[0] = new ElementEditAction() {
      @Override
      public void actionPerformed(final ActionEvent _arg0) {
        editGridPoly();
      }
    };
    r[1] = new ElementEditGeomAction();
    return r;
  }

  protected final void addLayerSiPoints() {
    buildGroupSiLayers(proj_.getH2dRefluxParametres().getSolutionInitiales());
    final BGroupeCalque gr = buildGroupSI();
    gr.putClientProperty(CtuluLib.getHelpProperty(), getImpl().buildHelpPathFor("reflux-editor-ic"));

    final TrSiNodeModel model = new TrSiNodeModel(proj_.getH2dRefluxParametres().getSolutionInitiales(), infoDelegate_) {
      @Override
      public EbliFindExpressionContainerInterface getExpressionContainer() {
        return new TrExpressionSupplierForData.Node(proj_.getH2dRefluxParametres().getSolutionInitiales(), this);
      }
    };
    final TrSiNodeLayer lay = new TrSiNodeLayer(model);
    lay.setTitle(TrResource.getS("CI: Noeuds"));
    lay.setActions(getActionsForSiNodeLayer());
    gr.add(lay);
  }

  protected void editBoundaryFromMiddleIdx(final EbliListeSelectionMultiInterface _selection) {
    if (_selection == null || _selection.isEmpty()) {
      return;
    }
    final int[] frIdx = _selection.getIdxSelected();
    final int[][] midx = new int[frIdx.length][];
    final H2dRefluxBcManager.RefluxMiddleFrontier[] fr = new H2dRefluxBcManager.RefluxMiddleFrontier[frIdx.length];
    for (int i = 0; i < frIdx.length; i++) {
      midx[i] = _selection.getSelection(frIdx[i]).getSelectedIndex();
      fr[i] = getBcMng().getRefluxMiddleFrontier(frIdx[i]);
    }
    editBoundaryFromMiddleIdx(fr, midx);
  }

  protected void editBoundaryFromMiddleIdx(final H2dRefluxBcManager.RefluxMiddleFrontier[] _fr, final int[][] _midx) {
    if (CtuluLibArray.isEmpty(_fr)) {
      return;
    }
    final Set bdType = new HashSet(H2dRefluxBcManager.getBoundaryTypeComportMap().size());
    for (int i = 0; i < _fr.length; i++) {
      _fr[i].addSelectedBoundaryType(_midx[i], bdType);
    }
    if (bdType.size() == 0) {
      return;
    }
    final H2dBoundaryType[] bdTypeArray = (H2dBoundaryType[]) bdType.toArray(new H2dBoundaryType[bdType.size()]);
    H2dBoundaryType b = null;
    if (bdTypeArray.length == 1) {
      b = bdTypeArray[0];
    } else {
      b = H2dRefluxBoundaryType.MIXTE;
      // vToSet= H2dRefluxBcManager.getCommonVariableAvailable(bdTypeArray);
    }
    final H2dVariableType[] vars = H2dRefluxBcManager.getClBordVariables();
    final Map varValue = new TreeMap();
    Double normal = null;
    for (int i = 0; i < _fr.length; i++) {
      final int[] idx = _midx[i];
      if (i == 0) {
        normal = _fr[i].getCommonNormalFromMiddle(idx);
      } else if (normal != null) {
        final Double newNormal = _fr[i].getCommonNormalFromMiddle(idx);
        // Comparaison de double ....pas beau
        if (newNormal == null || newNormal.doubleValue() != normal.doubleValue()) {
          normal = null;
        }
      }
      for (int j = vars.length - 1; j >= 0; j--) {
        final H2dRefluxValue value = _fr[i].getCommonValueFromMiddle(idx, vars[j], (H2dRefluxValue) varValue.get(vars[j]));
        varValue.put(vars[j], value);
      }
    }

    Set varToEnableIfMixt = null;
    if (b == H2dRefluxBoundaryType.MIXTE) {
      varToEnableIfMixt = H2dRefluxBcManager.getCommonVariableAvailable(bdTypeArray);
    }
    final TrRefluxBoundaryEditor editor = new TrRefluxBoundaryEditor(false, varToEnableIfMixt, varValue, normal,
        H2dRefluxBcManager.getBoundaryTypeComportMap(), b, proj_.getRefluxParametres().getEvolMng());
    if (CtuluDialogPanel.isOkResponse(editor.afficheModale(getFrame(), TrResource.getS("Edition des bords")))) {
      final CtuluCommandComposite cmp = new CtuluCommandComposite();
      final H2dBoundaryType newType = editor.getSelectedBoundaryType();
      final Map newValues = editor.getNewValues();
      final H2dRefluxValue newNormal = editor.getCommonNormalValue();
      for (int i = 0; i < _fr.length; i++) {

        cmp.addCmd(_fr[i].setValues(_midx[i], newType, newValues, newNormal));
      }
      getCmdMng().addCmd(cmp.getSimplify());
    }
  }

  protected void editBoundaryMiddleLayer() {
    editBoundaryFromMiddleIdx(((TrRefluxBcBoundaryMiddleLayer) getLayerBcBoundary()).getLayerSelectionMulti());
  }

  protected void editPtBcPointSegmentLayer() {
    final MvFrontierPointLayer layer = getLayerBcPoint();
    if (layer.isSelectionEmpty()) {
      return;
    }
    final int[] fridx = layer.getLayerSelectionMulti().getIdxSelected();
    final H2dRefluxBcManager.RefluxMiddleFrontier[] fr = new H2dRefluxBcManager.RefluxMiddleFrontier[fridx.length];
    final int[][] midx = new int[fridx.length][];
    for (int i = 0; i < fr.length; i++) {
      fr[i] = getBcMng().getRefluxMiddleFrontier(fridx[i]);
      int idx = 0;
      final int[] select = layer.getLayerSelectionMulti().getSelection(fridx[i]).getSelectedIndex();
      final int[] temp = new int[select.length];
      for (int j = select.length - 1; j >= 0; j--) {
        if (select[j] % 2 == 1) {
          temp[idx++] = EfLib.getIdxMidFromIdx(select[j]);
        }
      }
      if (idx == temp.length) {
        midx[i] = temp;
      } else {
        midx[i] = new int[idx];
        System.arraycopy(temp, 0, midx[i], 0, idx);
      }
    }
    editBoundaryFromMiddleIdx(fr, midx);
  }

  /**
   * Ouvre un dialogue pour choisir le fichier solution final et initialise les solutions initiales.
   */
  public void repriseCalcul() {
    final String s = proj_.getRefluxParametres().getINPVersion().getSolutionFinaleExtension();
    final File f = proj_.getImpl().ouvrirFileChooser(TrResource.getS("Reflux conditions finales"), new String[]{s});
    if (f != null) {
      new CtuluTaskOperationGUI(proj_.getImpl(), CtuluLibString.EMPTY_STRING) {
        @Override
        public void act() {
          final CtuluIOOperationSynthese synthese = RefluxSolutionInitReader.loadSI(proj_.getMaillage(), f, proj_.getImpl()
              .createProgressionInterface(this));
          if (!proj_.getImpl().manageErrorOperationAndIsFatal(synthese)) {
            final H2dRefluxSolutionInitAdapterInterface si = (H2dRefluxSolutionInitAdapterInterface) synthese.getSource();
            if (si.getNbPt() != proj_.getMaillage().getPtsNb()) {
              proj_.getImpl().error(TrResource.getS("Suite de calcul"),
                  TrResource.getS("Les conditions initiales ne sont pas compatibles avec le projet"), false);
            }
            final CtuluAnalyze a = new CtuluAnalyze();
            final CtuluCommand c = proj_.getH2dRefluxParametres().getSolutionInitiales().initFromSI(si, a);
            if (c != null) {
              getCmdMng().addCmd(c);
            }
            if (!proj_.getImpl().manageAnalyzeAndIsFatal(a)) {
              proj_.getImpl().setMainMessageAndClear(TrResource.getS("Importation r�ussie"));
            }
          }
        }
      }.start();
    }
  }

  protected void defineGlobalSi() {
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.addEmptyBorder(10);
    pn.setLayout(new BuGridLayout(2, 5, 5));
    final BuTextField f = pn.addLabelDoubleText(TrResource.getS("Cote d'eau globale"));
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(proj_.getFrame())) && f.getText() != null && f.getText().trim().length() > 0) {
      final double d = Double.parseDouble(f.getText().trim());
      new CtuluTaskOperationGUI(proj_.getImpl(), TrResource.getS("Conditions initiales globales")) {
        @Override
        public void act() {
          proj_.getH2dRefluxParametres().getSolutionInitiales().initFromCoteEau(d, proj_.getImpl().createProgressionInterface(this), getCmdMng());
        }
      }.start();
    }
  }

  private EbliActionInterface[] getActionsForSiNodeLayer() {
    final List r = new ArrayList();
    final EbliActionSimple edit = new SiEditAction();
    r.add(edit);
    r.add(new SiEditGeomAction());
    r.add(null);
    r.add(new SiCheckHauteurEau());
    r.add(new SiClearHAction());
    r.add(null);
    final TrSiProfilLayer pro = (TrSiProfilLayer) getGroupSI().getCalqueParNom(TrSiProfilLayer.getDefaultName());
    final EbliActionInterface compute = pro == null ? null : pro.getComputeAction();
    if (compute != null) {
      r.add(compute);
    }
    r.add(null);
    r.add(new EbliActionSimple(TrResource.getS("Importer un fichier de reprise de calcul (fichier sfv)"), null, "SI_PLAN_CONTINUE_COMPUTE") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        repriseCalcul();
      }
    });
    r.add(new EbliActionSimple(TrResource.getS("Conditions initiales globales"), null, "SI_PLAN_GLOBAL") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        defineGlobalSi();
      }
    });
    r.add(new MvVolumeAction(getParams().createGridDataAdapter(), TrRefluxVisuPanel.this));
    final EbliActionInterface[] rf = new EbliActionInterface[r.size()];
    r.toArray(rf);
    return rf;
  }

  public final MvFrontierPointLayer createBoundaryPointLayer() {
    final TrBcPointModelDefault r = new TrBcPointModelDefault(getGrid());
    r.setDelegate(infoDelegate_);
    return new TrRefluxBcNodeLayer(r, proj_.getRefluxParametres().getRefluxBcMng());
  }

  @Override
  public String editBcPoint() {
    final MvFrontierPointLayer layer = getLayerBcPoint();
    final int i = layer.isSelectionInUniqueBloc();
    if (i < 0) {
      return TrResource.getS("Une seule fronti�re doit �tre s�lectionn�e");
    }
    final int[] select = layer.getLayerSelectionMulti().getSelection(i).getSelectedIndex();
    final H2dRefluxBcManager.RefluxMiddleFrontier frontier = getBcMng().getRefluxMiddleFrontier(i);
    final H2dBoundaryType bd = frontier.getCommonBoundaryType(select);
    final H2dVariableType[] vars = H2dRefluxBcManager.getClNodaleVariables();
    Set varToEnableIfMixt = null;
    if (bd == H2dRefluxBoundaryType.MIXTE) {
      varToEnableIfMixt = frontier.getAvailablesVariables(select);
    }
    final Map varValue = new TreeMap();
    for (int j = vars.length - 1; j >= 0; j--) {
      varValue.put(vars[j], frontier.getCommonValue(select, vars[j]));
    }
    final Double normal = frontier.getCommonNormal(select);
    final TrRefluxBoundaryEditor editor = new TrRefluxBoundaryEditor(true, varToEnableIfMixt, varValue, normal,
        H2dRefluxBcManager.getBoundaryTypeComportMap(), bd, proj_.getRefluxParametres().getEvolMng(), false);
    if (editor.afficheModaleOk(getFrame(), TrResource.getS("Edition points s�lectionn�s"))) {
      getCmdMng().addCmd(frontier.setValuesForPoints(select, editor.getNewValues(), editor.getCommonNormalValue()));
    }
    return null;
  }

  @Override
  public H2dParameters getParams() {
    return proj_.getH2dRefluxParametres();
  }

  @Override
  public final boolean isGridPointEditable() {
    return true;
  }

  @Override
  public String editGridPoly() {
    final ZCalqueAffichageDonnees cq = getGridGroup().getPolygonLayer();
    if (!cq.isSelectionEmpty()) {
      final int[] s = cq.getSelectedIndex();
      final H2dRefluxElementPropertyMngAbstract m = proj_.getElementPropMng();
      final TrRefluxPropEditor editor = new TrRefluxPropEditor(m, proj_.getRefluxParametres().getEvolMng(), s);
      if (CtuluDialogPanel.isOkResponse(editor.afficheModale(getFrame(), TrResource.getS("propri�t�s �l�mentaires")))) {
        getCmdMng().addCmd(proj_.getElementPropMng().setValues(s, editor.getNewValues()));
      }
    }
    return null;
  }

  /**
   * @return le calque des conditions limites
   */
  public TrBcBoundaryLayer getBcLayer() {
    return getLayerBcBoundary();
  }

  /**
   * @return le manager des conditions limites
   */
  public H2dRefluxBcManager getBcMng() {
    return proj_.getRefluxParametres().getRefluxBcMng();
  }

  @Override
  public String getProjectName() {
    return proj_.getTitle() == null ? "reflux" : proj_.getTitle();
  }

  @Override
  public EfGridInterface getGrid() {
    return proj_.getMaillage();
  }

  /*
   * public void getIsoPainters(final Collection _setToFill) {
   * TrIsoPainter.fillWithIsoPainter(getParams().getNodalData(), _setToFill, false);
   * TrIsoPainter.fillWithIsoPainter(getParams().getSiData(), _setToFill, true);
   * TrIsoPainter.fillWithIsoPainter(getParams().getElementData(), _setToFill, true); }
   */

  @Override
  public boolean isBcPointEditable() {
    return true;
  }

  @Override
  public boolean isGridElementEditable() {
    return true;
  }

  protected void addActionsForNodeLayer() {
    final MvNodeLayer l = getGridGroup().getPointLayer();
    final String desc = TrResource.getS("Affiche les noeuds du maillage");
    l.putClientProperty(Action.SHORT_DESCRIPTION, desc);
    final TrRefluxForceEnableAction actVent = new TrRefluxForceEnableAction(proj_, ((H2dRefluxParameters) getParams()).getPropNodal().getForceData(
        H2dVariableType.VENT), getCmdMng(), getImpl());
    final TrRefluxForceEnableAction actRadiation = new TrRefluxForceEnableAction(proj_, ((H2dRefluxParameters) getParams()).getPropNodal()
        .getForceData(H2dVariableType.RADIATION), getCmdMng(), getImpl());
    addCalqueActions(l, new EbliActionInterface[]{new NodalEditAction(), new NodalEditGeomAction(), null, actVent, actRadiation,
        new TrEditPointAction(this),new TrValidGridAction(this)});
  }
}
