package org.fudaa.fudaa.tr.post.actions;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import javax.swing.tree.TreePath;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostMultiSourceActivator;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;

/**
 * Action de suppression d un source du multi-projet.
 * 
 * @author Adrien Hadoux
 */
// FIXME Fred: traduire et utilise les actions de Fudaa
public class TrPostActionRemoveSrc extends EbliActionSimple {
  /**
   * projet en cours contenant les multi projets.
   */
  TrPostProjet projet_;

  /**
   * Activator multi source permettant de gerer une nouvelle source pour trpostProjet
   */
  TrPostMultiSourceActivator multiProjectActivator_;

  public TrPostActionRemoveSrc(final TrPostProjet _projet) {
    super(TrLib.getString("Enlever projet"), BuResource.BU.getIcon("crystal_enlever"), "REMOVE_PROJECT");
    projet_ = _projet;

    // -- creation de l activator multi-source avec le projet param --//
    this.multiProjectActivator_ = new TrPostMultiSourceActivator(_projet);
  }

  @Override
  public void actionPerformed(final ActionEvent e) {

    // -- recuperation de la source a supprimer --//

    // -- cas rien de selectionnee --//
    if (projet_.getSources().getSrcSize() <= 1) {
      // FIXME . utilise impl_.error()
      // projet_.getImpl().error(_titre, _text)
      projet_.getImpl().error(
          TrResource.getS("Ce fichier ne peut �tre supprim�.\n Il faut garder au moins un fichier de donn�es."));
      return;
    }

    if (projet_.filleProjetctManager_.listProjets_.getSelectedRow() == -1) {
      // FIXME . utilise impl_.error()
      projet_.getImpl().error(
          TrResource.getS("Il faut selectionner le fichier � retirer de la liste avant de cliquer."));
    } else {

      // -- recupeation de la source a enlever --//
      int intSelect = projet_.filleProjetctManager_.listProjets_.getSelectedRow();
      TreePath path = projet_.filleProjetctManager_.listProjets_.getPathForRow(intSelect);
      MutableTreeTableNode treeNode = (MutableTreeTableNode) path.getLastPathComponent();
      Object selection = null;
      if (treeNode != null) selection = treeNode.getUserObject();

      if (selection != null && selection instanceof TrPostSource) {

      } else {
        projet_.getImpl().error("Il faut s�lectionner un projet (ic�ne r�pertoire) pour supprimer le projet");
        return;
      }

      final TrPostSource src = (TrPostSource) selection;

      // final TrPostSource src = projet_.listeSrc_.get(projet_.filleProjetctManager_.listProjets_.getSelectedRow());

      if (projet_.getImpl().question(TrLib.getString("Supprimer la source"),
          TrLib.getString("�tes-vous sur de vouloir enlever \n le fichier " + src.getTitle() + "\n du projet?"))) {
        if (projet_.removeSource(src)) {
          projet_.impl_.message(TrResource.getS("Le fichier a �t� correctement enlev� du projet"));
          // -- notify aux observers --//
          projet_.notifyObservers();

        } else {
          projet_.impl_.error(TrResource.getS("Le fichier n'a pas �t� correctement enlev� du projet."));
        }

      }
    }

  }
}
