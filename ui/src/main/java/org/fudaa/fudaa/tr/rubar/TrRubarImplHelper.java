/**
 *  @creation     9 juin 2004
 *  @modification $Date: 2007-05-04 14:01:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuLib;
import com.memoire.bu.BuMenuItem;
import com.memoire.fu.FuLog;
import java.io.File;
import java.util.Map;
import java.util.Properties;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.tr.common.TrImplHelperAbstract;
import org.fudaa.fudaa.tr.common.TrImplementationEditorAbstract;
import org.fudaa.fudaa.tr.common.TrLauncher;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrProjet;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarImplHelper.java,v 1.19 2007-05-04 14:01:53 deniger Exp $
 */
public final class TrRubarImplHelper extends TrImplHelperAbstract {

  public static String getID() {
    return FileFormatSoftware.RUBAR_IS.name;
  }

  /**
   * @param _l le lanceur d'appli
   */
  public TrRubarImplHelper(final TrLauncher _l) {
    super(new TrRubarAppliManager(_l));
  }

  @Override
  public TrProjet creer(final TrImplementationEditorAbstract _impl, final ProgressionInterface _op,
      final File _initGrid, final Properties _options) {
    return TrRubarProjectFactory.createNewProject(_initGrid, _op, _impl);

  }

  @Override
  public BuMenuItem[] getMenuItemsOuvrir(final TrImplementationEditorAbstract _impl) {
    final BuMenuItem r = new BuMenuItem(FudaaResource.FUDAA.getToolIcon("appli/reflux"), TrResource
        .getS("Projet Reflux"));
    r.setActionCommand(TrImplementationEditorAbstract.PREF_OUVRIR + FileFormatSoftware.REFLUX_IS.name);
    final BuMenuItem r0 = new BuMenuItem(FudaaResource.FUDAA.getToolIcon("appli/telemac"), TrResource
        .getS("Projet T�l�mac"));
    r0.setActionCommand(TrImplementationEditorAbstract.PREF_OUVRIR + FileFormatSoftware.TELEMAC_IS.name);
    return new BuMenuItem[] { r, r0 };
  }

  @Override
  public String getSoftwareID() {
    return getID();
  }

  @Override
  public TrProjet ouvrir(final TrImplementationEditorAbstract _impl, final ProgressionInterface _op) {
    final File fic = _impl.ouvrirFileChooser(TrResource.getS("fichier rubar"), null);
    if (fic == null) {
      return null;
    } else if (getAppliMng().isEditorAlreadyOpen(fic)) {
      _impl.error(TrLib.getMessageAlreadyOpen(fic));
      BuLib.invokeLater(new Runnable() {
        @Override
        public void run() {
          getAppliMng().getEditorOpenedFor(fic).getFrame().toFront();
        }
      });
      return null;

    }
    return ouvrir(_impl, _op, fic, null);

  }

  @Override
  public TrProjet ouvrir(final TrImplementationEditorAbstract _impl, final ProgressionInterface _op, final File _f,
      final Map _params) {
    if (_f != null && getAppliMng().isEditorAlreadyOpen(_f)) {
      _impl.error(TrLib.getMessageAlreadyOpen(_f));
      BuLib.invokeLater(new Runnable() {
        @Override
        public void run() {
          getAppliMng().getEditorOpenedFor(_f).getFrame().toFront();
        }
      });
      return null;
    }
    if (_f == null) { return ouvrir(_impl, _op); }
    try {
      return TrRubarProjectFactory.loadProject(_f, _op, _impl);
    } catch (final Throwable e) {
      FuLog.warning(e.getMessage(),e);
    }
    return null;
  }
}
