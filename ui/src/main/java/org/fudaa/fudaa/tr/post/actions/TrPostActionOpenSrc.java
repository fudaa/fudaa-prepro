package org.fudaa.fudaa.tr.post.actions;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuResource;
import com.memoire.fu.FuLog;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileFilter;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.dodico.rubar.io.RubarFileFormatDefault;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;
import org.fudaa.fudaa.tr.common.TrFileFormatManager;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostMultiSourceActivator;
import org.fudaa.fudaa.tr.post.TrPostMultiSourceActivator2;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;

/**
 * Gere l'action d'ouvrir un nouveau source et de l integrer au projet
 *
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public class TrPostActionOpenSrc extends EbliActionSimple {

  /**
   * projet en cours contenant les multi projets.
   */
  TrPostProjet projet_;
  /**
   * Activator multi source permettant de gerer une nouvelle source pour trpostProjet
   */
  TrPostMultiSourceActivator multiProjectActivator_;

  public TrPostActionOpenSrc(final TrPostProjet _projet) {
    super(TrLib.getString("Ajouter projet"), BuResource.BU.getIcon("crystal_ouvrirprojet"), "ADD_PROJECT");
    projet_ = _projet;

    // -- creation de l activator multi-source avec le projet param --//
    this.multiProjectActivator_ = new TrPostMultiSourceActivator(_projet);
  }

  class SwingWorkerCompletionWaiter implements PropertyChangeListener {

    private JDialog dialog;

    public SwingWorkerCompletionWaiter(JDialog dialog) {
      this.dialog = dialog;
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
      if ("state".equals(event.getPropertyName()) && SwingWorker.StateValue.DONE == event.getNewValue()) {
        dialog.setVisible(false);
        dialog.dispose();
      }
    }
  }

  @Override
  public void actionPerformed(final ActionEvent e) {

    final File fichierAbsolu = selectFile();

    // -- verification que le fichier n est pas deja ouvert --//
    if (fichierAbsolu == null) {
      return;
    }
    // -- tentative de chargement du source dans le multi-projet --//
    multiProjectActivator_.active(fichierAbsolu, projet_.impl_);
  }

  public TrPostSource openSource() {
    final File fichierAbsolu = selectFile();

    // -- verification que le fichier n est pas deja ouvert --//
    if (fichierAbsolu == null) {
      return null;
    }

    JDialog dialog = new JDialog(projet_.getImpl().getFrame(), true);
    SwingWorker<TrPostSource, TrPostSource> swingWorker = new SwingWorker<TrPostSource, TrPostSource>() {
      @Override
      protected TrPostSource doInBackground() throws Exception {
        return new TrPostMultiSourceActivator2(projet_).active(fichierAbsolu, projet_.impl_, null);
      }
    };
    dialog.setContentPane(new BuLabel(TrResource.getS("Chargement...")));
    swingWorker.addPropertyChangeListener(new SwingWorkerCompletionWaiter(dialog));
    swingWorker.execute();
    // the dialog will be visible until the SwingWorker is done
    dialog.pack();
    dialog.setModal(true);
    dialog.setVisible(true);
    try {
      return swingWorker.get();
    } catch (Exception e) {
      FuLog.error(e);
    }
    return null;

  }

  public File selectFile() {
    FileFormat[] listeFormats = TrFileFormatManager.getAllGridFormat();

    List<FileFilter> filters = new ArrayList<FileFilter>();
    for (int i = 0; i < listeFormats.length; i++) {
      filters.add(listeFormats[i].createFileFilter());
    }
    filters.add(new RubarFileFormatDefault("TPC").createFileFilter());
    filters.add(new RubarFileFormatDefault("TPS").createFileFilter());

    final File fichierAbsolu = FudaaGuiLib.ouvrirFileChooser(TrResource.getS("S�lectionnez le fichier r�sultat"),
            filters.toArray(new FileFilter[filters.size()]), projet_.getImpl().getParentComponent(), false, "preproPostSource");

    // -- verification que le fichier n est pas deja ouvert --//
    if (fichierAbsolu != null && projet_.getSources().isSourceLoaded(fichierAbsolu.getAbsolutePath())) {
      projet_.getImpl().warn(TrLib.getString("Fichier r�sultat d�j� charg� dans le projet"),
              TrLib.getString("Le fichier source existe d�j� et ne peut �tre ajout� au projet."));
      return null;
    }
    return fichierAbsolu;
  }
}
