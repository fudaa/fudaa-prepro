/*
 * @creation 9 mars 2006
 * 
 * @modification $Date: 2007-06-28 09:28:19 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormatVersionInterface;
import org.fudaa.dodico.ef.io.serafin.SerafinMaillageBuilderAdapter;
import org.fudaa.dodico.h2d.H2dNodalPropertiesMngI;
import org.fudaa.dodico.h2d.telemac.H2dTelemacParameters;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.telemac.io.TelemacVariableMapper;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author fred deniger
 * @version $Id: TrTelemacSerafinSaver.java,v 1.9 2007-06-28 09:28:19 deniger Exp $
 */
public class TrTelemacSerafinSaver extends TelemacVariableMapper {
  
  private final H2dTelemacParameters params_;

  /**
   * @param _params
   */
  public TrTelemacSerafinSaver(final H2dTelemacParameters _params) {
    super();
    params_ = _params;
    
  }
  
  boolean saveGeometryFile(final File _f, final ProgressionInterface _prog, final FudaaCommonImplementation _ui, final boolean _isInitialGridSave) {
    return saveGeometryFile(_f, _prog, _ui, _isInitialGridSave, params_.getNodalProperties(), 0, params_.getInitialDateInMillis());
  }
  
  private boolean saveGeometryFile(final File _f, final ProgressionInterface _prog, final FudaaCommonImplementation _ui,
          final boolean _isInitialGridSave, final H2dNodalPropertiesMngI _prop, final double _timeStep, long idate) {
    boolean r = _isInitialGridSave;
    if (!_isInitialGridSave) {
      // si le fichier init n'existe pas c'est que le mot-cle maillage a �t� change
      if (_f.exists()) {
        r = saveCopy(_f, _ui, _f);
      } else {
        r = true;
      }
    }
    if (CtuluLibMessage.INFO) {
      CtuluLibMessage.info("save geometry file in " + _f);
    }
    saveSerafin(_f, _prog, _ui, _prop, _timeStep, idate);
    return r;
  }

  /**
   * @param _f
   * @param _prog
   * @param _ui
   * @param _prop
   * @param _timeStep
   */
  public void saveSerafin(final File _f, final ProgressionInterface _prog, final FudaaCommonImplementation _ui, final H2dNodalPropertiesMngI _prop,
          final double _timeStep, final long idate) {
    final List varList = _prop.getAllVariables();
    final Map m = varList == null ? Collections.EMPTY_MAP : new HashMap(varList.size());
    if (varList != null) {
      final int nb = varList.size();
      for (int i = 0; i < nb; i++) {
        
        final H2dVariableType ti = (H2dVariableType) varList.get(i);
        m.put(ti, _prop.getModifiableModel(ti));
      }
    }
    saveSerafin(_f, _prog, _ui, m, _timeStep, idate);
  }
  
  public void saveSerafin(final File _f, final ProgressionInterface _prog, final FudaaCommonImplementation _ui, final Map _prop,
          final double _timeStep, final long idate) {
    final SerafinFileFormatVersionInterface ft = (SerafinFileFormatVersionInterface) params_.getTelemacVersion().getDefaultSerafinFormat();
    Map values = _prop;
    if (values == null) {
      values = Collections.EMPTY_MAP;
    }
    final String[] name = new String[values.size()];
    final String[] unit = new String[name.length];
    final CtuluCollectionDouble[] doubleValues = new CtuluCollectionDouble[name.length];
    final int langu = params_.getTelemacVersion().getDico().getLanguageIndex();
    int idx = 0;
    for (final Iterator it = values.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      final H2dVariableType ti = (H2dVariableType) e.getKey();
      String namei = getSerafinKnownVar(ti, langu);
      if (namei == null) {
        namei = ti.getName();
      }
      final String uniti = getUnits(namei);
      name[idx] = namei;
      unit[idx] = uniti;
      doubleValues[idx] = (CtuluCollectionDouble) e.getValue();
      idx++;
    }
    final SerafinMaillageBuilderAdapter a = new SerafinMaillageBuilderAdapter(ft, params_.getMaillage());
    a.setTitre(params_.getDicoParams().getTitle());
    a.setFirstTimeStep(_timeStep);
    a.setDate(idate);
    a.setxYdoublePrecision(params_.isXyDoublePrecision());
    a.setValues(name, doubleValues, unit);
    _ui.manageErrorOperationAndIsFatal(ft.writeSparc(_f, a, _prog));
    _ui.clearMainMessageInTwoSec();
  }

  /**
   * @param _f
   * @param _ui
   * @param _initFile
   * @return
   */
  private boolean saveCopy(final File _f, final FudaaCommonImplementation _ui, final File _initFile) {
    boolean r;
    final String s = TrResource.getS("Sauvegarde du fichier de de g�om�trie...");
    _ui.setMainMessage(s);
    r = true;
    final File saveFile = new File(_initFile.getAbsolutePath() + "-" + FudaaLib.getCurrentDateForFile() + ".old");
    if (saveFile.exists() || CtuluLibFile.canWrite(saveFile) != null) {
      new TrTelemacSaveBackupGeometryFile(_initFile).afficheModale(_ui.getFrame(), s);
    } else {
      CtuluLibMessage.info("save old geometry file in " + _f);
      CtuluLibFile.copyFile(_initFile, saveFile);
      _ui.setMainMessageAndClear(TrResource.getS("Copie du fichier de g�om�trie effecut�e:") + CtuluLibString.ESPACE + saveFile.getName());
    }
    
    return r;
  }
  
  boolean saveSiFile(final File _f, final ProgressionInterface _prog, final FudaaCommonImplementation _ui, final boolean _isInitialGridSave,
          final long initTime) {
    return saveGeometryFile(_f, _prog, _ui, _isInitialGridSave, params_.getSolutionsInit(), params_.getSolutionsInit().getTimeStep(), initTime);
  }
}
