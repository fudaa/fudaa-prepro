/**
 * @creation 16 d�c. 2004
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuMenu;
import com.memoire.fu.FuLog;
import gnu.trove.*;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.helper.LRUCache;
import org.fudaa.dodico.h2d.rubar.H2dRubarGrid;
import org.fudaa.dodico.h2d.rubar.H2dRubarGridAreteSource;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageContainer;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.dodico.rubar.io.*;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.data.*;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: TrPostSourceRubar.java,v 1.39 2007-06-28 09:28:18 deniger Exp $
 */
public class TrPostSourceRubar extends TrPostSourceAbstractFromKey {
  class EnvelopListModel extends AbstractListModel {
    @Override
    public Object getElementAt(final int _index) {
      return envVar_[_index];
    }

    @Override
    public int getSize() {
      return envVar_ == null ? 0 : envVar_.length;
    }
  }

  private class TimeStepProcedure implements TIntProcedure {
    double[] dest_;
    int idx_;

    TimeStepProcedure(final double[] _dest) {
      dest_ = _dest;
    }

    @Override
    public boolean execute(final int _value) {
      dest_[idx_++] = time_.getTimeStep(_value);
      return true;
    }
  }

  private final TrPostRubarBcEdgeModel bcEdgeModel_;
  TrPostDataCreatedConstant frt_;
  private TrPostRubarEdgesResults edgesRes_;
  // autant que le nombre d'�l�ments
  private Map maxResults_;
  private boolean tpcShowError_ = true;
  private boolean tpsShowError_ = true;
  double[] allTimeStep_;
  private EnvelopListModel envModel_;
  private H2dVariableType[] envVar_;
  private H2dVariableTypeCreated envVitesse_;
  TIntObjectHashMap exposeTimeExtrapolValues_;
  TIntIntHashMap exposeTimeInitTime_;
  private Set<String> formatLoaded_;
  private H2dRubarGridAreteSource gridSrc_;
  TDoubleObjectHashMap initEnvResultat_;
  private TrPostRubarLimniMng limni_;
  private  Map<Integer, SoftReference> zfnCacheMap = Collections.synchronizedMap(new LRUCache<Integer, SoftReference>(8));
  /**
   * several file are used for transports: TPC, TPC1,.... files
   */
  Map<TrPostKey, RubarSolutionSequentielReader> tpcReadersByKeys = new HashMap<>();
  RubarSolutionSequentielReader tpsReader_;
  boolean useEnvTime_;
  RubarSolutionSequentielReader zfnReader_;
  RubarDFZResult dzfResult;
  File tpsFile_;
  RubarTimeIndexResolver timeIndexResolver_ = new RubarTimeIndexResolver();

  @Override
  public File getMainFile() {
    if (tpsFile_ == null) {
      final File f = mainFile_;
      tpsFile_ = FileFormat.getFileExistFor(f.getParentFile(), CtuluLibFile.getSansExtension(f.getName()), "tps");
      if (tpsFile_ == null) {
        tpsFile_ = f;
      }
    }
    return tpsFile_;
  }

  File mainFile_;

  /**
   * @param _selectedFile le fichier selectionne pour l'ouverture
   * @param _titre le titre
   * @param _g le maillage
   * @param _impl l'impl parente
   */
  public TrPostSourceRubar(final File _selectedFile, final String _titre, final H2dRubarGridAreteSource _g, final CtuluUI _impl) {
    super(_titre, _g.getRubarGrid(), null, _impl);
    mainFile_ = _selectedFile;
    bcEdgeModel_ = TrPostRubarBcEdgeModel.buildModel(_g.getRubarGrid(), null);
    gridSrc_ = _g;
  }

  @Override
  public H2dRubarGrid getGrid() {
    return (H2dRubarGrid) super.getGrid();
  }

  private EfData getInitDataTPC(RubarSolutionSequentielReader tpcReader, final int _varIdx, final int _timeIdx) {
    boolean ok = true;
    double[][] tpcValues = new double[tpcReader.getNbVar()][tpcReader.getNbElt()];
    try {
      if (useEnvTime_ && exposeTimeInitTime_ != null) {
        if (exposeTimeInitTime_.containsKey(_timeIdx)) {
          ok = tpcReader.read(exposeTimeInitTime_.get(_timeIdx), tpcValues);
        } else {
          getMaxContainer(_timeIdx).getValuesinTPC(tpcValues, tpcReader);
        }
      } else {
        ok = tpcReader.read(_timeIdx, tpcValues);
      }
    } catch (final IOException _e) {
      if (!inspected_) {
        impl_.error(CtuluUIAbstract.getDefaultErrorTitle(), _e.getLocalizedMessage(), false);
      }
      FuLog.warning(_e);
      return null;
    }
    if (!inspected_ && !ok && tpcShowError_) {
      final String erreur = CtuluLib.getS("Le fichier {0} est corrompu", "TPC") + CtuluLibString.LINE_SEP
          + CtuluLib.getS("Pas de temps:" + super.getTimeListModel().getElementAt(_timeIdx));
      super.impl_.error(CtuluUIAbstract.getDefaultErrorTitle(), erreur, false);
      tpcShowError_ = false;
    }
    return new EfDataElement(CtuluLibArray.copy(tpcValues[_varIdx]));
  }

  private EfData getInitDataTPS(final int _varIdx, final int _timeIdx) {
    if (_timeIdx < 0 || tpsReader_ == null) {
      return null;
    }
    double[][] tpsValues_ = new double[tpsReader_.getNbVar()][tpsReader_.getNbElt()];
    boolean ok = true;
    try {

      if (useEnvTime_ && exposeTimeInitTime_ != null) {
        if (exposeTimeInitTime_.containsKey(_timeIdx)) {
          ok = tpsReader_.read(exposeTimeInitTime_.get(_timeIdx), tpsValues_);
        } else {
          final TrPostSourceRubarMaxContainer max = getMaxContainer(_timeIdx);
          if (max != null) {
            max.getValueTPS(tpsValues_, tpsReader_);
            ok = true;
          }
        }
      } else {
        try {
          ok = tpsReader_.read(_timeIdx, tpsValues_);
        } catch (final NumberFormatException e1) {
          if (!inspected_ && tpsShowError_) {
            super.impl_.error(CtuluUIAbstract.getDefaultErrorTitle(), CtuluLib.getS("Le fichier {0} est corrompu", "TPS") + CtuluLibString.LINE_SEP
                + CtuluLib.getS("Pas de temps:") + ' ' + super.getTimeListModel().getElementAt(
                _timeIdx), false);
            tpsShowError_ = false;
          }
          FuLog.warning("TPS: Erreur pour lire la variable " + _varIdx + " au temps " + _timeIdx);
          return null;
        }
      }
    } catch (final IOException _e) {
      if (!inspected_) {
        impl_.error(CtuluUIAbstract.getDefaultErrorTitle(), _e.getLocalizedMessage(), false);
        FuLog.warning(_e);
      }
      return null;
    }
    if (!inspected_ && !ok && tpsShowError_) {
      final String erreur = CtuluLib.getS("Le fichier {0} est corrompu", "TPS") + CtuluLibString.LINE_SEP
          + CtuluLib.getS("Pas de temps:" + super.getTimeListModel().getElementAt(_timeIdx));
      super.impl_.error(CtuluUIAbstract.getDefaultErrorTitle(), erreur, false);
      tpsShowError_ = false;
    }
    return new EfDataElement(CtuluLibArray.copy(tpsValues_[_varIdx]));
  }

  private double getInitDataTPC(RubarSolutionSequentielReader tpcReader, final int _varIdx, final int _timeIdx, final int _ptIdx) {
    try {
      if (useEnvTime_ && exposeTimeExtrapolValues_ != null) {
        if (exposeTimeInitTime_.containsKey(_timeIdx)) {
          return tpcReader.read(_varIdx, exposeTimeInitTime_.get(_timeIdx), _ptIdx);
        }
        return getMaxContainer(_timeIdx).getValueTPC(_varIdx, _ptIdx, tpcReader);
      }
      return tpcReader.read(_varIdx, _timeIdx, _ptIdx);
    } catch (final IOException _e) {
      if (!inspected_) {
        impl_.error(CtuluUIAbstract.getDefaultErrorTitle(), _e.getLocalizedMessage(), false);
      }
      FuLog.warning(_e);
    }
    return -1;
  }

  private double getInitDataTPS(final int _varIdx, final int _timeIdx, final int _ptIdx) {
    try {
      if (useEnvTime_ && exposeTimeExtrapolValues_ != null) {
        if (exposeTimeInitTime_.containsKey(_timeIdx)) {
          return tpsReader_.read(_varIdx, exposeTimeInitTime_.get(_timeIdx), _ptIdx);
        }
        final TrPostSourceRubarMaxContainer max = getMaxContainer(_timeIdx);
        if (max != null) {
          return max.getValueTPS(_varIdx, _ptIdx, tpsReader_);
        }
      }
      return tpsReader_.read(_varIdx, _timeIdx, _ptIdx);
    } catch (final IOException _e) {
      if (!inspected_) {
        impl_.error(CtuluUIAbstract.getDefaultErrorTitle(), _e.getLocalizedMessage(), false);
      }
      FuLog.warning(_e);
    }
    return -1;
  }

  private EfData getInitDataZFN(final int _timeIdx) {
    final SoftReference dataNodeReference = zfnCacheMap.get(_timeIdx);
    if (dataNodeReference != null && dataNodeReference.get() != null) {
      return (EfData) dataNodeReference.get();
    }
    try {
      final double[][] initValues = getBathyValues(_timeIdx);
      if (initValues == null) {
        return null;
      }
      double[] zfnValues = initValues[0];
      final EfDataNode efDataNode = new EfDataNode(CtuluLibArray.copy(zfnValues));
      zfnCacheMap.put(_timeIdx, new SoftReference<>(efDataNode));
      return efDataNode;
    } catch (final IOException _e) {
      if (!inspected_) {
        impl_.error(CtuluUIAbstract.getDefaultErrorTitle(), _e.getMessage(), false);
      }
      FuLog.warning(_e);
      return null;
    }
  }

  /**
   * @param _timeIdx
   * @return tableau de double dont le 2 est de la taille du nombre de points.
   */
  double[][] getBathyValues(final int _timeIdx) throws IOException {
    final double[][] initValues = new double[1][g_.getPtsNb()];
    if (zfnReader_ == null) {
      for (int i = g_.getPtsNb() - 1; i >= 0; i--) {
        initValues[0][i] = g_.getPtZ(i);
      }
    } else {
      if (useEnvTime_ && exposeTimeExtrapolValues_ != null) {
        if (exposeTimeInitTime_.containsKey(_timeIdx)) {
          zfnReader_.read(exposeTimeInitTime_.get(_timeIdx), initValues);
        } else {
          final TrPostSourceRubarMaxContainer c = getMaxContainer(_timeIdx);
          if (c == null) {
            return null;
          }
          c.getValueZFN(initValues, zfnReader_);
        }
      } else {
        zfnReader_.read(_timeIdx, initValues);
      }
    }
    return initValues;
  }

  private synchronized double getInitDataZFN(final int _timeIdx, final int idxPt) {
    try {
      if (useEnvTime_ && exposeTimeExtrapolValues_ != null) {
        if (exposeTimeInitTime_.containsKey(_timeIdx)) {
          return zfnReader_.read(0, exposeTimeInitTime_.get(_timeIdx), idxPt);
        } else {
          final TrPostSourceRubarMaxContainer c = getMaxContainer(_timeIdx);
          if (c == null) {
            return -1;
          }
          c.getValueZFN(idxPt, zfnReader_);
        }
      } else {
        return zfnReader_.read(0, _timeIdx, idxPt);
      }
    } catch (final IOException _e) {
      if (!inspected_) {
        impl_.error(CtuluUIAbstract.getDefaultErrorTitle(), _e.getMessage(), false);
      }
      FuLog.warning(_e);
    }
    return -1;
  }

  TrPostRubarLimniMng createLimni() {
    if (limni_ == null) {
      limni_ = new TrPostRubarLimniMng();
    }
    return limni_;
  }

  String[] getLoadedFmtId() {
    return formatLoaded_ == null ? null : (String[]) formatLoaded_.toArray(new String[formatLoaded_.size()]);
  }

  boolean isLoaded(final String _fmtName) {
    return formatLoaded_ != null && formatLoaded_.contains(_fmtName);
  }

  void setEdgesResults(final int[] _idx, final RubarOutEdgeResult[] _res) {
    if (edgesRes_ == null) {
      edgesRes_ = new TrPostRubarEdgesResults(impl_);
    }
    edgesRes_.setOuvrageResults(_idx, _res);
  }

  void setOuvrages(final H2dRubarOuvrageContainer _ouvrages) {
    if (edgesRes_ == null) {
      edgesRes_ = new TrPostRubarEdgesResults(impl_);
    }
    edgesRes_.setOuvrages(gridSrc_, _ouvrages);
  }

  void setNuaResults(RubarNUAResult result) {
    if (edgesRes_ == null) {
      edgesRes_ = new TrPostRubarEdgesResults(impl_);
    }
    edgesRes_.setNua(result);
  }

  void setEnvResultats(final Map _res) {
    if (maxResults_ == null) {
      maxResults_ = _res;
      initEnvelopVar();
    }
  }

  List<File> filesLoaded = new ArrayList<File>();

  void setLoaded(final String _fmt, File f) {
    if (formatLoaded_ == null) {
      formatLoaded_ = new HashSet<String>(10);
    }
    filesLoaded.add(f);
    formatLoaded_.add(_fmt);
  }

  @Override
  protected void addOtherVariables(final Map _l, final Map _fleche) {
    if (frt_ != null) {
      _l.put(H2dVariableType.COEF_FROTTEMENT_FOND, frt_);
    }
    if (getInitDataIndex(H2dVariableType.HAUTEUR_EAU) != null) {
      if (getInitDataIndex(H2dVariableType.DEBIT_X) != null && (!_l.containsKey(H2dVariableType.VITESSE_U))) {
        _l.put(H2dVariableType.VITESSE_U, new TrPostDataCreatedRubarVitesse(this, H2dVariableType.DEBIT_X));
      }
      if (getInitDataIndex(H2dVariableType.DEBIT_Y) != null && (!_l.containsKey(H2dVariableType.VITESSE_V))) {
        _l.put(H2dVariableType.VITESSE_V, new TrPostDataCreatedRubarVitesse(this, H2dVariableType.DEBIT_Y));
      }
      if (getInitDataIndex(H2dVariableType.BATHYMETRIE) == null && !_l.containsKey(H2dVariableType.BATHYMETRIE)) {
        _l.put(H2dVariableType.BATHYMETRIE, new TrPostDataCreatedRubarBathy(this));
        if (!_l.containsKey(H2dVariableType.COTE_EAU)) {
          _l.put(H2dVariableType.COTE_EAU, new TrPostDataCreatedPlus(this, H2dVariableType.BATHYMETRIE,
              H2dVariableType.HAUTEUR_EAU));
        }
      }
    }
    if (_l.containsKey(H2dVariableType.VITESSE_U) && _l.containsKey(H2dVariableType.VITESSE_V) && !_l.containsKey(
        H2dVariableType.VITESSE)) {
      final TrPostFlecheContent vec = createSimpleVecteurContent(H2dVariableType.VITESSE, H2dVariableType.VITESSE_U,
          H2dVariableType.VITESSE_V);
      _fleche.put(H2dVariableType.VITESSE, vec);
      _l.put(H2dVariableType.VITESSE, vec);
    }
    addFroud(_l);
    final ListModel envelopModel = getEnvelopModel();
    if (envelopModel != null) {
      if (envVitesse_ == null) {
        envVitesse_ = H2dVariableType.createTempVar(TrResource.getS("Vitesse maxi"), "maxV", H2dVariableType.getVitesseUnit(),
            getShortNameCreateVar());
        envVitesse_.setParent(H2dVariableType.VITESSE);
      }
      _fleche.put(envVitesse_, new TrPostRubarEnvFlecheContent(this, envVitesse_, H2dVariableType.VITESSE_U,
          H2dVariableType.VITESSE_V));
      // on ajoute les variables max dans les variables normales.
      for (int i = envVar_.length - 1; i >= 0; i--) {
        final H2dVariableTypeCreated newVar = H2dVariableType.createTempVar("Max: " + envVar_[i].getName(), null,
            envVar_[i].getCommonUnitString(),
            getShortNameCreateVar());
        newVar.setParent(envVar_[i].getParentVariable());
        _l.put(newVar, new TrPostDataCreatedConstant(this, getEnveloppData(envVar_[i]), newVar));
      }
    }
  }

  @Override
  protected void afterOpenDatas(final ProgressionInterface _int, final CtuluAnalyze _analyze) {
    super.afterOpenDatas(_int, _analyze);
    final RubarPARFileFormat ft = RubarPARFileFormat.getInstance();
    final File par = ft.getFileFor(getMainFile().getParentFile(), CtuluLibFile.getSansExtension(getMainFile().getName()));
    if (par.exists()) {
      final CtuluIOOperationSynthese s = ft.getLastVersionImpl().read(par, _int);
      if (!impl_.manageErrorOperationAndIsFatal(s)) {
        final DicoParams p = RubarPARFileFormat.createParams((TIntObjectHashMap) s.getSource());
        DicoEntite e = p.getDico().getEntite("Acc�l�ration de la pesanteur");
        if (e == null) {
          e = p.getDico().getEntite("Gravity acceleration");
        }
        if (e != null) {
          final String gravity = p.getValue(e);
          try {
            setGravity(Double.parseDouble(gravity));
          } catch (final NumberFormatException e1) {
          }
        }
      }
    }
  }

  @Override
  protected TrPostDataMinMaxGlobalItem computeMinMax(final H2dVariableType _variable, final ProgressionInterface _prog) {
    if (!useEnvTime_ || exposeTimeExtrapolValues_ == null) {
      return super.computeMinMax(_variable, _prog);
    }
    final ProgressionUpdater up = createUpdaterForMinMax(_variable);
    up.majProgessionStateOnly();
    TIntIntIterator it = exposeTimeInitTime_.iterator();
    TrPostExtremVisitor visitor = null;
    for (int i = exposeTimeInitTime_.size(); i-- > 0; ) {
      it.advance();
      final int idxTime = it.key();
      final EfData d = getData(_variable, idxTime);
      if (visitor == null) {
        visitor = new TrPostExtremVisitor(d.isElementData() ? getGrid().getEltNb() : getGrid().getPtsNb(), d.isElementData());
      }
      visitor.setCurrentTime(getTimeStep(idxTime));
      d.iterate(visitor);
      up.majAvancement();
    }
    it = null;
    final TIntObjectIterator itObj = exposeTimeExtrapolValues_.iterator();
    for (int i = exposeTimeExtrapolValues_.size(); i-- > 0; ) {
      itObj.advance();
      final TrPostSourceRubarMaxContainer c = (TrPostSourceRubarMaxContainer) itObj.value();
      c.iterate(visitor, _variable, itObj.key(), this);
      up.majAvancement();
    }
    impl_.clearMainProgression();
    return new TrPostDataMinMaxGlobalItem(_variable.getShortName(), visitor);
  }

  protected TrPostSourceRubarMaxContainer getMaxContainer(final int _i) {
    return (TrPostSourceRubarMaxContainer) exposeTimeExtrapolValues_.get(_i);
  }

  @Override
  public void activate() {
    final TrPostRubarLoader loader = new TrPostRubarLoader(this, null, false);
    if (loader.setToLoad(getMainFile())) {
      loader.active(null, impl_, null);
    }
  }

  @Override
  public boolean isOpened(final File _f) {
    if (_f == null) {
      return false;
    }
    return getFiles().contains(_f);
  }

  /**
   * Appele pour ajouter des variables apres initialisation.
   *
   * @param initialMap
   */
  final void addInitData(final Map<H2dVariableType, TrPostKey> initialMap) {
    if (initialMap == null || initialMap.size() == 0) {
      return;
    }
    boolean added = false;
    for (Map.Entry<H2dVariableType, TrPostKey> entry : initialMap.entrySet()) {
      if (!this.keyByVariables.containsKey(entry.getKey())) {
        added = true;
        keyByVariables.put(entry.getKey(), entry.getValue());
        super.vars.add(entry.getKey());
      }
    }
    // pas de modif
    if (!added) {
      return;
    }
    buildDefaultVectors();
    updateVarList();
    fireFlecheListModelChanged();
  }

  protected void initEnvelopVar() {
    if (envVar_ == null && maxResults_ != null) {
      envVar_ = new H2dVariableType[maxResults_.size()];
      maxResults_.keySet().toArray(envVar_);
      Arrays.sort(envVar_);
    }
  }

  /**
   * Cette variable est utilise pour stocker temporairement le temps du fichier env.
   */
  private double envTMax_ = -1;

  void setEnvTime(final double _envTMax) {
    envTMax_ = _envTMax;
  }

  void clearEnvTime() {
    setEnvTime(-1);
  }

  @Override
  protected boolean readMinMaxSave() {
    return false;
  }

  protected void setNewCoteEau(final double[] _coteEau) {
    if (FuLog.isTrace()) {
      FuLog.trace("new water elevation");
    }
    maxResults_.put(H2dVariableType.COTE_EAU, new EfDataElement(_coteEau));
  }

  protected synchronized void updateTimeStep(final int _oldNb) {
    time_.updateTimeStepStructure(_oldNb);
    clearCaches();
  }

  @Override
  public void close() {
    if (tpsReader_ != null) {
      try {
        tpsReader_.close();
      } catch (final IOException e) {
        FuLog.warning(e);
      }
    }
    if (zfnReader_ != null) {
      try {
        zfnReader_.close();
      } catch (final IOException e) {
        FuLog.warning(e);
      }
    }
    for (RubarSolutionSequentielReader reader : tpcReadersByKeys.values()) {
      if (reader != null) {
        try {
          reader.close();
        } catch (final IOException e) {
          FuLog.warning(e);
        }
      }
    }

    super.close();
  }

  public TrPostInspectorReader createWatcher(final TrPostProjet _proj, final boolean _auto) {
    final int nbLoaded = formatLoaded_ == null ? 0 : formatLoaded_.size();
    // pour rubar on interdit la maj auto si + d'un fichier est charge
    // Si >1, la coherence des fichiers n'est pas assur�e.
    if (_auto && nbLoaded != 1) {
      _proj.getImpl().error(TrResource.getS("L'inspection n'est possible que si un seul fichier est charg�"));
      return null;
    }
    return new TrPostInspectorReaderRubar(_proj, this);
  }

  /**
   * @param _m les infos a remplir
   * @param _element
   * @param _x le x du point d'interpolation
   * @param _y le y du point d'interpolation
   * @param _addXY true si on doit ajoute les coordonnees du point interpole
   */
  public void fillMaximaInterpolateInfo(final InfoData _m, final int _element, final double _x, final double _y,
                                        final boolean _addXY) {
    _m.setTitle("-- " + TrResource.getS("Maxima:") + CtuluLibString.ESPACE + TrResource.getS("Interpolation") + " --");
    if (_element >= 0 && envVar_ != null) {
      if (_addXY) {
        _m.put("X", CtuluLib.DEFAULT_NUMBER_FORMAT.format(_x));
        _m.put("Y", CtuluLib.DEFAULT_NUMBER_FORMAT.format(_y));
      }
      for (int i = 0; i < envVar_.length; i++) {
        final EfData d = getEnveloppData(envVar_[i]);
        _m.put(envVar_[i].getName(), CtuluLib.DEFAULT_NUMBER_FORMAT.format(d.getValue(_element)));
      }
    }
  }

  @Override
  public H2dVariableType[] getAllVariablesNonVec() {
    final Set<H2dVariableType> s = new HashSet<H2dVariableType>(Arrays.asList(variable_));
    if (envVar_ != null) {
      s.addAll(Arrays.asList(envVar_));
    }
    final H2dVariableType[] rf = new H2dVariableType[s.size()];
    s.toArray(rf);
    Arrays.sort(rf);
    return rf;
  }

  public final TrPostRubarBcEdgeModel getBcEdgeModel() {
    return bcEdgeModel_;
  }

  @Override
  public EfData getData(final H2dVariableType _variable, final int _timeStep) {
    if (envVitesse_ != null && _variable == envVitesse_) {
      return getFlecheContent(envVitesse_).getValues(_timeStep);
    }
    return super.getData(_variable, _timeStep);
  }

  Map<H2dVariableType, TrPostDataCreatedDefault> sedimentVariable = new HashMap<>();

  @Override
  public EfData getInitData(H2dVariableType _varIdx, int _timeIdx) {
    if (sedimentVariable.containsKey(_varIdx)) {
      final int sedIdx = this.timeIndexResolver_.getSEDTimeStep(_timeIdx);
      return sedimentVariable.get(_varIdx).buildDataFor(sedIdx);
    }
    return super.getInitData(_varIdx, _timeIdx);
  }

  @Override
  public double getInitData(H2dVariableType _varIdx, int _timeIdx, int _ptIdx) throws IOException {
    if (sedimentVariable.containsKey(_varIdx)) {
      final int sedIdx = this.timeIndexResolver_.getSEDTimeStep(_timeIdx);
      return sedimentVariable.get(_varIdx).buildDataFor(sedIdx, _ptIdx);
    }
    return super.getInitData(_varIdx, _timeIdx, _ptIdx);
  }

  @Override
  public double getData(final H2dVariableType _variable, final int _timeStep, final int _idxPt) throws IOException {
    if (envVitesse_ != null && _variable == envVitesse_) {
      return getFlecheContent(envVitesse_).getValues(_timeStep).getValue(_idxPt);
    }
    return super.getData(_variable, _timeStep, _idxPt);
  }

  public final TrPostRubarEdgesResults getEdgesRes() {
    return edgesRes_;
  }

  public ListModel getEnvelopModel() {
    if (envModel_ == null && isEnvelopDataSet()) {
      envModel_ = new EnvelopListModel();
    }
    return envModel_;
  }

  public EfData getEnveloppData(final H2dVariableType _t) {
    if (maxResults_ == null) {
      return null;
    }
    return (EfData) maxResults_.get(_t);
  }

  public H2dVariableType[] getEnvVar() {
    if (envVar_ == null) {
      return null;
    }
    final H2dVariableType[] res = new H2dVariableType[envVar_.length];
    System.arraycopy(envVar_, 0, res, 0, res.length);
    return res;
  }

  @Override
  public EfData getInitData(final TrPostKey trPostKey, final int timeIdx) {
    if (trPostKey == null) {
      return null;
    }
    if (TrPostRubarKeys.ZFN.equalsIgnoreCase(trPostKey.key)) {
      final int zfnIdx = this.timeIndexResolver_.getZFNTimeStep(timeIdx);
      return getInitDataZFN(zfnIdx);
    }
    if (TrPostRubarKeys.DZF.equalsIgnoreCase(trPostKey.key)) {
      final int dzfIdx = this.timeIndexResolver_.getDZFTimeStep(timeIdx);
      if (dzfIdx >= 0 && dzfIdx < dzfResult.getBlocs().size()) {
        return new EfDataNode(dzfResult.getBlocs().get(dzfIdx).getDzfs());
      }
    }
    if (TrPostRubarKeys.TPS.equalsIgnoreCase(trPostKey.key)) {
      final int tpsIdx = this.timeIndexResolver_.getTPSTimeStep(timeIdx);
      return getInitDataTPS(trPostKey.idxVariable, tpsIdx);
    }
    if (TrPostRubarKeys.TPC.equalsIgnoreCase(trPostKey.key)) {
      final int tpcIdx = this.timeIndexResolver_.getTPCTimeStep(timeIdx);
      final RubarSolutionSequentielReader tpcReader = tpcReadersByKeys.get(trPostKey);
      if (tpcReader != null) {
        return getInitDataTPC(tpcReader, trPostKey.idxVariable, tpcIdx);
      }
    }
    return null;
  }

  @Override
  public double getInitData(final TrPostKey trPostKey, final int _timeIdx, final int _ptIdx) {
    if (TrPostRubarKeys.ZFN.equalsIgnoreCase(trPostKey.key)) {
      final int zfnIdx = this.timeIndexResolver_.getZFNTimeStep(_timeIdx);
      return getInitDataZFN(zfnIdx, _ptIdx);
    }
    if (TrPostRubarKeys.TPS.equalsIgnoreCase(trPostKey.key)) {
      final int tpsIdx = this.timeIndexResolver_.getTPSTimeStep(_timeIdx);
      return getInitDataTPS(trPostKey.idxVariable, tpsIdx, _ptIdx);
    }
    if (TrPostRubarKeys.TPC.equalsIgnoreCase(trPostKey.key)) {
      final int tpcIdx = this.timeIndexResolver_.getTPCTimeStep(_timeIdx);
      final RubarSolutionSequentielReader tpcReader = tpcReadersByKeys.get(trPostKey);
      if (tpcReader != null) {
        return getInitDataTPC(tpcReader, trPostKey.idxVariable, tpcIdx, _ptIdx);
      }
    }
    return 0;
  }

  public TrPostRubarLimniMng getLimni() {
    return limni_;
  }

  public boolean isEnvelopDataSet() {
    return maxResults_ != null;
  }

  public boolean isEnvTimeStepActivated() {
    return useEnvTime_;
  }

  public boolean isEnvTimeStepAvailable() {
    return allTimeStep_ != null;
  }

  /**
   * @return true si le fond est variable
   */
  public final boolean isFondVariable() {
    return zfnReader_ != null;
  }

  /**
   * @return true si limni charg�
   */
  public boolean isLimniLoaded() {
    return limni_ != null && limni_.isLimniPointsLoaded();
  }

  @Override
  public final boolean containsElementVar() {
    return true;
  }

  @Override
  public final boolean isVolumique() {
    return true;
  }

  @Override
  public boolean isElementVar(final CtuluVariable _idxVar) {
    //fot bathymetry and sediment variable,t the datas are defined on nodes
    if (H2dVariableType.BATHYMETRIE.equals(_idxVar) || sedimentVariable.containsKey(_idxVar)) {
      return false;
    }

    return isVolumique();
  }

  @Override
  public TrPostVisuPanel buildVisuPanel(final TrPostProjet _parent, final BCalqueLegende _legende) {
    return new TrPostVisuPanelRubar(_parent.getImpl(), _parent, _legende, this);
  }

  @Override
  public void addSpecificItemInMainMenu(final BuMenu _m, final TrPostCommonImplementation _impl) {
    new TrPostRubarSpecificAction(this, _impl).addSpecificItemInMainMenu(_m);
  }

  public double getEnvTMax() {
    return envTMax_;
  }

  @Override
  public Collection<File> getFiles() {
    return filesLoaded;
  }

  /**
   * @param timeIndexResolver the timeIndexResolver to set
   */
  public void setTimeIndexResolver(RubarTimeIndexResolver timeIndexResolver) {
    if (timeIndexResolver == null) {
      this.timeIndexResolver_ = new RubarTimeIndexResolver();
    } else {
      this.timeIndexResolver_ = timeIndexResolver;
    }
  }
}
