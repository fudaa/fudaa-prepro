/*
 * @creation 3 mai 2005
 *
 * @modification $Date: 2007-04-30 14:22:38 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuComparator;
import com.memoire.fu.FuSort;
import org.fudaa.dodico.ef.EfFilterTime;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrPreferences;
import org.fudaa.fudaa.tr.common.TrResource;

import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: TrPostFilterLayer.java,v 1.11 2007-04-30 14:22:38 deniger Exp $
 */
public class TrPostFilterLayer implements EfFilterTime, TrPostListener {
  /**
   * @return true si la prop strict est validee dans les preferences
   */
  public static boolean getAlwaysStrictFromPref() {
    return TrPreferences.TR.getBooleanProperty("filter.strict.enable", false);
  }

  /**
   * Recuperer les variables a ignorer des preferences.
   *
   * @return les var a ignorer ou null.
   */
  public static String[] getVarFromPref() {
    return TrPreferences.TR.getStringArrayProperty(getFilterKey(), new String[]{H2dVariableType.BATHYMETRIE.getShortName(),
        H2dVariableType.COEF_FROTTEMENT_FOND.getShortName()});
  }

  private static String getFilterKey() {
    return "filter.ignore.vars";
  }

  /**
   * Recuperer les variables a ignorer des preferences.
   *
   * @return les var a ignorer ou null.
   */
  public static H2dVariableType[] getH2dVarFromPref(final Map _shortNameCreated) {
    final String[] avoidVarShortNames = getVarFromPref();
    return getH2dVarFromShortName(_shortNameCreated, avoidVarShortNames);
  }

  private static H2dVariableType[] getH2dVarFromShortName(final Map _shortNameCreated, final String[] _avoidVarShortNames) {
    if (_avoidVarShortNames == null || _avoidVarShortNames.length == 0) {
      return new H2dVariableType[0];
    }
    final Collection avoidVarList = new HashSet(_avoidVarShortNames.length);
    for (int i = _avoidVarShortNames.length - 1; i >= 0; i--) {
      final H2dVariableType t = H2dVariableType.getVarFromShortName(_avoidVarShortNames[i], _shortNameCreated);
      if (t != null) {
        avoidVarList.add(t);
      }
    }
    final H2dVariableType[] res = (H2dVariableType[]) avoidVarList.toArray(new H2dVariableType[avoidVarList.size()]);
    FuSort.sort(res, FuComparator.STRING_COMPARATOR);
    return res;
  }

  /**
   * Enleve la pref strict.
   */
  public static void removeAlwaysStrictInPref() {
    TrPreferences.TR.removeProperty("filter.strict.enable");
  }

  /**
   * Supprimer les variables a ignorer des preferences.
   */
  public static void removeVarInPref() {
    TrPreferences.TR.removeProperty(getFilterKey());
  }

  /**
   * @param _yes la nouvelle valeurs de la prop strict
   */
  public static void setAlwaysStrictInPref(final boolean _yes) {
    TrPreferences.TR.putBooleanProperty("filter.strict.enable", _yes);
  }

  /**
   * @param _d le tableau a enregistrer dans les preferences.
   */
  public static void setVarInPref(final String[] _d) {
    if (_d == null) {
      TrPreferences.TR.removeProperty(getFilterKey());
    } else {
      TrPreferences.TR.putStringArrayProperty(getFilterKey(), _d);
    }
  }

  /**
   * Les noms courts des variables non touch�es.
   */
  private H2dVariableType[] avoidVar_;
  private Set<H2dVariableType> avoidVarSet_;
  transient int currentTime_ = -1;
  TrPostFilterHauteur hauteur_;
  boolean isHauteurActivated_;
  // boolean isStrict_;
  boolean isHauteurDefinedOnMeshes;

  /**
   * @param _src la source pour recuperer les noms courts �ventuels
   */
  public TrPostFilterLayer(final TrPostSource _src) {
    super();
    isHauteurDefinedOnMeshes = _src.isElementVar(H2dVariableType.HAUTEUR_EAU);
    setAvoidVar(getH2dVarFromPref(_src.getShortNameCreateVar()));
    isHauteurActivated_ = TrPostFilterHauteur.isActivatedFromPref();
    if (isHauteurActivated_) {
      hauteur_ = createPostDataConditionHauteur();
    }
  }

  private Set<H2dVariableType> createSetOfAvoidVars() {
    return avoidVar_ == null ? Collections.<H2dVariableType>emptySet() : new TreeSet<H2dVariableType>(Arrays.asList(avoidVar_));
  }

  public Object createSaver() {
    final TrPostFilterSaver res = new TrPostFilterSaver();
    res.saveIsHauteurActivated_ = isHauteurActivated_;
    res.saveAvoidVar_ = new String[avoidVar_ == null ? 0 : avoidVar_.length];
    if (avoidVar_ != null) {
      for (int i = res.saveAvoidVar_.length - 1; i >= 0; i--) {
        res.saveAvoidVar_[i] = avoidVar_[i].getShortName();
      }
    }
    if (hauteur_ != null) {
      res.saverHauteurFilter_ = hauteur_.createSaver();
    }

    return res;
  }

  public void restoreFromSaver(final Object _o, final TrPostSource _src) {
    if (!(_o instanceof TrPostFilterSaver)) {
      return;
    }
    final TrPostFilterSaver saver = (TrPostFilterSaver) _o;
    setAvoidVar(getH2dVarFromShortName(_src.getShortNameCreateVar(), saver.saveAvoidVar_));
    // isStrict_ = saver.saveIsStrict_;
    isHauteurActivated_ = saver.saveIsHauteurActivated_;
    if (isHauteurActivated_) {
      hauteur_ = createPostDataConditionHauteur();
      hauteur_.restoreFromSaver(saver.saverHauteurFilter_);
    }
  }

  protected TrPostFilterHauteur createPostDataConditionHauteur() {
    return isHauteurDefinedOnMeshes ? new TrPostFilterHauteurElement() : new TrPostFilterHauteur();
  }

  protected TrPostFilterHauteur createPostDataConditionHauteur(final TrPostFilterHauteur _h) {
    return isHauteurDefinedOnMeshes ? new TrPostFilterHauteurElement(_h) : new TrPostFilterHauteur(_h);
  }

  final H2dVariableType[] getAvoidVar() {
    return avoidVar_;
  }

  final void setAvoidVar(final H2dVariableType[] _avoidVar) {
    avoidVar_ = _avoidVar;
    avoidVarSet_ = createSetOfAvoidVars();
  }

  public static void fillWithHauteurAvoidVar(final StringBuffer _buf, final H2dVariableType[] _avoidVar) {
    if (_avoidVar != null && _avoidVar.length > 0) {
      for (int i = 0; i < _avoidVar.length; i++) {
        _buf.append("<br> - ").append(_avoidVar[i]);
      }
    } else {
      _buf.append("<br> - ").append(TrResource.getS("Aucune"));
    }
  }

  public final TrPostFilterHauteur getHauteur() {
    return hauteur_;
  }

  public double getHauteurEps() {
    return hauteur_ == null ? 0 : hauteur_.getEps();
  }

  /**
   * @param _cond la condition sur la hauteur a prendre en compte
   * @param _src la source
   */
  public boolean initFor(final TrPostFilterHauteur _cond, final TrPostSource _src) {
    if (_cond == hauteur_) {
      return false;
    }
    if (hauteur_ == null) {
      hauteur_ = createPostDataConditionHauteur(_cond);
      hauteur_.updateTimeStep(currentTime_, _src);
    } else {
      hauteur_.initFrom(_cond);
    }
    return true;
  }

  public boolean isVarModifiedByFilter(final H2dVariableType _var) {
    // si la variable doit etre ignoree on renvoie false
    if (_var == null || (avoidVarSet_ != null && avoidVarSet_.contains(_var))) {
      return false;
    }
    return true;
  }

  @Override
  public boolean isActivated(final int _idxPt) {
    if (isHauteurActivated()) {
      return hauteur_.isActivated(_idxPt);
    }
    return true;
  }

  @Override
  public boolean isActivatedElt(final int _idxElt) {
    if (isHauteurActivated()) {
      return hauteur_.isActivatedElt(_idxElt);
    }
    return true;
  }

  @Override
  public boolean isActivatedElt(final int _idxElt, boolean strict) {
    if (isHauteurActivated()) {
      return hauteur_.isActivatedElt(_idxElt, strict);
    }
    return true;
  }

  public boolean isConditionContainsVar() {
    return avoidVar_ != null;
  }

  public boolean isHauteurActivated() {
    return hauteur_ != null && isHauteurActivated_;
  }

  public boolean isHauteurDefined() {
    return hauteur_ != null;
  }

  public boolean setHauteurActivated(final boolean _b, final TrPostSource _src) {
    if (_b == isHauteurActivated_) {
      return false;
    }
    if (_b && hauteur_ == null) {
      hauteur_ = createPostDataConditionHauteur();
    }
    isHauteurActivated_ = _b;
    if (isHauteurActivated_) {
      hauteur_.updateTimeStep(currentTime_, _src);
    }
    return true;
  }

  @Override
  public void updateTimeStep(final int _t, final EfGridData _src) {
    currentTime_ = _t;
    if (isHauteurActivated()) {
      hauteur_.updateTimeStep(_t, _src);
    }
  }

  @Override
  public boolean valueChanged(final H2dVariableType _t, final TrPostSource _src) {
    // la condition hauteur est la seule qui
    if (hauteur_ != null) {
      return hauteur_.valueChanged(_t, _src);
    }
    return false;
  }
}
