/*
 *  @creation     6 avr. 2005
 *  @modification $Date: 2008-03-26 16:46:43 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import javax.swing.JPanel;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.h2d.H2DSiProfilListener;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeDefault;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliListeSelectionMulti;
import org.fudaa.ebli.commun.EbliSelectionState;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.trace.TraceLigneVisuComponent;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.sig.layer.FSigLayerLineEditable;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.persistence.TrSiProfilLayerPersistence;

/**
 * @author Fred Deniger
 * @version $Id: TrSiProfilLayer.java,v 1.35.4.1 2008-03-26 16:46:43 bmarchan Exp $
 */
public class TrSiProfilLayer extends FSigLayerLineEditable implements H2DSiProfilListener {

  protected class ComputeAction extends EbliActionSimple {

    protected ComputeAction() {
      super(TrResource.getS("Calculer h,u,v"), null, "COMPUTE_HUV_LEVEL");
      setDefaultToolTip(TrResource.getS("Calcul de la surface libre, u et v � partir des surfaces libres"));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final BCalque siGroup = (BCalque) getParent();
      final TrSiNodeLayer nodeLayer = (TrSiNodeLayer) siGroup.getCalqueParNom(TrSiNodeLayer.getDefaultName());
      if (!nodeLayer.getSource().isSolutionInitialesActivated()) { return; }
      TrSiPlanLayer planLayer = getPlanLayer(siGroup);
      // on doit calculer les plans avant de determiner u,v,h
      if (getProfilModel().isProfilTwoPointOut() && planLayer == null) {
        buildPlanLayer(null);
        planLayer = getPlanLayer(siGroup);
      }
      if (!getProfilModel().isProfilTwoPointOut() && isPlanLayerEmpty()) {
        computePlans();
        planLayer = getPlanLayer(siGroup);

      }

      // si le calque des plans existe et s'il y a des zones d�finies.
      if (!isPlanLayerEmpty() || (getProfilModel().isProfilTwoPointOut())) {
        final TrSiProfilModel.Zone z = (TrSiProfilModel.Zone) planLayer.getGeomData();

        final TrSiProfilModel model = (TrSiProfilModel) modeleDonnees();
        final CtuluTaskDelegate task = editor_.getUi().createTask((String) super.getValue(Action.NAME));
        task.start(new Runnable() {

          @Override
          public void run() {
            model.computeZeUV(z, nodeLayer.getSource(), task.getStateReceiver());
          }
        });

      }
    }

    private TrSiPlanLayer getPlanLayer(final BCalque _siGroup) {
      return (TrSiPlanLayer) _siGroup.getCalqueParNom(TrSiPlanLayer.getDefaultName());
    }

    @Override
    public String getEnableCondition() {
      final BCalque siGroup = (BCalque) getParent();
      final TrSiNodeLayer nodeLayer = (TrSiNodeLayer) siGroup.getCalqueParNom(TrSiNodeLayer.getDefaultName());
      if (nodeLayer == null || nodeLayer.getSource() == null || !nodeLayer.getSource().isSolutionInitialesActivated()) { return TrResource
          .getS("Les conditions initiales (reprise de calcul) ne sont pas activ�es"); }
      return TrResource.getS("Les surfaces libres doivent �tre actualis�es");
    }

    @Override
    public void updateStateBeforeShow() {
      final TrSiNodeLayer nodeLayer = (TrSiNodeLayer) ((BCalque) getParent()).getCalqueParNom(TrSiNodeLayer
          .getDefaultName());
      if (!nodeLayer.getSource().isSolutionInitialesActivated()) {
        super.setEnabled(false);
        return;
      }
      // actif si les plans sont calculables
      super.setEnabled(getProfilModel() != null && getProfilModel().isProfilLineDefined());
      if (!isEnabled()) {
        super.setEnabled(!isPlanLayerEmpty() || getProfilModel().isProfilTwoPointOut());
      }
    }
  }

  boolean isPlanLayerEmpty() {
    final BCalque siGroup = (BCalque) getParent();
    final TrSiPlanLayer planLayer = (TrSiPlanLayer) siGroup.getCalqueParNom(TrSiPlanLayer.getDefaultName());
    return planLayer == null || planLayer.modeleDonnees() == null || planLayer.modeleDonnees().getNombre() == 0;

  }

  protected class PlanAction extends EbliActionSimple {

    PlanAction() {
      super(TrResource.getS("Construire les surfaces libres"), null, "BUILD_PLANE");
      setDefaultToolTip(TrResource.getS("Construire les surfaces libres � partir du profil en long"));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      computePlans();

    }

    @Override
    public String getEnableCondition() {
      if (getProfilModel() != null) {
        if (isLevelUpdateToDate_) { return TrResource.getS("Les surfaces libres sont � jour"); }
        if (getProfilModel().isProfilLineDefined()) { return TrResource.getS("D�finir un profil en long"); }
      }
      return CtuluLibString.EMPTY_STRING;

    }

    @Override
    public void updateStateBeforeShow() {
      // l'action est valide que si les plans ne sont pas a jour et si un profil
      // existe.
      setEnabled(!isLevelUpdateToDate_ && getProfilModel() != null && getProfilModel().isProfilLineDefined());
    }
  }

  protected TrSiProfilModel.Zone computePlans() {
    // les plans n'existent pas: on les cree
    final CtuluAnalyze analyze = new CtuluAnalyze();
    if (plans_ == null) {
      buildPlanLayer(null);
    }
    // sinon on le met a jour
    getProfilModel().computeZones(plans_.getZone(), analyze);
    plans_.repaint();
    boolean res = true;
    if (analyze.containsFatalError()) {
      editor_.getUi().error(TrResource.getS("Construire les surfaces libres"), analyze.getFatalError(),
          false);
      res = false;
    } else {
      isLevelUpdateToDate_ = true;
    }
    editor_.getPanel().getArbreCalqueModel().fireObservableChanged();
    return res ? plans_.getZone() : null;
  }

  protected class SetProfilAction extends EbliActionSimple {

    protected SetProfilAction() {
      super(TrResource.getS("D�finir comme profil"), null, "DEFINE_AS_PROFIL");
      setDefaultToolTip(TrResource.getS("D�finit la ligne s�lectionn�e comme profil"));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if (isSelectionEmpty() && isAtomicMode()) { return; }
      final int idx = getLayerSelection().getMaxIndex();
      final TrSiProfilModel model = (TrSiProfilModel) modeleDonnees();
      model.setProfil(idx, model.cmd_, true);

    }

    @Override
    public String getEnableCondition() {
      return TrResource.getS("S�lectionner une ligne");
    }

    @Override
    public void updateStateBeforeShow() {
      super.setEnabled(modeleDonnees().getNombre() > 0 && !isAtomicMode() && !isSelectionEmpty()
          && getLayerSelection().isOnlyOnIndexSelected());
    }
  }

  protected class ValidAction extends EbliActionSimple {

    ValidAction() {
      super(TrResource.getS("Valider"), null, "VALID_PLANE");
      setDefaultToolTip(TrResource.getS("Valide les donn�es"));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if (getProfilModel().getNombre() == 0 || !getProfilModel().isProfilLineDefined()) {
        editor_.getUi().error(CtuluUIAbstract.getDefaultErrorTitle(), TrResource.getS("Aucun profil n'est d�fini"),
            false);
        return;
      }
      final EbliListeSelectionMulti outPoints = getProfilModel().getProfilPointsOut();
      if (outPoints != null && !outPoints.isEmpty()) {
        setSelectionMode(SelectionMode.ATOMIC);
        TrSiProfilLayer.this.changeSelectionMulti(outPoints, EbliSelectionState.ACTION_REPLACE);
        editor_.getUi().error(
            CtuluUIAbstract.getDefaultErrorTitle(),
            TrResource.getS("Des points internes au profil sont � l'ext�rieur du mod�le") + CtuluLibString.LINE_SEP
                + TrResource.getS("Ils sont s�lectionn�s."), false);
        return;
      }
      final int[] berges = getProfilModel().getBergesIntersectProfil();
      if (berges != null && berges.length > 0) {
        setSelectionMode(SelectionMode.ATOMIC);
        final CtuluListSelection si = new CtuluListSelection();
        for (int i = berges.length - 1; i >= 0; i--) {
          si.setSelectionInterval(berges[i], berges[i]);
        }
        TrSiProfilLayer.this.changeSelection(si, EbliSelectionState.ACTION_REPLACE);
        editor_.getUi().error(
            CtuluUIAbstract.getDefaultErrorTitle(),
            TrResource.getS("Des berges coupent le profil") + CtuluLibString.LINE_SEP
                + TrResource.getS("Elles sont s�lectionn�es."), false);
      } else {
        editor_.getUi().message(CtuluUIAbstract.getDefaultInfoTitle(), TrResource.getS("Les profils sont valides"),
            false);
      }

    }

  }

  /**
   * @return le nom par defaut pour ce calque
   */
  public static String getDefaultName() {
    return "cqSiProfil";
  }

  private ComputeAction computeAction_;

  transient EbliActionInterface export_;

  transient EbliActionInterface import_;

  // EbliActionInterface[] actionPlan_;
  boolean isLevelUpdateToDate_;

  TrSiPlanLayer plans_;

  FSigEditor editor_;

  /**
   * @param _modele le modele associe
   * @param _legende la legende
   */
  public TrSiProfilLayer(final TrSiProfilModel _modele, final BCalqueLegende _legende, final FSigEditor _editor) {
    super(_modele, _editor);
    _modele.addListener(this);
    super.setName(getDefaultName());
    setTitle(TrResource.getS("Profils"));
    editor_ = _editor;
    ligneModelOuvert_ = new TraceLigneModel(TraceLigne.TIRETE, 2f, Color.GRAY);
    iconeModelOuvert_ = new TraceIconModel(TraceIcon.RIEN, 4, Color.GRAY);
    ligneModel_.updateData(new TraceLigneModel(TraceLigne.LISSE, 2f, Color.ORANGE.darker()));
    iconModel_ = new TraceIconModel(TraceIcon.RIEN, 4, ligneModel_.getCouleur());
    setLegende(_legende);
    final EbliActionInterface[] edit = editor_.getEditAction();
    if (edit == null) {
      super.setActions(new EbliActionInterface[] { new PlanAction(), new ValidAction() });
    } else {
      final List act = new ArrayList(edit.length + 5);

      int i = 0;
      for (i = 0; i < edit.length; i++) {
        act.add(edit[i]);
      }
      act.add(new TrSiInterpolAction(this));
      // le separateur
      act.add(null);
      act.add(editor_.getActionDelete());
      act.add(null);
      act.add(new SetProfilAction());
      act.add(new ValidAction());
      act.add(new PlanAction());
      act.add(getComputeAction());
      act.add(null);
      export_ = editor_.getExportAction();
      import_ = editor_.getImportAction();
      act.add(export_);
      if (import_ != null) {
        act.add(import_);
      }

      final EbliActionInterface[] newAct = new EbliActionInterface[act.size()];
      act.toArray(newAct);
      super.setActions(newAct);
    }

  }

  private JPanel buildPanel(final JPanel _dest) {
    JPanel result = _dest;
    if (result == null) {
      result = new BuPanel();
      result.setOpaque(false);
      result.setLayout(new BuGridLayout(2, 5, 2));
    } else {
      result.removeAll();
    }
    BuLabel lb = new BuLabel(TrResource.getS("Profil en long"));
    lb.setOpaque(false);
    result.add(lb);
    result.add(new TraceLigneVisuComponent(ligneModel_));
    lb = new BuLabel(TrResource.getS("Limites "));
    lb.setOpaque(false);
    result.add(lb);
    result.add(new TraceLigneVisuComponent(ligneModelOuvert_));
    return result;
  }

  @Override
  protected void construitLegende() {
    final BCalqueLegende l = getLegende();
    if (l == null) { return; }
    if (!l.containsLegend(this)) {
      l.ajoute(this, buildPanel(null), getTitle());
      if (getModelePoly().getNombre() == 0) {
        l.setVisible(this, false);
      }
    }
    if (l.isVisible()) {
      l.revalidate();
    }
  }

  @Override
  public BCalquePersistenceInterface getPersistenceMng() {
    return new TrSiProfilLayerPersistence();
  }

  public void buildPlanLayer(final TrSiProfilModel.Zone _zone) {
    if (plans_ != null) { return; }
    final TrSiProfilModel.Zone zone = _zone == null ? new TrSiProfilModel.Zone() : _zone;
    plans_ = new TrSiPlanLayer(new ZModeleLigneBriseeDefault(zone) {

      @Override
      public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
        final BuTable r = new CtuluTable();
        r.setModel(new ZModeleLigneBriseeDefault.LignesTableModel(false, (GISZoneCollectionLigneBrisee) super
            .getGeomData()));
        EbliTableInfoPanel.setTitle(r, getTitle());
        return r;
      }
    });
    // EbliActionInterface[] act = TrSiProfilLayer.this.getActions();
    final List actList = new ArrayList();
    actList.add(editor_.getVisuAction());
    actList.add(editor_.getActionDelete());
    actList.add(editor_.getActionCancel());
    actList.add(null);
    actList.add(getComputeAction());
    actList.add(null);
    actList.add(export_);
    if (import_ != null) {
      actList.add(import_);
    }

    final EbliActionInterface[] act = new EbliActionInterface[actList.size()];
    actList.toArray(act);
    plans_.setActions(act);
    final BCalque parentCq = (BCalque) getParent();
    parentCq.enPremier(plans_);
    // le calque des fleches est mis en premier
    final BCalque fl = parentCq.getCalqueParNom("cqSiFleche");
    if (fl != null) {
      parentCq.enPremier(fl);
    }
    editor_.getPanel().getArbreCalqueModel().setSelectionCalque(plans_);
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
      final GrBoite _clipReel) {
    super.paintDonnees(_g, _versEcran, _versReel, _clipReel);
  }

  @Override
  protected void initTrace(final TraceIconModel _icon, final int _idxPoly) {
    if (_icon == null) { return; }
    if (((TrSiProfilModel) modele_).isSpecLigne(_idxPoly)) {
      _icon.updateData(super.iconModel_);
    } else {
      _icon.updateData(super.iconeModelOuvert_);
    }
    if (isAttenue()) {
      _icon.setCouleur(EbliLib.getAlphaColor(attenueCouleur(_icon.getCouleur()), alpha_));
    } else if (EbliLib.isAlphaChanged(alpha_)) {
      _icon.setCouleur(EbliLib.getAlphaColor(_icon.getCouleur(), alpha_));

    }
  }

  @Override
  protected void initTrace(final TraceLigneModel _ligne, final int _idxPoly) {
    if (_ligne == null) { return; }
    if (((TrSiProfilModel) modele_).isSpecLigne(_idxPoly)) {
      _ligne.updateData(super.ligneModel_);
    } else {
      _ligne.updateData(super.ligneModelOuvert_);
    }
    if (isAttenue()) {
      _ligne.setCouleur(EbliLib.getAlphaColor(attenueCouleur(_ligne.getCouleur()), alpha_));
    } else if (EbliLib.isAlphaChanged(alpha_)) {
      _ligne.setCouleur(EbliLib.getAlphaColor(_ligne.getCouleur(), alpha_));

    }

  }

  /**
   * @param _o l'objet a ajouter
   * @param _deforme la forme
   * @param _profile true si ligne represente le profil
   * @param _cmd le receveur de commande
   * @param _ui l'ui
   * @param _d les donn�es associ�es
   * @return true si ok
   */
  public boolean addForme(final GrObjet _o, final int _deforme, final boolean _profile,
      final CtuluCommandContainer _cmd, final CtuluUI _ui, final ZEditionAttributesDataI _d) {
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    if (FuLog.isTrace()) {
      FuLog.trace("ajout si poly avec donnees= " + (_d != null));
    }
    final boolean r = super.addForme(_o, _deforme, cmp, _ui, _d);
    if (r && _profile) {
      final TrSiProfilModel model = (TrSiProfilModel) modele_;
      model.setProfil(model.getNombre() - 1, cmp, true);
    }
    if (r && _cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return r;
  }

  @Override
  public boolean addForme(final GrObjet _o, final int _deforme, final CtuluCommandContainer _cmd, final CtuluUI _ui,
      final ZEditionAttributesDataI _data) {
    return TrSiProfilLayer.this.addForme(_o, _deforme, false, _cmd, _ui, _data);
  }

  /**
   * @return l'action a utiliser pour calculer les hauteurs en fonction des plans d'eau
   */
  public final EbliActionInterface getComputeAction() {
    if (computeAction_ == null) {
      computeAction_ = new ComputeAction();
    }
    return computeAction_;
  }

  @Override
  public String getSetTitle(final int _idx) {
    return TrResource.getS(_idx == 0 ? "Profil en long" : "Berges");
  }

  /**
   * @return le modele du profil.
   */
  public TrSiProfilModel getProfilModel() {
    return (TrSiProfilModel) modele_;
  }

  @Override
  public void setName(final String _name) {
    FuLog.error(new Throwable("no allowed"));
  }

  @Override
  public void siProfilDataChanged() {
    editor_.getPanel().getArbreCalqueModel().fireObservableChanged();
  }

  @Override
  public void siProfilGeometryChanged() {
    isLevelUpdateToDate_ = false;
    repaint();
    editor_.getPanel().getArbreCalqueModel().fireObservableChanged();
    if (plans_ != null) {
      ((TrSiProfilModel.Zone) plans_.getGeomData()).setEditable(true);
      ((TrSiProfilModel.Zone) plans_.getGeomData()).clear();
      plans_.repaint();
    }

  }

  public boolean isLevelUpdateToDate() {
    return isLevelUpdateToDate_;
  }

  public TrSiPlanLayer getPlans() {
    return plans_;
  }

  public void setLevelUptodate(final boolean _isLevelUpdateToDate) {
    isLevelUpdateToDate_ = _isLevelUpdateToDate;
  }
  
  @Override
  public boolean canAddForme(int _typeForme) {
    return _typeForme==DeForme.LIGNE_BRISEE;
  }
}
