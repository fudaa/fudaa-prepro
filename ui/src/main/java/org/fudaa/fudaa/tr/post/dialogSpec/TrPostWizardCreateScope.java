package org.fudaa.fudaa.tr.post.dialogSpec;

import com.memoire.bu.BuResource;
import com.memoire.bu.BuWizardDialog;
import java.awt.event.ActionEvent;
import java.io.File;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.fudaa.commun.courbe.FudaaGrapheTimeAnimatedVisuPanel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrCourbeImporter;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCourbeTreeModel;
import org.fudaa.fudaa.tr.post.TrPostLayoutFille;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostScopCourbeTreeModel;

/**
 * Wizard de cr�ation de widget graphe au format scope. Differents de sa classe mere qui ne propose que l'ajout des
 * courbes pour une target sp�cifi�e.
 * 
 * @author Adrien Hadoux
 */
public class TrPostWizardCreateScope extends TrPostWizardImportScope {

  TrPostCourbeTreeModel modelTarget_;
  private TrPostLayoutFille inLayout;

  @SuppressWarnings("serial")
  public static class ImportAction extends EbliActionSimple {
    final FudaaCommonImplementation ui_;
    TrPostProjet projet_;
    TrCourbeImporter.Target target_;
    private TrPostLayoutFille inLayout;

    public ImportAction(final TrPostProjet projet) {
      super(TrResource.getS("Importer fichier scope dans un nouveau graphe"), BuResource.BU.getIcon("IMPORTER"),
          "IMPORTER");
      ui_ = projet.impl_;
      projet_ = projet;

    }

    @Override
    public void actionPerformed(final ActionEvent _e) {

      // --Cr�ation d'un nouveau target
      final TrPostScopCourbeTreeModel model = new TrPostScopCourbeTreeModel();

      final TrPostWizardCreateScope wizard = new TrPostWizardCreateScope(projet_, model);
      wizard.setInLayout(inLayout);
      final BuWizardDialog DialogWizard = new BuWizardDialog(ui_.getFrame(), wizard);
      // --affichage du wizard --//
      DialogWizard.setSize(600, 600);
      DialogWizard.setLocationRelativeTo(ui_.getFrame());
      DialogWizard.setVisible(true);
    }

    /**
     * @return the inLayout
     */
    public TrPostLayoutFille getInLayout() {
      return inLayout;
    }

    /**
     * @param inLayout the inLayout to set
     */
    public void setInLayout(TrPostLayoutFille inLayout) {
      this.inLayout = inLayout;
    }
  }

  public TrPostWizardCreateScope(final TrPostProjet projet_, final TrPostCourbeTreeModel target) {
    super(projet_, target);
    modelTarget_ = target;
  }

  /**
   *Constructeur utilis� pour le pr�chargement avec un fichier d�ja connu
   */
  public TrPostWizardCreateScope(final TrPostProjet projet_, final TrPostCourbeTreeModel target, File fileTocharge) {
    super(projet_, target, false, fileTocharge);
    modelTarget_ = target;
  }

  /**
   * Creation du graphe widget
   */
  @Override
  public void createGraphImported() {
    final EGGraphe graphe = new EGGraphe(modelTarget_);
    projet_.addGrapheNodeInCurrentScene(new FudaaGrapheTimeAnimatedVisuPanel(graphe), data_.getTitleFormFile(), inLayout);
  }

  /**
   * @return the inLayout
   */
  public TrPostLayoutFille getInLayout() {
    return inLayout;
  }

  /**
   * @param inLayout the inLayout to set
   */
  public void setInLayout(TrPostLayoutFille inLayout) {
    this.inLayout = inLayout;
  }

}
