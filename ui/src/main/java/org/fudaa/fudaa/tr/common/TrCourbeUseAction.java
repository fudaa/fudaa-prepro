package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.commun.EbliActionSimple;

/**
 * @author fred deniger
 * @version $Id: TrCourbeUseAction.java,v 1.5 2007-05-04 14:01:46 deniger Exp $
 */
public final class TrCourbeUseAction extends EbliActionSimple {
  /**
   * @author fred deniger
   * @version $Id: TrCourbeUseAction.java,v 1.5 2007-05-04 14:01:46 deniger Exp $
   */
  static final class CuDialog extends CtuluDialog {
    /**
     * @param _parent
     * @param _dial
     */
    CuDialog(final Frame _parent, final CtuluDialogPanel _dial) {
      super(_parent, _dial);
    }

    @Override
    protected JButton construireOk() {
      final String string = BuResource.BU.getString("Femer");
      final JButton btOk = construireBuButton(string, "OK");
      btOk.setIcon(BuResource.BU.getIcon("fermer"));
      btOk.setMnemonic(string.charAt(0));
      return btOk;
    }
  }

  private final TrCourbeUseFinder finder_;

  public TrCourbeUseAction(final TrCourbeUseFinder _finder) {
    super(TrLib.getString("Utilisation des courbes"), TrResource.TR.getToolIcon("find-curve"), "USED"); //Use of the plots
    finder_ = _finder;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final TrCourbeUseFinder finder = finder_;
    if (finder == null) { return; }
    final TrCourbeUseResultsI res = finder.findAll(null);
    if (res == null) { return; }
    final JTable tb = res.getTableOfUse(finder.getSelectedEvolutions());
    if (tb == null) { return; }
    tb.setColumnSelectionAllowed(false);
    tb.setRowSelectionAllowed(true);
    final JButton bt = new JButton();
    bt.setEnabled(false);
    bt.setText(TrLib.getString("Sélectionner et afficher les supports")); //To select and display the supports
    bt.setIcon(BuResource.BU.getIcon("voir"));
    bt.setIconTextGap(4);
    bt.setEnabled(tb.getSelectedRowCount() == 1);
    tb.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(final ListSelectionEvent _evt) {
        bt.setEnabled(tb.getSelectedRowCount() == 1);
      }

    });
    bt.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _evt) {
        final int idx = tb.getSelectedRow();
        res.selectInView((EvolutionReguliereInterface) tb.getModel().getValueAt(idx, 0));

      }

    });
    final CtuluDialogPanel pn = new CtuluDialogPanel(false);
    pn.setLayout(new BuBorderLayout(0, 5, true, true));
    final BuScrollPane buScrollPane = new BuScrollPane(tb);
    pn.add(buScrollPane, BuBorderLayout.CENTER);
    buScrollPane.setPreferredHeight(90);
    pn.add(bt, BuBorderLayout.SOUTH);
    final Frame frame = finder.getImpl().getFrame();
    final CtuluDialog dialog = new CuDialog(frame, pn);
    dialog.setOption(CtuluDialog.OK_OPTION);
    dialog.setTitle(getTitle());
    dialog.pack();
    final Dimension d = dialog.getPreferredSize();
    dialog.afficheDialog(new Point(frame.getX(), frame.getY()), d, true);
  }
}