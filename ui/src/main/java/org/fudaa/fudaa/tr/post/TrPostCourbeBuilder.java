/*
 * @creation 1 sept. 06
 *
 * @modification $Date: 2007-05-04 14:01:51 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuWizardDialog;
import com.memoire.fu.FuLog;
import org.apache.commons.lang.ArrayUtils;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.fudaa.meshviewer.export.MvExportChooseVarAndTime;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.actions.TrPostCourbeAddPointsAction;
import org.fudaa.fudaa.tr.post.actions.TrPostCourbeAddVariableAction;
import org.fudaa.fudaa.tr.post.actions.TrPostCourbeRemoveVariableAction;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostWizardCourbeTemporelle;

import javax.swing.*;
import java.io.IOException;

/**
 * @author fred deniger
 * @version $Id: TrPostCourbeBuilder.java,v 1.9 2007-05-04 14:01:51 deniger Exp $
 */
public final class TrPostCourbeBuilder {
  private TrPostCourbeBuilder() {
  }

  public static TrPostCourbeTreeModel build(final TrPostProjet _proj, final TrPostSource _src, final H2dVariableType[] _vars,
                                            final int[] _idxPtOrMeshesArray, final ProgressionInterface _prog) {
    if (_prog != null) {
      _prog.setDesc(TrResource.getS("Construction des courbes"));
    }
    final TrPostCourbeTreeModel treeModel = new TrPostCourbeTreeModel(_proj);
    TrPostCourbeTreeModelBuilder builder = new TrPostCourbeTreeModelBuilder(treeModel, _idxPtOrMeshesArray, _vars);
    builder.addAllCourbes(_src, _prog, null);

    return treeModel;
  }

  public static EvolutionReguliere getEvolOnMesh(final TrPostSource _src, final int _idxOfMesh, final H2dVariableType _var, double minTime, double maxTime) {
    if (!_src.isDefined(_var)) {
      return null;
    }
    double[] times = _src.getTime().getTimeListModel().getTimesInSec();
    if (times == null) {
      return null;
    }
    int minIdx = 0;
    int maxIdx = times.length - 1;
    for (int i = 0; i <= maxIdx; i++) {
      if (times[i] <= minTime) {
        minIdx = i;
      } else {
        break;
      }
    }
    for (int i = maxIdx; i > minIdx; i--) {
      if (times[i] >= maxTime) {
        maxIdx = i;
      } else {
        break;
      }
    }
    times = ArrayUtils.subarray(times, minIdx, maxIdx + 1);
    final double[] values = new double[times.length];
    //if the values are not defined on meshes, we built a data defined on meshes:
    if (!_src.isElementVar(_var)) {
      for (int i = values.length - 1; i >= 0; i--) {
        EfData dataOnNode = _src.getData(_var, i);
        EfDataElement dataOnMesh = EfLib.getElementDataDanger(dataOnNode, _src.getGrid());
        values[i] = dataOnMesh.getValue(_idxOfMesh);
      }
    } else {
      try {
        for (int i = values.length - 1; i >= 0; i--) {
          values[i] = _src.getData(_var, i, _idxOfMesh);
        }
      } catch (final IOException _evt) {
        FuLog.error(_evt);
      }
    }
    return new EvolutionReguliere(times, values, true);
  }

  /**
   * Permet de creer un courbe avec plusieurs point interpoles.
   *
   * @param projet
   * @param _src
   * @param _vars
   * @param _idxPtArray
   * @param _prog
   */
  public static TrPostCourbeTreeModel build(final TrPostProjet projet, final TrPostSource _src, final H2dVariableType[] _vars, final TrPostInterpolatePoint[] _idxPtArray,
                                            final ProgressionInterface _prog) {
    if (_prog != null) {
      _prog.setDesc(TrResource.getS("Construction des courbes"));
    }
    final TrPostCourbeTreeModel treeModel = new TrPostCourbeTreeModel(projet);
    TrPostCourbeTreeModelBuilder builder = new TrPostCourbeTreeModelBuilder(treeModel, _idxPtArray, _vars);
    builder.addAllCourbes(_src, _prog, null);
    return treeModel;
  }

  public static void chooseAndBuild(final TrPostVisuPanel vue, final ZCalqueAffichageDonnees activeLayer, final int[] _ptIdx,
                                    final H2dVariableType _varSelected) {
    chooseAndBuild(vue, activeLayer, _ptIdx, null, _varSelected);
  }

  /**
   * Methode principale de construction d'une courbe avec l'aide d'un wizard
   */
  public static void chooseAndBuild(final TrPostVisuPanel vue, final ZCalqueAffichageDonnees activeLayer, final int[] _ptIdx,
                                    final TrPostInterpolatePoint _pt, final H2dVariableType _varSelected) {
    TrPostSource source = null;
    if (activeLayer instanceof TrIsoLayerDefault) {
      TrIsoLayerDefault trIsoLayerDefault = (TrIsoLayerDefault) activeLayer;
      source = trIsoLayerDefault.getSource();
    } else if (activeLayer instanceof TrPostFlecheLayer) {
      TrPostFlecheLayer trPostFlecheLayer = (TrPostFlecheLayer) activeLayer;
      source = trPostFlecheLayer.getSource();
    }
    if (source == null) {
      return;
    }
    DefaultListModel listModel = new DefaultListModel();
    H2dVariableType[] availableVar = source.getAvailableVar();
    final boolean isElement = source.isElementVar(_varSelected);
    for (H2dVariableType h2dVariableType : availableVar) {
      if (source.isElementVar(h2dVariableType) == isElement) {
        listModel.addElement(h2dVariableType);
      }
    }

    final MvExportChooseVarAndTime chooser = new MvExportChooseVarAndTime(listModel, null,
        TrResource.getS("Choisir les variables � afficher dans les �volutions temporelles"));
    chooser.setVarSelected(_varSelected);

    // -- wizard construction --//
    final TrPostWizardCourbeTemporelle wizard = new TrPostWizardCourbeTemporelle(vue, source, _varSelected, chooser, _ptIdx, _pt);

    final BuWizardDialog DialogWizard = new BuWizardDialog(vue.getImpl().getFrame(), wizard);

    // --affichage du wizard --//
    DialogWizard.setSize(600, 500);
    DialogWizard.setLocationRelativeTo(vue.getPostImpl().getCurrentLayoutFille());
    DialogWizard.setVisible(true);
  }

  public static void chooseAndBuild(final TrPostVisuPanel vue, final ZCalqueAffichageDonnees activeLayer, final TrPostInterpolatePoint _pt,
                                    final H2dVariableType _varSelected) {
    chooseAndBuild(vue, activeLayer, null, _pt, _varSelected);
  }

  public static EbliActionInterface[] getSpecActions(final EGGraphe _g, final TrPostCommonImplementation _impl, final TrPostVisuPanel _vue2d) {
    return new EbliActionInterface[]{new TrPostCourbeAddVariableAction(_impl, _g), new TrPostCourbeRemoveVariableAction(_impl, _g),
        new TrPostCourbeAddPointsAction(_impl, _g, _vue2d)};
  }
}
