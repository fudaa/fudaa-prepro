/**
 * @creation 13 nov. 2003
 * @modification $Date: 2007-06-14 12:01:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.reflux;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.h2d.reflux.H2dRefluxElementPropertyMngAbstract;
import org.fudaa.dodico.h2d.reflux.H2dRefluxProjectDispatcherListener;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrProjectDispatcherListener;

/**
 * @author deniger
 * @version $Id: TrRefluxProjectDispatcherListener.java,v 1.34 2007-06-14 12:01:39 deniger Exp $
 */
public class TrRefluxProjectDispatcherListener extends TrProjectDispatcherListener implements
    H2dRefluxProjectDispatcherListener {

  public TrRefluxProjectDispatcherListener() {}

  @Override
  public void nodalPropertyChanged(final H2dVariableType _t, final boolean _addedOrRemove) {
    setParamsChanged();
    fireGridDataChanged(_t);
    if (_addedOrRemove && proj_.getVisuFille() != null) {
      proj_.getVisuFille().updateIsoVariables();
      proj_.getVisuFille().updateInfoComponentAndIso();
    }
  }

  @Override
  public void dicoParamsVersionChanged(final DicoParams _cas) {}

  @Override
  public void siAdded(final H2dVariableType _var) {
    setParamsChanged();
    fireGridDataChanged(_var);

  }
  
  

  @Override
  public void nodeXYChanged() {
    setParamsChanged();
    fireGridDataChanged();
    
  }

  @Override
  public void siChanged(final H2dVariableType _var) {
    setParamsChanged();
    fireGridDataChanged(_var);
  }

  @Override
  public void siRemoved(final H2dVariableType _var) {
    setParamsChanged();
    fireGridDataChanged(_var);

  }

  /**
   * Methode appelee lorsque les infos sur le document sont modifiees.
   */
  public void majDocumentInfo() {
    CtuluLibMessage.info("CHANGE: DOCUMENT INFORMATION");
    setParamsChanged();
  }

  @Override
  public void bcPointsNormalChanged() {
    CtuluLibMessage.info("CHANGE: NORMAL FOR BOUNDARY POINTS");
  }

  @Override
  public void elementPropertyChanged(final H2dRefluxElementPropertyMngAbstract _source, final H2dVariableType _v) {
    CtuluLibMessage.info("CHANGE: ELEMENT PROPERTIES FOR " + _v.getName());
    fireGridDataChanged(_v);
    setParamsChanged();
  }

  @Override
  public void timeStepChanged() {
    if (proj_ == null) { return; }
    CtuluLibMessage.info("CHANGE: TIME STEPS");
    setParamsChanged();
  }

  @Override
  public void projectTypeChanged() {
    if (proj_ == null) { return; }
    CtuluLibMessage.info("CHANGE: PROJECT TYPE");
    setParamsChanged();
  }

}