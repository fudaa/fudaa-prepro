/**
 * @creation 10 janv. 2005
 * @modification $Date: 2007-06-11 13:08:22 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuLib;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import gnu.trove.TObjectIntHashMap;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.interpolation.InterpolationParameters;
import org.fudaa.ctulu.interpolation.InterpolationResultsHolderI;
import org.fudaa.ctulu.interpolation.bilinear.InterpolatorBilinear;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.dico.DicoDataType;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.interpolation.EfInterpolationGridSupportAdapter;
import org.fudaa.dodico.ef.interpolation.EfInterpolationTargetGridAdapter;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinInterface;
import org.fudaa.dodico.h2d.telemac.H2dTelemacParameters;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.telemac.io.TelemacVariableMapper;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacSerafinHelper.java,v 1.16 2007-06-11 13:08:22 deniger Exp $
 */
public class TrTelemacSerafinHelper extends TelemacVariableMapper {

  private final TrTelemacCommunParametres params_;

  /**
   * @param _params
   */
  public TrTelemacSerafinHelper(final TrTelemacCommunParametres _params) {
    super();
    params_ = _params;

  }

  FudaaCommonImplementation getUI() {
    return params_.getImpl();
  }

  public SerafinInterface readRepriseFile(final ProgressionInterface _prg, final File _file) {
    if (Fu.DEBUG && FuLog.isDebug() && _file != null) {
      FuLog.debug("read si file " + _file.getAbsolutePath());
    }
    getUI().setMainMessage(TrResource.getS("Lecture fichier reprise"));
    if (_file == null || !_file.exists() || _file.length() == 0) {
      getUI().error(getRepriseTitle(), TrResource.getS("Le fichier de reprise de calcul n'a pas �t� trouv�"));
      return null;
    }
    final CtuluIOOperationSynthese op = ((SerafinFileFormat) params_.getTelemacParametres().getTelemacVersion()
            .getDefaultSerafinFormat()).readLast(_file, _prg);
    final SerafinInterface serafinResults = (SerafinInterface) op.getSource();
    if (getUI().manageErrorOperationAndIsFatal(op)) {
      return null;
    }
    // le maillage n'est pas conforme
    if (serafinResults != null) {
      final String diff = EfLib.isDiffQuick(serafinResults.getGrid(), params_.getMaillage());
      if (diff != null) {
        getUI().error(getRepriseTitle(),
                TrResource.getS("Le fichier de reprise n'est pas compatible") + CtuluLibString.LINE_SEP + diff, false);
        return null;
      }
    }
    return serafinResults;
  }

  public void unsetMessage() {
    getUI().unsetMainMessage();
    getUI().unsetMainProgression();
  }

  /**
   * @author fred deniger
   * @version $Id: TrTelemacSerafinHelper.java,v 1.16 2007-06-11 13:08:22 deniger Exp $
   */
  public static class RepriseResults {

    boolean isModified_;
    double timeStep_;
    long initialDate;
  }

  /**
   * @param _prg la progression
   * @param _ui l'interface
   * @return si le fichier a ete simplifie
   */
  public RepriseResults loadRepriseData(final ProgressionInterface _prg, final File _file, final Map _m) {
    if (_m == null) {
      throw new IllegalArgumentException("map must not be null");
    }
    final SerafinInterface serafinResults = readRepriseFile(_prg, _file);
    // le maillage n'a pas pu etre lu ou le maillage ne correspond pas
    if (serafinResults == null) {
      unsetMessage();
      return null;
    }
    getUI().setMainProgression(50);
    // on prend le dernier pas de temps
    final int timeStep = serafinResults.getTimeStepNb() - 1;
    final Map t = buildVariables(serafinResults, timeStep, _m);
    // il n'y a pas de variables
    if (t == null || t.size() == 0) {
      unsetMessage();
      return null;
    }
    boolean isLastTimeKept = false;
    boolean isVarRemoved = false;
    if (serafinResults.getTimeStepNb() > 1) {
      isLastTimeKept = true;
      // if (FuLog.isTrace()) FuLog.trace("FTR : several time steps");
    }
    // determineBathyForCI(t, messageBody);
    // les variables non support�es
    final H2dVariableType[] varsToRemove = new H2dVariableType[]{H2dVariableType.VITESSE, H2dVariableType.DEBIT,
      H2dVariableType.DEBIT_NORMAL, H2dVariableType.DEBIT_TANGENTIEL, H2dVariableType.DEBIT_X,
      H2dVariableType.DEBIT_Y};
    for (int i = varsToRemove.length - 1; i >= 0; i--) {
      if (t.containsKey(varsToRemove[i])) {
        isVarRemoved = true;
        t.remove(varsToRemove[i]);
      }
    }
    String msg = null;

    final RepriseResults r = new RepriseResults();
    r.initialDate = serafinResults.getIdate();
    r.timeStep_ = serafinResults.getTimeStep(timeStep);
    // simplification du fichier de reprise
    if (isVarRemoved || isLastTimeKept) {
      r.isModified_ = true;
      msg = TrResource.getS("Une version simplifi�e du fichier de reprise a �t� charg�e:");
      if (isLastTimeKept) {
        msg += CtuluLibString.LINE_SEP + " -" + TrResource.getS("seul le dernier pas de temps a �t� gard�");
      }
      if (isVarRemoved) {
        msg += CtuluLibString.LINE_SEP + " -"
                + TrResource.getS("les variables vitesses scalaires, d�bit ont �t� supprim�es");
      }

      msg += CtuluLibString.LINE_SEP
              + TrResource.getS("Si vous modifiez et sauvegardez les conditions initiales, "
                      + "ce nouveau fichier de reprise remplacera l'ancien.");
    }
    // on charge les donn�es
    // si.load(t, si.getRepriseFile(_dirBase), serafinResults.getTimeStep(timeStep));
    if (msg != null) {
      final String message = msg;
      BuLib.invokeNow(new Runnable() {
        @Override
        public void run() {
          getUI().warn(TrResource.getS("Simplification du fichier de reprise"), message, false);
        }

      });
    }
    unsetMessage();

    return r;
  }

  public boolean isLoaded(final DicoEntite _ent) {
    return params_.getState().isLoaded(_ent);
  }

  public File getFileSet(final DicoEntite _ent) {
    return params_.getFileSet(_ent);
  }

  public File getGridFileSet() {
    return getFileSet(getGridKw());
  }

  public H2dTelemacParameters getH2DParams() {
    return params_.getTelemacParametres();
  }

  public boolean isRepriseAvailable() {
    return getRepriseBooleanKw() != null;
  }

  public void setGridLoaded(final boolean _modified) {
    params_.getState().setLoaded(getGridKw(), getFileSet(getGridKw()), _modified);
  }

  public String getGridName() {
    final File f = getFileSet(getGridKw());
    return f == null ? MvResource.getS("Fichier de maillage") : f.getName();
  }

  public static String getNewRepriseName() {
    return "condInit.ser";
  }

  public void setRepriseLoaded(final boolean _modified) {
    if (!params_.getDicoParams().isValueSetFor(getRepriseFileKw())) {
      params_.getDicoParams().setValue(getRepriseFileKw(), getNewRepriseName());
    }
    params_.getState().setLoaded(getRepriseFileKw(), getFileSet(getRepriseFileKw()), _modified);
    getRepriseFileKw().setRequired(true);
  }

  public boolean isRepriseFileSetAndValid() {
    return params_.getDicoParams().isValueSetFor(getRepriseFileKw())
            && params_.getDicoParams().isValueValideFor(getRepriseFileKw());
  }

  public boolean isRepriseActivated() {
    return getRepriseBooleanKw() != null && params_.getDicoParams().isValueSetFor(getRepriseBooleanKw())
            && DicoDataType.Binaire.getValue(params_.getDicoParams().getValue(getRepriseBooleanKw()));
  }

  /**
   * @param _varDouble
   * @param _msgs
   */
  public boolean determineBathyForCI(final Map _varDouble, final Map _varDoubleGeo, final List _msgs) {
    // on recup�re la bathy
    CtuluCollectionDouble bathy = (CtuluCollectionDouble) _varDouble.get(H2dVariableType.BATHYMETRIE);
    boolean modified = false;
    // s'il n'y pas de bathy dans le fichier de reprise
    if (bathy == null) {
      // Essai 1: on le recalcule
      final CtuluCollectionDouble cote = (CtuluCollectionDouble) _varDouble.get(H2dVariableType.COTE_EAU);
      final CtuluCollectionDouble he = (CtuluCollectionDouble) _varDouble.get(H2dVariableType.HAUTEUR_EAU);
      if (cote == null || he == null) {
        bathy = (CtuluCollectionDouble) _varDoubleGeo.get(H2dVariableType.BATHYMETRIE);
        if (bathy != null && _msgs != null) {
          _msgs.add(TrResource.getS("La bathym�trie a �t� r�cup�r�e � partir du fichier de g�om�trie"));
        }
        _varDouble.put(H2dVariableType.BATHYMETRIE, bathy);
        modified = true;
      } else {
        bathy = new CtuluArrayDouble(cote.getSize());
        for (int i = bathy.getSize() - 1; i >= 0; i--) {
          ((CtuluArrayDouble) bathy).set(i, cote.getValue(i) - he.getValue(i));
        }
        _varDouble.put(H2dVariableType.BATHYMETRIE, bathy);
        modified = true;
        // Essai 2: on le recupere de le fichier go
      }

    }
    return modified;
  }

  public boolean determineBathyAndHForCI(final Map _varDouble, final Map _varDoubleGeo) {
    // on recup�re la bathy
    final CtuluCollectionDouble bathy = (CtuluCollectionDouble) _varDouble.get(H2dVariableType.BATHYMETRIE);
    boolean modified = false;
    final List msgs = new ArrayList(5);
    // s'il n'y pas de bathy dans le fichier de reprise
    if (bathy == null) {
      modified = determineBathyForCI(_varDouble, _varDoubleGeo, msgs);
    }

    // on verifie que la cote soit presente ou puisse etre calculee
    // non presente-> on la calcul
    if (!_varDouble.containsKey(H2dVariableType.COTE_EAU)) {
      modified = true;
      // on peut calculer la cote
      if (bathy != null && _varDouble.containsKey(H2dVariableType.HAUTEUR_EAU)) {
        final CtuluCollectionDouble hauteur = (CtuluCollectionDouble) _varDouble.get(H2dVariableType.HAUTEUR_EAU);

        final double[] cote = new double[bathy.getSize()];
        // calcul de la hauteur a partir de la cote et de la bathy.
        for (int i = cote.length - 1; i >= 0; i--) {
          cote[i] = hauteur.getValue(i) + bathy.getValue(i);
        }
        // on ajoute la cote d'eau et on enleve la hauteur
        _varDouble.put(H2dVariableType.COTE_EAU, new CtuluArrayDouble(cote));
        _varDouble.remove(H2dVariableType.HAUTEUR_EAU);
        FuLog.trace("TSI: add water depth and remove water elevation");
      } else if (bathy == null) {
        msgs.add(TrResource.getS("La surface libre a �t� initialis�e � z�ro!"));
        _varDouble.put(H2dVariableType.COTE_EAU, new double[params_.getTelemacParametres().getMaillage().getPtsNb()]);
      } else {
        msgs
                .add(TrResource.getS("La surface libre ne peut pas �tre r�cup�r�e. Elle est initialis�e � partir du fond."));
        _varDouble.put(H2dVariableType.COTE_EAU, new CtuluArrayDouble(bathy));
        // on initialise avec cote=0;
      }

    }
    if (_varDouble.containsKey(H2dVariableType.HAUTEUR_EAU)) {
      FuLog.trace("TSI: remove water depth.");
      _varDouble.remove(H2dVariableType.HAUTEUR_EAU);
      modified = true;
    }
    if (msgs.size() > 0) {
      params_.getImpl().warn(getRepriseTitle(), CtuluLibString.arrayToString(msgs.toArray()));
    }
    return modified;
  }

  public static String getRepriseTitle() {
    return TrResource.getS("Reprise de calcul");
  }

  public static String getGridTitle() {
    return MvResource.getS("Fichier de maillage");
  }

  DicoEntite getGridKw() {
    return params_.getTelemacParametres().getTelemacVersion().getMaillageEntiteFile();
  }

  DicoEntite getRepriseBooleanKw() {
    return params_.getTelemacParametres().getTelemacVersion().getRepriseBooleanKeyword();
  }

  DicoEntite getRepriseFileKw() {
    return params_.getTelemacParametres().getTelemacVersion().getRepriseFileKeyword();
  }

  public SerafinInterface readGridFile(final ProgressionInterface _prg) {
    return readGridFile(getFileSet(getGridKw()), _prg);

  }

  public SerafinInterface readGridFile(final File _f, final ProgressionInterface _prg) {
    getUI().setMainMessage(MvResource.getS("Lecture maillage"));
    final File f = _f;
    if (f == null) {
      if (getParams().getDicoFileFormatVersion().isGridRequired()) {
        getUI().error(getGridTitle(), TrResource.getS("Le mot-cl� n'est pas renseign�"), false);
      }
      return null;
    }
    if (!f.exists()) {
      getUI().error(getGridTitle(), TrResource.getS("Le fichier n'existe pas"), false);
      return null;
    }
    final CtuluIOOperationSynthese op = params_.getTelemacParametres().getTelemacVersion().getDefaultSerafinFormat()
            .read(f, _prg);
    if (getUI().manageErrorOperationAndIsFatal(op)) {
      return null;
    }
    final SerafinInterface r = (SerafinInterface) op.getSource();
    if (r.getGrid().getEltType() == EfElementType.T6) {
      getUI().error(getGridTitle(), TrResource.getS("Les maillages 3D ne sont pas support�s"), false);
    }
    return r;
  }

  public int getTimeStepToUse(final SerafinInterface _serafinResults) {
    int timeStep = _serafinResults.getTimeStepNb() - 1;
    if (timeStep > 0) {
      final String[] str = new String[_serafinResults.getTimeStepNb()];
      for (int i = 0; i < str.length; i++) {
        str[i] = Double.toString(_serafinResults.getTimeStep(i));
      }
      timeStep = TrLib.chooseTimeStepToUse(str, getUI());
    }
    return timeStep;
  }

  public Map buildVariables(final SerafinInterface _serafin, final int _timeStep, final Map _mDest) {
    Map nodalVal = _mDest;
    if (nodalVal == null) {
      nodalVal = new HashMap();
    }
    final int nb = _serafin.getValueNb();
    for (int i = 0; i < nb; i++) {
      final String str = _serafin.getValueId(i);
      H2dVariableType knownVar = getUsedKnownVar(str);
      if (knownVar == null) {
        knownVar = H2dVariableType.createTempVar(str, null, _serafin.getUnite(i), null);
      }
      try {
        nodalVal.put(knownVar, new CtuluArrayDouble(_serafin.getReadingInfo().getDouble(i, _timeStep)));
      } catch (final IOException e) {
        getUI().error(e.getMessage());
      }
    }
    return nodalVal;

  }

  /**
   * @author fred deniger
   * @version $Id: TrTelemacSerafinHelper.java,v 1.16 2007-06-11 13:08:22 deniger Exp $
   */
  public static class GridInterpolAdapter extends EfInterpolationGridSupportAdapter.Node {

    final SerafinInterface ser_;

    double[] cache_;

    final int timeStep_;

    int oldVarIdx_ = -1;

    final TObjectIntHashMap nameIdx_;
    final CtuluPermanentList values_;

    /**
     * @param _grid
     */
    public GridInterpolAdapter(final SerafinInterface _grid, final int _timeStep) {
      super(_grid.getGrid());
      timeStep_ = _timeStep;
      ser_ = _grid;
      nameIdx_ = new TObjectIntHashMap(_grid.getValueNb());
      final String[] values = new String[_grid.getValueNb()];
      for (int i = 0; i < _grid.getValueNb(); i++) {
        values[i] = _grid.getValueId(i);
        nameIdx_.put(values[i], i);
      }
      values_ = new CtuluPermanentList(values);
    }

    public CtuluPermanentList getAllValues() {
      return values_;

    }

    @Override
    public int getNbValues() {
      return ser_.getValueNb();
    }

    public double getV(final Object _value, final int _ptIdx) {
      final int valueIdx = nameIdx_.get(_value);
      if (valueIdx == oldVarIdx_) {
        return cache_[_ptIdx];
      }
      oldVarIdx_ = valueIdx;
      try {
        cache_ = ser_.getReadingInfo().getDouble(valueIdx, timeStep_);
      } catch (final IOException _e) {
        FuLog.warning(_e);
        return 0;
      }
      return cache_[_ptIdx];
    }

  }

  public Map buildVariables(final SerafinInterface _serafin, final int _timeStep, final EfGridInterface _new,
          final ProgressionInterface _prog) {
    if (_new == null) {
      return buildVariables(_serafin, _timeStep, null);
    }
    final Map nodalVal = new HashMap();
    final int nb = _serafin.getValueNb();
    final GridInterpolAdapter src = new GridInterpolAdapter(_serafin, _timeStep);
    final CtuluPermanentList allValues = src.getAllValues();
    final InterpolationParameters params = new InterpolationParameters(allValues, new EfInterpolationTargetGridAdapter(
            _new, false), src);
    params.setProg(_prog);
    params.createDefaultResults();
    final InterpolationResultsHolderI res = params.getResults();
    final InterpolatorBilinear interpolateur = new InterpolatorBilinear(src);
    final CtuluAnalyze analyze = new CtuluAnalyze();
    if (_prog != null) {
      _prog.setDesc(DodicoLib.getS("Interpolation"));
    }
    interpolateur.interpolate(params);
    getUI().manageAnalyzeAndIsFatal(analyze);
    for (int i = 0; i < nb; i++) {
      final String str = _serafin.getValueId(i);
      H2dVariableType knownVar = getUsedKnownVar(str);
      if (knownVar == null) {
        knownVar = H2dVariableType.createTempVar(str, null, _serafin.getUnite(i), null);
      }
      nodalVal.put(knownVar, new CtuluArrayDouble(res.getValueForVariable((CtuluVariable) allValues.get(i))));
    }
    return nodalVal;

  }

  public TrTelemacCommunParametres getParams() {
    return params_;
  }
}
