/**
 * @creation 12 janv. 2005
 * @modification $Date: 2007-02-07 09:56:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuTable;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dVariableProviderContainerInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;
import org.fudaa.fudaa.meshviewer.model.MvInfoDelegate;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarElementModel.java,v 1.11 2007-02-07 09:56:20 deniger Exp $
 */
public class TrRubarElementModel extends MvElementModelDefault {
  final H2dVariableProviderContainerInterface container_;

  /**
   * @param _g
   * @param _d
   */
  public TrRubarElementModel(final H2dVariableProviderContainerInterface _container, final EfGridInterface _g,
                             final MvInfoDelegate _d) {
    super(_g, _d);
    container_ = _container;
  }

  @Override
  public EfGridInterface getGrid() {
    return g_;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    if (delegate_ instanceof TrRubarInfoSenderDelegate) {
      return ((TrRubarInfoSenderDelegate) delegate_)
          .createElementTable();
    }
    return super.createValuesTable(_layer);
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    H2dRubarParameters parameters = null;
    if (delegate_ instanceof TrRubarInfoSenderDelegate) {
      parameters = ((TrRubarInfoSenderDelegate) delegate_).getRub();
    }

    if (container_ != null) {
      return new TrRubarElementModelExpression(container_, this, parameters);
    }
    return super.getExpressionContainer();
  }
}
