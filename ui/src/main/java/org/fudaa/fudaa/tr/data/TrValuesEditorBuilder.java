/*
 * @creation 20 avr. 2005
 *
 * @modification $Date: 2007-04-30 14:22:41 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuValueValidator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.collection.CtuluCollection;
import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.gui.*;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrLib;

import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: TrValuesEditorBuilder.java,v 1.9 2007-04-30 14:22:41 deniger Exp $
 */
public final class TrValuesEditorBuilder {
  private TrValuesEditorBuilder() {

  }

  public static CtuluValuesEditorPanel buildPanelFor(final H2dVariableProviderInterface _src, final int[] _idx, final Predicate variablePredicate,
                                                     final CtuluCommandContainer _cmd) {
    return new CtuluValuesEditorPanel(buildParameters(_src, _idx, variablePredicate), _cmd);
  }

  @SuppressWarnings("unchecked")
  public static CtuluValuesParameters buildParameters(final H2dVariableProviderInterface _src, final int[] _idx, final Predicate variablePredicate) {
    final CtuluValuesParametersDefault res = new CtuluValuesParametersDefault();
    res.setVariableProvider(new TrValuesVariablesProvider(_src));
    res.idx_ = _idx;
    if (variablePredicate == null) {
      res.names_ = _src.getVarToModify();
    } else {
      res.names_ = CollectionUtils.select(Arrays.asList(_src.getVarToModify()), variablePredicate).toArray(new H2dVariableType[0]);
    }
    res.editors_ = new CtuluValueEditorDouble[res.names_.length];
    Arrays.fill(res.editors_, CtuluValueEditorDefaults.DOUBLE_EDITOR);
    res.values_ = new CtuluCollection[res.names_.length];
    int variablesBlockLength = _src.getVariablesBlockLength();
    int idxBlock = 1;
    int idxTransport = 0;
    Class blockClass = null;
    if (variablesBlockLength >= res.names_.length) {
      variablesBlockLength = 0;
    }
    String prefix = TrLib.getString("Block") + " ";
    if (variablesBlockLength > 0) {
      blockClass = res.names_[res.names_.length - 1].getClass();
      if (H2dRubarSedimentVariableType.class == blockClass) {
        prefix = TrLib.getString("Couche") + " ";
      }
    }

    for (int i = 0; i < res.values_.length; i++) {
      final H2dVariableType variableType = (H2dVariableType) res.names_[i];
      res.values_[i] = _src.getModifiableModel(variableType);
      final BuValueValidator val = _src.getValidator(variableType);
      if (val != null) {
        res.editors_[i] = new CtuluValueEditorDouble();
        ((CtuluValueEditorDouble) res.editors_[i]).setVal(val);
        if (variablesBlockLength > 0) {
          if (blockClass.equals(variableType.getClass()) || blockClass.isInstance(variableType)) {
            if (idxTransport % variablesBlockLength == 0) {

              ((CtuluValueEditorDouble) res.editors_[i]).setSeparator(prefix + idxBlock);
              idxBlock++;
            }
            idxTransport++;
          }
        }
      }
      final CtuluNumberFormatI format = _src.getFormater(variableType);
      if (format != null) {
        ((CtuluValueEditorDouble) res.editors_[i]).setFormatter(format);
      }
    }
    return res;
  }

  public static CtuluValuesMultiEditorPanel buildMultiFor(final H2dVariableProviderInterface _src, final int[] _idx, final Predicate variablePredicate) {
    return new CtuluValuesMultiEditorPanel(buildParameters(_src, _idx, variablePredicate));
  }

  public static CtuluValuesCommonEditorPanel buildFor(final H2dVariableProviderInterface _src, final int[] _idx, final Predicate variablePredicate) {
    return new CtuluValuesCommonEditorPanel(buildParameters(_src, _idx, variablePredicate));
  }
}
