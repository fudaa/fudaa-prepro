/*
 * Bnp Paribas Services
 *
 * $Id$
 */
package org.fudaa.fudaa.tr.post;

import java.util.Set;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreatedExpr;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreatedSaver;
import org.fudaa.fudaa.tr.post.data.TrPostDataVecteur;
import org.nfunk.jep.Variable;

/**
 * @author denf01a @creation 27 janv. 2009
 * @version
 *
 */
public class TrPostFlecheContentExpr implements TrPostFlecheContent {

  H2dVariableType varName_;
  TrPostDataCreatedExpr vx_;
  TrPostDataCreatedExpr vy_;
  private TrPostDataVecteur lastValue_;
  boolean isElementData_;
  private int lastTime_;

  protected TrPostFlecheContentExpr(boolean _isElementData, H2dVariableType varName_, TrPostDataCreatedExpr vx_,
                                    TrPostDataCreatedExpr vy_) {
    super();
    this.varName_ = varName_;
    this.vx_ = vx_;
    this.vy_ = vy_;
    isElementData_ = _isElementData;
  }

  /**
   *
   */
  @Override
  public TrPostFlecheContent changeVar(H2dVariableType _name) {
    return new TrPostFlecheContentExpr(isElementData_, _name, vx_, vy_);
  }

  @Override
  public void clearCache() {
    vx_.clearCache();
    vy_.clearCache();
  }

  @Override
  public void restore() {
  }

  @Override
  public TrPostDataCreatedSaver createSaver() {
    return null;
  }

  @Override
  public TrPostFlecheContent duplicate(TrPostSource _src) {
    return new TrPostFlecheContentExpr(isElementData_, varName_, vx_.createCopy(_src), vy_.createCopy(_src));
  }

  @Override
  public TrPostFlecheContent duplicate(TrPostSource _src, H2dVariableType name) {
    return new TrPostFlecheContentExpr(isElementData_, name, vx_.createCopy(_src), vy_.createCopy(_src));
  }

  @Override
  public void fillWhithAllUsedVar(Set _res) {
    vx_.fillWhithAllUsedVar(_res);
    vy_.fillWhithAllUsedVar(_res);
  }

  /**
   *
   */
  @Override
  public EfData getDataFor(int time) {
    return getValues(time);
  }

  @Override
  public String getDescription() {
    return varName_.getName();
  }

  /**
   *
   */
  @Override
  public double getValue(int time, int object) {
    return Math.hypot(vx_.getValue(time, object), vy_.getValue(time, object));
  }

  /**
   *
   */
  @Override
  public TrPostDataVecteur getValues(int _tIdx) {
    if (_tIdx != lastTime_) {
      lastTime_ = _tIdx;
      lastValue_ = new TrPostDataVecteur(vx_.getDataFor(_tIdx), vy_.getDataFor(_tIdx), vx_.getSrc().getGrid());
    }
    return lastValue_;
  }

  /**
   *
   */
  @Override
  public H2dVariableType getVar() {
    return varName_;
  }

  /**
   *
   */
  @Override
  public H2dVariableType getVx() {
    return null;
  }

  /**
   *
   */
  @Override
  public String getVxDesc() {
    return getDescription() + " X";
  }

  /**
   *
   */
  @Override
  public H2dVariableType getVy() {
    return null;
  }

  /**
   *
   */
  @Override
  public String getVyDesc() {
    return getDescription() + " Y";
  }

  /**
   *
   */
  @Override
  public boolean isEditable() {
    return true;
  }

  /**
   *
   */
  @Override
  public boolean isExpr() {
    return true;
  }

  /**
   *
   */
  @Override
  public TrPostFlecheContent isFleche() {
    return this;
  }

  /**
   *
   */
  @Override
  public boolean updateConstantVar(Variable _var) {
    return vx_.updateConstantVar(_var) | vy_.updateConstantVar(_var);
  }
}
