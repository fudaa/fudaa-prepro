/**
 * @creation 16 juin 2004
 * @modification $Date: 2007-03-02 13:01:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextArea;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluCellBooleanEditor;
import org.fudaa.ctulu.gui.CtuluCellBooleanRenderer;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluListModelEmpty;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dRubarBcTypeList;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryFlowrateGroupType;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryTarageGroupType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFlowrateGroupEditorPanel.java,v 1.28 2007-03-02 13:01:34 deniger Exp $
 */
public final class TrRubarTarageGroupEditorPanel extends JPanel implements CtuluUndoRedoInterface {
  class BooleanEditor extends CtuluCellBooleanEditor {
    @Override
    public boolean getBooleanValue(final Object _o) {
      return tarageMng_.isTarageGroupDefined((H2dRubarBoundaryTarageGroupType) _o);
    }
  }

  private class EnabledCellRenderer extends CtuluCellBooleanRenderer {
    public EnabledCellRenderer() {
      setText(CtuluLibString.EMPTY_STRING);
      revalidate();
    }

    @Override
    public void setValue(final Object _value) {
      setSelected(tarageMng_.isTarageGroupDefined((H2dRubarBoundaryTarageGroupType) _value));
    }
  }

  private class EvolutionEditor extends DefaultCellEditor {
    JComboBox cb_;

    public EvolutionEditor() {
      super(new BuComboBox());
    }

    @Override
    public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _isSelected, final int _row,
                                                 final int _column) {
      cb_ = (JComboBox) editorComponent;
      final ComboBoxModel cb = getModel();
      cb_.setModel(CtuluListModelEmpty.EMPTY);
      cb_.setModel(cb);
      return super.getTableCellEditorComponent(_table, _value, _isSelected, _row, _column);
    }
  }

  class TarageGroupModel extends AbstractTableModel implements H2dRubarBcListener, H2dRubarTarageListener {
    @Override
    public void areteTypeChanged() {
    }

    @Override
    public void bathyChanged() {
    }

    @Override
    public void fondDurChanged() {
    }

    @Override
    public void nodeInGridChanged() {

    }

    @Override
    public void projectTypeChanged() {
    }

    @Override
    public void numberOfConcentrationChanged() {

    }

    @Override
    public void flowrateGroupChanged(final H2dRubarBoundaryFlowrateGroupType _t) {
    }

    @Override
    public void timeClChanged() {
    }

    @Override
    public int getColumnCount() {
      return 3;
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) {
        if (!(_value instanceof Boolean)) {
          return;
        }
        final boolean isActivated = ((Boolean) _value).booleanValue();
        ComboBoxModel m = getModel();
        final EvolutionReguliereAbstract tarage = (EvolutionReguliereAbstract) m.getElementAt(0);
        if (tarage != null) {
          if (isActivated) {
            addCmd(tarageMng_.setDefined(groupType_[_rowIndex], tarage));
          } else if (!bcMng_.isUsedBoundaryType(groupType_[_rowIndex])) {
            addCmd(tarageMng_.setUndefined(groupType_[_rowIndex]));
          }
        }
      } else if (_columnIndex == 2) {
        if (_value == null) {
          return;
        }
        EvolutionReguliereAbstract e = (EvolutionReguliereAbstract) _value;
        addCmd(tarageMng_.setEvolution(groupType_[_rowIndex], e));
      }
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return CtuluLibString.ESPACE;
      }
      if (_column == 1) {
        return TrResource.getS("Groupe");
      }
      return H2dResource.getS("Loi de tarage");
    }

    @Override
    public int getRowCount() {
      return groupType_.length;
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex <= 1) {
        return groupType_[_rowIndex];
      }
      final EvolutionReguliereAbstract t = tarageMng_.getGroupTimeCondition(groupType_[_rowIndex]);
      return t == null ? null : t.getNom();
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) {
        return !bcMng_.isUsedBoundaryType(groupType_[_rowIndex]);
      } else if (_columnIndex == 1) {
        return false;
      }
      return tarageMng_.isTarageGroupDefined(groupType_[_rowIndex]);
    }

    @Override
    public void tarageCourbeChanged(H2dRubarTarageMng _mng, EvolutionReguliereInterface _e) {
    }

    @Override
    public void tarageUsedChanged(H2dRubarTarageMng _mng) {
    }

    @Override
    public void TarageGroupeUsedChanged(H2dRubarTarageMng _mng, H2dRubarBoundaryTarageGroupType tarageGroupeType) {
      table_.clearSelection();
      final int row = table_.getSelectedRow();
      if (row >= 0) {
        table_.addRowSelectionInterval(row, row);
      }
      table_.addColumnSelectionInterval(1, 1);
      fireTableDataChanged();
    }
  }

  protected void addCmd(final CtuluCommand _m) {
    cmd_.addCmd(_m);
  }

  final CtuluCommandManager cmd_;
  H2dRubarBoundaryTarageGroupType[] groupType_;
  private final H2dRubarTarageMng tarageMng_;
  private final H2dRubarBcMng bcMng_;
  TrRubarTarageCourbesManager evolMng_;
  JTable table_;
  TrRubarTarageCourbesListModel comboboxModel;

  protected ComboBoxModel getModel() {
    if (comboboxModel == null) {
      comboboxModel = new TrRubarTarageCourbesListModel(evolMng_, false);
    }
    return comboboxModel;
  }

  TarageGroupModel tableModel_;

  /**
   * @param _para les parametres.
   */
  public TrRubarTarageGroupEditorPanel(final H2dRubarParameters _para, final TrRubarTarageCourbesManager _courbeMng) {
    super();
    cmd_ = new CtuluCommandManager();
    evolMng_ = _courbeMng;
    // evolMng_.addListener(this);
    tarageMng_ = _para.getTarageMng();
    bcMng_ = _para.getBcMng();
    groupType_ = H2dRubarBcTypeList.getTarageGroupType();
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    table_ = new JTable();
    setLayout(new BuBorderLayout(5, 5));
    tableModel_ = new TarageGroupModel();
    tarageMng_.addTarageListener(tableModel_);
    bcMng_.addListener(tableModel_);
    table_.setModel(tableModel_);
    final TableColumnModel c = table_.getColumnModel();
    c.getColumn(0).setCellRenderer(new EnabledCellRenderer());
    c.getColumn(0).setCellEditor(new BooleanEditor());
    c.getColumn(0).setMaxWidth(25);
    final CtuluCellTextRenderer r = new CtuluCellTextRenderer();
    final EvolutionEditor evolEditor = new EvolutionEditor();
    TableColumn col = c.getColumn(1);
    col.setCellRenderer(new NameCellRenderer());
    col = c.getColumn(2);
    col.setCellEditor(evolEditor);
    col.setCellRenderer(r);
    table_.revalidate();
    add(new BuScrollPane(table_));
    final BuTextArea txt = new BuTextArea(5, 80);
    txt.setEditable(false);
    txt.setLineWrap(true);
    txt.setWrapStyleWord(true);
    txt.setText(getHelpText());
    txt.setMargin(new Insets(3, 3, 3, 3));
    add(txt, BuBorderLayout.SOUTH);
  }

  /**
   * @return l'aide.
   */
  public String getHelpText() {
    if (CtuluLib.isFrenchLanguageSelected()) {
      return getFrHelpText();
    }
    return "The   groupes can be specified only if curves are available. You can import"
        + " curves thanks to the menu File>Import.\n\nIf the name of a group is displayed in blue,"
        + " it means that this group is used by an edge and can't be cleared";
  }

  /**
   * @return l'aide en francais.
   */
  public String getFrHelpText() {
    return "Les groupes sont modifiables seulement si des courbes sont disponibles. Vous pouvez "
        + "les importer gr�ce au menu Fichier>Import.\n\nSi un groupe est affich� en bleu, cela signifie "
        + "qu'il est utilis� par une ar�te et il ne peut pas �tre enlev�";
  }

  class NameCellRenderer extends CtuluCellTextRenderer {
    @Override
    protected void setValue(final Object _value) {
      if (bcMng_.isUsedBoundaryType((H2dRubarBoundaryTarageGroupType) _value)) {
        setForeground(Color.blue);
      } else {
        setForeground(Color.black);
      }
      setText(_value.toString());
    }
  }

  @Override
  public void clearCmd(final CtuluCommandManager _source) {
    if ((cmd_ != null) && (_source != cmd_)) {
      cmd_.clean();
    }
  }

  @Override
  public CtuluCommandManager getCmdMng() {
    return cmd_;
  }

  @Override
  public void setActive(final boolean _b) {
    if (table_.getCellEditor() != null) {
      table_.getCellEditor().stopCellEditing();
    }
  }
}
