/*
 * @creation 21 nov. 06
 * 
 * @modification $Date: 2007-05-04 13:59:49 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import com.memoire.bu.BuResource;
import org.locationtech.jts.geom.LineString;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ebli.animation.EbliAnimationAdapterInterface;
import org.fudaa.ebli.controle.BSelecteurListTimeTarget;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGCourbePersistBuilder;
import org.fudaa.ebli.courbe.EGGrapheDuplicator;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGParent;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;

/**
 * @author fred deniger
 * @version $Id: MvProfileCourbeTime.java,v 1.6 2007-05-04 13:59:49 deniger Exp $
 */
public class MvProfileCourbeTime extends MvProfileCourbe implements BSelecteurListTimeTarget, ListSelectionListener,
        ListDataListener, EbliAnimationAdapterInterface {

  final ListSelectionModel selection_ = new DefaultListSelectionModel();
  FudaaCourbeTimeListModel time_;

  public MvProfileCourbeTime(final EGGroup _m, final MVProfileCourbeModel _model,
          final FudaaCourbeTimeListModel _time) {
    super(_m, _model);
    time_ = _time;
    time_.addListDataListener(this);
    selection_.setSelectionInterval(_model.getTime(), _model.getTime());
    selection_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    selection_.addListSelectionListener(this);
  }

  @Override
  protected EGCourbePersistBuilder<MvProfileCourbeTime> createPersistBuilder() {
    return new MvProfileCourbeTimePersistBuilder();
  }

  public MvProfileCourbe duplicate() {
    final MvProfileCourbeTime res = new MvProfileCourbeTime((MvProfileCourbeGroup) getParent(), getM().duplicate(),
            time_);
    res.setAspectContour(getAspectContour());
    res.getM().setTitle(getTitle() + CtuluLibString.ESPACE + BuResource.BU.getString("Copie"));
    return res;
  }

  @Override
  public EGCourbe duplicate(EGParent newParent, EGGrapheDuplicator duplicator) {
	  //--  ahx - Resolve item 1.1.3 - newParent peut etre EGGGroup et non MvProfileCourbeGroup. --//
	MVProfileCourbeModel model =  getM().duplicate();
    final MvProfileCourbeTime res = new MvProfileCourbeTime( (EGGroup)newParent,model , time_);
    
   res.getYRange().setMax(((EGGroup)newParent).getYRange().max_);
   res.getYRange().setMin(((EGGroup)newParent).getYRange().min_);
    res.initGraphicConfigurationFrom(this);
    return res;
  }

  @Override
  public void contentsChanged(final ListDataEvent _e) {
    updateTitle();
  }

  @Override
  public int getNbTimeStep() {
    return time_.getSize();
  }

  @Override
  public String getTimeStep(final int _idx) {
    return time_.getElementAt(_idx).toString();
  }

  @Override
  public double getTimeStepValueSec(final int _idx) {
    return time_.getTimeInSec(_idx);
  }

  @Override
  public void setTimeStep(final int _idx) {
    if (_idx >= 0 && _idx < getNbTimeStep()) {
      selection_.setSelectionInterval(_idx, _idx);
    }

  }

  @Override
  public ListModel getTimeListModel() {
    return time_;
  }

  @Override
  public ListSelectionModel getTimeListSelectionModel() {
    return selection_;
  }

  @Override
  public void intervalAdded(final ListDataEvent _e) {
  }

  @Override
  public void intervalRemoved(final ListDataEvent _e) {
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    getM().setTime(selection_.getMaxSelectionIndex());
    updateTitle();
    fireCourbeAspectChanged(false);
  }

  protected String format(double d) {
    return CtuluNumberFormatDefault.DEFAULT_FMT.format(d);
  }

  public void updateTitle() {
    if (titleModifiedByUser) {
      return;
    }
    // -- titre --//
    CtuluVariable variable = getM().getVariable();
    String title = variable.getName() + CtuluLibString.ESPACE;
    if (variable.getCommonUnit() != null) {
      title += "(" + variable.getCommonUnit() + ")" + CtuluLibString.ESPACE;
    }
    title += time_.getElementAt(getM().getTime());

    LineString polyligne = getM().getInitLine();
    // -- on ajoute les extremites du segment choisi --//
    title += ", P1(" + format(polyligne.getCoordinateN(0).x) + ";" + format(polyligne.getCoordinateN(0).y) + ")" + ";"
            + "P" + polyligne.getNumPoints() + "(" + format(polyligne.getCoordinateN(polyligne.getNumPoints() - 1).x) + ";"
            + format(polyligne.getCoordinateN(polyligne.getNumPoints() - 1).y) + ")";
    titleModifiedByUser = false;
    super.setTitle(title);
  }
  protected boolean titleModifiedByUser;

  @Override
  public boolean setTitle(String newTitle) {
    boolean setTitle = super.setTitle(newTitle);
    if (setTitle) {
      titleModifiedByUser = true;
    }
    return setTitle;
  }

  @Override
  public boolean initTitle(final String _newTitle) {
    boolean setTitle = setTitle(_newTitle);
    titleModifiedByUser = false;
    return setTitle;
  }

  @Override
  public void setVar(final CtuluVariable _v, final boolean _contentChanged) {
    getM().setVariable(_v, _contentChanged);
    if (_v != getM().getVariable()) {
      updateTitle();
      fireCourbeContentChanged();
    }

  }
}
