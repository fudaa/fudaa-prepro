/*
 * @creation 17 juil. 06
 * 
 * @modification $Date: 2007-06-05 09:01:14 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.persist.TrPostSourceOneTimeStepReplayPersist;

/**
 * Source definie a partir d'une autre et sur un seul pas de temps.
 * 
 * @author fred deniger
 * @version $Id: TrPostSourceOneTimeStep.java,v 1.4 2007-06-05 09:01:14 deniger Exp $
 */
public class TrPostSourceOneTimeStep extends TrPostSourceAbstractFromIdx implements TrPostSourceBuilt {

  final int timeStep_;
  final TrPostSource init_;

  public TrPostSourceOneTimeStep(final TrPostSource _srcInit, final int _timeIdx, final TrPostCommonImplementation _impl) {
    super(_srcInit.getTitle() + CtuluLibString.ESPACE + _srcInit.getTime().getTimeListModel().getElementAt(_timeIdx),
        _srcInit.getGrid(), new double[] { _srcInit.getTimeStep(_timeIdx) }, _srcInit.getAllVariablesNonVec(), _impl);
    init_ = _srcInit;
    timeStep_ = _timeIdx;
  }

  @Override
  public TrPostSourceOneTimeStepReplayPersist getReplay() {
    return new TrPostSourceOneTimeStepReplayPersist(getId(), init_.getId(), timeStep_);
  }

  @Override
  public EfData getInitData(final int _varIdx, final int _timeIdx) {
    final H2dVariableType var = getVariableWithInitIdx(_varIdx);
    return init_.getData(var, timeStep_);
  }

  @Override
  public double getInitData(final int _varIdx, final int _timeIdx, final int _ptIdx) throws IOException {
    return getInitData(_varIdx, _timeIdx).getValue(_ptIdx);
  }

  public TrPostInspectorReader createWatcher(final TrPostProjet _proj, final boolean _auto) {
    return null;
  }

  @Override
  public boolean containsElementVar() {
    return init_.containsElementVar();
  }

  @Override
  public Collection<File> getFiles() {
    return init_.getFiles();
  }

}
