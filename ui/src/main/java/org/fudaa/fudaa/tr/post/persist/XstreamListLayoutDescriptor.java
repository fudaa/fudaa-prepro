package org.fudaa.fudaa.tr.post.persist;

import java.util.List;

/**
 * Classe plus parlante en nommage de balise
 * @author Adrien Hadoux
 *
 */

public class XstreamListLayoutDescriptor {

	//@XStreamAlias("NombreLayout")
	int nbFichiersToGenrate;
	
	//@XStreamImplicit(itemFieldName="FichierLayout")
	public List<String> listeFiles;

	public XstreamListLayoutDescriptor(int nbFichiersToGenrate,
			List<String> listeFiles) {
		super();
		this.nbFichiersToGenrate = nbFichiersToGenrate;
		this.listeFiles = listeFiles;
	}
	
	
}
