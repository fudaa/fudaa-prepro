/**
 * @creation 13 nov. 2003
 * @modification $Date: 2007-05-22 14:20:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.util.Observable;
import java.util.Observer;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.h2d.H2dBcFrontierInterface;
import org.fudaa.dodico.h2d.H2dBoundary;
import org.fudaa.dodico.h2d.H2dProjectDispatcherListener;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.fudaa.commun.FudaaProjectStateFrameListener;
import org.fudaa.fudaa.fdico.FDicoProjectState;

/**
 * @author deniger
 * @version $Id: TrProjectDispatcherListener.java,v 1.30 2007-05-22 14:20:38 deniger Exp $
 */
public abstract class TrProjectDispatcherListener implements H2dProjectDispatcherListener, Observer {

  static protected class GridDataObservable extends Observable {
    protected void fireChanged() {
      setChanged();
      super.notifyObservers();
    }

    protected void fireChanged(final Object _o) {
      setChanged();
      super.notifyObservers(_o);
    }

  }

  GridDataObservable gridDataObservable_;

  boolean isInitProjectChanged_;

  FDicoProjectState projectState_;

  protected TrProjet proj_;

  public TrProjectDispatcherListener() {
    super();
  }

  public void setParamsChanged() {
    if (projectState_ == null) {
      isInitProjectChanged_ = true;
    } else {
      projectState_.setParamsModified(true);
    }
  }

  public void addGridDataObserver(final Observer _obs) {
    if (gridDataObservable_ == null) {
      gridDataObservable_ = new GridDataObservable();
    }
    gridDataObservable_.addObserver(_obs);
  }

  @Override
  public void bcBoundaryTypeChanged(final H2dBoundary _b, final H2dBoundaryType _old) {
    CtuluLibMessage.info("CHANGE: BOUNDARY TYPE old=" + _old + " new = " + _b.getType());
    setParamsChanged();
  }

  @Override
  public void bcFrontierStructureChanged(final H2dBcFrontierInterface _b) {
    CtuluLibMessage.info("CHANGE: BOUNDARY LIQUID BORDER STRUCTURE");
    setParamsChanged();
  }

  @Override
  public void bcParametersChanged(final H2dBoundary _b, final H2dVariableType _t) {
    CtuluLibMessage.info("CHANGE: BOUNDARY PARAMETER "
        + (_b == null ? CtuluLibString.EMPTY_STRING : "( BOUNDARY " + _b + ')')
        + (_t == null ? CtuluLibString.EMPTY_STRING : "( PARAMETER " + _t.getName() + ')'));
    setParamsChanged();
  }

  @Override
  public void bcPointsParametersChanged(final H2dVariableType _t) {
    CtuluLibMessage.info("CHANGE: PARAMETERS FOR BOUNDARY POINTS"
        + (_t == null ? CtuluLibString.EMPTY_STRING : "( PARAMETER " + _t.getName() + ')'));
    setParamsChanged();
  }

  @Override
  public void dicoParamsEntiteAdded(final DicoParams _cas, final DicoEntite _ent) {
    setParamsChanged();
    CtuluLibMessage.info("CHANGE: KEYWORD ADD " + _ent.getNom() + " value " + _cas.getValue(_ent));
  }

  @Override
  public void dicoParamsEntiteCommentUpdated(final DicoParams _cas, final DicoEntite _ent) {
    setParamsChanged();
    CtuluLibMessage.info("CHANGE: KEYWORD COMMENT UPDATE " + _ent.getNom());
  }

  @Override
  public void dicoParamsEntiteRemoved(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    setParamsChanged();
    CtuluLibMessage.info("CHANGE: KEYWORD REMOVE " + _ent.getNom());
  }

  @Override
  public void dicoParamsEntiteUpdated(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    setParamsChanged();
    CtuluLibMessage.info("CHANGE: KEYWORD UPDATE " + _ent.getNom() + " value " + _cas.getValue(_ent) + " old= "
        + _oldValue);
  }

  public void dicoParamsStateLoadedEntiteChanged(final DicoParams _cas, final DicoEntite _ent) {}

  @Override
  public void dicoParamsValidStateEntiteUpdated(final DicoParams _cas, final DicoEntite _ent) {
    CtuluLibMessage.info("CHANGE: KEYWORD COMPORTMENT UDPATE " + _ent.getNom());
  }

  @Override
  public void evolutionChanged(final EvolutionReguliereInterface _e) {
    // si modification dans une courbe utilisee:
    if (_e.isUsed() && projectState_ != null) setParamsChanged();
    CtuluLibMessage.info("CHANGE: EVOLUTION " + _e.getNom());
  }

  @Override
  public void evolutionUsedChanged(final EvolutionReguliereInterface _e, final int _old, final int _new,
      final boolean _isAdjusting) {
    if ((_old > 0) || (_new > 0)) {
      final String s = "CHANGE: EVOLUTION " + _e.getNom() + " USED CHANGED ( old=" + _old + ", new= " + _new + " )";
      if (_isAdjusting && (CtuluLibMessage.DEBUG)) {
        CtuluLibMessage.debug(s);
      } else {
        CtuluLibMessage.info(s);
      }
    }
    setParamsChanged();
  }

  public void fireGridDataChanged() {
    if (gridDataObservable_ != null) {
      gridDataObservable_.fireChanged();
    }
  }

  public void fireGridDataChanged(final Object _o) {
    if (gridDataObservable_ != null) {
      gridDataObservable_.fireChanged(_o);
    }
  }

  public TrProjet getProj() {
    return proj_;
  }

  public FDicoProjectState getProjectState() {
    return projectState_;
  }

  public final boolean isProjetChanged() {
    return projectState_ != null && projectState_.isModified();
  }

  public void removeGridDataObserver(final Observer _obs) {
    if (gridDataObservable_ != null) {
      gridDataObservable_.deleteObserver(_obs);
    }
  }

  public void setProj(final TrProjet _proj) {
    proj_ = _proj;
    projectState_ = _proj.getState();
    if (isInitProjectChanged_ || proj_.getLastSaveDate() == null) {
      projectState_.setParamsModified(true);
    }
    new FudaaProjectStateFrameListener(proj_.getImpl().getFrame(), proj_).initWith(projectState_);
  }

  @Override
  public void update(final Observable _o, final Object _arg) {
    if ("versEcran".equals(_arg)) { return; }
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FTR: update from " + _o.getClass() + " arg " + (_arg == null ? "null" : _arg.getClass().toString()));
    }
    if (projectState_ != null) {
      projectState_.setUIModified(true);
    }

  }

}
