/**
 * @creation 4 janv. 2005
 * @modification $Date: 2007-04-16 16:35:33 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuTable;
import gnu.trove.TIntHashSet;
import java.awt.Color;
import java.util.Arrays;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridVolumeInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarLimniListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarLimniMng;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.calque.ZModelPointAbstract;
import org.fudaa.ebli.calque.find.CalqueFindPointExpression;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.tr.common.TrResource;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarLimniModel.java,v 1.20 2007-04-16 16:35:33 deniger Exp $
 */
public class TrRubarLimniModel extends ZModelPointAbstract {

  /**
   * @author Fred Deniger
   * @version $Id: TrRubarLimniModel.java,v 1.20 2007-04-16 16:35:33 deniger Exp $
   */
  public static class DefaultTableModel extends AbstractTableModel {

    final TrRubarLimniModel model_;

    /**
     * @param _model
     */
    public DefaultTableModel(final TrRubarLimniModel _model) {
      super();
      model_ = _model;
    }

    @Override
    public int getColumnCount() {
      return 4;
    }

    @Override
    public String getColumnName(final int _column) {

      if (_column == 0) { return EbliLib.getS("Indice"); }
      if (_column == 1) { return MvResource.getS("L'indice de l'�l�ment"); }
      if (_column == 2) { return "X"; }
      return "Y";
    }

    @Override
    public int getRowCount() {
      return model_.getNombre();
    }

    @Override
    public Class getColumnClass(final int _columnIndex) {
      return _columnIndex <= 1 ? Integer.class : Double.class;
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) { return new Integer(_rowIndex + 1); }
      if (_columnIndex == 1) { return new Integer(model_.mng_.getIdxElt(_rowIndex) + 1); }
      if (_columnIndex == 2) { return CtuluLib.getDouble(model_.getX(_rowIndex)); }
      return CtuluLib.getDouble(model_.getY(_rowIndex));
    }
  }

  H2dRubarLimniMng mng_;

  @Override
  public double getX(final int _i) {
    return mng_.getX(_i);
  }

  @Override
  public double getY(final int _i) {
    return mng_.getY(_i);
  }

  @Override
  public GrBoite getDomaine() {
    if (getNombre() == 0) { return null; }
    return new GrBoite(mng_.getEnv());
  }

  @Override
  public double getZ(final int _i) {
    return mng_.getZ(_i);
  }

  /**
   * @param _mng le manager de limni
   */
  public TrRubarLimniModel(final H2dRubarLimniMng _mng) {
    super();
    mng_ = _mng;
  }

  @Override
  public int getNombre() {
    return mng_.getNbPoint();
  }

  @Override
  protected void fillWithXYZInfo(final InfoData _m, final int _idx) {
    final int idxElt = mng_.getIdxElt(_idx);
    _m.setTitle(TrResource.getS("Limnigramme {0}", CtuluLibString.getString(_idx + 1)));
    _m.put(MvResource.getS("L'indice de l'�l�ment"), CtuluLibString.getString(idxElt + 1));
    _m.put("X", Double.toString(getX(_idx)));
    _m.put("Y", Double.toString(getY(_idx)));
    _m.put("Z", Double.toString(getZ(_idx)));

  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    final BuTable table = new CtuluTable(new DefaultTableModel(this));
    // final CtuluCellTextRenderer render = new CtuluCellTextRenderer() {
    //
    // protected void setValue(Object _value){
    // setText(mng_.getXYFormatter().format(((Double) _value).doubleValue()));
    // }
    // };
    // final TableColumnModel model = table.getColumnModel();
    // model.getColumn(2).setCellRenderer(render);
    // model.getColumn(3).setCellRenderer(render);
    return table;
  }

  public ZCalquePoint buildLayer() {
    return buildLayer(true);
  }

  private static class LimniFindExpression extends CalqueFindPointExpression {

    Variable idxElt_;

    /**
     * @param _data
     */
    public LimniFindExpression(final TrRubarLimniModel _data) {
      super(_data);
    }

    @Override
    public void initialiseExpr(final CtuluExpr _expr) {
      super.initialiseExpr(_expr);
      idxElt_ = _expr.addVar("idxElt", MvResource.getS("L'indice de l'�l�ment"));
    }

    @Override
    public void majVariable(int _idx, Variable[] _varToUpdate) {
      super.majVariable(_idx, _varToUpdate);
      if (CtuluLibArray.containsObject(_varToUpdate, idxElt_)) {
        idxElt_.setValue(new Integer(((TrRubarLimniModel) super.data_).mng_.getIdxElt(_idx)));
      }
    }
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return new LimniFindExpression(this);
  }

  public ZCalquePoint buildLayer(final boolean _addListener) {
    final LimniLayer r = new LimniLayer(this);
    r.setIconModel(0, new TraceIconModel(TraceIcon.CARRE_PLEIN, 3, Color.ORANGE));
    if (_addListener) {
      mng_.addListener(r);
    }
    return r;
  }

  private static class LimniLayer extends ZCalquePoint implements H2dRubarLimniListener, MvLayerGrid {

    protected LimniLayer(final TrRubarLimniModel _m) {
      super(_m);
    }

    @Override
    public int[] getSelectedElementIdx() {
      final int[] pt = getSelectedIndex();
      final H2dRubarLimniMng mng = ((TrRubarLimniModel) super.modele_).mng_;
      if (pt != null) {
        for (int i = pt.length - 1; i >= 0; i--) {
          pt[i] = mng.getIdxElt(pt[i]);
        }
      }
      return pt;
    }

    @Override
    public int[] getSelectedEdgeIdx() {
      return MvElementLayer.getSelectedEdgeIdx((EfGridVolumeInterface) ((TrRubarLimniModel) super.modele_).mng_
          .getGrid(), getSelectedElementIdx());
    }

    @Override
    public boolean isSelectionEdgeEmpty() {
      return isSelectionEmpty();
    }

    @Override
    public int[] getSelectedPtIdx() {
      if (isSelectionEmpty()) { return null; }
      final TIntHashSet set = new TIntHashSet();
      final CtuluListSelectionInterface select = getLayerSelection();
      final H2dRubarLimniMng mng = ((TrRubarLimniModel) super.modele_).mng_;
      final EfGridInterface g = mng.getGrid();
      final int max = select.getMaxIndex();
      for (int i = select.getMinIndex(); i <= max; i++) {
        if (select.isSelected(i)) {
          g.getElement(mng.getIdxElt(i)).fillList(set);
        }
      }
      final int[] res = set.toArray();
      Arrays.sort(res);
      return res;
    }

    @Override
    public boolean isSelectionElementEmpty() {
      return isSelectionEmpty();
    }

    @Override
    public boolean isSelectionPointEmpty() {
      return isSelectionEmpty();
    }

    @Override
    public void limniPointChanged(final H2dRubarLimniMng _mng) {
      repaint();
    }

    @Override
    public void limniTimeStepChanged(final H2dRubarLimniMng _mng) {}

    @Override
    public boolean isPaletteModifiable() {
      return false;
    }

    @Override
    public boolean isDestructible() {
      return false;
    }

  }

}