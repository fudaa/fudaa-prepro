/*
 * @creation 24 mars 2004
 *
 * @modification $Date: 2007-06-28 09:28:18 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Query;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuMenu;
import com.memoire.fu.Fu;
import com.memoire.fu.FuComparator;
import com.memoire.fu.FuLog;
import com.memoire.fu.FuSort;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISPrecision;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfFilter;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;
import org.fudaa.dodico.ef.operation.EfIndexHelper;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.commun.save.FudaaSaveLib;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrPreferences;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.data.*;
import org.nfunk.jep.Variable;
import org.nfunk.jep.VariableFactory;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: TrPostSourceAbstract.java,v 1.52 2007-06-28 09:28:18 deniger Exp $
 */
public abstract class TrPostSourceAbstract implements TrPostSource {
  protected List<H2dVariableType> vars = new ArrayList<>();
  private List<H2dVariableType> varsImmutable = Collections.unmodifiableList(vars);

  /**
   * Un modele pour les pas de temps.
   *
   * @author Fred Deniger
   * @version $Id: TrPostSourceAbstract.java,v 1.52 2007-06-28 09:28:18 deniger Exp $
   */
  public class FlecheListModel extends AbstractListModel {
    public void fireStructureChanged() {
      super.fireIntervalAdded(this, 0, getSize());
    }

    @Override
    public Object getElementAt(final int _index) {
      return (fleches_ == null || _index >= fleches_.length || _index < 0) ? null : fleches_[_index];
    }

    @Override
    public int getSize() {
      return fleches_ == null ? 0 : fleches_.length;
    }
  }

  @Override
  public String getSourceName() {
    return getMainFile() == null ? TrLib.getString("Aucune") : getMainFile().getName();
  }

  /**
   * Un modele pour les pas de temps.
   *
   * @author Fred Deniger
   * @version $Id: TrPostSourceAbstract.java,v 1.52 2007-06-28 09:28:18 deniger Exp $
   */
  public class VariableListModel extends AbstractListModel {
    public void fireStructureChanged() {
      // super.fireIntervalRemoved(this, 0, 0);
      super.fireIntervalAdded(this, 0, getSize());
      super.fireContentsChanged(this, 0, getSize());
    }

    public void fireVarChanged(final int _i) {
      super.fireContentsChanged(this, _i, _i);
    }

    @Override
    public Object getElementAt(final int _index) {
      return getVariable(_index);
    }

    @Override
    public int getSize() {
      return getVariableNb();
    }
  }

  protected class VariableObserver implements Observer {
    @Override
    public void update(final Observable _o, final Object _arg) {
      final Variable v = (Variable) _o;
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("TRP: constant variable changed " + v.getName());
      }
      final H2dVariableTypeCreated fr = getFroudVar();
      if (fr != null) {
        if (Fu.DEBUG && FuLog.isDebug()) {
          FuLog.debug("TRP: froud changed");
        }
        fireVariableChanged(fr, fr, true, false, TrPostDataHelper.getAllVarDependingOn(fr, TrPostSourceAbstract.this));
      }
      if (varUserCreateData_ != null) {
        for (final Iterator it = varUserCreateData_.entrySet().iterator(); it.hasNext(); ) {
          final Map.Entry e = (Map.Entry) it.next();
          final TrPostDataCreated data = (TrPostDataCreated) e.getValue();
          if (data == null) {
            it.remove();
          } else if (data.updateConstantVar(v)) {
            if (Fu.DEBUG && FuLog.isDebug()) {
              FuLog.debug("TRP:    ->" + e.getKey() + " changed");
            }
            final H2dVariableType vChanged = (H2dVariableType) e.getKey();
            fireVariableChanged(vChanged, vChanged, true, false, TrPostDataHelper.getAllVarDependingOn(fr,
                TrPostSourceAbstract.this));
          }
        }
      }
    }
  }

  public static void fillWithSourceInfo(InfoData _table, TrPostSource src) {

    Collection<File> files = src.getFiles();
    int idx = 1;
    for (File file : files) {
      String suffixe = files.size() > 1 ? CtuluLibString.getEspaceString(idx) : CtuluLibString.EMPTY_STRING;
      _table.put(TrResource.getS("Fichier r�sultat") + suffixe, file.getName());
      _table.put(TrResource.getS("Chemin complet") + suffixe, file.getAbsolutePath());
      _table.put(TrResource.getS("Titre Fichier r�sultat") + suffixe, src.getTitle());
      idx++;
    }
  }

  protected static void fillWithSourceInfo(String _pref, Map _table, TrPostSource src) {

    Collection<File> files = src.getFiles();
    int idx = 1;
    String pref = _pref == null ? CtuluLibString.EMPTY_STRING : _pref;
    boolean use2Digts = files.size() > 9;
    final boolean useSuffix = files.size() > 1;
    for (File file : files) {
      String suffix = CtuluLibString.EMPTY_STRING;
      if (useSuffix) {
        if (use2Digts) {
          suffix = " " + StringUtils.leftPad(Integer.toString(idx), 2, '0');
        } else {
          suffix = " " + Integer.toString(idx);
        }
      }
//      _table.put(pref + TrResource.getS("Fichier r�sultat") + suffix, file.getName());
      _table.put(pref + TrResource.getS("Chemin complet") + suffix, file.getAbsolutePath());
//      _table.put(pref + TrResource.getS("Titre Fichier r�sultat") + suffix, src.getTitle());
      idx++;
    }
  }

  protected static void fillWithSourceInfo(String _pref, Map _table, TrPostSourceReaderInterface src) {

    Collection<File> files = src.getFiles();
    int idx = 1;
    String pref = _pref == null ? CtuluLibString.EMPTY_STRING : _pref;
    for (File file : files) {
      String suffixe = files.size() > 1 ? CtuluLibString.getEspaceString(idx) : CtuluLibString.EMPTY_STRING;
      _table.put(pref + TrResource.getS("Fichier r�sultat") + suffixe, file.getName());
      _table.put(pref + TrResource.getS("Chemin complet") + suffixe, file.getAbsolutePath());
      idx++;
    }
  }

  public static Collection<File> getFiles(File _file) {
    return Collections.unmodifiableCollection(Arrays.asList(_file));
  }

  public static String getPrefNameSet(final H2dVariableType _t) {
    return "palette.min.var." + _t.getShortName() + ".set";
  }

  public static String getPrefNameValue(final H2dVariableType _t) {
    return "palette.min.var." + _t.getShortName() + ".val";
  }

  boolean close_;
  File dbTmpFile_;
  // final File f_;
  FlecheListModel flecheModel_;
  protected TrPostFlecheContent[] fleches_;
  EfGridInterface g_;
  Variable gravite_;
  CtuluUI impl_;
  TrPostDataInfoDoc info_;
  protected boolean inspected_;
  EfGridDataInterpolator interpolator_;
  GISPrecision precision_;
  private final Map shortNameCreateVar_ = new HashMap();
  TrPostTimeModel time_;
  protected final String title_;
  protected ObjectContainer tmpContainer_;
  Map varCreateData_;
  /**
   * Les variables utilisees.
   */
  H2dVariableType[] variable_;
  List varListener_;
  VariableListModel varModel_;
  Map varUserCreateData_;
  InterpolationVectorContainer vectors_;
  private String id_;

  protected TrPostSourceAbstract(final String _titre, final EfGridInterface _g, final double[] _time,
                                 final CtuluUI _impl) {
    title_ = _titre;
    gravite_ = new VariableFactory().createVariable("g", CtuluLib.getDouble(TrPreferences.TR.getDoubleProperty(
        "tr.gravity.value", 9.81)));
    gravite_.addObserver(new VariableObserver());
    time_ = new TrPostTimeModel();
    time_.setTimeSteps(_time);
    g_ = _g;
    impl_ = _impl;
    // setInitVar(_v);
  }

  protected void setInitVar(final H2dVariableType[] _v) {
    if (_v == null) {
      variable_ = new H2dVariableType[0];
    }else{
      variable_= (H2dVariableType[]) ArrayUtils.clone(_v);
    }
    FuSort.sort(variable_);
    vars.clear();
    vars.addAll(Arrays.asList(variable_));
  }

  @Override
  public void activate() {
  }

  protected final void addFroud(final Map _l) {
    if (getInitDataIndex("FROUDE") != null) {
      return;
    }
    // les variables h,u et v doivent etre d�finies
    if ((isInitVar(H2dVariableType.HAUTEUR_EAU) || _l.containsKey(H2dVariableType.HAUTEUR_EAU))
        && (isInitVar(H2dVariableType.VITESSE_U) || _l.containsKey(H2dVariableType.VITESSE_U))
        && (isInitVar(H2dVariableType.VITESSE_V) || _l.containsKey(H2dVariableType.VITESSE_V))) {
      final H2dVariableTypeCreated c = H2dVariableType.createTempVar(H2dResource.getS("Nombre de Froude"), "froude",
          shortNameCreateVar_);
      _l.put(c, new TrPostDataCreatedFroud(this));
    }
  }

  protected void addOtherVariables(final Map _l, final Map _fleche) {
    addFroud(_l);
  }

  @Override
  public long getReferenceDateInMillis() {
    return 0;
  }

  @Override
  public void addSpecificItemInMainMenu(final BuMenu _m, final TrPostCommonImplementation _impl) {
  }

  @Override
  public final void addUserVar(final H2dVariableTypeCreated[] _t, final TrPostDataCreated[] _cr,
                               final CtuluCommandContainer _cmd) {
    if (_t == null || _t.length == 0) {
      return;
    }
    buildUserVarMap();
    final Set newList = new HashSet(_t.length + variable_.length);
    newList.addAll(Arrays.asList(variable_));
    if (!newList.addAll(Arrays.asList(_t))) {
      return;
    }
    List newFleche = null;
    for (int i = _t.length - 1; i >= 0; i--) {
      varUserCreateData_.put(_t[i], _cr[i]);
      if (_cr[i] == null) {
        continue;
      }
      if (!shortNameCreateVar_.containsKey(_t[i].getShortName())) {
        shortNameCreateVar_.put(_t[i].getShortName(), _t[i]);
        final TrPostFlecheContent fleche = _cr[i].isFleche();
        // inutile de dupliquer les fleches: elles sont normalement deja au
        // format de la cible
        if (fleche != null) {
          if (newFleche == null) {
            newFleche = new ArrayList(_t.length);
          }
          newFleche.add(fleche.changeVar(_t[i]));
        }
      }
    }
    variable_ = (H2dVariableType[]) newList.toArray(new H2dVariableType[newList.size()]);
    FuSort.sort(variable_);
    if (newFleche != null) {
      if (fleches_ != null) {
        newFleche.addAll(Arrays.asList(fleches_));
      }
      fleches_ = (TrPostFlecheContent[]) newFleche.toArray(new TrPostFlecheContent[newFleche.size()]);
      Arrays.sort(fleches_, TrPostDataCreatedDefault.NAME_COMPARATOR);
    }
    fireVariableAdd(false);
    fireVarListModelChanged();
    if (newFleche != null) {
      fireVariableAdd(true);
      fireFlecheListModelChanged();
    }

    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          addUserVar(_t, _cr, null);
        }

        @Override
        public void undo() {
          removeUserVar(_t, null);
        }
      });
    }
  }

  @Override
  public final void addVariableListener(final TrPostDataListener _listener) {
    if (varListener_ == null) {
      varListener_ = new ArrayList(2);
    }
    if (!varListener_.contains(_listener)) {
      varListener_.add(_listener);
    }
  }

  protected void afterOpenDatas(final ProgressionInterface _int, final CtuluAnalyze _analyze) {
    if (time_ != null) {
      time_.checkTime(_analyze);
    }
  }

  @Override
  public final void buildDefaultVarUpdateLists() {
    buildDefaultVectors();
    updateVarList();
    clearInterpolator();
  }

  protected void buildDefaultVectors() {
    final Map nameFleche = new HashMap();
    varCreateData_ = new HashMap();
    //x et y :
    varCreateData_.put(H2dVariableType.POINT_X, new TrPostDataCreatedPtX(this));    // par defaut vitesse et debit
    varCreateData_.put(H2dVariableType.POINT_Y, new TrPostDataCreatedPtY(this));    // par defaut vitesse et debit
    if (isInitVar(H2dVariableType.VITESSE_U)) {
      if (isInitVar(H2dVariableType.VITESSE_V)) {
        final TrPostFlecheContent vec = createSimpleVecteurContent(H2dVariableType.VITESSE, H2dVariableType.VITESSE_U,
            H2dVariableType.VITESSE_V);
        nameFleche.put(H2dVariableType.VITESSE, vec);

        if (!isInitVar(H2dVariableType.VITESSE)) {
          varCreateData_.put(H2dVariableType.VITESSE, vec);
        }
      }
    }
    boolean qxDefined = isInitVar(H2dVariableType.DEBIT_X);
    if (!qxDefined) {
      if (isInitVar(H2dVariableType.HAUTEUR_EAU) && isInitVar(H2dVariableType.VITESSE_U)) {
        qxDefined = true;
        TrPostDataCreatedMultiply qx = new TrPostDataCreatedMultiply(this, H2dVariableType.HAUTEUR_EAU, H2dVariableType.VITESSE_U);
        varCreateData_.put(H2dVariableType.DEBIT_X, qx);
      }
    }
    boolean qyDefined = isInitVar(H2dVariableType.DEBIT_Y);
    if (!qyDefined) {
      if (isInitVar(H2dVariableType.HAUTEUR_EAU) && isInitVar(H2dVariableType.VITESSE_V)) {
        qyDefined = true;
        TrPostDataCreatedMultiply qy = new TrPostDataCreatedMultiply(this, H2dVariableType.HAUTEUR_EAU, H2dVariableType.VITESSE_V);
        varCreateData_.put(H2dVariableType.DEBIT_Y, qy);
      }
    }
    if (qxDefined && qyDefined) {
      final TrPostFlecheContent vec = createSimpleVecteurContent(H2dVariableType.DEBIT, H2dVariableType.DEBIT_X,
          H2dVariableType.DEBIT_Y);
      nameFleche.put(H2dVariableType.DEBIT, vec);
      if (!isInitVar(H2dVariableType.DEBIT)) {
        varCreateData_.put(H2dVariableType.DEBIT, vec);
      }
    }

    if (!isInitVar(H2dVariableType.COTE_EAU)) {
      if (isInitVar(H2dVariableType.BATHYMETRIE) && isInitVar(H2dVariableType.HAUTEUR_EAU)) {
        varCreateData_.put(H2dVariableType.COTE_EAU, new TrPostDataCreatedPlus(this, H2dVariableType.BATHYMETRIE,
            H2dVariableType.HAUTEUR_EAU));
      }
    } // sinon on cree la hauteur d'eau
    else if (!isInitVar(H2dVariableType.HAUTEUR_EAU) && isInitVar(H2dVariableType.BATHYMETRIE)) {
      varCreateData_.put(H2dVariableType.HAUTEUR_EAU, new TrPostDataCreatedMoins(this, H2dVariableType.COTE_EAU,
          H2dVariableType.BATHYMETRIE));
    }
    Collection<H2dVariableType> vars = getInitVar();
    if (vars != null) {
      for (H2dVariableType vi : vars) {
        final String name = vi.getName();
        if (name.endsWith("X") && vi != H2dVariableType.DEBIT_X && vi != H2dVariableType.SXX) {
          final H2dVariableType idxY = getInitDataIndex(name.substring(0, name.length() - 1) + 'Y');
          if (idxY != null) {
            H2dVariableType nt = null;
            if (vi.getParentVariable() != null) {
              nt = vi.getParentVariable();
            } else {
              nt = H2dVariableType.createTempVar(name.substring(0, name.length() - 1).trim(), shortNameCreateVar_);
            }
            if (!nameFleche.containsKey(nt)) {
              final TrPostFlecheContent vec = createSimpleVecteurContent(nt, vi, idxY);
              nameFleche.put(nt, vec);
              varCreateData_.put(nt, vec);
            }
          }
        }
      }
    }
    addOtherVariables(varCreateData_, nameFleche);
    fleches_ = new TrPostFlecheContent[nameFleche.size()];
    nameFleche.values().toArray(fleches_);
    Arrays.sort(fleches_, TrPostDataCreatedDefault.NAME_COMPARATOR);
  }

  private void buildUserVarMap() {
    if (varUserCreateData_ == null) {
      varUserCreateData_ = new HashMap();
    }
  }

  @Override
  public TrPostVisuPanel buildVisuPanel(final TrPostProjet _parent, final BCalqueLegende _legende) {
    return new TrPostVisuPanel(_parent.getImpl(), _parent, _legende, this);
  }

  protected final void changeUserVar(final H2dVariableTypeCreated _old, final H2dVariableTypeCreated _new) {
    if (_old != null && _new != null && isUserCreatedVar(_old)) {
      final int i = CtuluLibArray.findObjectEgalEgal(variable_, _old);
      if (i < 0) {
        return;
      }
      _old.removeShortName(getShortNameCreateVar());
      getShortNameCreateVar().put(_new.getShortName(), _new);
      if (!varUserCreateData_.containsKey(_old)) {
        FuLog.warning("TRP: the variable " + _old + " is unknown !");
      }
      varUserCreateData_.put(_new, varUserCreateData_.remove(_old));
      variable_[i] = _new;
      FuSort.sort(variable_, FuComparator.STRING_COMPARATOR);
      if (varModel_ != null) {
        varModel_.fireStructureChanged();
      }
    }
  }

  protected final void clearInterpolator() {
    if (interpolator_ != null) {
      interpolator_.getVect().clear();
    }
  }

  @Override
  public void close() {
    close_ = true;
    // on enregistre les valeurs
    CtuluLibMessage.info("save database");
    // majDbMinMax();
    if (tmpContainer_ != null) {
      tmpContainer_.close();
      tmpContainer_ = null;
    }
    if (dbTmpFile_ != null) {

      try {
        Files.delete(dbTmpFile_.toPath());
      } catch (IOException e) {
        FuLog.error(e);
      }
    }
  }

  protected TrPostDataMinMaxGlobalItem computeMinMax(final H2dVariableType _variable, final ProgressionInterface _prog) {
    final ProgressionUpdater up = createUpdaterForMinMax(_variable);
    TrPostExtremVisitor visitor = null;
    for (int i = getNbTimeStep() - 1; i >= 0; i--) {
      final EfData d = getData(_variable, i);
      if (visitor == null) {
        visitor = new TrPostExtremVisitor(d.isElementData() ? getGrid().getEltNb() : getGrid().getPtsNb(), d.isElementData());
      }
      visitor.setCurrentTime(getTimeStep(i));
      d.iterate(visitor);
      if (up != null) {
        up.majAvancement();
      }
    }
    impl_.clearMainProgression();
    if (visitor == null) {
      return null;
    }
    return new TrPostDataMinMaxGlobalItem(_variable.getShortName(), visitor);
  }

  /**
   * @param _name le nom de la variable
   * @param _idxX le vx
   * @param _idxY le vy
   * @return le conteneur du vecteur
   */
  public final TrPostFlecheContent createSimpleVecteurContent(final H2dVariableType _name, final H2dVariableType _idxX, final H2dVariableType _idxY) {
    return new TrPostFlecheContentDefaut(this, _name, _idxX, _idxY);
  }

  protected final ProgressionUpdater createUpdaterForMinMax(final H2dVariableType _variable) {
    final ProgressionUpdater up = new ProgressionUpdater(impl_.getMainProgression());
    up.setValue(10, getNbTimeStep());
    up.majProgessionStateOnly(TrResource.getS("Recherche des extrema pour {0}", _variable.getName()));
    return up;
  }

  protected final InterpolationVectorContainer createVectorInterpolation() {
    if (vectors_ == null) {
      vectors_ = new InterpolationVectorContainer();
      // if (fleches_ != null) {
      // for (int i = 0; i < fleches_.length; i++) {
      // vectors_.addVxVy(fleches_[i].getVx(), fleches_[i].getVy());
      // }
      // }
    }
    return vectors_;
  }

  private void deleteAll(final ObjectSet _set) {
    FudaaSaveLib.deleteAll(tmpContainer_, _set);
  }

  private void fillAllValuesFrom(final InfoData _m, final int _element, final double _x, final double _y,
                                 final int _time) {
    if (_element >= 0) {
      _m.put("X", CtuluLib.DEFAULT_NUMBER_FORMAT.format(_x));
      _m.put("Y", CtuluLib.DEFAULT_NUMBER_FORMAT.format(_y));
      for (int i = 0; i < variable_.length; i++) {
        final EfData d = getData(variable_[i], _time);
        _m.put(variable_[i].getName(), CtuluLib.DEFAULT_NUMBER_FORMAT.format(d.getValue(_element)));
      }
    }
  }

  @Override
  public void fillInterpolateInfo(final InfoData _m, final int _element, final double _x, final double _y,
                                  final int _time, final String _layerTitle) {
    // TODO a remplacer par isVolumique
    if (containsElementVar()) {
      fillVolumiqueInterpolateInfo(_m, _element, _x, _y, _time, _layerTitle);
      return;
    }
    CtuluListSelection selection = EfIndexHelper.getNearestNode(getGrid(), _x, _y, 0.05, null);
    if (selection != null && selection.isOnlyOnIndexSelected()) {
      int maxIndex = selection.getMaxIndex();
      _m.setTitle("-- " + TrResource.getS("Valeurs au noeud") + CtuluLibString.getEspaceString(maxIndex + 1)
          + (_layerTitle == null ? CtuluLibString.EMPTY_STRING : CtuluLibString.ESPACE + _layerTitle) + " --");
      fillAllValuesFrom(_m, maxIndex, getGrid().getPtX(maxIndex), getGrid().getPtY(maxIndex), _time);
    } else {
      _m.setTitle("-- " + TrResource.getS("Interpolation")
          + (_layerTitle == null ? CtuluLibString.EMPTY_STRING : CtuluLibString.ESPACE + _layerTitle) + " --");
      if (_element >= 0) {
        _m.put("X", CtuluLib.DEFAULT_NUMBER_FORMAT.format(_x));
        _m.put("Y", CtuluLib.DEFAULT_NUMBER_FORMAT.format(_y));
        _m.put(TrResource.getS("El�ment englobant"), CtuluLibString.getString(_element + 1));
        final EfGridDataInterpolator interpolator = getInterpolator();
        for (int i = 0; i < variable_.length; i++) {
          try {
            _m.put(variable_[i].getName(),
                CtuluLib.DEFAULT_NUMBER_FORMAT.format(interpolator.interpolate(_element, _x,
                    _y, variable_[i], _time)));
          } catch (final IOException _evt) {
            FuLog.error(_evt);
            _m.put(variable_[i].getName(), "ERROR");
          }
        }
      }
    }
  }

  public void fillVolumiqueInterpolateInfo(final InfoData _m, final int _element, final double _x, final double _y,
                                           final int _time, final String _layerTitle) {
    _m.setTitle("-- " + TrResource.getS("Valeurs sur l'�l�ment") + CtuluLibString.getEspaceString(_element + 1)
        + " --");
    fillAllValuesFrom(_m, _element, _x, _y, _time);
  }

  @Override
  public final void fillWithConstantVar(final CtuluExpr _expr) {
    final Variable vg = _expr.addVar(gravite_.getName(), H2dResource.getS("Acc�l�ration de la pesanteur"));
    vg.setValue(gravite_.getValue());
    vg.setIsConstant(true);
  }

  @Override
  public void fillWithSourceCreationInfo(String _pref, Map _table) {
    _table.put(ZEbliCalquesPanel.TITRE_FIC, getTitle());
    fillWithSourceInfo(CtuluLibString.EMPTY_STRING, _table, this);
  }

  protected final void fireFlecheListModelChanged() {
    if (flecheModel_ != null) {
      flecheModel_.fireStructureChanged();
    }
    clearInterpolator();
  }

  protected final void fireVariableAdd(final boolean _isVect) {
    if (varListener_ != null) {
      for (int i = varListener_.size() - 1; i >= 0; i--) {
        ((TrPostDataListener) varListener_.get(i)).dataAdded(_isVect);
      }
    }
    clearInterpolator();
  }

  protected final void fireVariableChanged(final H2dVariableType _old, final H2dVariableType _new,
                                           final boolean _dataChanged, final boolean _isVect, final Set _varDepending) {
    if (_dataChanged) {
      updateCache(_new);
      if (_varDepending != null) {
        for (final Iterator iter = _varDepending.iterator(); iter.hasNext(); ) {
          updateCache((H2dVariableType) iter.next());
        }
      }
    }
    if (varListener_ != null) {
      for (int i = varListener_.size() - 1; i >= 0; i--) {
        ((TrPostDataListener) varListener_.get(i)).dataChanged(_old, _new, _dataChanged, _isVect, _varDepending);
      }
    }
    clearInterpolator();
  }

  protected final void fireVariableRemoved(final H2dVariableType[] _vars, final boolean _isVect) {
    if (varListener_ != null) {
      for (int i = varListener_.size() - 1; i >= 0; i--) {
        ((TrPostDataListener) varListener_.get(i)).dataRemoved(_vars, _isVect);
      }
    }
    clearInterpolator();
  }

  protected final void fireVarListModelChanged() {
    if (varModel_ != null) {
      varModel_.fireStructureChanged();
    }
    clearInterpolator();
  }

  @Override
  public H2dVariableType[] getAllVariablesNonVec() {
    final H2dVariableType[] r = new H2dVariableType[variable_.length];
    System.arraycopy(variable_, 0, r, 0, r.length);
    return r;
  }

  /**
   * @return tous les id des variables utilisees (vec ou non)
   */
  @Override
  public final H2dVariableType[] getAvailableVar() {
    final H2dVariableType[] rf = new H2dVariableType[variable_.length];
    System.arraycopy(variable_, 0, rf, 0, rf.length);
    return rf;
  }

  public final ObjectContainer getBdTemp() {
    initBd();
    return tmpContainer_;
  }

  @Override
  public final Variable[] getConstantVar() {
    final VariableFactory fact = new VariableFactory();
    final Variable pi = fact.createVariable("pi", CtuluLib.getDouble(Math.PI));
    final Variable e = fact.createVariable("e", CtuluLib.getDouble(Math.E));
    return new Variable[]{gravite_, pi, e};
  }

  public final TrPostDataCreated getCreatedVar(final H2dVariableType _t) {
    return varCreateData_ == null ? null : (TrPostDataCreated) varCreateData_.get(_t);
  }

  @Override
  public final EfData getData(final CtuluVariable _o, final int _timeIdx) throws IOException {
    return getData((H2dVariableType) _o, _timeIdx);
  }

  @Override
  public final double getData(final CtuluVariable _o, final int _timeIdx, final int _idxObjet) throws IOException {
    return getData((H2dVariableType) _o, _timeIdx, _idxObjet);
  }

  @Override
  public EfData getData(final H2dVariableType _variable, final int _timeStep) {
    if (_timeStep < 0) {
      return null;
    }
    if (isInitVar(_variable)) {
      return getInitData(_variable, _timeStep);
    }
    if (isCreatedVar(_variable)) {
      return ((TrPostDataCreated) varCreateData_.get(_variable)).getDataFor(_timeStep);
    }
    if (isUserCreatedVar(_variable)) {
      return ((TrPostDataCreated) varUserCreateData_.get(_variable)).getDataFor(_timeStep);
    }
    return null;
  }

  @Override
  public double getData(final H2dVariableType _variable, final int _timeStep, final int _idxPt) throws IOException {
    if (isInitVar(_variable)) {
      return getInitData(_variable, _timeStep, _idxPt);
    }
    if (isCreatedVar(_variable)) {
      return ((TrPostDataCreated) varCreateData_.get(_variable)).getValue(_timeStep,
          _idxPt);
    }
    if (isUserCreatedVar(_variable)) {
      return ((TrPostDataCreated) varUserCreateData_.get(_variable)).getValue(
          _timeStep, _idxPt);
    }
    return -1;
  }

  private final File getDbFile() {
    if (dbTmpFile_ == null) {
      try {
        dbTmpFile_ = File.createTempFile("fudaaPrepro", ".db");
      } catch (final IOException e) {
        FuLog.warning(e);
      }
      if (dbTmpFile_ != null) {
        dbTmpFile_.deleteOnExit();
      }
    }
    return dbTmpFile_;
  }

  @Override
  public final double getDefaultPaletteMinValueFor(final H2dVariableType _t) {
    return TrPreferences.TR.getDoubleProperty(getPrefNameValue(_t), 1E-4);
  }

  // public final File getFile() {
  // return f_;
  // }
  @Override
  public final CtuluRange getExtrema(final CtuluRange _r, final H2dVariableType _variable, final EfFilter _cond,
                                     final ProgressionInterface _prog) {
    if (_variable == null) {
      if (_r != null) {
        _r.setToNill();
        return _r;
      }
      return null;
    }
    final TrPostDataMinMaxGlobalItem it = getGlobalExtrema(_variable, _prog);
    if (it == null) {
      if (_r != null) {
        _r.setToNill();
        return _r;
      }
      return null;
    }
    CtuluRange range = _r;
    if (range == null) {
      range = new CtuluRange();
    } else {
      _r.setToNill();
    }
    if (_cond == null) {
      range.max_ = it.getMax();
      range.min_ = it.getMin();
    } else {
      range.setToNill();
      final CtuluCollectionDouble mins = it.getMinOnObjects();
      final CtuluCollectionDouble maxs = it.getMaxOnObjects();
      final boolean elt = it.isElementData();
      final int nb = elt ? g_.getEltNb() : g_.getPtsNb();
      for (int i = nb - 1; i >= 0; i--) {
        if ((elt && _cond.isActivatedElt(i)) || (!elt && _cond.isActivated(i))) {
          double tmp = mins.getValue(i);
          range.expandTo(tmp);
          tmp = maxs.getValue(i);
          range.expandTo(tmp);
        }
      }
    }
    if (range.max_ < range.min_) {
      range.max_ = 0;
      range.min_ = 0;
    }
    return range;
  }

  /**
   * @param _variable la variable demandee
   * @param _time le pas de temps
   * @return le min pour la variable sur le pas de temps demandee
   */
  @Override
  public final CtuluRange getExtremaForTimeStep(final CtuluRange _r, final H2dVariableType _variable, final int _time,
                                                final EfFilter _cond) {
    if (_variable == null) {
      return new CtuluRange();
    }
    CtuluRange range = _r;
    if (range == null) {
      range = new CtuluRange();
    } else {
      _r.setToNill();
    }
    final EfData d = getData(_variable, _time);
    if (d == null) {
      return null;
    }
    return EfLib.getMinMax(d, range, _cond);
  }

  @Override
  public final TrPostFlecheContent getFlecheContent(final H2dVariableType _t) {
    if (fleches_ != null) {
      for (int i = fleches_.length - 1; i >= 0; i--) {
        if (fleches_[i].getVar() == _t) {
          return fleches_[i];
        }
      }
    }
    return null;
  }

  @Override
  public final ListModel getFlecheListModel() {
    if (flecheModel_ == null) {
      flecheModel_ = new FlecheListModel();
    }
    return flecheModel_;
  }

  /**
   * @return la variable utilisee pour le nombre de froude. null si non utilisee.
   */
  protected final H2dVariableTypeCreated getFroudVar() {
    if (varCreateData_ == null) {
      return null;
    }
    final String name = "froude";
    for (final Iterator it = varCreateData_.keySet().iterator(); it.hasNext(); ) {
      final H2dVariableType t = (H2dVariableType) it.next();
      if (name.equals(t.getShortName())) {
        return (H2dVariableTypeCreated) t;
      }
    }
    return null;
  }

  @Override
  public final TrPostDataMinMaxGlobalItem getGlobalExtrema(final H2dVariableType _variable,
                                                           final ProgressionInterface _prog) {
    if (_variable == null) {
      FuLog.warning("TRP: _var null");
      return null;
    }
    if (tmpContainer_ != null) {
      final Query q = tmpContainer_.query();
      TrPostDataMinMaxGlobalItem.updateQueryFor(q, _variable.getShortName());
      final ObjectSet s = q.execute();
      if (s.size() > 1) {
        FuLog.warning(new Throwable());
        deleteAll(s);
      } else if (s.size() == 1) {
        return (TrPostDataMinMaxGlobalItem) s.next();
      }
    }
    initBd();
    final TrPostDataMinMaxGlobalItem r = computeMinMax(_variable, _prog);
    if (r != null && tmpContainer_ != null) {
      tmpContainer_.set(r);
    }
    if (impl_ != null) {
      BuLib.invokeLater(new Runnable() {
        @Override
        public void run() {
          impl_.clearMainProgression();
        }
      });
    }

    return r;
  }

  @Override
  public final Variable getGravity() {
    return gravite_;
  }

  @Override
  public EfGridInterface getGrid() {
    return g_;
  }

  @Override
  public String getId() {
    return id_;
  }

  public abstract EfData getInitData(H2dVariableType _varIdx, int _timeIdx);

  // protected final int getInitDataIndex(final H2dVariableType _t) {
  // if (initVarIdx_ != null && _t != null && initVarIdx_.contains(_t)) { return
  // initVarIdx_.get(_t); }
  // return -1;
  // }
  public abstract double getInitData(H2dVariableType _varIdx, int _timeIdx, int _ptIdx) throws IOException;

  protected final H2dVariableType getInitDataIndex(final String _varName) {
    Collection<H2dVariableType> vars = getInitVar();
    if (vars != null) {
      for (H2dVariableType variableType : vars) {
        if (variableType.getName().equals(_varName)) {
          return variableType;
        }
      }
    }
    return null;
  }

  protected final Collection<H2dVariableType> getInitVar() {
    return varsImmutable;
  }

  @Override
  public final EfGridDataInterpolator getInterpolator() {
    if (interpolator_ == null) {
      interpolator_ = new EfGridDataInterpolator(this, createVectorInterpolation());
    }
    return interpolator_;
  }

  @Override
  public final boolean[] getMinPaletteActived(final H2dVariableType[] _v) {
    if (_v == null) {
      return null;
    }
    final boolean[] r = new boolean[_v.length];
    for (int i = _v.length - 1; i >= 0; i--) {
      r[i] = isDefaultPaletteMinValueDefined(_v[i]);
    }
    return r;
  }

  @Override
  public final TrPostMinPaletteTableModel getMinPaletteModel() {
    return new TrPostMinPaletteTableModel(this);
  }

  @Override
  public final double[] getMinPaletteValues(final H2dVariableType[] _v) {
    if (_v == null) {
      return null;
    }
    final double[] r = new double[_v.length];
    for (int i = _v.length - 1; i >= 0; i--) {
      r[i] = getDefaultPaletteMinValueFor(_v[i]);
    }
    return r;
  }

  /**
   * @return nombre de variables definissant une fleche
   */
  public final int getNbFleche() {
    return fleches_ == null ? 0 : fleches_.length;
  }

  @Override
  public final int getNbTimeStep() {
    return time_.getNbTimeStep();
  }

  /**
   * @return le nombre de variables
   */
  public final int getNbVar() {
    return variable_ == null ? 0 : variable_.length;
  }

  @Override
  public final ListModel getNewFlecheListModel() {
    return new FlecheListModel();
  }

  @Override
  public final FudaaCourbeTimeListModel getNewTimeListModel() {
    return time_.createNewTimeModel();
  }

  @Override
  public final ListModel getNewVarListModel() {
    return new VariableListModel();
  }

  @Override
  public final GISPrecision getPrecisionModel() {
    if (precision_ == null) {
      precision_ = new GISPrecision();
    }
    return precision_;
  }

  /**
   * @return le tableau nom court -> var cr��e
   */
  @Override
  public final Map getShortNameCreateVar() {
    return shortNameCreateVar_;
  }

  @Override
  public TrPostTimeModel getTime() {
    return time_;
  }

  @Override
  public final CtuluNumberFormatI getTimeFormatter() {
    return time_.getTimeFormatter();
  }

  @Override
  public String getFormattedTime(int i) {
    return getTimeFormatter().format(getTimeStep(i));
  }

  /**
   * @return un model representant les pas de temps
   */
  @Override
  public ListModel getTimeListModel() {
    return time_.getTimeListModel();
  }

  @Override
  public final double getTimeStep(final int _i) {
    return time_.getTimeStep(_i);
  }

  @Override
  public String getTitle() {
    return title_;
  }

  @Override
  public String getFormatedTitle() {
    return TrLib.formatName(getTitle()) + " | " + TrResource.getS("Fichier") + ": "
        + TrLib.formatFichier(getMainFile());
  }

  @Override
  public final H2dVariableType[] getUserCreatedVar() {
    if (varUserCreateData_ == null) {
      return new H2dVariableType[0];
    }
    final H2dVariableType[] res = new H2dVariableType[varUserCreateData_.size()];
    varUserCreateData_.keySet().toArray(res);
    FuSort.sort(res);
    return res;
  }

  @Override
  public final TrPostDataCreated getUserCreatedVar(final H2dVariableType _t) {
    return varUserCreateData_ == null ? null : (TrPostDataCreated) varUserCreateData_.get(_t);
  }

  public final H2dVariableType getVarForShortName(final String _var) {
    if (variable_ != null) {
      for (int i = variable_.length - 1; i >= 0; i--) {
        if (variable_[i].getShortName().equals(_var)) {
          return variable_[i];
        }
      }
    }
    return null;
  }

  @Override
  public final H2dVariableType getVariable(final int _idx) {
    if (_idx >= variable_.length || _idx < 0) {
      return null;
    }
    return variable_[_idx];
  }

  @Override
  public final int getVariableNb() {
    return variable_.length;
  }

  /**
   * @return un model representant les pas de temps
   */
  @Override
  public final ListModel getVarListModel() {
    if (varModel_ == null) {
      varModel_ = new VariableListModel();
    }
    return varModel_;
  }

  @Override
  public final H2dVariableType[] getVarToDefinedMinPalette() {
    final List r = new ArrayList();
    if (isDefined(H2dVariableType.HAUTEUR_EAU)) {
      r.add(H2dVariableType.HAUTEUR_EAU);
    }
    for (int i = fleches_.length - 1; i >= 0; i--) {
      r.add(fleches_[i].getVar());
    }
    final H2dVariableType[] rf = new H2dVariableType[r.size()];
    r.toArray(rf);
    Arrays.sort(rf);
    return rf;
  }

  /**
   * @return les erreurs possibles lors de l'ouverture
   */
  public final String initBd() {
    if (tmpContainer_ == null) {
      final File dbFile = getDbFile();
      final String err = CtuluLibFile.canWrite(dbFile);
      if (err != null) {
        return err;
      }
      if (dbFile != null) {
        tmpContainer_ = Db4o.openFile(dbFile.getAbsolutePath());
      }
    }
    return null;
  }

  @Override
  public final void initializeTempDb(final CtuluAnalyze _analyze) {
    final String err = initBd();
    if (err != null) {
      _analyze.setDesc(TrResource.getS("Sauvegarde des donn�es"));
      final File dbFile = getDbFile();
      final String name = dbFile == null ? "" : dbFile.getName();
      final String mess = TrResource.getS("Les sauvegardes ne seront pas effectu�es") + CtuluLibString.LINE_SEP
          + TrResource.getS("Le fichier de sauvegarde {0} ne peut pas �tre acc�der:", name)
          + CtuluLibString.LINE_SEP + err;
      _analyze.addError(mess, -1);
    }
  }

  protected final void initShortNameMap(final Map _init) {
    shortNameCreateVar_.putAll(_init);
  }

  /**
   * @param _t les variables a interpoler
   * @param _time le pas de temps
   * @param _values le tableau qui recevra les valeurs
   * @param _elementIdx l'element selectionne
   * @param _x le x du point a interpoler
   * @param _y le y du point a interpoler
   * @return true si operation reussie
   */
  public final boolean interpolate(final H2dVariableType[] _t, final int _time, final double[] _values,
                                   final int _elementIdx, final double _x, final double _y) {
    final EfGridDataInterpolator interpolator = getInterpolator();
    for (int i = _t.length - 1; i >= 0; i--) {
      try {
        _values[i] = interpolator.interpolate(_elementIdx, _x, _y, _t[i], _time);
      } catch (final IOException _evt) {
        FuLog.error(_evt);
      }
    }
    return true;
  }

  public final boolean isAutoCreatedVar(final H2dVariableType _t) {
    return varCreateData_ != null && varCreateData_.containsKey(_t);
  }

  @Override
  public final boolean isClosed() {
    return close_;
  }

  public final boolean isCreatedVar(final H2dVariableType _t) {
    return varCreateData_ != null && varCreateData_.containsKey(_t);
  }

  @Override
  public final boolean isDefaultPaletteMinValueDefined(final H2dVariableType _t) {
    return TrPreferences.TR.getBooleanProperty(getPrefNameSet(_t), false);
  }

  @Override
  public final boolean isDefined(final CtuluVariable _var) {
    return (_var instanceof H2dVariableType) ? isDefined((H2dVariableType) _var) : false;
  }

  @Override
  public final boolean isDefined(final H2dVariableType _variable) {
    return isInitVar(_variable) || isCreatedVar(_variable) || isUserCreatedVar(_variable);
  }

  @Override
  public boolean isElementVar(final CtuluVariable _idxVar) {
    return isVolumique();
  }

  @Override
  public final boolean isInitVar(final H2dVariableType _t) {
    return getInitVar().contains(_t);
  }

  public final boolean isInspectable() {
    return true;
  }

  @Override
  public final boolean isInspected() {
    return inspected_;
  }

  /**
   * @param _var le nom court de la variable vect ou non
   * @return true si les min/max de cette variable sont calcules
   */
  @Override
  public final boolean isMinMaxCompute(final H2dVariableType _var) {
    if (tmpContainer_ == null) {
      return false;
    }
    final Query q = tmpContainer_.query();
    TrPostDataMinMaxGlobalItem.updateQueryFor(q, _var.getShortName());
    return q.execute().size() == 1;
  }

  @Override
  public final boolean isMinMaxCompute(final H2dVariableType _varIdx, final int _timeIdx) {
    // return isMinMaxComputeCommon(_varIdx, _timeIdx, false);
    // pour l'instant on ne sauvegarde pas les donnees.
    return false;
  }

  @Override
  public boolean isOpened(final File _f) {
    if (_f == null) {
      return false;
    }
    return getFiles().contains(_f);
  }

  @Override
  public boolean containsElementVar() {
    return false;
  }

  public final boolean isSamePoint(final double _x1, final double _y1, final double _x2, final double _y2) {
    return CtuluLibGeometrie.getDistance(_x1, _y1, _x2, _y2) < 1E-5;
  }

  @Override
  public final boolean isUserCreatedVar(final H2dVariableType _t) {
    return varUserCreateData_ != null && varUserCreateData_.containsKey(_t);
  }

  @Override
  public final boolean isUserVarEditable(final H2dVariableTypeCreated _var) {
    final TrPostDataCreated userCreatedVar = getUserCreatedVar(_var);
    return userCreatedVar != null && userCreatedVar.isEditable();
  }

  public final int isVectVar(final H2dVariableType _var) {
    if (fleches_ != null) {
      for (int i = fleches_.length - 1; i >= 0; i--) {
        if (fleches_[i].getVar().equals(_var)) {
          return i;
        }
      }
    }
    return -1;
  }

  public boolean isVolumique() {
    return false;
  }

  @Override
  public boolean openDatas(final ProgressionInterface _int, final CtuluAnalyze _analyze, final CtuluUI _ui) {
    if (tmpContainer_ != null) {
      FuLog.error("the database is already opened", new Throwable());
    }
    FudaaSaveLib.configureDb4o();

    initializeTempDb(_analyze);

    // c'est la premiere fois que l'on ouvre le fichier de post.
    // on enregistre les infos
    return true;
  }

  protected boolean readMinMaxSave() {
    return true;
  }

  @Override
  public final void removeMinMax(final String _var) {
    if (tmpContainer_ == null) {
      return;
    }
    final Query q = tmpContainer_.query();
    TrPostDataMinMaxGlobalItem.updateQueryFor(q, _var);
    final ObjectSet s = q.execute();
    if (s != null) {
      deleteAll(s);
    }
  }

  @Override
  public final void removeUserVar(final H2dVariableTypeCreated[] _t, final CtuluCommandContainer _cmd) {
    if (varUserCreateData_ == null) {
      return;
    }
    if (_t != null) {
      final List toRemove = Arrays.asList(_t);
      final List varToKeep = new ArrayList(Arrays.asList(variable_));
      // si rien n'a ete enleve on retourne
      if (!varToKeep.removeAll(toRemove)) {
        return;
      }
      // on teste les vecteurs
      List flecheToRemove = null;
      List flecheVarToRemove = null;
      for (int i = _t.length - 1; i >= 0; i--) {
        final int idx = isVectVar(_t[i]);
        if (idx >= 0) {
          if (flecheToRemove == null) {
            flecheToRemove = new ArrayList(_t.length);
            flecheVarToRemove = new ArrayList(_t.length);
          }
          flecheToRemove.add(fleches_[idx]);
          // pas null !
          flecheVarToRemove.add(fleches_[idx].getVar());
        }
      }
      variable_ = (H2dVariableType[]) varToKeep.toArray(new H2dVariableType[varToKeep.size()]);
      if (flecheToRemove != null) {
        final List l = new ArrayList(Arrays.asList(fleches_));
        l.removeAll(flecheToRemove);
        fleches_ = (TrPostFlecheContent[]) l.toArray(new TrPostFlecheContent[l.size()]);
      }

      final TrPostDataCreated[] cr = _cmd == null ? null : new TrPostDataCreated[_t.length];
      if (_cmd == null) {
        varUserCreateData_.keySet().removeAll(toRemove);
      } else {
        for (int i = cr.length - 1; i >= 0; i--) {
          cr[i] = (TrPostDataCreated) varUserCreateData_.remove(_t[i]);
        }
      }
      for (int i = _t.length - 1; i >= 0; i--) {
        _t[i].removeShortName(shortNameCreateVar_);
      }
      fireVariableRemoved(_t, false);
      fireVarListModelChanged();
      if (flecheToRemove != null) {
        fireVariableRemoved((H2dVariableType[]) flecheVarToRemove.toArray(new H2dVariableType[flecheVarToRemove.size()]), true);
        fireFlecheListModelChanged();
      }
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {
          @Override
          public void redo() {
            removeUserVar(_t, null);
          }

          @Override
          public void undo() {
            addUserVar(_t, cr, null);
          }
        });
      }
    }
  }

  @Override
  public final void removeVariableListener(final TrPostDataListener _listener) {
    if (varListener_ != null) {
      varListener_.remove(_listener);
    }
  }

  @Override
  public final void saveIn(final CtuluArkSaver _writer, final ProgressionInterface _prog) {
    try {
      saveIn(_writer.getDb(), _prog);
    } catch (final IOException _evt) {
      FuLog.error(_evt);
    }
  }

  @Override
  public final void saveIn(final ObjectContainer _db, final ProgressionInterface _prog) {
    if (_db == null) {
      return;
    }
    _db.set(info_);
    if (tmpContainer_ != null) {
      final Query q = tmpContainer_.query();
      q.constrain(TrPostDataMinMaxGlobalItem.class);
      for (final ObjectSet s = q.execute(); s.hasNext(); ) {
        _db.set(s.next());
      }
    }
    TrPostUserVariableSaver.saveIn(this, _db, _prog);
    TrPostTimeModelSaver.save(this, _db);
  }

  @Override
  public final void setDefaultPaletteMinPalette(final H2dVariableType[] _v, final boolean[] _actived, final double[] _d) {
    for (int i = _v.length - 1; i >= 0; i--) {
      final H2dVariableType vi = _v[i];
      if (_actived[i]) {
        TrPreferences.TR.putBooleanProperty(getPrefNameSet(vi), true);
      } else {
        TrPreferences.TR.removeProperty(getPrefNameSet(vi));
      }
      TrPreferences.TR.putDoubleProperty(getPrefNameValue(vi), _d[i]);
    }
  }

  public final void setGravity(final double _d) {
    if (_d > 0) {
      gravite_.setValue(CtuluLib.getDouble(_d));
    }
  }

  @Override
  public void setId(String id) {
    id_ = id;
  }

  public final void setInspected(final boolean _inspected) {
    inspected_ = _inspected;
  }

  @Override
  public final boolean setTimeFormat(final CtuluNumberFormatI _fmt) {
    return time_.setTimeFormat(_fmt);
  }

  protected final void updateCache(final H2dVariableType _var) {
    TrPostDataCreated cr = null;
    if (isUserCreatedVar(_var)) {
      cr = (TrPostDataCreated) varUserCreateData_.get(_var);
    } else if (isCreatedVar(_var)) {
      cr = (TrPostDataCreated) varCreateData_.get(_var);
    }
    if (cr != null) {
      cr.clearCache();
    }
  }

  @Override
  public final void updateUserValue(final H2dVariableTypeCreated _old, final H2dVariableTypeCreated _new,
                                    final TrPostDataCreated _newData, final CtuluCommandContainer _cmd) {
    // on change la formule si necessaire, puis la variable
    if (!isUserCreatedVar(_old)) {
      FuLog.warning("TRP: var not created " + _old);
      return;
    }
    final TrPostDataCreated oldExpr = getUserCreatedVar(_old);
    final boolean isFormuleChanged = _newData != null && !_newData.equals(oldExpr);
    // pas de changement !
    if (!isFormuleChanged && (_new == _old)) {
      return;
    }
    if (isFormuleChanged) {
      varUserCreateData_.put(_old, _newData);
    }

    if (_new != _old) {
      if (FuLog.isDebug()) {
        FuLog.debug("TRP:" + getClass().getName() + " var changed: var name changed");
      }
      changeUserVar(_old, _new);
    }

    fireVariableChanged(_old, _new, isFormuleChanged, false, TrPostDataHelper.getAllVarDependingOn(_old,
        TrPostSourceAbstract.this));
    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          updateUserValue(_old, _new, _newData, null);
        }

        @Override
        public void undo() {
          updateUserValue(_new, _old, oldExpr, null);
        }
      });
    }
  }

  protected void updateVarList() {
    Collection<H2dVariableType> initVar = getInitVar();
    final Set r = initVar == null ? new HashSet() : new HashSet(initVar);
    if (FuLog.isDebug()) {
      FuLog.debug("nb var creee auto " + varCreateData_.size());
    }
    r.addAll(varCreateData_.keySet());
    if (varUserCreateData_ != null) {
      r.addAll(varUserCreateData_.keySet());
    }
    variable_ = (H2dVariableType[]) r.toArray(new H2dVariableType[r.size()]);
    FuSort.sort(variable_, FuComparator.STRING_COMPARATOR);
    fireVariableAdd(false);
    fireVarListModelChanged();
  }

  @Override
  public File getMainFile() {
    return getFiles().iterator().next();
  }

  @Override
  public List<TrPostSource> getUsedSources() {
    return Collections.emptyList();
  }
}
