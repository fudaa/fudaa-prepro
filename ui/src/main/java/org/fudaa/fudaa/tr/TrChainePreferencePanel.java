/**
 * @creation 26 mai 2004
 * @modification $Date: 2006-09-19 15:07:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.fudaa.tr.common.TrPreferences;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrChainePreferencePanel.java,v 1.11 2006-09-19 15:07:28 deniger Exp $
 */
public class TrChainePreferencePanel extends BuAbstractPreferencesPanel implements ItemListener {

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    setDirty(true);
  }

  @Override
  public void cancelPreferences() {
    cb_.setSelectedItem(TrLauncherDefault.getHydIdFromPrefFile());
  }

  @Override
  public boolean isPreferencesApplyable() {
    return false;
  }

  BuComboBox cb_;

  public static String getModelisationCategory() {
    return TrResource.getS("Syst�me de mod�lisation");
  }

  @Override
  public String getCategory() {
    return getModelisationCategory();
  }

  public TrChainePreferencePanel() {
    setLayout(new BuVerticalLayout(5));
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    final JPanel center = new BuPanel();
    final String shyd = TrLauncherDefault.getHydIdFromPrefFile();
    cb_ = new BuComboBox(TrLauncherDefault.getChoices());
    if (shyd == null) {
      setDirty(true);
    } else {
      cb_.setSelectedItem(shyd);
    }
    cb_.addItemListener(this);

    center.setLayout(new BuGridLayout(2, 5, 5));
    center.add(new BuLabel(TrResource.getS("Syst�me de mod�lisation")));
    center.add(cb_);
    final BuLabel l = new BuLabel();
    l.setText("<html>"
        + TrResource.getS("Les modifications seront appliqu�es<br> au red�marrage de toutes les applications")
        + "</html>");
    add(center);
    final BuPanel s = new BuPanel();
    s.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), CtuluResource.CTULU
        .getString("Avertissement")));
    s.add(l);
    add(s);

  }

  @Override
  public String getTitle() {
    return TrResource.getS("cha�ne de calcul");
  }

  @Override
  public void validatePreferences() {
    TrPreferences.TR.putStringProperty("tr.code.id", (String) cb_.getSelectedItem());
    TrPreferences.TR.writeIniFile();
    setDirty(false);
  }

  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }

  @Override
  public boolean isPreferencesValidable() {
    return true;
  }
}