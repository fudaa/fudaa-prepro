/**
 * @creation 25 mars 2004
 * @modification $Date: 2007-06-20 12:23:40 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.*;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliNodeDefault;
import org.fudaa.ebli.visuallibrary.SceneHelper;
import org.fudaa.ebli.visuallibrary.calque.CalqueLegendeWidgetAdapter;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetCreatorVueCalque;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreator;
import org.fudaa.ebli.visuallibrary.graphe.EbliWidgetControllerGraphe;
import org.fudaa.ebli.visuallibrary.graphe.EbliWidgetCreatorGraphe;
import org.fudaa.fudaa.commun.courbe.FudaaGrapheTimeAnimatedVisuPanel;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.actions.TrPostActionOpenSrc;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreationPanel;
import org.fudaa.fudaa.tr.post.data.TrPostDataListener;
import org.fudaa.fudaa.tr.post.persist.CommonProperties;
import org.fudaa.fudaa.tr.post.persist.TrPostPersistenceManager;
import org.fudaa.fudaa.tr.post.persist.TrPostSourceProjectedPersistReplay;
import org.fudaa.fudaa.tr.post.profile.MvProfileFillePanel;
import org.fudaa.fudaa.tr.post.save.TrPostProjetSaver;
import org.netbeans.api.visual.widget.Widget;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

/**
 * Cette classe fournit les modeles pour le post.
 *
 * @author Fred Deniger
 * @version $Id: TrPostProjet.java,v 1.51 2007-06-20 12:23:40 deniger Exp $
 */
public class TrPostProjet implements ActionListener {
  class ModifyObserver implements Observer {
    private boolean isModified_;

    protected void clearModified() {
      isModified_ = false;
      updateFrameState();
    }

    public boolean isModified() {
      return isModified_;
    }

    public void setModified() {
      isModified_ = true;
      updateFrameState();
    }

    @Override
    public void update(final Observable _o, final Object _arg) {
      setModified();
    }

    protected void updateFrameState() {
      if (impl_ != null) {
        changedMainFrameState();
      }
    }
  }

  /**
   * Observable custom reserv� aux modifs apport�es a la liste des src.
   *
   * @author Adrien Hadoux
   */
  class observableSupport extends Observable {
    @Override
    public void notifyObservers() {
      this.setChanged();
      super.notifyObservers();
    }

    @Override
    public void notifyObservers(final Object arg) {
      this.setChanged();
      super.notifyObservers(arg);
    }
  }

  /**
   * Manager de sauvegarder/charghement des donn�es
   */
  private TrPostPersistenceManager manager_;
  private final TrPostSourcesManager sources_ = new TrPostSourcesManager();
  /**
   * liste des noeuds pour gerer le cut/copy/paste avec en + le undo/redo
   */
  // liste des noeuds copies
  Set<EbliNode> nodesCopyied = null;
  // liste des noeuuds coupes
  Set<EbliNode> nodesCutted = null;
  /**
   * internalframe qui gere les multiProjets
   */
  public TrPostProjetsManagerFille filleProjetctManager_;
  /**
   * Le menu du post qui contient tout les sous menus des projets
   */
  BuMenu menuPost_;
  BuMenu menuLayout_ = new BuMenu(TrResource.getS("Layout"), "LAYOUTMANAGER");
  List<BuMenu> listeMenuProjets_;
  ;
  List<ArrayList<BuMenuItem>> listeSousMenuProjets_;
  private observableSupport observable;
  transient int idxFilleG_;
  public transient TrPostCommonImplementation impl_;
  ModifyObserver modifyState_ = new ModifyObserver();
  /**
   * liste des sources pour le multiProjet.
   */
  private BuDynamicMenu exportMenu;
  private CommonProperties commonProperties;

  public TrPostProjet(final TrPostCommonImplementation _impl) {
    this(null, _impl);
  }

  public TrPostProjet(final TrPostSource _src, final TrPostCommonImplementation _impl) {
    if (_src != null) {
      addSource(_src);
    }

    // -- creation de la frame de gestion multi projet --//
    filleProjetctManager_ = new TrPostProjetsManagerFille(this);
    impl_ = _impl;
  }

  /**
   * performed d action pour toutes les simulations charg�es. compl�xit� de l algo: lin�aire Cette methode est robuste en cas de suppression de
   * sources et donc de menus.
   */
  @Override
  public void actionPerformed(final ActionEvent _event) {
    final String commandeBrute = _event.getActionCommand();

    // i indique le numero de la simulation
    for (int i = 0; i < getlisteSousMenuProjets().size(); i++) {

      // --recuperation de la liste des sousmenu de la simulation i --//
      final ArrayList<BuMenuItem> listeSousMenus = getlisteSousMenuProjets().get(i);
      final TrPostSource source = getSource(i);
      // -- on recherche si l action provient d un des fils --//
      for (int j = 0; j < listeSousMenus.size(); j++) {
        final BuMenuItem item = listeSousMenus.get(j);

        // -- on essaie de matcher a la fois la commande et l objet pour
        // savoir si c est le bon --//
        // -- si une clause du if est v�rifi� alors on gere la
        // simulation i
        // --//
        final boolean isIt = item.equals(_event.getSource());
        if (isIt) {
          if ("COMPUTE_EXTREMA".equals(commandeBrute) && item.equals(_event.getSource())) {

            FuLog.debug("je fais partie de la simulation " + i);
            showExtremum(i);
          } else if ("PALETTE_DEFAULT".equals(commandeBrute)) {
            updatePaletteMin(source);
          } else if ("TIME_FORMAT".equals(commandeBrute)) {
            TrPostTimeFmtPanel.updateTimeStepFmt(source, getImpl().getFrame());
          } else if ("VARIABLES".equals(commandeBrute)) {
            TrPostDataCreationPanel.activeVarFrame(this, source, getImpl());
          } else if ("EXPORTDATA".equals(commandeBrute)) {
            final TrPostVisuPanel selected = getActiveOrSelectedVisuPanel(source);
            TrPostVisuPanel.startExport(getImpl(), source, selected == null ? null : selected.getCurrentSelection(),
                selected);
          } else if ("COMPARE".equals(commandeBrute)) {
            compareWith(source);
          } else if ("PROJECT".equals(commandeBrute)) {
            projectOn(source);
          }
        }
      }
    }
  }

  /**
   * @param _impl l'implementation parente
   */
  public void active(final TrPostCommonImplementation _impl) {
    impl_ = _impl;
    // pour mettre a jour le receveur d'evt
    impl_.getUndoCmdListener();

    modifyState_.updateFrameState();
    if (isModified()) {
      _impl.setMainMessageAndClear(TrResource.getS("Le fichier des r�sultats a �t� modifi�: la base de donn�es a �t� mise � jour"));
    }

    final Runnable r = new Runnable() {
      @Override
      public void run() {

        // -- si il y a deja une source de pr�charg�e --//
        if (sources_.isNotEmpty()) {
          final TrPostLayoutFille filleLayout = new TrPostLayoutFille(TrPostProjet.this);
          // -- creation d une instance de la legende du calque --//
          final CalqueLegendeWidgetAdapter legendeCalque = new CalqueLegendeWidgetAdapter(filleLayout.getScene());

          final TrPostSource source = sources_.getSource(0);
          final TrPostVisuPanel pnVisu = source.buildVisuPanel(TrPostProjet.this, legendeCalque);

          source.fillWithSourceCreationInfo(CtuluLibString.EMPTY_STRING, pnVisu.getInfosCreation());

          filleLayout.addCalque(TrResource.getS("Calque {0}", Integer.toString((filleLayout.getScene().getAllVue2d().size() + 1))), null, null, pnVisu, legendeCalque);

          // -- decoration de la fille layout --//
          filleLayout.setFrameIcon(EbliResource.EBLI.getToolIcon("lissage"));

          filleLayout.setClosable(true);
          filleLayout.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
          filleLayout.getCmdMng().clean();

          // --ajout dans le post de la frame --//
          impl_.addInternalFrame(filleLayout);
        }

        // -- creation du menu POST de base --//
        setMenuPost();
        changedMainFrameState();
        addMenuExporter();
      }
    };
    EventQueue.invokeLater(r);
  }

  @SuppressWarnings("serial")
  private void addMenuExporter() {
    final JMenu menu = impl_.getMainMenuBar().getMenu("EXPORTER");
    exportMenu = new BuDynamicMenu(TrResource.getS("Exporter les r�sultats"), "EXPORT_SOURCES", TrResource.TR.getToolIcon("crystal_exporter")) {
      @Override
      protected boolean isActive() {
        return true;
      }

      @Override
      protected void build() {
        super.removeAll();
        final TrPostSourcesManager sources = getSources();
        final List<TrPostSource> allClassicalSource = sources.getAllClassicalSource();
        for (final TrPostSource source : allClassicalSource) {
          add(source.getTitle()).addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
              final TrPostVisuPanel selected = getActiveOrSelectedVisuPanel(source);
              TrPostVisuPanel.startExport(getImpl(), source, selected == null ? null : selected.getCurrentSelection(),
                  selected);
            }
          });
        }
      }
    };
    menu.add(exportMenu);
  }

  /**
   * Methode appelee apres ouverture du fichier de resultats.
   *
   * @param _destGrid the destination grid
   * @param _prog
   * @param _isInitGridDisplay
   */
  protected void activeProjectionAction(final TrPostSource _src, final EfGridInterface _destGrid,
                                        final ProgressionInterface _prog, final boolean _isInitGridDisplay, final String destGridId) {
    final CtuluAnalyze ana = new CtuluAnalyze();
    _destGrid.computeBord(_prog, ana);
    getImpl().manageAnalyzeAndIsFatal(ana);
    final TrPostSourceProjectedPersistReplay replay = new TrPostSourceProjectedPersistReplay(_src.getId(), destGridId,
        false);
    final TrPostSourceProjected projection = new TrPostSourceProjected(_src, _destGrid, null, false, getImpl(), replay);
    projection.openDatas(_prog, null, getImpl());
    projection.buildDefaultVectors();

    // creation du panel de visualisation du posttraitement
    final TrPostVisuPanel pnVisu = TrPostVisuPanel.buildVisuPanelForWidgets(this, impl_, projection);
    impl_.display(pnVisu, pnVisu.getIsoLayer(), TrResource.getS("Projection") + " " + getSources().getShortNameOfSource(_src), false);
  }

  /**
   * ajout obligatoire d un graphe.
   *
   * @param pn
   * @param calque
   */
  private void addEbliNode(final EGFillePanel pn, final TrPostVisuPanel calque) {
    if (calque != null) {
      addGrapheNodeInCurrentScene(pn, formatInfoSource(calque.getSource()), null);
    } else {
      addGrapheNodeInCurrentScene(pn, "profil spatial", null);
    }
  }

  /**
   * refonte de addFille qui ajoute des widgets
   */
  public void addFille(final TrPostCourbeTreeModel _model, final String _titre, final ProgressionInterface _prog,
                       final TrPostVisuPanel calque) {
    final EGGraphe graphe = new EGGraphe(_model);
    graphe.setXAxe(EGAxeHorizontal.buildDefautTimeAxe(calque.getSource().getTimeFormatter()));
    graphe.getModel().getAxeX().setUnite("s");
    graphe.setCmd(new CtuluCommandManager());
    addEbliNode(new FudaaGrapheTimeAnimatedVisuPanel(graphe), calque);
  }

  public void addGrapheNodeInCurrentScene(final EGFillePanel _pn, final String _desc, final TrPostLayoutFille inLayout) {
    EGGrapheModel model = _pn.getGraphe().getModel();
    final EbliNode nodeG = new EbliNodeDefault();

    final EbliWidgetCreatorGraphe creator = new EbliWidgetCreatorGraphe(_pn);
    nodeG.setCreator(creator);
    if (_desc == null) {
      nodeG.setTitle("Graphe " + idxFilleG_++);
    } else {
      nodeG.setTitle("Graphe " + (idxFilleG_++) + ":" + _desc);
    }

    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {

        // Ajout du node dans la scene
        TrPostLayoutFille currentLayoutFilleOrFirst = inLayout;
        if (currentLayoutFilleOrFirst == null) {
          currentLayoutFilleOrFirst = getImpl().getCurrentLayoutFilleOrFirst();
        }
        creator.setPreferredSize(new Dimension(600, 400));
        creator.setPreferredLocation(SceneHelper.getOptimalePosition(currentLayoutFilleOrFirst.getScene()));
        final Widget addNode = currentLayoutFilleOrFirst.addNode(nodeG);
        _pn.getGraphe().restore();
        if (_desc != null) {
          nodeG.setDescription(_desc);
        }
        currentLayoutFilleOrFirst.getScene().setSelectedObjects(Collections.emptySet());

        // -- ajout syst�matique de la legende associee --//
        final EbliWidgetControllerGraphe controller = (EbliWidgetControllerGraphe) nodeG.getWidget().getIntern().getController();
        controller.ajoutLegende();
        currentLayoutFilleOrFirst.getScene().setSelectedObjects(new HashSet(Arrays.asList(nodeG)));
      }
    });
  }

  /**
   * Permet d ajouter une source dans le projet afin de gerer le multi source. On doit toujorus passer par cette methode pour ajouter une source.
   *
   * @param _src
   */
  public void addSource(final TrPostSource _src) {
    addSource(_src, null);
  }

  Map<TrPostSource, TrPostDataListenerDispatcher> variableListener = new HashMap<TrPostSource, TrPostDataListenerDispatcher>();
  Map<TrPostSource, TrPostTimeContentListenerDispatcher> timeListener = new HashMap<TrPostSource, TrPostTimeContentListenerDispatcher>();

  public void registerListenerTo(final TrPostSource src, final TrPostDataListener listener) {
    final TrPostDataListenerDispatcher dispatcher = variableListener.get(src);
    if (dispatcher != null) {
      dispatcher.addWeakListener(listener);
    }
  }

  public void unregisterListenerTo(final TrPostSource src, final TrPostDataListener listener) {
    final TrPostDataListenerDispatcher dispatcher = variableListener.get(src);
    if (dispatcher != null) {
      dispatcher.removeWeakListener(listener);
    }
  }

  public void registerTimeListenerTo(final TrPostSource src, final ListDataListener listener) {
    final TrPostTimeContentListenerDispatcher dispatcher = timeListener.get(src);
    if (dispatcher != null) {
      dispatcher.addWeakListener(listener);
    }
  }

  public void unregisterTimeListenerTo(final TrPostSource src, final ListDataListener listener) {
    final TrPostTimeContentListenerDispatcher dispatcher = timeListener.get(src);
    if (dispatcher != null) {
      dispatcher.removeWeakListener(listener);
    }
  }

  public void addSource(final TrPostSource _src, final String title) {
    final TrPostDataListenerDispatcher listener = new TrPostDataListenerDispatcher(this);
    TrPostTimeContentListenerDispatcher value = new TrPostTimeContentListenerDispatcher(this);
    _src.addVariableListener(listener);
    _src.getTimeListModel().addListDataListener(value);
    variableListener.put(_src, listener);
    timeListener.put(_src, value);
    _src.addVariableListener(listener);
    if (_src.getFiles() == null || !sources_.isOneSourceLoaded(_src.getFiles())) {
      sources_.addSource(_src);
      _src.buildDefaultVarUpdateLists();

      // -- ajout du menu correspondant uniquement a partir de la 2eme
      // --//
      if (!EventQueue.isDispatchThread()) {
        EventQueue.invokeLater(new Runnable() {
          @Override
          public void run() {
            ajouteMenu(_src, title);
          }
        });
      } else {
        ajouteMenu(_src, title);
      }
    } else {
      // -- messqge d erreur: le fichier est deja ouvert --//
      new BuDialogMessage(impl_.getApp(), impl_.getInformationsSoftware(), "Le fichier est deja ouvert.").activate();
    }
    // -- notify aux observers --//
    this.notifyObservers();
  }

  private void ajouteMenu(final TrPostSource _src, final String title) {
    construitMenuPostSpecifiqueSource(_src, title);
  }

  public void changedMainFrameState() {
  }

  /**
   * Ferme le projet.
   */
  public final void close() {
    sources_.closeAll();
    // src_.close();
    final Runnable runnable = new Runnable() {
      @Override
      public void run() {
        final JMenu menu = impl_.getMainMenuBar().getMenu("Post");
        if (menu != null) {
          impl_.getMainMenuBar().remove(menu);
          // menu.removeAll();
        }

        final JMenu menuExporter = impl_.getMainMenuBar().getMenu("EXPORTER");
        if (menuExporter != null && exportMenu != null) {
          menuExporter.remove(exportMenu);
        }
        final JInternalFrame[] frames = impl_.getAllInternalFrames();
        if (frames != null) {
          impl_.removeInternalFrames(frames);
        }
        impl_.getFrame().setTitle(impl_.getInformationsSoftware().name);
      }
    };
    BuLib.invokeNow(runnable);
  }

  /**
   * Ouvre une interface permettant de comparer 2 source entre elles.
   */
  public void compareWith(final TrPostSource sourceSelectionnee) {
    final CtuluTaskDelegate task = getImpl().createTask(TrPostSourceComparatorBuilder.getComparaisonTitle());
    // final ProgressionInterface prog = task.getStateReceiver();
    final TrPostSource toProject = getChooserMultiSources(-1);
    if (toProject != null) {
      final Runnable r = new Runnable() {
        @Override
        public void run() {
          TrPostSourceComparatorBuilder.activeComparaison(sourceSelectionnee, toProject, EfLib.isDiffQuick(
              sourceSelectionnee.getGrid(), toProject.getGrid()), TrPostProjet.this);
        }
      };
      task.start(r);
    }
  }

  public void projectOn(final TrPostSource sourceSelectionnee) {
    final TrPostSource toProject = getChooserMultiSources(-1);
    if (toProject != null) {
      final CtuluTaskDelegate task = getImpl().createTask(TrPostSourceComparatorBuilder.getComparaisonTitle());
      final Runnable r = new Runnable() {
        @Override
        public void run() {
          activeProjectionAction(sourceSelectionnee, toProject.getGrid(), task.getMainStateReceiver(), false, toProject.getId());
        }
      };
      task.start(r);
    }
  }

  /**
   * Construit un menu specifique a la source choisie. A chaque fois que l on ajoute une source au projet, il faut ajouter un menu sp�cifique.
   *
   * @param _src
   */
  public void construitMenuPostSpecifiqueSource(final TrPostSource _src, final String title) {

    BuMenu menuSimul = null;
    final int nb = sources_.getSrcSize();
    if (title == null) {
      menuSimul = new BuMenu((nb) + ". " + TrLib.formatFichier(_src.getMainFile()), "SIMULATION" + (nb));
    } else {
      menuSimul = new BuMenu((nb) + ". " + _src.getTitle(), "SIMULATION" + (nb));
    }
    // -- ajout du menu au menu post --//
    getMenuPost().add(menuSimul);

    // -- sauvegarde d une trace du menu dans la liste pour une suppression
    // ulterieure --//
    getlisteMenuProjets().add(menuSimul);

    final ArrayList<BuMenuItem> listSousMenus = getSousMenusSpecifiqueSource(_src, menuSimul);
    // -- ajout de la sous liste de menus pour la garder en memoire --//
    getlisteSousMenuProjets().add(listSousMenus);
  }

  /**
   * Cree une nouvelle fenetre dans le post contenant son jeu de layouts
   */
  public TrPostLayoutFille createNewLayoutFrame() {

    final int indiceNbframes = impl_.getAllLayoutFille().size() + 1;

    final TrPostLayoutFille newLayoutFille = new TrPostLayoutFille(this);
    newLayoutFille.setTitle(newLayoutFille.getTitle() + " N� " + indiceNbframes);

    // addFillesLayout(newLayoutFille);
    impl_.addInternalFrame(newLayoutFille);

    return newLayoutFille;
  }

  /**
   * genere uniquement l id.
   *
   * @param name
   */
  public String deliverSourceId(final String name) {
    final String uniqueId = CtuluLibGenerator.getInstance().deliverUniqueStringId(name);

    return uniqueId;
  }

  /**
   * Genere et affecte a la source l'id.
   *
   * @param src
   */
  public String deliverSourceId(final TrPostSource src) {
    if (src.getId() == null) {
      final String uniqueId = CtuluLibGenerator.getInstance().deliverUniqueStringId();
      src.setId(uniqueId);
    }
    return src.getId();
  }

  /**
   * methode qui formatte les infos de la source
   *
   * @param _src
   */
  public String formatInfoSource(final TrPostSource _src) {
    return _src.getFormatedTitle();
  }

  /**
   * Methode qui formatte les donnees de la liste.
   */
  public String[] getListSources() {
    return this.getListSources(false);
  }

  public String[] getListSources(final boolean proposeNewDAtaFirst) {
    String[] listeSimul;
    int cpt = 0;
    final int nb = sources_.getSrcSize();
    if (proposeNewDAtaFirst) {
      listeSimul = new String[nb + 1];
      listeSimul[cpt++] = TrResource.getS("Ajouter un nouveau projet");
    } else {
      listeSimul = new String[nb];
    }

    for (int i = 0; i < nb; i++) {
      listeSimul[cpt++] = formatInfoSource(sources_.getSource(i));
    }

    return listeSimul;
  }

  /**
   * Retourne toutes les sources qui sont des composites
   */
  public List<TrPostSourceFromReader> getAllCCompositeSource() {
    return sources_.getAllCCompositeSource();
  }

  /**
   * Retourne toutes les sources qui ne sont pas des composites
   */
  public List<TrPostSource> getAllClassicalSource() {
    return sources_.getAllClassicalSource();
  }

  /**
   * @param src
   * @return the first selected or active TrPostVisuPanel containing this src.
   */
  public TrPostVisuPanel getActiveOrSelectedVisuPanel(final TrPostSource src) {
    if (src == null) {
      return null;
    }
    final List<TrPostLayoutFille> allLayoutFille = getImpl().getAllLayoutFille();
    final TrPostLayoutFille currentLayoutFille = getImpl().getCurrentLayoutFille();
    TrPostVisuPanel res = getSelectedVisuPanelFor(src, currentLayoutFille);
    if (res != null) {
      return res;
    }
    for (final TrPostLayoutFille trPostLayoutFille : allLayoutFille) {
      res = getSelectedVisuPanelFor(src, trPostLayoutFille);
      if (res != null) {
        return res;
      }
    }
    return res;
  }

  private TrPostVisuPanel getSelectedVisuPanelFor(final TrPostSource src, final TrPostLayoutFille in) {
    if (in == null || src == null) {
      return null;
    }
    Collection<EbliNode> selectedNodes = in.getScene().getSelectedNodes();
    for (final EbliNode ebliNode : selectedNodes) {
      final EbliWidgetCreator creator = ebliNode.getCreator();
      if (creator instanceof EbliWidgetCreatorVueCalque
          && (((EbliWidgetCreatorVueCalque) creator).getCalquesPanel() instanceof TrPostVisuPanel)
          && ((TrPostVisuPanel) ((EbliWidgetCreatorVueCalque) creator).getCalquesPanel()).getSource().equals(src)) {
        return (TrPostVisuPanel) ((EbliWidgetCreatorVueCalque) creator).getCalquesPanel();
      }
    }
    selectedNodes = in.getScene().getNodes();
    for (final EbliNode ebliNode : selectedNodes) {
      final EbliWidgetCreator creator = ebliNode.getCreator();
      if (creator instanceof EbliWidgetCreatorVueCalque
          && (((EbliWidgetCreatorVueCalque) creator).getCalquesPanel() instanceof TrPostVisuPanel)
          && ((TrPostVisuPanel) ((EbliWidgetCreatorVueCalque) creator).getCalquesPanel()).getSource().equals(src)) {
        return (TrPostVisuPanel) ((EbliWidgetCreatorVueCalque) creator).getCalquesPanel();
      }
    }
    return null;
  }

  public TrPostSource openSource() {
    return getChooserMultiSources(-1);
  }

  /**
   * retourne un chooser avec tous les multi sources et un bouton pour ajouter un nouveau projet.
   */
  public TrPostSource getChooserMultiSources(final int indiceChoix) {

    BuDialogChoice dialog = null;
    // -- recuperation de la liste des src sous forme de liste --//
    final String[] values = getListSources(true);

    // --construction graphique --//
    dialog = new BuDialogChoice(impl_.getApp(), impl_.getInformationsSoftware(),
        TrResource.getS("S�lection du projet"), TrResource.getS("S�lectionnez le jeux de donn�es "), values);
    dialog.setSize(400, 250);
    // -- on indice au choix +1 car on a le choix creation au debut --//
    if (indiceChoix != -1) {
      dialog.getChValeur().setSelectedIndex(indiceChoix + 1);
    }
    dialog.setModal(true);
    if (dialog.activate() == 0) {
      final int reponse = dialog.getSelectedIndex();

      if (reponse == -1) {
        impl_.error(TrResource.getS("Il faut choisir un projet"));
      } else if (reponse == 0) {
        // -- ouverture d un nouveau projet --//
        final TrPostSource openSource = new TrPostActionOpenSrc(this).openSource();
        // -- on retourne le dernier projet ouvert --//
        // return getSource(listeSrc_.size() - 1);
        if (openSource != null) {
          impl_.message(TrResource.getS("Projet charg�, il est d�sormais disponible"));
          return getChooserMultiSources(sources_.getSrcSize() - 1);
        } else {
          return getChooserMultiSources(-1);
        }
      } else {
        return getSource(reponse - 1);
      }
    }

    return null;
  }

  public final TrPostCommonImplementation getImpl() {
    return impl_;
  }

  public List<BuMenu> getlisteMenuProjets() {
    if (listeMenuProjets_ == null) {
      listeMenuProjets_ = new ArrayList<BuMenu>();
    }
    return listeMenuProjets_;
  }

  public List<ArrayList<BuMenuItem>> getlisteSousMenuProjets() {
    if (listeSousMenuProjets_ == null) {
      listeSousMenuProjets_ = new ArrayList<ArrayList<BuMenuItem>>();
    }
    return listeSousMenuProjets_;
  }

  public TrPostPersistenceManager getManager() {
    if (manager_ == null) {
      manager_ = new TrPostPersistenceManager(this);
    }

    return manager_;
  }

  public BuMenu getMenuPost() {
    if (menuPost_ == null) {
      menuPost_ = new BuMenu(TrResource.getS("Post"), "POST");
    }
    return menuPost_;
  }

  public observableSupport getObservable() {
    if (observable == null) {
      observable = new observableSupport();
    }
    return observable;
  }

  protected Runnable getRunnableForVisu(final Runnable _other, final TrPostLayoutFille _fille) {
    return new Runnable() {
      @Override
      public void run() {

        // -- ajout de la frame layout
        impl_.addInternalFrame(_fille);

        if (_other != null) {
          _other.run();
        }
        _fille.getVisuPanel().getArbreCalqueModel().getObservable().addObserver(modifyState_);
      }
    };
  }

  /**
   * retourne la source de la liste des sources
   *
   * @param i
   */
  public TrPostSource getSource(final int i) {
    return sources_.getSource(i);
  }

  public TrPostSourcesManager getSources() {
    return sources_;
  }

  /**
   * Cree la liste des menu items sp�cifiques au fichier source charg�.
   *
   * @param _src
   * @param menuSimul
   */
  public ArrayList<BuMenuItem> getSousMenusSpecifiqueSource(final TrPostSource _src, final BuMenu menuSimul) {
    final ArrayList<BuMenuItem> listSousMenus = new ArrayList<BuMenuItem>();

    final BuIcon ic = BuResource.BU.getIcon("aucun");

    listSousMenus.add(menuSimul.addMenuItem(TrResource.getS("Extrema..."), "COMPUTE_EXTREMA", ic, TrPostProjet.this));
    final BuMenuItem item = new BuMenuItem();

    listSousMenus.add(menuSimul.addMenuItem(CtuluLib.getS("Editer les variables..."), "VARIABLES", ic,
        TrPostProjet.this));
    menuSimul.addSeparator();
    listSousMenus.add(menuSimul.addMenuItem(TrResource.getS("Palettes de couleurs"), "PALETTE_DEFAULT", ic,
        TrPostProjet.this));
    listSousMenus.add(menuSimul.addMenuItem(TrResource.getS("Formater/modifier les pas de temps..."), "TIME_FORMAT",
        ic, TrPostProjet.this));

    menuSimul.addSeparator();

    listSousMenus.add(menuSimul
        .addMenuItem(new TrPostBuilderSuiteCalcul.ActionBuildSuite(_src, TrResource.getS("Suite de calcul"), EbliResource.EBLI.getIcon(""), "SUITECALCUL", TrPostProjet.this)));
    listSousMenus.add(menuSimul.addMenuItem(TrResource.getS("Comparer..."), "COMPARE", ic, TrPostProjet.this));
    listSousMenus.get(listSousMenus.size() - 1).setToolTipText(
        TrResource.getS("Comparer les r�sultats avec ceux d'un autre projet"));
    listSousMenus.add(menuSimul.addMenuItem(TrResource.getS("Projeter..."), "PROJECT", ic, TrPostProjet.this));
    listSousMenus.get(listSousMenus.size() - 1).setToolTipText(
        TrResource.getS("Projeter les r�sultats sur un maillage diff�rent"));

    menuSimul.addSeparator();

    listSousMenus.add(menuSimul.addMenuItem(TrResource.getS("Exporter"), "EXPORTDATA", TrPostProjet.this));
    _src.addSpecificItemInMainMenu(menuSimul, getImpl());
    return listSousMenus;
  }

  public boolean isModified() {
    return modifyState_.isModified();
  }

  public void notifyObservers() {
    // getObservable().setChanged();

    getObservable().notifyObservers();
  }

  boolean openSrcDataAndIsModified(final CtuluUI _ui, final ProgressionInterface _prog) {
    return new TrPostProjetSaver(this).openSrcDataAndIsModified(_ui, _prog);
  }

  /**
   * creation de courbe spatiale
   */
  public void profilPanelCreated(final MvProfileFillePanel _panel, final ProgressionInterface _prog, final String _title) {
    addEbliNode(_panel, null);
  }

  public void removeAllSources() {
    final int nb = sources_.getSrcSize();
    for (int i = 0; i < nb; i++) {
      removeSource(sources_.getSource(i));
    }
  }

  /**
   * Methode de suppression de la source
   *
   * @param src : TrPostSource, retourne true si la suppression a bien ete effectuee.
   */
  public boolean removeSource(final TrPostSource src) {

    if (sources_.isOneSourceLoaded(src.getFiles())) {
      final int n = sources_.remove(src);
      // --recuperation du menu simul --//
      final BuMenu menuSimul = getlisteMenuProjets().get(n);

      // -- on retire le menuSimul de la liste --//
      menuPost_.remove(menuSimul);

      // -- on retire de la liste des menus le menusimul et la liste des
      // sous
      // menus --//
      getlisteMenuProjets().remove(n);
      getlisteSousMenuProjets().remove(n);

      // -- mise a jour de la barre des menus --//
      impl_.getMainMenuBar().revalidate();

      // -- on met a jour la scene en enlevant tous les objets reli�s �
      // cette source --//
      for (final TrPostLayoutFille frame : impl_.getAllLayoutFille()) {
        frame.getScene().removeAllWidgetLinkedToSrc(src);
      }
      final TrPostDataListenerDispatcher removeDataListener = variableListener.remove(src);
      if (removeDataListener != null) {
        removeDataListener.clear();
      }
      final TrPostTimeContentListenerDispatcher timeDataListener = timeListener.remove(src);
      if (timeDataListener != null) {
        timeDataListener.clear();
      }

      return true;
    } else {
      // -- messqge d erreur: le fichier est deja ouvert --//
      new BuDialogMessage(impl_.getApp(), impl_.getInformationsSoftware(),
          "Impossible de supprimer ce fichier de la liste.").activate();

      return false;
    }
  }

  public void save(final CtuluUI _ui, final ProgressionInterface _prog) {
    new TrPostProjetSaver(this).save(_ui, _prog);
  }

  /**
   * Creation par defaut du menu post de l interface. Cette methdoe est appelee une seule dfois au moment de la creation de l impl.
   */
  public void setMenuPost() {
    final BuMenuBar menubar = impl_.getMainMenuBar();
    Component comp = null;
    if (getMenuPost().getMenuComponentCount() > 0) {
      comp = getMenuPost().getMenuComponent(0);
      // getMenuPost().remove(0);
    }

    getMenuPost().setIcon(null);
    getMenuPost().setName("mnPost");

    menubar.add(getMenuPost(), 2);

    getMenuPost().addSeparator(TrResource.getS("Layout"));
    getMenuPost().addMenuItem(TrResource.getS(("Ajouter une nouvelle fen�tre")), "AJOUTFRAME",
        CtuluResource.CTULU.getIcon("crystal_ajouter"), new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent _e) {
            createNewLayoutFrame();
          }
        });
    getMenuPost().addMenuItem(TrResource.getS(("Fusionner avec un autre projet POST")), "FUSIONPOST",
        CtuluResource.CTULU.getIcon("crystal_ajouter"), new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent _e) {
            final boolean ok = CtuluLibDialog.showConfirmation(
                getImpl().getFrame(),
                TrResource.getS("Fusionner avec un autre projet POST"),
                TrResource.getS("Le projet courant va �tre ferm�. \n Voulez vous sauvegarder le projet courant avant sa fermeture?"),
                TrResource.getS("Sauvegarder et continuer"), TrResource.getS("Annuler"));
            if (!ok) {
              return;
            }
            getManager().saveProject(false);
            getManager().loadProject(false, null, false);
            // -- on reinitialise le nom path du projet a null pour
            // qu'il soit redemand� lors de la nouvelle sauvegarde
            // --//
            getManager().setProjet(null);
          }
        });

    getMenuPost().addSeparator(TrResource.getS("Actions"));
    // -- ajout de l action de gestion des simus dans post --//
    final BuMenuItem item = new BuMenuItem(TrResource.getS("Gestion Multi-Sources"));
    item.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        // -- affichage de la fenetre de gestion multi projet --//

        if (!filleProjetctManager_.isVisible()) {
          impl_.addInternalFrame(filleProjetctManager_);
        } else {
          filleProjetctManager_.moveToFront();
        }
      }
    });
    getMenuPost().add(item);
    getMenuPost().addSeparator(TrResource.getS("Liste des Fichiers r�sultats"));
    // -- init de la liste des menus --//

    // -- construction par defaut du premier menu de simulation --//
    // construitMenuPostSpecifiqueSource(getSource(0));

    if (comp != null) {
      getMenuPost().add(comp);
    }
  }

  protected void setProjectModified() {
    if (modifyState_ != null) {
      modifyState_.setModified();
    }
  }

  public void setProjectNotModified() {
    if (modifyState_ != null) {
      modifyState_.clearModified();
    }
  }

  /**
   * calcul les extrema en fonction de la source proposee
   *
   * @param indiceSource_
   */
  private void showExtremum(final int indiceSource_) {
    final JTable table = TrPostMinMaxTableModel.createTableFor(getSource(indiceSource_));
    final TrPostMinMaxTableModel model = (TrPostMinMaxTableModel) table.getModel();
    final CtuluDialogPanel panel = new CtuluDialogPanel();
    final CtuluDialog dial = new CtuluDialog(getImpl().getFrame(), panel);
    final BuButton btCalcul = new BuButton(TrResource.getS("Calcul des extrema"));
    dial.setTitle(btCalcul.getText());
    btCalcul.setActionCommand("EXTREMUM");
    panel.setLayout(new BuHorizontalLayout(3, false, false));
    final BuPanel center = new BuPanel();
    center.setLayout(new BuBorderLayout(2, 2, true, true));
    final BuScrollPane scroll = new BuScrollPane(table);
    scroll.setPreferredHeight((int) (table.getPreferredSize().height * 1.3));
    center.add(scroll, BuBorderLayout.CENTER);
    center.add(model.createInfoPanel(table), BuBorderLayout.SOUTH);
    panel.add(center);
    btCalcul.setAlignmentX(1f);
    btCalcul.setAlignmentY(0f);
    panel.add(btCalcul);

    btCalcul.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _event) {
        final BuGlassPaneStop stop = new BuGlassPaneStop();
        dial.setGlassPane(stop);
        stop.setVisible(true);
        new CtuluTaskOperationGUI(impl_, CtuluLibString.EMPTY_STRING) {
          @Override
          public void act() {
            model.apply(new ProgressionBuAdapter(this), impl_);
            BuLib.invokeLater(new Runnable() {
              @Override
              public void run() {
                TrPostProjet.this.setProjectModified();
                stop.setVisible(false);
                dial.getRootPane().remove(stop);
              }
            });
          }
        }.start();
      }
    });
    dial.afficheDialogModal();
  }

  protected void updateComponents() {
  }

  private void updatePaletteMin(final TrPostSource _src) {
    final TrPostMinPaletteTableModel model = _src.getMinPaletteModel();
    final BuTable table = new BuTable(model);
    final CtuluDialogPanel pnDial = new CtuluDialogPanel() {
      @Override
      public boolean apply() {
        if (table.isEditing()) {
          table.getCellEditor().stopCellEditing();
        }
        model.apply();
        return true;
      }
    };

    final TableColumn col0 = table.getColumnModel().getColumn(0);
    col0.setMaxWidth(20);
    col0.setCellRenderer(new CtuluCellBooleanRenderer());
    pnDial.setLayout(new BuVerticalLayout(2, true, true));
    pnDial.addLabel(TrResource.getS("Activer et pr�ciser les valeurs minimales � utiliser par d�faut pour les palettes"));
    pnDial.add(new BuScrollPane(table));
    pnDial.afficheModale(impl_.getFrame(), TrResource.getS("Valeurs par d�faut pour les palettes"));
  }

  public CommonProperties getCommonProperties() {
    return commonProperties;
  }

  public void setCommonProperties(CommonProperties commonProperties) {
    this.commonProperties = commonProperties;
  }
}
