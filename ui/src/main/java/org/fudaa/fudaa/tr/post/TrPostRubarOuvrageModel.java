/*
 *  @creation     29 mars 2005
 *  @modification $Date: 2006-09-19 15:07:27 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import gnu.trove.TIntHashSet;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.dodico.h2d.rubar.H2dRubarGridAreteSource;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrage;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageMng;
import org.fudaa.fudaa.tr.data.TrInfoSenderDelegate;
import org.fudaa.fudaa.tr.rubar.TrRubarOuvrageLayerModel;

/**
 * @author Fred Deniger
 * @version $Id: TrPostRubarOuvrageModel.java,v 1.6 2006-09-19 15:07:27 deniger Exp $
 */
public class TrPostRubarOuvrageModel extends TrRubarOuvrageLayerModel {

  /**
   * @param _grid le maillage
   * @param _ouvrages les ouvrages
   * @param _sender l'envoyeur d'infos.
   */
  public TrPostRubarOuvrageModel(final H2dRubarGridAreteSource _grid, final H2dRubarOuvrageMng _ouvrages,
      final TrInfoSenderDelegate _sender) {
    super(_grid, _ouvrages, _sender);
  }

  @Override
  public H2dRubarOuvrage addOuvrage(final int[] _eltIntern, final int _elt1, final int _ar1, final int _elt2,
      final int _ar2, final CtuluCommandContainer _cmd) {
    return null;
  }

  protected void setInfoDelegate(final TrInfoSenderDelegate _d) {
    sender_ = _d;
  }

  public int[] getSelectedAreteGlobalIdx(final int[] _idxSelectedOuvrages) {
    if (_idxSelectedOuvrages == null) { return null; }
    // en gros: 4 aretes par poissons
    final TIntHashSet selectedArete = new TIntHashSet(_idxSelectedOuvrages.length * 4);
    for (int i = _idxSelectedOuvrages.length - 1; i >= 0; i--) {
      final H2dRubarOuvrage o = getOuvrage(_idxSelectedOuvrages[i]);
      selectedArete.add(o.getArete1());
      if (o.isElt2Set()) {
        selectedArete.add(o.getArete2());
      }
    }
    final int[] idxArete = selectedArete.toArray();
    Arrays.sort(idxArete);
    return idxArete;
  }

}
