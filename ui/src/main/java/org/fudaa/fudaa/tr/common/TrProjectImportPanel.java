/*
 * @creation 8 sept. 06
 * @modification $Date: 2006-09-19 15:07:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuVerticalLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.ebli.calque.BCalquePersistenceGroupe;
import org.fudaa.fudaa.commun.save.FudaaSaveLib;
import org.fudaa.fudaa.commun.save.FudaaSaveZipLoader;
import org.fudaa.fudaa.sig.FSigLib;

/**
 * @author fred deniger
 * @version $Id: TrProjectImportPanel.java,v 1.2 2006-09-19 15:07:30 deniger Exp $
 */
public class TrProjectImportPanel extends CtuluDialogPanel implements DocumentListener {

  final CtuluFileChooserPanel pn_;
  final BuCheckBox cbNoGeom_;
  final BuCheckBox cbNoNewCalque_;

  public TrProjectImportPanel() {
    setLayout(new BuVerticalLayout(5));
    final String title = FSigLib.getS("Le fichier projet");
    pn_ = new CtuluFileChooserPanel(title);
    add(CtuluFileChooserPanel.buildPanel(pn_, title));
    pn_.setAllFileFilter(false);
    pn_.setFileSelectMode(JFileChooser.FILES_ONLY);
    pn_.setWriteMode(false);
    pn_.setFilter(new FileFilter[] { new FileFilter() {
      @Override
      public String getDescription() {
        return FSigLib.getS("Fichier projet" + CtuluLibString.ESPACE + TrProjectPersistence.getPostProjectExt(),
            TrProjectPersistence.getPreProjectExt());
      }

      @Override
      public boolean accept(final File _f) {
        return _f.isDirectory() || isAcceptable(_f);
      }
    } });
    cbNoGeom_ = new BuCheckBox(FSigLib.getS("Ne pas importer les donn�es g�ographiques")); //Don't import geographic datas
    cbNoNewCalque_ = new BuCheckBox(FSigLib.getS("Ne pas ajouter de nouveaux calques")); //Don't add new layers
    add(cbNoNewCalque_);
    add(cbNoGeom_);
    cbNoNewCalque_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        cbNoGeom_.setEnabled(!cbNoNewCalque_.isSelected());
      }
    });
    cbNoGeom_.setToolTipText(TrResource
        .getS("Si s�lectionn�, aucun calque ce sera ajout� dans le groupe \"Donn�es geog.\".")); //If selected, no tracing will be added in the group geog data bank	
    cbNoNewCalque_.setToolTipText(TrResource.getS("Si s�lectionn�, aucun calque ne sera ajout� � la vue en cours.")); //If selected, no tracing will be added in the group geog data bank	
    pn_.getTf().getDocument().addDocumentListener(this);

  }

  @Override
  public boolean isDataValid() {
    final File f = pn_.getFile();
    if (!isAcceptable(f)) {
      if (f == null || !f.exists()) {
        setErrorText(CtuluLib.getS("Le fichier n'existe pas")); //the file doesn't exist
      } else {
        setErrorText(TrResource.getS("Le fichier choisi n'est pas un fichier projet")); //the file selected is not a project file
      }
      return false;
    }
    setErrorText(null);
    return true;
  }

  @Override
  public void changedUpdate(final DocumentEvent _e) {
    isDataValid();
  }

  @Override
  public void insertUpdate(final DocumentEvent _e) {
    isDataValid();
  }

  @Override
  public void removeUpdate(final DocumentEvent _e) {
    isDataValid();
  }

  public FudaaSaveZipLoader getLoader() throws IOException {
    final FudaaSaveZipLoader res = new FudaaSaveZipLoader(pn_.getFile());
    if (cbNoNewCalque_.isSelected()) {
      res.setOption(BCalquePersistenceGroupe.getNoLayerOpt(), CtuluLibString.toString(true));
    }
    if (cbNoGeom_.isSelected()) {
      res.setOption(FudaaSaveLib.getNoGeogLayerOpt(), CtuluLibString.toString(true));
    }
    return res;
  }

  boolean isAcceptable(final File _f) {
    if (_f != null && _f.exists() && _f.isFile()) {
      final String name = _f.getName();
      return name.endsWith(TrProjectPersistence.getPostProjectExt())
          || name.endsWith(TrProjectPersistence.getPreProjectExt());
    }
    return false;
  }
}
