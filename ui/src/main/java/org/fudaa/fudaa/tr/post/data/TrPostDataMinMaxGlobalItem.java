/**
 * @creation 20 d�c. 2004 @modification $Date: 2007-01-10 09:03:19 $ @license GNU General Public License 2 @copyright (c)1998-2001
 * CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.data;

import com.db4o.query.Query;
import java.io.Serializable;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;

/**
 * @author Fred Deniger
 * @version $Id: TrPostDataMinMaxGlobalItem.java,v 1.10 2007-01-10 09:03:19 deniger Exp $
 */
public class TrPostDataMinMaxGlobalItem implements Serializable {

  public static void updateQueryFor(final Query _q, final String _varId) {
    _q.constrain(TrPostDataMinMaxGlobalItem.class);
    _q.descend("id_").constrain(_varId);
  }
  final String id_;
  final double maxGlobal_;
  final CtuluArrayDouble[] maxTimeStep_;
  final CtuluArrayDouble maxGlobalTimeStep_;
  final CtuluArrayDouble minGlobalTimeStep_;
  final CtuluArrayDouble maxValues_;
  final double minGlobal_;
  final CtuluArrayDouble[] minTimeStep_;
  final CtuluArrayDouble minValues_;

  private TrPostDataMinMaxGlobalItem() {
    maxGlobal_ = 0;
    minGlobal_ = 1;
    id_ = null;
    maxTimeStep_ = new CtuluArrayDouble[0];
    minTimeStep_ = new CtuluArrayDouble[0];
    maxGlobalTimeStep_ = null;
    minGlobalTimeStep_ = null;
    maxValues_ = null;
    minValues_ = null;
    elementData = false;
  }
  final boolean elementData;

  public boolean isElementData() {
    return elementData;
  }

  /**
   * @param _id l'identifiant
   * @param _extrems les extrema calcules
   */
  public TrPostDataMinMaxGlobalItem(final String _id, final TrPostExtremVisitor _extrems) {
    id_ = _id;
    elementData = _extrems.isElementData();
    minGlobal_ = _extrems.minGlobal_;
    maxGlobal_ = _extrems.maxGlobal_;
    maxValues_ = new CtuluArrayDouble(_extrems.maxValues_);
    minValues_ = new CtuluArrayDouble(_extrems.minValues_);

    if (_extrems.minGlobalTimeStep_ == null) {
      minGlobalTimeStep_ = null;
    } else {
      _extrems.minGlobalTimeStep_.sort();
      minGlobalTimeStep_ = new CtuluArrayDouble(_extrems.minGlobalTimeStep_);
    }
    if (_extrems.maxGlobalTimeStep_ == null) {
      maxGlobalTimeStep_ = null;
    } else {
      maxGlobalTimeStep_ = new CtuluArrayDouble(_extrems.maxGlobalTimeStep_);
      _extrems.maxGlobalTimeStep_.sort();
    }
    minTimeStep_ = new CtuluArrayDouble[_extrems.minTimeStep_.length];
    for (int i = minTimeStep_.length - 1; i >= 0; i--) {
      _extrems.minTimeStep_[i].sort();
      minTimeStep_[i] = new CtuluArrayDouble(_extrems.minTimeStep_[i]);
    }
    maxTimeStep_ = new CtuluArrayDouble[_extrems.maxTimeStep_.length];
    for (int i = maxTimeStep_.length - 1; i >= 0; i--) {
      _extrems.maxTimeStep_[i].sort();
      maxTimeStep_[i] = new CtuluArrayDouble(_extrems.maxTimeStep_[i]);
    }
  }

  public int getNbValues() {
    return maxValues_.getSize();
  }

  public double getGlobalMaxFirstTimeStep() {
    return maxGlobalTimeStep_.getValue(0);
  }

  public int getGlobalMaxOccurence() {
    return maxGlobalTimeStep_.getSize();
  }

  public int getGlobalMinOccurence() {
    return minGlobalTimeStep_.getSize();
  }

  public double getGlobalMinFirstTimeStep() {
    return minGlobalTimeStep_.getValue(0);
  }

  public final double getMax() {
    return maxGlobal_;
  }

  public final CtuluCollectionDouble getMaxOnObjects() {
    return maxValues_;
  }

  public final CtuluCollectionDouble getMinOnObjects() {
    return minValues_;
  }

  public final CtuluCollectionDouble getTimeStepsForMax(final int _idxObject) {
    return maxTimeStep_[_idxObject];
  }

  public final CtuluCollectionDouble getTimeStepsForMin(final int _idxObject) {
    return minTimeStep_[_idxObject];
  }

  public final double getMinValue(final int _idxObject) {
    return minValues_.getValue(_idxObject);
  }

  public final double getMaxValue(final int _idxObject) {
    return minValues_.getValue(_idxObject);
  }

  public final double getMin() {
    return minGlobal_;
  }
}