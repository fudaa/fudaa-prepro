/*
 *  @creation     9 ao�t 2005
 *  @modification $Date: 2007-01-17 10:44:30 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.bu.BuComboBox;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostFlecheContentDefaut;
import org.fudaa.fudaa.tr.post.TrPostSource;

/**
 * Permet l'edition des variables creees par l'utilisateur.
 * 
 * @author Fred Deniger
 * @version $Id: TrPostDataEditCstPanel.java,v 1.4 2007-01-17 10:44:30 deniger Exp $
 */
@SuppressWarnings("serial")
public class TrPostDataEditFlechePanel extends TrPostDataEditCommonPanel {

  final BuComboBox cbVarX_;
  final BuComboBox cbVarY_;

  H2dVariableType initVarX_;
  H2dVariableType initVarY_;

  TrPostFlecheContentDefaut initFleche_;

  /**
   * @param _src la source
   * @param _var la variable a modifier: si null, phase de creation.
   */
  public TrPostDataEditFlechePanel(final TrPostSource _src, final H2dVariableTypeCreated _var) {
    super(_src, _var);
    initFleche_ = _var == null ? null : (TrPostFlecheContentDefaut) _src.getUserCreatedVar(_var);
    cbVarX_ = new BuComboBox(super.varToAdd_.toArray());
    cbVarY_ = new BuComboBox(super.varToAdd_.toArray());
    if (initFleche_ != null) {
      initVarX_ = initFleche_.getVx();
      initVarY_ = initFleche_.getVy();
      cbVarX_.setSelectedItem(initVarX_);
      cbVarY_.setSelectedItem(initVarY_);
    }
    addLabel(TrResource.getS("Composante selon X"));
    add(cbVarX_);
    addLabel(TrResource.getS("Composante selon Y"));
    add(cbVarY_);

  }

  @Override
  public TrPostDataCreated createData() {
    return new TrPostFlecheContentDefaut(src_, getNewVar(), (H2dVariableType) cbVarX_.getSelectedItem(),
        (H2dVariableType) cbVarY_.getSelectedItem(), true);
  }

  /**
   * @return true si des modifications ont ete faites.
   */
  @Override
  public boolean isModified() {
    if (var_ == null || initVarX_ == null || initVarY_ == null) { return true; }
    return super.isVarModified() || cbVarX_.getSelectedItem() != initVarX_ || cbVarY_.getSelectedItem() != initVarY_;
  }
}
