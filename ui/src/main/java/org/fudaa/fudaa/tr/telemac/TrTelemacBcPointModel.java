/*
 *  @creation     10 juin 2005
 *  @modification $Date: 2007-04-30 14:22:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuTable;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.h2d.H2dNodalPropertiesMngI;
import org.fudaa.dodico.h2d.telemac.H2dTelemacParameters;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.tr.data.TrBcPointModelDefault;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacBcPointModel.java,v 1.12 2007-04-30 14:22:37 deniger Exp $
 */
public class TrTelemacBcPointModel extends TrBcPointModelDefault {

  class BcTableModel extends AbstractTableModel {

    final int[] temp_ = new int[2];
    final List var_ = props_.getAllVariables();

    @Override
    public int getColumnCount() {
      return 5 + props_.getAllVariables().size();
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) { return MvResource.getS("Fronti�re"); }
      if (_column == 1) { return MvResource.getS("Indice sur la fronti�re"); }
      if (_column == 2) { return MvResource.getS("Indice global"); }
      if (_column == 3) { return "X"; }
      if (_column == 4) { return "Y"; }
      return ((H2dVariableType) var_.get(_column - 5)).getName();
    }

    @Override
    public int getRowCount() {
      return getNbTotalPoint();
    }

    @Override
    public Class getColumnClass(final int _columnIndex) {
      if (_columnIndex < 3) { return Integer.class; }
      return Double.class;
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      getIdxFrIdxOnFrFromFrontier(_rowIndex, temp_);

      if (_columnIndex == 0) { return new Integer(temp_[0] + 1); }
      if (_columnIndex == 1) { return new Integer(temp_[1] + 1); }
      if (_columnIndex == 2) { return new Integer(getGlobalIdx(temp_[0], temp_[1]) + 1); }
      if (_columnIndex == 3) {
        return CtuluLib.getDouble(getX(temp_[0], temp_[1]));
      } else if (_columnIndex == 4) { return CtuluLib.getDouble(getY(temp_[0], temp_[1])); }
      final int global = getGlobalIdx(temp_[0], temp_[1]);
      final H2dVariableType var = (H2dVariableType) var_.get(_columnIndex - 5);
      return props_.getViewedModel(var).getObjectValueAt(global);
    }
  }

  final H2dNodalPropertiesMngI props_;

  /**
   * @param _props les parametres associees / the assocated parameters
   */
  public TrTelemacBcPointModel(final H2dTelemacParameters _props) {
    super(_props.getMaillage());
    props_ = _props.getNodalProperties();
  }

  protected TableModel getModel() {
    return new BcTableModel();
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    final BuTable table = new CtuluTable();
    table.setModel(getModel());
    return table;
  }

}
