package org.fudaa.fudaa.tr.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.fudaa.ctulu.gui.CtuluValuesCommonEditorVariableProvider;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.nfunk.jep.Variable;

/**
 * @author genesis
 */
public class TrValuesVariablesProvider implements CtuluValuesCommonEditorVariableProvider {
  private final H2dVariableProviderInterface src;
  private final Map<String, H2dVariableType> varByName = new HashMap<String, H2dVariableType>();
  private final String x = "x";
  private final String y = "y";
  private final Collection<String> variables;

  public TrValuesVariablesProvider(H2dVariableProviderInterface src) {
    super();
    this.src = src;
    H2dVariableType[] H2dVariableType = src.getVarToModify();
    List<String> allVariables = new ArrayList<String>();
    allVariables.add(x);
    allVariables.add(y);
    for (H2dVariableType var : H2dVariableType) {
      varByName.put(var.getName(), var);
      allVariables.add(var.getName());
    }
    variables = Collections.unmodifiableCollection(allVariables);
  }

  @Override
  public Collection<String> getVariables() {
    return variables;
  }

  @Override
  public void modifyVariablesFor(int idx, Map<String, Variable> variables) {
    Variable xVar = variables.get(x);
    if (xVar != null) {
      xVar.setValue(src.getGrid().getPtX(idx));
    }
    Variable yVar = variables.get(y);
    if (yVar != null) {
      yVar.setValue(src.getGrid().getPtY(idx));
    }
    for (Entry<String, H2dVariableType> entry : varByName.entrySet()) {
      Variable var = variables.get(entry.getKey());
      if (var != null) {
        var.setValue(src.getValues(entry.getValue()).getObjectValueAt(idx));
      }

    }

  }
}
