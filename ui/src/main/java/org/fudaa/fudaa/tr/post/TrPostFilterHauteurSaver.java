/*
 * @creation 28 f�vr. 07
 * @modification $Date: 2007-04-30 14:22:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

class TrPostFilterHauteurSaver {
  double eps_;

  public TrPostFilterHauteurSaver(final double _eps) {
    super();
    eps_ = _eps;
  }
}