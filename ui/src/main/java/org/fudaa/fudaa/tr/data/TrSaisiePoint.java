/**
 * @creation 5 oct. 2004
 * @modification $Date: 2007-05-04 14:01:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPalette;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZCalqueClickInteraction;
import org.fudaa.ebli.calque.ZCalqueClikInteractionListener;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrSaisiePoint.java,v 1.14 2007-05-04 14:01:51 deniger Exp $
 */
public abstract class TrSaisiePoint extends JPanel implements ZCalqueClikInteractionListener, InternalFrameListener,
    TreeSelectionListener, ActionListener, PropertyChangeListener {

  class VisuFrameListener extends InternalFrameAdapter {

    @Override
    public void internalFrameActivated(final InternalFrameEvent _e) {
      palette_.setVisible(true);
    }

    @Override
    public void internalFrameClosed(final InternalFrameEvent _e) {
      try {
        palette_.setClosed(true);
      } catch (final PropertyVetoException _ex) {
        FuLog.warning(_ex);
      }
    }

    @Override
    public void internalFrameDeactivated(final InternalFrameEvent _e) {
      palette_.setVisible(false);
    }

  }

  protected BuButton bt_;
  protected BuLabel infoLabel_;

  protected ZCalqueAffichageDonnees layer_;
  protected TrVisuPanel pn_;
  protected BuPalette palette_;

  VisuFrameListener parentListener_;
  JInternalFrame pnParentFrame_;
  ZCalqueClickInteraction z_;

  public TrSaisiePoint(final TrVisuPanel _pn, final ZCalqueAffichageDonnees _layer) {
    final Container c = SwingUtilities.getAncestorOfClass(JInternalFrame.class, _pn);
    if (c instanceof JInternalFrame) {
      pnParentFrame_ = (JInternalFrame) c;
      parentListener_ = new VisuFrameListener();
      pnParentFrame_.addInternalFrameListener(parentListener_);
    }
    pn_ = _pn;
    pn_.getArbreCalqueModel().addTreeSelectionListener(this);
    layer_ = _layer;

    z_ = new ZCalqueClickInteraction();
    z_.addPropertyChangeListener("gele", this);
    z_.setListener(this);
    pn_.addCalqueInteraction(z_);
    pn_.setCalqueInteractionActif(z_);
    bt_ = new BuButton();
    setBoutonActifState();
    bt_.setActionCommand("ACTIVE");
    bt_.addActionListener(this);
    infoLabel_ = new BuLabel(TrResource.getS("Derni�re action"));
    infoLabel_.setHorizontalAlignment(SwingConstants.CENTER);
    infoLabel_.setHorizontalTextPosition(SwingConstants.CENTER);
    setLayout(new BuVerticalLayout(5, true, false));
  }

  protected void buildPanel() {
    add(infoLabel_);
    add(bt_);
  }

  protected void pointAjouteOk(final String _x, final String _y) {
    okMessage(TrResource.getS("Point ajout�:") + CtuluLibString.ESPACE + _x + ", " + _y);
  }

  protected void okMessage(final String _txt) {
    infoLabel_.setForeground(Color.BLACK);
    infoLabel_.setText(_txt);
  }

  protected void errMessage(final String _txt) {
    infoLabel_.setForeground(Color.RED);
    java.awt.Toolkit.getDefaultToolkit().beep();
    infoLabel_.setText(_txt);
  }

  protected void pointErrone() {
    errMessage(TrResource.getS("Donn�es invalides"));
  }

  protected void removePalette() {
    if (palette_ == null) { return; }
    if (SwingUtilities.isEventDispatchThread()) {
      // pour envoyer un evt
      try {
        palette_.setClosed(true);
      } catch (final PropertyVetoException _e) {
        FuLog.warning(_e);
      }
      if (pn_ != null && palette_ != null) {
        pn_.getImpl().getMainPanel().getDesktop().removeInternalFrame(palette_);
      }
    } else {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          try {
            // pour envoyer un evt
            palette_.setClosed(true);
          } catch (final PropertyVetoException _e) {
            FuLog.warning(_e);
          }
          if (pn_ != null) pn_.getImpl().getMainPanel().getDesktop().removeInternalFrame(palette_);
        }
      });
    }
  }

  protected final void setBoutonActifState() {
    bt_.setText(TrResource.getS("D�sactiver la saisie"));
  }

  protected final void setBoutonInActifState() {
    bt_.setText(TrResource.getS("Activer la saisie"));
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bt_) {
      z_.setGele(!z_.isGele());
      if (!z_.isGele()) {
        pn_.setCalqueInteractionActif(z_);
      }
    }
  }

  TrSaisiePointAction act_;

  protected void selectPalette() {
    if (palette_ != null) {
      try {
        palette_.show();
        palette_.setSelected(true);
      } catch (final PropertyVetoException _e) {
        FuLog.warning(_e);
      }
    }
  }

  /**
   * Active la saisie des points.
   */
  public void activeSaisiePoint(final TrSaisiePointAction _act) {
    if (getComponentCount() == 0) {
      buildPanel();
    }
    act_ = _act;
    if (act_ != null) {
      act_.pt_ = this;
    }
    if (palette_ == null) {
      palette_ = new BuPalette();
      palette_.setResizable(true);
      palette_.setContent(this);
      palette_.setClosable(true);
      palette_.addInternalFrameListener(this);
    }

    if (SwingUtilities.isEventDispatchThread()) {
      palette_.pack();
      pn_.getImpl().getMainPanel().getDesktop().addInternalFrame(palette_);
      palette_.setLocation(pn_.getLocation());
    } else {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          palette_.pack();
          pn_.getImpl().getMainPanel().getDesktop().addInternalFrame(palette_);
        }
      });
    }

  }

  @Override
  public void internalFrameActivated(final InternalFrameEvent _e) {}

  @Override
  public void internalFrameClosed(final InternalFrameEvent _e) {
    z_.setGele(true);
    z_.setListener(null);
    pn_.removeCalqueInteraction(z_);
    if (parentListener_ != null) {
      pnParentFrame_.removeInternalFrameListener(parentListener_);
    }
    pn_.getArbreCalqueModel().removeTreeSelectionListener(this);
    if (act_ != null) {
      act_.pt_ = null;
      act_ = null;
    }
    // palette_.dispose();
    palette_ = null;
  }

  @Override
  public void internalFrameClosing(final InternalFrameEvent _e) {}

  @Override
  public void internalFrameDeactivated(final InternalFrameEvent _e) {}

  @Override
  public void internalFrameDeiconified(final InternalFrameEvent _e) {}

  @Override
  public void internalFrameIconified(final InternalFrameEvent _e) {}

  @Override
  public void internalFrameOpened(final InternalFrameEvent _e) {}

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    if (z_.isGele()) {
      setBoutonInActifState();
    } else {
      setBoutonActifState();
    }

  }

  @Override
  public void valueChanged(final TreeSelectionEvent _e) {
    if (pn_.getArbreCalqueModel().getSelectedCalque() != layer_) {
      removePalette();
    }
  }

}