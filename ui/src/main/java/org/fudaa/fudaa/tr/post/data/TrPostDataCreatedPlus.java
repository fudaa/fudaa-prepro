/*
 *  @creation     3 mai 2005
 *  @modification $Date: 2007-04-30 14:22:38 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.fu.FuLog;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.TrPostSource;

import java.io.IOException;
import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: TrPostDataCreatedPlus.java,v 1.9 2007-04-30 14:22:38 deniger Exp $
 */
public final class TrPostDataCreatedPlus extends TrPostDataCreatedDefault {
  final H2dVariableType v1_;
  final H2dVariableType v2_;

  @Override
  public void fillWhithAllUsedVar(final Set _res) {
    _res.add(v1_);
    _res.add(v2_);
  }

  @Override
  public String getDescription() {
    return v1_.toString() + " + " + v2_.toString();
  }

  /**
   * @param _v1 la premiere var pour l'addition
   * @param _v2 la deuxieme
   */
  public TrPostDataCreatedPlus(final TrPostSource _src, final H2dVariableType _v1, final H2dVariableType _v2) {
    super(_src);
    v1_ = _v1;
    v2_ = _v2;
  }

  @Override
  public double buildDataFor(final int _idxTime, final int _idxObject) {
    try {
      if (src_.isElementVar(v1_) != src_.isElementVar(v2_)) {
        final EfData efData = buildDataFor(_idxTime);
        return efData == null ? 0d : efData.getValue(_idxObject);
      }
      return src_.getData(v1_, _idxTime, _idxObject) + src_.getData(v2_, _idxTime, _idxObject);
    } catch (final IOException _e) {
      FuLog.warning(_e);
    }
    return 0;
  }

  @Override
  public EfData buildDataFor(final int _idxTime) {
    EfData v1Data = src_.getData(v1_, _idxTime);
    EfData v2Data = src_.getData(v2_, _idxTime);
    if (v1Data.isElementData() != v2Data.isElementData()) {
      if (!v1Data.isElementData()) {
        v1Data = EfLib.getElementDataDanger(v1Data, src_.getGrid());
      }
      if (!v2Data.isElementData()) {
        v2Data = EfLib.getElementDataDanger(v2Data, src_.getGrid());
      }
    }
    if (v1Data == null || v2Data == null) {
      return null;
    }
    if (v2Data.getSize() != v1Data.getSize()) {
      throw new IllegalArgumentException("values sizes are not equal");
    }
    final double[] plusRes = new double[v1Data.getSize()];
    for (int i = plusRes.length - 1; i >= 0; i--) {
      plusRes[i] = v1Data.getValue(i) + v2Data.getValue(i);
    }
    return v1Data.isElementData() ? ((EfData) new EfDataElement(plusRes)) : ((EfData) new EfDataNode(plusRes));
  }
}
