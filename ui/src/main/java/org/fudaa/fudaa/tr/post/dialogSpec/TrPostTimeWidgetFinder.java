/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.dialogSpec;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.graphe.EbliWidgetCreatorGraphe;
import org.fudaa.fudaa.tr.post.TrPostCourbeTreeModel;

/**
 *
 * @author Frederic Deniger
 */
public class TrPostTimeWidgetFinder {

  private final EbliScene scene;

  public TrPostTimeWidgetFinder(EbliScene scene) {
    this.scene = scene;
  }

  public List<EbliNode> findGraphTimeNodes() {
    // -- recuperation de la liste des nodes de la scene --//
    final Set<EbliNode> listeNode = (Set<EbliNode>) scene.getObjects();
    List<EbliNode> res = new ArrayList<EbliNode>();
    for (final Iterator<EbliNode> it = listeNode.iterator(); it.hasNext();) {
      final EbliNode currentNode = it.next();
      if (currentNode.getCreator() instanceof EbliWidgetCreatorGraphe) {
        final EbliWidgetCreatorGraphe graphe = (EbliWidgetCreatorGraphe) currentNode.getCreator();
        if (isCorrectGraph(graphe.getGraphe().getModel())) {
          res.add(currentNode);
        }
      }
    }
    return res;
  }

  public boolean isCorrectGraph(final EGGrapheModel _model) {
    return (_model instanceof TrPostCourbeTreeModel);
  }
}
