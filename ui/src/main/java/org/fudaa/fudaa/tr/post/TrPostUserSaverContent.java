package org.fudaa.fudaa.tr.post;

import java.util.Map;

public class TrPostUserSaverContent {

  private final Map<String, double[]> persistData;
  private final TrPostUserVariableSaver saver;

  public TrPostUserSaverContent(Map<String, double[]> persistData, TrPostUserVariableSaver saver) {
    super();
    this.persistData = persistData;
    this.saver = saver;
  }

  public Map<String, double[]> getPersistData() {
    return persistData;
  }

  public TrPostUserVariableSaver getSaver() {
    return saver;
  }

}