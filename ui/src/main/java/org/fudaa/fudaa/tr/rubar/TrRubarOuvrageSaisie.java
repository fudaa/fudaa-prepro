/**
 * @creation 6 janv. 2005 @modification $Date: 2007-05-04 14:01:53 $ @license GNU General Public License 2 @copyright (c)1998-2001 CETMEF 2 bd
 *     Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuButtonLayout;
import com.memoire.bu.BuPanel;
import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import gnu.trove.TIntHashSet;
import org.fudaa.ctulu.CtuluCommandCompositeInverse;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.ef.EfElementVolume;
import org.fudaa.dodico.h2d.rubar.H2dRubarGridAreteSource;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrage;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrSaisiePoint;
import org.fudaa.fudaa.tr.data.TrVisuPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarOuvrageSaisie.java,v 1.18 2007-05-04 14:01:53 deniger Exp $
 */
public class TrRubarOuvrageSaisie extends TrSaisiePoint {
  int[] selectedElement_;
  int step_;
  private final TrRubarEvolutionManager evolManager;
  private final TrRubarTarageCourbesManager tarageEvolManager;
  BuButton btSuivant_;
  BuButton btApply_;
  final H2dRubarGridAreteSource grid_;
  final int[] areteAlreadyUsed_;
  final String[] stepTitle_ = new String[]{TrResource.getS("S�lectionner l'�l�ment amont 1"), TrResource.getS("S�lectionner l'ar�te amont 1"),
      TrResource.getS("S�lectionner l'�l�ment aval 2 (option)"), TrResource.getS("S�lectionner l'ar�te aval 2")};

  /**
   * @param _pn
   * @param _layer
   */
  public TrRubarOuvrageSaisie(final TrVisuPanel _pn, H2dRubarGridAreteSource grid, final TrRubarOuvrageLayer _layer, final int[] _selectedElt,
                              final int[] _areteAlreadyUsed, TrRubarEvolutionManager evolManager, TrRubarTarageCourbesManager tarageEvolManager) {
    super(_pn, _layer);
    this.grid_ = grid;
    selectedElement_ = _selectedElt;
    this.evolManager = evolManager;
    this.tarageEvolManager = tarageEvolManager;
    step_ = 0;
    areteAlreadyUsed_ = (CtuluLibArray.isEmpty(_areteAlreadyUsed)) ? FuEmptyArrays.INT0 : _areteAlreadyUsed;
    Arrays.sort(areteAlreadyUsed_);
    super.infoLabel_.setText(stepTitle_[step_]);
  }

  @Override
  protected void buildPanel() {
    super.buildPanel();
    final BuPanel south = new BuPanel();
    south.setLayout(new BuButtonLayout(5, SwingConstants.RIGHT));
    btSuivant_ = new BuButton();
    btSuivant_.setText(TrResource.getS("Prochaine �tape"));
    btSuivant_.setEnabled(false);
    btSuivant_.setActionCommand("OUV_NEXT");
    btSuivant_.addActionListener(this);
    btApply_ = new BuButton();
    btApply_.setEnabled(false);
    btApply_.setText(TrResource.getS("Ajouter"));
    btApply_.setActionCommand("OUV_APPLY");
    btApply_.addActionListener(this);
    south.add(btSuivant_);
    south.add(btApply_);
    add(south);
  }

  void updateApply() {
    btApply_.setEnabled((elt1_ >= 0) && (ar1_ >= 0) && (elt2_ < 0 || ar2_ >= 0));
  }

  void updateSuivant() {
    boolean ok = false;
    switch (step_) {
      case 0:
        ok = elt1_ >= 0;
        break;
      case 1:
        ok = ar1_ >= 0;
        break;
      case 2:
        ok = elt2_ >= 0;
        break;
      case 3:
        ok = false;
        break;

      default:
        FuLog.warning(new Throwable());
        break;
    }
    btSuivant_.setEnabled(ok);
  }

  TrRubarOuvrageLayer getOuvLayer() {
    return (TrRubarOuvrageLayer) super.layer_;
  }

  Coordinate pt_ = new Coordinate();

  protected void paintTempo() {
    final TrRubarOuvrageLayer l = getOuvLayer();
    if (elt1_ >= 0) {
      l.getM().getCentreElt(elt1_, pt_);
    }
  }

  final int[] values_ = new int[]{-1, -1, -1, -1};

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if ("OUV_NEXT".equals(_e.getActionCommand())) {
      infoLabel_.setText(stepTitle_[++step_]);
      btSuivant_.setEnabled(false);
    } else if ("OUV_APPLY".equals(_e.getActionCommand())) {
      final TIntHashSet set = new TIntHashSet(selectedElement_);
      set.remove(elt1_);
      set.remove(elt2_);
      final CtuluCommandCompositeInverse cmp = new CtuluCommandCompositeInverse();
      final H2dRubarOuvrage o = getOuvLayer().getM().addOuvrage(set.toArray(), elt1_, ar1_, elt2_, ar2_, cmp);
      if (o != null) {
        final TrRubarOuvrageEditorPanel edit = new TrRubarOuvrageEditorPanel(o, grid_, cmp, pn_.getImpl(), evolManager, tarageEvolManager);
        if (CtuluDialogPanel.isOkResponse(edit.afficheModale(pn_.getImpl().getFrame()))) {
          pn_.getCmdMng().addCmd(cmp.getSimplify());
          getOuvLayer().ouvrageAddByUser(o);
        } else {
          cmp.undo();
        }
      }

      removePalette();
      getOuvLayer().unsetTempo();
      getOuvLayer().clearSelection();
      elt1_ = -1;
      elt2_ = -1;
      ar1_ = -1;
      ar2_ = -1;
    } else {
      super.actionPerformed(_e);
    }
  }

  @Override
  public void pointClicked(GrPoint ptReel) {
    GrPoint ptEcran = ptReel.applique(pn_.getVueCalque().getVersEcran());
    pointClicked((int) ptEcran.x_, (int) ptEcran.y_);
  }

  public void pointClicked(final int _xEcran, final int _yEcran) {
    boolean ok = false;
    switch (step_) {
      case 0:
        ok = pointClickedElt1(_xEcran, _yEcran);
        break;
      case 1:
        ok = pointClickedArete1(_xEcran, _yEcran);
        break;
      case 2:
        ok = pointClickedElt2(_xEcran, _yEcran);
        break;
      case 3:
        ok = pointClickedArete2(_xEcran, _yEcran);
        break;

      default:
        FuLog.warning(new Throwable());
        break;
    }
    if (ok) {
      updateApply();
      updateSuivant();
    }
  }

  int elt1_ = -1;
  int ar1_ = -1;
  int elt2_ = -1;
  int ar2_ = -1;

  boolean pointClickedElt1(final int _xEcran, final int _yEcran) {
    elt1_ = getSelectedElement(_xEcran, _yEcran);
    if (elt1_ >= 0) {
      values_[0] = elt1_;
      getOuvLayer().setTempo(values_);
      getOuvLayer().repaint();
      return true;
    }
    return false;
  }

  /**
   * @param _xEcran le x a l'ecran
   * @param _yEcran le y a l'ecran
   * @return l'indice de l'element selection ou -1 si aucun
   */
  int getSelectedElement(final int _xEcran, final int _yEcran) {
    final GrPoint ptSaisie = new GrPoint(_xEcran, _yEcran, 0);
    ptSaisie.autoApplique(super.layer_.getVersReel());
    final TrRubarOuvrageLayerModel m = getOuvLayer().getM();
    for (int i = selectedElement_.length - 1; i >= 0; i--) {
      if (m.isEltContainedXY(selectedElement_[i], ptSaisie.x_, ptSaisie.y_)) {
        return selectedElement_[i];
      }
    }
    return -1;
  }

  GrSegment s_ = new GrSegment(new GrPoint(), new GrPoint());

  int getNearestSelectedPt(final int _idxElt, final int _xEcran, final int _yEcran) {
    if (_idxElt < 0) {
      return _idxElt;
    }
    final GrPoint ptSaisie = new GrPoint(_xEcran, _yEcran, 0);
    final TrRubarOuvrageLayerModel m = getOuvLayer().getM();
    final EfElementVolume elt1 = m.getElt(_idxElt);
    final int tolerance = 5;
    int idx = -1;
    double min = -1;
    double dist;
    for (int i = elt1.getNbAretes() - 1; i >= 0; i--) {
      m.getArete(elt1.getIdxArete(i), s_);
      s_.autoApplique(getOuvLayer().getVersEcran());
      dist = s_.distance(ptSaisie);
      if (dist < tolerance && (idx < 0 || (idx >= 0 && dist < min))) {
        idx = elt1.getIdxArete(i);
        min = dist;
      }
    }
    return idx;
  }

  /**
   * @param _i l'indice de l'arete
   * @return true si l'arete est deja utilise par autre ouvrage
   */
  protected boolean isEdgeUsed(final int _i) {
    final boolean used = Arrays.binarySearch(areteAlreadyUsed_, _i) >= 0;
    if (used) {
      super.errMessage(TrResource.getS("L'ar�te est d�j� utilis�e par un autre ouvrage"));
    }
    return used;
  }

  public boolean pointClickedArete1(final int _xEcran, final int _yEcran) {
    final int idx = getNearestSelectedPt(elt1_, _xEcran, _yEcran);
    if (idx >= 0) {
      if (isEdgeUsed(idx)) {

        return false;
      }
      ar1_ = idx;
      values_[1] = ar1_;
      getOuvLayer().repaint();
      return true;
    }
    return false;
  }

  public boolean pointClickedElt2(final int _xEcran, final int _yEcran) {
    elt2_ = getSelectedElement(_xEcran, _yEcran);
    if (elt2_ >= 0) {
      values_[2] = elt2_;
      getOuvLayer().repaint();
      return true;
    }
    return false;
  }

  public boolean pointClickedArete2(final int _xEcran, final int _yEcran) {
    final int idx = getNearestSelectedPt(elt2_, _xEcran, _yEcran);
    if (idx >= 0) {
      if (isEdgeUsed(idx)) {
        return false;
      }
      ar2_ = idx;
      values_[3] = ar2_;
      getOuvLayer().repaint();
      return true;
    }
    return false;
  }
}
