/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post;

import org.locationtech.jts.geom.Coordinate;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.operation.EfIndexHelper;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.fudaa.tr.common.TrLib;

/**
 *
 * @author Frederic Deniger
 */
public class TrPostCourbeModelBuilder {

  static EGCourbeChild buildCourbeModel(final H2dVariableType _var, final EGGroup _group, final TrPostSource _newSrc, double x, double y) {
    EfGridInterface grid = _newSrc.getGrid();
    if (grid.getIndex() == null) {
      grid.createIndexRegular(null);
    }
    int idxElt = EfIndexHelper.getElementEnglobant(grid, x, y, null);
    if (idxElt < 0) {
      return null;
    }
    if (!_newSrc.isElementVar(_var)) {
      int idxPt = EfLib.getIdxGlobalForPt(idxElt, x, y, _newSrc.getPrecisionModel(), grid);
      if (idxPt >= 0) {
        return buildCourbeModel(_var, idxPt, _group, _newSrc);
      }
    }
    return buildCourbeModel(_var, new TrPostInterpolatePoint(idxElt, x, y, _newSrc.getPrecisionModel()), _group, _newSrc);
  }

  public static EGCourbeChild buildCourbeModel(final H2dVariableType _var, final TrPostInterpolatePoint _idxPt, final EGGroup _group, final TrPostSource _src) {
    if (!_src.isElementVar(_var)) {
      int idxPt = EfLib.getIdxGlobalForPt(_idxPt.getIdxElt(), _idxPt.getX(), _idxPt.getY(), _src.getPrecisionModel(), _src.getGrid());
      if (idxPt >= 0) {
        return buildCourbeModel(_var, idxPt, _group, _src);
      }
    }
    final TrPostCourbeModel model = new TrPostCourbeModel.Interpolated(_src, _idxPt, _var);
    final EGCourbeChild courbe = new TrPostCourbe(_group, model, _src.getNewTimeListModel());
    _group.addEGComponent(courbe);
    String unit = _var.getCommonUnitString();
    if (unit != null) {
      unit = "(" + _var.getCommonUnitString() + ")";
    } else {
      unit = CtuluLibString.EMPTY_STRING;
    }
    courbe.getModel().setTitle(courbe.getModel().getTitle() + unit + " (" + _src.getPrecisionModel().round(_idxPt.getX()) + ", " + _src.getPrecisionModel().round(_idxPt.getY()) + ')');
    return courbe;
  }

  public static EGCourbeChild buildCourbeModel(final H2dVariableType _var, final int _idxPt, final EGGroup _group, final TrPostSource _src) {
    final TrPostCourbeModel model = new TrPostCourbeModel.Node(_src, _idxPt, _var, _src.getTime().getTimeListModel().getTimesInSec());
    final EGCourbeChild courbe = new TrPostCourbe(_group, model, _src.getNewTimeListModel());
    _group.addEGComponent(courbe);
    String title = courbe.getModel().getTitle() + CtuluLibString.ESPACE;
    if (_var.getCommonUnitString() != null) {
      title += "(" + _var.getCommonUnitString() + ")" + CtuluLibString.ESPACE;
    }
    Coordinate coor = new Coordinate();
    String obj = CtuluLibString.EMPTY_STRING;
    if (_src.isElementVar(_var)) {
      _src.getGrid().getMoyCentreElement(_idxPt, coor);
      obj = TrLib.getString("El�ment");
    } else {
      _src.getGrid().getCoord(_idxPt, coor);
      obj = TrLib.getString("Noeud");
    }
    title += obj + " " + (_idxPt + 1) + " (" + _src.getPrecisionModel().round(coor.x) + ", " + _src.getPrecisionModel().round(coor.y) + ")";
    courbe.getModel().setTitle(title);
    return courbe;
  }

  /**
   * Attention, le point d'interpolation concerne l'ancienne source de donn�e.
   *
   * @param _var
   * @param _idxPt
   * @param _group
   * @param _newSrc
   * @param oldSource
   * @return
   */
  public static EGCourbeChild buildCourbeModelOnNewSource(final H2dVariableType _var, final TrPostInterpolatePoint _idxPt, final EGGroup _group, final TrPostSource _newSrc, TrPostSource oldSource) {
    if (oldSource == _newSrc) {
      return buildCourbeModel(_var, _idxPt, _group, _newSrc);
    }
    double x = _idxPt.x_;
    double y = _idxPt.y_;
    return buildCourbeModel(_var, _group, _newSrc, x, y);
  }

  /**
   * Attention, le point d'interpolation concerne l'ancienne source de donn�e.
   *
   * @param _var
   * @param _idxPt
   * @param _group
   * @param _newSrc
   * @param oldSource
   * @return
   */
  public static EGCourbeChild buildCourbeModelOnNewSource(final H2dVariableType _var, final int _idxPt, final EGGroup _group, final TrPostSource _newSrc, TrPostSource oldSource) {
    if (oldSource == _newSrc) {
      return buildCourbeModel(_var, _idxPt, _group, _newSrc);
    }
    return buildCourbeModel(_var, _group, _newSrc, oldSource.getGrid().getPtX(_idxPt), oldSource.getGrid().getPtY(_idxPt));
  }
  
}
