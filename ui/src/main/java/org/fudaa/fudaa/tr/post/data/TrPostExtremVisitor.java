/*
 *  @creation     2 mai 2005
 *  @modification $Date: 2007-01-10 09:03:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import gnu.trove.TDoubleArrayList;
import java.util.Arrays;
import org.fudaa.ctulu.collection.CtuluDoubleVisitor;

/**
 * @author Fred Deniger
 * @version $Id: TrPostExtremVisitor.java,v 1.6 2007-01-10 09:03:19 deniger Exp $
 */
public class TrPostExtremVisitor implements CtuluDoubleVisitor {

  double currentTime_;
  double maxGlobal_;
  TDoubleArrayList maxGlobalTimeStep_;
  TDoubleArrayList minGlobalTimeStep_;
  TDoubleArrayList[] maxTimeStep_;
  double[] maxValues_;
  double minGlobal_;
  TDoubleArrayList[] minTimeStep_;
  double[] minValues_;
  final boolean elementData;

  /**
   *
   */
  public TrPostExtremVisitor(final int _nbValues, final boolean isElement) {
    this.elementData = isElement;
    minValues_ = new double[_nbValues];
    maxValues_ = new double[_nbValues];
    Arrays.fill(minValues_, Double.MAX_VALUE);
    Arrays.fill(maxValues_, -Double.MAX_VALUE);
    maxGlobal_ = -Double.MAX_VALUE;
    minGlobal_ = Double.MAX_VALUE;
    minTimeStep_ = new TDoubleArrayList[_nbValues];
    maxTimeStep_ = new TDoubleArrayList[_nbValues];
  }

  public boolean isElementData() {
    return elementData;
  }
  

  @Override
  public boolean accept(final int _idxPt, final double _v) {
    if (_v >= maxGlobal_) {
      if (maxGlobalTimeStep_ == null) {
        maxGlobalTimeStep_ = new TDoubleArrayList();
      } else if (_v > maxGlobal_) {
        maxGlobalTimeStep_.clear();
      }
      maxGlobalTimeStep_.add(currentTime_);
      maxGlobal_ = _v;
    }
    if (_v <= minGlobal_) {
      if (minGlobalTimeStep_ == null) {
        minGlobalTimeStep_ = new TDoubleArrayList();
      } else if (_v < minGlobal_) {
        minGlobalTimeStep_.clear();
      }
      minGlobalTimeStep_.add(currentTime_);
      minGlobal_ = _v;
    }
    if (_v >= maxValues_[_idxPt]) {
      if (maxTimeStep_[_idxPt] == null) {
        maxTimeStep_[_idxPt] = new TDoubleArrayList();
      } else if (_v > maxValues_[_idxPt]) {
        maxTimeStep_[_idxPt].clear();
      }
      maxTimeStep_[_idxPt].add(currentTime_);
      maxValues_[_idxPt] = _v;
    }
    if (_v <= minValues_[_idxPt]) {
      if (minTimeStep_[_idxPt] == null) {
        minTimeStep_[_idxPt] = new TDoubleArrayList();
      } else if (_v > maxValues_[_idxPt]) {
        minTimeStep_[_idxPt].clear();
      }
      minTimeStep_[_idxPt].add(currentTime_);
      minValues_[_idxPt] = _v;
    }
    return true;
  }

  public final double getCurrentTime() {
    return currentTime_;
  }

  public final void setCurrentTime(final double _currentTime) {
    currentTime_ = _currentTime;
  }
}
