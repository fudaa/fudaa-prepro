/**
 *  @creation     10 juin 2003
 *  @modification $Date: 2007-06-28 09:28:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.fu.FuLog;
import java.awt.Frame;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dParameters;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBcManager;
import org.fudaa.dodico.h2d.reflux.H2dRefluxDicoVersion;
import org.fudaa.dodico.h2d.reflux.H2dRefluxNodalPropertiesMng.ForceItem;
import org.fudaa.dodico.h2d.reflux.H2dRefluxParameters;
import org.fudaa.dodico.h2d.reflux.H2dRefluxSolutionInitAdapterInterface;
import org.fudaa.dodico.h2d.reflux.H2dRefluxSolutionInitialeInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.reflux.io.INPFileFormat;
import org.fudaa.dodico.reflux.io.RefluxInpAdapter;
import org.fudaa.dodico.reflux.io.RefluxSollicitationFileFormat;
import org.fudaa.dodico.reflux.io.RefluxSolutionInitWriter;
import org.fudaa.fudaa.commun.FudaaUI;
import org.fudaa.fudaa.tr.common.TrCourbeTemporelleManager;
import org.fudaa.fudaa.tr.common.TrImplementationEditorAbstract;
import org.fudaa.fudaa.tr.common.TrParametres;
import org.fudaa.fudaa.tr.common.TrPreferences;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrRefluxParametres.java,v 1.46 2007-06-28 09:28:19 deniger Exp $
 */
public class TrRefluxParametres implements TrParametres {

  private boolean fileIsProject_;
  File fINP_;

  H2dRefluxParameters params_;
  String title_;
  TrImplementationEditorAbstract ui_;
  TrCourbeTemporelleManager courbeMng_;

  protected TrRefluxParametres() {
    super();
  }

  /**
   * @return l'implementation
   */
  public TrImplementationEditorAbstract getImpl() {
    return ui_;
  }

  /**
   * @return la fenetre de l'interface
   */
  public Frame getFrame() {
    return ui_.getFrame();
  }

  private void errorFileAlreadyUsed(final String _f) {
    ui_.error(CtuluResource.CTULU.getString("Le fichier {0} est d�ja ouvert et ne peut pas �tre �cras�", _f));
  }

  /**
   * @return true si l'operation a ete effectu�e
   */
  private boolean internSave(final File _f, final ProgressionInterface _prog, final boolean _ask) {
    final File dir = _f.getAbsoluteFile().getParentFile();
    String ficNameInit = _f.getName();
    final String ficSansExt = CtuluLibFile.getSansExtension(ficNameInit);
    final ArrayList l = new ArrayList(10);
    final ArrayList lToWrite = new ArrayList(10);
    final FileToWrite files = getFileToWrite(ficSansExt, l, lToWrite);
    final H2dRefluxDicoVersion inpVersion = getINPVersion();
    if ((ficNameInit.indexOf('.') < 0) && (TrPreferences.TR.getBooleanProperty("add.extension", true))) {
      ficNameInit = CtuluLibFile.getFileName(ficNameInit, inpVersion.getFileFormat().getExtensions()[0]);
    }
    final File inp = new File(dir, ficNameInit);
    if (_ask) {
      final ArrayList fileToOverWrite = new ArrayList(10);
      if (inp.exists()) {
        fileToOverWrite.add(inp.getAbsolutePath());
      }
      for (final Iterator it = lToWrite.iterator(); it.hasNext();) {
        final File f = new File(dir, (String) it.next());
        final String res = CtuluLibFile.canWrite(f);
        if (res != null) {
          getImpl().error(f.getName(), res);
          return false;
        }
        if (f.exists()) {
          fileToOverWrite.add(f.getAbsolutePath());
        }
      }
      if (fileToOverWrite.size() > 0) {
        final StringBuffer b = new StringBuffer(100);
        b.append(TrResource.getS("Les fichiers suivants vont �tre remplac�s")).append(":\n");
        for (final Iterator it = fileToOverWrite.iterator(); it.hasNext();) {
          b.append((String) it.next()).append(CtuluLibString.LINE_SEP);
        }
        b.append(CtuluLibString.LINE_SEP).append(TrResource.getS("Voulez-vous continuer ?"));
        if (!getUI().question(TrResource.getS("Remplacer") + '?', b.toString())) { return false; }
      }
    }

    final RefluxInpAdapter inter = new RefluxInpAdapter(params_, CtuluLibString.enTableau(l));
    if (_prog != null) {
      _prog.setDesc(TrResource.getS("Enregistrement"));
    }
    getUI().manageErrorOperationAndIsFatal(

    TrRefluxParametres.this.getINPVersion().write(inp, inter, _prog));
    // on ecrit les solutions initiales
    if (files.si_ != null) {
      if (FuLog.isTrace()) {
        FuLog.trace("TRR : write reflux si");
      }
      final H2dRefluxSolutionInitialeInterface si2d = params_.getSolutionInitiales();
      final Object o = si2d.createWriterInterface();
      RefluxSolutionInitWriter.writeSI(getMaillage(), (H2dRefluxSolutionInitAdapterInterface) o, new File(dir,
          files.si_), _prog);
    }
    if (files.vent_ != null) {
      if (FuLog.isTrace()) {
        FuLog.trace("TRR : write reflux vent");
      }
      final ForceItem it = params_.getPropNodal().getForceData(H2dVariableType.VENT);
      new RefluxSollicitationFileFormat(it.getNbVar(), inpVersion.getFileExtensionForVent(params_.getProjetType()))
          .write(new File(dir, files.vent_), inter.getVentResult(), _prog);
    }
    if (files.radiation_ != null) {
      if (FuLog.isTrace()) {
        FuLog.trace("TRR : write reflux radiation");
      }
      final ForceItem it = params_.getPropNodal().getForceData(H2dVariableType.RADIATION);
      new RefluxSollicitationFileFormat(it.getNbVar(), inpVersion.getFileExtensionForRadiation(params_.getProjetType()))
          .write(new File(dir, files.radiation_), inter.getRadiationResult(), _prog);
    }
    return true;
  }

  class FileToWrite {
    public String si_;
    public String vent_;
    public String radiation_;
  }

  private FileToWrite getFileToWrite(final String _ficSansExt, final List _l, final List _lToWrite) {
    final FileToWrite res = new FileToWrite();
    if (params_.contientClTransitoire()) {
      final String s = CtuluLibFile.getFileName(_ficSansExt, getINPVersion().getClTransitoireFileExtension());
      _l.add(s);
      _lToWrite.add(s);
    }
    if (params_.contientPnTransitoire()) {
      final String s = CtuluLibFile.getFileName(_ficSansExt, getINPVersion().getPnTransitoireFileExtension());
      _l.add(s);
      _lToWrite.add(s);
    }

    if (params_.isSolutionInitialSet()) {
      res.si_ = CtuluLibFile.getFileName(_ficSansExt, getINPVersion().getSolutionInitFileExtension());
      _l.add(res.si_);
      _lToWrite.add(res.si_);
    }
    if (params_.getPropNodal().isVentEnable()) {
      res.vent_ = CtuluLibFile.getFileName(_ficSansExt, getINPVersion()
          .getFileExtensionForVent(params_.getProjetType()));
      _l.add(res.vent_);
      _lToWrite.add(res.vent_);
    }
    if (params_.getPropNodal().isRadiationEnable()) {
      res.radiation_ = CtuluLibFile.getFileName(_ficSansExt, getINPVersion().getFileExtensionForRadiation(
          params_.getProjetType()));
      _l.add(res.radiation_);
      _lToWrite.add(res.radiation_);
    }
    _l.add(CtuluLibFile.getFileName(_ficSansExt, getINPVersion().getSolutionFileExtension()));
    _l.add(CtuluLibFile.getFileName(_ficSansExt, getINPVersion().getSolutionFinaleExtension()));
    return res;
  }

  /**
   * @return true
   */
  @Override
  public boolean canImportEvolution() {
    return true;
  }

  /**
   * @param _f le repertoire de dest.
   * @return le fichier unique
   */
  public File enregistrerUniqueSousRepertoire(final File _f) {
    if (!_f.isDirectory()) {
      ui_.error(TrResource.getS("Un r�pertoire est attendu"), TrResource.getS("Le fichier {0} n'est pas un r�pertoire",
          _f.getAbsolutePath()), false);
      return null;
    }
    File dest = null;
    try {
      dest = File.createTempFile(title_, INPFileFormat.getInstance().getExtensions()[0], _f);
    } catch (final IOException _e) {
      FuLog.warning(_e);
    }
    if (dest == null) {
      ui_.error(TrResource.getS("Erreur"), "Il est impossible de cr�er un fichier dans le dossier"
          + CtuluLibString.LINE_SEP + _f.getAbsolutePath(), false);
      return null;
    }
    CtuluLibFile.copyFile(fINP_, dest);
    return dest;
  }

  /**
   * @return les params issu du dico
   */
  public DicoParams getDicoParam() {
    return params_.getDicoParams();
  }

  /**
   * @return le manager des evolutions
   */
  @Override
  public TrCourbeTemporelleManager getEvolMng() {
    if (courbeMng_ == null) {
      courbeMng_ = new TrCourbeTemporelleManager(getH2dParametres());
    }
    return courbeMng_;
  }

  /**
   * @return le nom du fichier sans extension
   */
  public String getFicNameSansExt() {
    return CtuluLibFile.getSansExtension(fINP_.getName());
  }

  public File getFile() {
    return fINP_;
  }

  @Override
  public H2dParameters getH2dParametres() {
    return getH2dRefluxParametres();
  }

  public H2dRefluxParameters getH2dRefluxParametres() {
    return params_;
  }

  /**
   * @return la version utilisee
   */
  public H2dRefluxDicoVersion getINPVersion() {
    return (H2dRefluxDicoVersion) params_.getVersion();
  }

  /**
   * @return le maillage
   */
  public EfGridInterface getMaillage() {
    return params_.getMaillage();
  }

  public File getOutFile() {
    return new File(CtuluLibFile.getSansExtension(fINP_.getAbsolutePath()) + ".out");
  }

  public H2dRefluxBcManager getRefluxBcMng() {
    return params_.getRefluxClManager();
  }

  public TrRefluxProjectDispatcherListener getRefluxListener() {
    return (TrRefluxProjectDispatcherListener) getH2dRefluxParametres().getListener();
  }

  public String getTitle() {
    return title_;
  }

  /**
   * @return l'ui (TrImplementation)
   */
  public FudaaUI getUI() {
    return ui_;
  }

  /**
   * @return true
   */
  @Override
  public boolean isGeometrieLoaded() {
    return true;
  }

  @Override
  public boolean loadData(final ProgressionInterface _interface) {
    return true;
  }

  /**
   * Sauve les parametres. Ouvre une boite de dialogue si necessaire.
   * 
   * @param _prog la barre de progression
   * @return true si l'operation n'a pas �t� annul�e.
   */
  public boolean save(final ProgressionInterface _prog) {
    if (fINP_ == null) {
      FuLog.warning(new Throwable());
    }
    final String res = CtuluLibFile.canWrite(fINP_);
    // on ne peut pas ecrire
    if (res != null) {
      getUI().error(res);
      return false;
    }
    boolean b;
    if (fileIsProject_) {
      b = internSave(fINP_, _prog, false);
    } else {
      fileIsProject_ = internSave(fINP_, _prog, true);
      b = fileIsProject_;
    }
    if (b && CtuluLibMessage.INFO) {
      CtuluLibMessage.info("save project in " + fINP_.getAbsolutePath());
    }
    return b;
  }

  /**
   * Sauve le projet dans un autre fichier.
   * 
   * @param _prog la barre de progression
   */
  public boolean saveAs(final ProgressionInterface _prog, final File _fic) {
    if (_fic == null) { return false; }
    if (fINP_ != null && fINP_.equals(_fic)) { return save(_prog); }
    if (ui_.getLauncher().isAlreadyUsed(_fic)) {
      errorFileAlreadyUsed(_fic.getName());
      return false;
    }
    if (internSave(_fic, _prog, true)) {
      if (fINP_ == null) {
        title_ = _fic.getName();
        final int i = title_.indexOf('.');
        if (i > 0) {
          title_ = title_.substring(0, i);
        }
      }
      fINP_ = _fic;
      fileIsProject_ = true;
      if (CtuluLibMessage.INFO) {
        CtuluLibMessage.info("save project in " + fINP_.getAbsolutePath());
      }
      return true;
    }
    return false;
  }

  /**
   * @param _prog la barre de progression
   */
  public File saveCopy(final ProgressionInterface _prog, final File _fic) {
    if (ui_.getLauncher().isAlreadyUsed(_fic)) {
      errorFileAlreadyUsed(_fic.getName());
      return null;
    }
    if ((_fic != null) && (internSave(_fic, _prog, true))) {
      if (CtuluLibMessage.INFO) {
        CtuluLibMessage.info("save copy in " + _fic.getAbsolutePath());
      }
      return _fic;
    }
    return null;
  }

  public File getDirBase() {
    return getFile().getParentFile();
  }
}
