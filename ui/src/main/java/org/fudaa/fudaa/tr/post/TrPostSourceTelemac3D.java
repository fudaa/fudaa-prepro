/*
 * @creation 16 mars 07
 *
 * @modification $Date: 2007-05-04 14:01:51 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.*;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridAbstract;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinInterface;
import org.fudaa.dodico.h2d.H2DLib;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.dodico.telemac.io.TelemacVariableMapper;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreatedDefault;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreatedNorme3D;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.*;

/**
 * @author fred deniger
 * @version $Id: TrPostSourceTelemac3D.java,v 1.5 2007-05-04 14:01:51 deniger Exp $
 */
public final class TrPostSourceTelemac3D extends TrPostSourceAbstractFromIdx {
  final int nbPlan_;
  TrPostSourceSerafin ser_;
  CtuluPermanentList cotes_;
  int[] iparams_;
  int[] ipobo_;
  long idate;

  public long getIdate() {
    return idate;
  }

  public void setIdate(long idate) {
    this.idate = idate;
  }

  private TrPostSourceTelemac3D(final int nbPlan, final EfGridInterface efGridInterface, final TrPostSourceSerafin sourceSerafin, final double[] time,
                                final H2dVariableType[] types, final CtuluUI ctuluUI) {
    super(sourceSerafin.getTitle(), efGridInterface, time, types, ctuluUI);
    ser_ = sourceSerafin;
    nbPlan_ = nbPlan;
  }

  @Override
  protected void buildDefaultVectors() {
  }

  void setVarAndFleche(final Map _m, final List _fleche) {
    varCreateData_ = new HashMap(_m.size());
    varCreateData_.putAll(_m);
    fleches_ = (TrPostFlecheContent[]) _fleche.toArray(new TrPostFlecheContent[_fleche.size()]);
    Arrays.sort(fleches_, TrPostDataCreatedDefault.NAME_COMPARATOR);
    updateVarList();
    fireFlecheListModelChanged();
  }

  /**
   * @param _varIdx l'indice de la variable expos�e
   * @return l'indice dans le fichier serafin
   */
  int get3dVarIdx(final int _varIdx) {
    return nbPlan_ == 0 ? 0 : ((int) Math.floor(((double)_varIdx) / nbPlan_));
  }

  /**
   * @return l'indice du plan.
   * @pa ram _varIdx l'indice de la variable expos�e
   */
  int getPlan(final int _varIdx) {
    return _varIdx % nbPlan_;
  }

  @Override
  public EfData getInitData(final int _varIdx, final int _timeIdx) {
    // on recupere les valeurs pour la variable concern�e
    final EfData values = ser_.getReaderSerafin().getInitData(get3dVarIdx(_varIdx), _timeIdx);
    // on reconstruit les valeurs
    final int ptsNb = getPtNb();
    final double[] res = new double[ptsNb];
    final int plan = getPlan(_varIdx);
    for (int i = 0; i < res.length; i++) {
      res[i] = values.getValue(ptsNb * plan + i);
    }
    return new EfDataNode(res);
  }

  private int getPtNb() {
    return getGrid().getPtsNb();
  }

  @Override
  public double getInitData(final int _varIdx, final int _timeIdx, final int _ptIdx) throws IOException {
    return ser_.getReaderSerafin().getInitData(get3dVarIdx(_varIdx), _timeIdx, getPtNb() * getPlan(_varIdx) + _ptIdx);
  }

  public TrPostInspectorReader createWatcher(final TrPostProjet _proj, final boolean _auto) {
    return null;
  }

  @Override
  public void close() {
    ser_.close();
  }

  @Override
  public boolean containsElementVar() {
    return false;
  }

  /**
   * Reconstruit une source 2d, en prenant compte les normes de telemac 3D. Les nbPt premier points correspondent au
   * maillage 3D.
   *
   * @param _init
   * @param _ser
   * @param _prog
   * @param _ui
   */
  public static TrPostSourceTelemac3D build(final TrPostSourceSerafin _init, final double[] _time, final SerafinInterface _ser, final Map _shortName,
                                            final ProgressionInterface _prog, final CtuluUI _ui) {
    final int nbplan = SerafinFileFormat.getNbPlanIn3D(_ser.getIparam());
    final EfGridInterface gridInit = _ser.getGrid();

    final int nbElt = gridInit.getEltNb() / (nbplan - 1);
    // le coup des poteaux et des intervalles
    // s'il y a 2 plans, il y 3 rang�es de noeuds
    final int nbPt = gridInit.getPtsNb() / (nbplan);
    final EfElement[] newElt = new EfElement[nbElt];
    for (int i = 0; i < nbElt; i++) {
      final EfElement eltInit = gridInit.getElement(i);
      // dans telemac 3D, on a des triangles 3D: les 3 premiers indices sont les indices du triangle du dessus
      // les 3 derniers celui du dessous
      // on recupere les 3 premiers, pour construire
      final int[] idxElt = new int[3];
      for (int j = 0; j < 3; j++) {
        idxElt[j] = eltInit.getPtIndex(j);
      }
      newElt[i] = new EfElement(idxElt);
    }

    final EfGridInterface g = new Grid3D2DAdapter(newElt, nbPt, gridInit);
    final CtuluAnalyze analyze = new CtuluAnalyze();
    g.computeBord(/* ser.getPtsFrontiere(), */_prog, analyze);
    g.createIndexRegular(_prog);
    if (_ui != null && _ui.manageAnalyzeAndIsFatal(analyze)) {
      return null;
    }
    final int nb = _ser.getValueNb();
    final H2dVariableTypeCreated[] variable = new H2dVariableTypeCreated[nb * nbplan];
    final TelemacVariableMapper mapper = new TelemacVariableMapper();
    final NumberFormat fmt = CtuluLibString.getFormatForIndexingInteger(nbplan);
    final List vars = new ArrayList(nb);
    final List coteVar = new ArrayList(nb);
    for (int i = 0; i < nb; i++) {
      String name = _ser.getValueId(i);
      final boolean isCoteZ = TelemacVariableMapper.is3DBottom(name);
      H2dVariableType t = mapper.getUsedKnownVar(name);
      if (t == null) {
        t = H2dVariableType.createTempVar(name, _shortName);
      } else {
        name = t.getName();
      }
      vars.add(t);
      // on reutilise t pour la suite
      // indique la variable parente
      if (t.getParentVariable() != null) {
        t = t.getParentVariable();
      }
      for (int k = 0; k < nbplan; k++) {
        final int j = i * nbplan + k;
        variable[j] = H2dVariableType.createTempVar(name + CtuluLibString.ESPACE + fmt.format(k + 1L), _shortName);
        if (isCoteZ) {
          coteVar.add(variable[j]);
        }
        variable[j].setParent(t);
      }
    }
    // la source
    final TrPostSourceTelemac3D source = new TrPostSourceTelemac3D(nbplan, g, _init, _time, variable, _ui);
    source.setCotes(new CtuluPermanentList(coteVar));
    // on va construire les variables compos�es: les vitesses 2d
    // les normes 3D
    final Map userCreatedVar = new HashMap();
    final List fleches = new ArrayList();
    for (int i = 0; i < vars.size(); i++) {
      final String name = ((H2dVariableType) vars.get(i)).getName();
      String endx = " u";
      String endy = null;
      String endz = null;
      if (name.endsWith(endx)) {
        endy = " v";
        endz = " w";
      } else {
        endx = "X";
        if (name.endsWith(endx)) {
          endy = "Y";
          endz = "Z";
        }
      }
      // le nom ne finit ni par X ni par ' u': on ignore la suite
      if (endy == null) {
        continue;
      }
      // le nom de base
      final String nameBase = name.substring(0, name.length() - endx.length());
      // on recherche dans les variables cr�es un equivalent y
      final int idxY = H2DLib.getIdxVarWithName(vars, nameBase + endy);
      // un equivalement Y existe
      if (idxY >= 0) {
        // on cree les vecteurs 2D
        for (int k = 0; k < nbplan; k++) {
          final H2dVariableType vx = variable[i * nbplan + k];
          final H2dVariableType vy = variable[idxY * nbplan + k];
          final H2dVariableTypeCreated var2d = H2dVariableType.createTempVar(nameBase + " 2D " + fmt.format(k + 1L), _shortName);
          fleches.add(new TrPostFlecheContentDefaut(source, var2d, vx, vy));
          var2d.setUnit(H2dVariableType.VITESSE.getCommonUnitString());
        }
        final int idxW = H2DLib.getIdxVarWithName(vars, nameBase + endz);
        // un equivalent en z existe: on cree la norme 3D
        if (idxW >= 0) {
          for (int k = 0; k < nbplan; k++) {
            final H2dVariableType vx = variable[i * nbplan + k];
            final H2dVariableType vy = variable[idxY * nbplan + k];
            final H2dVariableType vw = variable[idxW * nbplan + k];
            final H2dVariableType var3d = H2dVariableType.createTempVar(nameBase + CtuluLibString.ESPACE + fmt.format(k + 1L), _shortName);
            userCreatedVar.put(var3d, new TrPostDataCreatedNorme3D(var3d.getName(), source, vx, vy, vw));
          }
        }
      }
    }
    source.ipobo_ = _ser.getIpoboInitial();
    source.iparams_ = _ser.getIparam();
    source.idate = _ser.getIdate();
    source.setVarAndFleche(userCreatedVar, fleches);
    return source;
  }

  private static class Grid3D2DAdapter extends EfGridAbstract {
    final EfGridInterface grid3d_;
    final int nbPt_;

    public Grid3D2DAdapter(final EfElement[] _elts, final int _nbPt, final EfGridInterface _grid3D) {
      super(_elts);
      nbPt_ = _nbPt;
      grid3d_ = _grid3D;
    }

    @Override
    protected boolean setZIntern(final int _i, final double _newV) {
      return false;
    }

    @Override
    public int getPtsNb() {
      return nbPt_;
    }

    @Override
    public double getPtX(final int _i) {
      return grid3d_.getPtX(_i);
    }

    @Override
    public double getPtY(final int _i) {
      return grid3d_.getPtY(_i);
    }

    @Override
    public double getPtZ(final int _i) {
      return grid3d_.getPtZ(_i);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPt(int _i, double _x, double _y) {
      this.setPt(_i, _x, _y, 0.0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPt(int _i, double _x, double _y, double _z) {
      grid3d_.setPt(_i, _x, _y, _z);
    }
  }

  public int getNbPlan() {
    return nbPlan_;
  }

  public CtuluPermanentList getCotes() {
    return cotes_;
  }

  void setCotes(final CtuluPermanentList _cotes) {
    cotes_ = _cotes;
  }

  public TrPostSourceSerafin getInit3DSource() {
    return ser_;
  }

  public int[] getIparams() {
    return CtuluLibArray.copy(iparams_);
  }

  public int[] getIpobo() {
    return CtuluLibArray.copy(ipobo_);
  }

  @Override
  public Collection<File> getFiles() {
    return ser_.getFiles();
  }
}
