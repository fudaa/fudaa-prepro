/*
 * @creation 20 avr. 2006
 *
 * @modification $Date: 2007-02-02 11:22:24 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.*;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.editor.CtuluExprGUI;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.fudaa.commun.impl.FudaaDurationChooserPanel;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class TrPostTimeFmtPanel extends CtuluDialogPanel {
    final BuCheckBox cbFormule_;
    CtuluExpr expr_;
    final TrPostSource source;
    final BuTextField tfFormule_;
    final String initExpr_;
    final FudaaDurationChooserPanel pn_;

    public TrPostTimeFmtPanel(final TrPostSource _prj) {
        source = _prj;
        CtuluNumberFormatI[] buildDefaultFormatter = FudaaDurationChooserPanel.buildDefaultFormatter();
        List<CtuluNumberFormatI> formatters = new ArrayList<CtuluNumberFormatI>();
        formatters.addAll(Arrays.asList(buildDefaultFormatter));

        CtuluNumberFormatI currentTimeFormatter = source.getTimeFormatter();
        if (currentTimeFormatter instanceof CtuluDurationDateFormatter) {
            currentTimeFormatter = ((CtuluDurationDateFormatter) currentTimeFormatter).getCopy();
            formatters.add(currentTimeFormatter);
        } else {
            formatters.add(new CtuluDurationDateFormatter(null, source.getReferenceDateInMillis()));
        }
        pn_ = new FudaaDurationChooserPanel((CtuluNumberFormatI[]) formatters.toArray(new CtuluNumberFormatI[formatters.size()]), currentTimeFormatter);
        pn_.setDefaultReferenceDate(source.getReferenceDateInMillis());
        final boolean isExpr = source.getTime().isExprAvailable();
        expr_ = isExpr ? buildExpr() : null;
        initExpr_ = isExpr ? source.getTime().getUsedFormule() : CtuluLibString.EMPTY_STRING;
        cbFormule_ = new BuCheckBox(TrResource.getS("Modifier"));
        tfFormule_ = new BuTextField();
        tfFormule_.setColumns(10);
        final BuButton btFormula = new BuButton();
        btFormula.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent _e) {
                expr_.getParser().parseExpression(tfFormule_.getText());
                final CtuluExprGUI gui = new CtuluExprGUI(expr_);
                final CtuluDialogPanel pnExpr = new CtuluDialogPanel() {
                    @Override
                    public boolean apply() {
                        tfFormule_.setText(gui.getExprText());
                        return true;
                    }

                    @Override
                    public boolean isDataValid() {
                        return gui.isExprValide();
                    }
                };
                pn_.setHelpText(getFmtHelpText());
                pnExpr.setLayout(new BuBorderLayout());
                pnExpr.add(gui, BuBorderLayout.CENTER);
                pnExpr.afficheModale(CtuluLibSwing.getActiveWindow(), TrResource.getS("Editeur de formules"));
            }
        });

        cbFormule_.getModel().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent _e) {
                final boolean b = cbFormule_.isSelected();
                tfFormule_.setEnabled(b);
                btFormula.setEnabled(b);
                if (b && expr_ == null) {
                    expr_ = buildExpr();
                }
            }
        });
        cbFormule_.setSelected(isExpr);
        tfFormule_.setEnabled(isExpr);
        btFormula.setEnabled(isExpr);
        tfFormule_.setText(initExpr_);
        setLayout(new BuVerticalLayout(4, true, false));
        pn_.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Format")));
        add(pn_);
        final BuPanel pnForm = new BuPanel();
        pnForm.setLayout(new BuGridLayout(2, 2, 2, false, false));
        pnForm.add(cbFormule_);
        pnForm.add(tfFormule_);
        pnForm.add(new BuLabel());
        btFormula.setText(CtuluLib.getS("Editeur d'expressions"));
        pnForm.add(btFormula);
        pnForm.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Modifier les pas de temps")));
        add(pnForm);
    }

    protected CtuluExpr buildExpr() {
        return source.getTime().buildExpr();
    }

    @Override
    public boolean apply() {
        boolean changed = false;
        if (cbFormule_.isSelected() && !initExpr_.equals(tfFormule_.getText())) {
            changed = true;
            source.getTime().setExpr(expr_.getParser(), null);
        } else if (!cbFormule_.isSelected()) {
            source.getTime().setExpr(null, null);
        }

        changed |= source.setTimeFormat(pn_.getFormatter());
        return true;
    }

    @Override
    public boolean isDataValid() {
        if (cbFormule_.isSelected()) {
            final String txt = tfFormule_.getText();
            expr_.getParser().parseExpression(txt);
            if (expr_.getParser().hasError()) {
                setErrorText(TrResource.getS("La formule est erron�e"));
                cbFormule_.setForeground(Color.RED);
                tfFormule_.setToolTipText(expr_.getParser().getHtmlError());
                return false;
            }
        }
        cbFormule_.setForeground(Color.BLACK);
        return true;
    }

    String getFmtHelpText() {
        return "<html><body><ul><li>" + TrResource.getS("La variable t repr�sente la valeur lue dans le fichier de r�sultat") + "</li><li>"
            + CtuluLib.getS("Cliquer deux fois sur une variable ou une fonction pour l'ins�rer") + "</li><li>"
            + CtuluLib.getS("Valider votre expression en utilisant l'action \"Valider\"") + "</li></ul></body></html>";
    }

    /**
     * L'action qui permet de modifier les pas de temps.
     */
    protected static void updateTimeStepFmt(final TrPostSource _proj, final Frame _impl) {
        final CtuluDialogPanel pnGlob = new TrPostTimeFmtPanel(_proj);
        pnGlob.afficheModale(_impl, TrResource.getS("Formater/modifier les pas de temps"), CtuluDialog.OK_CANCEL_APPLY_OPTION);
    }
}
