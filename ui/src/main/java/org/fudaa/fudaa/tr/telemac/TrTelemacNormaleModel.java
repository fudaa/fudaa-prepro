/*
 * @creation 29 janv. 07
 * @modification $Date: 2007-02-02 11:22:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcManager;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBordParFrontiere;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBoundary;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBoundaryCondition;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZModeleSegment;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.fudaa.meshviewer.model.MvFrontierModelDefault;

/**
 * Une classe permettant de tracer les normales aux noeuds frontières. a class allowing to draw the normals to the boundary nodes
 *
 * @author fred deniger
 * @version $Id: TrTelemacNormaleModel.java,v 1.2 2007-02-02 11:22:25 deniger Exp $
 */
public class TrTelemacNormaleModel extends MvFrontierModelDefault implements ZModeleSegment {

  final H2dTelemacBcManager bc_;
  final int nbPtFrontier_;

  public TrTelemacNormaleModel(final H2dTelemacBcManager _bc) {
    super(_bc.getGrid());
    bc_ = _bc;
    nbPtFrontier_ = _bc.getNbTotalPoint();
  }

  @Override
  public void prepare() {
  }

  H2dTelemacBoundaryCondition getBc(final int _idxOnFr) {
    final int[] idxFrIdxPt = new int[2];
    getIdxFrIdxOnFrFromFrontier(_idxOnFr, idxFrIdxPt);
    return ((H2dTelemacBordParFrontiere) bc_.getBlockFrontier(idxFrIdxPt[0])).getTelemacCl(idxFrIdxPt[1]);
  }

  H2dTelemacBoundary getBoundary(final int _idxOnFr) {
    final int[] idxFrIdxPt = new int[2];
    getIdxFrIdxOnFrFromFrontier(_idxOnFr, idxFrIdxPt);
    return (H2dTelemacBoundary) bc_.getBlockFrontier(idxFrIdxPt[0]).getBordContainingIdx(idxFrIdxPt[1]);
  }

  @Override
  public double getNorme(final int _i) {
    return 1;
  }

  @Override
  public double getVy(final int _i) {
    final GrVecteur vi = getVec(_i);
    return vi == null ? 0 : vi.y_;
  }

  @Override
  public double getVx(final int _i) {
    final GrVecteur vi = getVec(_i);
    return vi == null ? 0 : vi.x_;
  }

  @Override
  public double getX(final int _i) {
    return bc_.getGrid().getPtX(getIdxGlobal(_i));
  }

  @Override
  public double getY(final int _i) {
    return bc_.getGrid().getPtY(getIdxGlobal(_i));
  }

  @Override
  public double getZ1(final int _i) {
    return bc_.getGrid().getPtZ(getIdxGlobal(_i));
  }

  @Override
  public double getZ2(final int _i) {
    return bc_.getGrid().getPtZ(getIdxGlobal(_i));
  }

  int getIdxGlobal(final int _idxOnFr) {
    return bc_.getGrid().getFrontiers().getIdxGlobalFrom(_idxOnFr);
  }

  private GrVecteur getVec(final int _i) {
    final H2dTelemacBoundary bd = getBoundary(_i);
    if (bd.getType() != H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE) {
      return null;
    }
    final int[] idxFrIdxPt = new int[2];
    getIdxFrIdxOnFrFromFrontier(_i, idxFrIdxPt);
    final H2dTelemacBordParFrontiere blockFrontier = (H2dTelemacBordParFrontiere) bc_.getBlockFrontier(idxFrIdxPt[0]);
    final H2dTelemacBoundary bci = (H2dTelemacBoundary) (blockFrontier).getBordContainingIdx(idxFrIdxPt[1]);
    final H2dVariableType[] var = bc_.getVariablesForPoint(bci);
    if (!CtuluLibArray.containsObject(var, H2dVariableType.VITESSE_U)) {
      return null;
    }
    final H2dTelemacBoundaryCondition bc = blockFrontier.getTelemacCl(idxFrIdxPt[1]);
    final double qn = bc.getU();

    final boolean uv = CtuluLibArray.containsObject(var, H2dVariableType.VITESSE_V);
    final double qt = uv ? bc.getV() : 0;
    if (uv) {
      return new GrVecteur(qn, qt, 0);
    }
    // dans ce cas, seul qn doit calcule
    // on calcul l'indice du point suivant
    // in this case, only somebody must compute
    // we compute the index of the following point
    final int iNext = (idxFrIdxPt[1] + 1) % bc_.getBlockFrontier(idxFrIdxPt[0]).getNbPt();
    final int iReal = getGlobalIdx(idxFrIdxPt[0], idxFrIdxPt[1]);
    final int iNextReal = getGlobalIdx(idxFrIdxPt[0], iNext);
    final double x2 = bc_.getGrid().getPtX(iNextReal);
    final double y2 = bc_.getGrid().getPtY(iNextReal);
    final GrVecteur vecN = new GrVecteur(-(y2 - bc_.getGrid().getPtY(iReal)), x2 - bc_.getGrid().getPtX(iReal), 0);
    vecN.autoNormaliseXY();
    // vecT.autoMultiplication(qt);
    vecN.autoMultiplication(qn);
    // vecN.autoAddition(vecT);
    return vecN;

  }

  @Override
  public boolean segment(final GrSegment _s, final int _i, final boolean _force) {

    final GrVecteur v = getVec(_i);
    if (v == null) {
      return false;
    }
    if (_s.o_ == null) {
      _s.o_ = new GrPoint();
    }
    if (_s.e_ == null) {
      _s.e_ = new GrPoint();
    }
    _s.o_.setCoordonnees(getX(_i), getY(_i), 0);
    _s.e_.initialiseAvec(_s.o_);
    _s.e_.autoAddition(v);

    return true;
  }

  @Override
  public int getNombre() {
    return nbPtFrontier_;
  }

  @Override
  public boolean isValuesTableAvailable() {
    return false;
  }
}
