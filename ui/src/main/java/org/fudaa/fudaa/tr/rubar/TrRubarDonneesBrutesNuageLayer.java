/**
 * @creation 4 oct. 2004
 * @modification $Date: 2007-03-30 15:40:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.h2d.rubar.H2dRubarDonneesBrutes;
import org.fudaa.dodico.h2d.rubar.H2dRubarDonneesBrutesListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarDonneesBrutesMng;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.tr.common.TrResource;

import java.awt.*;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarDonneesBrutesNuageLayer.java,v 1.9 2005/10/14 15:36:55 deniger Exp $
 */
public class TrRubarDonneesBrutesNuageLayer extends ZCalquePointEditable implements H2dRubarDonneesBrutesListener {
  private static class DelegateNuagePoint extends ZModelePointEditable {
    transient H2dRubarDonneesBrutes br_;
    H2dRubarDonneesBrutes.NuagePoint target_;

    protected DelegateNuagePoint(final H2dRubarDonneesBrutes _br) {
      super(_br == null ? null : _br.getNuage());
      target_ = (H2dRubarDonneesBrutes.NuagePoint) super.getGeomData();
      br_ = _br;
    }

    @Override
    public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
      final int nbSelected = _layer.isSelectionEmpty() ? 0 : _layer.getLayerSelection().getNbSelectedIndex();
      _d.put(TrResource.getS("Nombre de points"), CtuluLibString.getString(target_.getNumPoints()));
      _d.put(TrResource.getS("Nombre de points sélectionnés"), CtuluLibString.getString(nbSelected));
      if (nbSelected == 2) {
        final int i = _layer.getLayerSelection().getMaxIndex();
        final int i2 = _layer.getLayerSelection().getMinIndex();
        _d.put(EbliLib.getS("Distance entre les 2 points"), CtuluLib.DEFAULT_NUMBER_FORMAT.format(CtuluLibGeometrie
            .getDistance(getX(i), getY(i), getX(i2), getY(i2))));
        return;
      }
      if (nbSelected == 1) {
        final CtuluNumberFormatI xy = br_.getFmt().getXY().getNumberFormat();
        final int idx = _layer.getLayerSelection().getMaxIndex();
        _d.put("X", xy.format(target_.getX(idx)));
        _d.put("Y", xy.format(target_.getY(idx)));
        final int nbVals = br_.getNbVariable();
        for (int i = 0; i < nbVals; i++) {
          final Object o = br_.getVariableId(i);
          String name = o.toString();
          if (o == H2dVariableType.DEBIT_X) {
            name = name + " / " + H2dVariableType.VITESSE_U.toString();
          } else if (o == H2dVariableType.DEBIT_Y) {
            name = name + " / " + H2dVariableType.VITESSE_V.toString();
          }
          _d.put(name, br_.getFmt().getFormatterFor((H2dVariableType) o).format(target_.getDoubleValue(i, idx)));
        }
      }
    }

    public boolean getDataRange(final CtuluRange _b) {
      return false;
    }

    @Override
    public GrBoite getDomaine() {
      if (target_ == null) {
        return null;
      }
      return super.getDomaine();
    }

    public void getGrPoint(final int _i, final GrPoint _p) {
      if (target_ == null) {
        return;
      }
      _p.setCoordonnees(super.getX(_i), super.getY(_i), 0);
    }

    @Override
    public int getNombre() {
      return target_ == null ? 0 : target_.getNumPoints();
    }

    @Override
    public Object getObject(final int _ind) {
      return null;
    }

    @Override
    public double getX(final int _i) {
      if (target_ == null) {
        return 0;
      }
      return target_.getX(_i);
    }

    @Override
    public double getY(final int _i) {
      if (target_ == null) {
        return 0;
      }
      return target_.getY(_i);
    }

    public double getZ(final int _i) {
      return 0;
    }

    public boolean isValuesTableAvailableFromModel() {
      return false;
    }

    @Override
    public boolean point(final GrPoint _p, final int _i, final boolean _force) {
      if (target_ == null) {
        return false;
      }
      getGrPoint(_i, _p);
      return true;
    }
  }

  @Override
  public String editSelected() {
    final EbliActionInterface[] actions = getActions();
    if (actions.length > 0) {
      actions[0].actionPerformed(null);
      return null;
    } else {
      return super.editSelected();
    }
  }

  protected final static String getLayerName(final String _id) {
    return "cqInitialNuageData" + _id;
  }

  transient String id_;

  /**
   */
  public TrRubarDonneesBrutesNuageLayer(final H2dRubarDonneesBrutesMng _mng, final String _id, final FSigEditor _editor) {
    super(new DelegateNuagePoint(_mng.get(_id)), _editor);
    setIconModel(0, new TraceIconModel(TraceIcon.CROIX, 3, Color.GRAY));
    setForeground(Color.GRAY);
    id_ = _id;
    setName(getLayerName(id_));
    _mng.addListener(this);
  }

  H2dRubarDonneesBrutes getBr() {
    return ((DelegateNuagePoint) modele_).br_;
  }

  @Override
  public void donneesBrutesDataChanged(final H2dRubarDonneesBrutes _source) {
    if (_source == getBr()) {
      repaint();
    }
  }

  @Override
  public void donneesBrutesDataNombreChanged(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesNuageDataChanged(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesNuageDataNombreChanged(final H2dRubarDonneesBrutes _source) {
    if (_source == getBr()) {
      repaint();
    }
  }

  @Override
  public void donneesBrutesNuageSupportAddedOrRemoved(final H2dRubarDonneesBrutes _source) {
    if (_source == getBr()) {
      repaint();
    }
  }

  @Override
  public void donneesBrutesNuageSupportChanged(final H2dRubarDonneesBrutes _source) {
    if (_source == getBr()) {
      repaint();
    }
  }

  @Override
  public void donneesBrutesSupportAddedOrRemoved(final H2dRubarDonneesBrutes _source) {

  }

  @Override
  public void donneesBrutesSupportChanged(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesTypeChanged(final H2dRubarDonneesBrutes _source) {
  }
}
