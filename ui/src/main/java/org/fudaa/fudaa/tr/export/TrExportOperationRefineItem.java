package org.fudaa.fudaa.tr.export;

import javax.swing.JComponent;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.operation.EfOperation;
import org.fudaa.dodico.ef.operation.refine.EfOperationRefineEdge;
import org.fudaa.fudaa.meshviewer.export.MvExportOperationBuilderInterface;
import org.fudaa.fudaa.meshviewer.export.MvExportOperationBuilderSelection;
import org.fudaa.fudaa.meshviewer.export.MvExportOperationItem;
import org.fudaa.fudaa.meshviewer.export.MvExportOperationSelectionItem;

/**
 * @author deniger
 */
public class TrExportOperationRefineItem extends MvExportOperationItem {
  protected class Builder implements MvExportOperationBuilderInterface {

    final MvExportOperationBuilderSelection preFilter;

    private Builder(final MvExportOperationBuilderSelection preFilter) {
      super();
      this.preFilter = preFilter;
    }

    @Override
    public void buildOperation(final ProgressionInterface progression) {
      if (preFilter != null) {
        preFilter.buildOperation(progression);
      }

    }

    @Override
    public EfOperation getOperation(final int tidx) {
      EfOperationRefineEdge res = new EfOperationRefineEdge();
      if (preFilter == null) { return res; }
      res.setSelectedElt(preFilter.getSelectedMeshes());
      return res;
    }

    @Override
    public void stop() {
      if (preFilter != null) preFilter.stop();

    }

  }

  String title;

  MvExportOperationSelectionItem preFilter;

  public TrExportOperationRefineItem(String title, MvExportOperationSelectionItem preFilter) {
    this.title = title;
    this.preFilter = preFilter;
  }

  public TrExportOperationRefineItem(String title) {
    this(title, null);
  }

  public TrExportOperationRefineItem() {
    this(null, null);
  }

  @Override
  public MvExportOperationBuilderInterface getBuilder(EfGridData src) {
    return new Builder(preFilter == null ? null : preFilter.getBuilder(src));
  }

  @Override
  public JComponent getConfigureComponent() {
    if (preFilter == null) return null;
    return preFilter.getConfigureComponent();
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public boolean isEnable() {
    return true;
  }

  @Override
  public String getErrorMessage() {
    return preFilter == null ? null : preFilter.getErrorMessage();
  }

  public void setTitle(String title) {
    this.title = title;
  }

}
