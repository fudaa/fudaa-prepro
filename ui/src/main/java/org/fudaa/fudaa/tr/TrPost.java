/**
 * @creation 24 mai 2004
 * @modification $Date: 2006-12-05 10:18:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr;

/**
 * @author Fred Deniger
 * @version $Id: TrPost.java,v 1.9 2006-12-05 10:18:19 deniger Exp $
 */
public final class TrPost {

  private TrPost() {

  }

  public static void main(final String[] _args) {
    TrInvocationHandler.initThreaExceptionHandler();
    TrLauncherDefault.init();
    TrLauncherDefault.launch(_args, null, new TrPostImplementation());
  }

}