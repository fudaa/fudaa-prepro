package org.fudaa.fudaa.tr.post;

import java.awt.event.ActionEvent;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.commun.impl.FudaaPanelTask;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostTrajectoireTaskModel;

/**
 * Permet d'afficher le dialogue pour les trajectoire/ligne de courant
 * Allow to display the dialog for the trajectory/curent line
 */
@SuppressWarnings("serial")
public class TrPostActionLigneCourants extends EbliActionSimple {

  TrPostVisuPanel calque_;
  TrPostCommonImplementation impl_;
  public TrPostActionLigneCourants(final TrPostVisuPanel panel, TrPostCommonImplementation impl) {
    super(TrResource.getS("Calcul des lignes de courants/trajectoires"), CtuluResource.CTULU
        .getIcon("crystal_oscilloscope.png"), "TRAJ");
    calque_ = panel;
    impl_ = impl;
  }

  TrPostTrajectoireTaskModel taskTrajectories = null;
  
  @Override
  public void actionPerformed(final ActionEvent _e) {
	  if(taskTrajectories == null)
		  taskTrajectories = new TrPostTrajectoireTaskModel(calque_,impl_);
	  taskTrajectories.setCalque_(calque_);
    new FudaaPanelTask(calque_.getCtuluUI(), taskTrajectories).afficheDialog();
  }

}
