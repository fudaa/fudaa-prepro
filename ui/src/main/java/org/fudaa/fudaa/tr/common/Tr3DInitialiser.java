/*
 * @creation 22 juin 2006
 * @modification $Date: 2006-09-19 15:07:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import org.fudaa.ebli.commun.BJava3DVersionTest;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.tr.data.TrVisuPanel;

/**
 * @author fred deniger
 * @version $Id: Tr3DInitialiser.java,v 1.5 2006-09-19 15:07:30 deniger Exp $
 */
public final class Tr3DInitialiser {

  private Tr3DInitialiser() {}

  public static String get3dName() {
    return "3D";
  }

  public static EbliActionInterface create3dAction(final TrVisuPanel _visu, final String _name) {
    if (!BJava3DVersionTest.isJava3DFound()) {
      final EbliActionSimple simple = new EbliActionSimple(get3dName(), EbliResource.EBLI.getToolIcon("3d"),
          get3dName());
      simple.setEnabled(false);
      simple.setDefaultToolTip(BJava3DVersionTest.getHtmlInstallMessage(false));
      return simple;
    }
    return new TrAction3D(_visu, _name);
  }

}
