/**
 *  @creation     26 nov. 2004
 *  @modification $Date: 2007-05-04 14:01:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import java.util.Set;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.fudaa.tr.post.TrPostFlecheContent;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id: TrPostDataCreated.java,v 1.11 2007-05-04 14:01:51 deniger Exp $
 */
public interface TrPostDataCreated {

  void clearCache();

  boolean isEditable();
  
  
  /**
   * called after initialization
   */
  void restore();

  /**
   * @return true si bas�e sur une expression
   */
  boolean isExpr();

  TrPostDataCreatedSaver createSaver();

  /**
   * @param _idxTime l'indice du pas de temps voulu
   * @return les valeurs sur les points du modele
   */
  EfData getDataFor(int _idxTime);

  String getDescription();

  /**
   * @return null si cette valeur ne correspond pas a une fleche
   */
  TrPostFlecheContent isFleche();

  /**
   * @param _idxTime l'indice du pas de temps voulu
   * @param _idxObject l'indice du point ou de l'element
   * @return la valeur demandee
   */
  double getValue(int _idxTime, int _idxObject);

  /**
   * @param _res la liste a remplir avec toutes les variables utilisee par cette var et les var dependantes
   */
  void fillWhithAllUsedVar(Set _res);

  /**
   * @param _var la constante modifiee
   * @return true si une telle constante est utils�e
   */
  boolean updateConstantVar(final Variable _var);

  

}
