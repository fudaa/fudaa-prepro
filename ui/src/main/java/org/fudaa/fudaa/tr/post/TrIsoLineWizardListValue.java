/*
 * @creation 24 avr. 07
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.iterator.NumberIterator;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.ebli.palette.BPalettePlageTarget;
import org.fudaa.fudaa.tr.common.TrLib;

class TrIsoLineWizardListValue extends CtuluListEditorModel {

  final DecimalFormat fmt_ = CtuluLib.getDecimalFormat();
  final TrIsoLineWizard support_;
  EfData currentData_;

  public TrIsoLineWizardListValue(final TrIsoLineWizard _support) {
    fmt_.setMaximumFractionDigits(1);
    support_ = _support;
    try {
      final int idx = support_.listTimes_.getSelectedIndex();
      if (idx >= 0) {
        currentData_ = support_.getSource().getData(support_.currentVariable_, idx);
      }
    } catch (final IOException _evt) {
      FuLog.error(_evt);

    }
  }

  @Override
  public String getColumnName(final int _column) {
    return TrLib.getString("Valeurs");
  }

  public List getSelectedValues() {
    if (support_.dest_.getCalqueActif() instanceof BPalettePlageTarget) {
      final BPalettePlageTarget cq = (BPalettePlageTarget) support_.dest_.getCalqueActif();
      final BPalettePlageInterface plage = cq.getPaletteCouleur();
      final int nb = plage.getNbPlages();
      final List res = new ArrayList(nb);
      int inc = 1;
      // pour eviter de proposer trop de valeurs
      if (nb > 20) {
        inc = nb / 20;
      }
      addVal(plage, res, plage.getPlageInterface(0).getMin());
      for (int i = 0; i < nb; i += inc) {
        addVal(plage, res, plage.getPlageInterface(i).getMax());
      }
      return res;
    }
    return null;
  }

  private void addVal(final BPalettePlageInterface _plage, final List _res, final double _val) {
    if (_plage.getFormatter() == null) {
      _res.add(fmt_.format(_val));
    } else {
      _res.add(_plage.getFormatter().format(_val));
    }
  }

  public void initFor(final EfData _data, final List _l) {
    if (_data == null || _l == null) { return; }
    _l.clear();
    final NumberIterator it = new NumberIterator();
    it.init(_data.getMin(), _data.getMax(), 10);

    while (it.hasNext()) {
      _l.add(CtuluLib.DEFAULT_NUMBER_FORMAT.format(it.currentValue()));
      it.nextMajor();
    }
  }

  protected void updateOrCreateValues() {
    final List newValue = new ArrayList();
    final int idx = support_.listTimes_.getSelectedIndex();
    final CtuluVariable var = (CtuluVariable) support_.cbVariables_.getModel().getSelectedItem();
    if (idx >= 0 && var != null) {
      try {
        currentData_ = support_.getSource().getData(var, idx);
        if (currentData_ != null) {
          initFor(currentData_, newValue);
        }
      } catch (final IOException _evt) {
        FuLog.error(_evt);

      }
    }
    if (newValue.size() == 0) {
      newValue.add(CtuluLibString.ZERO);
      newValue.add(CtuluLibString.UN);
      newValue.add(CtuluLibString.DEUX);
    }
    setData(newValue);
  }

  @Override
  public Object createNewObject() {
    final int val = getRowCount();
    if (val >= 2) {
      final double v0 = Double.parseDouble(((String) getValueAt(getRowCount() - 2)));
      final double v1 = Double.parseDouble(((String) getValueAt(getRowCount() - 1)));
      return fmt_.format(v1 + v1 - v0);
    }
    if (currentData_ != null) {
      if (val == 1) {
        final double v1 = Double.parseDouble(((String) getValueAt(getRowCount() - 1)));
        final double inc = Math.abs((currentData_.getMax() - v1) / 10);
        return fmt_.format(v1 + inc);
      }
      return fmt_.format(currentData_.getMin());
    }
    return CtuluLibString.ZERO;
  }
}