package org.fudaa.fudaa.tr.data;

import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueGeometry;
import org.fudaa.ebli.calque.ZCalqueSelectionInteractionAbstract;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.calque.edition.ZEditorInterface;
import org.fudaa.ebli.calque.edition.ZModeleEditable;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.fudaa.meshviewer.layer.MvNodeLayer;
import org.fudaa.fudaa.meshviewer.model.MvNodeModel;
import org.fudaa.fudaa.tr.common.TrResource;

import java.awt.*;
import java.text.DecimalFormat;

public class TrNodeLayerEditable extends MvNodeLayer implements ZCalqueEditable {
  private final TrVisuPanelEditor visuPanelEditor;

  public TrNodeLayerEditable(MvNodeModel _modele, TrVisuPanelEditor visuPanelEditor) {
    super(_modele);
    this.visuPanelEditor = visuPanelEditor;
  }

  @Override
  public void paintDeplacement(Graphics2D _g2d, int _dx, int _dy, TraceIcon _ic) {
    if (!isSelectionEmpty()) {

      final GrMorphisme versEcran = getVersEcran();

      GrPoint ptScreen = null;
      TraceIcon icone = _ic;
      TraceIcon fusion = new TraceIcon(_ic);
      fusion.setCouleur(Color.RED);
      fusion.setTaille(_ic.getTaille() + 3);
      int min = selection_.getMinIndex();
      for (int i = selection_.getMaxIndex(); i >= min; i--) {
        if (selection_.isSelected(i)) {
          if (ptScreen == null) {
            ptScreen = new GrPoint();
          }
          modele_.point(ptScreen, i, true);
          ptScreen.autoApplique(versEcran);

          ptScreen.x_ += _dx;
          ptScreen.y_ += _dy;
          double x = ptScreen.x_;
          double y = ptScreen.y_;
          boolean isFusion = isAccroche(ptScreen) >= 0;
          if (isFusion) {
            fusion.paintIconCentre(this, _g2d, x, y);
          } else {
            icone.paintIconCentre(this, _g2d, x, y);
          }
        }
      }
    }
  }

  public int isAccroche(GrPoint ptEcran) {
    ptEcran.autoApplique(getVersReel());
    final CtuluListSelection selection = selection(ptEcran, ZCalqueSelectionInteractionAbstract.DEFAULT_TOLERANCE_PIXEL);
    if (selection != null && selection.isOnlyOnIndexSelected()) {
      return selection.getMinIndex();
    }
    return -1;
  }

  @Override
  public boolean removeSelectedObjects(CtuluCommandContainer _cmd, CtuluUI _ui) {
    return false;
  }

  @Override
  public boolean moveSelectedObjects(double _reelDx, double _reelDy, double _reelDz, CtuluCommandContainer _cmd, CtuluUI _ui) {
    if (!isSelectionEmpty()) {
      final EfGridInterface grid = visuPanelEditor.getGrid();
      final int[] selectedPtIdx = getSelectedPtIdx();
      TrEditNodesPanel.Data data = new TrEditNodesPanel.Data(selectedPtIdx);
      GrPoint pt = new GrPoint();
      final DecimalFormat decimalFormat = CtuluLib.getDecimalFormat(2);
      for (int i = 0; i < selectedPtIdx.length; i++) {
        int idxPoint = selectedPtIdx[i];
        data.oldX[i] = grid.getPtX(idxPoint);
        data.oldY[i] = grid.getPtY(idxPoint);
        data.oldZ[i] = grid.getPtZ(idxPoint);

        data.newX[i] = grid.getPtX(idxPoint) + _reelDx;
        data.newY[i] = grid.getPtY(idxPoint) + _reelDy;
        data.newZ[i] = grid.getPtZ(idxPoint) + _reelDz;
        pt.x_ = data.newX[i];
        pt.y_ = data.newY[i];
        pt.autoApplique(getVersEcran());
        int idxAccroche = isAccroche(pt);
        if (idxAccroche >= 0) {
          data.newX[i] = grid.getPtX(idxAccroche);
          data.newY[i] = grid.getPtY(idxAccroche);
        } else {
          try {
            data.newX[i] = decimalFormat.parse(decimalFormat.format(data.newX[i])).doubleValue();
            data.newY[i] = decimalFormat.parse(decimalFormat.format(data.newY[i])).doubleValue();
          } catch (Exception e) {
            //nothing to do here
          }
        }
      }

      TrEditNodesPanel panel = TrEditNodesPanel.build(this.visuPanelEditor, data,
          visuPanelEditor.getParams().getNodalData().getModifiableModel(H2dVariableType.BATHYMETRIE), visuPanelEditor.getUndefinedValueForZ());
      String title;
      if (isOnlyOneObjectSelected()) {
        title = TrResource.getS("Modifier le point sélectionné: {0}", CtuluLibString.getString(selectedPtIdx[0] + 1));
      } else {
        title = TrResource.getS("Modifier les {0} points sélectionnés", CtuluLibString.getString(selectedPtIdx.length));
      }
      if (CtuluDialogPanel
          .isOkResponse(panel.afficheModale(visuPanelEditor.getFrame(), title))) {
        panel.checkAndApplyModifications(_cmd);
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean rotateSelectedObjects(double _angRad, double _xreel0, double _yreel0, CtuluCommandContainer _cmd, CtuluUI _ui) {
    return false;
  }

  @Override
  public boolean copySelectedObjects(CtuluCommandContainer _cmd, CtuluUI _ui) {
    return false;
  }

  @Override
  public boolean splitSelectedObject(CtuluCommandContainer _cmd, CtuluUI _ui) {
    return false;
  }

  @Override
  public boolean joinSelectedObjects(CtuluCommandContainer _cmd, CtuluUI _ui) {
    return false;
  }

  @Override
  public boolean canAddForme(int _typeForme) {
    return false;
  }

  @Override
  public boolean addForme(GrObjet _o, int _deforme, CtuluCommandContainer _cmd, CtuluUI _ui, ZEditionAttributesDataI _data) {
    return false;
  }

  @Override
  public boolean addAtome(GrPoint _ptReel, CtuluCommandContainer _cmd, CtuluUI _ui) {
    return false;
  }

  @Override
  public boolean addAtomeProjectedOnSelectedGeometry(GrPoint grPoint, CtuluCommandComposite cmp, CtuluUI _ui) {
    return false;
  }

  @Override
  public boolean canUseAtomicMode() {
    return false;
  }

  @Override
  public boolean canUseSegmentMode() {
    return false;
  }

  @Override
  public ZCalqueGeometry.SelectionMode getSelectionMode() {
    return ZCalqueGeometry.SelectionMode.NORMAL;
  }

  @Override
  public boolean setSelectionMode(ZCalqueGeometry.SelectionMode mode) {
    return false;
  }

  @Override
  public boolean isAtomicMode() {
    return false;
  }

  @Override
  public ZModeleEditable getModelEditable() {
    return null;
  }

  @Override
  public ZEditorInterface getEditor() {
    return null;
  }

  @Override
  public void setEditor(ZEditorInterface _editor) {

  }
}
