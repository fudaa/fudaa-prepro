package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.CtuluLibGenerator;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.fudaa.tr.common.TrResource;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TrPostSourcesManager {
  public static boolean isAsuiteCalcul(final TrPostSource _src) {
    if (_src instanceof TrPostSourceFromReader) {
      TrPostSourceFromReader s = (TrPostSourceFromReader) _src;
      return s.getReader() instanceof TrPostSourceReaderComposite;
    }

    return false;
  }

  final List<TrPostSource> lstSrc;
  final List<TrPostSourceBuilt> lstSrcBuilt;
  final List<TrPostSourceBuilt> lstSrcBuiltIm;
  final List<TrPostSource> lstSrcImmutable;

  /**
   *
   */
  public TrPostSourcesManager() {
    super();
    lstSrc = new ArrayList<>();
    lstSrcBuilt = new ArrayList<>();
    lstSrcImmutable = Collections.unmodifiableList(lstSrc);
    lstSrcBuiltIm = Collections.unmodifiableList(lstSrcBuilt);
  }

  public String getNameOfSource(TrPostSource source) {
    int index = lstSrc.indexOf(source);
    final String name = source.getTitle() + " [" + source.getMainFile().getParentFile().getAbsolutePath() + "]";
    if (index > -1) {
      return TrResource.getS("Projet {0}", CtuluLibString.getString(index + 1)) + " - " + name;
    }
    return name;
  }

  public String getShortNameOfSource(TrPostSource source) {
    int index = lstSrc.indexOf(source);
    final String name = source.getTitle();
    if (index > -1) {
      return TrResource.getS("Projet {0}", CtuluLibString.getString(index + 1)) + " - " + name;
    }
    return name;
  }

  public TrPostSource addSource(TrPostSource src) {
    deliverSourceId(src);
    lstSrc.add(src);
    return src;
  }

  public void closeAll() {
    for (final TrPostSource src : lstSrc) {
      src.close();
    }
  }

  public String deliverSourceId(TrPostSource src) {
    if (CtuluLibString.isEmpty(src.getId())) {
      String uniqueId = CtuluLibGenerator.getInstance().deliverUniqueStringId();
      src.setId(uniqueId);
    }
    return src.getId();
  }

  public TrPostSource findSource(final Collection<File> _file) {

    for (final TrPostSource src : lstSrc) {
      if (isOpenedIn(_file, src)) {
        return src;
      }
    }
    return null;
  }

  /**
   * retourne la source dans la liste des sources correspondant au fichier. retourne null sinon.
   *
   * @param _file : path absolu du fichier
   */
  public TrPostSource findSource(final File _file) {

    for (final TrPostSource src : lstSrc) {
      if (src.isOpened(_file)) {
        return src;
      }
    }

    return null;
  }

  public TrPostSource findSource(final String _file) {
    return findSource(new File(_file));
  }

  /**
   * Retourne la source asscoi� a l id.
   *
   * @param _id
   */
  public TrPostSource findSourceById(final String _id) {

    for (final TrPostSource src : lstSrc) {
      if (src.getId().equals(_id)) {
        return src;
      }
    }
    for (final TrPostSource src : lstSrcBuilt) {
      if (src.getId().equals(_id)) {
        return src;
      }
    }

    return null;
  }

  /**
   * Retourne toutes les sources qui sont des composites
   */
  public List<TrPostSourceFromReader> getAllCCompositeSource() {
    List<TrPostSourceFromReader> res = new ArrayList<TrPostSourceFromReader>();
    for (TrPostSource src : lstSrc) {
      if (isAsuiteCalcul(src)) {
        res.add((TrPostSourceFromReader) src);
      }
    }
    return res;
  }

  /**
   * Retourne toutes les sources qui ne sont pas des composites
   */
  public List<TrPostSource> getAllClassicalSource() {
    List<TrPostSource> res = new ArrayList<>();
    for (TrPostSource src : lstSrc) {
      if (!isAsuiteCalcul(src)) {
        res.add(src);
      }
    }
    return res;
  }

  public int getIndexOf(TrPostSource src) {
    return lstSrc.indexOf(src);
  }

  /**
   * @return liste imuttables des sources
   */
  public List<TrPostSource> getLstSources() {
    return lstSrcImmutable;
  }

  /**
   * @return liste non modifiable des sources-construites
   */
  public List<TrPostSourceBuilt> getLstSrcBuilt() {
    return lstSrcBuiltIm;
  }

  public void addSourceBuilt(TrPostSourceBuilt src) {
    deliverSourceId(src);
    lstSrcBuilt.add(src);
  }

  /**
   * @param idx l'ordre d'ajout
   */
  public TrPostSource getSource(int idx) {
    if (idx < 0 || idx >= lstSrc.size()) {
      return null;
    }
    return lstSrc.get(idx);
  }

  public TrPostSource getSource(String id) {
    for (TrPostSource src : lstSrc) {
      if (src.getId().equals(id)) {
        return src;
      }
    }
    for (TrPostSource src : lstSrcBuilt) {
      if (src.getId().equals(id)) {
        return src;
      }
    }
    return null;
  }

  public int getSrcSize() {
    return lstSrc.size();
  }

  public boolean isEmpty() {
    return lstSrc.isEmpty();
  }

  public boolean isNotEmpty() {
    return !lstSrc.isEmpty();
  }

  public boolean isOneSourceLoaded(final Collection<File> _files) {
    if (findSource(_files) == null) {
      return false;
    }
    return true;
  }

  public boolean isOpened(final File _f) {
    if (_f == null) {
      return false;
    }
    for (final TrPostSource it : lstSrc) {
      if (it.isOpened(_f)) {
        return true;
      }
    }
    return false;
  }

  private boolean isOpenedIn(Collection<File> files, TrPostSource _in) {
    if (_in == null) {
      return false;
    }
    for (File file : files) {
      if (_in.isOpened(file)) {
        return true;
      }
    }
    return false;
  }

  /**
   * indique si il existe une source portant le nom du fichier en param.
   *
   * @param _file : path absolu du fichier
   */
  public boolean isSourceLoaded(final File _file) {
    if (findSource(_file) == null) {
      return false;
    }
    return true;
  }

  public boolean isSourceLoaded(final String _file) {
    return isSourceLoaded(new File(_file));
  }

  /**
   * @param src
   * @return l'ancien indice de la source.
   */
  public int remove(TrPostSource src) {
    int indexOf = getIndexOf(src);
    if (indexOf >= 0) {
      lstSrc.remove(src);
    }
    return indexOf;
  }

  /**
   * @return true if toTest is used by a built Source different from actualSourceUser.
   */
  private boolean isUsedByAnotherBuiltSource(TrPostSourceBuilt toTest, TrPostSourceBuilt actualSourceUser) {
    for (TrPostSourceBuilt build : lstSrcBuilt) {
      if (build != actualSourceUser && build.getUsedSources().contains(toTest)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Used to clean the built sources: if the widget of a built source is no removed, we remove the built source.
   *
   * @param source
   */
  public void nodeForSourceRemoved(TrPostSource source) {
    if (source instanceof TrPostSourceBuilt) {
      lstSrcBuilt.remove(source);
      List<TrPostSource> usedSources = source.getUsedSources();
      for (TrPostSource trPostSource : usedSources) {
        if (trPostSource instanceof TrPostSourceBuilt && isUsedByAnotherBuiltSource((TrPostSourceBuilt) trPostSource, (TrPostSourceBuilt) source)) {
          nodeForSourceRemoved(trPostSource);
        }
      }
    }
  }

  /**
   * Use to test if a built source is in the saved built sources.
   *
   * @param source
   */
  public void nodeForSourceAdded(TrPostSource source) {
    if (source instanceof TrPostSourceBuilt && !lstSrcBuilt.contains(source)) {
      lstSrcBuilt.add((TrPostSourceBuilt) source);
      if (source.getId() == null) {
        source.setId(deliverSourceId(source));
      }
      List<TrPostSource> usedSources = source.getUsedSources();
      for (TrPostSource usedSource : usedSources) {
        if (usedSource instanceof TrPostSourceBuilt) {
          nodeForSourceAdded(usedSource);
        }
      }
    }
  }
}
