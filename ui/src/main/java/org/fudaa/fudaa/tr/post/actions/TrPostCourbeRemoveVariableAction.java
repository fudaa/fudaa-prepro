/*
 * @creation 1 sept. 06
 * @modification $Date: 2007-01-19 13:14:11 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.actions;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuBorders;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuList;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.fu.FuComparator;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.swing.Action;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostCourbeTreeModel;

public class TrPostCourbeRemoveVariableAction extends EbliActionSimple {

  final FudaaCommonImplementation impl_;
  final EGGraphe model_;

  public TrPostCourbeRemoveVariableAction(final FudaaCommonImplementation _impl, final EGGraphe _model) {
    super(CtuluLib.getS("Enlever des variables"), BuResource.BU.getToolIcon("enlever"), "ADD_VARIALBES");
    impl_ = _impl;
    model_ = _model;

  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final TrPostCourbeTreeModel model = (TrPostCourbeTreeModel) model_.getModel();
    final List<H2dVariableType> vars = new ArrayList<H2dVariableType>(model.getVarSupported());
    Collections.sort(vars, FuComparator.STRING_COMPARATOR);
    final BuList l = CtuluLibSwing.createBuList(vars);
    final CtuluDialogPanel pn = new CtuluDialogPanel(false);
    pn.setLayout(new BuBorderLayout());
    pn.setBorder(BuBorders.EMPTY3333);
    pn.add(new BuLabel(CtuluLib.getS("S�lectionner les variables � enlever")), BuBorderLayout.NORTH);
    pn.add(new BuScrollPane(l));
    final String title = (String) getValue(Action.NAME);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(impl_.getFrame(), title))) {
      final Object[] val = l.getSelectedValues();
      if (val != null && val.length > 0) {
        new CtuluTaskOperationGUI(impl_, title) {
          @Override
          public void act() {
            model.removeVariables(Arrays.asList(val), model_.getCmd());
            EventQueue.invokeLater(new Runnable() {
              @Override
              public void run() {
                model_.restore();
              }
            });
          }
        }.start();
      }
    }

  }
}