package org.fudaa.fudaa.tr.post.profile;

import org.locationtech.jts.geom.LineString;
import java.io.File;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.courbe.EGCourbePersistBuilder;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimePersistBuilder;

public class MvProfileCourbeTimePersistBuilder extends EGCourbePersistBuilder<MvProfileCourbeTime> {

  @Override
  protected MvProfileCourbeTime createEGObject(EGCourbePersist target, Map params, CtuluAnalyze log) {
    MVProfileCourbeModel createModel = (MVProfileCourbeModel) super.createModel(target, params);
    FudaaCourbeTimeListModel timeModel = (FudaaCourbeTimeListModel) params
        .get(FudaaCourbeTimePersistBuilder.TIME_MODEL);
    if (timeModel == null) {
      log.addError(FudaaLib.getS("Le temps n'a pas �t� trouv� pour la courbe {0}", target.getTitle()));
      return null;
    }
    return new MvProfileCourbeTime((EGGroup) getGroup(params), createModel, timeModel);
  }
  
   @Override
  protected Map<String, double[]> generateMapToPersistInBinFile(EGModel initialModel) {
    final Map<String, double[]> map = super.generateMapToPersistInBinFile(initialModel);
    LineString lineString = ((MvProfileCourbeModelInterface) initialModel).getLineString();
    MvProfileCourbeCoordinatePersistBuilder.putInMap(lineString, map);
    return map;
  }
  

  private int findTime(MvProfileCourbeTime courbeTime, double valueToFind, double eps) {
    int nbTimeStep = courbeTime.getTimeListModel().getSize();
    for (int i = 0; i < nbTimeStep; i++) {
      if (CtuluLib.isEquals(valueToFind, courbeTime.getTimeStepValueSec(i), eps)) { return i; }
    }
    return -1;
  }

  @Override
  protected void postRestore(MvProfileCourbeTime courbeTime, EGCourbePersist persist, Map params, CtuluAnalyze log) {
    super.postRestore(courbeTime, persist, params, log);
    courbeTime.titleModifiedByUser = persist.getSpecificBooleanValue("titleModified");
    int idx = persist.getSpecificIntValue("timeIdx", 0);
    int nbTimeStep = courbeTime.getTimeListModel().getSize();
    if (idx < 0 || idx >= nbTimeStep) {
      idx = nbTimeStep - 1;
    }
    Double timeValue = (Double) persist.getSpecificValue("timeValue");
    final double eps = 1e-1;
    if (timeValue != null && nbTimeStep > 0 && !CtuluLib.isEquals(timeValue, courbeTime.getTimeStepValueSec(idx), eps)) {
      idx = findTime(courbeTime, timeValue, eps);
      if (idx < 0 || idx >= nbTimeStep) {
        idx = nbTimeStep - 1;
      }
    }
    courbeTime.selection_.setSelectionInterval(idx, idx);

  }

  @Override
  protected void postCreatePersist(EGCourbePersist res, MvProfileCourbeTime courbe, EGGraphe graphe,File binFile) {
    super.postCreatePersist(res, courbe, graphe,binFile);

    int selectedTime = courbe.selection_.getLeadSelectionIndex();
    res.saveSpecificData("timeIdx", selectedTime);
    res.saveSpecificData("timeValue", courbe.getTimeStepValueSec(selectedTime));
    res.saveSpecificData("titleModified", Boolean.valueOf(courbe.titleModifiedByUser));
  }

}
