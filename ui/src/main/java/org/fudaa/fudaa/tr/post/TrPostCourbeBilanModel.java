/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.cubature.EfBilanHelper;
import org.fudaa.dodico.ef.cubature.EfDataIntegrale;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsCorrectionActivity;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsBuilder;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;
import org.fudaa.dodico.ef.operation.EfLineIntersectorActivity;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimePersistBuilder;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.data.TrPostDataVecteur;
import org.fudaa.fudaa.tr.post.dialogSpec.EnumThresholdCalculation;
import org.fudaa.fudaa.tr.post.persist.TrPostReloadParameter;
import org.fudaa.fudaa.tr.post.profile.MvProfileCoteTester;
import org.fudaa.fudaa.tr.post.profile.MvProfileCourbeModelInterface;
import org.fudaa.fudaa.tr.post.replay.TrReplayerBilan;
import org.locationtech.jts.geom.LineString;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public class TrPostCourbeBilanModel extends AbstractPostCourbeModel implements MvProfileCourbeModelInterface {
  private EnumThresholdCalculation computation = EnumThresholdCalculation.ALL;
  private LineString line;
  private double seuil;
  private boolean vectoriel;
  private int minTimeIdx = -1;
  private int maxTimeIdx = -1;

  @Override
  public boolean isCourbeForInterpolation(TrPostInterpolatePoint _int) {
    return false;
  }

  public void setNom(String nom) {
    this.nom_ = nom;
  }

  public EnumThresholdCalculation getComputation() {
    return computation;
  }

  public void setComputation(EnumThresholdCalculation computation) {
    this.computation = computation;
  }

  public LineString getLine() {
    return line;
  }

  public void setLine(LineString line) {
    this.line = line;
  }

  public double getSeuil() {
    return seuil;
  }

  public void setSeuil(double seuil) {
    this.seuil = seuil;
  }

  public boolean isVectoriel() {
    return vectoriel;
  }

  public void setVectoriel(boolean vectoriel) {
    this.vectoriel = vectoriel;
  }

  public int getMinTimeIdx() {
    return minTimeIdx;
  }

  public void setMinTimeIdx(int minTimeIdx) {
    this.minTimeIdx = minTimeIdx;
  }

  public int getMaxTimeIdx() {
    return maxTimeIdx;
  }

  public void setMaxTimeIdx(int maxTimeIdx) {
    this.maxTimeIdx = maxTimeIdx;
  }

  @Override
  public boolean isCourbeForObject(int _idxObj) {
    return false;
  }

  public int getNbTimeSteps() {
    if (minTimeIdx < 0 || maxTimeIdx < 0) {
      return source_.getNbTimeStep();
    }
    return Math.abs(maxTimeIdx - minTimeIdx) + 1;
  }

  @Override
  public Object savePersistSpecificDatas() {
    Map savedData = new HashMap();
    savedData.put("sourceId", source_.getId());
    savedData.put("line", line);
    savedData.put("var", var_.getID());
    savedData.put("seuil", seuil);
    savedData.put("startTime", minTimeIdx);
    savedData.put("endTime", maxTimeIdx);
    savedData.put("vector", vectoriel);
    if (EnumThresholdCalculation.ALL != computation) {
      savedData.put("type", computation.getPersistName());
    }
    return savedData;
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
    super.restoreFromSpecificDatas(data, infos);
    if (data == null || !(data instanceof Map)) {
      return;
    }
    Map mapData = (Map) data;
    String idSource = (String) mapData.get("sourceId");
    TrPostProjet projet = (TrPostProjet) infos.get(TrPostReloadParameter.POST_PROJET);
    TrPostSource src = null;
    if (projet != null) {
      // -- etape 1: recherche du source qui contient le path donn� --//
      src = projet.getSources().findSourceById(idSource);
      if (src != null) {
        this.source_ = src;
      } else {
        ((List<String>) infos.get("errorMsg")).add("Erreur, la frame graphe ne trouve pas le fichier r�sultat qui correspond � l'ID " + idSource);
        return;
      }
    } else {
      ((List<String>) infos.get("errorMsg")).add("Erreur, la frame graphe ne trouve pas le fichier r�sultat qui correspond � l'ID " + idSource);
      return;
    }

    String idVar = (String) mapData.get("var");
    H2dVariableType var = null;
    infos.put(FudaaCourbeTimePersistBuilder.TIME_MODEL, src.getNewTimeListModel());
    // -- recherche dans les sources de la variable par id --//
    for (int i = 0; i < src.getAvailableVar().length; i++) {
      if (src.getAvailableVar()[i].getID().equals(idVar)) {
        var = src.getAvailableVar()[i];
      }
    }
    if (var != null) {
      this.var_ = var;
    } else {
      ((List<String>) infos.get("errorMsg")).add(TrResource.TR.getString("La frame graphe ne trouve pas la variable qui correspond � l'ID {0}", idVar));
      return;
    }
    vectoriel = (Boolean) mapData.get("vector");
    line = (LineString) mapData.get("line");
    seuil = (Double) mapData.get("seuil");
    minTimeIdx = getCorrectedTime((Integer) mapData.get("startTime"));
    maxTimeIdx = getCorrectedTime((Integer) mapData.get("endTime"));
    String typeString = (String) mapData.get("type");
    if (typeString != null) {
      computation = EnumThresholdCalculation.getFromSaved(typeString);
    }
    if (computation == null) {
      computation = EnumThresholdCalculation.ALL;
    }
    if (TrPostReloadParameter.isForceRecompute(infos)) {
      updateData(null);
    }
  }

  private int getCorrectedTime(int i) {
    int res = Math.max(0, i);
    return Math.min(res, source_.getTime().getNbTimeStep() - 1);
  }

  @Override
  public void fillWithInfo(InfoData _table, CtuluListSelectionInterface _selectedPt) {
    final String fonctionName = TrLib.getString("Bilan");
    _table.put(TrResource.getS("Type"), fonctionName);
    _table.put(TrResource.getS("Calcul"), vectoriel ? TrLib.getString("Calcul vectoriel") : TrLib.getString("Calcul scalaire"));
    super.fillWithInfo(_table, _selectedPt);
    if (computation != EnumThresholdCalculation.ALL) {
      _table.put(TrResource.getS("Seuil"), (EnumThresholdCalculation.NEGATIVE.equals(computation) ? "<= " : ">= ") + Double.toString(seuil));
    }
    _table.put(TrResource.getS("Temps de d�but"), source_.getFormattedTime(minTimeIdx));
    _table.put(TrResource.getS("Temps de fin"), source_.getFormattedTime(maxTimeIdx));
    _table.put(TrResource.getS("Variable source"), this.getVar().getName() + " [" + this.getVar().getCommonUnitString() + "]");
    if (H2dVariableType.DEBIT.equals(getVar())) {
      _table.put(TrResource.getS("Variable"), H2dVariableType.DEBIT_M3.getName() + " [" + H2dVariableType.DEBIT_M3.getCommonUnitString() + "]");
    } else {
      _table.put(TrResource.getS("Variable"), fonctionName + "(" + this.getVar().getName() + ")");
    }
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
    if (!(impl instanceof TrPostCommonImplementation)) {
      impl.error(TrResource.getS("Impossible de r�cup�rer la bonne interface fudaa"));
      return;
    }
    TrPostCommonImplementation implementation = (TrPostCommonImplementation) impl;
    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    new TrReplayerBilan((TrPostSource) getData(), implementation).displayBilanProfilOrigin(this, implementation);
  }

  @Override
  protected void createY() {
    if (y_ == null || y_.length != getNbTimeSteps()) {
      y_ = new double[getNbTimeSteps()];
    }
  }

  @Override
  public boolean isValid() {
    return true;
  }

  @Override
  public void replayData(EGGrapheModel grapheTreeModel, Map infos, CtuluUI impl) {
    if (!(impl instanceof TrPostCommonImplementation)) {
      impl.error(TrResource.getS("Impossible de r�cup�rer la bonne interface fudaa"));
      return;
    }
    TrPostCommonImplementation implementation = (TrPostCommonImplementation) impl;
    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    new TrReplayerBilan(this.source_, implementation).displayBilanReplayData((TrPostCourbeTreeModel) grapheTreeModel, this);
  }

  public void updateData(ProgressionInterface prog) {
    int firstIdx = Math.max(0, minTimeIdx);
    int lastIdx = source_.getNbTimeStep() - 1;
    if (maxTimeIdx >= 0) {
      lastIdx = Math.min(maxTimeIdx, lastIdx);
    }
    if (firstIdx > lastIdx) {
      int tmp = lastIdx;
      lastIdx = firstIdx;
      firstIdx = tmp;
    }
    time_ = new double[getNbTimeSteps()];
    int idx = 0;
    for (int timeStepIdx = firstIdx; timeStepIdx <= lastIdx; timeStepIdx++) {
      time_[idx++] = source_.getTime().getTimeStep(timeStepIdx);
    }
    updateY(prog);
  }

  @Override
  public void updateY() {
    updateY(null);
  }

  public void updateY(ProgressionInterface prog) {
    createY();
    int firstIdx = Math.max(0, minTimeIdx);
    int lastIdx = source_.getNbTimeStep() - 1;
    if (maxTimeIdx >= 0) {
      lastIdx = Math.min(maxTimeIdx, lastIdx);
    }
    if (firstIdx > lastIdx) {
      int tmp = lastIdx;
      lastIdx = firstIdx;
      firstIdx = tmp;
    }
    final H2dVariableType selectedVariable = var_;
    final EfLineIntersectorActivity lineIntersect = new EfLineIntersectorActivity(getSource());

    EfLineIntersectionsResultsI interfaceRes = (source_.isElementVar(selectedVariable)) ? lineIntersect.computeForMeshes(line, null)
        .getDefaultRes() : lineIntersect.computeForNodes(line, null).getDefaultRes();

    // le builder permet de corriger les erreurs concernant les cote d'eau / bathymetrie
    EfLineIntersectionsCorrectionActivity act = new EfLineIntersectionsCorrectionActivity();
    boolean isVect = vectoriel && source_.getFlecheContent(var_) != null;
    List<DataIntegraleCallable> callables = createCallables(prog, firstIdx, lastIdx, act, interfaceRes, selectedVariable, isVect);
    ExecutorService newFixedThreadPool = null;
    try {
      newFixedThreadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
      List<Future<Result>> invokeAll = newFixedThreadPool.invokeAll(callables);
      for (Future<Result> future : invokeAll) {
        Result result = future.get();
        switch (computation) {
          case NEGATIVE:
            y_[result.idx] = result.dataIntegrale.getZoneMoins();
            break;
          case POSITIVE:
            y_[result.idx] = result.dataIntegrale.getZonePlus();
            break;
          default:
            y_[result.idx] = result.dataIntegrale.getResultat();
            break;
        }
      }
    } catch (Exception e) {
      Logger.getLogger(TrPostCourbeBilanModel.class.getName()).log(Level.INFO, "updateY", e);
    } finally {
      if (newFixedThreadPool != null) {
        newFixedThreadPool.shutdownNow();
      }
    }
  }

  private List<DataIntegraleCallable> createCallables(ProgressionInterface prog, int firstIdx, int lastIdx, EfLineIntersectionsCorrectionActivity act,
                                                      EfLineIntersectionsResultsI interfaceRes, final H2dVariableType selectedVariable, boolean isVect) {

    if (prog != null) {
      prog.setProgression(0);
    }
    double nb = getNbTimeSteps();
    ProgressionTarget progTarget = new ProgressionTarget(nb, prog);
    //a revoir pour multi-thread.
    List<DataIntegraleCallable> callables = new ArrayList<DataIntegraleCallable>();
    int idx = 0;
    for (int timeStepIdx = firstIdx; timeStepIdx <= lastIdx; timeStepIdx++) {
      DataIntegraleCallable callable = new DataIntegraleCallable(source_, idx, act, interfaceRes, timeStepIdx, selectedVariable, isVect, seuil);
      callable.setProgTarget(progTarget);
      callables.add(callable);
      idx++;
    }
    return callables;
  }

  @Override
  public LineString getLineString() {
    return line;
  }

  @Override
  public EfGridData getData() {
    return source_;
  }

  @Override
  public LineString getInitLine() {
    return line;
  }

  @Override
  public boolean isDefinedOnMeshes() {
    return source_.isElementVar(var_);
  }

  @Override
  public void setRes(EfLineIntersectionsResultsBuilder _res, ProgressionInterface _prog) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int[] getInitRows() {
    return null;
  }

  private static class ProgressionTarget {
    int currentIdx = 0;
    double nb;
    ProgressionInterface progression;

    public ProgressionTarget(double nb, ProgressionInterface progression) {
      this.progression = progression;
      this.nb = nb;
    }

    public void inc() {
      EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
          ++currentIdx;
          if (progression != null) {
            progression.setProgression((int) (((double) currentIdx) / nb * 100d));
          }
        }
      });
    }
  }

  public static class Result {
    final EfDataIntegrale dataIntegrale;
    final int timeStepIdx;
    final int idx;

    public Result(EfDataIntegrale dataIntegrale, int idx, int timeStepIdx) {
      this.dataIntegrale = dataIntegrale;
      this.timeStepIdx = timeStepIdx;
      this.idx = idx;
    }
  }

  private static class DataIntegraleCallable implements Callable<Result> {
    final EfLineIntersectionsCorrectionActivity act;
    final EfLineIntersectionsResultsI interfaceRes;
    final H2dVariableType selectedVariable;
    final boolean isVect;
    final int idx;
    final double seuil;
    final TrPostSource source;
    int timeStepIdx;
    ProgressionTarget progTarget;

    public DataIntegraleCallable(TrPostSource source_, int idx, EfLineIntersectionsCorrectionActivity act, EfLineIntersectionsResultsI interfaceRes,
                                 int timeStepIdx,
                                 H2dVariableType selectedVariable, boolean isVect, double seuil) {
      this.act = act;
      this.idx = idx;
      this.source = source_;
      this.seuil = seuil;
      this.interfaceRes = interfaceRes;
      this.timeStepIdx = timeStepIdx;
      this.selectedVariable = selectedVariable;
      this.isVect = isVect;
    }

    public void setProgTarget(ProgressionTarget progTarget) {
      this.progTarget = progTarget;
    }

    @Override
    public Result call() throws Exception {
      EfLineIntersectionsResultsI intersections = act.correct(interfaceRes, new MvProfileCoteTester(), timeStepIdx, null);
      // -- calcul des efdata --//
      final EfData data = source.getData(selectedVariable, timeStepIdx);
      EfDataIntegrale resCalcul = null;
      if (isVect) {
        TrPostDataVecteur vectData = (TrPostDataVecteur) data;
        resCalcul = EfBilanHelper.integrerMethodeTrapezeVector(intersections, vectData.getVxData(), vectData.getVyData(), seuil);
      } else {
        resCalcul = EfBilanHelper.integrerMethodeTrapeze(intersections, data, seuil);
      }
      if (progTarget != null) {
        progTarget.inc();
      }
      return new Result(resCalcul, idx, timeStepIdx);
    }
  }
}
