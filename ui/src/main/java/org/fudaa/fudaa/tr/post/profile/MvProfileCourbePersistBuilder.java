package org.fudaa.fudaa.tr.post.profile;

import org.locationtech.jts.geom.LineString;
import java.io.File;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.courbe.EGCourbePersistBuilder;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGModel;

public class MvProfileCourbePersistBuilder extends EGCourbePersistBuilder<MvProfileCourbe> {

  @Override
  protected MvProfileCourbe createEGObject(EGCourbePersist target, Map params, CtuluAnalyze log) {
    return new MvProfileCourbe((MvProfileCourbeGroup) getGroup(params), (MVProfileCourbeModel) super.createModel(
            target, params));
  }

  /**
   * will persist the x,y of the profile lineString
   *
   * @param initialModel
   * @return
   */
  @Override
  protected Map<String, double[]> generateMapToPersistInBinFile(EGModel initialModel) {
    final Map<String, double[]> map = super.generateMapToPersistInBinFile(initialModel);
    LineString lineString = ((MvProfileCoordinatesModel) initialModel).getLineString();
    MvProfileCourbeCoordinatePersistBuilder.putInMap(lineString, map);
    return map;
  }

  @Override
  protected void postCreatePersist(EGCourbePersist res, MvProfileCourbe courbe, EGGraphe graphe, File binFile) {
    super.postCreatePersist(res, courbe, graphe, binFile);
    MVProfileCourbeModel model = (MVProfileCourbeModel) courbe.getModel();
    res.saveSpecificData(MvProfileCourbeCoordinatePersistBuilder.HIDDEN_POINTS, model.xyProvider.getHiddenPoints());
    res.saveSpecificData(MvProfileCourbeCoordinatePersistBuilder.HIDDEN_SEGMENTS, model.xyProvider.getHiddenSegments());
  }

  @Override
  protected void postRestore(MvProfileCourbe egObject, EGCourbePersist persist, Map params, CtuluAnalyze log) {
    super.postRestore(egObject, persist, params, log);
    params.put(MvProfileCourbeCoordinatePersistBuilder.HIDDEN_POINTS, persist.getSpecificValue(MvProfileCourbeCoordinatePersistBuilder.HIDDEN_POINTS));
    params.put(MvProfileCourbeCoordinatePersistBuilder.HIDDEN_SEGMENTS, persist.getSpecificValue(MvProfileCourbeCoordinatePersistBuilder.HIDDEN_SEGMENTS));
  }
}
