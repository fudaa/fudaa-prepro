package org.fudaa.fudaa.tr.post.actions;

import com.memoire.bu.BuTask;
import org.locationtech.jts.geom.LineString;
import java.awt.event.ActionEvent;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionBuAdapter;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.action.CalqueActionTable;
import org.fudaa.ebli.courbe.EGTableAction;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.post.profile.MvProfileBuilderFromTree;
import org.fudaa.fudaa.tr.post.profile.MvProfileCoteTester;
import org.fudaa.fudaa.tr.post.profile.MvProfileFillePanel;

/**
 * Surcharge de l'action calque Action Table qui propose 2 modes:
 * - affiche dans le cas 'classique'  les noeuds s�lectionn�es
 * - affiche dans le cas 'interpol�' les selection interpol�es.
 * le mode est activ� automatiquement en fonction de la s�lection.
 *
 * @author Adrien Hadoux
 */
public class TrPostActionCalqueTable extends CalqueActionTable {
  private static final long serialVersionUID = 1L;
  BArbreCalqueModel model_;

  public TrPostActionCalqueTable(BArbreCalqueModel model, CtuluUI ui,
                                 ZEbliCalquesPanel calque) {
    super(model, ui, calque);
    model_ = model;
  }

  private LineString getSelectedLine() {
    if (getCalque() instanceof TrPostVisuPanel) {
      TrPostVisuPanel panel = (TrPostVisuPanel) getCalque();
      if (panel.getSelectedLine() != null) {
        return panel.getSelectedLine();
      }
    }
    return null;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    boolean displayInterpolatedTable = false;
    //-- check if a linestring is displayed in the layer --//
    final LineString selectedLine = getSelectedLine();
    if (selectedLine != null && selectedLine.getNumPoints() > 0) {
      displayInterpolatedTable = true;
    }

    if (!displayInterpolatedTable) {
      super.actionPerformed(_e);
    } else {
      buildInterpolatedData(_e);
    }
  }

  private void buildInterpolatedData(final ActionEvent e) {
    LineString initLine = getSelectedLine();
    TrPostVisuPanel panel = (TrPostVisuPanel) getCalque();
    BuTask task = new BuTask("display interpolated data");
    ProgressionInterface prog = new ProgressionBuAdapter(task);

    MvProfileBuilderFromTree builder = new MvProfileBuilderFromTree(
        TrPostProfileAction.createProfileAdapter(panel),
        panel.getCtuluUI(), getSelectedLine(),
        new MvProfileCoteTester());

    builder.selectedLine_ = initLine;
    builder.buildVartime();
    H2dVariableType variable = panel.getSelectedVarInCalqueActif();
    int time = panel.getSelectedTimeInCalqueActif();
    MvProfileFillePanel graphePanel = builder.actBuildGroup(prog,
        builder.getVarTime(), false, variable, time);

    EGTableAction action = new EGTableAction(graphePanel.getGraphe());
    action.actionPerformed(e);
  }
}
