/**
 *  @creation     16 nov. 2004
 *  @modification $Date: 2007-06-05 09:01:13 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreated;
import org.fudaa.fudaa.tr.post.data.TrPostDataVecteur;

/**
 * @author Fred Deniger
 * @version $Id: TrPostFlecheContent.java,v 1.8 2007-06-05 09:01:13 deniger Exp $
 */
public interface TrPostFlecheContent extends TrPostDataCreated {

  /**
   * @return la description de la composante y
   */
  String getVyDesc();

  /**
   * @return la description de la composante x
   */
  String getVxDesc();

  /**
   * @return le nom du contenu de cette fleche
   */
  H2dVariableType getVar();

  H2dVariableType getVx();

  H2dVariableType getVy();

  

  
  /**
   * @return un clone de l'objet
   */
  TrPostFlecheContent duplicate(TrPostSource _src);

  TrPostFlecheContent duplicate(TrPostSource _src, H2dVariableType _newName);

  TrPostFlecheContent changeVar(H2dVariableType _newName);

  /**
   * @param _tIdx le pas de temps
   * @return les valeurs au pas de temps demande
   */
  TrPostDataVecteur getValues(int _tIdx);

}
