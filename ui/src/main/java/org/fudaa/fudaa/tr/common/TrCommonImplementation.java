/**
 * @creation 24 mai 2004
 * @modification $Date: 2008-03-18 11:05:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuActionRemover;
import com.memoire.bu.BuBrowserPreferencesPanel;
import com.memoire.bu.BuColumn;
import com.memoire.bu.BuDesktopPreferencesPanel;
import com.memoire.bu.BuIcon;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLanguagePreferencesPanel;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuMainPanel;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuBar;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTaskView;
import com.memoire.bu.BuToolBar;
import com.memoire.bu.BuUserPreferencesPanel;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.event.InternalFrameEvent;
import org.fudaa.ctulu.BuNetworkPreferencesPanel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluFavoriteFiles;
import org.fudaa.ctulu.gui.CtuluHelpComponent;
import org.fudaa.ctulu.image.CtuluImageImporter;
import org.fudaa.dodico.commun.DodicoPreferences;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.ZCalqueSelectionInteractionAbstract;
import org.fudaa.ebli.commun.BJava3DVersionTest;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.impression.EbliMiseEnPagePreferencesPanel;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaPreferences;
import org.fudaa.fudaa.commun.aide.FudaaHelpPanel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.impl.FudaaLookPreferencesPanel;
import org.fudaa.fudaa.commun.impl.FudaaStartupExitPreferencesPanel;
import org.fudaa.fudaa.commun.trace2d.FudaaPerformancePreferencesPanel;
import org.fudaa.fudaa.fdico.FDicoPreferences;
import org.fudaa.fudaa.sig.layer.FSigImageImportAction;
import org.jdesktop.swingx.ScrollPaneSelector;

/**
 * @author Fred Deniger
 * @version $Id: TrCommonImplementation.java,v 1.59.2.1 2008-03-18 11:05:44 bmarchan Exp $
 */
public abstract class TrCommonImplementation extends FudaaCommonImplementation implements ItemListener {

  protected BArbreCalque arbre_;
  protected TrLauncher launcher_;
  public BuTaskView taches_;

  public TrCommonImplementation() {
    useNewHelp_ = true;
    setOnlyOneApplicationFrame(false);
    documentationJar = "Fudaa-preproDocumentation.jar";
  }

  @Override
  public void addInternalFrame(final JInternalFrame jInternalFrame) {
    super.addInternalFrame(jInternalFrame);
    jInternalFrame.setLocation(0, 0);
    final Dimension d = getMainPanel().getDesktop().getSize();
    d.width = d.width - 40;
    d.height = d.height - 40;
    jInternalFrame.setSize(d);
  }

  protected void addExportCurrentFrame(final BuMenu _m) {
    if (_m == null) {
      return;
    }
    final BuMenu m = new BuMenu();
    m.setIcon(BuResource.BU.getIcon("bu_internalframe_corner_ocean"));
    m.setText(TrResource.getS("Fen�tre active"));
    final JInternalFrame f = getCurrentInternalFrame();
    String[] com = null;
    if (f instanceof BuInternalFrame) {
      final BuInternalFrame buf = (BuInternalFrame) f;
      com = buf.getEnabledActions();
    }
    BuMenuItem data = new BuMenuItem();
    super.initExportDataButton(data);
    if (com != null && CtuluLibArray.findObject(com, data.getActionCommand()) >= 0) {
      data.setEnabled(true);
    }
    m.add(data);
    data = new BuMenuItem();
    super.initExportImageButton(data);
    if (com != null && CtuluLibArray.findObject(com, data.getActionCommand()) >= 0) {
      data.setEnabled(true);
    }
    m.add(data);
    _m.add(m);
  }

  @Override
  protected boolean buildExportDataToolIcon() {
    return true;
  }

  @Override
  protected boolean buildImageToolIcon() {
    return true;
  }

  public String getExporterCmd() {
    return "EXPORTER";
  }

  /**
   * Methode appelee une seule fois pour construire le menu d'export. Methods called once to build the export menu
   */
  protected void buildExporterMenu() {
    addExportCurrentFrame((BuMenu) getMainMenuBar().getMenu(getExporterCmd()));
  }

  @Override
  protected void buildPreferences(final List _prefs) {
    _prefs.add(new FudaaPerformancePreferencesPanel(this));
    _prefs.add(new BuUserPreferencesPanel(this));
    _prefs.add(new BuBrowserPreferencesPanel(this));
    _prefs.add(new FudaaStartupExitPreferencesPanel(true));
    _prefs.add(new BuLanguagePreferencesPanel(this));
    _prefs.add(new BuDesktopPreferencesPanel(this));
    _prefs.add(new FudaaLookPreferencesPanel(this));
    _prefs.add(new EbliMiseEnPagePreferencesPanel());
    _prefs.add(new BuNetworkPreferencesPanel());
  }

  protected void buildTaskView() {
    final BuMainPanel mp = getMainPanel();
    final BuColumn lc = mp.getLeftColumn();
    lc.setFocusable(false);
    final BuColumn rc = mp.getRightColumn();
    rc.setFocusable(false);
    lc.setBorder(null);
    taches_ = new BuTaskView();
    taches_.setToolTipText(TrResource.getS("Les t�ches en cours"));
    final BuScrollPane sp = new BuScrollPane(taches_);
    sp.setPreferredSize(new Dimension(150, 80));
    sp.setToolTipText(TrResource.getS("Les t�ches en cours"));
    rc.addToggledComponent(BuResource.BU.getString("T�ches"), "TOGGLE_TACHE", BuResource.BU.getToolIcon("tache"), sp,
            true, this).setToolTipText(TrResource.getS("Cacher/Afficher les t�ches"));
    mp.setTaskView(taches_);
  }

  protected final void displayAide() {
    final JInternalFrame f = getCurrentInternalFrame();
    if (f == null) {
      displayHelp(getAideUrl());
      return;
    }
    String content = null;
    if (f instanceof CtuluHelpComponent) {
      content = ((CtuluHelpComponent) f).getShortHtmlHelp();
    } else {
      content = (String) f.getClientProperty(Action.SHORT_DESCRIPTION);
    }
    if (content == null) {
      displayHelp(getAideUrl());
    } else {
      showInternalHelp(content, f.getTitle());
    }
  }

  protected final void displayAideIndex() {
    displayHelp(getAideIndexUrl());
  }

  protected String getAideIndexUrl() {
    return getHelpDir() + "index." + (CtuluLib.isFrenchLanguageSelected() ? "fr" : "en") + ".html";
  }

  protected String getAideUrl() {
    return getAideIndexUrl();
  }

  protected String getCurrentProjectHelp() {
    return null;
  }

  protected abstract void ouvrir();

  protected void removeUnusedActions() {
    final BuMenu r = (BuMenu) getMainMenuBar().getMenu("MENU_EDITION");
    if (r != null) {
      r.removeAll();
      r.addMenuItem(BuResource.BU.getString("D�faire"), "DEFAIRE", false, KeyEvent.VK_Z);
      r.addMenuItem(BuResource.BU.getString("Refaire"), "REFAIRE", false).setAccelerator(
              KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
      // normalement Ctrl-Y
      // r.addSeparator();
      //
      r.addMenuItem(BuResource.BU.getString("Copier"), "COPIER", false, KeyEvent.VK_C);
      r.addMenuItem(BuResource.BU.getString("Couper"), "COUPER", false, KeyEvent.VK_X);
      r.addMenuItem(BuResource.BU.getString("Coller"), "COLLER", false, KeyEvent.VK_V);
      // r.addMenuItem(BuResource.BU.getString("Dupliquer" ),"DUPLIQUER" ,false)
      // .setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V,KeyEvent.CTRL_MASK|KeyEvent.SHIFT_MASK));
      r.addSeparator();
//      FSigLib.addSelectionAction(r);
      ZCalqueSelectionInteractionAbstract.addSelectionAction(r,true,r);
      r.addSeparator();
      r.addMenuItem(BuResource.BU.getString("Pr�f�rences"), "PREFERENCE", false, KeyEvent.VK_F2);
      addConsoleMenu(r);
      final BuToolBar tb = getMainToolBar();
      BuActionRemover.removeAction(tb, "COUPER");
      BuActionRemover.removeAction(tb, "COLLER");
      BuActionRemover.removeAction(tb, "COPIER");
      BuActionRemover.removeAction(tb, "DUPLIQUER");
      BuActionRemover.removeAction(tb, "RANGERICONES");
      BuActionRemover.removeAction(tb, "REMPLACER");

    }
    /*
     * removeAction("COUPER"); removeAction("COLLER"); removeAction("COPIER"); removeAction("DUPLIQUER");
     * removeAction("TOUTSELECTIONNER"); removeAction("REMPLACER");
     */
    removeAction("ASSISTANT");
    removeAction("ASTUCE");
    removeAction("POINTEURAIDE");
    removeAction("INDEX_THEMA");
    removeAction("INDEX_ALPHA");
    removeAction("PROPRIETE");
    removeAction("PLEINECRAN");
    removeAction("VISIBLE_LEFTCOLUMN");

  }

  protected void savePref() {
    final Point p = getFrame().getLocation();
    final Dimension d = getFrame().getSize();
    BuPreferences.BU.putIntegerProperty("window.x", p.x);
    BuPreferences.BU.putIntegerProperty("window.y", p.y);
    BuPreferences.BU.putIntegerProperty("window.w", d.width);
    BuPreferences.BU.putIntegerProperty("window.h", d.height);
    BuPreferences.BU.writeIniFile();
    TrPreferences.TR.writeIniFile();
    DodicoPreferences.DODICO.writeIniFile();
    CtuluFavoriteFiles.INSTANCE.saveFavorites();
    FudaaPreferences.FUDAA.writeIniFile();
    FDicoPreferences.FD.writeIniFile();
    EbliPreferences.EBLI.writeIniFile();

  }

  protected void showInternalHelp(final String _s, final String _title) {
    final FudaaHelpPanel pn = getHelpPanel();
    pn.showHelp();
    if (EventQueue.isDispatchThread()) {
      showHelp(_s, _title, pn);
    } else {
      BuLib.invokeLater(new Runnable() {
        @Override
        public void run() {
          showHelp(_s, _title, pn);
        }
      });
    }
  }

  protected void showHelp(final String _s, final String _title, final FudaaHelpPanel _pn) {
    _pn.setHtmlSource("<html><body style=\"margin:2px\">" + _s + "</body></html>", _title);
  }

  @Override
  public void actionPerformed(final ActionEvent event) {
    final String act = event.getActionCommand();
    if ("SUPERVISEUR".equals(act)) {
      launcher_.openSupervisor();
    } /*
     * else if ("CLEARSELECTION".equals(act)) { final JInternalFrame frame = getCurrentInternalFrame();
     * 
     * if (frame instanceof CtuluSelectionInterface) { ((CtuluSelectionInterface) frame).clearSelection(); } } else if
     * ("INVERSESELECTION".equals(act)) { final JInternalFrame frame = getCurrentInternalFrame();
     * 
     * if (frame instanceof CtuluSelectionInterface) { ((CtuluSelectionInterface) frame).inverseSelection(); } }
     */ else if ("OUVRIR".equals(act)) {
      ouvrir();
    } else if ("AIDE".equals(act)) {
      // displayAide();
      displayHelpPDF(getInformationsSoftware().baseManUrl());
    } else if ("AIDE_INDEX".equals(act)) {
      // displayAideIndex();
      displayHelpPDF(getInformationsSoftware().baseManUrl());
    } else if ("SEND_COMMENT".equals(act)) {
      new Thread() {
        @Override
        public void run() {
          TrBugCommentGenerator.sendAComment();
        }
      }.start();
    } else if (act.startsWith("TOGGLE")) {
      final BuColumn c = getMainPanel().getRightColumn();
      final JComponent comp = c.getToggleComponent(act);
      if (comp != null) {
        comp.setVisible(!comp.isVisible());
        c.revalidate();
      }
    } else if ("LAUNCH_JAVAWS".equals(act)) {
      try {
        FuLib.startProgram(new String[]{TrLib.getJavaws(), "-viewer"});
      } catch (final IOException _ioEvt) {
        FuLog.error(_ioEvt);

      }

    } else if ("JAVA3D".equals(act)) {
      BJava3DVersionTest.showVersionDialog(this);
    } else if ("CLOSE_ALL".equals(act)) {
      getLauncher().closeAll(getApp());
    } else if ("IMPORT_IMAGE".equals(act) && getCurrentInternalFrame() instanceof CtuluImageImporter) {
      ((CtuluImageImporter) getCurrentInternalFrame()).importImage();
    } else if ("IMPORT_PROJECT".equals(act)) {
      doImportProject();
    } else {
      super.actionPerformed(event);
    }
  }

  public abstract void doImportProject();

  @Override
  public boolean confirmExit() {
    if (FudaaStartupExitPreferencesPanel.isExitConfirmed()) {
      return question(BuResource.BU.getString("Quitter"),
              BuResource.BU.getString("Voulez-vous vraiment quitter ce logiciel ?"));
    } //Do you really want to leave this software		
    return true;
  }

  public ProgressionInterface createProgressionForMainPanel() {
    return new ProgressionInterface() {
      @Override
      public void reset() {
        unsetMainMessage();
        unsetMainProgression();
      }

      @Override
      public void setDesc(final String _s) {
        setMainMessage(_s);
      }

      @Override
      public void setProgression(final int _v) {
        setMainProgression(_v);
      }
    };
  }

  @Override
  public void exit() {
    savePref();
    super.exit();
  }

  @Override
  public BuPreferences getApplicationPreferences() {
    return TrPreferences.TR;
  }

  @Override
  protected FudaaHelpPanel getHelpPanel() {
    final String dir = getHelpDir();
    if (aide_ == null) {
      try {
        final URL url = new URL(getInformationsSoftware().http + "doc."
                + (CtuluLib.isFrenchLanguageSelected() ? "fr" : "en") + ".html");
        aide_ = new FudaaHelpPanel(this, new URL(dir), url);
      } catch (final MalformedURLException _e) {
        error(BuResource.BU.getString("Aide"), _e.getMessage());
        FuLog.warning(_e);
      }
      return aide_;
    }
    try {
      if (!aide_.getHtmlPane().isSameToc(dir)) {
        try {
          aide_.getHtmlPane().setUrlBase(new URL(dir));
        } catch (final Exception _e) {
          error(_e.getMessage());
          FuLog.warning(_e);
        }

      }
    } catch (final MalformedURLException _e) {
      error(BuResource.BU.getString("Aide"), _e.getMessage());
      FuLog.warning(_e);
    }
    return aide_;

  }

  @Override
  public String getHelpDir() {
    final String m = launcher_.getCurrentPrefHydId();
    final String currentProjectHelp = getCurrentProjectHelp();
    return currentProjectHelp == null ? getInformationsSoftware().man + m + '/' : currentProjectHelp;
  }

  public final TrLauncher getLauncher() {
    return launcher_;
  }

  public abstract BuIcon getSpecificIcon();

  public abstract String getSpecificName();

  protected void buildImportMenu() {
  }

  @Override
  protected boolean buildFudaaReportTool() {
    return false;
  }

  @Override
  protected boolean useScrollInBuDesktop() {
    return true;
  }

  @Override
  public void init() {
    super.init();
    addFrameListLeft();

    removeUnusedActions();
    final BuMenuBar b = getMainMenuBar();
    // on enleve le menu des look and feel : moche car tout n'est pas mis a jour
    //we remove the look and feel from the menu : bad because all is not up to date 
    final BuMenu mAide = (BuMenu) b.getMenu("MENU_AIDE");

    mAide.add(TrLib.buildAideContextItem(this), 0);
    TrLib.addJavawsForJnlp(mAide);
    mAide.addMenuItem(TrResource.getS("Soumettre une demande d'am�lioration"), "SEND_COMMENT", BuResource.BU
            .getIcon("envoyer"), true);
    TrLib.addJava3DJMFTest(mAide);

    setEnabledForAction("QUITTER", true);
    setEnabledForAction("PREFERENCE", true);
    setEnabledForAction("SEND_COMMENT", true);
    setEnabledForAction("LAUNCH_JAVAWS", true);
    final BuMenu mFichier = (BuMenu) b.getMenu("MENU_FICHIER");
    mFichier.addMenuItem(TrResource.getS("Fermer toutes les applications"), "CLOSE_ALL", BuResource.BU
            .getIcon("fermer"), true, 0);
    final BuToolBar tb = getMainToolBar();
    if (!isSupervisor()) {

      tb.addToolButton(TrResource.getSupervisorName(), TrResource.getS("ouvrir le superviseur"), "SUPERVISEUR",
              TrResource.getSupervisorIcon(), true).setVisible(true);

    }
    // les menus exporter et importer sont construit dynamiquement
    // the exported and imported menus are dynamically built
    b.getMenu(getExporterCmd()).addItemListener(this);
    final BuMenu menu = (BuMenu) b.getMenu("IMPORTER");
    menu.addMenuItem(FSigImageImportAction.getCommonTitle(), "IMPORT_IMAGE", FSigImageImportAction.getCommonImage(),
            this).setEnabled(false);
    menu
            .addMenuItem(FudaaLib.getS("Importer un projet"), "IMPORT_PROJECT", BuResource.BU.getMenuIcon("importer"), this)
            .setEnabled(false);
    menu.addItemListener(this);
    setEnabledForAction(getExporterCmd(), true);
    menu.setEnabled(true);
    setEnabledForAction("RANGERICONES", true);
    if (!isSupervisor()) {
      final JComponent cp = getMainPanel().getMiddleComponent();
      if (cp instanceof JScrollPane) {
        ScrollPaneSelector.installScrollPaneSelector((JScrollPane) cp);
      }
    }

  }

  @Override
  public void installContextHelp(final JComponent _cp) {
  }

  @Override
  public void installContextHelp(final JComponent _cp, final String _url) {
  }

  @Override
  public void internalFrameDeactivated(final InternalFrameEvent _evt) {

    super.internalFrameDeactivated(_evt);
  }

  /**
   * @return true si superviseur / true if supervisor
   */
  public boolean isSupervisor() {
    return false;
  }

  /**
   * @param _e l'evenement
   */
  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if (_e.getStateChange() == ItemEvent.SELECTED) {
      final BuMenu menu = (BuMenu) _e.getSource();
      if (menu.getMenuComponentCount() != 0) {
        return;
      }
      final String com = menu.getActionCommand();
      if ("IMPORTER".equals(com)) {
        buildImportMenu();
      }
      menu.removeItemListener(this);
    }
  }

  @Override
  public void openFileInLogFrame(final File _f) {
    launcher_.openLogFrame(_f);
  }

  public void setLauncher(final TrLauncher _l) {
    launcher_ = _l;
  }

  @Override
  public void start() {
    super.start();
    buildExporterMenu();
    final Frame f = getFrame();
    f.setTitle(getSpecificName());
    f.setIconImage(getSpecificIcon().getImage());
    f.repaint();
  }
}
