/**
 * @creation 4 janv. 2005
 * @modification $Date: 2006-09-08 16:53:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import org.locationtech.jts.geom.Coordinate;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrSaisiePoint;
import org.fudaa.fudaa.tr.data.TrVisuPanel;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarLimniSaisie.java,v 1.9 2006-09-08 16:53:09 deniger Exp $
 */
public class TrRubarLimniSaisie extends TrSaisiePoint {

  /**
   * @param _pn
   * @param _layer
   */
  public TrRubarLimniSaisie(final TrVisuPanel _pn, final ZCalquePoint _layer) {
    super(_pn, _layer);
  }

  Coordinate m_ = new Coordinate();
  
  @Override
  public void pointClicked(GrPoint ptReel) {
    final TrRubarLimniModel model = (TrRubarLimniModel) layer_.modeleDonnees();
    final int idxToAdd = model.mng_.getEltContaining(ptReel.x_, ptReel.y_);
    if (idxToAdd < 0) {
      super.errMessage(TrResource.getS("Le point n'appartient pas � un �l�ment"));
      return;
    }
    if (model.mng_.add(idxToAdd, super.pn_.getCmdMng())) {
      super.okMessage(TrResource.getS("Limnigramme ajout� pour l'�l�ment {0}", CtuluLibString.getString(idxToAdd + 1)));
    } else {
      super.errMessage(TrResource.getS("Un limnigramme est d�j� d�fini pour cet �l�ment"));
    }

  }
}
