/*
 *  @creation     6 avr. 2005
 *  @modification $Date: 2007-03-30 15:39:15 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuFileFilter;
import gnu.trove.TIntObjectIterator;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.ProgressionBuAdapter;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.exporter.GISExportDataStoreFactory;
import org.fudaa.ctulu.gis.exporter.GISFileFormat;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.telemac.io.SinusxFileFormat;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.*;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;
import org.fudaa.fudaa.meshviewer.layer.MvNodeLayer;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.sig.wizard.FSigFileLoadResult;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderGIS;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderI;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderSinusX;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.rubar.TrRubarDonneesBrutesNuageLayer;
import org.locationtech.jts.geom.LineString;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: TrGisProjectEditor.java,v 1.19 2007-03-30 15:39:15 deniger Exp $
 */
public class TrGisProjectEditor extends FSigEditor {
  /**
   * @param _panel
   */
  public TrGisProjectEditor(final TrVisuPanelEditor _panel) {
    super(_panel);
  }

  protected final TrVisuPanelEditor getTrPanel() {
    return (TrVisuPanelEditor) getPanel();
  }

  // ZEditionAttibutesContainer siFeatures_;

  @Override
  public boolean addNewPolyligne(final GrPolyligne _pt, final ZEditionAttributesDataI _d) {
    // pour le calque des plan d'hauteur d'eau on doit savoir si c'est le profil
    if (isSIplanTarget()) {
      final TrSiPlanEditorPanel panel = (TrSiPlanEditorPanel) super.editorPanel_;
      final boolean r = ((TrSiProfilLayer) getSupport().getCalqueActif()).addForme(_pt, DeForme.LIGNE_BRISEE, panel.isProfil(),
          getMng(), getUi(), _d);
      if (r) {
        panel.objectAdded();
      }
      return r;
    }
    return super.addNewPolyligne(_pt, _d);
  }

  @Override
  protected void editSingleObject(final ZCalqueEditable zCalqueEditable) {
    if (zCalqueEditable instanceof TrRubarDonneesBrutesNuageLayer || zCalqueEditable instanceof MvNodeLayer) {
      zCalqueEditable.editSelected();
      return;
    }
    final int idxSelected = zCalqueEditable.getLayerSelection().getMaxIndex();
    boolean editAttribute = true;
    // pour le calque des profil, seules la lignes des profils doit etre
    // editable
    if (zCalqueEditable instanceof TrSiProfilLayer) {
      final TrSiProfilLayer lay = (TrSiProfilLayer) zCalqueEditable;
      editAttribute = lay.getProfilModel().isSpecLigne(idxSelected);
    }
    if (zCalqueEditable.getModelEditable() != null) {
      final EbliSingleObjectEditorPanel ed = new EbliSingleObjectEditorPanel(zCalqueEditable.getModelEditable(), idxSelected, true,
          editAttribute, getCoordinateDefinitions(), null);
      ed.setCmd(getMng());
      ed.afficheModale(getFrame(), zCalqueEditable.getTitle());
    }
  }

  @Override
  protected void editVertexObject(final ZCalqueEditable _c) {
    final EbliListeSelectionMultiInterface idxSelected = ((ZCalqueAffichageDonnees) _c).getLayerSelectionMulti();
    if (idxSelected.getNbListSelected() > 1) {
      return;
    }
    final TIntObjectIterator it = idxSelected.getIterator();
    it.advance();
    final int idx = it.key();
    final int[] vertex = ((CtuluListSelectionInterface) it.value()).getSelectedIndex();
    boolean editAttribute = true;
    // pour le calque des profil, seules la lignes des profils doit etre
    // editable
    if (_c instanceof TrSiProfilLayer) {
      final TrSiProfilLayer lay = (TrSiProfilLayer) _c;
      editAttribute = lay.getProfilModel().isSpecLigne(idx);
    }
    final EbliAtomicsEditorPanel ed = new EbliAtomicsEditorPanel(idx, vertex, getCoordinateDefinitions(), _c.getModelEditable(),
        editAttribute, null, getMng());
    ed.afficheModale(getFrame(), _c.getTitle());
  }

  @Override
  public void importSelectedLayer() {
    if (isSIplanTarget()) {
      final Map<BuFileFilter, GISFileFormat> gisFileFormatMap = GISExportDataStoreFactory.buildFileFilterMap();
      final List<BuFileFilter> filtersList = new ArrayList(gisFileFormatMap.size() + 2);
      final BuFileFilter sinusx = SinusxFileFormat.getInstance().createFileFilter();
      filtersList.add(sinusx);
      filtersList.addAll(gisFileFormatMap.keySet());
      final BuFileFilter[] filters = new BuFileFilter[filtersList.size()];
      filtersList.toArray(filters);
      final CtuluFileChooser fileChooser = FudaaGuiLib.getFileChooser(TrResource.getS("Export"), filters, null);
      fileChooser.setAcceptAllFileFilterUsed(false);
      final File f = FudaaGuiLib.chooseFile(getTrPanel().getFrame(), true, fileChooser);
      if (f == null) {
        return;
      }
      FSigFileLoaderI loaderI = null;
      if (fileChooser.getFileFilter() == sinusx) {
        loaderI = new FSigFileLoaderSinusX(sinusx);
      } else {
        loaderI = new FSigFileLoaderGIS((BuFileFilter) fileChooser.getFileFilter(), gisFileFormatMap
            .get(fileChooser.getFileFilter()));
      }
      final FSigFileLoaderI loader = loaderI;
      new CtuluTaskOperationGUI(getTrPanel().getImpl(), TrResource.getS("Import")) {
        @Override
        public void act() {
          final CtuluAnalyze ana = new CtuluAnalyze();
          final FSigFileLoadResult res = new FSigFileLoadResult();
          loader.setInResult(res, f, f.getName(), new ProgressionBuAdapter(this), ana);
          if (res.nbPolylignes_ > 0) {
            final TrSiProfilModel model = ((TrSiProfilLayer) getTrPanel().getCalqueActif()).getProfilModel();
            final CtuluCommandComposite cmp = new CtuluCommandComposite();
            final List lignes = res.ligneModel_;
            final int nb = lignes.size();
            for (int i = 0; i < nb; i++) {
              final GISDataModel gisModel = (GISDataModel) lignes.get(i);
              final int locNb = gisModel.getNumGeometries();
              for (int j = 0; j < locNb; j++) {
                if (gisModel.getGeometry(j) instanceof LineString) {
                  model.getGeomData().addCoordinateSequence(
                      ((LineString) gisModel.getGeometry(j)).getCoordinateSequence(), null, cmp);
                }
                if (model.getGeomData().getNumGeometries() == 1) {
                  model.setProfil(0, cmp, false);
                }
              }
            }
            getTrPanel().getCmdMng().addCmd(cmp.getSimplify());
          }
        }
      }.start();
    } else {
      super.importSelectedLayer();
    }
  }

  @Override
  public GISPolygone[] getEnglobPolygone() {
    return getTrPanel().getGrid().getExtRings();
  }

  @Override
  protected ZEditorPanelInterface getToolPanel() {
    if (isSIplanTarget() && isCalqueDessin(getCurrentAction())) {
      return new TrSiPlanEditorPanel(this);
    }
    return super.getToolPanel();
  }

  private boolean isSIplanTarget() {
    return getTarget() != null && getTarget() == getTrPanel().getLayerSiProfiles();
  }
}
