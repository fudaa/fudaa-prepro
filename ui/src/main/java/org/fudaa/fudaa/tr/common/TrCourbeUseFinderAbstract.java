/*
 * @creation 15 mars 07
 * @modification $Date: 2007-04-30 14:22:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.H2dParameters;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.courbe.EGObject;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeModel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.data.TrVisuPanel;

/**
 * @author fred deniger
 * @version $Id: TrCourbeUseFinderAbstract.java,v 1.3 2007-04-30 14:22:41 deniger Exp $
 */
public abstract class TrCourbeUseFinderAbstract implements TrCourbeUseFinder {
  final H2dParameters params_;
  final TrVisuPanel panel_;
  boolean stop_;
  final EGGrapheModel graphe_;

  public TrCourbeUseFinderAbstract(final H2dParameters _params, final TrVisuPanel _panel, final EGGrapheModel _graphe) {
    super();
    graphe_ = _graphe;
    params_ = _params;
    panel_ = _panel;
  }

  @Override
  public EvolutionReguliereInterface[] getSelectedEvolutions() {
    final EGObject[] obj = graphe_.getSelectedObjects();
    if (obj == null) { return null; }
    final List res = new ArrayList(obj.length);
    for (int i = 0; i < obj.length; i++) {
      final EGObject oi = obj[i];
      if (oi instanceof EGCourbe) {
        final EGModel model = ((EGCourbe) oi).getModel();
        addEvolutionInList(res, model);
      } else if (oi instanceof EGGroup) {
        final EGGroup gr = (EGGroup) oi;
        for (int j = 0; j < gr.getChildCount(); j++) {
          final EGModel model = gr.getCourbeAt(j).getModel();
          addEvolutionInList(res, model);
        }
      }
    }
    return (EvolutionReguliereInterface[]) res.toArray(new EvolutionReguliereInterface[res.size()]);
  }

  private void addEvolutionInList(final List _res, final EGModel _model) {
    if (_model instanceof FudaaCourbeModel) {
      _res.add(((FudaaCourbeModel) _model).getEvolution());
    }
  }

  @Override
  public final TrCourbeUseResultsI findAll(final ProgressionInterface _prog) {
    final TrCourbeUseResultsI createResult = createResult();
    go(createResult, _prog);
    if (stop_) { return null; }
    createResult.validSearch();
    return createResult;
  }

  protected abstract TrCourbeUseResultsI createResult();

  protected abstract void go(TrCourbeUseResultsI _res, ProgressionInterface _prog);

  @Override
  public final TrCourbeUseResultsI findFor(final EvolutionReguliereInterface _evol, final ProgressionInterface _prog) {
    final TrCourbeUseResultsI createResult = createResult();
    createResult.setFilter(_evol);
    go(createResult, _prog);
    if (stop_) { return null; }
    createResult.validSearch();
    return createResult;
  }

  @Override
  public final FudaaCommonImplementation getImpl() {
    return panel_.getImpl();
  }

  @Override
  public final void stop() {
    stop_ = true;

  }

  public TrVisuPanel getPanel() {
    return panel_;
  }

  public H2dParameters getParams() {
    return params_;
  }

  public boolean isStop() {
    return stop_;
  }

}
