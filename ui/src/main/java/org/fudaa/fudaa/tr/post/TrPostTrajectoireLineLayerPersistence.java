/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.swing.JFrame;
import org.fudaa.ctulu.CtuluArkLoader;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.CtuluXmlReaderHelper;
import org.fudaa.ctulu.CtuluXmlWriter;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.dodico.ef.operation.EfTrajectoireParameters;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueSaverInterface;
import org.fudaa.ebli.calque.BCalqueSaverTargetInterface;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeDefault;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.sig.persistence.FSigLayerLinePersistence;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.persist.TrPostReloadParameter;

/**
 * @author deniger
 */
public class TrPostTrajectoireLineLayerPersistence extends FSigLayerLinePersistence {

  public static final String COMPUTE_DATA = "computeData";
  public static final String INIT_DATA = "initData";

  @Override
  protected BCalque addInParent(FSigLayerGroup _gr, BCalqueSaverInterface _cqName, GISZoneCollection _collection) {
    TrPostTrajectoireLineLayer layer = new TrPostTrajectoireLineLayer((TrPostVisuPanel) _gr.getGisEditor().getPanel(), null,
        new ZModeleLigneBriseeDefault((GISZoneCollectionLigneBrisee) _collection), null);
    layer.setTitle(_cqName.getLayerName());
    _gr.enDernier(layer);
    return layer;
  }

 

  @Override
  protected boolean restoreFromSpecific(BCalqueSaverInterface _saver, CtuluArkLoader _loader, BCalqueSaverTargetInterface _parentPanel,
      BCalque _parentCalque, String _parentDirEntry, String _entryName, ProgressionInterface _proj) {
    boolean res = super.restoreFromSpecific(_saver, _loader, _parentPanel, _parentCalque, _parentDirEntry, _entryName, _proj);
    if (res) {
      boolean reload = Boolean.valueOf(_loader.getOption(TrPostReloadParameter.RECOMPUTE));
      _saver.getUI().put(TrPostReloadParameter.RECOMPUTE, reload);
    }
    return res;

  }

  @Override
  protected EbliUIProperties createPropForArk(BCalque cqToSave) {
    EbliUIProperties createPropForArk = super.createPropForArk(cqToSave);
    //we don't save init data in the files: just use to duplicate layer
    createPropForArk.remove(INIT_DATA);
    return createPropForArk;
  }

  @Override
  protected void writeBodyData(CtuluXmlWriter out, BCalque cqToSave) throws IOException {
    super.writeBodyData(out, cqToSave);
    TrPostTrajectoireLineLayer layer = (TrPostTrajectoireLineLayer) cqToSave;
    EfTrajectoireParameters dataMemory = layer.dataMemory_;
    if (dataMemory != null && dataMemory.segment_.size() == 2) {
      out.writeEntry("IsStreamLine", Boolean.toString(dataMemory.isLigneDeCourant));
      out.writeEntry("Finesse", Integer.toString(dataMemory.finesse_));
      out.writeEntry("VectorXName", dataMemory.vx.getID());
      out.writeEntry("VectorYName", dataMemory.vy.getID());
      Coordinate coordinate = dataMemory.segment_.get(0);
      out.writeEntry("Point1.x", Double.toString(coordinate.x));
      out.writeEntry("Point1.y", Double.toString(coordinate.y));
      coordinate = dataMemory.segment_.get(1);
      out.writeEntry("Point2.x", Double.toString(coordinate.x));
      out.writeEntry("Point2.y", Double.toString(coordinate.y));
      out.writeEntry("NumberOfPoints", Integer.toString(dataMemory.nbPointsInitiaux_));
      out.writeEntry("InitTimeStep", Double.toString(dataMemory.firstTimeStep_));
      out.writeEntry("Duration", Double.toString(dataMemory.dureeIntegration_));
      if (dataMemory.marqueur_ != null) {
        out.writeEntry("Mark.delta", Double.toString(dataMemory.marqueur_.deltaMax_));
        out.writeEntry("Mark.OnTime", Boolean.toString(dataMemory.marqueur_.timeStep_));
      }
      if (!CtuluLibArray.isEmpty(dataMemory.varsASuivre_)) {
        StringBuilder builder = new StringBuilder();
        List<CtuluVariable> varsASuivre = dataMemory.varsASuivre_;
        boolean first = true;
        for (Iterator<CtuluVariable> iterator = varsASuivre.iterator(); iterator.hasNext();) {
          CtuluVariable ctuluVariable = iterator.next();
          if (!first) {
            builder.append(' ');
          }
          first = false;
          builder.append(ctuluVariable.getID());
        }
        out.writeEntry("ComputedVariables", builder.toString());
      }

    } else {
      CtuluLibDialog.showError((JFrame) null, TrResource.getS("Sauvegarde trajectoires"),
          TrResource.getS("Erreur lors de la sauvegarde des donn�es d'entr�e utilis� pour le calcul de trajectoires/lignes de courant"));
    }

  }

  @Override
  public void readXmlSpecific(CtuluXmlReaderHelper helper, BCalqueSaverInterface target) {
    super.readXmlSpecific(helper, target);
    try {
      TrPostTrajectoireLineLayerPersistData data = new TrPostTrajectoireLineLayerPersistData();
      data.setStreamLine(Boolean.parseBoolean(helper.getTrimTextFor("IsStreamLine")));
      data.setVx(helper.getTrimTextFor("VectorXName"));
      data.setVy(helper.getTrimTextFor("VectorYName"));
      double x = Double.parseDouble(helper.getTrimTextFor("Point1.x"));
      double y = Double.parseDouble(helper.getTrimTextFor("Point1.y"));
      data.setPoint1(new Coordinate(x, y));
      x = Double.parseDouble(helper.getTrimTextFor("Point2.x"));
      y = Double.parseDouble(helper.getTrimTextFor("Point2.y"));
      data.setPoint2(new Coordinate(x, y));
      data.setNumberOfPoint(Integer.parseInt(helper.getTrimTextFor("NumberOfPoints")));
      data.setInitTime(Double.parseDouble(helper.getTrimTextFor("InitTimeStep")));
      data.setDuration(Double.parseDouble(helper.getTrimTextFor("Duration")));
      String markDelta = helper.getTrimTextFor("Mark.delta");
      if (!CtuluLibString.isEmpty(markDelta)) {
        data.setMarkDeta(Double.parseDouble(markDelta));
        data.setMarkOnTimeStep(Boolean.parseBoolean(helper.getTrimTextFor("Mark.OnTime")));
      }
      String variables = helper.getTrimTextFor("ComputedVariables");
      if (!CtuluLibString.isEmpty(variables)) {
        data.setComputeVariables(CtuluLibString.parseString(variables, " "));
      }
      int finesse = 1;
      try {
        finesse = Integer.parseInt(helper.getTrimTextFor("Finesse"));
      } catch (Exception e) {
      }
      data.setFinesse(finesse);
      target.getUI().put(COMPUTE_DATA, data);

    } catch (Exception e) {
      FuLog.error(e);
      CtuluLibDialog.showError((JFrame) null, TrResource.getS("Lecture des donn�es de trajectoires"),
          TrResource.getS("Erreur lors de la relecture des donn�es"));

    }

  }
}
