/*
 * @creation 2 mai 2005
 *
 * @modification $Date: 2007-04-30 14:22:38 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import org.fudaa.dodico.ef.*;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrPreferences;

import java.io.IOException;

/**
 * @author Fred Deniger
 * @version $Id: TrPostFilterHauteur.java,v 1.8 2007-04-30 14:22:38 deniger Exp $
 */
public class TrPostFilterHauteur implements EfFilterTime, TrPostListener {
  public static double getEpsFromPref() {
    return TrPreferences.TR.getDoubleProperty("condition.depth.eps", 0.001);
  }

  public static boolean isActivatedFromPref() {
    return TrPreferences.TR.getBooleanProperty("condition.depth.activated", false);
  }

  public static void setActivatedInPref(final boolean _b) {
    TrPreferences.TR.putBooleanProperty("condition.depth.activated", _b);
  }

  public static void setEpsInPref(final double _d) {
    if (_d > 0) {
      TrPreferences.TR.putDoubleProperty("condition.depth.eps", _d);
    }
  }

  transient EfData hauteur_;
  transient boolean isDelta;
  transient int tidx_ = -1;
  double eps_;
  private boolean strict;
  EfGridInterface grid;
  protected static final H2dVariableType DELTA_H = H2dVariableType.createTempVar("\u0394" + H2dVariableType.HAUTEUR_EAU.getName(), null);

  public TrPostFilterHauteur() {
    this(getEpsFromPref());
  }

  public TrPostFilterHauteur(final double _eps) {
    eps_ = _eps;
  }

  public TrPostFilterHauteur(final TrPostFilterHauteur _data) {
    initFrom(_data);
  }

  public Object createSaver() {
    return new TrPostFilterHauteurSaver(eps_);
  }

  public final double getEps() {
    return eps_;
  }

  /**
   * @param _h la condition servant � l'initialisation.
   */
  public final void initFrom(final TrPostFilterHauteur _h) {
    if (_h != null) {
      eps_ = _h.eps_;
    }
  }

  @Override
  public boolean isActivated(final int _idxPt) {
    if (hauteur_ == null) {
      return true;
    }
    if (isDelta) {
      return Math.abs(hauteur_.getValue(_idxPt)) > eps_;
    }
    return hauteur_.getValue(_idxPt) > eps_;
  }

  @Override
  public boolean isActivatedElt(final int _idxElt) {
    return isActivatedElt(_idxElt, strict);
  }

  @Override
  public boolean isActivatedElt(final int _idxElt, boolean strictAsked) {
    final EfElement el = grid.getElement(_idxElt);
    for (int i = el.getPtNb() - 1; i >= 0; i--) {
      if (isActivated(el.getPtIndex(i))) {
        if (!strictAsked) {
          return true;
        }
      } else if (strictAsked) {
        return false;
      }
    }
    return strictAsked;
  }

  public boolean isStrict() {
    return strict;
  }

  public void restoreFromSaver(final Object _o) {
    if (_o instanceof TrPostFilterHauteurSaver) {
      eps_ = ((TrPostFilterHauteurSaver) _o).eps_;
    }
  }

  public void setStrict(boolean strict) {
    this.strict = strict;
  }

  /**
   * @param _t le pas de temps a consid�rer
   * @param _src la source des donnees
   */
  @Override
  public void updateTimeStep(final int _t, final EfGridData _src) {
    grid = _src.getGrid();
    if (_t < 0) {
      hauteur_ = null;
      tidx_ = -1;
    } else if (hauteur_ == null || _t != tidx_) {
      try {
        isDelta = false;
        hauteur_ = _src.getData(H2dVariableType.HAUTEUR_EAU, _t);
        if (hauteur_ == null) {
          hauteur_ = _src.getData(DELTA_H, _t);
          if (hauteur_ != null) {
            isDelta = true;
          }
        }
      } catch (final IOException _e) {
        FuLog.warning(_e);
      }
      tidx_ = _t;
    }
  }

  @Override
  public boolean valueChanged(final H2dVariableType _t, final TrPostSource _src) {
    if (_t != null && (_t == H2dVariableType.HAUTEUR_EAU || DELTA_H.getName().equals(_t.getName())) && hauteur_ != null && tidx_ >= 0) {
      hauteur_ = _src.getData(H2dVariableType.HAUTEUR_EAU, tidx_);
      isDelta = true;
      if (hauteur_ == null) {
        hauteur_ = _src.getData(DELTA_H, tidx_);
        if (hauteur_ != null) {
          isDelta = true;
        }
      }
      return true;
    }
    return false;
  }
}
