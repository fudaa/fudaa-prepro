package org.fudaa.fudaa.tr.post.persist;

import java.util.Map;

/**
 *
 * @author deniger ( genesis)
 */
public class TrPostReloadParameter {

  public static final String POST_PROJET = "TrPostProjet";
  public static final String RECOMPUTE = "recompute";

  public static boolean isForceRecompute(Map in) {
    return Boolean.TRUE.equals(in.get(RECOMPUTE));

  }
}
