/**
 * @creation 7 f�vr. 2005
 * @modification $Date: 2007-05-22 14:20:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereFixe;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.rubar.io.RubarNUAResult;
import org.fudaa.dodico.rubar.io.RubarNUAResult.RubarNUAResultBloc;
import org.fudaa.dodico.rubar.io.RubarOutEdgeResult;
import org.fudaa.ebli.courbe.*;
import org.fudaa.ebli.palette.BPalettePlageDefault;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeDefaultModelName;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeDefautModel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;

/**
 * @author Fred Deniger
 * @version $Id: TrPostRubarEdgesResults.java,v 1.20 2007-05-22 14:20:38 deniger Exp $
 */
public class TrPostRubarEdgesResults {
  private final CtuluUI ctuluUI;

  /**
   * @author fred deniger
   * @version $Id: TrPostRubarEdgesResults.java,v 1.20 2007-05-22 14:20:38 deniger Exp $
   */
  protected static final class UnmodifiableGraphe extends EGGrapheTreeModel {
    @Override
    public boolean canAddCourbe() {
      return false;
    }

    @Override
    public boolean isContentModifiable() {
      return false;
    }

    @Override
    public boolean isStructureModifiable() {
      return false;
    }
  }

  int[] idxInOutMas_;
  CtuluPermanentList listVar_;
  TrPostRubarOuvrageModel model_;
  RubarOutEdgeResult[] results_;
  RubarNUAResult nuaResult;
  private List<H2dVariableType> ouvrageVariables;
  private List<H2dVariableType> nuaVariables;
  boolean isOutMasTimeChecked_;
  boolean isNuaTimeChecked_;

  protected TrPostRubarEdgesResults(CtuluUI ui) {
    this.ctuluUI = ui;
  }

  public RubarNUAResult getNuaResult() {
    return nuaResult;
  }

  private void updateVariables() {
    List<H2dVariableType> all = new ArrayList<H2dVariableType>();
    if (ouvrageVariables != null) {
      all.addAll(ouvrageVariables);
    }

    if (nuaVariables != null) {
      all.addAll(nuaVariables);
    }
    Collections.sort(all);
    listVar_ = new CtuluPermanentList(all);
  }

  protected void setOuvrageResults(final int[] _idx, final RubarOutEdgeResult[] _results) {
    assert isOuvrageSet();
    idxInOutMas_ = _idx;
    results_ = _results;
    if (results_ != null && results_.length > 0) {
      ouvrageVariables = results_[0].getVariables();
      updateVariables();
    }
  }

  public boolean isOuvrageSet() {
    return model_ != null;
  }

  public void setOuvrages(final H2dRubarGridAreteSource _g, final H2dRubarOuvrageContainer _ouvrages) {
    final H2dRubarOuvrageMng mng = new H2dRubarOuvrageMng(_g);
    mng.initWith(_ouvrages, new CtuluAnalyze());
    model_ = new TrPostRubarOuvrageModel(_g, mng, null);
  }

  public String checkOutMasTime(final TrPostSource _src) {
    if (isOutMasTimeChecked_) {
      return null;
    }
    if (!_src.getTime().isUseTempTimeStep() && _src.getNbTimeStep() > 0 && !CtuluLibArray.isEmpty(results_)) {
      isOutMasTimeChecked_ = true;
      final double last = _src.getTime().getInitTimeStep(_src.getNbTimeStep() - 1);
      final double lastHere = results_[0].getLastTimeStep();
      if (Math.abs(last - lastHere) > 1E-1) {
        return TrResource.getS("Les pas de temps du fichier {0} ne sont pas compatibles", "OUT/MAS");
      }
    }
    return null;
  }

  public String checkNuaTime(TrPostSourceRubar src) {
    if (isNuaTimeChecked_) {
      return null;
    }
    if (!src.getTime().isUseTempTimeStep() && src.getNbTimeStep() > 0 && nuaResult != null) {
      isNuaTimeChecked_ = true;
      final double last = src.getTime().getInitTimeStep(src.getNbTimeStep() - 1);
      final double lastHere = nuaResult.getBloc(nuaResult.getNbBlocSize() - 1).getTime();
      if (Math.abs(last - lastHere) > 1E-1) {
        return TrResource.getS("Les pas de temps du fichier {0} ne sont pas compatibles", "NUA");
      }
    }
    return null;
  }

  public boolean containResOnOuvrage() {
    return !evolutionByOuvrageElementaire.isEmpty();
  }

  void addOuvrageGraphe(final int[] ouvrageIdx, final ProgressionInterface _prog, final FudaaCommonImplementation _impl, final TrPostProjet _proj,
                        final TrPostSource _src) {
    if (this.evolutionByOuvrageElementaire.isEmpty()) {
      return;
    }
    final EGGrapheTreeModel treeModel = new UnmodifiableGraphe();
    final Map<H2dVariableType, EGGroup> varAxe = new LinkedHashMap<H2dVariableType, EGGroup>();
    final ProgressionUpdater up = new ProgressionUpdater(_prog, false);
    up.majProgessionStateOnly();
    for (int idxPt = 0; idxPt < ouvrageIdx.length; idxPt++) {
      H2dRubarOuvrage ouvrage = model_.getOuvrage(ouvrageIdx[idxPt]);
      if (!ouvrage.containsOuvrageElementaire()) {
        continue;
      }
      int nbEleme = ouvrage.getNbOuvrageElementaires();
      Map<H2dVariableType, Color> colorByVars = new HashMap<H2dVariableType, Color>();
      int idxColor = 0;
      for (int j = 0; j < nbEleme; j++) {
        H2dRubarOuvrageElementaireInterface ouvrageElementaire = ouvrage.getOuvrageElementaire(j);
        if (evolutionByOuvrageElementaire.containsKey(ouvrageElementaire)) {
          Map<H2dVariableType, EvolutionReguliereInterface> map = evolutionByOuvrageElementaire.get(ouvrageElementaire);
          for (Entry<H2dVariableType, EvolutionReguliereInterface> entry : map.entrySet()) {
            H2dVariableType key = entry.getKey();
            EGGroup g = (EGGroup) varAxe.get(key);
            if (g == null) {
              final EGAxeVertical axeV = new EGAxeVertical();
              axeV.setTitre(key.getName());
              axeV.setUnite(key.getCommonUnit());
              g = new EGGroup();
              g.setAxeY(axeV);
              g.setTitle(key.getName());
              varAxe.put(key, g);
              treeModel.add(g);
              g.setVisible(varAxe.size() == 1);
            }
            final FudaaCourbeDefautModel model = new FudaaCourbeDefaultModelName(entry.getValue());
            final EGCourbeChild c = new EGCourbeChild(g, model);
            Color color = colorByVars.get(key);
            if (color == null) {
              color = BPalettePlageDefault.getColor(idxColor++);
              colorByVars.put(key, color);
            }
            c.setAspectContour(color);
            g.addEGComponent(c);
            model.setTitle(entry.getValue().getNom());
            up.majAvancement();
          }
        }
      }
    }
    final EGGraphe graphe = new EGGraphe(treeModel);
    graphe.setXAxe(EGAxeHorizontal.buildDefautTimeAxe(_src.getTimeFormatter()));
    addFille(new TrPostRubarOuvrageElementairesTreeFille(graphe, TrResource.getS("Evolutions sur ouvrages"), _impl, null), _impl);
  }

  void addGraphe(final int _globalEdgeIdx, final ProgressionInterface _prog, final FudaaCommonImplementation _impl, final TrPostProjet _proj,
                 final TrPostSource _src) {
    final int res = getLocalEdgeSet(_globalEdgeIdx);
    if (nuaResult == null && res < 0) {
      TrPostRubarEdgeFille.getErrorSelectedEdge(_impl);
      return;
    }
    List listVariables = listVar_;
    final boolean isCoteEau = !((TrPostSourceRubar) _src).isFondVariable();
    listVariables = addCoteEauIfOutLoaded(listVariables, isCoteEau);
    int size = listVariables.size();
    final List<EGCourbeSimple> courbes = new ArrayList<EGCourbeSimple>(size);
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(4, size);
    up.majProgessionStateOnly();
    final RubarOutEdgeResult r = res >= 0 ? results_[res] : null;
    for (int i = 0; i < size; i++) {
      final H2dVariableType t = (H2dVariableType) listVariables.get(i);
      EGModel model = null;
      if (t == H2dVariableTransType.COEF_DIFFUSION) {
        model = createNuaEGModel(_globalEdgeIdx);
      } else if (r != null) {
        model = createOuvrageEvolution(r, t);
      }
      if (model == null) {
        continue;
      }
      model.setTitle(t.getName());
      final EGAxeVertical v = new EGAxeVertical();
      v.setGraduations(true);
      v.setTitre(t.getName());
      v.setUnite(t.getCommonUnit());
      EGCourbeSimple courbe = new EGCourbeSimple(v, model);
      courbe.setVisible(courbes.isEmpty());
      courbe.setAspectContour(BPalettePlageDefault.getColor(i));
      courbes.add(courbe);
      up.majAvancement();
    }
    final EGGraphe graphe = new EGGraphe(EGGrapheSimpleModel.createSimpleModel(courbes.toArray(new EGCourbeSimple[courbes.size()])));
    graphe.setXAxe(EGAxeHorizontal.buildDefautTimeAxe(_src.getTimeFormatter()));
    graphe.restore();
    final EGFilleSimple fille = new TrPostRubarEdgeFille(graphe, TrResource.getS("Ar�te {0}", CtuluLibString.getString(_globalEdgeIdx + 1)), _impl,
        null, getNuaAction());
    addFille(fille, _impl);
  }

  private TrPostRubarEdgeNuaAction getNuaAction() {
    if (nuaResult == null) {
      return null;
    }
    return new TrPostRubarEdgeNuaAction(this, ctuluUI);
  }

  Map<H2dRubarOuvrageElementaireInterface, Map<H2dVariableType, EvolutionReguliereInterface>> evolutionByOuvrageElementaire = new HashMap<H2dRubarOuvrageElementaireInterface, Map<H2dVariableType, EvolutionReguliereInterface>>();

  private EGModel createNuaEGModel(int _globalEdgeIdx) {
    EvolutionReguliereInterface e = createNuaEvolution(_globalEdgeIdx);
    return new FudaaCourbeDefaultModelName(e);
  }

  private EvolutionReguliereInterface createNuaEvolution(int _globalEdgeIdx) {
    double[] t = new double[nuaResult.getNbBlocSize()];
    double[] values = new double[t.length];
    for (int i = 0; i < t.length; i++) {
      RubarNUAResultBloc bloc = nuaResult.getBloc(i);
      t[i] = bloc.getTime();
      values[i] = bloc.getLines().get(_globalEdgeIdx).getCoeff();
    }
    EvolutionReguliereInterface e = new EvolutionReguliereFixe(t, values);
    e.setUnite(H2dVariableTransType.COEF_DIFFUSION.getCommonUnit());
    return e;
  }

  private EvolutionReguliereInterface createEvolutionOuvrage(final RubarOutEdgeResult r, final H2dVariableType t) {
    EvolutionReguliereInterface e = null;
    if (t == H2dVariableType.COTE_EAU) {
      final EvolutionReguliereInterface eHauteur = r.getEvol(H2dVariableType.HAUTEUR_EAU);
      final double[] y = new double[eHauteur.getNbValues()];
      final double z = r.getZCentre();
      for (int iPt = y.length - 1; iPt >= 0; iPt--) {
        y[iPt] = z + eHauteur.getY(iPt);
      }
      e = new EvolutionReguliereFixe(eHauteur.getArrayX(), y);
    } else {
      e = r.getEvol(t);
    }
    return e;
  }

  private EGModel createOuvrageEvolution(final RubarOutEdgeResult r, final H2dVariableType t) {
    EvolutionReguliereInterface e = createEvolutionOuvrage(r, t);
    e.setUnite(t.getCommonUnit());
    final EGModel model = new FudaaCourbeDefaultModelName(e);
    return model;
  }

  void addGraphe(final int[] _globalEdgeIdx, final ProgressionInterface _prog, final FudaaCommonImplementation _impl, final TrPostProjet _proj,
                 final TrPostSource _src) {
    if (_globalEdgeIdx == null) {
      return;
    }
    if (_globalEdgeIdx.length == 1) {
      addGraphe(_globalEdgeIdx[0], _prog, _impl, _proj, _src);
      return;
    }
    final TIntArrayList localEdge = new TIntArrayList(_globalEdgeIdx.length);
    for (int i = 0; i < _globalEdgeIdx.length; i++) {
      final int localEdgeTmp = getLocalEdgeSet(_globalEdgeIdx[i]);
      if (localEdgeTmp >= 0) {
        localEdge.add(localEdgeTmp);
      }
    }
    if (localEdge.size() == 0 && nuaResult == null) {
      TrPostRubarEdgeFille.getErrorSelectedEdges(_impl);
      return;
    }
    final EGGrapheTreeModel treeModel = new UnmodifiableGraphe();
    final int nbVar = listVar_.size();
    final Map varAxe = new HashMap();
    final ProgressionUpdater up = new ProgressionUpdater(_prog, false);
    up.setValue(10, _globalEdgeIdx.length * nbVar, 10, 90);
    up.majProgessionStateOnly();
    List variable = listVar_;
    final boolean isCoteEau = !((TrPostSourceRubar) _src).isFondVariable();
    variable = addCoteEauIfOutLoaded(variable, isCoteEau);
    for (int idxPt = 0; idxPt < _globalEdgeIdx.length; idxPt++) {
      final RubarOutEdgeResult r = results_ == null ? null : results_[localEdge.getQuick(idxPt)];
      for (int i = 0; i < nbVar; i++) {
        final H2dVariableType t = (H2dVariableType) variable.get(i);
        EvolutionReguliereInterface e = null;
        if (t == H2dVariableTransType.COEF_DIFFUSION) {
          e = createNuaEvolution(_globalEdgeIdx[idxPt]);
        } else if (r != null) {
          e = createEvolutionOuvrage(r, t);
        }
        if (e == null) {
          continue;
        }
        H2dVariableType parent = t.getParentVariable();
        if (parent == null) {
          parent = t;
        }
        EGGroup g = (EGGroup) varAxe.get(parent);
        if (g == null) {
          final EGAxeVertical axeV = new EGAxeVertical();
          axeV.setTitre(parent.getName());
          axeV.setUnite(parent.getCommonUnit());
          g = new EGGroup();
          g.setAxeY(axeV);
          g.setTitle(parent.getName());
          varAxe.put(parent, g);
          treeModel.add(g);
          g.setVisible(i == 0);
        }
        final FudaaCourbeDefautModel model = new FudaaCourbeDefaultModelName(e);
        final EGCourbeChild c = new EGCourbeChild(g, model);
        c.setAspectContour(BPalettePlageDefault.getColor(i));
        g.addEGComponent(c);
        model.setTitle(t.getName() + CtuluLibString.getEspaceString(_globalEdgeIdx[idxPt] + 1));
        up.majAvancement();
      }
    }
    final EGGraphe graphe = new EGGraphe(treeModel);
    graphe.setXAxe(EGAxeHorizontal.buildDefautTimeAxe(_src.getTimeFormatter()));
    addFille(new TrPostRubarEdgeTreeFille(graphe, TrResource.getS("Evolutions sur {0} ar�tes", CtuluLibString.getString(_globalEdgeIdx.length)),
        _impl, null, getNuaAction()), _impl);
  }

  private List addCoteEauIfOutLoaded(List variable, final boolean isCoteEau) {
    if (isCoteEau && idxInOutMas_ != null) {
      variable = new ArrayList(listVar_);
      variable.add(H2dVariableType.COTE_EAU);
      Collections.sort(variable);
    }
    return variable;
  }

  protected void addEdgeFrame(final int[] _globalEgeIdx, final FudaaCommonImplementation _impl, final TrPostProjet _prj, final TrPostSource _src) {
    if (idxInOutMas_ != null || nuaResult != null) {
      new CtuluTaskOperationGUI(_impl, H2dResource.getS("Limnigrammes")) {
        @Override
        public void act() {
          addGraphe(_globalEgeIdx, _impl.createProgressionInterface(this), _impl, _prj, _src);
        }
      }.start();
    }
  }

  protected void addWorkFrame(final int[] ouvrageIdx, final FudaaCommonImplementation _impl, final TrPostProjet _prj, final TrPostSource _src) {
    if (!evolutionByOuvrageElementaire.isEmpty()) {
      new CtuluTaskOperationGUI(_impl, H2dResource.getS("Limnigrammes")) {
        @Override
        public void act() {
          addOuvrageGraphe(ouvrageIdx, _impl.createProgressionInterface(this), _impl, _prj, _src);
        }
      }.start();
    }
  }

  protected H2dVariableType getVar(final int _i) {
    return (H2dVariableType) listVar_.get(_i);
  }

  public void addFille(final EGFille _fille, final FudaaCommonImplementation _impl) {
    _fille.setPreferredSize(new Dimension(600, 400));
    _fille.setName("filleEdge" + _fille.hashCode());
    _fille.setFrameIcon(EbliResource.EBLI.getToolIcon("curves"));
    final Runnable run = new Runnable() {
      @Override
      public void run() {
        _fille.pack();
        _impl.addInternalFrame(_fille);
        TrLib.initFrameDimensionWithPref(_fille, _impl.getMainPanel().getDesktop().getSize());
        _fille.getGraphe().restore();
      }
    };
    SwingUtilities.invokeLater(run);
  }

  /**
   * @param _i l'indice [0,getNbEdges[
   * @return l'indice global de l'arete
   */
  public int getEdgeIdx(final int _i) {
    return idxInOutMas_[_i];
  }

  public final CtuluPermanentList getListVar() {
    return listVar_;
  }

  /**
   * @param _globalEdgeIdx l'indice global de l'arete
   * @return l'indice dans les resultats locaux
   */
  public int getLocalEdgeSet(final int _globalEdgeIdx) {
    return idxInOutMas_ == null ? -1 : Arrays.binarySearch(idxInOutMas_, _globalEdgeIdx);
  }

  public final TrPostRubarOuvrageModel getModel() {
    return model_;
  }

  /**
   * @param _globalEdgeIdx l'indice global de l'arete
   * @return le resultat correspondant ou null si aucun
   */
  public RubarOutEdgeResult getResultFromGlobal(final int _globalEdgeIdx) {
    final int i = getLocalEdgeSet(_globalEdgeIdx);
    if (i >= 0) {
      return results_[i];
    }
    return null;
  }

  public boolean isLocalEdgeSet(final int[] _globalEdgeIdx) {

    if (_globalEdgeIdx == null) {
      return false;
    }
    if (nuaResult != null) {
      return true;
    }
    for (int i = _globalEdgeIdx.length - 1; i >= 0; i--) {
      if (getLocalEdgeSet(_globalEdgeIdx[i]) >= 0) {
        return true;
      }
    }
    return false;
  }

  public int[] getCorrectLocalEdgeSet(final int[] _globalEdgeIdx) {
    if (_globalEdgeIdx == null) {
      return null;
    }
    if (nuaResult != null) {
      return _globalEdgeIdx;
    }
    final int nb = _globalEdgeIdx.length;
    final TIntArrayList res = new TIntArrayList(nb);
    for (int i = 0; i < nb; i++) {
      if (getLocalEdgeSet(_globalEdgeIdx[i]) >= 0) {
        res.add(_globalEdgeIdx[i]);
      }
    }
    return res.toNativeArray();
  }

  public void setNua(RubarNUAResult result) {
    nuaVariables = Arrays.asList(H2dVariableTransType.COEF_DIFFUSION);
    updateVariables();
    this.nuaResult = result;
  }

  public void setOuvrageBResultat(Map<H2dRubarOuvrageElementaireInterface, Map<H2dVariableType, EvolutionReguliereInterface>> resByOuvrageElementaire) {
    this.evolutionByOuvrageElementaire = resByOuvrageElementaire;
  }
}
