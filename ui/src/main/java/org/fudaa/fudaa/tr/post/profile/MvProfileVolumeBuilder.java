/*
 * @creation 31 janv. 07
 * @modification $Date: 2007-06-13 12:58:10 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import com.memoire.bu.BuLib;
import com.memoire.bu.BuTextField;
import java.util.List;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfFilter;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.cubature.EfComputeVolume;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * @author fred deniger
 * @version $Id: MvProfileVolumeBuilder.java,v 1.8 2007-06-13 12:58:10 deniger Exp $
 */
public final class MvProfileVolumeBuilder {

  private MvProfileVolumeBuilder() {
  }

  public static void computeVolume(final EfGridData _data, final ZEbliCalquesPanel _pn,
          final FudaaCommonImplementation _impl, final double[] _ts, final FudaaCourbeTimeListModel _timeFmt) {
    final CtuluVariable var = H2dVariableType.HAUTEUR_EAU;
    if (!_data.isDefined(var)) {
      _impl.error(DodicoLib.getS("La hauteur d'eau n'est pas d�finie"));
      return;
    }
    EfFilter filter = MvFilterHelper.getSelectedFilter(_data, _pn);
    final MvVolumePanel volumePanel = new MvVolumePanel(filter, _impl, _ts == null ? 0 : _ts.length);
    final String s = DodicoLib.getS("Calcul du volume");
    if (volumePanel.afficheModaleOk(_impl.getFrame(), s)) {
      final CtuluTaskDelegate task = _impl.createTask(s);
      task.start(new Runnable() {
        @Override
        public void run() {
          final EfComputeVolume cmp = EfComputeVolume.getVolumeComputer(volumePanel.getSelectedFilter(), task.getStateReceiver(), _data, var, new CtuluAnalyze());
          computeVolumeAct(cmp, volumePanel, null, _ts, _timeFmt, _impl);

        }
      });

    }
  }

  protected static void computeVolumeAct(final EfComputeVolume _cmp, final MvVolumePanel _iframe,
          final String _titleForCourbe, final double[] _ts, final FudaaCourbeTimeListModel _timeFmt,
          final FudaaCommonImplementation _impl) {
    final double[] v = _cmp.getVolume(_ts.length);
    if (v != null) {
      BuLib.invokeLater(new Runnable() {
        @Override
        public void run() {
          if (_ts.length == 1) {
            final CtuluDialogPanel pn = new CtuluDialogPanel(false);
            final BuTextField tf = new BuTextField();
            tf.setText(DodicoLib.getS("Volume") + "= " + CtuluLib.DEFAULT_NUMBER_FORMAT.format(v[0]) + " m3");
            tf.setEditable(false);
            tf.setBorder(null);
            pn.add(tf);
            pn.afficheModale(_impl.getFrame(), DodicoLib.getS("Volume"), CtuluDialog.OK_OPTION);
          } else {
            MvProfileFlowrateFille dest = (MvProfileFlowrateFille) _iframe.getSelectedFrame();
            if (dest == null) {
              dest = MvProfileFlowrateFille.createBaseFille(_impl, _iframe.getNewFrameTitle(), _timeFmt);
            } else {
              _impl.activateInternalFrame(dest);
            }
            dest.addVolumeCourbe(_ts, v, _titleForCourbe);

          }
        }
      });
    }
  }

  static List getInternalFrames(final FudaaCommonImplementation _impl) {
    return FudaaLib.getFrameWithClientProperty(_impl, "fudaa.profile.flowrate");
  }
}
