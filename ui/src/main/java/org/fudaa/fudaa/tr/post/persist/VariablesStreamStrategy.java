package org.fudaa.fudaa.tr.post.persist;

import com.memoire.fu.FuLog;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.StreamException;
import com.thoughtworks.xstream.persistence.StreamStrategy;
import org.fudaa.fudaa.tr.post.TrPostSource;

import java.io.*;
import java.nio.file.Files;
import java.util.*;

/**
 * Classe qui surcharge FileStreamStrategy de xstream pour g�rer les noms des ficheirs � sa propre sauce.
 * Les fichiers seront de la forme nomFIchier_KEY.xml
 *
 * @author Adrien Hadoux
 */
public class VariablesStreamStrategy implements StreamStrategy {
  List<TrPostSource> listeSourcesAssociees_;
  private final FilenameFilter filter;
  private final XStream xstream;
  private final File baseDirectory;

  public VariablesStreamStrategy(File baseDirectory, List<TrPostSource> listeSourcesAssociees) {
    this(baseDirectory, new XStream(), listeSourcesAssociees);
  }

  public VariablesStreamStrategy(File baseDirectory, XStream xstream, List<TrPostSource> listeSourcesAssociees) {

    listeSourcesAssociees_ = listeSourcesAssociees;

    this.baseDirectory = baseDirectory;
    this.xstream = xstream;
    this.filter = new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return new File(dir, name).isFile() && isValid(dir, name);
      }
    };
  }

  /**
   * Il faut extraire la clef du nom du fichier.
   * Les fichiers seront toujours de la forme
   * nomFIchier_KEY.xml
   */
  protected String extractKey(String name) {
    // TODO Auto-generated method stub
    return name.substring(name.lastIndexOf('_'), name.length() - 4);
  }

  /**
   * Genere le nom du fichier a partir du nom de la source associ�e a la variable et de l'indice
   */
  protected String getName(Object key) {
    // TODO Auto-generated method stub
    int value = Integer.parseInt(key.toString());

    return listeSourcesAssociees_.get(value).getId() + "_" + key.toString() + ".xml";
  }

  protected boolean isValid(File dir, String name) {
    return name.endsWith(".xml");
  }

  class XmlMapEntriesIterator implements Iterator {
    private File[] files = baseDirectory.listFiles(filter);
    private int position = -1;
    private File current = null;

    @Override
    public boolean hasNext() {
      return position + 1 < files.length;
    }

    @Override
    public void remove() {
      if (current == null) {
        throw new IllegalStateException();
      }
      // removes without loading
      try {
        Files.delete(current.toPath());
      } catch (IOException e) {
        FuLog.error(e);
      }
    }

    @Override
    public Object next() {
      if(position>=files.length){
        throw  new NoSuchElementException("no more files");
      }
      return new MyEntry();
    }

    private class MyEntry implements Map.Entry {
      private File file = current = files[++position];
      private String key = extractKey(file.getName());

      @Override
      public Object getKey() {
        return key;
      }

      @Override
      public Object getValue() {
        return readFile(file);
      }

      @Override
      public Object setValue(Object value) {
        return put(key, value);
      }

      @Override
      public boolean equals(Object obj) {
        if (obj == null || !(MyEntry.class.equals(obj.getClass()))) {
          return false;
        }
        Map.Entry e2 = (Map.Entry) obj;
        final Object value = getValue();
        final Object e2Value = e2.getValue();
        final Object e2Key = e2.getKey();
        return (Objects.equals(this.key, e2Key))
            && (Objects.equals(value, e2Value));
      }

      @Override
      public int hashCode() {
        return key.hashCode();
      }
    }
  }

  private void writeFile(File file, Object value) {
    try {
      OutputStream os = new FileOutputStream(file);
      try {
        this.xstream.toXML(value, os);
      } finally {
        os.close();
      }
    } catch (IOException e) {
      throw new StreamException(e);
    }
  }

  private File getFile(String filename) {
    return new File(this.baseDirectory, filename);
  }

  private Object readFile(File file) {
    try {
      InputStream is = new FileInputStream(file);
      try {
        return this.xstream.fromXML(is);
      } finally {
        is.close();
      }
    } catch (FileNotFoundException e) {
      // not found... file.exists might generate a sync problem
      return null;
    } catch (IOException e) {
      throw new StreamException(e);
    }
  }

  @Override
  public Object put(Object key, Object value) {
    Object oldValue = get(key);
    String filename = getName(key);
    writeFile(new File(baseDirectory, filename), value);
    return oldValue;
  }

  @Override
  public Iterator iterator() {
    return new XmlMapEntriesIterator();
  }

  @Override
  public int size() {
    return baseDirectory.list(filter).length;
  }

  public boolean containsKey(Object key) {
    // faster lookup
    File file = getFile(getName(key));
    return file.exists();
  }

  @Override
  public Object get(Object key) {
    return readFile(getFile(getName(key)));
  }

  @Override
  public Object remove(Object key) {
    // faster lookup
    File file = getFile(getName(key));
    Object value = null;
    if (file.exists()) {
      value = readFile(file);
      try {
        Files.delete(file.toPath());
      } catch (IOException e) {
        FuLog.error(e);
      }
    }
    return value;
  }
}
