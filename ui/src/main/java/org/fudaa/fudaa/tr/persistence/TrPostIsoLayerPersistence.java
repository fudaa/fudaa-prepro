/*
 *  @creation     14 mars 2005
 *  @modification $Date: 2006-09-19 15:07:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.persistence;

import java.io.IOException;
import org.fudaa.ctulu.CtuluArkLoader;
import org.fudaa.ctulu.CtuluArkSaver;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluXmlReaderHelper;
import org.fudaa.ctulu.CtuluXmlWriter;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalquePersistenceGroupe;
import org.fudaa.ebli.calque.BCalquePersistenceSingle;
import org.fudaa.ebli.calque.BCalqueSaverInterface;
import org.fudaa.ebli.calque.BCalqueSaverTargetInterface;
import org.fudaa.fudaa.tr.post.TrIsoLayerDefault;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;

/**
 * @author Fred Deniger
 * @version $Id: TrPostIsoLayerPersistence.java,v 1.4 2006-09-19 15:07:28 deniger Exp $
 */
public class TrPostIsoLayerPersistence extends BCalquePersistenceSingle {

  public static String getIsBaseId() {
    return "postLayer.isBase";
  }

  // private boolean isBase_;

  public TrPostIsoLayerPersistence() {
    super();
  }

  @Override
  public final BCalqueSaverInterface saveIn(final BCalque _cqToSave, final CtuluArkSaver _saver,
      final String _parentDirEntry, final String _parentDirIndice) {
    final BCalqueSaverInterface res = super.saveIn(_cqToSave, _saver, _parentDirEntry, _parentDirIndice);
    res.getUI().put(getIsBaseId(), isBase(_cqToSave));
    return res;
  }

  public boolean isBase(final BCalque _cq) {
    return ((TrIsoLayerDefault) _cq).isBase();
  }

  @Override
  public BCalqueSaverInterface save(final BCalque _cq, final ProgressionInterface _prog) {
    final BCalqueSaverInterface res = super.save(_cq, _prog);
    res.getUI().put(getIsBaseId(), isBase(_cq));
    return res;
  }

  @Override
  protected final void writeBodyData(final CtuluXmlWriter _out, final BCalque _cqToSave) throws IOException {
    _out.writeEntry(TrPostIsoLayerPersistence.getIsBaseId(), CtuluLibString.toString(isBase(_cqToSave)));
  }

  @Override
  protected final boolean restoreFromSpecific(final BCalqueSaverInterface _saver, final CtuluArkLoader _loader,
      final BCalqueSaverTargetInterface _parentPanel, final BCalque _parentCalque, final String _parentDirEntry,
      final String _entryName, final ProgressionInterface _proj) {
    final String s = _loader.getOption(BCalquePersistenceGroupe.getNoLayerOpt());
    if (CtuluLibString.toBoolean(s)) {
      _saver.getUI().put(BCalquePersistenceGroupe.getNoLayerOpt(), true);
    }
    return super.restoreFromSpecific(_saver, _loader, _parentPanel, _parentCalque, _parentDirEntry, _entryName, _proj);
  }

  @Override
  public final void readXmlSpecific(final CtuluXmlReaderHelper _helper, final BCalqueSaverInterface _target) {
    final String txt = _helper.getTextFor(TrPostIsoLayerPersistence.getIsBaseId());
    _target.getUI().put(TrPostIsoLayerPersistence.getIsBaseId(), CtuluLibString.toBoolean(txt));
  }

  @Override
  protected final BCalque findCalque(final BCalqueSaverInterface _cqName, final BCalqueSaverTargetInterface _pn,
      final BCalque _parent) {
    final boolean isBase = _cqName.getUI().getBoolean(getIsBaseId());
    if (isBase) { return super.findCalque(_cqName, _pn, _parent); }
    final boolean noAdded = _cqName.getUI().getBoolean(BCalquePersistenceGroupe.getNoLayerOpt());
    if (noAdded) { return null; }
    final TrPostVisuPanel pn = (TrPostVisuPanel) _pn;
    final BCalque lay = createLayer(pn);
    // inutile car cela va etre fait par la suite
    // lay.initFrom(_cqName.getUI());
    pn.getDonneesCalque().enDernier(lay);
    return lay;
  }

  protected BCalque createLayer(final TrPostVisuPanel _pn) {
    return _pn.getIsoLayer().duplicate();
  }

}