/*
 *  @creation     4 ao�t 2005
 *  @modification $Date: 2007-01-19 13:14:08 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import org.fudaa.dodico.h2d.H2dSiSourceInterface;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;

/**
 * Une interface gerant les noeuds/elements des solutions initiales.
 * 
 * @author Fred Deniger
 * @version $Id: TrSiLayer.java,v 1.4 2007-01-19 13:14:08 deniger Exp $
 */
public interface TrSiLayer extends MvLayerGrid, ZCalqueAffichageDonneesInterface {

  H2dSiSourceInterface getSource();

}
