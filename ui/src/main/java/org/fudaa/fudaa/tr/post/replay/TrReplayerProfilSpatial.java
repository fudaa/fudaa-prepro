package org.fudaa.fudaa.tr.post.replay;

import com.memoire.bu.BuButton;
import org.locationtech.jts.geom.LineString;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.concurrent.Callable;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.post.profile.MVProfileCourbeModel;
import org.fudaa.fudaa.tr.post.profile.MvProfileCourbeModelInterface;
import org.fudaa.fudaa.tr.post.profile.MvProfileTreeModel;

/**
 * Classe qui se charge d'afficher et rejouer les donn�es d'origine des courbes TR. C'set a dire les courbes spatiales et evolutions temporelles.
 *
 * @author Adrien Hadoux
 */
public class TrReplayerProfilSpatial extends AbstractTrReplayer {

  public TrReplayerProfilSpatial(TrPostSource source,
          final TrPostCommonImplementation implementation) {
    super(source, implementation);
  }

  /**
   * Cree la dialog qui affiche tout le contenu d'origine du graphe.
   *
   * @param vue2d
   * @param implementation
   * @param modele
   */
  /**
   * Constructeur d'une dialog qui affiche la vue 2d avec le point initial qui a servi a la creation du modele. Ajoute �galement la liste des
   * variables � choisir. Il est possible dans la vue de s�lectionner un autre point ou plusieurs. LA validation de la frame permettra de r�cup�rer
   * les nouveaux point ainsi que les variables qui seront utilis�es pour refaire le calcul.
   *
   * @param modele
   */
  @SuppressWarnings("serial")
  public void displayProfilSpatialReplayData(final MvProfileTreeModel treeModel, final MVProfileCourbeModel modele) {

    final LineString listePoints = modele.getInitLine();
    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    final TrPostVisuPanel vue2d = TrReplayVue2dBuilder.constructVue2d(implementation, source, new TrReplayLineEditCustomizer(listePoints, TrResource.TR.getString("Profil d'origine")));
    vue2d.removeCalqueLegend();
    //
    // -- creation du panel de choix des variables a selectionner --//
    final String title = TrResource.getS("<html><body>Choisir les points, la variable et le pas de temps qui seront utilis�s pour refaire le calcul(<b>Il est possible de cr�er des polylignes en choisissant l'outil sonde et en maintenant shift+clic</b>).Les points rouge d�signent l'origine.</body></html>");
    // -- creation du panel de choix des variables a selectionner --//
    rejouerSrc_ = new BuButton();

    Callable<Boolean> actionOnOk = new Callable<Boolean>() {
      @Override
      public Boolean call() throws Exception {
        return replayProfilSpatialFor(vue2d, treeModel, modele);
      }
    };

    final TrReplayDialog dialog = constructEditionDialog(actionOnOk, vue2d, modele, null, title);
    rejouerSrc_.setAction(new AbstractAction(TrLib.getString("Fichier r�sultat")) {
      @Override
      public void actionPerformed(final ActionEvent e) {
        final TrPostSource selectedSource = selectSource(implementation.getCurrentProject(),
                (String) getValue(Action.NAME), implementation.getFrame(), source);
        if (selectedSource != null) {
//          replayWithAnotherSource = true;
          // -- on ferme l'interface courante --//
          if (dialog != null) {
            dialog.getDialog().cancel();
          }
          // -- on rappelle l'interface --//
          new TrReplayerProfilSpatial(selectedSource, implementation).displayProfilSpatialReplayData(treeModel, modele);
        }

      }
    });

    // -- on initialise la sonde --//
    vue2d.getSondeAction().setSelected(true);

    // -- on positionne la bonne variable et le bon pas de temps --//
    comboVar_.getCb().setSelectedItem(modele.getVariable());
    if (comboTime != null) {
      comboTime.getCb().setSelectedIndex(modele.getTime());
    }
    // -- ajout du calque ligne bris�e pour r�aliser une polyligne --//

    dialog.afficheModaleOk(implementation.getParentComponent(), TrResource.getS("Rejouer ")
            + " " + modele.getTitle(), createRestaurerRunnable(vue2d));

  }

  /**
   * Affiche une dialog avec une vue 2d qui affiche l'ensemble des points de la polyligne utilis�e pour la construction du graphe.
   *
   * @param modele
   * @param implementation
   */
  public void displaySpatialProfilOrigin(final MvProfileCourbeModelInterface modele) {

    if (!(modele.getData() instanceof TrPostSource)) {
      implementation.error(TrResource.getS("Erreur"), TrResource.getS("Impossible de r�cup�rer le fichier r�sultat"),
              true);
      return;
    }
    if (modele.getInitLine() == null) {
      implementation.error(TrResource.getS("Erreur"), TrResource.getS("Impossible de r�cup�rer la polyligne"), true);
      return;
    }

    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    final TrReplayLineVisuCustomizer customizer = new TrReplayLineVisuCustomizer(modele, TrResource.TR.getString("Profil d'origine"));
    final TrPostVisuPanel vue2d = TrReplayVue2dBuilder.constructVue2d(implementation, (TrPostSource) modele.getData(), customizer);
    vue2d.removeCalqueLegend();
    // -- on affiche le contenu dans une dialog--//
    final Object[] vect = new Object[2];
    vect[0] = "X";
    vect[1] = "Y";
    final JTable tableauVal = new JTable(new DefaultTableModel(customizer.getData(), vect));
    tableauVal.setPreferredSize(new Dimension(200, 300));
    final JScrollPane pane = new JScrollPane(tableauVal);
    pane.setPreferredSize(new Dimension(200, 300));
    final String title = "<html><body>"
            + TrResource.getS("Origine de {0}.<br/>Les points de la polyligne sont encadr�s en rouge", modele.getTitle())
            + "</body></html>";
    final CtuluDialogPanel panel = constructDialog(getTrueCallable(), vue2d, modele, pane, false, title,
            new TrReplayGenereVue2DInSceneAction(implementation, (TrPostSource) modele.getData(), customizer, modele.getTitle()));
    // -- on positionne la bonne variable et le bon pas de temps --//
    if (modele instanceof MVProfileCourbeModel) {
      MVProfileCourbeModel varTimeModel = (MVProfileCourbeModel) modele;
      comboVar_.getCb().setSelectedItem(varTimeModel.getVariable());
      if (comboTime != null && comboTime.getCb().getModel().getSize() > 0) {
        comboTime.getCb().setSelectedIndex(varTimeModel.getTime());
      }
    }
    panel.afficheModale(implementation.getParentComponent(), TrResource.getS("Origine ") + " " + modele.getTitle(),
            createRestaurerRunnable(vue2d));
  }

  /**
   * Methode qui v�rifie les saisies sur la vue 2d et r�alise les cr�ations/ecrasements avec les nouveaut�s des profils temporels.
   */
  private boolean replayProfilSpatialFor(final TrPostVisuPanel vue2d, final MvProfileTreeModel tree, final MVProfileCourbeModel modele) {
    final H2dVariableType newVariable = vue2d.getSelectedVarInCalqueActif();
    final int newTimeStep_ = vue2d.getSelectedTimeInCalqueActif();

    LineString initLine = modele.getInitLine();
    if (vue2d.isSelectionOkForEvolution()) {
      int[] newPoints = null;
      final ZCalqueAffichageDonnees calque = (ZCalqueAffichageDonnees) vue2d.getCalqueActif();

      final int[] idx = calque.getLayerSelection().getSelectedIndex();
      newPoints = idx;
      final String title2 = " [P1"
              + format(source.getGrid().getPtX(newPoints[0]), source.getGrid().getPtY(newPoints[0]))
              + ";P"
              + initLine.getNumPoints()
              + "("
              + format(source.getGrid().getPtX(newPoints[newPoints.length - 1]), source.getGrid().getPtY(
              newPoints[newPoints.length - 1])) + "]";
      // -- on rejoue les donn�es avec les nouvelles valeurs --//
      if (this.ecraser_.isSelected()) {
        implementation.message(
                TrResource.getS("La courbe actuelle va �tre recalcul�e pour les donn�es suivantes:\n Variable: {0}\n Pas de temps: {1}\n Ligne: {2}", newVariable.getName(), source.getTime().getTimeFormatter().format(source.getTimeStep(vue2d.getSelectedTimeInCalqueActif())), title2));
      } else {
        String msg = TrResource.getS("Une nouvelle courbe va �tre recalcul�e pour les donn�es suivantes:\n Variable: {0}\n Pas de temps: {1}\n Ligne: {2}.\nLa courbe actuelle ne sera pas �cras�e", newVariable.getName(), source.getTime().getTimeFormatter().format(source.getTimeStep(vue2d.getSelectedTimeInCalqueActif())), title2);
        implementation.message(msg);
      }

      // -- on rejoue les donn�es avec ou sans ecrasement --//
      tree.replayPoints(implementation, vue2d, modele, source, newPoints, vue2d.getCmdMng(), implementation.getMainProgression(), newVariable, newTimeStep_, this.ecraser_.isSelected());

      return true;

    } else if (vue2d.isSelectionOkForEvolutionSonde()) {

      // -- on recupere la ligne avec la ligne formee des sondes --//
      final List<GrPoint> listePoints = vue2d.getLigneBriseeFromSonde();
      if (listePoints != null && listePoints.size() > 1) {
        final String title2 = " [P1(" + format(listePoints.get(0).x_) + ";" + format(listePoints.get(0).y_) + ")" + ";"
                + "P" + listePoints.size() + "(" + format(listePoints.get(listePoints.size() - 1).x_) + ";"
                + format(listePoints.get(listePoints.size() - 1).y_) + ")]";
        // -- on rejoue les donn�es avec les nouvelles valeurs --//
        if (this.ecraser_.isSelected()) {
          implementation.message(TrResource.getS("La courbe actuelle va �tre recalcul�e pour les donn�es suivantes:\n Variable: {0}\n Pas de temps: {1}\n Ligne: {2}", newVariable.getName(), source.getTime().getTimeFormatter().format(source.getTimeStep(vue2d.getSelectedTimeInCalqueActif())), title2));
        } else {
          String msg = TrResource.getS("Une nouvelle courbe va �tre recalcul�e pour les donn�es suivantes:\n Variable: {0}\n Pas de temps: {1}\n Ligne: {2}.\nLa courbe actuelle ne sera pas �cras�e.\n", newVariable.getName(), source.getTime().getTimeFormatter().format(source.getTimeStep(vue2d.getSelectedTimeInCalqueActif())), title2);
          implementation.message(msg);
        }

        // -- on rejoue les donn�es avec ou sans ecrasement --//
        tree.replayPoints(implementation, vue2d, modele, source, listePoints, vue2d.getCmdMng(), implementation.getMainProgression(), newVariable, newTimeStep_, this.ecraser_.isSelected());

        return true;

      } else {
        implementation.error(TrLib.getString("Il faut choisir au moins 2 points avec la sonde afin de recalculer les donn�es de la courbe.\nPour s�lectionner plusieurs points, maintenir la touche shift"));
        return false;
      }

    } else {
      implementation.error(TrLib.getString(
              "Il faut choisir au moins 2 points afin de recalculer les donn�es de la courbe.\nPour s�lectionner plusieurs points, maintenir la touche shift"));
      return false;

    }

  }
}
