/*
 * @creation 27 nov. 06
 * @modification $Date: 2006-12-05 10:18:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr;

import com.memoire.fu.FuLog;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author fred deniger
 * @version $Id: TrInvocationHandler.java,v 1.1 2006-12-05 10:18:20 deniger Exp $
 */
public final class TrInvocationHandler {
  private TrInvocationHandler() {
  }

  public static class ErrorHandler implements InvocationHandler {
    @Override
    public Object invoke(final Object _proxy, final Method _method, final Object[] _args) throws Throwable {
      TrUncaughtExceptionHandler.uncaughtExceptionOccured((Thread) _args[0], (Throwable) _args[1]);
      return null;
    }
  }

  public static Object createUncaughtExceptionHandler() {
    try {
      final Class c = getClassUncaughtExceptionHandler();
      return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{c},
          new ErrorHandler());
    } catch (final IllegalArgumentException | ClassNotFoundException e) {
      FuLog.error(e);
    }
    return null;
  }

  private static Class getClassUncaughtExceptionHandler() throws ClassNotFoundException {
    return Class.forName("java.lang.Thread$UncaughtExceptionHandler");
  }

  public static void initThreaExceptionHandler() {
    boolean isOk = false;
    try {
      final String r = System.getProperty("java.version");
      isOk = r != null && r.compareTo("1.5") >= 0;
    } catch (final SecurityException _evt1) {

    }
    if (!isOk) {
      return;
    }
    try {
      final Method m = Thread.class.getMethod("setDefaultUncaughtExceptionHandler",
          new Class[]{getClassUncaughtExceptionHandler()});
      m.invoke(Thread.currentThread(), new Object[]{createUncaughtExceptionHandler()});
    } catch (final Exception evt) {
      FuLog.error(evt);
    }
  }
}
