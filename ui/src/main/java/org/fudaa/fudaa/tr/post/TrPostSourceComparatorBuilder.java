/*
 * @creation 19 mars 07
 *
 * @modification $Date: 2007-06-13 12:58:14 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.fudaa.fdico.FDicoLib;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.layer.MvGridLayerGroup;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.persist.TrPostSourceComparaisonPersistReplay;
import org.geotools.util.WeakValueHashMap;

import javax.swing.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe utilitaire pour construire des sources issues d'une comparaison.
 *
 * @author fred deniger
 * @version $Id: TrPostSourceComparatorBuilder.java,v 1.6 2007-06-13 12:58:14 deniger Exp $
 */
public final class TrPostSourceComparatorBuilder {
  private TrPostSourceComparatorBuilder() {
  }

  private final static WeakValueHashMap DELTA_VAR = new WeakValueHashMap();

  public static H2dVariableType createDeltaVar(final H2dVariableType _init, final boolean _abs) {
    H2dVariableType var = (H2dVariableType) DELTA_VAR.get(_init);
    if (var == null) {
      var = H2dVariableType.createTempVar("\u0394" + _init.getName(), null);
      DELTA_VAR.put(_init, var);
    }
    return var;
  }

  public static TrPostSourceComparator createSrcComparator(final TrPostSource _initRef, final TrPostSource _initProj,
                                                           final TrPostSourceComparaisonPersistReplay replay,
                                                           final TrPostCommonImplementation _impl,
                                                           final ProgressionInterface _prog) {
    TrPostSource ref = _initRef;
    TrPostSource proj = _initProj;
    proj.buildDefaultVarUpdateLists();
    boolean useProjTime = replay.isUseProjTime();
    boolean useProjGrid = replay.isUseProjGrid();
    final double[] time = useProjTime ? _initProj.getTime().getInitTimeSteps() : _initRef.getTime().getInitTimeSteps();
    final EfGridInterface grid = useProjGrid ? _initProj.getGrid() : _initRef.getGrid();
    final String gridId = useProjGrid ? _initProj.getId() : _initRef.getId();
    // on est en mode rubar, si les donn�es source sont rubar.
    final boolean rubar = _initProj.containsElementVar();

    boolean openRef = false;
    boolean openProj = false;
    boolean isGridEquals = replay.isGridEquals();

    // le projet de reference
    // il est modifie si on utilise le maillage de projet ou si le type (rubar ou non) est modifie
    // la comparaison se fait sur le maillage importe ou le mode rubar est modifie
    if (useProjGrid || rubar != ref.containsElementVar()) {
      ref = new TrPostSourceProjected(ref, grid, useProjTime ? time : null, isGridEquals, _impl, gridId);
      openRef = true;
    } // il est egalement modifie si les pas de temps sont diff�rents
    else if (useProjTime) {
      // seuls les pas de temps sont modifies
      ref = new TrPostSourceProjected(ref, grid, time, true, _impl, gridId);
      openRef = true;
    }

    // le projet importe
    // modifie si on utilise le maillage du projet de ref ou si le mode est modifie
    if (!useProjGrid || rubar != proj.containsElementVar()) {
      proj = new TrPostSourceProjected(proj, grid, useProjTime ? null : time, isGridEquals, _impl, gridId);
      openProj = true;
    } // modifie si on utilise les pas de temps du projet importe
    // Dans ce cas, seuls les pas de temps sont modifies
    else if (!useProjTime) {
      proj = new TrPostSourceProjected(proj, grid, time, true, _impl, gridId);
      openProj = true;
    }
    if (openProj) {
      proj.openDatas(_prog, null, _impl);
      proj.buildDefaultVarUpdateLists();
    }
    if (openRef) {
      ref.openDatas(_prog, null, _impl);
      ref.buildDefaultVarUpdateLists();
    }
    final TrPostSourceComparator comparaison = new TrPostSourceComparator(ref, proj, _impl, replay);
    if (useProjTime) {
      comparaison.time_.setExprAndFmt(_initRef.getTime(), _prog);
    }

    comparaison.openDatas(_prog, null, _impl);
    comparaison.updateVarList();
    comparaison.buildDefaultVectors();
    comparaison.setInv(replay.isResulatInverse());
    _impl.getProject().getSources().addSourceBuilt(comparaison);
    return comparaison;
  }

  public static void createFleches(final TrPostSource _src, final List _target, final String _prefix) {
    final ListModel flecheRef = _src.getFlecheListModel();
    final int nb = flecheRef.getSize();
    final String suff = " - " + _prefix + "-" + (_src.getTitle());
    for (int i = 0; i < nb; i++) {
      final TrPostFlecheContent cont = (TrPostFlecheContent) _src.getFlecheListModel().getElementAt(i);
      final H2dVariableType var = H2dVariableType.createTempVar(cont.getDescription() + suff, cont.getVar().getShortName()
              + suff,
          cont.getVar().getCommonUnitString(), null);
      _target.add(cont.duplicate(_src, var));
    }
  }

  /**
   *
   */
  public static String getComparaisonTitle() {
    return FDicoLib.getS("Comparaison");
  }

  protected static void activeComparaison(final TrPostSource _src, final TrPostSource _toProject,
                                          final String _quickDiff, final TrPostProjet projet) {

    final TrPostCommonImplementation impl = projet.getImpl();
    final TrPostProjectCompPanel pn = new TrPostProjectCompPanel(_src.getGrid(), _toProject.getGrid(), _quickDiff, impl);
    pn.addComparePanel();
    final TrPostProjetCompTimeStepPanel timeStep = new TrPostProjetCompTimeStepPanel(_src, _toProject);
    pn.add(timeStep.getPn());
    final CtuluDialog dial = pn.createDialog(impl.getFrame());

    pn.setParent(dial);
    dial.setTitle(FDicoLib.getS("Comparaison avec {0}", _toProject.getTitle()));
    if (dial.afficheAndIsOk()) {
      final CtuluTaskDelegate task = impl.createTask(TrPostSourceComparatorBuilder.getComparaisonTitle());
      final ProgressionInterface prog = task.getStateReceiver();
      // true si le fichier src contient des donn�es zfn d�finies sur les noeuds

      boolean mustChangeTime = timeStep.useOneTimeStepImported();
      int selectedTimeStep = timeStep.getImportedTimeStepToUse();
      // la projection
      final TrPostSource proj = createSrcForSelectedTime(_toProject, impl, mustChangeTime, selectedTimeStep);
      mustChangeTime = timeStep.useOneTimeStepSrc();
      selectedTimeStep = timeStep.getSrcTimeStepToUse();

      // la source
      final TrPostSource src = createSrcForSelectedTime(_src, impl, mustChangeTime, selectedTimeStep);

      final Runnable r = new Runnable() {
        @Override
        public void run() {
          String srcName = impl.getCurrentProject().getSources().getNameOfSource(src);
          String srcNameShort = impl.getCurrentProject().getSources().getShortNameOfSource(src);
          String projName = impl.getCurrentProject().getSources().getNameOfSource(proj);
          String projNameShort = impl.getCurrentProject().getSources().getShortNameOfSource(proj);
          final boolean isOnImpGrid = pn.isCompareOnImportedGrid();
          Map<String, String> infosCreation = new HashMap<String, String>();
          TrPostSourceComparaisonPersistReplay replay = new TrPostSourceComparaisonPersistReplay(src.getId(), proj.getId(),
              pn.isInv(), pn.isGridEquals(),
              isOnImpGrid,
              pn.isCompareOnImportedTime());
          final TrPostSourceComparator comparaison = createSrcComparator(src, proj, replay, impl, prog);
          String viewTitle = null;
          if (pn.isInv()) {
            infosCreation.put(ZEbliCalquesPanel.DIFF_FIC, srcNameShort + " / " + projNameShort);
            viewTitle = getComparaisonTitle() + " " + srcNameShort + " / " + projNameShort;
          } else {
            infosCreation.put(ZEbliCalquesPanel.DIFF_FIC, projNameShort + " / " + srcNameShort);
            viewTitle = getComparaisonTitle() + " " + projNameShort + " / " + srcNameShort;
          }

          // -- ajout des infos de cr�ation --//
          String pref = TrResource.getS("Source") + " ";
          infosCreation.put(pref + ZEbliCalquesPanel.TITRE_FIC, srcName);
          TrPostSourceAbstractFromIdx.fillWithSourceInfo(pref, infosCreation, src);
          pref = TrResource.getS("Projet�") + " ";
          infosCreation.put(pref + ZEbliCalquesPanel.TITRE_FIC, projName);
          TrPostSourceAbstractFromIdx.fillWithSourceInfo(pref, infosCreation, proj);

          if (isOnImpGrid) {
            infosCreation.put(ZEbliCalquesPanel.MAILLAGE_FIC, projName);
          } else {
            infosCreation.put(ZEbliCalquesPanel.MAILLAGE_FIC, srcName);
          }

          final TrPostVisuPanel pnVisu = TrPostVisuPanel.buildVisuPanelForWidgets(projet, impl, comparaison);
          pnVisu.getInfosCreation().putAll(infosCreation);

          final BCalque calqueActif = pnVisu.getCalqueActif();

          if (isOnImpGrid) {
            pnVisu.getGridGroup().setTitle(MvResource.getS("Maillage import�"));
          }
          if (pn.isComparedGridDisplayed()) {
            final MvGridLayerGroup srcGrp = new MvGridLayerGroup(isOnImpGrid ? src.getGrid() : proj.getGrid());
            srcGrp.setTitle(isOnImpGrid ? MvResource.getS("Maillage") : MvResource.getS("Maillage import�"));
            pnVisu.addCalque(srcGrp);
            srcGrp.monter();
            srcGrp.monter();
          }
          impl.display(pnVisu, calqueActif, viewTitle, false);
        }
      };
      task.start(r);
    }
  }

  public static TrPostSource createSrcForSelectedTime(final TrPostSource _src, final TrPostCommonImplementation _impl,
                                                      final boolean _mustChangeTime, final int _selectedTimeStep) {
    if (_mustChangeTime) {
      final TrPostSourceOneTimeStep res = new TrPostSourceOneTimeStep(_src, _selectedTimeStep, _impl);
      res.buildDefaultVectors();
      _impl.getProject().getSources().addSourceBuilt(res);
      return res;
    }
    return _src;
  }
}
