/**
 * @creation 24 mars 2004
 * @modification $Date: 2006-10-27 10:24:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: TrPostSourceDefault.java,v 1.16 2006-10-27 10:24:42 deniger Exp $
 */
public final class TrPostSourceDefault extends TrPostSourceAbstractFromIdx {

  private final EfData[][] datas_;

  @Override
  public boolean containsElementVar() {
    return false;
  }

  final Collection<File> files;

  public TrPostInspectorReader createWatcher(final TrPostProjet _proj, final boolean _auto) {
    return null;
  }

  public TrPostSourceDefault(final File _file, final String _titre, final EfGridInterface _g, final double[] _time,
      final H2dVariableType[] _v, final EfData[][] _datas, final CtuluUI _impl) {
    super(_titre, _g, _time, _v, _impl);
    datas_ = _datas;
    files = Collections.unmodifiableList(Arrays.asList(_file));
  }

  @Override
  public EfData getInitData(final int _data, final int _timeStep) {
    return datas_[_data][_timeStep];
  }

  @Override
  public double getInitData(final int _varIdx, final int _timeIdx, final int _ptIdx) {
    return getInitData(_varIdx, _timeIdx).getValue(_ptIdx);
  }

  @Override
  public Collection<File> getFiles() {
    return files;
  }
}