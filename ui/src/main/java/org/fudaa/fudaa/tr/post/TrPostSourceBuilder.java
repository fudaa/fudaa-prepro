/*
 * @creation 24 mars 2004
 *
 * @modification $Date: 2007-06-20 12:23:39 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.ef.*;
import org.fudaa.dodico.ef.io.corelebth.CorEleBthFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinInterface;
import org.fudaa.dodico.ef.io.serafin.SerafinVolumeFileFormat;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.rubar.H2dRubarGridAreteSource;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.reflux.io.*;
import org.fudaa.dodico.refonde.io.RefondeINPResult;
import org.fudaa.dodico.refonde.io.RefondeQuickINPReader;
import org.fudaa.dodico.rubar.io.RubarDATFileFormat;
import org.fudaa.dodico.telemac.io.TelemacVariableMapper;
import org.fudaa.ebli.commun.BJava3DVersionTest;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.tr.common.Tr3DFactory;
import org.fudaa.fudaa.tr.common.TrFileFormatManager;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: TrPostSourceActivator.java,v 1.62 2007-06-20 12:23:39 deniger Exp $
 */
public final class TrPostSourceBuilder {
  private TrPostSourceBuilder() {
    super();
  }

  protected static void affiche3D(final TrPostSource _src, final TrPostCommonImplementation _impl) {
    if (!BJava3DVersionTest.isJava3DFound()) {
      _impl
          .error(
              "3D",
              TrResource
                  .getS(
                      "Le fichier de r�sultats corresond � un calcul 3D\n. Or, Java3D n'est pas install� sur votre poste.\nVous ne pouvez pas visualiser ce fichier de r�sultats.\nVoir le menu 'Aide>Tester Java3D' pour installer Java 3D"));
      return;
    }
    if (_impl.question("3D", TrResource.getS("Le fichier de r�sultats corresond � un calcul 3D.\nVoulez-vous ouvrir la vue 3D?"))) {
      final JFrame f = new JFrame();
      f.setIconImage(EbliResource.EBLI.getImage("3d"));
      new Tr3DFactory().afficheFrame(f, _src, _src.getInterpolator().getVect(), _impl, null, null);
    }
  }

  static boolean isReflux(final String _name) {
    return INPFileFormat.getInstance().isAccepted(_name) || RefluxRefondeSolutionFileFormat.getInstance().isAccepted(_name);
  }

  public static TrPostSource activeSourceAction(final File _f, final TrPostCommonImplementation _impl, final ProgressionInterface _prog) {
    return activeSourceAction(_f, _impl, _impl.getLauncher().getCurrentPrefHydId(), _prog, null);
  }

  /**
   * M�thode qui active la sourceReaderInterface.
   *
   * @param _f
   * @param _impl
   * @param _prog
   */
  public static TrPostSourceReaderInterface activeSourceReaderInterface(final File _f, final TrPostCommonImplementation _impl,
                                                                        final ProgressionInterface _prog) {
    TrPostSource src = activeSourceAction(_f, _impl, _prog);

    if (src != null && src instanceof TrPostSourceFromReader) {
      return ((TrPostSourceFromReader) src).getReader();
    } else {
      return null;
    }
  }

  /**
   * @param _f
   * @param _impl
   * @param _id l'identifiant du fichier utilise si le format du fichier n'a pas �t� trouve selon l'extension
   * @param _prog
   * @param _otherData
   */
  public static TrPostSource activeSourceAction(final File _f, final CtuluUI _impl, final String _id, final ProgressionInterface _prog,
                                                final Map _otherData) {
    if (_f == null) {
      return null;
    }
    final TrFileFormatManager mng = TrFileFormatManager.INSTANCE;
    final String name = _f.getName();
    if (isReflux(name)) {
      return activeINPSource(_f, _impl, _prog);
    } else if (mng.isRubarTPSFile(name)) {
      final TrPostSourceRubar rubar = activeRubarSrcAction(_f, _impl, _prog);
      if (rubar == null) {
        return null;
      }
      final TrPostRubarLoader loader = new TrPostRubarLoader(rubar, _f, true);
      loader.active(null, _impl, _prog);
      return rubar;
    } else
      //-- test serafin volumique --//
      if (SerafinVolumeFileFormat.getInstance().createFileFilter().accept(_f)) {
        return activeSerafinSourceAction(_f, _impl, _prog, _otherData, true);
      }

      //-- sinon test serafin classique--//
      // mettre serafin en dernier car le test est tres large ....
      else if (SerafinFileFormat.getInstance().createFileFilter().accept(_f)) {
        return activeSerafinSourceAction(_f, _impl, _prog, _otherData, false);
      }
    final FileFormat ft = FileFormat.findFileFormat(TrFileFormatManager.getAllGridFormat(), _f);
    if (ft != null) {
      return activeGridSource(_f, (FileFormatGridVersion) ft, _impl, _prog);
    }
    final String id = _id;
    if (FileFormatSoftware.REFLUX_IS.name.equals(id) || RefluxRefondeSolutionFileFormat.getInstance().isAccepted(_f)) {
      return activeINPSource(_f, _impl, _prog);
    } else if (FileFormatSoftware.TELEMAC_IS.name.equals(id)) {
      return activeSerafinSourceAction(_f, _impl, _prog, _otherData, false);
    } else if (FileFormatSoftware.RUBAR_IS.name.equals(id)) {
      final TrPostSourceRubar rubar = activeRubarSrcAction(_f, _impl, _prog);
      if (rubar != null) {
        final TrPostRubarLoader loader = new TrPostRubarLoader(rubar, _f, true);
        loader.active(null, _impl, _prog);
        return rubar;
      }
    }
    return null;
  }

  /**
   * @param file le fichier serafin
   * @param _impl l'impl parent
   * @param _inter la barre de progress
   * @return le source post
   */
  @SuppressWarnings("unchecked")
  public static TrPostSource activeSerafinSourceAction(final File file, final CtuluUI _impl, final ProgressionInterface _inter, final Map _otherData,
                                                       boolean isVolumique) {
    final CtuluIOOperationSynthese s;

    //-- si le format n'est pas volumique, ouverture classique --//
    if (isVolumique) {
      s = SerafinVolumeFileFormat.getInstance().read(file, _inter);
    } else {
      s = SerafinFileFormat.getInstance().read(file, _inter);
    }

    if (isFatalError(_impl, s)) {
      return null;
    }
    if (s == null) {
      return null;
    }
    final SerafinInterface ser = (SerafinInterface) s.getSource();
    if (_otherData != null) {
      _otherData.put("IPOBO", ser.getIpoboInitial());
    }
    if (_otherData != null) {
      _otherData.put("IPARAMS", ser.getIparam());
    }
    final EfGridInterface g = ser.getGrid();
    // 3D
    if (!SerafinFileFormat.is3DGrid(g)) {
      s.getAnalyze().clear();
      g.computeBord(/* ser.getPtsFrontiere(), */_inter, s.getAnalyze());
      g.createIndexRegular(_inter);
      if (_impl != null) {
        _impl.manageAnalyzeAndIsFatal(s.getAnalyze());
      }
    }
    final double[] timeStep = new double[ser.getTimeStepNb()];
    for (int i = timeStep.length - 1; i >= 0; i--) {
      timeStep[i] = ser.getTimeStep(i);
    }
    final Map shortNameVar = new HashMap();
    final H2dVariableType[] variable = new H2dVariableType[ser.getValueNb()];
    final TelemacVariableMapper mapper = new TelemacVariableMapper();
    for (int i = variable.length - 1; i >= 0; i--) {
      variable[i] = mapper.getUsedKnownVar(ser.getValueId(i));
      if (variable[i] == null) {
        variable[i] = H2dVariableType.createTempVar(ser.getValueId(i), shortNameVar);
      }
    }
    TrPostSourceReaderSerafin serafin = new TrPostSourceReaderSerafin(file, timeStep, variable, ser.getReadingInfo(), ser.getIdate());
    TrPostSourceAbstract r = new TrPostSourceSerafin(serafin, ser.getTitre(), g, _impl);
    if (SerafinFileFormat.is3DGrid(g)) {
      r = TrPostSourceTelemac3D.build((TrPostSourceSerafin) r, timeStep, ser, shortNameVar, _inter, _impl);
    }
    r.initShortNameMap(shortNameVar);
    return r;
  }

  public static void loadData(final CtuluUI _impl, final ProgressionInterface _inter, final TrPostProjet _proj) {
    if (_proj != null && _proj.openSrcDataAndIsModified(_impl, _inter)) {
      _proj.setProjectModified();
    }
  }

  /**
   * @param _f le fichier serafin
   * @param _impl l'impl parent
   * @param _inter la barre de progress
   * @return le source post
   */
  public static TrPostSourceRubar activeRubarSrcAction(final File _f, final CtuluUI _impl, final ProgressionInterface _inter) {
    // on recherche le fichier contenant le maillage
    final File dir = _f.getParentFile();
    final String name = CtuluLibFile.getSansExtension(_f.getName());
    final File maillage = RubarDATFileFormat.getInstance().getFileFor(dir, name);
    CtuluIOOperationSynthese s;
    H2dRubarGridAreteSource grid;
    // fichier DAT existe
    if (maillage.exists()) {
      s = RubarDATFileFormat.getInstance().readGrid(maillage, _inter);
      if (isFatalError(_impl, s)) {
        return null;
      }
      if (s == null) {
        return null;
      }
      grid = (H2dRubarGridAreteSource) s.getSource();
    } else {
      _impl.error(TrResource.getS("Le fichier {0} est requis", maillage.getName()));
      return null;
    }
    if (grid == null) {
      return null;
    }
    grid.getGrid().computeBord(_inter, s.getAnalyze());
    grid.getGrid().createIndexRegular(_inter);
    if (_impl != null) {
      _impl.manageAnalyzeAndIsFatal(s.getAnalyze());
    }
    return new TrPostSourceRubar(_f, name, grid, _impl);
  }

  public static TrPostSource activeINPSource(final File _f, final CtuluUI _impl, final ProgressionInterface _inter) {
    if (RefluxRefondeSolutionSequentielResult.isRefonde(_f)) {
      final File inp = getInpFileForRefonde(_f, _impl);
      if (inp == null) {
        return null;
      }
      return activeINPSource(inp, _f, _impl, _inter);
    }
    final INPChooseFile pn = new INPChooseFile(_f);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_impl.getParentComponent()))) {
      return activeINPSource(pn.getInp(), pn.getsov(), _impl, _inter);
    }
    return null;
  }

  static File getInpFileForRefonde(final File _f, final CtuluUI _impl) {
    final File inp = CtuluLibFile.changeExtension(_f, "inp");
    if (!CtuluLibFile.exists(inp)) {
      _impl.error("Refonde:\n" + TrResource.getS("Le fichier inp est requis pour la lecture des r�sultats.") + CtuluLibString.LINE_SEP
          + TrResource.getS("Vous pouvez le cr�er en utilisant les foncitons d'exportation de Fudaa-Refonde"));
      return null;
    }
    return inp;
  }

  public static TrPostSource activeGridSource(final File _f, final FileFormatGridVersion _fmt, final CtuluUI _impl, final ProgressionInterface _prog) {
    final CtuluIOOperationSynthese op = _fmt.readGrid(_f, _prog);
    if (isFatalError(_impl, op)) {
      return null;
    }
    final String[] ext = _fmt.getFileFormat().getExtensions();
    File f = _f;
    // pour les formats � fichier multiple, on prend la premiere extension (cor,ele,bth au hasard).
    if (ext.length > 1 && !CtuluLibFile.getExtension(f.getName()).toLowerCase().equals(ext[0].toLowerCase())) {
      f = CtuluLibFile.changeExtension(f, ext[0]);
    }
    if (op == null) {
      return null;
    }

    final EfGridInterface grid = ((EfGridSource) op.getSource()).getGrid();
    op.getAnalyze().clear();
    EfLib.orienteGrid(grid, _prog, true, op.getAnalyze());
    grid.computeBord(_prog, op.getAnalyze());
    grid.createIndexRegular(_prog);
    if (_impl != null) {
      _impl.manageAnalyzeAndIsFatal(op.getAnalyze());
    }
    final EfData[][] data = new EfData[1][1];
    data[0][0] = EfLib.getBathy(grid);
    return new TrPostSourceDefault(f, f.getName(), grid, new double[]{0}, new H2dVariableType[]{H2dVariableType.BATHYMETRIE}, data, _impl);
  }

  public static boolean fileExists(final CtuluUI _ui, final File _f) {
    if (_f == null || !_f.exists()) {
      if (_ui != null) {
        _ui.error(CtuluUIAbstract.getDefaultErrorTitle(), getNotFoundErr(_f), false);
      }
      return false;
    }
    return true;
  }

  public static String getNotFoundErr(final File _f) {
    return CtuluLib.getS("Le fichier {0} n'existe pas", _f == null ? "?" : _f.getAbsolutePath());
  }

  /**
   * @param _inpOrxinp le fichier inp ou ximp
   * @param _sovFile le fichier contenant les solutions
   * @param _impl l'impl parent
   * @param _inter la barre de progression
   * @return le source
   */
  public static TrPostSource activeINPSource(final File _inpOrxinp, final File _sovFile, final CtuluUI _impl, final ProgressionInterface _inter) {
    if (_impl == null) {
      FuLog.warning("_impl is null");
    }
    if (_inter != null) {
      _inter.setDesc(MvResource.getS("Lecture maillage"));
    }
    final File inpFile = _inpOrxinp;
    if (!fileExists(_impl, inpFile)) {
      return null;
    }
    EfGridInterface g = null;
    FileFormatGridVersion ft = null;
    boolean isSeiche = false;
    H2dVariableType refondeFirstVar = H2dVariableType.PHASE;
    CtuluIOOperationSynthese s = null;
    // le cas refonde
    if (RefluxRefondeSolutionSequentielResult.isRefonde(_sovFile)) {
      s = new RefondeQuickINPReader().read(_inpOrxinp, _inter);
      if (isFatalError(_impl, s)) {
        return null;
      }
      if (s == null) {
        return null;
      }
      final RefondeINPResult res = (RefondeINPResult) s.getSource();
      g = res.getGrid();
      isSeiche = res.isModuleSeiche();
      refondeFirstVar = res.getVariableForFirstCol();
    } else {
      if (INPFileFormat.getInstance().createFileFilter().accept(_inpOrxinp)) {
        ft = INPFileFormat.getInstance().getLastINPVersionImpl();
      } else if (CorEleBthFileFormat.getInstance().createFileFilter().accept(_inpOrxinp)) {
        ft = CorEleBthFileFormat.getInstance();
      } else {
        ft = (FileFormatGridVersion) FileFormat.findFileFormat(TrFileFormatManager.getAllGridFormat(), _inpOrxinp);
      }
      s = ft.readGrid(inpFile, _inter);

      if (isFatalError(_impl, s) ||s==null) {
        return null;
      }
      s.getAnalyze().clear();
      g = ((EfGridSource) s.getSource()).getGrid();
    }
    g.computeBord(_inter, s.getAnalyze());
    g.createIndexRegular(_inter);
    if (_impl != null) {
      _impl.manageAnalyzeAndIsFatal(s.getAnalyze());
    }
    final File sovFile = _sovFile;
    if (!fileExists(_impl, sovFile)) {
      return null;
    }
    if (_inter != null) {
      _inter.setDesc(TrResource.getS("Lecture solutions"));
      _inter.setProgression(0);
    }
    final RefluxRefondeSolutionNewReader r = new RefluxRefondeSolutionNewReader(RefluxRefondeSolutionFileFormat.getInstance());
    r.setFile(sovFile);
    s = r.read();
    if (isFatalError(_impl, s)) {
      return null;
    }
    if (s == null) {
      return null;
    }
    final RefluxRefondeSolutionSequentielResult sol = (RefluxRefondeSolutionSequentielResult) s.getSource();
    // pour le cas refonde
    if (sol.isRefonde()) {
      sol.setRefondeSeiche(isSeiche, refondeFirstVar);
    }
    if (sol.getNbPoint() == 0) {
      if (_impl != null) {
        _impl.error(TrResource.getS("Le fichier {0} est corrompu", sovFile.getAbsolutePath()));
      }
      return null;
    }
    if (sol.getNbPoint() != g.getPtsNb()) {
      if (_impl != null) {
        _impl.error(TrResource.getS("Le fichier de maillage ne correspond pas au fichier des solutions s�lectionn�"));
      }
      if (Fu.DEBUG) {
        FuLog.debug("FTR: nb point maillage: " + g.getPtsNb() + " sol=" + sol.getNbPoint());
      }
      return null;
    }
    final double[] timeStep = new double[sol.getTimeStepNb()];
    for (int i = timeStep.length - 1; i >= 0; i--) {
      timeStep[i] = sol.getTimeStep(i);
    }

    // support temperature
    final H2dVariableType[] variable = sol.getVars();
    final int[] placement = sol.getPosition();
    TrPostSourceReaderReflux reflux = new TrPostSourceReaderReflux(sovFile, g, timeStep, variable, placement, new RefluxSolutionSequentielReader(sol,
        sovFile));
    final TrPostSourceReflux res = new TrPostSourceReflux(reflux, sovFile.getAbsolutePath(), g, _impl);
    return res;
  }

  private static boolean isFatalError(final CtuluUI _impl, final CtuluIOOperationSynthese _s) {
    if (_s == null) {
      return false;
    }
    if (_impl == null) {
      if (_s.containsSevereError()) {
        _s.printAnalyze();
        return true;
      }
      return false;
    }
    return _impl.manageErrorOperationAndIsFatal(_s);
  }

  static class INPChooseFile extends CtuluDialogPanel {
    private JTextField inpField_;
    private JTextField sovField_;
    private File inp_;
    private File sov_;

    /**
     * @param _inpOrSov le fichier inp ou sov
     */
    public INPChooseFile(final File _inpOrSov) {
      final String fic = CtuluLibFile.getSansExtension(_inpOrSov.getName());
      File inp = new File(_inpOrSov.getParentFile(), CtuluLibFile.getFileName(fic, INPFileFormat.getINPExtension()));
      if (!inp.exists()) {
        final File cor = CtuluLibFile.changeExtension(_inpOrSov, CorEleBthFileFormat.getInstance().getExtensions()[0]);
        if (cor.exists()) {
          inp = cor;
        }
      }

      final File sov = RefluxRefondeSolutionFileFormat.getInstance().isAccepted(_inpOrSov) ? _inpOrSov : new File(_inpOrSov.getParentFile(),
          CtuluLibFile.getFileName(fic, RefluxRefondeSolutionFileFormat.getInstance().getExtensions()[0]));
      init(inp, sov);
    }

    /**
     * @param _inp le chemin du fichier inp
     * @param _sov le chemin du fichier sov
     */
    public INPChooseFile(final File _inp, final File _sov) {
      init(_inp, _sov);
    }

    private void init(final File _inp, final File _sov) {
      addEmptyBorder(10);
      final String s = TrResource.getS("Fichier") + CtuluLibString.ESPACE;
      setLayout(new BuGridLayout(2, 10, 10, true, false));
      addLabel(CtuluLibString.EMPTY_STRING);
      add(new BuLabelMultiLine(TrResource.getS("Choisir le fichier sov et le fichier de maillage correspondant")));
      inpField_ = addLabelFileChooserPanel(MvResource.getS("Fichier de maillage"), _inp, false, false);
      sovField_ = addLabelFileChooserPanel(s + RefluxRefondeSolutionFileFormat.getInstance().getExtensions()[0], _sov, false, false);
    }

    /**
     * @return le fichier inp choisi
     */
    public File getInp() {
      return inp_;
    }

    /**
     * @return le fichier sov
     */
    public File getsov() {
      return sov_;
    }

    @Override
    public boolean isDataValid() {
      inp_ = new File(inpField_.getText());
      sov_ = new File(sovField_.getText());
      if (!inp_.exists() || !sov_.exists()) {
        final StringBuffer b = new StringBuffer();
        if (!inp_.exists()) {
          b.append(getNotFoundErr(inp_));
        }
        if (!sov_.exists()) {
          if (b.length() > 0) {
            b.append(CtuluLibString.LINE_SEP);
          }
          b.append(getNotFoundErr(sov_));
        }
        setErrorText(b.toString());
        return false;
      }
      return true;
    }
  }
}
