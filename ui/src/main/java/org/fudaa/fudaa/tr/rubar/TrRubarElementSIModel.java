/**
 * @creation 12 janv. 2005
 * @modification $Date: 2007-01-19 13:14:12 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuTable;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;
import org.fudaa.fudaa.meshviewer.model.MvInfoDelegate;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarElementSIModel.java,v 1.12 2007-01-19 13:14:12 deniger Exp $
 */
public class TrRubarElementSIModel extends MvElementModelDefault {

  public TrRubarElementSIModel(final EfGridInterface _g) {
    super(_g);
  }

  public TrRubarElementSIModel(final EfGridInterface _g, final MvInfoDelegate _d) {
    super(_g, _d);
  }

  @Override
  public EfGridInterface getGrid() {
    return g_;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    if (delegate_ != null) { return ((TrRubarInfoSenderDelegate) delegate_).createElementSITable(); }
    return null;
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
    if (delegate_ != null) {
      ((TrRubarInfoSenderDelegate) delegate_).fillWithElementSIInfo(_d, _layer.getLayerSelection(), _layer.getTitle());
    }
  }
}