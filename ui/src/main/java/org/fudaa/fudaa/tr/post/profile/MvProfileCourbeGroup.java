/*
 * @creation 21 nov. 06
 * 
 * @modification $Date: 2007-05-04 13:59:49 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import com.memoire.fu.FuLog;
import java.util.Iterator;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGGrapheDuplicator;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGGroupPersistBuilder;

/**
 * @author fred deniger
 * @version $Id: MvProfileCourbeGroup.java,v 1.5 2007-05-04 13:59:49 deniger Exp $
 */
public class MvProfileCourbeGroup extends EGGroup {

  private CtuluVariable var_;

  public MvProfileCourbeGroup(final CtuluVariable _var) {
    super();
    var_ = _var;
    updateTitle();
  }

  private void updateTitle() {
    setTitle(var_==null?CtuluLibString.EMPTY_STRING:var_.toString());
  }

  @Override
  protected EGGroupPersistBuilder createPersistBuilder() {
    return new MvProfileCourbeGroupPersistBuilder();
  }

  @Override
  public EGGroup duplicate(EGGrapheDuplicator _duplicator) {

    // -- duplication de l objet --//
    EGGroup duplic = new MvProfileCourbeGroup(var_);
    duplic.setVisible(this.isVisible_);
    duplic.setTitle(this.getTitle());
    duplic.setEgParent(this.getEgParent());
    duplic.setAxeY(this.getAxeY().duplicate());
    for (Iterator it = this.getChildren().iterator(); it.hasNext();) {
      Object item = it.next();
      if (item instanceof EGCourbeChild) {
        duplic.addEGComponent((EGCourbeChild) ((EGCourbeChild) item).duplicate(duplic, _duplicator),-1,false);
      }

    }

    return duplic;

  }

  public CtuluVariable getVar() {
    return var_;
  }

  /**
   * @return the var_
   */
  protected CtuluVariable getVar_() {
    return var_;
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  /**
   * @param var the var_ to set
   */
  public void setVar(CtuluVariable var) {
    var_ = var;
    if (varId != null && !varId.equals(var.getID())) {
      FuLog.error("The variable used " + var.getID() + " is not equals to the saved id" + varId);
    }
    varId = null;
    updateTitle();
  }

  String varId;

  public void setVarId(String varId) {
    this.varId = varId;
  }

}
