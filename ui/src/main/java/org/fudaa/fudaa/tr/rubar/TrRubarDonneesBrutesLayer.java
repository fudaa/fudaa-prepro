/**
 * @creation 4 oct. 2004
 * @modification $Date: 2008-02-20 10:11:49 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.dodico.h2d.H2dParallelogrammeDataAbstract;
import org.fudaa.dodico.h2d.H2dParallelogrammeManager;
import org.fudaa.dodico.h2d.H2dRegularGridData;
import org.fudaa.dodico.h2d.rubar.H2dRubarDonneesBrutes;
import org.fudaa.dodico.h2d.rubar.H2dRubarDonneesBrutesListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarDonneesBrutesMng;
import org.fudaa.dodico.h2d.rubar.H2dRubarDonneesBrutesVisitorClient;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.calque.find.CalqueFindActionDefault;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarDonneesBrutesLayer.java,v 1.27.6.1 2008-02-20 10:11:49 bmarchan Exp $
 */
public final class TrRubarDonneesBrutesLayer extends ZCalqueAffichageDonnees implements H2dRubarDonneesBrutesListener {

  @Override
  public EbliFindActionInterface getFinder() {
    return new CalqueFindActionDefault(this);
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return null;
  }

  @Override
  public BConfigurableInterface getSingleConfigureInterface() {
    return delegate_ == null ? null : delegate_.getSingleConfigureInterface();
  }

  @Override
  public int[] getSelectedObjectInTable() {
    return getSelectedIndex();
  }

  @Override
  public void donneesBrutesNuageDataChanged(final H2dRubarDonneesBrutes _source) {}

  @Override
  public void donneesBrutesNuageDataNombreChanged(final H2dRubarDonneesBrutes _source) {}

  @Override
  public void donneesBrutesNuageSupportAddedOrRemoved(final H2dRubarDonneesBrutes _source) {}

  @Override
  public void donneesBrutesNuageSupportChanged(final H2dRubarDonneesBrutes _source) {}

  class ChangeTypeClient implements H2dRubarDonneesBrutesVisitorClient {

    @Override
    public void visitGrid(final H2dRubarDonneesBrutes _b) {
      setDelegate(new ZCalqueGrilleReguliere(new DelegateRegularGrid(br_)));
    }

    @Override
    public void visitParall(final H2dRubarDonneesBrutes _b) {
      setDelegate(new ZCalquePolygone(new DelegateParall(br_)) {

        @Override
        public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
          super.paintIcon(_c, _g, _x, _y);
          if ((modele_ == null) || (modele_.getNombre() == 0)) { return; }
          _g.setColor(getForeground());
          final int w = getIconWidth();
          final int h = getIconHeight();
          int x1 = _x + 2 * w / 5;
          final int y1 = _y + h / 5;
          final int x2 = _x + 4 * w / 5;
          final int x4 = _x + w / 5;
          final int y4 = _y + 4 * h / 5;
          _g.drawLine(x1, y1, x2, y1);
          _g.drawLine(x1, y1, x4, y4);
          // on recupere X1 pour x3
          x1 = _x + 3 * w / 5;
          _g.drawLine(x1, y4, x4, y4);
          _g.drawLine(x1, y4, x2, y1);

        }
      });
    }
  }

  static class DelegateParall extends ZModeleDonnesAbstract implements ZModelePolygone {

    private final H2dParallelogrammeManager target_;

    protected DelegateParall(final H2dRubarDonneesBrutes _br) {
      if (_br == null) {
        target_ = null;
      } else {
        target_ = _br.getParall();
      }
    }
    
    @Override
    public boolean isPainted(int idx) {
      return true;
    }

    /**
     * @return le domaine pris par les parall ou null si aucun.
     */
    @Override
    public GrBoite getDomaine() {
      if (getNombre() > 0) {
        final GrBoite b = new GrBoite();
        final CtuluRange range = new CtuluRange();
        H2dParallelogrammeDataAbstract d = target_.getParall(0);
        d.getXrange(range);
        b.e_ = new GrPoint();
        b.o_ = new GrPoint();
        b.e_.x_ = range.max_;
        b.o_.x_ = range.min_;
        d.getYrange(range);
        b.e_.y_ = range.max_;
        b.o_.y_ = range.min_;
        for (int i = target_.getNbParall() - 1; i > 0; i--) {
          d = target_.getParall(i);
          // les X
          d.getXrange(range);
          if (range.max_ > b.e_.x_) {
            b.e_.x_ = range.max_;
          }
          if (range.min_ < b.o_.x_) {
            b.o_.y_ = range.min_;
          }
          d.getYrange(range);
          if (range.max_ > b.e_.y_) {
            b.e_.y_ = range.max_;
          }
          if (range.min_ < b.o_.y_) {
            b.o_.y_ = range.min_;
          }
        }
        return b;
      }
      return null;
    }

    @Override
    public int getNombre() {
      return target_ == null ? 0 : target_.getNbParall();
    }

    @Override
    public Object getObject(final int _ind) {
      return null;
    }

    /**
     * @param _idxPar l'indice du parallelogramme
     * @param _idxPtOnParall l'indice du point sur le parall
     * @param _dest le point recevant les coordonnees.
     */
    public void getPoint(final int _idxPar, final int _idxPtOnParall, final GrPoint _dest) {
      final H2dParallelogrammeDataAbstract d = target_.getParall(_idxPar);
      if (_idxPar == 0) {
        _dest.x_ = d.getX1();
        _dest.y_ = d.getY1();
      } else if (_idxPar == 1) {
        _dest.x_ = d.getX2();
        _dest.y_ = d.getY2();
      } else if (_idxPar == 3) {
        _dest.x_ = d.getX4();
        _dest.y_ = d.getY4();
      } else if (_idxPar == 2) {
        _dest.x_ = d.getX4() - d.getX1() + d.getX2();
        _dest.y_ = d.getY4() - d.getY1() + d.getY2();
      }
      FuLog.warning(new Throwable());

    }

    public int nbPoints(final int _i) {
      return 4;
    }

    @Override
    public boolean getCentre(GrPoint pt, int idx, boolean force) {
      final H2dParallelogrammeDataAbstract d = target_.getParall(idx);
      pt.x_ = 0;
      pt.y_ = 0;
      for (int i = 0; i < 4; i++) {
        pt.x_ += d.getX(i);
        pt.y_ += d.getY(i);
      }
      pt.x_ = pt.x_ / 4;
      pt.y_ = pt.y_ / 4;
      return true;

    }

    @Override
    public void polygone(final GrPolygone _p, final int _i, final boolean _force) {
      if (_p.nombre() != 4) {
        _p.sommets_.setSize(4);
      }
      final H2dParallelogrammeDataAbstract d = target_.getParall(_i);
      GrPoint grpt = _p.sommets_.renvoie(0);
      if (grpt == null) {
        grpt = new GrPoint(d.getX1(), d.getY1(), 0);
        _p.sommets_.remplace(grpt, 0);
      } else {
        grpt.setCoordonnees(d.getX1(), d.getY1(), 0);
      }
      grpt = _p.sommets_.renvoie(1);
      if (grpt == null) {
        grpt = new GrPoint(d.getX2(), d.getY2(), 0);
        _p.sommets_.remplace(grpt, 1);
      } else {
        grpt.setCoordonnees(d.getX2(), d.getY2(), 0);
      }
      grpt = _p.sommets_.renvoie(2);
      if (grpt == null) {
        grpt = new GrPoint(d.getX4() - d.getX1() + d.getX2(), d.getY4() - d.getY1() + d.getY2(), 0);
        _p.sommets_.remplace(grpt, 2);
      } else {
        grpt.setCoordonnees(d.getX4() - d.getX1() + d.getX2(), d.getY4() - d.getY1() + d.getY2(), 0);
      }
      grpt = _p.sommets_.renvoie(3);
      if (grpt == null) {
        grpt = new GrPoint(d.getX4(), d.getY4(), 0);
        _p.sommets_.remplace(grpt, 3);
      } else {
        grpt.setCoordonnees(d.getX4(), d.getY4(), 0);
      }
    }
  }

  static class DelegateRegularGrid extends ZModeleDonnesAbstract implements ZModeleGrilleReguliere {

    H2dRegularGridData target_;

    protected DelegateRegularGrid(final H2dRubarDonneesBrutes _br) {
      if (_br != null) {
        target_ = _br.getGrid();
      }
    }

    @Override
    public GrBoite getDomaine() {
      if (target_ == null) { return null; }
      final GrBoite b = new GrBoite();
      b.e_ = new GrPoint(target_.getXmax(), target_.getYmax(), 0);
      b.o_ = new GrPoint(target_.getXmin(), target_.getYmin(), 0);
      return b;
    }

    /**
     * @return l'ecart entre deux lignes.
     */
    @Override
    public double getDX() {
      return target_.getDX();
    }

    /**
     * @return l'ecart entre deux colonnes.
     */
    @Override
    public double getDY() {
      return target_.getDY();
    }

    @Override
    public int getNbPtOnX() {
      return target_.getNbPtOnX();
    }

    @Override
    public double getXMax() {
      return target_.getXmax();
    }

    @Override
    public double getYMax() {
      return target_.getYmax();
    }

    @Override
    public int getNbPtOnY() {
      return target_.getNbPtOnY();
    }

    /**
     * @return le nombre de points de la grille.
     */
    @Override
    public int getNombre() {

      return target_ == null ? 0 : target_.getNbPoint();
    }

    @Override
    public Object getObject(final int _ind) {
      return null;
    }

    @Override
    public double getXMin() {
      return target_.getX0();
    }

    @Override
    public double getYMin() {
      return target_.getY0();
    }

    @Override
    public boolean point(final GrPoint _p, final int _i, final boolean _force) {
      if (target_ == null) { return false; }
      _p.setCoordonnees(target_.getX(_i), target_.getY(_i), 0);
      return true;
    }

    @Override
    public double getX(final int _i) {
      return target_.getX(_i);
    }

    @Override
    public double getY(final int _i) {
      return target_.getY(_i);
    }
  }

  protected final static String getLayerName(final String _id) {
    return "cqInitialData" + _id;
  }

  H2dRubarDonneesBrutes br_;

  ZCalqueAffichageDonnees delegate_;
  String id_;

  /**
   */
  public TrRubarDonneesBrutesLayer(final H2dRubarDonneesBrutesMng _mng, final String _id) {
    setForeground(Color.LIGHT_GRAY);
    br_ = _mng.get(_id);
    id_ = _id;
    setName(getLayerName(id_));
    _mng.addListener(this);
    updateType();
  }

  protected void setDelegate(final ZCalqueAffichageDonnees _a) {
    if (_a != delegate_) {
      if (delegate_ != null) {
        remove(delegate_);
        delegate_.clearSelection();
      }
      delegate_ = _a;
      if (delegate_ != null) {
        add(delegate_);
        if (getForeground() != null) {
          delegate_.setForeground(getForeground());
        }
        delegate_.setRapide(isRapide());
        delegate_.setAttenue(isAttenue());
        if (getIconModel(0) != null) {
          delegate_.setIconModel(0, getIconModel(0));
        }
        if (getFont() != null) {
          delegate_.setFont(getFont());
        }
      }
      // pour avertir l'arbre des calques.
      firePropertyChange("foreground", null, Color.BLACK);
      revalidate();
      repaint();
    }
  }

  // normalement, je devrais utiliser une interface pour decoupler modele/vue.
  protected final void updateType() {
    if ((br_ == null) || (br_.getTypeOrganisation() == null)) {
      setDelegate(null);
    } else {
      br_.getTypeOrganisation().visit(new ChangeTypeClient(), br_);
    }
  }

  @Override
  public boolean changeSelection(final GrPoint _pt, final int _tolerancePixel, final int _action) {
    return (delegate_ == null) ? false : delegate_.changeSelection(_pt, _tolerancePixel, _action);
  }

  @Override
  public boolean changeSelection(final LinearRing _poly, final int _action, final int _mode) {
    return (delegate_ == null) ? false : delegate_.changeSelection(_poly, _action, _mode);
  }

  @Override
  public String editSelected() {
    final EbliActionInterface[] actions = getActions();
    if (actions.length > 0) {
      actions[0].actionPerformed(null);
      return null;
    } else {
      return super.editSelected();
    }
  }

  @Override
  public void clearSelection() {
    if (delegate_ != null) {
      delegate_.clearSelection();
    }
  }

  @Override
  public void selectAll() {
    if (delegate_ != null) {
      delegate_.selectAll();
    }
  }

  @Override
  public void inverseSelection() {
    if (delegate_ != null) {
      delegate_.inverseSelection();
    }
  }

  @Override
  public void donneesBrutesDataChanged(final H2dRubarDonneesBrutes _source) {}

  @Override
  public void donneesBrutesDataNombreChanged(final H2dRubarDonneesBrutes _source) {}

  @Override
  public void donneesBrutesSupportAddedOrRemoved(final H2dRubarDonneesBrutes _source) {
    if (_source == br_) {
      repaint();
    }

  }

  @Override
  public void donneesBrutesSupportChanged(final H2dRubarDonneesBrutes _source) {
    if (_source == br_) {
      repaint();
    }
  }

  public void donneesBrutesMngCreated(final H2dRubarDonneesBrutes _sourceCreated) {
    if ((br_ == null) && (_sourceCreated.getID().equals(id_))) {
      br_ = _sourceCreated;
      updateType();

    }
  }

  @Override
  public void donneesBrutesTypeChanged(final H2dRubarDonneesBrutes _source) {
    updateType();
  }

  @Override
  public GrBoite getDomaine() {
    return (delegate_ == null) ? null : delegate_.getDomaine();
  }

  @Override
  public LineString getSelectedLine() {
    return (delegate_ == null) ? null : delegate_.getSelectedLine();

  }

  @Override
  public int[] getSelectedIndex() {
    return (delegate_ == null) ? null : delegate_.getSelectedIndex();
  }

  @Override
  public int getNbSelected() {
    return (delegate_ == null) ? 0 : delegate_.getNbSelected();
  }

  @Override
  public GrBoite getDomaineOnSelected() {
    return (delegate_ == null) ? null : delegate_.getDomaineOnSelected();
  }

  @Override
  public boolean isOnlyOneObjectSelected() {
    return (delegate_ == null) ? false : delegate_.isOnlyOneObjectSelected();
  }

  @Override
  public boolean isSelectionEmpty() {
    return (delegate_ == null) ? true : delegate_.isSelectionEmpty();
  }

  @Override
  public ZModeleDonnees modeleDonnees() {
    return (delegate_ == null) ? null : delegate_.modeleDonnees();
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
      final GrBoite _clipReel) {
    if (delegate_ == null) { return; }
    delegate_.paintDonnees(_g, _versEcran, getVersReel(), _clipReel);

  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    if (delegate_ == null) {
      super.paintIcon(_c, _g, _x, _y);
      final int w = getIconWidth();
      final int h = getIconHeight();
      _g.drawLine(_x, _y, _x + w, _y + h);
      _g.drawLine(_x + w, _y, _x, _y + h);
    } else {
      delegate_.paintIcon(_c, _g, _x, _y);
    }
  }

  @Override
  public void doPaintSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
      final GrBoite _clipReel) {
    if (delegate_ == null) { return; }
    delegate_.paintSelection(_g, _trace, _versEcran, _clipReel);
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    if (delegate_ != null) { return delegate_.selection(_pt, _tolerance); }
    return null;
  }

  @Override
  public CtuluListSelection selection(final LinearRing _poly, final int _mode) {
    if (delegate_ != null) { return delegate_.selection(_poly, _mode); }
    return null;
  }

  @Override
  public void setAttenue(final boolean _attenue) {
    if (delegate_ != null) {
      delegate_.setAttenue(_attenue);
    }
    super.setAttenue(_attenue);
  }

  @Override
  public void setFont(final Font _v) {
    if (delegate_ != null) {
      delegate_.setFont(_v);
    }
    super.setFont(_v);
  }

  @Override
  public void setForeground(final Color _v) {
    if (delegate_ != null) {
      delegate_.setForeground(_v);
    }
    super.setForeground(_v);
  }

  @Override
  public void setRapide(final boolean _v) {
    if (delegate_ != null) {
      delegate_.setRapide(_v);
    }
    super.setRapide(_v);
  }

}
