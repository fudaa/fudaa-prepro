/**
 *  @creation     19 janv. 2005
 *  @modification $Date: 2006-09-19 15:07:27 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

/**
 * @author Fred Deniger
 * @version $Id: TrPostRubarEnvResultat.java,v 1.5 2006-09-19 15:07:27 deniger Exp $
 */
public class TrPostRubarEnvResultat {
  double hMax_;
  double quMax_;
  double qvMax_;

  double tHMax_;
  double tQMax_;

  public TrPostRubarEnvResultat() {
    super();
  }

  public final double getHMax() {
    return hMax_;
  }

  public final double getQuMax() {
    return quMax_;
  }

  public final double getQvMax() {
    return qvMax_;
  }

  public final double getTHMax() {
    return tHMax_;
  }

  public final double getTQMax() {
    return tQMax_;
  }

  final void setHMax(final double _max) {
    hMax_ = _max;
  }

  final void setQuMax(final double _quMax) {
    quMax_ = _quMax;
  }

  final void setQvMax(final double _qvMax) {
    qvMax_ = _qvMax;
  }

  final void setTHMax(final double _max) {
    tHMax_ = _max;
  }

  final void setTQMax(final double _max) {
    tQMax_ = _max;
  }

}