/**
 * 
 */
package org.fudaa.fudaa.tr.post;

import org.locationtech.jts.geom.Coordinate;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.operation.EfTrajectoireParameters;
import org.fudaa.dodico.ef.operation.EfTrajectoireParametersMarqueur;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.data.TrPostVariableFinder;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostTrajectoireTaskModel;

class TrPostTrajectoireLineLayerPersistData {

  private boolean isStreamLine;
  private int finesse=1;
  private String vx;
  private String vy;
  private Coordinate point1;
  private Coordinate point2;
  private int numberOfPoint;
  private double initTime;
  private double duration;
  private boolean isMarkDefined;
  private double markDeta;
  private boolean markOnTimeStep;
  private String[] computeVariables;

  /**
   * 
   */
  public TrPostTrajectoireLineLayerPersistData() {}

  public TrPostTrajectoireLineLayerPersistData(EfTrajectoireParameters params) {
    isStreamLine = params.isLigneDeCourant;
    vx = params.vx.getID();
    vy = params.vy.getID();
    finesse = params.finesse_;
    point1 = new Coordinate(params.segment_.get(0));
    point2 = new Coordinate(params.segment_.get(params.segment_.size() - 1));
    numberOfPoint = params.nbPointsInitiaux_;
    initTime = params.firstTimeStep_;
    duration = params.dureeIntegration_;
    if (params.marqueur_ != null) {
      isMarkDefined = true;
      markDeta = params.marqueur_.deltaMax_;
      markOnTimeStep = params.marqueur_.timeStep_;
    }
    if (!CtuluLibArray.isEmpty(params.varsASuivre_)) {
      computeVariables = new String[params.varsASuivre_.size()];
      List<CtuluVariable> varsASuivre = params.varsASuivre_;
      int nb = varsASuivre.size();
      for (int i = 0; i < nb; i++) {
        computeVariables[i] = varsASuivre.get(i).getID();
      }

    }

  }

  public EfTrajectoireParameters restore(TrPostSource src, CtuluAnalyze log) {
    EfTrajectoireParameters res = new EfTrajectoireParameters();
    res.finesse_ = finesse;
    res.dureeIntegration_ = duration;
    res.nbPointsInitiaux_ = numberOfPoint;
    res.isLigneDeCourant=isStreamLine;
    TrPostVariableFinder finder = new TrPostVariableFinder(src);
    res.vx = finder.getVariable(vx);
    if (res.vx == null) {
      log.addFatalError(TrResource.getS("La vitesse Vx d�finie par {0} n'existe pas dans le projet", vx));
      return null;
    }
    res.vy = finder.getVariable(vy);
    if (res.vy == null) {
      log.addFatalError(TrResource.getS("La vitesse Vy d�finie par {0} n'existe pas dans le projet", vy));
      return null;
    }
    res.segment_ = new ArrayList<Coordinate>();
    res.segment_.add(point1);
    res.segment_.add(point2);
    res.points_=TrPostTrajectoireTaskModel.getPointsFromSegment(point1, point2, numberOfPoint);
    if ((isMarkDefined||markOnTimeStep) && markDeta > 0) {
      res.marqueur_ = new EfTrajectoireParametersMarqueur();
      res.marqueur_.deltaMax_ = markDeta;
      res.marqueur_.timeStep_ = markOnTimeStep;
    }

    res.firstTimeStepIdx_ = findTimeStep(src);
    if (res.firstTimeStepIdx_ < 0) {
      res.firstTimeStepIdx_ = src.getNbTimeStep() / 2;
      if (res.firstTimeStepIdx_ < 0) res.firstTimeStepIdx_ = 0;
      else if (res.firstTimeStepIdx_ >= src.getNbTimeStep()) {
        res.firstTimeStepIdx_ = src.getNbTimeStep() - 1;
      }
      log
          .addWarn(TrResource
              .getS(
                  "Le pas de temps {0} n'a pas �t� trouv� dans le projet. Le pas de temps initial a �t� r�initialis� par d�faut",
                  Double.toString(initTime)));
    }
    res.firstTimeStep_ = src.getTimeStep(res.firstTimeStepIdx_);
    if(!CtuluLibArray.isEmpty(computeVariables)){
      res.varsASuivre_=new ArrayList<CtuluVariable>();
      for (String var : computeVariables) {
        H2dVariableType variable = finder.getVariable(var);
        if(variable!=null) res.varsASuivre_.add(variable);
        
      }
    }
    return res;
  }

  private int findTimeStep(TrPostSource src) {
    int nbTime = src.getTime().getNbTimeStep();
    for (int i = 0; i < nbTime; i++) {
      if (CtuluLib.isEquals(initTime, src.getTimeStep(i), 1e-3)) { return i; }

    }
    return -1;
  }

  public void setStreamLine(boolean isStreamLine) {
    this.isStreamLine = isStreamLine;
  }

  public boolean isStreamLine() {
    return isStreamLine;
  }

  public void setVx(String vx) {
    this.vx = vx;
  }

  public String getVx() {
    return vx;
  }

  public void setPoint1(Coordinate point1) {
    this.point1 = point1;
  }

  public Coordinate getPoint1() {
    return point1;
  }

  public void setVy(String vy) {
    this.vy = vy;
  }

  public String getVy() {
    return vy;
  }

  public void setNumberOfPoint(int numberOfPoint) {
    this.numberOfPoint = numberOfPoint;
  }

  public int getNumberOfPoint() {
    return numberOfPoint;
  }

  public void setInitTime(double initTime) {
    this.initTime = initTime;
  }

  public double getInitTime() {
    return initTime;
  }

  public void setDuration(double duration) {
    this.duration = duration;
  }

  public double getDuration() {
    return duration;
  }

  public void setMarkDeta(double markDeta) {
    this.markDeta = markDeta;
  }

  public double getMarkDeta() {
    return markDeta;
  }

  public void setPoint2(Coordinate point2) {
    this.point2 = point2;
  }

  public Coordinate getPoint2() {
    return point2;
  }

  public void setMarkOnTimeStep(boolean markOnTimeStep) {
    this.markOnTimeStep = markOnTimeStep;
  }

  public boolean isMarkOnTimeStep() {
    return markOnTimeStep;
  }

  public void setComputeVariables(String[] computeVariables) {
    this.computeVariables = computeVariables;
  }

  public String[] getComputeVariables() {
    return computeVariables;
  }

  public void setFinesse(int finesse) {
    this.finesse=finesse;
    
  }

}
