/*
 * @creation 19 avr. 2006
 * @modification $Date: 2007-02-07 09:56:18 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormat;

/**
 * @author fred deniger
 * @version $Id: TrPostInspectorReaderRubar.java,v 1.7 2007-02-07 09:56:18 deniger Exp $
 */
public class TrPostInspectorReaderRubar implements TrPostInspectorReader {

  private static class FileState {
    File f_;
    long lastUpdate_;

    public FileState(final long _lastUpdate, final File _f) {
      super();
      lastUpdate_ = _lastUpdate;
      f_ = _f;
    }

    boolean isUpdated() {
      return lastUpdate_ >= f_.lastModified();
    }

    public void updateTs() {
      lastUpdate_ = f_.lastModified();
    }
  }

  boolean blocked_;

  File f_;
  TrPostCommonImplementation impl_;
  boolean isActivating_;
  TrPostRubarLoader loader_;
  TrPostProjet prj_;
  TrPostSource src_;
  ProgressionInterface prog_;

  Map<FileFormat, FileState> readFileTstamp_ = new HashMap<FileFormat, FileState>(10);

  public TrPostInspectorReaderRubar(final File _f) {
    super();
    f_ = _f;
  }

  public TrPostInspectorReaderRubar(final TrPostProjet _prj, final TrPostSource _src) {
    super();
    prj_ = _prj;
    src_ = _src;
    loader_ = new TrPostRubarLoader((TrPostSourceRubar) _src, null, false);
    impl_ = _prj.getImpl();
  }

  private void updateLoadedFmt() {
    // on recupere les formats charges: l'utilisateur peut demander le chargement d'un
    // fichier a tous moments. On ne peut pas enregistrer les formats charg�s dans cette
    // classe
    final String[] fmt = loader_.getSrc().getLoadedFmtId();
    if (fmt != null) {
      // les formats qui seront a recharger
      final Set<FileFormat> fmtToLoad = new HashSet<>(fmt.length);
      final String name = CtuluLibFile.getSansExtension(loader_.getSrc().getMainFile().getName());
      for (int i = fmt.length - 1; i >= 0; i--) {
        final int idx = FileFormat.findFileFormat(loader_.availFmt_, fmt[i]);
        if (idx < 0) {
          continue;
        }
        final FileFormat fmti = loader_.availFmt_[idx];
        FileState state = readFileTstamp_.get(fmti);
        // il s'agit d'un fichier nouvellement charg� par l'utilisateur
        // on met le timestamp a 0 pour forcer la maj au prochain cycle
        if (state == null) {
          final File fi = fmti.getFileExistFor(loader_.dir_, name);
          state = new FileState(0, fi);
          readFileTstamp_.put(fmti, state);
          loader_.fmtLoadable_.put(fmti, fi);
        }
        if (!state.isUpdated()) {
          fmtToLoad.add(fmti);
          // on update par avance car on va remettre � jour
          state.updateTs();
        }
      }
      // tous les formats rassembler sont destiner a etre mis a jour
      loader_.fmtToLoad_ = fmtToLoad.toArray(new FileFormat[fmtToLoad.size()]);
      loader_.fileToLoad_ = new boolean[loader_.fmtToLoad_.length];
      Arrays.fill(loader_.fileToLoad_, true);

    }
  }

  protected boolean isBlocked() {
    return blocked_;
  }

  protected void setBlocked(final boolean _blocked) {
    blocked_ = _blocked;
  }

  @Override
  public void close() {

  }

  public TrPostCommonImplementation getImpl() {
    return null;
  }

  @Override
  public TrPostProjet getPostActivatedProject() {
    return prj_;
  }

  @Override
  public boolean isPostActivated() {
    return prj_ != null;
  }

  @Override
  public boolean isPostActivating() {
    if (isActivating_ && TrPostInspectorReaderSerafin.isVisuDisplayed(prj_)) {
      isActivating_ = false;
    }
    return isActivating_;
  }

  @Override
  public int read() {
    if (blocked_ || f_ == null || !f_.exists()) { return 0; }
    if (loader_ == null) {
      isActivating_ = true;
      // prj_ = TrPostSourceBuilder.activeRubarAction(f_, impl_, prog_);
      if (prj_ != null) {
        loader_ = new TrPostRubarLoader(prj_, src_, f_, false);
        loader_.loadFiles(prog_, impl_);
        impl_.setProjet(prj_);
      }
      return 0;
    }
    final int lastTimeStep = loader_.getSrc().getNbTimeStep();
    updateLoadedFmt();
    loader_.loadFiles(prog_, impl_);
    return loader_.getSrc().getNbTimeStep() - lastTimeStep;
  }

  @Override
  public void setImpl(final TrPostCommonImplementation _impl) {
    impl_ = _impl;
  }

  @Override
  public void setProgression(final ProgressionInterface _prog) {
    prog_ = _prog;
  }

}
