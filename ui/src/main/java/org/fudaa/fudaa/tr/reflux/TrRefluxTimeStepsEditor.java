/**
 * @creation 28 nov. 2003
 * @modification $Date: 2007-06-28 09:28:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.dodico.h2d.H2dTimeStepGroup;
import org.fudaa.dodico.h2d.type.H2dResolutionMethodType;
import org.fudaa.dodico.h2d.type.H2dResolutionSchemaType;
import org.fudaa.dodico.h2d.type.H2dVariableTimeType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author deniger
 * @version $Id: TrRefluxTimeStepsEditor.java,v 1.20 2007-06-28 09:28:19 deniger Exp $
 */
public class TrRefluxTimeStepsEditor extends CtuluDialogPanel implements ItemListener, CaretListener {
    final CtuluListEditorPanel pnPrinc_;
    final JRadioButton cbStationnaire_;
    TimeTableModelStationnaire statModel_;
    TimeTableModelTransitoire transModel_;
    JTextField txtTimeBeginning_;

    /**
     *
     */
    public TrRefluxTimeStepsEditor(final H2dTimeStepGroup[] _t, final double _timeBeginning) {
        final boolean isStationnaire = _t[0].getSchema() == H2dResolutionSchemaType.STATIONNAIRE;
        setLayout(new BuBorderLayout());
        final BuPanel top = new BuPanel();
        top.setLayout(new BuGridLayout(2));
        final ButtonGroup gp = new ButtonGroup();
        cbStationnaire_ = new BuRadioButton(TrResource.getS("Stationnaire"));
        top.add(cbStationnaire_);
        gp.add(cbStationnaire_);
        final JRadioButton cbTrans = new BuRadioButton(TrResource.getS("Transitoire"));
        top.add(cbTrans);
        gp.add(cbTrans);
        txtTimeBeginning_ = addLabelDoubleText(top, TrResource.getS("D�but:"));
        if (isStationnaire) {
            cbStationnaire_.setSelected(true);
        } else {
            cbTrans.setSelected(true);
        }
        cbStationnaire_.addItemListener(this);
        add(top, BuBorderLayout.NORTH);
        if (isStationnaire) {
            statModel_ = new TimeTableModelStationnaire(_t);
            pnPrinc_ = new CtuluListEditorPanel(statModel_, true, true, true, true, false);
            pnPrinc_.setState(false, false, false, true);
            txtTimeBeginning_.setEnabled(false);
        } else {
            txtTimeBeginning_.setText(Double.toString(_timeBeginning));
            transModel_ = new TimeTableModelTransitoire(_t);
            pnPrinc_ = new CtuluListEditorPanel(transModel_, true, true, true, true, false);
        }
        pnPrinc_.setDoubleClickEdit(true);
        txtTimeBeginning_.addCaretListener(this);
        add(pnPrinc_, BuBorderLayout.CENTER);
    }

    protected double getBeginning() {
        final String t = txtTimeBeginning_.getText();
        return t.length() == 0 ? 0 : Double.parseDouble(t);
    }

    protected H2dTimeStepGroup[] getTimeSteps() {
        if (cbStationnaire_.isSelected()) {
            return statModel_.getTimeSteps();
        }
        return transModel_.getTimeSteps();
    }

    private class TimeTableModelStationnaire extends CtuluListEditorModel {
        public TimeTableModelStationnaire(final H2dTimeStepGroup[] _t) {
            super(_t, false);
        }

        @Override
        public int getColumnCount() {
            return 1;
        }

        public H2dTimeStepGroup[] getTimeSteps() {
            return new H2dTimeStepGroup[]{(H2dTimeStepGroup) getValueAt(0)};
        }

        @Override
        public String getColumnName(final int _column) {
            if (_column == 0) {
                return TrResource.getS("M�thode");
            }
            return CtuluLibString.EMPTY_STRING;
        }

        @Override
        public void edit(final int _row) {
            final H2dTimeStepGroup s = (H2dTimeStepGroup) getValueAt(_row);
            if (new TimeStepStationaryEditor(s).afficheModaleOk(TrRefluxTimeStepsEditor.this)) {
                super.fireTableRowsUpdated(
                    _row, _row);
            }
        }

        /*
         * public void edit(final H2dTimeStepGroup _t) { new
         * TimeStepStationaryEditor(_t).afficheModale(TrRefluxTimeStepsEditor.this); }
         */

        @Override
        public Object getValueAt(final int _row, final int _col) {
            if (_col == 0) {
                return ((H2dTimeStepGroup) getValueAt(_row)).getMethode().getName();
            }
            return CtuluLibString.EMPTY_STRING;
        }

        @Override
        public boolean actionAdd() {
            return false;
        }

        @Override
        public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
            return false;
        }
    }

    private class TimeTableModelTransitoire extends CtuluListEditorModel {
        public TimeTableModelTransitoire(final H2dTimeStepGroup[] _t) {
            super(_t, false);
        }

        @Override
        public int getColumnCount() {
            return 5;
        }

        public H2dTimeStepGroup[] getTimeSteps() {
            final H2dTimeStepGroup[] r = new H2dTimeStepGroup[v_.size()];
            v_.toArray(r);
            return r;
        }

        @Override
        public String getColumnName(final int _co) {
            if (_co == 0) {
                return TrResource.getS("Temps d�but");
            } else if (_co == 1) {
                return TrResource.getS("Temps fin");
            } else if (_co == 2) {
                return TrResource.getS("Sch�ma");
            } else if (_co == 3) {
                return TrResource.getS("M�thode");
            } else if (_co == 4) {
                return TrResource.getS("Nombre de pas");
            }
            return CtuluLibString.EMPTY_STRING;
        }

        @Override
        public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
            return false;
        }

        public double getBeginTime(final int _r) {
            double r = getBeginning();
            for (int i = _r - 1; i >= 0; i--) {
                r += ((H2dTimeStepGroup) getValueAt(i)).getTimeLength();
            }
            return r;
        }

        public double getEndTime(final int _r) {
            return getBeginTime(_r) + ((H2dTimeStepGroup) getValueAt(_r)).getTimeLength();
        }

        @Override
        public Object getValueAt(final int _row, final int _col) {
            final H2dTimeStepGroup s = (H2dTimeStepGroup) getValueAt(_row);
            if (_col == 0) {
                return Double.toString(getBeginTime(_row));
            } else if (_col == 1) {
                return Double.toString(getEndTime(_row));
            } else if (_col == 2) {
                return s.getSchema();
            } else if (_col == 3) {
                return s.getMethode();
            } else if (_col == 4) {
                return CtuluLibString.getString(s.getNbPasTemps());
            }
            return CtuluLibString.EMPTY_STRING;
        }

        @Override
        public boolean actionAdd() {
            final Object o = createNewObject();
            if (edit((H2dTimeStepGroup) o)) {
                addElement(o);
                TrRefluxTimeStepsEditor.this.cancelErrorText();
                return true;
            }
            return false;
        }

        @Override
        public Object createNewObject() {
            return H2dTimeStepGroup.createDefaultTransient();
        }

        @Override
        public void edit(final int _row) {
            final H2dTimeStepGroup s = (H2dTimeStepGroup) getValueAt(_row);
            new TimeStepTransientEditor(s, getBeginTime(_row)).afficheModale(TrRefluxTimeStepsEditor.this);
            fireTableRowsUpdated(_row, _row);
        }

        public boolean edit(final H2dTimeStepGroup _t) {
            return new TimeStepTransientEditor(_t, getBeginTime(getIndexOf(_t)))
                .afficheModaleOk(TrRefluxTimeStepsEditor.this);
        }

        @Override
        public boolean actionInserer(final int _r) {
            TrRefluxTimeStepsEditor.this.cancelErrorText();
            final Object o = createNewObject();
            if (edit((H2dTimeStepGroup) o)) {
                add(_r, o);
                return true;
            }
            return false;
        }

        @Override
        public void remove(final ListSelectionModel _m) {
            super.remove(_m);
            if (v_.size() == 0) {
                validate();
            }
        }
    }

    private void changeToStat() {
        if (statModel_ == null) {
            statModel_ = new TimeTableModelStationnaire(
                new H2dTimeStepGroup[]{H2dTimeStepGroup.createDefaultStationnaire()});
        }
        pnPrinc_.setModel(statModel_);
        pnPrinc_.setState(false, false, false);
        maj();
    }

    private void changeToTrans() {
        if (transModel_ == null) {
            transModel_ = new TimeTableModelTransitoire(new H2dTimeStepGroup[]{H2dTimeStepGroup.createDefaultTransient()});
        }
        pnPrinc_.setModel(transModel_);
        pnPrinc_.setState(true, true, true);
        maj();
    }

    private void maj() {
        pnPrinc_.revalidate();
        revalidate();
        repaint();
    }

    @Override
    public void itemStateChanged(final ItemEvent _evt) {
        if (_evt.getStateChange() == ItemEvent.SELECTED) {
            changeToStat();
            txtTimeBeginning_.setEnabled(false);
        } else {
            changeToTrans();
            txtTimeBeginning_.setEnabled(true);
        }
    }

    /**
     * @author Fred Deniger
     * @version $Id: TrRefluxTimeStepsEditor.java,v 1.20 2007-06-28 09:28:19 deniger Exp $
     */
    public static class TimeStepStationaryEditor extends CtuluDialogPanel {
        // H2dTimeStepGroup step_;
        MethodPanel pnMeth_;

        public TimeStepStationaryEditor(final H2dTimeStepGroup _step) {
            // step_ = _step;
            pnMeth_ = new MethodPanel(_step);
            addEmptyBorder(5);
            setLayout(new BuBorderLayout(5, 5));
            add(pnMeth_, BuBorderLayout.CENTER);
        }

        /**
         *
         */
        @Override
        public boolean apply() {
            pnMeth_.apply();
            return true;
        }
    }

    /**
     * @author Fred Deniger
     * @version $Id: TrRefluxTimeStepsEditor.java,v 1.20 2007-06-28 09:28:19 deniger Exp $
     */
    public static class TimeStepTransientEditor extends CtuluDialogPanel implements CaretListener, ItemListener {
        H2dTimeStepGroup step_;
        JTextField tfNbTimeStep_;
        JTextField tfTimeStepValue_;
        JTextField tfPrintFreq_;
        JTextField tfSchemaCoef_;
        JComboBox cbSchema_;
        double debut_;
        JTextField tfEndTimeStep_;
        MethodPanel pnMeth_;

        public TimeStepTransientEditor(final H2dTimeStepGroup _g, final double _debut) {
            step_ = _g;
            setLayout(new BuVerticalLayout(10));
            addEmptyBorder(5);
            final BuPanel beginEnd = new BuPanel();
            beginEnd.setLayout(new BuGridLayout(4, 5, 10));
            addLabel(beginEnd, TrResource.getS("D�but"));
            debut_ = _debut;
            addDoubleText(beginEnd, debut_).setEditable(false);
            addLabel(beginEnd, TrResource.getS("Fin"));
            tfEndTimeStep_ = addDoubleText(beginEnd, debut_ + step_.getTimeLength());
            tfEndTimeStep_.setEditable(false);
            add(beginEnd);
            final BuPanel timeValue = new BuPanel();
            timeValue.setLayout(new BuGridLayout(2, 10, 10));
            tfNbTimeStep_ = addLabelIntegerText(timeValue, H2dVariableTimeType.NB_TIME_STEP_SCHEMA.getName());
            tfNbTimeStep_.setText(CtuluLibString.getString(step_.getNbPasTemps()));
            tfNbTimeStep_.addCaretListener(this);
            tfTimeStepValue_ = addLabelDoubleText(timeValue, H2dVariableTimeType.VALUE_TIME_STEP_SCHEMA.getName() + "(sec)");
            tfTimeStepValue_.setText(Double.toString(step_.getValeurPasTemps()));
            tfTimeStepValue_.addCaretListener(this);
            tfPrintFreq_ = addLabelIntegerText(timeValue, H2dVariableTimeType.FREQUENCE_PRINT_SCHEMA.getName());
            tfPrintFreq_.setText(CtuluLibString.getString(step_.getFrequenceImpression()));
            addCommonBorder(timeValue);
            add(timeValue);
            final BuPanel schemaPn = new BuPanel();
            schemaPn.setLayout(new BuGridLayout(2, 10, 10));
            addLabel(schemaPn, TrResource.getS("Sch�ma"));
            cbSchema_ = new BuComboBox();
            cbSchema_.setModel(new DefaultComboBoxModel(H2dResolutionSchemaType.getRefluxTranSchema()));
            cbSchema_.setSelectedItem(step_.getSchema());
            schemaPn.add(cbSchema_);
            tfSchemaCoef_ = addLabelDoubleText(schemaPn, H2dVariableTimeType.COEF_SCHEMA.getName());
            tfSchemaCoef_.setText(Double.toString(step_.getCoefSchema()));
            addCommonBorder(schemaPn);
            add(schemaPn);
            pnMeth_ = new MethodPanel(step_);
            add(pnMeth_);
            cbSchema_.addItemListener(this);
        }

        @Override
        public void caretUpdate(final CaretEvent _evt) {
            if ((tfNbTimeStep_.getText().length() == 0) || (tfTimeStepValue_.getText().length() == 0)) {
                return;
            }
            tfEndTimeStep_.setText(Double.toString(debut_ + Integer.parseInt(tfNbTimeStep_.getText())
                * Double.parseDouble(tfTimeStepValue_.getText())));
        }

        @Override
        public void itemStateChanged(final ItemEvent _evt) {
            if (_evt.getStateChange() != ItemEvent.SELECTED) {
                return;
            }
            if (_evt.getSource() == cbSchema_) {
                pnMeth_.setSchema((H2dResolutionSchemaType) cbSchema_.getSelectedItem());

                tfSchemaCoef_.setText(Double
                    .toString(cbSchema_.getSelectedItem() == H2dResolutionSchemaType.KAWAHARA ? H2dTimeStepGroup
                        .getKawarahDefaultCoefSchema() : H2dTimeStepGroup.getEulerDefaultCoefSchema()));
            }
        }

        @Override
        public boolean apply() {
            pnMeth_.apply();
            step_.setSchema((H2dResolutionSchemaType) cbSchema_.getSelectedItem());
            step_.setValeurPasTemps(Double.parseDouble(tfTimeStepValue_.getText()));
            step_.setNbPasTemps(Integer.parseInt(tfNbTimeStep_.getText()));
            step_.setCoefSchema(Double.parseDouble(tfSchemaCoef_.getText()));
            step_.setFrequenceImpression(Integer.parseInt(tfPrintFreq_.getText()));
            return true;
        }
    }

    @Override
    public boolean isDataValid() {
        if (pnPrinc_.getTableModel().getRowCount() == 0) {
            setErrorText(TrResource.getS("Au moins un groupe de pas de temps doit �tre d�fini"));
            return false;
        }
        return true;
    }

    public static void addCommonBorder(final JPanel _pn) {
        _pn.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BorderFactory
            .createEmptyBorder(5, 5, 5, 5)));
    }

    protected static class MethodPanel extends BuPanel implements ItemListener {
        H2dTimeStepGroup step_;
        JComboBox cbMethod_;
        Map varTxtField_;
        JPanel pnMethValues_;

        public MethodPanel(final H2dTimeStepGroup _step) {
            step_ = _step;
            setLayout(new BuBorderLayout());
            final BuPanel methType = new BuPanel();
            methType.setLayout(new BuHorizontalLayout(10, true, false));
            final BuLabel lb = new BuLabel(TrResource.getS("M�thode"));
            lb.setVerticalAlignment(SwingConstants.CENTER);
            methType.add(lb);
            cbMethod_ = new BuComboBox();
            cbMethod_.setModel(step_.getSchema().getMethodList().createComboBoxModel());
            final H2dResolutionMethodType m = step_.getMethode();
            cbMethod_.setSelectedItem(m);
            methType.add(cbMethod_);
            add(methType, BuBorderLayout.NORTH);
            pnMethValues_ = new BuPanel();
            pnMethValues_.setLayout(new BuGridLayout(2, 10, 10));
            final CtuluPermanentList l = m.getVarList();
            varTxtField_ = new TreeMap();
            if (l != null) {
                final int n = l.size();
                for (int i = 0; i < n; i++) {
                    final H2dVariableType v = (H2dVariableType) l.get(i);
                    pnMethValues_.add(new BuLabel(v.getName()));
                    final JTextField txt = BuTextField.createDoubleField();
                    pnMethValues_.add(txt);
                    txt.setText(Double.toString(step_.getMethodeValue(v)));
                    varTxtField_.put(v, txt);
                }
            }
            add(pnMethValues_, BuBorderLayout.CENTER);
            addCommonBorder(this);
            doLayout();
            final Dimension lineSize = cbMethod_.getPreferredSize();
            final Dimension pnDim = pnMethValues_.getPreferredSize();
            final int h = lineSize.height * 8;
            if (pnDim.height < h) {
                pnDim.height = h;
            }
            pnMethValues_.setPreferredSize(pnDim);
            cbMethod_.addItemListener(this);
        }

        protected void setSchema(final H2dResolutionSchemaType _t) {
            final CtuluPermanentList l = _t.getMethodList();
            cbMethod_.setModel(l.createComboBoxModel());
            cbMethod_.setSelectedItem(l.get(0));
        }

        protected void apply() {
            step_.setMethode((H2dResolutionMethodType) cbMethod_.getSelectedItem());
            for (final Iterator it = varTxtField_.entrySet().iterator(); it.hasNext(); ) {
                final Map.Entry e = (Map.Entry) it.next();
                step_.setMethodeValue((H2dVariableType) e.getKey(), Double.parseDouble(((JTextField) e.getValue()).getText()));
            }
        }

        private void changeMethod() {
            final CtuluPermanentList l = ((H2dResolutionMethodType) cbMethod_.getSelectedItem()).getVarList();
            pnMethValues_.removeAll();
            pnMethValues_.invalidate();
            if (l == null) {
                varTxtField_.clear();
            } else {
                varTxtField_.keySet().retainAll(l);
                for (int i = l.size() - 1; i >= 0; i--) {
                    final H2dVariableType v = (H2dVariableType) l.get(i);
                    if (!varTxtField_.containsKey(v)) {
                        final JTextField t = BuTextField.createDoubleField();
                        t.setText(Double.toString(H2dTimeStepGroup.getDefaultValue(v)));
                        varTxtField_.put(v, t);
                    }
                }
                for (final Iterator it = varTxtField_.entrySet().iterator(); it.hasNext(); ) {
                    final Map.Entry entry = (Map.Entry) it.next();
                    pnMethValues_.add(new BuLabel(((H2dVariableType) entry.getKey()).getName()));
                    pnMethValues_.add((JTextField) entry.getValue());
                }
            }
            pnMethValues_.revalidate();
            pnMethValues_.repaint();
        }

        @Override
        public void itemStateChanged(final ItemEvent _evt) {
            if (_evt.getStateChange() == ItemEvent.SELECTED) {
                changeMethod();
            }
        }
    }

    @Override
    public void caretUpdate(final CaretEvent _evt) {
        if ((statModel_ != null) && (cbStationnaire_.isSelected())) {
            statModel_.fireTableDataChanged();
        } else if (transModel_ != null) {
            transModel_.fireTableDataChanged();
        }
    }
}
