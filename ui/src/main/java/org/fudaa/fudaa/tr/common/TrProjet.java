/**
 *  @creation     29 avr. 2003
 *  @modification $Date: 2007-06-11 13:08:20 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuMenu;
import java.io.File;
import java.util.Properties;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.fudaa.commun.calcul.FudaaCalculSupportInterface;
import org.fudaa.fudaa.fdico.FDicoProjetInterface;
import org.fudaa.fudaa.sig.FSigProjet;

/**
 * @author deniger
 * @version $Id: TrProjet.java,v 1.22 2007-06-11 13:08:20 deniger Exp $
 */
public interface TrProjet extends FudaaCalculSupportInterface, FDicoProjetInterface, FSigProjet {

  void initInformationsDocument(BuInformationsDocument _info);

  void finishCreation(ProgressionInterface _prog, Properties _options);

  @Override
  boolean save(ProgressionInterface _prog);

  boolean saveAs(ProgressionInterface _prog);

  /**
   * @param _prog la barre de progression
   * @param _f le fichier de dest: peut-etre null
   * @return le fichier de sauvegarde
   */
  File saveCopy(ProgressionInterface _prog, File _f);

  @Override
  void close();

  String getSoftwareID();

  boolean isAlreadyUsed(File _f);

  TrCourbeUseFinder getUsedCourbeFinder(EGGrapheModel _model);

  void active();

  @Override
  String getTitle();

  @Override
  File getParamsFile();

  void exportMaillage();

  void showGeneralFille();

  void showVisuFille();

  BuMenu getProjectMenu();

  TrFilleVisuInterface getVisuFille();

  TrParametres getTrParams();

  boolean loadAll(ProgressionInterface _inter);

  void showGrapheFille();

  void importCourbesTemporelles(EvolutionReguliereInterface[] _r);

}
