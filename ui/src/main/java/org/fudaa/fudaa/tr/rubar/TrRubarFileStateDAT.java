/**
 * @creation 13 oct. 2004
 * @modification $Date: 2007-03-02 13:01:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarBcListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryFlowrateGroupType;
import org.fudaa.dodico.rubar.io.RubarDATFileFormat;
import org.fudaa.dodico.rubar.io.RubarDATWriter;

import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFileStateDAT.java,v 1.10 2007-03-02 13:01:34 deniger Exp $
 */
public class TrRubarFileStateDAT extends TrRubarFileState implements H2dRubarBcListener {
  @Override
  public void bathyChanged() {
    setModified(true);
  }

  @Override
  public void fondDurChanged() {
  }

  @Override
  public void nodeInGridChanged() {
    setModified(true);
  }

  @Override
  public void areteTypeChanged() {
    setModified(true);
  }

  @Override
  public void flowrateGroupChanged(final H2dRubarBoundaryFlowrateGroupType _t) {
  }

  @Override
  public void timeClChanged() {
  }

  @Override
  public void projectTypeChanged() {
  }

  @Override
  public void numberOfConcentrationChanged() {

  }

  @Override
  public void fileFormatDigitsChanged() {
    setModified(true);
  }

  /**
   * @param _l le listener
   */
  public TrRubarFileStateDAT(final TrRubarFileStateListener _l, final H2dRubarParameters _p) {
    super(RubarDATFileFormat.getInstance(), _l);
    _p.getBcMng().addListener(this);
  }

  @Override
  public CtuluIOOperationSynthese save(final File _dir, final String _projectName, final ProgressionInterface _progression,
                                       final TrRubarProject _projet) {
    final File f = RubarDATFileFormat.getInstance().getFileFor(_dir, _projectName);
    final RubarDATWriter writer = (RubarDATWriter) RubarDATFileFormat.getInstance().createWriter();
    writer.setNewFormat(_projet.isNewFormatForNumberOfDigits());
    return writer.write(_projet.getH2dRubarParameters().getBcMng().getSrcAdapter(), f, _progression);
  }
}
