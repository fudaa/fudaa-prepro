/*
 * @creation 20 sept. 06
 * @modification $Date: 2007-05-04 14:01:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.data;

import java.util.Set;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.TrPostFlecheContent;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.nfunk.jep.Variable;

/**
 * @author fred deniger
 * @version $Id: TrPostDataCreatedConstant.java,v 1.4 2007-05-04 14:01:52 deniger Exp $
 */
public class TrPostDataCreatedConstant implements TrPostDataCreated {

  @Override
  public boolean updateConstantVar(final Variable _var) {
    return false;
  }

  EfData data_;
  H2dVariableType var_;
  final TrPostSource src_;

  public TrPostDataCreatedConstant(final TrPostSource _src, final EfData _data, final H2dVariableType _var) {
    super();
    data_ = _data;
    src_ = _src;
    var_ = _var;
  }
  
  
  @Override
  public void restore() {
  }
  

  @Override
  public TrPostDataCreatedSaver createSaver() {
    return null;
  }

  @Override
  public boolean isEditable() {
    return false;
  }

  @Override
  public void clearCache() {}

  @Override
  public final void fillWhithAllUsedVar(final Set _res) {
    if (_res != null && !_res.contains(var_)) {
      _res.add(var_);

      TrPostDataHelper.addDependingVarOf(var_, src_, _res);
    }

  }

  @Override
  public EfData getDataFor(final int _idxTime) {
    return data_;
  }

  @Override
  public String getDescription() {
    return var_.getName();
  }

  @Override
  public double getValue(final int _idxTime, final int _idxObject) {
    return data_.getValue(_idxObject);
  }

  @Override
  public TrPostFlecheContent isFleche() {
    return null;
  }

  @Override
  public boolean isExpr() {
    return false;
  }

}
