/*
 * @creation 6 sept. 06
 *
 * @modification $Date: 2007-06-11 13:08:21 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuInformationsDocument;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.ZEbliFilleCalques;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.save.FudaaSaveLib;
import org.fudaa.fudaa.commun.save.FudaaSaveProject;
import org.fudaa.fudaa.commun.save.FudaaSaveZipLoader;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.sig.FSigProjectPersistence;
import org.fudaa.fudaa.sig.FSigProjet;
import org.fudaa.fudaa.tr.TrEditorImplementation;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @author fred deniger
 * @version $Id: TrProjectPersistence.java,v 1.6 2007-06-11 13:08:21 deniger Exp $
 */
public final class TrProjectPersistence {
  private TrProjectPersistence() {
  }

  public static boolean saveProject(final FudaaCommonImplementation _impl, final FSigProjet _proj,
                                    final File _destFile, final ProgressionInterface _prog) {
    if (_proj == null) {
      return false;
    }
    final boolean r = FSigProjectPersistence.saveProject(_impl, _proj, TrLib.getPreSaveFile(_destFile), _prog);
    if (r) {
      // ancien format a effacer
      // old format to erase
      final File db = FSigProjectPersistence.getPreSaveFileOld(_destFile);
      if (db.exists()) {
        try {
          Files.delete(db.toPath());
        } catch (IOException e) {
          FuLog.error(e);
        }
      }
    }
    return r;
  }

  public static boolean loadProject(final TrEditorImplementation _impl, final File _destFile) {
    if (_destFile == null) {
      return false;
    }
    final TrProjet prj = _impl.currentProject_;
    if (prj == null) {
      return false;
    }
    final File prefFile = TrLib.getPreSaveFile(_destFile);
    BuInformationsDocument doc = null;
    if (prefFile != null && prefFile.exists()) {
      FudaaSaveZipLoader loader = null;
      try {
        loader = new FudaaSaveZipLoader(prefFile);
        doc = FudaaSaveProject.loadInfo(loader);
      } catch (final IOException _evt) {
        FuLog.error(_evt);
      } finally {
        if (loader != null) {
          try {
            loader.close();
          } catch (final IOException _evt) {
            FuLog.error(_evt);
          }
        }
      }
    } else {
      final File oldFile = FSigProjectPersistence.getPreSaveFileOld(_destFile);
      if (oldFile != null) {
        FudaaSaveLib.configureDb4o();
        try {
          doc = (BuInformationsDocument) FudaaSaveLib.getUniqueDataInDb(oldFile, BuInformationsDocument.class);
        } catch (final Throwable _evt) {
          FuLog.error(_evt);
        }
      }
    }

    if (doc != null) {
      prj.initInformationsDocument(doc);
      // le reste ce fait automatiquement par les objets sauv�s.
      // the rest is automatically made by the saved objects
    }

    return true;
  }

  public static String getPreProjectExt() {
    return ".pre.fzip";
  }

  public static String getPostProjectExt() {
    return ".post.fzip";
  }

  public static FudaaSaveZipLoader importForFrame(final JInternalFrame _currentFrame,
                                                  final FudaaCommonImplementation _impl) {
    return importForFrame(((ZEbliFilleCalques) _currentFrame).getVisuPanel(), _impl);
  }

  public static FudaaSaveZipLoader importForFrame(final ZEbliCalquesPanel _calquePanel,
                                                  final FudaaCommonImplementation _impl) {
    final TrProjectImportPanel pn = new TrProjectImportPanel();
    final String s = FSigLib.getS("Importer un projet");
    if (pn.afficheModaleOk(_calquePanel.getCtuluUI().getParentComponent(), s)) {
      FudaaSaveZipLoader loader = null;
      try {
        loader = pn.getLoader();
        final FudaaSaveZipLoader finalLoader = loader;
        final CtuluTaskDelegate createTask = _impl.createTask(s);
        createTask.start(new Runnable() {
          @Override
          public void run() {
            FudaaSaveLib.restoreAndLaunch(_impl, _calquePanel, createTask.getStateReceiver(), finalLoader);
          }
        });
      } catch (final IOException _evt) {
        FuLog.error(_evt);
      }
      return loader;
    }
    return null;
  }
}
