/*
 * @creation 9 mars 2004
 * @modification $Date: 2007-06-28 09:28:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuResource;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFilter;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.telemac.TelemacDicoFileFormat;
import org.fudaa.dodico.telemac.TelemacDicoManager;
import org.fudaa.dodico.telemac.TelemacVersionManager;
import org.fudaa.fudaa.commun.FudaaUI;
import org.fudaa.fudaa.commun.calcul.FudaaCalculOp;
import org.fudaa.fudaa.commun.exec.FudaaExec;
import org.fudaa.fudaa.fdico.FDicoCalculLocalBuilder;
import org.fudaa.fudaa.tr.common.*;
import org.fudaa.fudaa.tr.post.TrPostInspector;
import org.fudaa.fudaa.tr.post.TrPostInspectorReaderSerafin;

import javax.swing.*;
import java.io.File;
import java.io.FileFilter;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacAppliManager.java,v 1.36 2007-06-28 09:28:19 deniger Exp $
 */
public final class TrTelemacAppliManager extends TrApplicationManager {
  @Override
  public String getSoftId() {
    return TrTelemacImplHelper.getID();
  }

  private class FudaaExecLaunchTelemac extends FudaaExec {
    /**
     * Constructeur par defaut.
     * Default constructor
     */
    public FudaaExecLaunchTelemac() {
      super(TrResource.getS("Ex�cuter application T�l�mac"));
    }

    @Override
    public void execInDir(final File _dir, final FudaaUI _ui, final Runnable _nexProcess) {
    }

    @Override
    public void execOnFile(final File _target, final FudaaUI _ui, final Runnable _nexProcess) {
      launchCalcul(_target, _ui);
    }

    @Override
    public Icon getIcon() {
      return BuResource.BU.getIcon("executer");
    }
  }

  protected BuMenuItem createH2dProject_;
  protected TrExplorer.ExplorerMenuItem execTelemac_;

  /**
   * @param _l le lanceur d'appli / lhe application launcher
   */
  public TrTelemacAppliManager(final TrLauncher _l) {
    super(_l);
    getBathygeoAnalyzeExec();
  }

  @Override
  protected void buildCmdForMenuFileOpenWith(final BuMenu _m, final TrExplorer _explor) {
    super.buildCmdForMenuFileOpenWith(_m, _explor);
    _m.addSeparator();
    _m.add(_explor.createFileAction(new FudaaExecCreateProjectH2d(TrResource.getS("Cr�er projet Telemac"), launcher_)));
    _m.add(_explor.createFileAction(new TrApplicationManager.FudaaExecGridManagement(TrFileFormatManager.getAllGridFormat())));
  }

  @Override
  protected synchronized void buildMainMenuFirstItems(final BuMenu _m) {

  }

  @Override
  public boolean buildCmdForMenuDir(final JPopupMenu _m, final TrExplorer _explorer) {
    _m.add(super.buildDirMenuItem(_explorer));
    return true;
  }

  @Override
  public FudaaExec[] getToolBarExecs() {
    return new FudaaExec[0];
  }

  @Override
  public void updateMenuFiles(final int _nbFileChoosen, final File _f) {
    super.updateMenuFiles(_nbFileChoosen, _f);
    createH2dProject_.setEnabled((_nbFileChoosen == 1) && (isSerafinFile(_f)));
    execTelemac_.setEnabled((_nbFileChoosen == 1) && (launcher_.getFileFormatManager().isCasFile(_f.getName())));
  }

  protected boolean isSerafinFile(final File _f) {
    return getFileFilterFor(SerafinFileFormat.getInstance()).accept(_f);
  }

  @Override
  public FudaaExec[] getActionForFile(final File _f) {
    final SerafinFileFilter ft = (SerafinFileFilter) getFileFilterFor(SerafinFileFormat.getInstance());
    if (ft.accept(_f)) {
      if (ft.isResFile(_f)) {
        return new FudaaExec[]{meshExec_};
      }
      return new FudaaExec[]{postExec_};
    } else if (getFileFilterFor(TelemacDicoFileFormat.getEmptyFormat()).accept(_f)) {
      return new FudaaExec[]{execTelemac_
          .getExec()};
    }
    return null;
  }

  @Override
  public void buildCmdForMenuFile(final JPopupMenu _m, final TrExplorer _explorer) {
    createH2dProject_ = _explorer.createFileAction(new FudaaExecCreateProjectH2d(TrResource
        .getS("Cr�er projet Telemac"), launcher_));
    _m.add(createH2dProject_);
    execTelemac_ = _explorer.createFileAction(new FudaaExecLaunchTelemac());
    _m.add(execTelemac_);
    postItem_ = _explorer.createFileAction(new FudaaExecPostView(launcher_));
    _m.add(postItem_);
    super.buildCmdForMenuFile(_m, _explorer);
  }

  /**
   * @return l'id du software / the software id
   */
  public String getSoftwareID() {
    return FileFormatSoftware.TELEMAC_IS.name;
  }

  @Override
  public void prefChanged() {
    super.prefChanged();
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug(getClass() + "Pref changed");
    }
  }

  /**
   * @param _f le fichier cas / _f the project file
   * @return les repertoires temporaires cr��es pour l'execution d'un calcul / the temporary folders created for the execution of a computation
   */
  public static File[] getTmpDirForSteeringFile(final File _f) {
    final File dir = _f.getParentFile();
    if (dir == null) {
      return null;
    }
    final String deb = _f.getName();

    final FileFilter filter = new FileFilter() {
      @Override
      public boolean accept(final File _pathname) {
        final String name = _pathname.getName();
        return name.startsWith(deb) && name.endsWith("_tmp");
      }
    };
    return dir.listFiles(filter);
  }

  protected void launchCalcul(final File _f, final FudaaUI _parent) {
    final FDicoCalculLocalBuilder build = new FDicoCalculLocalBuilder(TelemacDicoManager.getINSTANCE(),
        TelemacVersionManager.INSTANCE, _f, true);
    // on regarde si le fichier n'a pas d�j� �t� ouvert
    // we look if the file has not already been opened
    final FileFormatSoftware ft = TrLib.findPreMainVersion(_f);
    if (ft != null) {
      build.setSelected(ft);
    }
    final int i = build.afficheModale(_parent.getParentComponent());
    if (CtuluDialogPanel.isOkResponse(i)) {
      final boolean follow = build.isFollow();

      final FudaaCalculOp f = new FudaaCalculOp(build.getTelemacExec(build.isParallel())) {
        @Override
        public File proceedParamFile(final ProgressionInterface _inter) {
          return _f;
        }
      };
      f.setUi(_parent);
      if (follow) {
        new TrPostInspector(new TrPostInspectorReaderSerafin(_f, getTmpDirForSteeringFile(_f)), build.getTelemacExec(build.isParallel())
            .getExecName()
            + CtuluLibString.ESPACE + _f.getName(), launcher_).start();
      }
      f.execute();
    }
  }
}
