/**
 * @creation 10 juin 2003
 * @modification $Date: 2007-06-29 15:09:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.io.corelebth.CorEleBthFileFormat;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.reflux.H2dRefluxDicoVersion;
import org.fudaa.dodico.h2d.type.H2dProjetType;
import org.fudaa.dodico.reflux.io.INPFileFormat;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.tr.common.*;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import java.io.File;
import java.util.Map;
import java.util.Properties;

/**
 * @author deniger
 * @version $Id: TrRefluxImplHelper.java,v 1.40 2007-06-29 15:09:41 deniger Exp $
 */
public class TrRefluxImplHelper extends TrImplHelperAbstract {
  private final static class OpenPanel extends CtuluDialogPanel implements CaretListener {
    private final JComboBox cbFormat_;
    private final JCheckBox cbOlb_;
    private final JComboBox cbProjectType_;
    private final JTextField file_;
    private final FileFormat[] fts_;
    private final Runnable gridRunnable_;
    private final TrApplicationManager mng_;

    public OpenPanel(final TrApplicationManager _mng, final File _f) {
      addEmptyBorder(5);
      mng_ = _mng;
      setLayout(new BuGridLayout(2, 5, 5));
      addLabel(TrResource.getS("Type:"));
      cbProjectType_ = new BuComboBox(new H2dProjetType[]{H2dProjetType.COURANTOLOGIE_2D,
          H2dProjetType.COURANTOLOGIE_2D_LMG});
      cbProjectType_.setSelectedIndex(0);
      add(cbProjectType_);
      // addLabel(TrResource.getS("Fichier contenant le maillage"));
      file_ = addLabelFileChooserPanel(TrResource.getS("Fichier contenant le maillage"), null, false, false);
      file_.addCaretListener(this);
      gridRunnable_ = CtuluDialog.createRunnableToScroll(file_);
      if (_f != null) {
        file_.setText(_f.getAbsolutePath());
      }
      fts_ = TrFileFormatManager.getGridFormat(getID());
      cbFormat_ = new BuComboBox(fts_);
      FileFormat f = null;
      if (_f != null) {
        f = mng_.findGridFileFormat(_f);
      }

      if (f != null) {
        cbFormat_.setSelectedItem(f);
      }
      addLabel(TrResource.getS("Format du fichier"));
      add(cbFormat_);
      addLabel(TrResource.getS("Optimiser avec OLB"));
      cbOlb_ = new BuCheckBox();
      if (f == CorEleBthFileFormat.getInstance()) {
        cbOlb_.setSelected(false);
      } else {
        cbOlb_.setSelected(TrPreferences.TR.getBooleanProperty("reflux.olb.use", true));
      }
      add(cbOlb_);
    }

    protected Runnable getGridRunnable() {
      return gridRunnable_;
    }

    @Override
    public void caretUpdate(final CaretEvent _e) {
      String s = file_.getText();
      if (s == null) {
        return;
      }
      final int ptIdx = s.lastIndexOf('.');
      if ((ptIdx <= 0) || ptIdx >= s.length() - 1) {
        return;
      }
      s = s.substring(ptIdx + 1);
      for (int i = fts_.length - 1; i >= 0; i--) {
        if (fts_[i].isExtension(s)) {
          cbFormat_.setSelectedItem(fts_[i]);
        }
      }
    }

    /**
     *
     */
    @Override
    public JFileChooser createFileChooser() {
      return new CtuluFileChooser(true);
    }

    public FileFormat getFileFormat() {
      return (FileFormat) cbFormat_.getSelectedItem();
    }

    public H2dProjetType getProjectType() {
      return (H2dProjetType) cbProjectType_.getSelectedItem();
    }

    public File getSelectedFile() {
      return new File(file_.getText());
    }

    /**
     * @return true si l'utilisateur veut utiliser olb.
     */
    public boolean isOlbSelected() {
      return cbOlb_.isSelected();
    }

    @Override
    public boolean isDataValid() {
      if (getFileFormat() != CorEleBthFileFormat.getInstance()) {
        TrPreferences.TR.putBooleanProperty("reflux.olb.use", isOlbSelected());
      }
      final File f = getSelectedFile();
      if (!f.exists()) {
        setErrorText(CtuluLib.getS("Le fichier {0} n'existe pas", f.getAbsolutePath()));
        return false;
      } else if (mng_.isEditorAlreadyOpen(f)) {
        setErrorText(TrLib.getMessageAlreadyOpen(f));
        return false;
      }
      return true;
    }
  }

  /**
   * @return l'identifiant de reflux
   */
  public static String getID() {
    return FileFormatSoftware.REFLUX_IS.name;
  }

  public TrRefluxImplHelper(final TrLauncher _r) {
    super(new TrRefluxAppliManager(_r));
  }

  private TrProjet internalOuvrir(final TrImplementationEditorAbstract _impl, final ProgressionInterface _op,
                                  final File _fic) {
    if (_fic != null) {
      _op.setDesc(TrResource.getS("import fichier inp"));
      final BuMainPanel mp = _impl.getMainPanel();
      mp.setMessage(TrResource.getS("Ouverture projet"));
      final TrRefluxProjet projet = TrRefluxProjectFactory.createRefluxProjectFromINP(_fic,
          (H2dRefluxDicoVersion) INPFileFormat.getInstance().getLastVersionInstance(_fic), _op, _impl);
      mp.setProgression(60);
      _op.setDesc(CtuluLibString.EMPTY_STRING);
      _op.setProgression(0);
      mp.setMessage(TrResource.getS("Affichage") + " ...");
      mp.setMessage(CtuluLibString.EMPTY_STRING);
      mp.setProgression(0);
      return projet;
    }
    return null;
  }

  @Override
  public TrProjet creer(final TrImplementationEditorAbstract _impl, final ProgressionInterface _op, final File _f,
                        final Properties _options) {
    final OpenPanel openPn = new OpenPanel(getAppliMng(), _f);
    if (CtuluDialogPanel.isOkResponse(openPn.afficheModale(_impl.getFrame(), TrResource.getS("Nouveau projet Reflux"),
        openPn.getGridRunnable()))) {
      _impl.setMainMessage(TrResource.getS("Cr�ation projet"));
      final File f = openPn.getSelectedFile();
      final FileFormat format = openPn.getFileFormat();
      if (format == null) {
        return null;
      }
      final FileFormatGridVersion v = (FileFormatGridVersion) format.getLastVersionInstance(f);
      final H2dProjetType t = openPn.getProjectType();
      if (t == null) {
        return null;
      }
      return TrRefluxProjectFactory.createRefluxProjectFromGrid(f, v, t, _op, _impl, openPn.isOlbSelected());
    }
    return null;
  }

  @Override
  public BuMenuItem[] getMenuItemsOuvrir(final TrImplementationEditorAbstract _impl) {
    final BuMenuItem r0 = new BuMenuItem(FudaaResource.FUDAA.getToolIcon("appli/telemac"), TrResource
        .getS("Projet T�l�mac"));
    r0.setActionCommand(TrImplementationEditorAbstract.PREF_OUVRIR + FileFormatSoftware.TELEMAC_IS.name);
    final BuMenuItem r = new BuMenuItem(null, TrResource.getS("Projet Rubar"));
    r.setActionCommand(TrImplementationEditorAbstract.PREF_OUVRIR + FileFormatSoftware.RUBAR_IS.name);
    return new BuMenuItem[]{r0, r};
  }

  /**
   * @return l'identifiant reflux
   */
  @Override
  public String getSoftwareID() {
    return FileFormatSoftware.REFLUX_IS.name;
  }

  @Override
  public TrProjet ouvrir(final TrImplementationEditorAbstract _impl, final ProgressionInterface _op) {
    final File fic = _impl.ouvrirFileChooser(TrResource.getS("fichier inp de Reflux"), null);
    if (fic != null) {
      return internalOuvrir(_impl, _op, fic);
    }
    return null;
  }

  @Override
  public TrProjet ouvrir(final TrImplementationEditorAbstract _impl, final ProgressionInterface _op, final File _f,
                         final Map _params) {
    if (_f == null) {
      return ouvrir(_impl, _op);
    }
    return internalOuvrir(_impl, _op, _f);
  }
}
