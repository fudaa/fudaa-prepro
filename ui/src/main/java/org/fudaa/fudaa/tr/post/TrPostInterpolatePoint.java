/*
 *  @creation     18 ao�t 2005
 *  @modification $Date: 2007-06-11 13:08:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import java.io.IOException;
import org.fudaa.ctulu.gis.GISPrecision;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;

public class TrPostInterpolatePoint {

  public final int idxElt_;
  final double x_;
  final double y_;
  private final GISPrecision precision_;

  /**
   * @param _elt l'indice de l'element contenant le point x,y
   * @param _x le x du point a interpoler
   * @param _y le y du point a interpoler
   * @param _prec la precision sur les coordonn�es.
   */
  public TrPostInterpolatePoint(final int _elt, final double _x, final double _y, final GISPrecision _prec) {
    super();
    idxElt_ = _elt;
    x_ = _x;
    y_ = _y;
    precision_ = _prec;
  }
  
  
  public TrPostInterpolatePoint(TrPostInterpolatePoint duplique) {
	    super();
	    idxElt_ = duplique.idxElt_;
	    x_ = duplique.x_;
	    y_ = duplique.y_;
	    precision_ = duplique.precision_;
	  }

  protected double getInterpolateValue(final H2dVariableType _var, final int _tIdx, final TrPostSource _src) {
    try {
      if (_src.isElementVar(_var)) return _src.getData(_var, _tIdx, idxElt_);
      return _src.getInterpolator().interpolate(idxElt_, x_, y_, _var, _tIdx);
    } catch (final IOException _evt) {
      FuLog.error(_evt);

    }
    return 0;

  }

  protected double getInterpolateValue(final H2dVariableType _var, final EfData _value, final int _tIdx,
      final TrPostSource _src) {
    if (_var == null) return 0;
    try {
      if (_value.isElementData()) return _value.getValue(idxElt_);
      return _src.getInterpolator().interpolateFromValue(idxElt_, x_, y_, _var, _value, _tIdx);
    } catch (final IOException _evt) {
      FuLog.error(_evt);

    }
    return 0;

  }

  @Override
  public boolean equals(final Object _obj) {
    if (_obj == this) { return true; }
    if (_obj == null) { return false; }
    if (_obj.getClass().equals(getClass())) {
      final TrPostInterpolatePoint d = (TrPostInterpolatePoint) _obj;
      return precision_.isSame(x_, y_, d.x_, d.y_);

    }
    return false;
  }

  @Override
  public int hashCode() {
    // Algorithm from Effective Java by Joshua Bloch [Jon Aquino]
    int result = 17 * idxElt_;
    result = 37 * result + hashCode(precision_.round(x_));
    result = 37 * result + hashCode(precision_.round(y_));
    return result;
  }

  /**
   * Returns a hash code for a double value, using the algorithm from Joshua Bloch's book <i>Effective Java"</i>.
   */
  public static int hashCode(final double _x) {
    final long f = Double.doubleToLongBits(_x);
    return (int) (f ^ (f >>> 32));
  }

  protected int getIdxElt() {
    return idxElt_;
  }

  public double getX() {
    return x_;
  }

  public double getY() {
    return y_;
  }

  /**
   * @return the precision_
   */
  public GISPrecision getPrecision() {
    return precision_;
  }
}