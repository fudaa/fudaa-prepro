package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * Une classe dans lequel les variables sont simplement stock�es.
 * @author deniger
 */
public abstract class TrPostSourceListVar extends TrPostSourceAbstract {

  /**
   * @param titre
   * @param g
   * @param time
   * @param v
   * @param impl
   */
  public TrPostSourceListVar(String titre, EfGridInterface g, double[] time, H2dVariableType[] v, CtuluUI impl) {
    super(titre, g, time, impl);
    setInitVar(v);
  }

  @Override
  protected final void setInitVar(final H2dVariableType[] _v) {
    super.setInitVar(_v);

  }

}
