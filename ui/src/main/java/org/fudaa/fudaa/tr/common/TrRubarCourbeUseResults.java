/*
 * @creation 15 mars 07
 * 
 * @modification $Date: 2007-04-30 14:22:41 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.fu.FuComparator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.fudaa.tr.data.TrVisuPanel;
import org.fudaa.fudaa.tr.rubar.TrRubarBcAreteLayer;
import org.fudaa.fudaa.tr.rubar.TrRubarVisuPanel;

/**
 * @author fred deniger
 * @version $Id: TrRubarCourbeUseResults.java,v 1.2 2007-04-30 14:22:41 deniger Exp $
 */
public class TrRubarCourbeUseResults extends TrCourbeUseResultsAbstract {

  transient final TrCourbeUseCounter usedInCl_ = new TrCourbeUseCounter(TrResource.getS("Conditions limites"));
  transient final TrCourbeUseCounter usedInApp_ = new TrCourbeUseCounter(H2dVariableTransType.APPORT_PLUIE.getName());
  transient final TrCourbeUseCounter usedInVent_ = new TrCourbeUseCounter(H2dVariableTransType.VENT.getName());
  transient private H2dVariableType[] types_;

  public TrRubarCourbeUseResults(final TrVisuPanel _visu) {
    super(_visu);
  }

  @Override
  public void setFilter(final EvolutionReguliereInterface _evol) {
    usedInApp_.setEvolToTest(_evol);
    usedInCl_.setEvolToTest(_evol);
    usedInVent_.setEvolToTest(_evol);
  }

  @Override
  public void validSearch() {
    final int nbEvolCl = usedInCl_.getNbEvol();
    final int nbEvolApp = usedInApp_.getNbEvol();
    final int nbEvolVent = usedInVent_.getNbEvol();
    final List allEvols = new ArrayList(nbEvolCl + nbEvolApp + nbEvolVent);
    final List allTypes = new ArrayList(nbEvolCl + nbEvolApp + nbEvolVent);
    EvolutionReguliereInterface[] evols = (EvolutionReguliereInterface[]) usedInCl_.getEvols().toArray(new EvolutionReguliereInterface[nbEvolCl]);
    Arrays.sort(evols, FuComparator.STRING_COMPARATOR);
    for (EvolutionReguliereInterface evolutionReguliereInterface : evols) {
      allEvols.add(evolutionReguliereInterface);
      allTypes.add(null);
    }
    evols = (EvolutionReguliereInterface[]) usedInApp_.getEvols().toArray(new EvolutionReguliereInterface[nbEvolApp]);
    Arrays.sort(evols, FuComparator.STRING_COMPARATOR);
    for (EvolutionReguliereInterface evolutionReguliereInterface : evols) {
      allEvols.add(evolutionReguliereInterface);
      allTypes.add(H2dVariableTransType.APPORT_PLUIE);
    }
    evols = (EvolutionReguliereInterface[]) usedInVent_.getEvols().toArray(new EvolutionReguliereInterface[nbEvolApp]);
    Arrays.sort(evols, FuComparator.STRING_COMPARATOR);
    for (EvolutionReguliereInterface evolutionReguliereInterface : evols) {
      allEvols.add(evolutionReguliereInterface);
      allTypes.add(H2dVariableTransType.VENT);
    }

    evols_ = (EvolutionReguliereInterface[]) allEvols.toArray(new EvolutionReguliereInterface[allEvols.size()]);
    types_ = (H2dVariableType[]) allTypes.toArray(new H2dVariableType[allTypes.size()]);
  }

  @Override
  public TrCourbeUseCounter getCounter(final int _rowIndex) {
    H2dVariableType var = types_[_rowIndex];
    if (var == H2dVariableTransType.APPORT_PLUIE) {
      return usedInApp_;
    }
    if (var == H2dVariableTransType.VENT) {
      return usedInVent_;
    }
    return usedInCl_;
  }

  @Override
  public void selectInView(final EvolutionReguliereInterface _eve) {
    final int idx = CtuluLibArray.findObject(evols_, _eve);
    if (idx >= 0) {
      H2dVariableType var = types_[idx];
      if (var == H2dVariableTransType.APPORT_PLUIE) {
        selectInMeshLayerViewApp(_eve);
      } else if (var == H2dVariableTransType.VENT) {
        selectInMeshLayerViewVent(_eve);
      } else {
        selectClInView(_eve);
      }
    }

  }

  protected void selectInMeshLayerViewApp(final EvolutionReguliereInterface _eve) {
    selectedInMeshLayer(usedInApp_.getIdxSelected(_eve));
  }

  protected void selectInMeshLayerViewVent(final EvolutionReguliereInterface _eve) {
    selectedInMeshLayer(usedInVent_.getIdxSelected(_eve));
  }

  protected void selectClInView(final EvolutionReguliereInterface _eve) {
    final int[] select = usedInCl_.getIdxSelected(_eve);
    if (select != null) {
      final TrRubarBcAreteLayer layer = ((TrRubarVisuPanel) visu_).getBcAreteLayer();
      visu_.getArbreCalqueModel().setSelectionCalque(layer);
      layer.setSelection(select);
      visu_.zoomOnSelected();
      activateVisuFrame();
    }

  }

}
