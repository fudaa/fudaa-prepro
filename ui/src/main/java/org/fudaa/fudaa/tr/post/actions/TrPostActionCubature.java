package org.fudaa.fudaa.tr.post.actions;

import java.awt.event.ActionEvent;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostDialogCubature;

/**
 * Action de gestion des cubatures
 *
 * @author Adrien Hadoux
 */
public class TrPostActionCubature extends EbliActionSimple {

  // final MvProfileTarget src_;
  final TrPostVisuPanel panel_;
  TrPostCommonImplementation impl_;
  TrPostDialogCubature dialogCubatures = null;
  public TrPostActionCubature(final TrPostVisuPanel _visu, final TrPostCommonImplementation impl) {
    super(TrLib.getString("Calcul des cubatures"), FudaaResource.FUDAA.getIcon("volume"), "CUBATURE");
    panel_ = _visu;
    impl_ = impl;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
	 // if(dialogCubatures == null)
		  dialogCubatures = new TrPostDialogCubature(panel_, impl_);
	  dialogCubatures.setCalque_(panel_);
	  dialogCubatures.setImpl_(impl_);
	  dialogCubatures.buildDialog();
    
  }
}
