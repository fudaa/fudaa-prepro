/**
 * @creation 10 janv. 2005
 * @modification $Date: 2007-05-22 14:20:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluOptionPane;
import org.fudaa.dodico.dico.DicoDataType;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoResource;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.io.serafin.SerafinInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSIProperties;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.olb.exec.OLBExec;
import org.fudaa.dodico.telemac.io.TelemacVariableMapper;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacSerafinBuilder.java,v 1.29 2007-05-22 14:20:38 deniger Exp $
 */
public class TrTelemacSerafinBuilder extends TelemacVariableMapper {

  SerafinInterface geoInterface_;
  Map geoValues_;
  TrTelemacSerafinHelper helper_;
  boolean isGridModified_;

  /**
   * @param _params
   */
  public TrTelemacSerafinBuilder(final TrTelemacSerafinHelper _helper) {
    super();
    helper_ = _helper;
    
  }
  
  private File findFileToSave(final File _init) {
    // premier test avec .old
    File test = new File(_init.getAbsolutePath() + ".old");
    if (!test.exists() && CtuluLibFile.isWritable(test)) {
      return test;
    }
    test = new File(CtuluLibFile.getSansExtension(_init).getAbsolutePath() + '-' + FuLib.date(System.currentTimeMillis(), "yyyy-MM-dd") + ".ser");
    if (!test.exists() && CtuluLibFile.isWritable(test)) {
      return test;
    }
    return null;
  }

  /**
   * @param _isNewProjet
   * @param _serafinFile
   * @param _isReprise
   * @return
   */
  private void finishLoadVars(final boolean _isNewProjet, final ProgressionInterface _prog) {
    loadGeoValues();
    boolean reprise = false;
    final File gFile = helper_.getGridFileSet();
    if (helper_.isRepriseAvailable()) {
      final H2dTelemacSIProperties prop = helper_.getH2DParams().getSolutionsInit();
      if (prop.containRepriseValues(geoValues_)) {
        // pour eventuellement r�cuperer la bathy avec la hauteur et la cote
        helper_.determineBathyForCI(geoValues_, geoValues_, null);
        if (_isNewProjet && prop.containsRequiredRepriseValues(geoValues_)) {
          final File saveFile = findFileToSave(gFile);
          reprise = autoActiveRepMsg(saveFile);
          if (reprise) {
            // si n�cessaire, on calcul la cote
            if (saveFile == null) {
              new TrTelemacSaveBackupGeometryFile(gFile).afficheModale(getUI().getFrame(),
                      TrResource.getS("Sauvegarde de l'ancien fichier de g�om�trie"));
            } else {
              CtuluLibFile.copyFile(gFile, saveFile);
            }
            helper_.determineBathyAndHForCI(geoValues_, geoValues_);
            // sauvegarde du nouveau fichier de geometrie
            final TrTelemacSerafinSaver saver = new TrTelemacSerafinSaver(helper_.getH2DParams());
            final Map newGeoData = new HashMap(geoValues_);
            prop.removeRepriseValues(newGeoData);
            saver.saveSerafin(gFile, _prog, getUI(), newGeoData, timeStepLoaded_, helper_.getH2DParams().getRepriseInitialDateInMillis());
            // activation de la reprise
            helper_.getH2DParams().getDicoParams().setValue(helper_.getRepriseBooleanKw(), DicoDataType.Binaire.TRUE_VALUE);
            helper_.getH2DParams().activeSi(geoValues_, null);
            
            helper_.getH2DParams().getSolutionsInit().setFirstTimeStep(timeStepLoaded_, null);
            helper_.setRepriseLoaded(true);
            
          } else {
            prop.removeRepriseValues(geoValues_);
            isGridModified_ = true;
          }
          
        } else {
          getUI().warn(
                  helper_.getGridName(),
                  TrResource.getS("Le fichier de g�om�trie contient des r�sultats.") + CtuluLibString.LINE_SEP
                  + TrResource.getS("Ces variables seront supprim�es."));
          prop.removeRepriseValues(geoValues_);
          isGridModified_ = true;
        }
        
      }
    }
    if (!reprise) {
      helper_.getH2DParams().initializeNodalProperties(geoValues_);
      helper_.setGridLoaded(isGridModified_);
    }
  }

  /**
   * @param _saveFile
   * @return
   */
  private boolean autoActiveRepMsg(final File _saveFile) {
    String msg = TrResource.getS("Le fichier de g�om�trie contient des r�sultats.") + CtuluLibString.ESPACE
            + TrResource.getS("Vous pouvez activer une reprise de calcul.") + CtuluLibString.LINE_SEP
            + TrResource.getS("Si vous acceptez, la reprise sera activ�e et un fichier de reprise sera cr���.") + CtuluLibString.LINE_SEP;
    if (_saveFile == null) {
      msg += TrResource.getS("Une copie de votre fichier de g�om�trie sera effectu�e (�tape suivante)");
    } else {
      msg += TrResource.getS("Une copie de votre fichier de g�om�trie sera effectu�e sous le nom {0}", _saveFile.getName());
    }
    msg += CtuluLibString.LINE_SEP + TrResource.getS("et il sera remplac� par un fichier simplifi�.") + CtuluLibString.LINE_SEP
            + CtuluLibString.LINE_SEP
            + TrResource.getS("Si vous refusez, les variables correspondant � des r�sultats seront supprim�es de votre nouveau projet.");
    final CtuluOptionPane p = new CtuluOptionPane(msg, JOptionPane.QUESTION_MESSAGE);
    final String[] opt = new String[]{TrResource.getS("Activer la reprise"), TrResource.getS("Ne pas activer la reprise de calcul")};
    p.setOptions(opt);
    CtuluOptionPane.showDialog(getUI().getFrame(), TrTelemacSerafinHelper.getRepriseTitle(), p);
    return opt[0].equals(p.getValue());
  }
  
  private int getTimeStepToUse(final SerafinInterface _serafinResults) {
    int timeStep = _serafinResults.getTimeStepNb() - 1;
    if (timeStep > 0) {
      isGridModified_ = true;
      final String[] str = new String[_serafinResults.getTimeStepNb()];
      for (int i = 0; i < str.length; i++) {
        str[i] = Double.toString(_serafinResults.getTimeStep(i));
      }
      timeStep = TrLib.chooseTimeStepToUse(str, getUI());
    }
    return timeStep;
  }
  double timeStepLoaded_;
  
  private void loadGeoValues() {
    if (geoValues_ == null) {
      final int timeStep = helper_.getTimeStepToUse(geoInterface_);
      geoValues_ = helper_.buildVariables(geoInterface_, timeStep, null);
      timeStepLoaded_ = geoInterface_.getTimeStep(timeStep);
    }
  }

  /**
   * Methode permettant de charger et de v�rifier un fichier de reprise. Cette methode doit etre appele lorsque l'utilisateur charge tout ces
   * fichiers.
   *
   * @param _prg
   * @param _geoValues
   * @param _dirBase
   * @return true si la reprise est bien effective. false si probleme
   */
  private boolean loadRepriseFile(final ProgressionInterface _prg, final File _dirBase) {
    boolean isReprise = false;
    final Map repriseValues = new HashMap();
    TrTelemacSerafinHelper.RepriseResults repRes = null;
    if (helper_.isRepriseActivated()) {
      repRes = readReprise(_prg, repriseValues);
      if (repRes != null) {
        helper_.getH2DParams().setRepriseInitialDate(repRes.initialDate);
      }
      // la reprise n'est pas valide.
      if (repRes == null || repriseValues.size() == 0) {
        getUI().warn(TrTelemacSerafinHelper.getRepriseTitle(), TrResource.getS("La reprise de calcul a �t� annul�e"));
        isReprise = false;
        helper_.getH2DParams().getDicoParams().removeValue(helper_.getRepriseBooleanKw());
        // la reprise est valide
      } else {
        isReprise = true;
        // le fichier de reprise ne contient pas la bathy: on essaie de la r�cup�rer depuis le fichier de geometrie
        if (!repriseValues.containsKey(H2dVariableType.BATHYMETRIE)) {
          loadGeoValues();
        }
        // On verifie que la cote et la bathy sont presentes sinon on les calcule.
        repRes.isModified_ |= helper_.determineBathyAndHForCI(repriseValues, geoValues_);
        
      }
      
    }
    if (isReprise && repRes != null) {
      helper_.getH2DParams().activeSi(repriseValues, null);
      helper_.setRepriseLoaded(repRes.isModified_);
      helper_.getH2DParams().getSolutionsInit().setFirstTimeStep(repRes.timeStep_, null);
    }
    return isReprise;
  }

  /**
   * Remplie _m avec variable->double[]. Renvoie true si modification dans le fichier de reprise (variables en trop supprim�es ...)
   *
   * @param _prg
   * @param _ui
   * @param _grid
   * @return true si modif
   */
  private TrTelemacSerafinHelper.RepriseResults readReprise(final ProgressionInterface _prg, final Map _m) {
    if (!helper_.isRepriseActivated()) {
      return null;
    }
    // la reprise n'a pas �t� activ�e.
    final DicoEntite repriseFile = helper_.getRepriseFileKw();
    if (!helper_.isRepriseFileSetAndValid()) {
      getUI().error(
              TrTelemacSerafinHelper.getRepriseTitle(),
              DicoResource.getS("La valeur {0} est invalide pour le mot-cl� {1}", helper_.getH2DParams().getDicoParams().getValue(repriseFile),
              repriseFile.getNom()));
      return null;
    }
    final File file = helper_.getFileSet(repriseFile);
    if (Fu.DEBUG && FuLog.isDebug() && file != null) {
      FuLog.debug("FTR: read si file " + file.getAbsolutePath());
    }
    return helper_.loadRepriseData(_prg, file, _m);
  }
  
  private boolean setMaillageIsOk(final EfGridInterface _grid, long initialDateInMillis, final ProgressionInterface _prg) {
    final CtuluAnalyze analyze = new CtuluAnalyze();
    analyze.setDesc(H2dResource.getS("Analyse donn�es"));
    helper_.getH2DParams().setMaillage(_grid, _prg, analyze);
    helper_.getH2DParams().setInitialDateInMillis(initialDateInMillis);
    getUI().manageAnalyzeAndIsFatal(analyze);
    helper_.unsetMessage();
    return !analyze.containsFatalError();
  }
  
  FudaaCommonImplementation getUI() {
    return helper_.getUI();
  }

  /**
   * Cette methode doit etre appelee lors du chargement d'un projet Telemac. Elle permet de charger le fichier de geo et/ou le fichier de reprise. Les
   * variables requises sont cr��es ( he, cote d'eau, ...) et si c'est un nouveau projet, on teste le fichier de geo pour savoir s'il contient des var
   * de reprise ou non: si oui, on propose � l'utilisateur d'activer une reprise de calcul. Si refuse, le fichier de geo est simplifi�.
   *
   * @param _dirBase le repertoire du fichier cas: pour r�cup�rer les fichiers � partir des chemins relatifs
   * @param _ipobo le tableau des indices des points externes dans l'ordre telemac
   * @param _prg le receveur de progression
   * @param _isNewProjet true si nouveau projet: dans ce cas on peut proposer d'activer une reprise par d�faut.
   */
  public synchronized boolean loadGeometrie(final File _dirBase, final int[] _ipobo, final ProgressionInterface _prg, final boolean _isNewProjet) {
    // test si le maillage est deja charge ou non.
    final DicoEntite ent = helper_.getGridKw();
    if (helper_.isLoaded(ent)) {
      FuLog.error("FTR: grid is already opened");
      return false;
    }
    final File serafinFile = helper_.getFileSet(ent);
    geoInterface_ = helper_.readGridFile(serafinFile, _prg);
    // il y a une erreur grave dans le fichier de maillage
    if (geoInterface_ == null) {
      helper_.unsetMessage();
      // eh bien au revoir
      return false;
    }
    getUI().setMainProgression(50);
    getUI().setMainMessage(TrResource.getS("Recherche des bords"));
    final EfGridInterface grid = geoInterface_.getGrid();
    // recherche des bords
    final int[] ipobo = _ipobo;
    try {
      final CtuluAnalyze logs = new CtuluAnalyze();
      if (ipobo == null) {
        
        grid.computeBord(_prg, logs);
        getUI().manageAnalyzeAndIsFatal(logs);
      } else {
        grid.computeBordFast(ipobo, _prg, logs);
      }
      if (logs.containsFatalError() && ipobo != null) {
        logs.changeFatalError(TrLib
                .getString("Les fronti�res ne peuvent pas �tre d�finies.\nIl semble que les conditions limites soient mal sp�cifi�es.")
                + CtuluLibString.LINE_SEP_SIMPLE + TrLib.getString("Il est conseill� de recr�er un projet � partir du fichier de g�om�trie."));
      }
      if (getUI().manageAnalyzeAndIsFatal(logs)) {
        return false;
      }
    } catch (final ArrayIndexOutOfBoundsException e) {
      // erreur dans le fichier
      getUI().error(TrResource.getS("Recherche des bords"), TrResource.getS("le fichier serafin est corrompu"), false);
      // grid.computeBord(_prg);
    }
    // affectation du maillage au projet
    // si le maillage est deja affect� : erreur
    // ne devrait jamais arrive car cette methode est appel�e � la cr�ation du projet
    if (!setMaillageIsOk(grid, geoInterface_.getIdate(), _prg)) {
      return false;
    }
    helper_.getH2DParams().setXyDoublePrecision(geoInterface_.isXYdoublePrecision());

    // les variables du fichier de g�om�trie
    boolean isReprise = helper_.isRepriseActivated();
    if (isReprise) {
      // renvoie false si erreur dans le reprise
      isReprise = loadRepriseFile(_prg, _dirBase);
    }
    // si la reprise n'est pas effective on charge les variables du fichier de geo.
    // si des variables de reprise sont pr�sentes on propose � l'utilisateur de cr�er un projet
    // avec une reprise de calcul activ�e.
    if (!isReprise) {
      finishLoadVars(_isNewProjet, _prg);
    }
    return true;
  }
  
  public synchronized boolean loadGeometryAndOptimize(final File _dirBase, final ProgressionInterface _prg) {
    final SerafinInterface serafinResults = helper_.readGridFile(_prg);
    isGridModified_ = true;
    if (serafinResults == null) {
      return false;
    }
    long initialDate = serafinResults.getIdate();
    EfGridInterface grid = serafinResults.getGrid();
    if (FuLog.isWarning()) {
      FuLog.warning("FTR: use olb to optimize grid");
    }
    final OLBExec olb = new OLBExec();
    grid = olb.computeGrid(grid, _prg, getUI());
    final CtuluAnalyze ana = new CtuluAnalyze();
    grid.computeBord(_prg, ana);
    getUI().manageAnalyzeAndIsFatal(ana);
    if (!setMaillageIsOk(grid, serafinResults.getIdate(), _prg)) {
      return false;
    }
    // le faire maintenant pour �viter de r�affecter le tableau des valeurs.
    final int ts = getTimeStepToUse(serafinResults);
    geoValues_ = helper_.buildVariables(serafinResults, ts, grid, _prg);
    timeStepLoaded_ = ts;
    finishLoadVars(true, _prg);
    return true;
  }
}
