/*
 *  @creation     26 ao�t 2005
 *  @modification $Date: 2006-09-01 14:56:44 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.calque.BCalqueSaverInterface;
import org.fudaa.ebli.calque.BCalqueSaverSingle;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.fudaa.sig.persistence.FSigLayerPointPersistence;
import org.fudaa.fudaa.tr.persistence.TrSiProfilLayerPersistence;

/**
 * @deprecated garde pour ancienne sauvegarde
 * @author fred deniger
 * @version $Id: TrSiProfilLayerProperties.java,v 1.9 2006-09-01 14:56:44 deniger Exp $
 */
@Deprecated
public class TrSiProfilLayerProperties extends BCalqueSaverSingle {

  int idxProfil_ = -1;

  boolean isLevelUpdateToDate_;

  GISZoneCollection profils_;

  TrSiProfilModel.Zone zones_;

  BCalqueSaverInterface zonesUI_;

  /**
   * @deprecated garde pour ancienne sauvegarde
   */
  @Deprecated
  public TrSiProfilLayerProperties() {
    setPersistenceClass(TrSiProfilLayerPersistence.class.getName());
  }

  @Override
  public EbliUIProperties getUI() {
    final EbliUIProperties ui = super.getUI();
    if (profils_ != null) {
      ui.put(FSigLayerPointPersistence.getGeomId(), profils_);
      ui.put(TrSiProfilLayerPersistence.getLevelUptodateId(), isLevelUpdateToDate_);
      ui.put(TrSiProfilLayerPersistence.getZoneModelId(), zones_);
      ui.put(TrSiProfilLayerPersistence.getZoneUIId(), zonesUI_);
      profils_ = null;
      zones_ = null;
      zonesUI_ = null;
    }
    return ui;

  }

}
