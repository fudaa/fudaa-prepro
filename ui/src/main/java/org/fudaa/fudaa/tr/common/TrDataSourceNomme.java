package org.fudaa.fudaa.tr.common;

/**
 * @author deniger
 *
 */
public interface TrDataSourceNomme {
  
  
  /**
   * @return the name of the data source.
   */
  String getSourceName();

}
