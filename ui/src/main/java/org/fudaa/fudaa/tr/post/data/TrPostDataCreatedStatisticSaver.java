/*
 * @creation 2 f�vr. 07
 * 
 * @modification $Date: 2007-04-30 14:22:38 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.data;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourcesManager;

/**
 * @author fred deniger
 * @version $Id: TrPostDataCreatedStatisticSaver.java,v 1.2 2007-04-30 14:22:38 deniger Exp $
 */
public class TrPostDataCreatedStatisticSaver implements TrPostDataCreatedSaver {

  @Override
  public TrPostDataCreatedStatistic restore(H2dVariableType _newVar, final TrPostSource _src, final CtuluUI _ui, final Map _shortName,
      TrPostSourcesManager srcMng, CtuluAnalyze log, double[] savedValues) {
    TrPostDataCreatedStatistic stat = TrPostDataCreatedStatistic.createFrom(this, _src, _shortName, _ui, savedValues);
    stat.setVarCreated((H2dVariableTypeCreated) _newVar);

    return stat;
  }

  String statId_;
  String shortName_;
  int tBegin_;
  int tEnd_;
  CtuluRange r_;

  public TrPostDataCreatedStatisticSaver() {
    super();
  }

  @Override
  public Collection<String> getVarNameUsed() {
    return shortName_ == null ? Collections.<String> emptyList() : Arrays.asList(shortName_);
  }

  @Override
  public double[] getValuesToPersist(TrPostDataCreated dataCreated) {
    TrPostDataCreatedStatistic init = (TrPostDataCreatedStatistic) dataCreated;
    return init.value_;
  }

  public TrPostDataCreatedStatisticSaver(final TrPostDataCreatedStatistic _data) {
    statId_ = _data.evaluator_.getStatId();
    tBegin_ = _data.tsBegin_;
    tEnd_ = _data.tsEnd_;
    shortName_ = _data.varToStat_.getShortName();
    r_ = _data.r_;
  }

}
