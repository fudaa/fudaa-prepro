package org.fudaa.fudaa.tr.export;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluValueValidator;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.fudaa.meshviewer.export.MvExportOperationBuilderSelection;
import org.fudaa.fudaa.meshviewer.export.MvExportOperationSelectionItem;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostFilterHauteur;

/**
 * @author Fred Deniger
 * @version $Id: TrExportFilterItemHauteurAbstract.java,v 1.8 2007-05-04 14:01:54 deniger Exp $
 */
public abstract class TrExportFilterItemHauteurAbstract extends MvExportOperationSelectionItem {

  MvExportOperationBuilderSelection save_;
  BuTextField tfHauteur_;

  String textForStrict;

  JPanel pn;

  BuCheckBox cbStrict_;

  public TrExportFilterItemHauteurAbstract() {
    super();
  }

  @Override
  public JComponent getConfigureComponent() {

    if (pn == null) {
      cbStrict_ = new BuCheckBox(textForStrict == null ? TrLib
          .getString("Exporter uniquement les �l�ments compl�tement mouill�s") : textForStrict);
      // cbStrict_.setSelected(_strictActif);
      tfHauteur_ = BuTextField.createDoubleField();
      tfHauteur_.setText(Double.toString(TrPostFilterHauteur.getEpsFromPref()));
      final CtuluValueValidator val = new CtuluValueValidator.DoubleMin(0);
      tfHauteur_.setValueValidator(val);
      tfHauteur_.setToolTipText(val.getDescription());
      tfHauteur_.setEnabled(true);
      tfHauteur_.getDocument().addDocumentListener(new DocumentListener() {

        @Override
        public void changedUpdate(final DocumentEvent _e) {
          save_ = null;
        }

        @Override
        public void insertUpdate(final DocumentEvent _e) {
          save_ = null;
        }

        @Override
        public void removeUpdate(final DocumentEvent _e) {
          save_ = null;
        }
      });
      BuVerticalLayout vl = new BuVerticalLayout(3);
      pn = new JPanel(vl);
      BuPanel pnHauteur = new BuPanel(new BuBorderLayout(4, 4));
      JLabel lb = new JLabel(TrResource.getS("Hauteur d'eau minimale"));
      lb
          .setToolTipText("<html><body>"
              + TrResource
                  .getS("Fixe la valeur du seuil (en m�tre) de hauteur d'eau en dessous <br>duquel un noeud est considere comme sec"));
      pnHauteur.add(lb, BorderLayout.WEST);
      pnHauteur.add(tfHauteur_);
      pn.add(pnHauteur);
      pn.add(cbStrict_);
    }
    return pn;
  }

  public String getTextForStrict() {
    return textForStrict;
  }

  @Override
  public String getTitle() {
    return TrResource.getS("Hauteur d'eau minimale");
  }

  protected boolean isStrict() {
    return cbStrict_ != null && cbStrict_.isSelected();
  }

  @Override
  public String getErrorMessage() {
    tfHauteur_.setForeground(CtuluLibSwing.getDefaultTextFieldForegroundColor());
    boolean r = true;
    final String s = tfHauteur_.getText();
    if (CtuluLibString.isEmpty(s) || !tfHauteur_.getValueValidator().isValueValid(tfHauteur_.getValue())) {
      tfHauteur_.setForeground(Color.RED);
      r = false;
    }
    if (r) {
      tfHauteur_.setToolTipText(getTitle());
    } else {
      tfHauteur_.setToolTipText(TrResource.getS("Valeur positive"));
    }
    return r ? null : TrResource.getS("La valeur utilis�e pour la hauteur n'est pas valide");
  }

  public void setTextForStrict(String textForStrict) {
    this.textForStrict = textForStrict;
  }

}