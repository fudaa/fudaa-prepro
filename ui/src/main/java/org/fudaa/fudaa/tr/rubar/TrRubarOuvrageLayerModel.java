/**
 * @creation 3 janv. 2005
 * @modification $Date: 2007-05-04 14:01:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuResource;
import com.memoire.bu.BuTable;
import org.locationtech.jts.geom.Coordinate;
import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfElementVolume;
import org.fudaa.dodico.ef.EfGridVolumeInterface;
import org.fudaa.dodico.ef.EfSegment;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.layer.MvGridLayerGroup;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrInfoSenderDelegate;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarOuvrageLayerModel.java,v 1.27 2007-05-04 14:01:53 deniger Exp $
 */
public class TrRubarOuvrageLayerModel extends MvElementModelDefault {
  /**
   * @author fred deniger
   * @version $Id: TrRubarOuvrageLayerModel.java,v 1.27 2007-05-04 14:01:53 deniger Exp $
   */
  static final class OuvrageCellRenderer extends CtuluCellTextRenderer {
    @Override
    protected void setValue(final Object _value) {
      if (_value == null) {
        setText(CtuluLibString.EMPTY_STRING);
      } else {
        setText(H2dRubarNumberFormatter.DEFAULT_XY_FMT.format(((Double) _value).doubleValue()));
      }
    }
  }

  private final class OuvTableModel extends AbstractTableModel {
    public OuvTableModel() {

    }

    @Override
    public int getColumnCount() {
      return 16;
    }

    @Override
    public String getColumnName(final int _column) {
      switch (_column) {
        case 0:
          return MvResource.getS("Indice");
        case 1:
          return TrResource.getS("R�f�rence de l'ouvrage");
        case 2:
          return TrResource.getS("Nombre de mailles internes");
        case 3:
          return TrResource.getS("Nombre d'ouvrages �l�mentaires");
        case 4:
          return getMailleAmontStr();
        case 5:
          return getMailleAmontStr() + getXStr();
        case 6:
          return getMailleAmontStr() + getYStr();
        case 7:
          return getAreteAmont();
        case 8:
          return getAreteAmont() + getXStr();
        case 9:
          return getAreteAmont() + getYStr();
        case 10:
          return getMailleAval();
        case 11:
          return getMailleAval() + getXStr();
        case 12:
          return getMailleAval() + getYStr();
        case 13:
          return getAreteAval();
        case 14:
          return getAreteAval() + getXStr();
        case 15:
          return getAreteAval() + getYStr();
        default:
          return CtuluLibString.EMPTY_STRING;
      }
    }

    @Override
    public int getRowCount() {
      return TrRubarOuvrageLayerModel.this.getNbOuvrage();
    }

    @Override
    public Class getColumnClass(final int _columnIndex) {
      if (_columnIndex == 1) {
        return String.class;
      }
      if (_columnIndex < 5 || _columnIndex == 7 || _columnIndex == 10 || _columnIndex == 13) {
        return Integer.class;
      }
      return Double.class;
    }



    @Override
    public Object getValueAt(final int _rowIndex, final int _column) {

      if (_column == 0) {
        return new Integer(_rowIndex + 1);
      }
      final H2dRubarOuvrage ouvSelected = TrRubarOuvrageLayerModel.this.getOuvrage(_rowIndex);
      switch (_column) {
        case 1:
          return ouvSelected.getOuvrageRef().getName();
        case 2:
          return new Integer(ouvSelected.getNbMailleIntern());
        case 3:
          return new Integer(ouvSelected.getNbOuvrageElementaires());
        case 4:
          return new Integer(ouvSelected.getElt1() + 1);
        case 5:
          return CtuluLib.getDouble(ouvSelected.getXElementAmont());
        case 6:
          return CtuluLib.getDouble(ouvSelected.getYElementAmont());
        case 7:
          return new Integer(ouvSelected.getArete1() + 1);
        case 8:
          return CtuluLib.getDouble(ouvSelected.getXaAmont());
        case 9:
          return CtuluLib.getDouble(ouvSelected.getYAmont());
        default:
          // ben rien tiens !
      }
      if (!ouvSelected.isElt2Set()) {
        return null;
      }
      switch (_column) {
        case 10:
          return new Integer(ouvSelected.getElt2() + 1);
        case 11:
          return CtuluLib.getDouble(ouvSelected.getXElementAval());
        case 12:
          return CtuluLib.getDouble(ouvSelected.getYELementAval());
        case 13:
          return new Integer(ouvSelected.getArete2() + 1);
        case 14:
          return CtuluLib.getDouble(ouvSelected.getXaAval());
        case 15:
          return CtuluLib.getDouble(ouvSelected.getYAval());
        default:
          return null;
      }
    }
  }

  private final transient H2dRubarOuvrageMng mng_;
  private transient boolean modeElt_;
  protected transient TrInfoSenderDelegate sender_;

  public TrRubarOuvrageLayerModel(final H2dRubarGridAreteSource _grid, final H2dRubarOuvrageMng _mng,
                                  final TrInfoSenderDelegate _sender) {
    super(_grid.getRubarGrid());
    sender_ = _sender;
    mng_ = _mng;
  }

  @Override
  public EfGridVolumeInterface getG() {
    return (EfGridVolumeInterface) super.g_;
  }

  // private transient H2dRubarOuvrageMng mng_;

  /**
   * @param _param les parametres de rubar
   * @param _sender l'envoyeur d'infos
   */
  public TrRubarOuvrageLayerModel(final H2dRubarParameters _param, final TrInfoSenderDelegate _sender) {
    super(_param.getGridVolume());
    sender_ = _sender;
    mng_ = _param.getOuvrageMng();
  }

  /**
   * @param _eltIntern les elemente internes
   * @param _elt1 l'element 1 amont
   * @param _ar1 l'arete 1
   * @param _elt2 l'element 2 aval
   * @param _ar2 l'arete 2
   * @param _cmd le receveur de commande
   * @return l'ouvrage cree
   */
  public H2dRubarOuvrage addOuvrage(final int[] _eltIntern, final int _elt1, final int _ar1, final int _elt2,
                                    final int _ar2, final CtuluCommandContainer _cmd) {
    return mng_.addOuvrage(_eltIntern, _elt1, _ar1, _elt2, _ar2, _cmd);
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    if (isModeElt() && getSender() != null) {
      final BuTable r = getSender().createElementTable();
      EbliTableInfoPanel.setTitle(r, MvResource.getS("El�ments"));
      return r;
    }
    return getOuvrageTable();
  }

  @Override
  public void fillWithInfo(final InfoData _destInfo, final ZCalqueAffichageDonneesInterface _layer) {
    final CtuluListSelectionInterface sltn = _layer.getLayerSelection();
    if (!isInitialized()) {
      _destInfo.setTitle(BuResource.BU.getString("Attention"));
      _destInfo.put(BuResource.BU.getString("Attention"), H2dResource.getS("Fichier dat non charg�"));
    }
    if (modeElt_) {
      if (sender_ != null) {
        sender_.fillWithElementInfo(_destInfo, sltn, _layer.getTitle());
      }
      return;
    }
    final int nbSelected = sltn == null ? 0 : sltn.getNbSelectedIndex();
    if (nbSelected != 1) {
      _destInfo.setTitle(H2dResource.getS("Ouvrages"));
      _destInfo.put(TrResource.getS("Nombre d'ouvrages"), CtuluLibString.getString(getNbOuvrage()));
      _destInfo.put(TrResource.getS("Nombre d'ouvrages s�lectionn�s"), CtuluLibString.getString(nbSelected));
    } else if (sltn != null) {
      final int idx = sltn.getMaxIndex();
      _destInfo.setTitle(H2dResource.getS("Ouvrage {0}", CtuluLibString.getString(idx + 1)));

      final H2dRubarOuvrage ouvSelected = getOuvrage(idx);
      _destInfo.put(TrResource.getS("R�f�rence de l'ouvrage"), ouvSelected.getOuvrageRef().getName());
      _destInfo.put(TrResource.getS("Nombre de mailles internes"), CtuluLibString.getString(ouvSelected
          .getNbMailleIntern()));
      final int nbOuvElem = ouvSelected.getNbOuvrageElementaires();
      _destInfo.put(TrResource.getS("Nombre d'ouvrages �l�mentaires"), CtuluLibString.getString(nbOuvElem));
      String init = getMailleAmontStr();
      _destInfo.put(init, CtuluLibString.getString(ouvSelected.getElt1() + 1));
      _destInfo.put(init + getXStr(), H2dRubarNumberFormatter.DEFAULT_XY_FMT.format(ouvSelected.getXElementAmont()));
      _destInfo.put(init + getYStr(), H2dRubarNumberFormatter.DEFAULT_XY_FMT.format(ouvSelected.getYElementAmont()));
      init = getAreteAmont();
      _destInfo.put(init, CtuluLibString.getString(ouvSelected.getArete1() + 1));
      _destInfo.put(init + getXStr(), H2dRubarNumberFormatter.DEFAULT_XY_FMT.format(ouvSelected.getXaAmont()));
      _destInfo.put(init + getYStr(), H2dRubarNumberFormatter.DEFAULT_XY_FMT.format(ouvSelected.getYAmont()));
      if (ouvSelected.isElt2Set()) {
        init = getMailleAval();
        _destInfo.put(init, CtuluLibString.getString(ouvSelected.getElt2() + 1));
        _destInfo.put(init + getXStr(), H2dRubarNumberFormatter.DEFAULT_XY_FMT.format(ouvSelected.getXElementAval()));
        _destInfo.put(init + getYStr(), H2dRubarNumberFormatter.DEFAULT_XY_FMT.format(ouvSelected.getYELementAval()));
        init = getAreteAval();
        _destInfo.put(init, CtuluLibString.getString(ouvSelected.getArete2() + 1));
        _destInfo.put(init + getXStr(), H2dRubarNumberFormatter.DEFAULT_XY_FMT.format(ouvSelected.getXaAval()));
        _destInfo.put(init + getYStr(), H2dRubarNumberFormatter.DEFAULT_XY_FMT.format(ouvSelected.getYAval()));
      } else {
        _destInfo.put(TrResource.getS("Maille aval ind�finie"), TrResource.getS("D�bit non r�inject�"));
      }

      for (int i = 0; i < nbOuvElem; i++) {
        _destInfo.put(TrResource.getS("Ouvrage �l�mentaire {0}", CtuluLibString.getString(i + 1)), ouvSelected
            .getOuvrageElementaire(i).getType().getName());
      }
    }
  }

  public static String getYStr() {
    return " Y";
  }

  /**
   * @param _idxArete l'indice de l'arete demande
   * @param _destSeg le segment a initialiser
   */
  public final void getArete(final int _idxArete, final GrSegment _destSeg) {
    final EfSegment segi = getG().getArete(_idxArete);
    int idx = segi.getPt1Idx();
    _destSeg.o_.setCoordonnees(getG().getPtX(idx), getG().getPtY(idx), getG().getPtZ(idx));
    idx = segi.getPt2Idx();
    _destSeg.e_.setCoordonnees(getG().getPtX(idx), getG().getPtY(idx), getG().getPtZ(idx));
  }

  /**
   * @param _idxElt l'indice de l'�l�ment pour lequel le centre est demande
   * @param _destPoint le point a modifier
   */
  public final void getCentreElt(final int _idxElt, final Coordinate _destPoint) {
    getG().getMoyCentreElement(_idxElt, _destPoint);
  }

  @Override
  public final GrBoite getDomaine() {
    return MvGridLayerGroup.getDomaine(getG());
  }

  /**
   * @param _destPolyg le polygone a initialiser avec l'element selectionne
   * @param _idxElt l'indice de l'element
   * @return true
   */
  public final void getElt(final GrPolygone _destPolyg, final int _idxElt) {
    MvElementModelDefault.initGrPolygone(getG(), getG().getElement(_idxElt), _destPolyg);
  }

  public final EfElementVolume getElt(final int _idxElt) {
    return getG().getEltVolume(_idxElt);
  }

  /**
   * @return le nombre d'ouvrage
   */
  public int getNbOuvrage() {
    return mng_.getNbOuvrage();
  }

  @Override
  public final int getNombre() {
    return this.modeElt_ ? getG().getEltNb() : getNbOuvrage();
  }

  @Override
  public final Object getObject(final int _ind) {
    return null;
  }

  /**
   * @param _idxOuvrage l'indice de l'ouvrage
   * @return l'ouvrage i
   */
  public final H2dRubarOuvrage getOuvrage(final int _idxOuvrage) {
    return mng_.getRubarOuvrage(_idxOuvrage);
  }

  /**
   * @return tous les ouvrages
   */
  public final H2dRubarOuvrageI[] getOuvrages() {
    final H2dRubarOuvrageI[] ouvs = new H2dRubarOuvrage[getNbOuvrage()];
    for (int i = 0; i < ouvs.length; i++) {
      ouvs[i] = getOuvrage(i);
    }
    return ouvs;
  }

  public final BuTable getOuvrageTable() {
    final BuTable table = new CtuluTable();
    final CtuluCellTextRenderer doubleRender = new OuvrageCellRenderer();
    table.setModel(new OuvTableModel());
    final TableColumnModel colModel = table.getColumnModel();
    for (int i = 5; i < colModel.getColumnCount(); i++) {
      if (i != 7 && i != 10 && i != 13) {
        colModel.getColumn(i).setCellRenderer(doubleRender);
      }
    }
    return table;
  }

  public final TrRubarInfoSenderDelegate getSender() {
    return (TrRubarInfoSenderDelegate) sender_;
  }

  public void initListener(final TrRubarOuvrageLayer _ouvLayer) {
    mng_.addListener(_ouvLayer);
  }

  /**
   * @param _idxElt l'indice de l'element
   * @param _ptX x
   * @param _ptY y
   * @return true si l'element _idxElt contient _x,_y
   */
  public final boolean isEltContainedXY(final int _idxElt, final double _ptX, final double _ptY) {
    return getG().getElement(_idxElt).contientXY(_ptX, _ptY, getG());
  }

  /**
   * @return true si pas d'ouvrage
   */
  public final boolean isEmpty() {
    return mng_.isEmpty();
  }

  public boolean isInitialized() {
    return mng_.isInitialized();
  }

  public final boolean isModeElt() {
    return modeElt_;
  }

  /**
   * @param _idxTrie les indices des aretes dessin�es
   * @return true si la selection permet de creer un ouvrage.
   */
  public boolean isSelectionValide(final int[] _idxTrie) {
    return mng_.isSelectionValide(_idxTrie);
  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  public final int nbPoints(final int _idxElt) {
    return getG().getElement(_idxElt).getPtNb();
  }

  @Override
  public final void polygone(final GrPolygone _destPolyg, final int _idxElt, final boolean _force) {
    getElt(_destPolyg, _idxElt);
  }

  public final void setModeElt(final boolean _modeElt) {
    modeElt_ = _modeElt;
  }

  public String getMailleAval() {
    return TrResource.getS("Maille aval");
  }

  public String getAreteAval() {
    return TrResource.getS("Ar�te aval");
  }

  public String getXStr() {
    return " X";
  }

  public static String getAreteAmont() {
    return TrResource.getS("Ar�te amont");
  }

  protected static String getMailleAmontStr() {
    return TrResource.getS("Maille amont");
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return new TrRubarOuvrageLayerExpressionFinder(this);
  }

  /**
   * @param downstreamMesh if true return indices of works with downstream mesh. if false, without
   */
  public int[] getWorksIdxWithDownstreamMesh(boolean downstreamMesh) {
    TIntArrayList res = new TIntArrayList();
    int nbWork = getNbOuvrage();
    for (int i = 0; i < nbWork; i++) {

      if (downstreamMesh && getOuvrage(i).isElt2Set()) {
        res.add(i);
      }
      if (!downstreamMesh && !getOuvrage(i).isElt2Set()) {
        res.add(i);
      }
    }
    return res.toNativeArray();
  }

  /**
   * @param rubarRef the ref to look for
   */
  public int[] getWorksIdxWithRubarRef(int rubarRef) {
    TIntArrayList res = new TIntArrayList();
    int nbWork = getNbOuvrage();
    for (int i = 0; i < nbWork; i++) {
      if (getOuvrage(i).getRubarRef() == rubarRef) {
        res.add(i);
      }
    }
    return res.toNativeArray();
  }
}
