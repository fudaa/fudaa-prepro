/*
 * @creation 7 d�c. 2005
 *
 * @modification $Date: 2007-06-28 09:28:18 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.interpolation.bilinear.InterpolationBilinearSupportSorted;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.interpolation.EfInterpolationGridSupportAdapter;
import org.fudaa.dodico.ef.operation.EfIndexHelper;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreatedDefault;
import org.fudaa.fudaa.tr.post.persist.TrPostSourceProjectedPersistReplay;
import org.fudaa.fudaa.tr.post.persist.TrPostSourceReplayPersist;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: TrPostSourceProjected.java,v 1.14 2007-06-28 09:28:18 deniger Exp $
 */
public class TrPostSourceProjected extends TrPostSourceListVar implements TrPostSourceBuilt {
  final double[] destTimeStep_;
  final boolean isGridEquals_;
  final TrPostSourceProjectedPersistReplay replay;
  final TrPostSource srcInit_;
  final EfGridInterface target_;
  double eps_ = 1E-5;
  /**
   * De la taille des points dest. Pour chaque point dest donne les points sources a utiliser pour interpoler la valeur. Si null, c'est que c'est
   * identique.
   */
  int[][] nearestPointByNode;
  /**
   * De la taille des elements dest. Pour chaque point dest donne les points sources a utiliser pour interpoler la valeur. Si null, c'est que c'est
   * identique.
   */
  int[][] nearestMeshesByMesh;
  /**
   * De la taille des points dest. Pour chaque point dest donne l'�l�ment source contenant le point dest
   */
  int[] idxMeshesContainingNode;
  /**
   * De la taille des elements dest. Pour chaque point dest donne l'�l�ment source contenant le point dest
   */
  int[] idxMeshesContainingMesh;
  /**
   * De la taille du nombre de pas de temps. Pour chaque pas de temps cible, donne les indices des pas temps src min et max. Si null, c'est que c'est
   * identique.
   */
  int[][] idxTime_;

  public TrPostSourceProjected(final TrPostSource _srcInit, final EfGridInterface _grid, final double[] _destTimeStep,
                               final boolean _isGridEquals, final TrPostCommonImplementation _impl, String gridId) {
    this(_srcInit, _grid, _destTimeStep, _isGridEquals, _impl, createReplay(_srcInit.getId(), gridId,
        _isGridEquals));
  }

  public TrPostSourceProjected(final TrPostSource _srcInit, final EfGridInterface _grid, final double[] _destTimeStep,
                               final boolean _isGridEquals, final TrPostCommonImplementation _impl,
                               TrPostSourceProjectedPersistReplay replay) {
    super(_impl == null ? "?" : _impl.getProject().getSources().getShortNameOfSource(_srcInit), _grid,
        _destTimeStep == null ? _srcInit.getTime().getInitTimeSteps() : _destTimeStep,
        _srcInit.getAvailableVar(), _impl);
    destTimeStep_ = _destTimeStep;
    srcInit_ = _srcInit;
    isGridEquals_ = _isGridEquals;
    target_ = _grid;
    this.replay = replay;
  }

  private static TrPostSourceProjectedPersistReplay createReplay(String refSourceId, String destSourceId, boolean gridEquals) {
    return new TrPostSourceProjectedPersistReplay(refSourceId, destSourceId, gridEquals);
  }

  @Override
  public boolean isElementVar(CtuluVariable _idxVar) {
    return srcInit_.isElementVar(_idxVar);
  }

  @Override
  public void buildDefaultVectors() {
    varCreateData_ = new HashMap();
    if (variable_ == null) {
      return;
    }
    final int nb = variable_.length;
    final List<TrPostFlecheContent> fleches = new ArrayList<TrPostFlecheContent>(nb);
    for (int i = 0; i < nb; i++) {
      final TrPostFlecheContent fleche = srcInit_.getFlecheContent(variable_[i]);
      if (fleche != null) {
        // on duplique les fleches pour qu'elles utilisent le nombre de noeuds/elements de la cible et qu'elles
        // n'utilisent pas le cache
        // de la source initiale
        final TrPostFlecheContent flecheCopy = fleche.duplicate(this);
        varCreateData_.put(variable_[i], flecheCopy);
        fleches.add(flecheCopy);
      }
    }
    fleches_ = new TrPostFlecheContent[fleches.size()];
    fleches.toArray(fleches_);
    Arrays.sort(fleches_, TrPostDataCreatedDefault.NAME_COMPARATOR);
  }

  @Override
  public boolean containsElementVar() {
    return srcInit_.containsElementVar();
  }

  @Override
  public String getFormatedTitle() {
    return TrResource.getS("Projet {0} projet�", srcInit_.getTitle());
  }

  public TrPostInspectorReader createWatcher(final TrPostProjet _proj, final boolean _auto) {
    return null;
  }

  @Override
  public Collection<File> getFiles() {
    return this.srcInit_.getFiles();
  }

  @Override
  public EfData getInitData(final H2dVariableType dest, final int _timeIdx) {
    try {
      // pas de temps identique
      if (idxTime_ == null) {
        return getInitDataWithTimeSrc(dest, _timeIdx);
      }
      final int i1 = idxTime_[_timeIdx][0];
      final int i2 = idxTime_[_timeIdx][1];
      // pas de temps apres et avant identique
      if (i1 == i2) {
        return getInitDataWithTimeSrc(dest, i1);
      }
      // interpolation lin�aire
      final EfData data1 = getInitDataWithTimeSrc(dest, i1);
      final EfData data2 = getInitDataWithTimeSrc(dest, i2);
      final double t1 = srcInit_.getTime().getInitTimeStep(i1);
      final double t2 = srcInit_.getTime().getInitTimeStep(i2);
      final double coeff = (destTimeStep_[_timeIdx] - t1) / (t2 - t1);
      final double[] res = new double[data1.getSize()];
      for (int i = res.length - 1; i >= 0; i--) {
        final double v1 = data1.getValue(i);
        res[i] = coeff * (data2.getValue(i) - v1) + v1;
      }
      return data1.isElementData() ? (EfData) new EfDataElement(res) : new EfDataNode(res);
    } catch (final IOException _e) {
      FuLog.warning(_e);
    }
    return null;
  }

  @Override
  public double getInitData(final H2dVariableType dest, final int _timeIdx, final int _ptIdx) throws IOException {

    try {
      // pas de temps identique
      if (idxTime_ == null) {
        return getInitDataWithTimeSrc(dest, _timeIdx, _ptIdx);
      }
      final int i1 = idxTime_[_timeIdx][0];
      final int i2 = idxTime_[_timeIdx][1];
      // pas de temps apres et avant identique
      if (i1 == i2) {
        return getInitDataWithTimeSrc(dest, i1, _ptIdx);
      }
      // interpolation lin�aire
      final double data1 = getInitDataWithTimeSrc(dest, i1, _ptIdx);
      final double data2 = getInitDataWithTimeSrc(dest, i2, _ptIdx);
      final double t1 = srcInit_.getTime().getInitTimeStep(i1);
      final double t2 = srcInit_.getTime().getInitTimeStep(i2);
      final double coeff = (destTimeStep_[_timeIdx] - t1) / (t2 - t1);
      return coeff * (data2 - data1) + data1;
    } catch (final IOException _e) {
      FuLog.warning(_e);
    }
    return 0;
  }

  /**
   * Recupere les valeurs sur les noeuds cibles.
   *
   * @param _varIdx la variable
   * @param _srcTimeIdx l'indice du pas de temps source
   * @return les valeurs (sur les elements si la cible est rubar)
   */
  private EfData getInitDataWithTimeSrc(final H2dVariableType _varIdx, final int _srcTimeIdx) throws IOException {

    final double[] values = getPtOrEltInitDataWithTimeSrc(_varIdx, _srcTimeIdx);
    return isElementVar(_varIdx) ? (EfData) new EfDataElement(values) : new EfDataNode(values);
  }

  /**
   * Recupere la valeur en un point ou element (si la cible est Rubar) cible.
   *
   * @param _varIdx la variable
   * @param _srcTimeIdx l'indice du pas de temps source
   * @param _ptOrEltIdx indice du noeud ou element du maillage cible
   * @return la valeur
   */
  private double getInitDataWithTimeSrc(final H2dVariableType _varIdx, final int _srcTimeIdx, final int _ptOrEltIdx)
      throws IOException {

    return getSrcInitDataWithTimeSrc(_varIdx, _srcTimeIdx, _ptOrEltIdx);
  }

  private int[] getNearestNodes(final int idxNode) {
    return nearestPointByNode == null ? new int[]{idxNode} : nearestPointByNode[idxNode];
  }

  private int[] getNearestMeshes(final int idxMeshes) {
    return nearestMeshesByMesh == null ? new int[]{idxMeshes} : nearestMeshesByMesh[idxMeshes];
  }

  private int getEnglobingMeshForNode(final int idxNode) {
    if (idxMeshesContainingNode == null || idxNode < 0 || idxNode >= idxMeshesContainingNode.length) {
      return -1;
    }
    return idxMeshesContainingNode[idxNode];
  }

  private int getEnglobingMeshForMesh(final int idxMesh) {
    if (idxMeshesContainingMesh == null || idxMesh < 0 || idxMesh >= idxMeshesContainingMesh.length) {
      return -1;
    }
    return idxMeshesContainingMesh[idxMesh];
  }

  private int[] getPt(final int[] _srcIdx, final double _xDest, final double _yDest, boolean useMesh) {
    int nb = 0;
    final int[] res = new int[4];
    final EfGridInterface grid = srcInit_.getGrid();
    for (int i = 0; i < 4; i++) {
      final int idx = _srcIdx[i];
      if (idx >= 0) {
        final double ptX = useMesh ? grid.getMoyCentreXElement(idx) : grid.getPtX(idx);
        final double ptY = useMesh ? grid.getMoyCentreYElement(idx) : grid.getPtY(idx);
        if (CtuluLibGeometrie.getD2(_xDest, _yDest, ptX, ptY) < eps_) {
          return new int[]{idx};
        }
        res[nb++] = idx;
      }
    }
    if (nb == 4) {
      return res;
    }
    final int[] resFinal = new int[nb];
    System.arraycopy(res, 0, resFinal, 0, nb);
    return resFinal;
  }

  /**
   * Permet de r�cuperer les valeurs sur les points ou les element du maillage cible. Si la source est rubar, la valeur represente la moyenne sur les
   * elements voisins.
   *
   * @param _varIdx
   * @param _srcTimeIdx l'indice du pas de temps dans la source
   * @return la valeur au point demande.
   */
  private double[] getPtOrEltInitDataWithTimeSrc(final H2dVariableType _varIdx, final int _srcTimeIdx)
      throws IOException {

    EfData data = srcInit_.getData(_varIdx, _srcTimeIdx);
    if (data == null) {
      throw new IOException(DodicoLib.getS("Fichier invalide"));
    }
    boolean isMesh = data.isElementData();
    final int nbDestSupport = isMesh ? target_.getEltNb() : target_.getPtsNb();
    final double[] res = new double[nbDestSupport];

    final EfGridInterface srcGrid = srcInit_.getGrid();
    for (int idxNodeOrMesh = 0; idxNodeOrMesh < nbDestSupport; idxNodeOrMesh++) {
      // arrive dans le cas representation par element -> Representation par noeud
      final int[] nearestPtOrMeshes = isMesh ? getNearestMeshes(idxNodeOrMesh) : getNearestNodes(idxNodeOrMesh);
      final int enclosingMesh = isMesh ? getEnglobingMeshForMesh(idxNodeOrMesh) : getEnglobingMeshForNode(idxNodeOrMesh);
      if (nearestPtOrMeshes.length == 1) {
        res[idxNodeOrMesh] = data.getValue(nearestPtOrMeshes[0]);
      } else if (enclosingMesh >= 0) {
        if (isMesh) {
          res[idxNodeOrMesh] = data.getValue(enclosingMesh);
        } else {
          final double ptX = target_.getPtX(idxNodeOrMesh);
          final double ptY = target_.getPtY(idxNodeOrMesh);
          res[idxNodeOrMesh] = srcInit_.getInterpolator().interpolateDangerous(enclosingMesh, ptX, ptY, data, srcGrid);
        }
      } else {
        double num = 0;
        double den = 0;
        final double ptX = isMesh ? target_.getMoyCentreXElement(idxNodeOrMesh) : target_.getPtX(idxNodeOrMesh);
        final double ptY = isMesh ? target_.getMoyCentreYElement(idxNodeOrMesh) : target_.getPtY(idxNodeOrMesh);
        for (int j = nearestPtOrMeshes.length - 1; j >= 0; j--) {
          // temp est l'indice du point de reference
          final int temp = nearestPtOrMeshes[j];
          final double xref = isMesh ? srcGrid.getMoyCentreXElement(temp) : srcGrid.getPtX(temp);
          final double yref = isMesh ? srcGrid.getMoyCentreYElement(temp) : srcGrid.getPtY(temp);

          final double dist = CtuluLibGeometrie.getD2(xref, yref, ptX, ptY);
          num += data.getValue(temp) / dist;
          den += 1d / dist;
        }
        if (den != 0) {
          res[idxNodeOrMesh] = num / den;
        }
      }
    }
    return res;
  }

  @Override
  public TrPostSourceReplayPersist getReplay() {
    replay.setId(getId());
    return replay;
  }

  /**
   * Permet de r�cuperer une valeur en un point donn� du maillage cible. Si la source est rubar, la valeur represente la moyenne sur les elements
   * voisins.
   *
   * @param _varIdx
   * @param _srcTimeIdx l'indice du pas de temps dans la source
   * @param _ptOrEltIdx l'indice du point cible
   * @return la valeur au point demande.
   */
  private double getSrcInitDataWithTimeSrc(final H2dVariableType _varIdx, final int _srcTimeIdx, final int _ptOrEltIdx)
      throws IOException {
    boolean isMesh = srcInit_.isElementVar(_varIdx);
    final int[] nearestPtOrMeshes = isMesh ? getNearestMeshes(_ptOrEltIdx) : getNearestNodes(_ptOrEltIdx);
    final int enclosingMesh = isMesh ? getEnglobingMeshForMesh(_ptOrEltIdx) : getEnglobingMeshForNode(_ptOrEltIdx);
    EfData data = srcInit_.getData(_varIdx, _srcTimeIdx);
    if (nearestPtOrMeshes.length == 1) {
      return data.getValue(nearestPtOrMeshes[0]);
    } else if (enclosingMesh >= 0) {
      if (isMesh) {
        return data.getValue(enclosingMesh);
      }
      final double ptX = target_.getPtX(_ptOrEltIdx);
      final double ptY = target_.getPtY(_ptOrEltIdx);
      return srcInit_.getInterpolator().interpolateDangerous(enclosingMesh, ptX, ptY, data, srcInit_.getGrid());
    }
    double num = 0;
    double den = 0;
    final double ptX = isMesh ? target_.getMoyCentreXElement(_ptOrEltIdx) : target_.getPtX(_ptOrEltIdx);
    final double ptY = isMesh ? target_.getMoyCentreYElement(_ptOrEltIdx) : target_.getPtY(_ptOrEltIdx);
    final EfGridInterface srcGrid = srcInit_.getGrid();
    for (int j = nearestPtOrMeshes.length - 1; j >= 0; j--) {
      // temp est l'indice du point de reference
      final int temp = nearestPtOrMeshes[j];

      final double xref = isMesh ? srcGrid.getMoyCentreXElement(temp) : srcGrid.getPtX(temp);
      final double yref = isMesh ? srcGrid.getMoyCentreYElement(temp) : srcGrid.getPtY(temp);

      final double dist = CtuluLibGeometrie.getD2(xref, yref, ptX, ptY);
      num += data.getValue(temp) / dist;
      den += 1d / dist;
    }
    if (den != 0) {
      return num / den;
    }
    return 0;
  }

  @Override
  public List<TrPostSource> getUsedSources() {
    return Arrays.asList(srcInit_);
  }

  /**
   * Cette methode est utilisee pour calcul des points le plus proches. Pour chaque point de la cible, on calcul les 4 plus proches dans la source
   * initiale
   */
  @Override
  public boolean openDatas(final ProgressionInterface progression, final CtuluAnalyze _analyze, final CtuluUI _ui) {
    if (!isGridEquals_) {
      final InterpolationBilinearSupportSorted interpolationOnPoints = InterpolationBilinearSupportSorted.buildSortedSrc(
          new EfInterpolationGridSupportAdapter.Node(srcInit_.getGrid()),
          progression);
      srcInit_.getGrid().createIndexRegular(progression);
      final ProgressionUpdater update = new ProgressionUpdater(progression);
      update.majProgessionStateOnly(TrResource.getS("Recherche des points supports"));

      // pour chaque noeud de dest, on calcul les 4 noeuds les plus proches
      final int ptsNb = target_.getPtsNb();
      nearestPointByNode = new int[ptsNb][];
      idxMeshesContainingNode = new int[ptsNb];
      final int[] tmp = new int[4];
      update.setValue(5, ptsNb);
      for (int i = 0; i < ptsNb; i++) {
        final double xdest = target_.getPtX(i);
        final double ydest = target_.getPtY(i);
        interpolationOnPoints.getQuadrantIdx(xdest, ydest, tmp);
        nearestPointByNode[i] = getPt(tmp, xdest, ydest, false);
        idxMeshesContainingNode[i] = EfIndexHelper.getElementEnglobant(srcInit_.getGrid(), xdest, ydest, null);
        update.majAvancement();
      }

      if (srcInit_.containsElementVar()) {
        final InterpolationBilinearSupportSorted interpolationOnMeshesPoints = InterpolationBilinearSupportSorted.buildSortedSrc(new EfInterpolationGridSupportAdapter.Mesh(
            srcInit_.getGrid()), progression);
        final int eltNb = target_.getEltNb();
        nearestMeshesByMesh = new int[eltNb][];
        idxMeshesContainingMesh = new int[eltNb];

        update.setValue(5, eltNb);
        for (int i = 0; i < eltNb; i++) {
          final double xdest = target_.getMoyCentreXElement(i);
          final double ydest = target_.getMoyCentreYElement(i);
          interpolationOnMeshesPoints.getQuadrantIdx(xdest, ydest, tmp);
          nearestMeshesByMesh[i] = getPt(tmp, xdest, ydest, true);
          idxMeshesContainingMesh[i] = EfIndexHelper.getElementEnglobant(srcInit_.getGrid(), xdest, ydest, null);
          update.majAvancement();
        }
      }
    }
    if (destTimeStep_ != null
        && !CtuluLibArray.isDoubleEquals(destTimeStep_, srcInit_.getTime().getInitTimeSteps(), 1E-3)) {
      idxTime_ = new int[destTimeStep_.length][2];
      final double[] srcTimeStep = srcInit_.getTime().getInitTimeSteps();
      final int nbSrcTimeStep = srcTimeStep.length;
      if (nbSrcTimeStep > 0) {
        for (int i = 0; i < destTimeStep_.length; i++) {
          final double time = destTimeStep_[i];
          if (time < srcTimeStep[0]) {
            idxTime_[i][0] = 0;
            idxTime_[i][1] = 0;
          } else if (time > srcTimeStep[nbSrcTimeStep - 1]) {
            idxTime_[i][0] = nbSrcTimeStep - 1;
            idxTime_[i][1] = nbSrcTimeStep - 1;
          } else {
            final int idx = Arrays.binarySearch(srcTimeStep, time);
            if (idx >= 0) {
              idxTime_[i][0] = idx;
              idxTime_[i][1] = idx;
            } else {
              idxTime_[i][0] = -idx - 2;
              idxTime_[i][1] = idxTime_[i][0] + 1;
            }
          }
        }
      }
    }
    return true;
  }
}
