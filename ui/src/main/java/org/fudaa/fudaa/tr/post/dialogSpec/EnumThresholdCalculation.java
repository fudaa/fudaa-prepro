/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.dialogSpec;

/**
 *
 * @author Frederic Deniger
 */
public enum EnumThresholdCalculation {

  POSITIVE("POSITIVE"), NEGATIVE("NEGATIVE"), ALL("ALL");
  final String persistName;

  private EnumThresholdCalculation(String persistName) {
    this.persistName = persistName;
  }

  public String getPersistName() {
    return persistName;
  }

  public static EnumThresholdCalculation getFromSaved(String persist) {
    if (POSITIVE.persistName.equals(persist)) {
      return POSITIVE;
    }
    if (NEGATIVE.persistName.equals(persist)) {
      return NEGATIVE;
    }
    return ALL;

  }
}
