package org.fudaa.fudaa.tr.post.persist;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourceOneTimeStep;

public class TrPostSourceOneTimeStepReplayPersist implements TrPostSourceReplayPersist {
  final String id;
  final String srcId;
  final int timeStepIdx;

  /**
   * @param srcId
   * @param idx
   */
  public TrPostSourceOneTimeStepReplayPersist(final String idThisSource, String srcId, int idx) {
    super();
    this.id = idThisSource;
    this.srcId = srcId;
    this.timeStepIdx = idx;
  }

  public String getSrcId() {
    return srcId;
  }

  public String getId() {
    return id;
  }

  @Override
  public TrPostSource replay(TrPostProjet projet, CtuluAnalyze log) {
    return new TrPostSourceOneTimeStep(projet.getSources().getSource(srcId), timeStepIdx, projet.getImpl());
  }

}
