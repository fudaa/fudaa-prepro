package org.fudaa.fudaa.tr.post;

import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author fred deniger
 * @version $Id: TrPostImportProjetTableModel.java,v 1.2 2006-10-19 13:55:17 deniger Exp $
 */
public final class TrPostImportProjetTableModel extends AbstractTableModel {
  private final String[] first_;
  private final int nb_;
  private final Boolean[] delete_;
  private final String[] path_;
  private final String[] title_;

  public TrPostImportProjetTableModel(final String[] _first, final Boolean[] _delete, final String[] _path,
      final String[] _title) {
    first_ = _first;
    nb_ = first_ == null ? 0 : first_.length;
    delete_ = _delete;
    path_ = _path;
    title_ = _title;
  }

  @Override
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {
    if (_columnIndex == 0) { return delete_[_rowIndex]; }
    if (_columnIndex == 1) { return title_[_rowIndex]; }
    if (_columnIndex == 2) { return path_[_rowIndex]; }
    if (_columnIndex == 3) { return first_[_rowIndex]; }
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public int getRowCount() {
    return nb_;
  }

  @Override
  public int getColumnCount() {
    return 4;
  }

  @Override
  public Class getColumnClass(final int _columnIndex) {
    return _columnIndex == 0 ? Boolean.class : String.class;
  }

  @Override
  public String getColumnName(final int _columnIndex) {
    if (_columnIndex == 0) { return TrResource.getS("Supprimer l'import"); }
    if (_columnIndex == 1) { return TrResource.getS("Nom du projet"); }
    if (_columnIndex == 2) { return TrResource.getS("Le chemin du fichier"); }
    if (_columnIndex == 3) { return TrResource.getS("Les variables"); }
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    return _columnIndex == 0;
  }

  @Override
  public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
    if (_columnIndex == 0) {
      delete_[_rowIndex] = (Boolean) _value;
    }
  }
}