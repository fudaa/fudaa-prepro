/*
 * @creation 20 oct. 06
 * @modification $Date: 2007-06-29 15:09:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuResource;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.ctulu.table.CtuluTableCsvWriter;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ctulu.table.CtuluWriter;
import org.fudaa.dodico.mesure.EvolutionFileFormat;
import org.fudaa.dodico.mesure.EvolutionFileFormatVersion;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.reflux.io.CLTransitoireFileFormat;
import org.fudaa.dodico.reflux.io.CrbFileFormat;
import org.fudaa.dodico.rubar.io.RubarAPPFileFormat;
import org.fudaa.dodico.rubar.io.RubarCLIFileFormat;
import org.fudaa.dodico.rubar.io.RubarTARFileFormat;
import org.fudaa.dodico.telemac.io.ScopeGENEFileFormat;
import org.fudaa.dodico.telemac.io.ScopeSFileFormat;
import org.fudaa.dodico.telemac.io.ScopeTFileFormat;
import org.fudaa.dodico.telemac.io.TelemacLiquideFileFormat;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.courbe.EGExporter;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;
import org.fudaa.fudaa.commun.impl.FudaaImportCsvPanel;
import org.fudaa.fudaa.tr.post.profile.MvProfileTreeModel;

import javax.swing.filechooser.FileFilter;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author fred deniger
 * @version $Id: FudaaCourbeImporter.java,v 1.8 2007-06-29 15:09:42 deniger Exp $
 */
public final class TrCourbeImporter {
  private TrCourbeImporter() {
  }

  public static void importTo(final File _initFile, final Target _target, final EvolutionFileFormatVersion _version,
                              final FudaaCommonImplementation _ui, final CtuluCommandManager _mng) {
    if (_target == null) {
      return;
    }
    final FileFormat ft = _version.getFileFormat();
    File f = _initFile;
    Map m = null;
    if (ft.getID().equals(EvolutionFileFormat.getInstance().getID())) {
      final FudaaImportCsvPanel pn = new FudaaImportCsvPanel(_initFile, true);
      if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_ui.getFrame(), FudaaLib
          .getS("Importer des courbes depuis fichier csv")))) { //To import curves from a csv file
        f = pn.getFile();
        m = pn.buildOption();
      } else {
        return;
      }
    }
    if (f != null) {
      final File fFinal = f;
      final Map options = m;
      new CtuluTaskOperationGUI(_ui, FudaaLib.getS("Import") + CtuluLibString.ESPACE
          + EvolutionFileFormat.getInstance().getName()) {
        @Override
        public void act() {
          final ProgressionInterface prog = getMainStateReceiver();
          _ui.setMainMessage(FudaaLib.getS("Chargement des fichiers du projet"));
          _ui.setMainProgression(50);
          _ui.setMainMessage(CtuluLib.getS("Lecture") + CtuluLibString.ESPACE + fFinal.getAbsolutePath());
          final CtuluIOOperationSynthese op = _version.readEvolutions(fFinal, prog, options);
          if (!_ui.manageErrorOperationAndIsFatal(op)) {
            _ui.setMainProgression(70);
            _ui.setMainMessage(BuResource.BU.getString("Mise � jour..."));
            if (op.getSource() == null) {
              _ui.warn(FudaaLib.getS("Import"), FudaaLib.getS("Pas de courbes � importer"));
            } else {
              if (op.getSource() instanceof EvolutionReguliereInterface[]) {
                _target.importCourbes((EvolutionReguliereInterface[]) op.getSource(), _mng, prog);
              }
            }
          }
          _ui.unsetMainMessage();
          _ui.unsetMainProgression();
        }
      }.start();
    }
  }

  public interface Target {
    void importCourbes(EvolutionReguliereInterface[] _crb, CtuluCommandManager _mng, ProgressionInterface _prog);

    /**
     * a utiliser pour les formats scope
     * To use for the scope formats
     */
    public boolean isSpatial();
  }

  public static class ExportAction extends EbliActionSimple {
    final EGGraphe graphe_;
    transient final CtuluUI impl_;

    public ExportAction(final EGGraphe _graphe, final CtuluUI _impl) {
      super(BuResource.BU.getString("Exporter"), BuResource.BU.getIcon("EXPORTER"), "EXPORTER");
      graphe_ = _graphe;
      impl_ = _impl;
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      EGExporter.startExport(graphe_, impl_, getFileFormatForExport(graphe_));
    }
  }

  public static EbliActionInterface[] getImportExportAction(final TrCourbeImporter.Target _target,
                                                            final EGGraphe _graphe, final FudaaCommonImplementation _imp) {
    return new EbliActionInterface[]{new ImportAction(_target, _imp, _graphe.getCmd()),
        new ExportAction(_graphe, _imp)};
  }

  public static class ImportAction extends EbliActionSimple {
    transient final TrCourbeImporter.Target target_;
    transient final FudaaCommonImplementation ui_;
    transient final CtuluCommandManager mng_;

    public ImportAction(final TrCourbeImporter.Target _target, final FudaaCommonImplementation _ui,
                        final CtuluCommandManager _mng) {
      super(BuResource.BU.getString("Importer"), BuResource.BU.getIcon("IMPORTER"), "IMPORTER");
      target_ = _target;
      ui_ = _ui;
      mng_ = _mng;
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final FileFormat[] ft = getFileFormatForImportEvol(target_);
      Arrays.sort(ft);
      final BuFileFilter[] filters = FileFormat.createFilters(ft);
      final CtuluFileChooser fileChooser = FudaaGuiLib.getFileChooser(BuResource.BU.getString("Importer"), filters,
          null);
      fileChooser.setAcceptAllFileFilterUsed(false);
      for (int i = filters.length - 1; i >= 0; i--) {
        if (ft[i].getID() == EvolutionFileFormat.getInstance().getID()) {
          fileChooser.setFileFilter(filters[i]);
          break;
        }
      }
      final File initFile = FudaaGuiLib.chooseFile(CtuluLibSwing.getFrameAncestor(ui_.getParentComponent()), true,
          fileChooser);
      if (initFile == null) {
        return;
      }
      final FileFilter filter = fileChooser.getFileFilter();
      final int i = CtuluLibArray.getIndex(filter, filters);
      if (i < 0) {
        return;
      }
      importTo(initFile, target_, (EvolutionFileFormatVersion) ft[i].getLastVersionInstance(initFile), ui_, mng_);
    }
  }

  public static FileFormat[] getFileFormatForImportEvol(final TrCourbeImporter.Target target) {
    final ArrayList<FileFormat> r = new ArrayList(10);
    r.add(TelemacLiquideFileFormat.getInstance());
    r.add(EvolutionFileFormat.getInstance());
    r.add(CrbFileFormat.getInstance());
    r.add(CLTransitoireFileFormat.getInstance());
    r.add(RubarTARFileFormat.getInstance());
    r.add(new RubarAPPFileFormat());
    r.add(RubarCLIFileFormat.getInstance());
    if (target != null && target.isSpatial()) {
      r.add(ScopeSFileFormat.getInstance());
    } else if (target != null) {
      r.add(ScopeTFileFormat.getInstance());
    }
    r.add(ScopeGENEFileFormat.getInstance());

    return r.toArray(new FileFormat[0]);
  }

  /**
   * Methode statique qui retourne les writer possible pour l exportation des
   * courbes
   * Static methods which returns the possible writer for the exportation of the curves.
   */
  public static List<CtuluWriter> getFileFormatForExport(EGGraphe graphe) {
    final ArrayList<CtuluWriter> liste = new ArrayList<CtuluWriter>(10);

    liste.add(new CtuluTableExcelWriter());
    liste.add(new CtuluTableCsvWriter());

    // --ajout des formats scope --//
    // add scope formats
    if (graphe.getModel() instanceof MvProfileTreeModel) {
      liste.add(new TrWriterScopeS());
    } else {
      liste.add(new TrWriterScopeT());
    }

    liste.add(new TrWriterScopeGENE());
    return liste;
  }
}
