package org.fudaa.fudaa.tr.post;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluLibGenerator;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.actions.TrPostActionOpenSrc;
import org.fudaa.fudaa.tr.post.actions.TrPostActionRemoveSrc;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

/**
 * InternalFrame qui gere plusieurs projets simultan�ment.
 *
 * @author Adrien Hadoux
 */
public class TrPostProjetsManagerFille extends BuInternalFrame implements ListSelectionListener, Observer {
  /**
   * projet contenant la liste des sources.
   */
  final TrPostProjet projet_;
  /**
   * La liste des projet afffichage graphique.
   */
  public JXTreeTable listProjets_;
  TreeTableModelGraphe modelList_;
  BuButton boutonAjoutSrc_ = new BuButton("Ajouter un projet", BuResource.BU.getIcon("crystal_ouvrirprojet"));
  BuButton boutonEnleveSrc_ = new BuButton("Enlever un projet", BuResource.BU.getIcon("crystal_enlever"));
  BuButton boutonFermer = new BuButton(TrLib.getString("Fermer"), BuResource.BU.getIcon("crystal_non"));

  public TrPostProjetsManagerFille(final TrPostProjet _projet) {
    super();

    // -- recuperation du projet --//
    projet_ = _projet;

    // -- ajout de la fenetre comme observer du projet --//
    projet_.getObservable().addObserver(this);

    // --creation de la liste graphique des projets --//
    modelList_ = constructStructureModel();

    listProjets_ = new JXTreeTable(modelList_);

    listProjets_.setBorder(BorderFactory.createEtchedBorder());

    // -- listener du tree --//
    listProjets_.addMouseListener(new MouseAdapter() {
      @Override
      public void mousePressed(final MouseEvent e) {
        if (e.isPopupTrigger() && e.getClickCount() == 1) {
          affichePopupNode(e.getX(), e.getY());
        }
      }

      @Override
      public void mouseReleased(final MouseEvent e) {
        if (e.isPopupTrigger() && e.getClickCount() == 1) {
          affichePopupNode(e.getX(), e.getY());
        }
      }
    });

    setTitle(TrResource.getS("Gestion Multi-Sources"));
    setLayout(new BorderLayout());
    final JScrollPane listScroller = new JScrollPane(listProjets_);
    this.add(listScroller, BorderLayout.CENTER);

    final BuPanel operationPanel = new BuPanel(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER));
    this.add(operationPanel, BorderLayout.SOUTH);
    operationPanel.add(boutonFermer);
    operationPanel.add(boutonAjoutSrc_);
    operationPanel.add(boutonEnleveSrc_);

    // -- ajout du listener du bouton d ajout d'un element source
    this.boutonAjoutSrc_.setAction(new TrPostActionOpenSrc(projet_));
    boutonEnleveSrc_.setAction(new TrPostActionRemoveSrc(projet_));
    // boutonChangeSrc_.setAction(new TrPostActionChangeSrc(projet_));

    // -- creation du menu --//
    final BuMenu menu = new BuMenu();
    menu.setText("Gestion");
    menu.add(new TrPostActionOpenSrc(projet_));
    menu.add(new TrPostActionRemoveSrc(projet_));

    final BuMenuBar barre = new BuMenuBar();
    barre.add(menu);
    this.setJMenuBar(barre);

    // action fermer
    final EbliActionSimple fermer = new EbliActionSimple("Fermer", BuResource.BU.getIcon("crystal_non"), "Fermer") {
      @Override
      public void actionPerformed(final ActionEvent e) {
        TrPostProjetsManagerFille.this.setVisible(false);
      }
    };

    boutonFermer.setAction(fermer);
    menu.add(fermer);
  }

  /**
   * Affiche le popup correspondant au fichier cliqu�
   *
   * @param x
   * @param y
   */
  public void affichePopupNode(final int x, final int y) {
    // Get the tree element under the mouse
    final TreePath clickedElement = listProjets_.getPathForLocation(x, y);

    MutableTreeTableNode treeNode = null;
    if (clickedElement != null) {
      treeNode = (MutableTreeTableNode) clickedElement.getLastPathComponent();
    }

    if (treeNode != null) {

      // -- recuperation du node associe --//
      TrPostSource src = null;
      if (treeNode.getUserObject() instanceof TrPostSource) {
        src = (TrPostSource) treeNode.getUserObject();
      } else if (treeNode.getUserObject() instanceof File) {
        if (treeNode.getParent().getUserObject() instanceof TrPostSource) {
          src = (TrPostSource) treeNode.getParent()
              .getUserObject();
        }
      }
      if (src == null) {
        return;
      }

      // -- recuperation de la popup du node et ajout a l element du tree a al
      // bonne location--//
      JPopupMenu popup = null;

      popup = new JPopupMenu();

      int indexSource = projet_.getSources().getIndexOf(src);

      for (BuMenuItem item : this.projet_.getlisteSousMenuProjets().get(indexSource)) {
        if (item.getId() == null) {
          item.setId(CtuluLibGenerator.getInstance().deliverUniqueStringId());
        }
        popup.add(new BuMenuItem(item));
      }

      // -- affichage au bon endroit --//
      popup.show(listProjets_, x, y);
    }
  }

  @Override
  public void valueChanged(final ListSelectionEvent e) {
  }

  /**
   * Construit le model du tableau.
   */
  public TreeTableModelGraphe constructStructureModel() {
    DefaultMutableTreeTableNode root = new DefaultMutableTreeTableNode("Gestion multi-fichiers");

    for (int i = 0; i < projet_.getSources().getSrcSize(); i++) {
      TrPostSource src = projet_.getSource(i);

      DefaultMutableTreeTableNode nodeCalque = new DefaultMutableTreeTableNode(src);
      root.add(nodeCalque);

      // - ajout des fichiers associ�s --//
      for (File f : src.getFiles()) {
        nodeCalque.add(new DefaultMutableTreeTableNode(f));
      }
    }
    TreeTableModelGraphe model = new TreeTableModelGraphe(projet_);
    model.setRoot(root);

    return model;
  }

  public class TreeTableModelGraphe extends DefaultTreeTableModel {
    String[] titre_;
    TrPostProjet projet;

    @Override
    public Class<?> getColumnClass(final int columnIndex) {

      switch (columnIndex) {
        case 0:
          return Integer.class;
        case 1:
          return String.class;
      }
      return null;
    }

    // -- data correspondant au x donn� --//
    double[] dataY_ = new double[0];

    public TreeTableModelGraphe(TrPostProjet p) {
      String[] val = {TrLib.getString("Fichiers"), TrLib.getString("Date modification")};
      titre_ = val;
      projet = p;
    }

    @Override
    public int getColumnCount() {

      return titre_.length;
    }

    @Override
    public boolean isCellEditable(Object _node, int _column) {
      return false;
    }

    @Override
    public String getColumnName(int _columnIndex) {
      return titre_[_columnIndex];
    }

    public int getRowCount() {
      return projet.getSources().getSrcSize();
    }

    @Override
    public Object getValueAt(Object node, int column) {
      Object res = new DefaultMutableTreeTableNode("n/a");
      if (node instanceof DefaultMutableTreeTableNode) {
        DefaultMutableTreeTableNode defNode = (DefaultMutableTreeTableNode) node;
        if (defNode.getUserObject() instanceof TrPostSource) {
          TrPostSource src = (TrPostSource) defNode.getUserObject();
          String label = (getRoot().getIndex(defNode) + 1) + ". " + TrLib.formatName(src.getTitle());
          if (column == 0) {
            if (src.getMainFile() != null) {
              return label + " [" + src.getMainFile().getAbsolutePath() + "]";
            }
            return label;
          } else {
            return "";
          }
        } else if (defNode.getUserObject() instanceof File) {
          File fichier = (File) defNode.getUserObject();

          if (column == 0) {
            return fichier.getAbsolutePath();
          } else if (column == 1) {
            return new Date(fichier.lastModified());
          } else {
            return "";
          }
        } else {
          return "";
        }
      }
      return res;
    }

    public DefaultMutableTreeTableNode getTreeNode(int i) {
      return (DefaultMutableTreeTableNode) getRoot().getChildAt(i);
    }

    public void reload() {

      this.modelSupport.fireTreeStructureChanged(listProjets_.getPathForRow(0));
    }
  }

  @Override
  public void update(final Observable o, final Object arg) {

    // mise a jour de la liste
    if (listProjets_ == null) {
      return;
    }

    Runnable r = new Runnable() {
      @Override
      public void run() {
        listProjets_.setTreeTableModel(constructStructureModel());
        listProjets_.expandAll();
        listProjets_.clearSelection();
        TrPostProjetsManagerFille.this.revalidate();
      }
    };
    BuLib.invokeNowOrLater(r);
  }
}
