package org.fudaa.fudaa.tr.post.replay;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import org.locationtech.jts.geom.LineString;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.concurrent.Callable;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import org.fudaa.ctulu.gui.CtuluComboBoxModelAdapter;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostCourbeBilanModel;
import org.fudaa.fudaa.tr.post.TrPostCourbeTreeModel;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.post.profile.MvProfileCourbeModelInterface;

/**
 * Classe qui se charge d'afficher et rejouer les donn�es d'origine des courbes TR. C'set a dire les courbes spatiales et evolutions temporelles.
 *
 * @author Adrien Hadoux
 */
public class TrReplayerBilan extends AbstractTrReplayer {

  public TrReplayerBilan(TrPostSource source,
          final TrPostCommonImplementation implementation) {
    super(source, implementation);
  }

  private boolean replayBilanFor(final TrPostVisuPanel vue2d, final TrPostCourbeTreeModel tree, final TrPostCourbeBilanModel modele, int startTime, int endTime, String title) {
    final H2dVariableType newVariable = vue2d.getSelectedVarInCalqueActif();

    LineString initLine = modele.getInitLine();
    if (vue2d.isSelectionOkForEvolution()) {
      final ZCalqueAffichageDonnees calque = (ZCalqueAffichageDonnees) vue2d.getCalqueActif();
      int[] newPoints = calque.getLayerSelection().getSelectedIndex();

      final String lineDescription = " [P1"
              + format(source.getGrid().getPtX(newPoints[0]), source.getGrid().getPtY(newPoints[0]))
              + ";P"
              + initLine.getNumPoints()
              + "("
              + format(source.getGrid().getPtX(newPoints[newPoints.length - 1]), source.getGrid().getPtY(
              newPoints[newPoints.length - 1])) + "]";
      // -- on rejoue les donn�es avec les nouvelles valeurs --//
      if (this.ecraser_.isSelected()) {
        implementation.message(
                TrResource.getS("La courbe actuelle va �tre recalcul�e pour les donn�es suivantes:\n Variable: {0}\n Ligne: {1}", newVariable.getName(), lineDescription));
      } else {
        String msg = TrResource.getS("Une nouvelle courbe de bilan va �tre recalcul�e pour les donn�es suivantes:\n Variable: {0}\n Ligne: {1}.\nLa courbe actuelle ne sera pas �cras�e.", newVariable.getName(), lineDescription);
        implementation.message(msg);
      }

      // -- on rejoue les donn�es avec ou sans ecrasement --//
      tree.replayBilans(implementation, vue2d, modele, source, newPoints, vue2d.getCmdMng(), implementation.getMainProgression(), newVariable, startTime, endTime, this.ecraser_.isSelected(), title);

      return true;

    } else if (vue2d.isSelectionOkForEvolutionSonde()) {

      // -- on recupere la ligne avec la ligne formee des sondes --//
      final List<GrPoint> listePoints = vue2d.getLigneBriseeFromSonde();
      if (listePoints != null && listePoints.size() > 1) {
        final String lineDescription = " [P1(" + format(listePoints.get(0).x_) + ";" + format(listePoints.get(0).y_) + ")" + ";"
                + "P" + listePoints.size() + "(" + format(listePoints.get(listePoints.size() - 1).x_) + ";"
                + format(listePoints.get(listePoints.size() - 1).y_) + ")]";
        // -- on rejoue les donn�es avec les nouvelles valeurs --//
        if (this.ecraser_.isSelected()) {
          implementation.message(TrResource.getS("La courbe actuelle va �tre recalcul�e pour les donn�es suivantes:\n Variable: {0}\n Ligne: {1}", newVariable.getName(), lineDescription));
        } else {
          String msg = TrResource.getS("Une nouvelle courbe de bilan va �tre recalcul�e pour les donn�es suivantes:\n Variable: {0}\n Ligne: {1}.\nLa courbe actuelle ne sera pas �cras�e.", newVariable.getName(), lineDescription);
          implementation.message(msg);
        }

        // -- on rejoue les donn�es avec ou sans ecrasement --//
        tree.replayBilans(implementation, vue2d, modele, source, listePoints, vue2d.getCmdMng(), implementation.getMainProgression(), newVariable, startTime, endTime, this.ecraser_.isSelected(), title);

        return true;

      } else {
        implementation.error(TrLib.getString("Il faut choisir au moins 2 points avec la sonde afin de recalculer les donn�es de la courbe.\nPour s�lectionner plusieurs points, maintenir la touche shift"));
        return false;
      }

    } else {
      implementation.error(TrLib.getString(
              "Il faut choisir au moins 2 points afin de recalculer les donn�es de la courbe.\nPour s�lectionner plusieurs points, maintenir la touche shift"));
      return false;

    }

  }

  @SuppressWarnings("serial")
  public void displayBilanReplayData(final TrPostCourbeTreeModel treeModel, final TrPostCourbeBilanModel modele) {

    final LineString listePoints = modele.getInitLine();
    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    final TrPostVisuPanel vue2d = TrReplayVue2dBuilder.constructVue2d(implementation, this.source, new TrReplayLineEditCustomizer(listePoints, TrResource.TR.getString("Ligne d'origine")));
    vue2d.removeCalqueLegend();
    //
    // -- creation du panel de choix des variables a selectionner --//
    final String title = TrResource.getS("<html><body>Choisir les points et la variable qui seront utilis�s pour refaire le calcul(<b>Il est possible de cr�er des polylignes en choisissant l'outil sonde et en maintenant shift+clic</b>).Les points rouge d�signent l'origine.</body></html>");
    // -- creation du panel de choix des variables a selectionner --//
    rejouerSrc_ = new BuButton();
    JPanel pnTime = new JPanel(new BuGridLayout(2));
    final JTextField tfTitle = new JTextField(10);
    tfTitle.setText(modele.getTitle());
    pnTime.add(new JLabel(TrLib.getString("Titre")));
    pnTime.add(tfTitle);
    pnTime.add(new JLabel(TrLib.getString("Variable")));
    final JComboBox cb = new JComboBox();
    cb.setModel(new DefaultComboBoxModel(this.source.getAvailableVar()));
    pnTime.add(cb);
    pnTime.add(new JLabel(TrLib.getString("Pas de temps d�part de la courbe")));
    final JComboBox startTime = new JComboBox();
    startTime.setModel(new CtuluComboBoxModelAdapter(source.getNewTimeListModel()));
    startTime.setSelectedIndex(0);
    final JComboBox endTime = new JComboBox();
    endTime.setModel(new CtuluComboBoxModelAdapter(this.source.getNewTimeListModel()));
    startTime.setSelectedIndex(Math.max(0, modele.getMinTimeIdx()));
    endTime.setSelectedIndex(Math.min(this.source.getTime().getNbTimeStep() - 1, modele.getMaxTimeIdx()));
    pnTime.add(startTime);
    pnTime.add(new JLabel(TrLib.getString("Pas de temps fin")));
    pnTime.add(endTime);
    Callable<Boolean> okAction = new Callable<Boolean>() {
      @Override
      public Boolean call() throws Exception {
        return replayBilanFor(vue2d, treeModel, modele, startTime.getSelectedIndex(), endTime.getSelectedIndex(), tfTitle.getText());
      }
    };
    final TrReplayDialog dialog = constructEditionDialog(okAction, vue2d, modele, pnTime, title);
    rejouerSrc_.setAction(new AbstractAction(TrLib.getString("Fichier r�sultat")) {
      @Override
      public void actionPerformed(final ActionEvent e) {
        final TrPostSource selectedSource = selectSource(implementation.getCurrentProject(),
                (String) getValue(Action.NAME), implementation.getFrame(), source);
        if (selectedSource != null) {
          // -- on ferme l'interface courante --//
          if (dialog != null) {
            dialog.getDialog().cancel();
          }
          // -- on rappelle l'interface --//
          new TrReplayerBilan(selectedSource, implementation).displayBilanReplayData(treeModel, modele);
        }

      }
    });

    cb.setSelectedItem(comboVar_.getCb().getSelectedItem());
    comboVar_.getCb().addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
          cb.setSelectedItem(comboVar_.getCb().getSelectedItem());
        }
      }
    });
    cb.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
          comboVar_.getCb().setSelectedItem(cb.getSelectedItem());
        }
      }
    });

    // -- on initialise la sonde --//
    vue2d.getSondeAction().setSelected(true);

    // -- on positionne la bonne variable et le bon pas de temps --//
    comboVar_.getCb().setSelectedItem(modele.getVar());
    // -- ajout du calque ligne bris�e pour r�aliser une polyligne --//
    dialog.afficheModale(implementation.getParentComponent(), TrResource.getS("Rejouer ")
            + " " + modele.getTitle(), createRestaurerRunnable(vue2d));

  }

  /**
   * Affiche une dialog avec une vue 2d qui affiche l'ensemble des points de la polyligne utilis�e pour la construction du graphe.
   *
   * @param modele
   * @param implementation
   */
  public void displayBilanProfilOrigin(final MvProfileCourbeModelInterface modele,
          final TrPostCommonImplementation implementation) {

    if (!(modele.getData() instanceof TrPostSource)) {
      implementation.error(TrResource.getS("Erreur"), TrResource.getS("Impossible de r�cup�rer le fichier r�sultat"),
              true);
      return;
    }
    if (modele.getInitLine() == null) {
      implementation.error(TrResource.getS("Erreur"), TrResource.getS("Impossible de r�cup�rer la polyligne"), true);
      return;
    }

    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    final TrReplayLineVisuCustomizer customizer = new TrReplayLineVisuCustomizer(modele, TrResource.TR.getString("Ligne d'origine"));
    final TrPostVisuPanel vue2d = TrReplayVue2dBuilder.constructVue2d(implementation, (TrPostSource) modele.getData(), customizer);
    vue2d.removeCalqueLegend();
    // -- on affiche le contenu dans une dialog--//
    final Object[] vect = new Object[2];
    vect[0] = "X";
    vect[1] = "Y";
    final JTable tableauVal = new JTable(new DefaultTableModel(customizer.getData(), vect));
    tableauVal.setPreferredSize(new Dimension(200, 300));
    final JScrollPane pane = new JScrollPane(tableauVal);
    pane.setPreferredSize(new Dimension(200, 300));
    final String title = "<html><body>"
            + TrResource.getS("Origine de {0}.<br/>Les points de la polyligne sont encadr�s en rouge", modele.getTitle())
            + "</body></html>";
    final CtuluDialogPanel panel = constructDialog(getTrueCallable(), vue2d, modele, pane, false, title,
            new TrReplayGenereVue2DInSceneAction(implementation, (TrPostSource) modele.getData(), customizer, modele.getTitle()));
    // -- on positionne la bonne variable et le bon pas de temps --//
    if (modele instanceof TrPostCourbeBilanModel) {
      TrPostCourbeBilanModel varTimeModel = (TrPostCourbeBilanModel) modele;
      comboVar_.getCb().setSelectedItem(varTimeModel.getVar());
    }
    panel.afficheModale(implementation.getParentComponent(), TrResource.getS("Origine ") + " " + modele.getTitle(),
            createRestaurerRunnable(vue2d));
  }
}
