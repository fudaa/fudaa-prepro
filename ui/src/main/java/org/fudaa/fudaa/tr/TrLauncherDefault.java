/**
 * @creation 24 mai 2004
 * @modification $Date: 2007-12-04 09:09:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr;

import com.memoire.bu.*;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import ghm.followgui.FollowApp;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.impl.Fudaa;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.save.FudaaSaveLib;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.tr.common.*;
import org.fudaa.fudaa.tr.reflux.TrRefluxImplHelper;
import org.fudaa.fudaa.tr.rubar.TrRubarImplHelper;
import org.fudaa.fudaa.tr.telemac.TrTelemacImplHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.jar.Manifest;

/**
 * @author Fred Deniger
 * @version $Id: TrLauncherDefault.java,v 1.103 2007-12-04 09:09:26 clavreul Exp $
 */
public final class TrLauncherDefault extends WindowAdapter implements TrLauncher {
  static BuInformationsSoftware infoSoft;

  public static void init() {
    if (infoSoft != null) {
      return;
    }

    final String languages = "fr,en";

    // on utilise toujours une lnf jgoodies
    final String lnfname = BuPreferences.BU.getStringProperty("lookandfeel.name", null);
    // si pas de lnf ou si l'utilisateur n'a pas explicitement choisi le lnf
    if (lnfname == null || !BuPreferences.BU.getBooleanProperty("tr.lnf", false)) {
      BuPreferences.BU.putStringProperty("lookandfeel.name", "ASPECT_PLASTIC3D");
    }
    BuPreferences.BU.applyLookAndFeel();
    BuPreferences.BU.applyLanguage(languages);
    BuResource.BU.setIconFamily("crystal");

    JOptionPane.setRootFrame(BuLib.HELPER);
    BuLib.HELPER.setIconImage(FudaaResource.FUDAA.getImage("fudaa-logo"));
    // pour ne pas noircir une case sur deux
    UIManager.put("Table.darkerOddLines", Boolean.FALSE);
    System.setProperty("swing.boldMetal", "false");
    FuLib.setSystemProperty("lnf.menu.needed", "false");

    infoSoft = new BuInformationsSoftware();
    infoSoft.name = "prepro";
    readVersionDate(infoSoft);
    infoSoft.rights = TrResource.getS("Tous droits r�serv�s") + ". CEREMA (c)2003-2018";
    infoSoft.contact = "frederic.deniger@gmail.com";
    infoSoft.license = "GPL2";
    infoSoft.languages = languages;
    infoSoft.logo = TrResource.TR.getIcon("prepro");
    infoSoft.banner = TrResource.TR.getIcon("banner");
    infoSoft.http = "http://prepro.fudaa.fr/";
    infoSoft.update = "http://fudaa.fr/prepro/update.php?version=" + infoSoft.version;
    // Attention: bien respecter cette ligne tres utile pour javawebstart
    final URL manUrl = TrLauncherDefault.class.getResource("/aide/prepro");
    if (manUrl == null) {
      infoSoft.man = "http://fudaa.fr/prepro/doc/html/";
    } else {
      infoSoft.man = manUrl.toString() + '/';
    }
    infoSoft.authors = new String[]{"Fr�d�ric Deniger", "Nicolas Clavreul"};
    infoSoft.contributors = new String[]{"Equipes Dodico, Ebli et Fudaa"};
    infoSoft.documentors = new String[]{""};
    infoSoft.testers = new String[]{"Fr�d�ric Deniger, Patrick Gomi, Vanessya Laborie", "Andr� Paquier", "Didier Roug�"};
    infoSoft.citation = "";
    FudaaSaveLib.configureDb4o();
  }

  private static void readVersionDate(BuInformationsSoftware infoSoft) {
    infoSoft.version = "Dev";
    infoSoft.date = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
    try {
      Manifest manifest = new Manifest();
      manifest.read(TrLauncherDefault.class.getClassLoader().getResourceAsStream("META-INF/MANIFEST.MF"));
      if (manifest.getMainAttributes() != null && manifest.getMainAttributes().getValue("fullVersion") != null) {
        infoSoft.version = manifest.getMainAttributes().getValue("version");
        infoSoft.date = manifest.getMainAttributes().getValue("date");
      }
    } catch (Exception ex) {

    }
  }

  /**
   * @param _args les arguments a passer : si null, on prend les args du launcher
   * @param _l le launcher. si null, il est cr��.
   * @param _impl l'impl a demarrer
   * @return l'impl demarree
   */
  static FudaaCommonImplementation launch(final String[] _args, final TrLauncher _l, final TrCommonImplementation _impl) {
    TrLauncher launcher = _l;
    String[] args = _args;
    if (launcher == null) {
      launcher = new TrLauncherDefault();
    } else {
      args = launcher.getDefaultArgs();
    }
    final Fudaa f = new Fudaa();
    f.setDoNotUseFudaaTee(false);
    f.setAddWelcomeMessage(false);
    f.setApplyLanguage(false);
    f.setApplyLookAndFeel(false);
    if (_l == null) {
      f.launch(args, TrLauncherDefault.getInfosSoftware(), true, true);
    } else {
      f.launch(args, TrLauncherDefault.getInfosSoftware(), false, true);
    }
    _impl.setLauncher(launcher);
    final Runnable r = new Runnable() {
      @Override
      public void run() {
        f.startApp(_impl);

        if (updateDial_ != null) {
          updateDial_.toFront();
        }
      }
    };
    if (SwingUtilities.isEventDispatchThread()) {
      r.run();
    } else {
      BuLib.invokeLater(r);
    }

    return _impl;
  }

  protected FollowApp tail_;
  TrImplHelperAbstract helper_;
  private TrRefluxImplHelper refluxImplHelper_;
  private TrTelemacImplHelper telemacImplHelper_;
  private TrRubarImplHelper rubarImplHelper_;
  protected TrSupervisorImplementation supervisor_;

  /**
   * Cree le helper par defaut.
   */
  public TrLauncherDefault() {
    helper_ = getImplHelper(getHydIdFromPrefFile());
    if (helper_ == null) {
      changeChaineCalcul(BuLib.HELPER);
      helper_ = getImplHelper(getHydIdFromPrefFile());
      if (helper_ == null) {
        changeChaineCalcul(BuLib.HELPER);
        helper_ = getImplHelper(getHydIdFromPrefFile());
      }
    }
  }

  /**
   * @return l'impl reflux
   */
  public TrRefluxImplHelper getRefluxImplHelper() {
    if (refluxImplHelper_ == null) {
      refluxImplHelper_ = new TrRefluxImplHelper(this);
    }
    return refluxImplHelper_;
  }

  /**
   * @return l'impl telemac
   */
  public TrTelemacImplHelper getTelemacImplHelper() {
    if (telemacImplHelper_ == null) {
      telemacImplHelper_ = new TrTelemacImplHelper(this);
    }
    return telemacImplHelper_;
  }

  public TrRubarImplHelper getRubarImplHelper() {
    if (rubarImplHelper_ == null) {
      rubarImplHelper_ = new TrRubarImplHelper(this);
    }
    return rubarImplHelper_;
  }

  @Override
  public void createHydProjet(final File _gridFile) {
    final TrEditorImplementation impl = (TrEditorImplementation) launch(null, this, new TrEditorImplementation());
    impl.creer(_gridFile);
  }

  @Override
  public TrImplHelperAbstract getCurrentImplHelper() {
    return helper_;
  }

  @Override
  public String[] getDefaultArgs() {
    return new String[]{"--no_splash", "--no_log"};
  }

  @Override
  public TrFileFormatManager getFileFormatManager() {
    return TrFileFormatManager.INSTANCE;
  }

  @Override
  public TrImplHelperAbstract getImplHelper(final String _id) {
    if (_id == null) {
      return null;
    }
    if (FileFormatSoftware.REFLUX_IS.name.equals(_id)) {
      return getRefluxImplHelper();
    } else if (FileFormatSoftware.TELEMAC_IS.name.equals(_id)) {
      return getTelemacImplHelper();
    } else if (FileFormatSoftware.RUBAR_IS.name.equals(_id)) {
      return getRubarImplHelper();
    }
    FuLog.warning(new Throwable());
    return null;
  }

  @Override
  public String getCurrentPrefHydId() {
    return helper_.getSoftwareID();
  }

  /**
   * @return l'identifiant de la chaine hyd 2d courante
   */
  public static String getHydIdFromPrefFile() {
    return TrPreferences.TR.getStringProperty("tr.code.id", null);
  }

  /**
   * @return les choix
   */
  public static String[] getChoices() {
    return new String[]{FileFormatSoftware.REFLUX_IS.name, FileFormatSoftware.RUBAR_IS.name, FileFormatSoftware.TELEMAC_IS.name};
  }

  /**
   * Permet de changer le systeme utilise.
   */
  @Override
  public void changeChaineCalcul(final Component _parent) {
    final String init = TrPreferences.TR.getStringProperty("tr.code.id", null);
    final String[] choix = getChoices();
    final BuDialogChoice dialc = new BuDialogChoice(null, TrEditorImplementation.informationsSoftware(),
        TrResource.getS("Configuration cha�ne de calcul"), TrResource.getS(
        "Veuillez s�lectionner la cha�ne de calcul utilis�e"), choix);
    dialc.setSize(200, 200);
    if (init != null) {
      dialc.setValue(init);
    }
    if (CtuluLibDialog.isOkResponse(dialc.activate())) {
      final String id = dialc.getValue();
      if (!id.equals(init)) {
        TrPreferences.TR.putStringProperty("tr.code.id", id);
        TrPreferences.TR.writeIniFile();
        // helper_=getImplHelper(id);
      }
    }
  }

  @Override
  public TrEditorImplementation findImplWithOpenedFile(final File _f) {
    if (_f == null || !_f.exists()) {
      return null;
    }
    final DefaultListModel model = BuRegistry.getModel();
    for (int i = model.getSize() - 1; i >= 0; i--) {
      final Object app = model.get(i);
      if (app instanceof BuApplication && ((BuApplication) app).getImplementation() instanceof TrEditorImplementation) {
        final TrEditorImplementation imp = (TrEditorImplementation) ((BuApplication) app).getImplementation();
        if (imp.projetCourant() != null && _f.equals(imp.projetCourant().getParamsFile())) {
          return imp;
        }
      }
    }
    return null;
  }

  public boolean isUsedInEditor(final File _f) {
    if (_f == null) {
      return false;
    }
    final DefaultListModel model = BuRegistry.getModel();
    for (int i = model.getSize() - 1; i >= 0; i--) {
      final Object app = model.get(i);
      if (app instanceof BuApplication && ((BuApplication) app).getImplementation() instanceof TrEditorImplementation) {
        final TrEditorImplementation imp = (TrEditorImplementation) ((BuApplication) app).getImplementation();
        if (imp.projetCourant() != null && imp.projetCourant().isAlreadyUsed(_f)) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public boolean isAlreadyUsed(File _f) {
    return isUsedInEditor(_f) || findPostWithOpenedFile(_f) != null;
  }

  @Override
  public TrPostImplementation findPostWithOpenedFile(final File _f) {
    if (_f == null || !_f.exists()) {
      return null;
    }
    final DefaultListModel model = BuRegistry.getModel();
    for (int i = model.getSize() - 1; i >= 0; i--) {
      final Object app = model.get(i);
      if (app instanceof BuApplication && ((BuApplication) app).getImplementation() instanceof TrPostImplementation) {
        final TrPostImplementation imp = (TrPostImplementation) ((BuApplication) app).getImplementation();
        if (imp.getCurrentProject() != null && imp.getCurrentProject().getSources().isOpened(_f)) {
          return imp;
        }
      }
    }
    return null;
  }

  @Override
  public void openLogFrame(final File _f) {
    if (tail_ == null) {
      new CtuluTaskOperationGUI(null, FudaaLib.getS("Console")) {
        @Override
        public void act() {
          try {
            tail_ = FollowApp.openFollowApp(new String[0], true);
            if (_f != null) {
              tail_.openFile(_f, false, true);
            }
            tail_.setCloseFrameExitJvm(false);
            BuRegistry.register(tail_);
            tail_.getFrame().addWindowListener(TrLauncherDefault.this);
          } catch (final IOException _e) {
            FuLog.warning(_e);
          } catch (final InterruptedException _e) {
            FuLog.warning(_e);
            Thread.currentThread().interrupt();
          } catch (final InvocationTargetException _e) {
            FuLog.warning(_e);
          }
        }
      }.start();
    } else {
      tail_.openFile(_f, false, true);
      tail_.getFrame().show();
    }
  }

  @Override
  public void openMeshView(final File _f) {
    final TrMeshImplementation mesh = (TrMeshImplementation) launch(null, TrLauncherDefault.this, new TrMeshImplementation());
    mesh.cmdOuvrirFile(_f);
  }

  /**
   * Methode appelee pour generer le trpost avec le ficheir de layout ouvert.
   */
  @Override
  public TrPostImplementation openLayoutPost(final File _f, final boolean forceRecompute) {

    final TrPostImplementation alreadyOpened = findPostWithOpenedFile(_f);
    if (alreadyOpened != null) {
      alreadyOpened.setMainErrorAndClear(TrLib.getMessageAlreadyOpen(_f));
      alreadyOpened.getFrame().setVisible(true);
      alreadyOpened.getFrame().setState(Frame.NORMAL);
      alreadyOpened.getFrame().requestFocus();
      // pour Rubar
      return alreadyOpened;
    }

    final TrPostImplementation post = (TrPostImplementation) launch(null, TrLauncherDefault.this, new TrPostImplementation());
    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {
        if (_f != null) {

          post.cmdOuvrirLayoutFile(_f, forceRecompute);
        }
      }
    });

    return post;
  }

  /**
   * Methode appelee pour generer le trpost avec le fichier scop choisi (S, T ou GENE)
   */
  @Override
  public TrPostImplementation openScopPost(final File _f) {
    final TrPostImplementation alreadyOpened = findPostWithOpenedFile(_f);
    if (alreadyOpened != null) {
      alreadyOpened.setMainErrorAndClear(TrLib.getMessageAlreadyOpen(_f));
      alreadyOpened.getFrame().setVisible(true);
      alreadyOpened.getFrame().setState(Frame.NORMAL);
      alreadyOpened.getFrame().requestFocus();
      // pour Rubar
      // alreadyOpened.getCurrentProject().activate(_f);
      return alreadyOpened;
    }

    final TrPostImplementation post = (TrPostImplementation) launch(null, TrLauncherDefault.this, new TrPostImplementation());
    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {
        if (_f != null) {

          post.cmdOuvrirScopFile(_f);
        }
      }
    });

    return post;
  }

  @Override
  public TrPostImplementation openPost(final File _f) {
    final TrPostImplementation alreadyOpened = findPostWithOpenedFile(_f);
    if (alreadyOpened != null) {
      alreadyOpened.setMainErrorAndClear(TrLib.getMessageAlreadyOpen(_f));
      alreadyOpened.getFrame().setVisible(true);
      alreadyOpened.getFrame().setState(Frame.NORMAL);
      alreadyOpened.getFrame().requestFocus();
      // pour Rubar
      // alreadyOpened.getCurrentProject().activate(_f);
      return alreadyOpened;
    }

    final TrPostImplementation post = (TrPostImplementation) launch(null, TrLauncherDefault.this, new TrPostImplementation());
    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {
        if (_f != null) {
          post.cmdOuvrirFile(_f);
        }
      }
    });

    return post;
  }

  @Override
  public void openSupervisor() {
    if (supervisor_ == null) {
      BuLib.invokeLater(new Runnable() {
        @Override
        public void run() {
          supervisor_ = (TrSupervisorImplementation) launch(null, TrLauncherDefault.this, new TrSupervisorImplementation());
        }
      });
    } else {
      supervisor_.getFrame().show();
    }
  }

  @Override
  public void closeAll(final BuCommonInterface _app) {
    if (_app != null) {
      _app.exit();
    }
    if (tail_ != null) {
      tail_.getFrame().hide();
      tail_.getFrame().dispose();
      BuRegistry.unregister(tail_);
      tail_ = null;
    }
    if (supervisor_ != null) {
      supervisor_.exit();
    }
    final ListModel model = BuRegistry.getModel();
    if (model.getSize() == 0) {
      System.exit(0);
    }
    int nb = model.getSize() - 1;
    for (int i = 0; i <= nb; i++) {
      final Object o = model.getElementAt(i);
      if (o instanceof BuApplication) {
        ((BuApplication) o).exit();
      } else if (o instanceof Window) {
        ((Window) o).hide();
        ((Window) o).dispose();
      }
    }
  }

  @Override
  public void ouvrirHydEditor(final String _hydId, final File _projectFile) {
    final TrEditorImplementation alreadyOpened = findImplWithOpenedFile(_projectFile);
    if (alreadyOpened != null) {
      alreadyOpened.warn(TrResource.getS("Ouverture"), TrResource.getS("Le fichier {0} est d�j� ouvert", _projectFile.getName()),
          false);
      alreadyOpened.getFrame().setVisible(true);
      alreadyOpened.getFrame().setState(Frame.NORMAL);
      alreadyOpened.getFrame().requestFocus();
      return;
    }
    BuLib.invokeNow(new Runnable() {
      @Override
      public void run() {
        final TrEditorImplementation impl = (TrEditorImplementation) launch(null, TrLauncherDefault.this,
            new TrEditorImplementation());
        BuLib.invokeLater(new Runnable() {
          @Override
          public void run() {
            if (_projectFile != null) {
              impl.ouvrir(_projectFile, getImplHelper(_hydId));
            }
          }
        });
      }
    });
  }

  static JDialog updateDial_;

  @Override
  public void windowClosed(final WindowEvent _e) {
    final Window e = _e.getWindow();
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FTR: close frame " + e.getName());
    }
    final boolean isTailClosed = (tail_ != null) && (tail_.getFrame() == e);
    if (isTailClosed) {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("FTR: close tail frame");
      }
      tail_ = null;
    }
    if (supervisor_ != null) {
      if (supervisor_.getFrame() == e) {
        if (Fu.DEBUG && FuLog.isDebug()) {
          FuLog.debug("FTR: close supervisor frame");
        }
        supervisor_ = null;
      } else if (!isTailClosed) {
        supervisor_.getFrame().toFront();
      }
    }
    e.removeWindowListener(this);
  }

  public static BuInformationsSoftware getInfosSoftware() {
    return infoSoft;
  }
}
