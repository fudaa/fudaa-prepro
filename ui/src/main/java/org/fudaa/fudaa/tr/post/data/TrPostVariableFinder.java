package org.fudaa.fudaa.tr.post.data;

import java.util.HashMap;
import java.util.Map;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.TrPostSource;

public class TrPostVariableFinder {

  private final Map<String, H2dVariableType> found;

  public TrPostVariableFinder(TrPostSource src) {
    int variableNb = src.getVariableNb();
    found = new HashMap<String, H2dVariableType>(variableNb + 3);
    for (int i = 0; i < variableNb; i++) {
      H2dVariableType variable = src.getVariable(i);
      found.put(variable.getID(), variable);
    }
  }

  public H2dVariableType getVariable(String id) {
    return found.get(id);
  }

}
