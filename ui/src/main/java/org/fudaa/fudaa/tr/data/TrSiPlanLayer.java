/*
 *  @creation     14 avr. 2005
 *  @modification $Date: 2007-01-19 13:14:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeDefault;
import org.fudaa.fudaa.sig.layer.FSigLayerLine;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.persistence.TrSiPlanLayerPersistence;

/**
 * @author Fred Deniger
 * @version $Id: TrSiPlanLayer.java,v 1.25 2007-01-19 13:14:09 deniger Exp $
 */
public class TrSiPlanLayer extends FSigLayerLine {

  /**
   * @return le nom par defaut
   */
  public static String getDefaultName() {
    return "cqSiPlans";
  }

  GISZoneCollection getGeomData() {
    return super.modele_.getGeomData();
  }

  @Override
  public BCalquePersistenceInterface getPersistenceMng() {

    return new TrSiPlanLayerPersistence();
  }

  /**
   * @param _model le model
   */
  public TrSiPlanLayer(final ZModeleLigneBriseeDefault _model) {
    super(_model);
    setTitle(TrResource.getS("Surfaces libres"));
    setDestructible(false);
    setName(getDefaultName());
    super.iconModel_.setTaille(2);
  }

  public TrSiProfilModel.Zone getZone() {
    return (TrSiProfilModel.Zone) ((ZModeleLigneBriseeDefault) modeleDonnees()).getGeometries();
  }

}
