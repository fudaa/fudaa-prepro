/**
 *  @creation     27 janv. 2005
 *  @modification $Date: 2006-09-19 15:07:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import javax.swing.Icon;
import org.fudaa.ebli.commun.EbliActionSimple;

/**
 * @author Fred Deniger
 * @version $Id: TrSaisiePointAction.java,v 1.6 2006-09-19 15:07:28 deniger Exp $
 */
public class TrSaisiePointAction extends EbliActionSimple {

  protected TrSaisiePoint pt_;

  /**
   * @param _name
   * @param _ic
   * @param _ac
   */
  public TrSaisiePointAction(final String _name, final Icon _ic, final String _ac) {
    super(_name, _ic, _ac);
  }

  protected boolean isEnCours() {
    return pt_ != null;
  }

  protected void selectPalette() {
    if (pt_ != null) {
      pt_.selectPalette();
    }
  }
}
