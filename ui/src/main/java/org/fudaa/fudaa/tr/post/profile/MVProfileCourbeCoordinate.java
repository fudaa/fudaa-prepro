/*
 * @creation 30 nov. 06
 * @modification $Date: 2007-02-02 11:22:13 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGCourbePersistBuilder;
import org.fudaa.ebli.courbe.EGGroup;

/**
 * @author fred deniger
 * @version $Id: MVProfileCourbeCoordinate.java,v 1.2 2007-02-02 11:22:13 deniger Exp $
 */
public class MVProfileCourbeCoordinate extends EGCourbeChild implements MvProfileCourbeInterface {

  public MVProfileCourbeCoordinate(final EGGroup _m, final MvProfileCoordinatesModel _model) {
    super(_m, _model);
  }

  @Override
  public MvProfileCourbeModelInterface getProfileModel() {
    return (MvProfileCourbeModelInterface) getModel();
  }
  
  @Override
  protected EGCourbePersistBuilder<? extends EGCourbeChild> createPersistBuilder() {
    return new MvProfileCourbeCoordinatePersistBuilder();
  }

}
