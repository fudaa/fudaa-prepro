/*
 * @creation 21 nov. 06
 * 
 * @modification $Date: 2007-06-13 12:58:09 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import org.locationtech.jts.geom.LineString;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsBuilder;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.commun.EbliFormatter;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimePersistBuilder;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.tr.common.TrDataSourceNomme;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourceAbstractFromIdx;
import org.fudaa.fudaa.tr.post.actions.TrPostProfileAction;
import org.fudaa.fudaa.tr.post.persist.TrPostReloadParameter;
import org.fudaa.fudaa.tr.post.replay.TrReplayerProfilSpatial;

/**
 * WARN: do not change the name of this class.
 *
 * @author fred deniger
 * @version $Id: MVProfileCourbeModel.java,v 1.10 2007-06-13 12:58:09 deniger Exp $
 */
public class MVProfileCourbeModel extends MVProfileCourbeModelDefault {

  private int time_;
  CtuluVariable variable_;
  EfGridData data_;
  //the builder with correction for water depth.
  EfLineIntersectionsResultsBuilder builder_;
  //used when the data are retrieved from persistence. In this case, only the initial builder is built.
  private MvProfileBuilder initialBuilder;

  private MVProfileCourbeModel(final CtuluVariable _variable, final EfGridData _data, final int _timeStep,
          final MvProfileXYProvider.Cached xyProvider, final EfLineIntersectionsResultsBuilder _builder) {
    super();
    variable_ = _variable;
    data_ = _data;
    title_ = _variable.toString();
    time_ = _timeStep;
    builder_ = _builder;
    this.xyProvider = xyProvider;
  }

  public MVProfileCourbeModel(final CtuluVariable _variable, final EfGridData _data, final int _timeStep,
          final EfLineIntersectionsResultsBuilder _builder, final ProgressionInterface _prog) {
    super();
    variable_ = _variable;
    data_ = _data;
    title_ = _variable.toString();
    time_ = _timeStep;
    builder_ = _builder;
    xyProvider = MvProfileXYProvider.createValueProvider(_builder, time_, _variable, _prog);
  }

  @Override
  public boolean isDefinedOnMeshes() {
    return data_.isElementVar(variable_);
  }

  /**
   * ACHTUNG: Constructeur uniquement utilis� pour la persistance des donn�es.
   */
  public MVProfileCourbeModel() {
    super();
  }

  @Override
  public MVProfileCourbeModel duplicate() {
    final MVProfileCourbeModel res = new MVProfileCourbeModel(variable_, data_, time_, xyProvider, builder_);
    res.initialBuilder = initialBuilder;
    res.setTitle(getTitle());
    return res;
  }
  EbliFormatter formater = new EbliFormatter();

  @Override
  public void fillWithInfo(InfoData _table, CtuluListSelectionInterface _selectedPt) {
    String pdt = "N�" + this.time_;
    if (this.data_ instanceof TrPostSource) {
      _table.put(TrResource.getS("Type"), "Profil spatial");
      TrPostSource src = (TrPostSource) this.data_;
      TrPostSourceAbstractFromIdx.fillWithSourceInfo(_table, src);
      if (this.time_ != -1 && this.time_ < src.getNbTimeStep()) {
        pdt += ": " + src.getTime().getTimeListModel().getElementAt(this.time_);
      }
    }
    if (this.variable_ != null) {
      _table.put(MvResource.getS("Variable"), this.variable_.getName() + " en " + this.variable_.getCommonUnit());
    }
    _table.put(MvResource.getS("Pas de temps"), pdt);

    LineString polyligne = getInitLine();
    if (polyligne != null && polyligne.getNumPoints() > 0) {
      _table.put(MvResource.getS("Polyligne, nb pts"), Integer.toString(polyligne.getNumPoints()));

      String ptDebut = "(";
      if (formater != null && formater.getFormatter() != null) {
        ptDebut += formater.getFormatter().format(polyligne.getCoordinateN(0).x);
      } else {
        ptDebut += polyligne.getCoordinateN(0).x;
      }
      ptDebut += ";";
      if (formater != null && formater.getFormatter() != null) {
        ptDebut += formater.getFormatter().format(polyligne.getCoordinateN(0).y);
      } else {
        ptDebut += polyligne.getCoordinateN(0).y;
      }
      ptDebut += ")";

      _table.put(MvResource.getS("Point d�but"), ptDebut);

      int fin = polyligne.getNumPoints() - 1;
      String ptFin = "(";
      if (formater != null && formater.getFormatter() != null) {
        ptFin += formater.getFormatter().format(polyligne.getCoordinateN(fin).x);
      } else {
        ptFin += polyligne.getCoordinateN(fin).x;
      }
      ptFin += ";";
      if (formater != null && formater.getFormatter() != null) {
        ptFin += formater.getFormatter().format(polyligne.getCoordinateN(fin).y);
      } else {
        ptFin += polyligne.getCoordinateN(fin).y;
      }
      ptFin += ")";

      _table.put(MvResource.getS("Point fin"), ptFin);

    }

  }

  /**
   *
   * @return the line used to build this profile
   */
  @Override
  public LineString getInitLine() {
    return builder_ == null ? initialBuilder.selectedLine_ : builder_.getInitLine();
  }

  public int getTime() {
    return time_;
  }

  public CtuluVariable getVariable() {
    return variable_;
  }

  @Override
  public boolean isDuplicatable() {
    return true;
  }
  public boolean isRemovable_ = true;

  @Override
  public boolean isRemovable() {
    return isRemovable_;
  }

  @Override
  public boolean isTitleModifiable() {
    return true;
  }

  @Override
  public void setRes(final EfLineIntersectionsResultsBuilder _builder, final ProgressionInterface _prog) {
    builder_ = _builder;
    initialBuilder = null;
    xyProvider = MvProfileXYProvider.createValueProvider(_builder, time_, variable_, _prog);
  }

  public void setTime(final int _time) {
    if (time_ != _time && _time >= 0) {
      time_ = _time;
      createBuilder();
      xyProvider = MvProfileXYProvider.change(xyProvider, builder_, time_, variable_, null);
    }
  }

  public void setVariable(final CtuluVariable _variable, final boolean _contentChanged) {
    variable_ = _variable;
    if (_contentChanged && data_.isDefined(_variable)) {
      createBuilder();
      xyProvider = MvProfileXYProvider.change(xyProvider, builder_, time_, variable_, null);
    }
  }

  @Override
  public Object savePersistSpecificDatas() {
    // -- retourne le quatuor point, pdt,variable et fichier source
    ArrayList<Object> listeData = new ArrayList<Object>();
    // listeData.add(this.);

    // -- ajout du pas de temps --//
    listeData.add(new Integer(time_));

    // -- ajout de la variable --//
    listeData.add(this.variable_.getID());

    // -- ajout de la polyligne --//
    //on sauvegarde la poyligne intiale.
    listeData.add(getInitLine());

    // -- ajout des datas --//
    if (data_ instanceof TrPostSource) {
      listeData.add(((TrPostSource) data_).getId());
    }

    return listeData;
  }

  /**
   * pourquoi avoir mis tout cela ici alors qu'il y a une classe sp�cifique !
   *
   * @param data
   * @param infos
   */
  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
    if (data == null || !(data instanceof ArrayList)) {
      return;
    }
    ArrayList<Object> liste = (ArrayList<Object>) data;

    TrPostProjet projet = (TrPostProjet) infos.get(TrPostReloadParameter.POST_PROJET);
    TrPostSource src = null;
    // -- etape 1: le pas de temps --//
    this.time_ = (Integer) liste.get(0);

    // -- etape 2 la variable --//
    String idVar = (String) liste.get(1);
    H2dVariableType var = null;// (H2dVariableType) liste.get(1);

    // -- etape 3: la polyligne --//
    LineString polyligne = (LineString) liste.get(2);

    // -- etape 4: recherche du source qui contient le path donn� --//
    String idSource = null;
    if (projet != null && liste.size() >= 4) {
      idSource = (String) liste.get(3);
      src = projet.getSources().findSourceById(idSource);
      if (src != null) {
        this.data_ = src;
      } else {
        ((List<String>) infos.get("errorMsg")).add("Erreur, la frame graphe ne trouve pas le fichier r�sultat qui correspond � l'ID " + idVar);
        return;
      }
    } else {
      ((List<String>) infos.get("errorMsg")).add("Erreur, la frame graphe ne trouve pas le fichier r�sultat qui correspond � l'ID " + idVar);
      return;
    }
    // -- recherche dans les sources de la variable par id --//
    // var =src.getAvailableVar()[0];
    for (int i = 0; i < src.getAvailableVar().length; i++) {
      if (src.getAvailableVar()[i].getID().equals(idVar)) {
        var = src.getAvailableVar()[i];
      }
    }

    if (var != null) {
      this.variable_ = var;
    } else {
      ((List<String>) infos.get("errorMsg")).add("Erreur, la frame graphe ne trouve pas la variable qui correspond � l'ID " + idVar);
      // return;
      // -- on alloue par defaut la premiere variable dispo --//
      this.variable_ = src.getAvailableVar()[0];
    }

    // -- on rejoue les donn�es: --//
    // -- creation du resultBuilder: --//
    MvProfileTarget target = new TrPostProfileAction.ProfileAdapter(src, projet);
    MvProfileBuilder builder = new MvProfileBuilderFromLine(target, projet.impl_, polyligne, new MvProfileCoteTester());

    infos.put(FudaaCourbeTimePersistBuilder.TIME_MODEL, src.getNewTimeListModel());
    boolean done = false;
    if (!TrPostReloadParameter.isForceRecompute(infos)) {
      int[] hiddenSegments = (int[]) infos.get(MvProfileCourbeCoordinatePersistBuilder.HIDDEN_SEGMENTS);
      int[] hiddenPoints = (int[]) infos.get(MvProfileCourbeCoordinatePersistBuilder.HIDDEN_POINTS);
      this.xyProvider = MvProfileCourbeCoordinatePersistBuilder.createPersistProfileData(infos, hiddenPoints, hiddenSegments);
      if (xyProvider != null) {
        initialBuilder = builder;
        done = true;

      }
    }
    if (!done) {
      EfLineIntersectionsResultsI res = builder.getDefaultRes(this.variable_, projet.impl_.getMainProgression());
      // -- EfLineIntersectionsResultsBuilder --//
      this.builder_ = new EfLineIntersectionsResultsBuilder(polyligne, res, new MvProfileCoteTester());
      this.xyProvider = MvProfileXYProvider.createValueProvider(builder_, time_, variable_, null);
    }
  }

  private void createBuilder() {
    if (builder_ != null) {
      initialBuilder = null;
      return;
    }
    EfLineIntersectionsResultsI res = initialBuilder.getDefaultRes(this.variable_, null);
    // -- EfLineIntersectionsResultsBuilder --//
    this.builder_ = new EfLineIntersectionsResultsBuilder(initialBuilder.selectedLine_, res, new MvProfileCoteTester());
  }

  @Override
  public EfGridData getData() {
    return data_;
  }

  public void setData(TrPostSource _data) {
    this.data_ = _data;
  }

  @Override
  public void replayData(org.fudaa.ebli.courbe.EGGrapheModel model, Map infos, CtuluUI impl) {
    if (!(impl instanceof TrPostCommonImplementation)) {
      impl.error(TrResource.getS("Impossible de r�cup�rer la bonne interface fudaa"));
      return;
    }
    TrPostCommonImplementation implementation = (TrPostCommonImplementation) impl;
    if (!(this.getData() instanceof TrPostSource)) {
      implementation.error(TrResource.getS("Impossible de r�cup�rer le fichier r�sulat depuis ce type de profil spatial"));
      return;
    }
    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    new TrReplayerProfilSpatial((TrPostSource) this.getData(), implementation).displayProfilSpatialReplayData((MvProfileTreeModel) model, this);
  }

  @Override
  public boolean isReplayable() {
    return true;
  }

  @Override
  public String getSourceName() {
    return (data_ instanceof TrDataSourceNomme) ? ((TrDataSourceNomme) data_).getSourceName() : MvResource.getS("Inconnu");
  }


@Override
public int[] getInitRows() {
	// TODO Auto-generated method stub
	return null;
}
}
