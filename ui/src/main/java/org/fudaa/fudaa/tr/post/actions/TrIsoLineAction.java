/*
 * @creation 18 avr. 07
 * @modification $Date: 2007-04-30 14:22:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.actions;

import com.memoire.bu.BuWizardDialog;
import java.awt.event.ActionEvent;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrIsoLineWizard;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;

/**
 * temporaire pour tester.
 * 
 * @author fred deniger
 * @version $Id: TrIsoLineAction.java,v 1.2 2007-04-30 14:22:38 deniger Exp $
 */
public class TrIsoLineAction extends EbliActionSimple {

  final TrPostVisuPanel dest_;
  final TrPostProjet project_;

  public TrIsoLineAction(final TrPostVisuPanel _dest) {
    super(TrResource.getS("Rechercher les isolignes"), EbliResource.EBLI.getIcon("crystal_analyser"), "FIND_ISOS");
    dest_ = _dest;
    project_ = dest_.getProjet();
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final BuWizardDialog dialog = new BuWizardDialog(dest_.getImpl().getFrame(), new TrIsoLineWizard(dest_));
    dialog.pack();
    dialog.setModal(true);
    dialog.setLocationRelativeTo(dest_.getImpl().getFrame());
    dialog.show();
  }
}
