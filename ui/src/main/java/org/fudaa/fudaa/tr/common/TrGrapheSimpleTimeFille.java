/*
 *  @creation     25 mars 2005
 *  @modification $Date: 2007-01-19 13:14:24 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuInformationsDocument;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.gui.CtuluHelpComponent;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.fudaa.commun.courbe.FudaaGrapheSimpleTimeFille;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * @author Fred Deniger
 * @version $Id: TrGrapheSimpleTimeFille.java,v 1.10 2007-01-19 13:14:24 deniger Exp $
 */
public class TrGrapheSimpleTimeFille extends FudaaGrapheSimpleTimeFille implements CtuluHelpComponent {

  public TrGrapheSimpleTimeFille(final EGGraphe _g, final String _titre, final FudaaCommonImplementation _appli) {
    this(new EGFillePanel(_g), _titre, _appli, null, new EGTableGraphePanel());
  }

  public TrGrapheSimpleTimeFille(final EGGraphe _g, final String _titre, final FudaaCommonImplementation _appli,
      final BuInformationsDocument _id, final EGTableGraphePanel _t) {
    this(new EGFillePanel(_g), _titre, _appli, _id, _t);
  }

  public TrGrapheSimpleTimeFille(final EGFillePanel _g, final String _titre, final FudaaCommonImplementation _appli,
      final BuInformationsDocument _id, final EGTableGraphePanel _t) {
    super(_g, _titre, _appli, _id, _t);
  }

  @Override
  public String getShortHtmlHelp() {
    final CtuluHtmlWriter buf = new CtuluHtmlWriter();
    buf.h2Center(getTitle());
    buf.close(buf.para(), TrResource.getS("Affiche des �volutions temporelles")); //Shows temporal evolutions 
    buf.h3(TrResource.getS("Documents associ�s"));
    buf.addTag("lu");
    buf.close(buf.addTag("li"), impl_.buildLink(TrResource.getS("Description du composant d'affichage des courbes"), //Description of the  component billing of the curves    
        "common-curves"));
    return buf.toString();
  }
}
