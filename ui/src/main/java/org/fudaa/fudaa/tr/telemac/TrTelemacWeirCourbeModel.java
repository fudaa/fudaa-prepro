/**
 * @creation 7 d�c. 2004 @modification $Date: 2007-05-22 14:20:38 $ @license GNU General Public License 2 @copyright (c)1998-2001
 *     CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuil;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuilMng;
import org.fudaa.ebli.courbe.EGCourbeModelDefault;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

import java.awt.*;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacWeirCourbeModel.java,v 1.14 2007-05-22 14:20:38 deniger Exp $
 */
public class TrTelemacWeirCourbeModel implements EGModel {
  boolean cote_;
  int i_;
  H2dTelemacSeuilMng mng_;
  H2dTelemacSeuil s_;
  double[] x_;

  /**
   * @param _i le seuil concerne
   * @param _mng le manager permettant de modifier cette courbe
   * @param _cote true si on suit la variable cote
   */
  public TrTelemacWeirCourbeModel(final int _i, final H2dTelemacSeuilMng _mng, final boolean _cote, final double[] _x) {
    super();
    i_ = _i;
    mng_ = _mng;
    s_ = mng_.getTelemacSeuil(_i);
    cote_ = _cote;
    x_ = _x;
  }

  @Override
  public boolean useSpecificIcon(int idx) {
    return false;
  }

  @Override
  public Color getSpecificColor(int idx) {
    return null;
  }

  @Override
  public boolean isGenerationSourceVisible() {
    return false;
  }

  protected final double[] getX() {
    return x_;
  }

  protected final void setX(final double[] _x) {
    x_ = _x;
  }

  @Override
  public boolean addValue(final double _x, final double _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean addValue(final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean deplace(final int[] _selectIdx, final double _deltaX, final double _deltaY,
                         final CtuluCommandContainer _cmd) {
    return EGCourbeModelDefault.deplace(this, _selectIdx, _deltaX, _deltaY, _cmd);
  }

  public int getActiveTimeIdx() {
    return 0;
  }

  @Override
  public int getNbValues() {
    return s_.getNbPoint();
  }

  @Override
  public String getTitle() {
    return H2dResource.getS(cote_ ? "Cote du seuil" : "Coefficient de d�bit");
  }

  @Override
  public double getX(final int _idx) {
    return x_[_idx];
  }

  @Override
  public double getXMax() {
    return x_[getNbValues() - 1];
  }

  @Override
  public double getXMin() {
    return 0;
  }

  @Override
  public double getY(final int _idx) {
    return cote_ ? s_.getCote(_idx) : s_.getCoefDebit(_idx);
  }

  @Override
  public double getYMax() {
    return cote_ ? s_.getCoteMax() : s_.getDebitMax();
  }

  @Override
  public double getYMin() {
    return cote_ ? s_.getCoteMin() : s_.getDebitMin();
  }

  public boolean isActiveTimeEnable() {
    return false;
  }

  @Override
  public boolean isDuplicatable() {
    return true;
  }

  @Override
  public boolean isModifiable() {
    return true;
  }

  @Override
  public boolean isPointDrawn(final int _i) {
    return true;
  }

  @Override
  public boolean isRemovable() {
    return false;
  }

  @Override
  public boolean isSegmentDrawn(final int _i) {
    return true;
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  public boolean isVisibleLong() {
    return false;
  }

  @Override
  public boolean isXModifiable() {
    return false;
  }

  @Override
  public boolean removeValue(final int _i, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean removeValue(final int[] _i, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean setTitle(final String _newName) {
    return false;
  }

  @Override
  public boolean setValue(final int _i, final double _x, final double _y, final CtuluCommandContainer _cmd) {
    if (cote_) {
      return mng_.modifySeuilCote(i_, _i, _y, _cmd);
    }
    return mng_.modifySeuilDebit(i_, _i, _y, _cmd);
  }

  @Override
  public boolean setValues(final int[] _idx, final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    if (cote_) {
      return mng_.modifySeuilCote(i_, _idx, _y, _cmd);
    }
    return mng_.modifySeuilDebit(i_, _idx, _y, _cmd);
  }

  @Override
  public void fillWithInfo(final InfoData _table, final CtuluListSelectionInterface _selectedPt) {
  }

  @Override
  public EGModel duplicate() {
    return new TrTelemacWeirCourbeModel(this.i_, this.mng_, this.cote_, CtuluLibArray.copy(this.x_));
  }

  @Override
  public Object savePersistSpecificDatas() {
    return null;
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
  }

  @Override
  public void replayData(EGGrapheModel model, Map infos, CtuluUI impl) {
    // TODO Auto-generated method stub
  }

  @Override
  public boolean isReplayable() {
    return false;
  }

  @Override
  public String getPointLabel(int i) {
    return null;
  }

  @Override
  public int[] getInitRows() {
    return null;
  }
}
