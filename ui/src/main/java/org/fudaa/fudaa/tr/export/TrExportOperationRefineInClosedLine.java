package org.fudaa.fudaa.tr.export;

import org.locationtech.jts.geom.LinearRing;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.operation.EfIndexVisitorEltInRing;
import org.fudaa.dodico.ef.operation.EfOperation;
import org.fudaa.dodico.ef.operation.refine.EfOperationRefineEdge;
import org.fudaa.fudaa.meshviewer.export.MvExportOperationBuilderInterface;
import org.fudaa.fudaa.tr.data.TrVisuPanel;

/**
 * @author deniger
 */
public class TrExportOperationRefineInClosedLine extends TrExportOperationOnClosedLineAbstract {
  protected class Builder implements MvExportOperationBuilderInterface {

    LinearRing line;
    final EfGridData grid;
    CtuluListSelectionInterface meshSelected;

    private Builder(final LinearRing line, final EfGridData grid) {
      super();
      this.line = line;
      this.grid = grid;
    }

    @Override
    public void buildOperation(final ProgressionInterface progression) {
      meshSelected = EfIndexVisitorEltInRing.findContainedElt(grid.getGrid(), line, true);

    }

    @Override
    public EfOperation getOperation(final int tidx) {
      final EfOperationRefineEdge res = new EfOperationRefineEdge();
      res.setSelectedElt(meshSelected);
      return res;
    }

    @Override
    public void stop() {

    }

  }

  public static TrExportOperationRefineInClosedLine build(String title, CtuluUI ui, EfGridInterface grid,
      TrVisuPanel visu) {
    if (visu == null) { return new TrExportOperationRefineInClosedLine(title, ui, grid); }
    return new TrExportOperationRefineInClosedLine(title, visu);
  }

  /**
   * @param title
   * @param ui
   * @param grid
   */
  public TrExportOperationRefineInClosedLine(String title, CtuluUI ui, EfGridInterface grid) {
    super(ui, grid, title);
  }

  /**
   * @param title
   * @param panel
   */
  public TrExportOperationRefineInClosedLine(String title, TrVisuPanel panel) {
    super(title, panel);
  }

  @Override
  public MvExportOperationBuilderInterface getBuilder(final EfGridData src) {
    return new Builder((LinearRing) mvLineChooser.getSelectedLine(), src);
  }

}
