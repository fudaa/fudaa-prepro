package org.fudaa.fudaa.tr.post;

import org.fudaa.fudaa.tr.post.persist.TrPostSourceReplayPersist;

/**
 * @author deniger
 * 
 * Interface pour les sources construites a partir d'autres source.
 *
 */
public interface TrPostSourceBuilt extends TrPostSource {

  public TrPostSourceReplayPersist getReplay();

}
