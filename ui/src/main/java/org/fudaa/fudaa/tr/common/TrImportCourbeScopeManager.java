package org.fudaa.fudaa.tr.common;

import java.util.List;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.telemac.io.ScopeStructure;
import org.fudaa.dodico.telemac.io.ScopeStructure.Gene;
import org.fudaa.dodico.telemac.io.ScopeStructure.SorT;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.fudaa.tr.post.profile.MvProfileTreeModel;

/**
 * Manager pour gerer l'importation de courbes au format scope.
 * Manage to manage the importing of curves to scope format
 * @author Adrien Hadoux
 */
public class TrImportCourbeScopeManager implements TrCourbeImporter.Target {

  EGGraphe graphe_;

  /**
   * indique si la courbe a importer est correcte
   * To indicate  if the curve to import is right
   */
  private final boolean enabled = true;
  TrCommonImplementation impl_;

  public TrImportCourbeScopeManager(final EGGraphe _graphe, final TrCommonImplementation _impl) {
    super();
    graphe_ = _graphe;
    impl_ = _impl;

  }

  @Override
  public void importCourbes(final EvolutionReguliereInterface[] _crb, final CtuluCommandManager _mng,
      final ProgressionInterface _prog) {

  // do nothing

  }

  public void importCourbes(final ScopeStructure _crb, final CtuluCommandManager _mng, final ProgressionInterface _prog) {


    if (_crb instanceof ScopeStructure.SorT) importCourbesSorT((SorT) _crb, _mng, _prog);
    else importCourbesGENE((Gene) _crb, _mng, _prog);
    // }
  }

  /**
   * Methode d import depuis format scope S ou t
   * Method of import since scope format S or t
   * @param _crb
   * @param _mng
   * @param _prog
   */
  public void importCourbesSorT(final ScopeStructure.SorT s, final CtuluCommandManager _mng,
      final ProgressionInterface _prog) {

    final EvolutionReguliere[] evolReg = new EvolutionReguliere[s.getNbVariables()];

    // -- on parcours toutes les variables --//
	// We browse all the variables
    for (int i = 0; i < s.getNbVariables(); i++) {
      final String nomVar = s.getVariable(i);

      final List<Double> listeX = s.getAllX();
      final List<Double> listeY = s.getListValueForVariable(i);

      final double[] tabX = new double[listeX.size()];
      final double[] tabY = new double[listeY.size()];

      for (int j = 0; j < listeX.size(); j++) {
        tabX[i] = listeX.get(i);
        tabY[i] = listeY.get(i);

      }

      evolReg[i] = new EvolutionReguliere(tabX, tabY, false);

    }

  }

  /**
   * Methode d import depusi scope GENE
   * Import methods from GENE scope
   * 
   * @param _crb
   * @param _mng
   * @param _prog
   */
  public void importCourbesGENE(final ScopeStructure.Gene s, final CtuluCommandManager _mng,
      final ProgressionInterface _prog) {

  }

  @Override
  public boolean isSpatial() {
    return graphe_.getModel() instanceof MvProfileTreeModel;
  }

}
