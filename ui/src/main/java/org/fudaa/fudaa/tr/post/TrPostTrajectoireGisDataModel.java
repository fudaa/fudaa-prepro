/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.collection.CtuluArrayBoolean;
import org.fudaa.ctulu.collection.CtuluListDouble;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.dodico.ef.operation.EfTrajectoireParameters;
import org.fudaa.dodico.ef.operation.EfTrajectoireResultBuilder;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * Ce dataModel est utilise pour transformer les resultats des calcul de trajectoire en donnees sig
 * 
 * @author deniger
 */
public class TrPostTrajectoireGisDataModel implements GISDataModel {

  List<GISAttributeInterface> att_;
  GISPolyligne[] ligne_;
  Map<GISAttributeInterface, List> values_;

  @SuppressWarnings("unchecked")
  protected static TrPostTrajectoireGisDataModel build(List<EfTrajectoireResultBuilder> _res,
      List<GISAttributeInterface> _att, Map<GISAttributeInterface, CtuluVariable> _corresp, CtuluAnalyze _analyse) {
    List<GISPolyligne> poly = new ArrayList<GISPolyligne>(_res.size());
    int nbRemove = 0;
    EfTrajectoireParameters param = null;
    Map<GISAttributeInterface, List> values = new HashMap<GISAttributeInterface, List>(_att.size());
    for (EfTrajectoireResultBuilder res : _res) {
      if (param == null) param = res.getParameters();
      List<Coordinate> coords = res.getCoords();
      if (coords.size() < 2) {
        nbRemove++;
      } else {
        poly.add((GISPolyligne) GISGeometryFactory.INSTANCE.createLineString((Coordinate[]) coords
            .toArray(new Coordinate[coords.size()])));
        for (GISAttributeInterface attribute : _att) {

          List dest = values.get(attribute);
          if (dest == null) {
            dest = new ArrayList(_res.size());
            values.put(attribute, dest);
          }
          CtuluVariable v = _corresp.get(attribute);
          if (v == H2dVariableType.TEMPS) {
            dest.add(new CtuluListDouble(res.getTimes()));
          } else if (v == TrPostTrajectoireComputeAndDisplayActivity.MARK) {
            dest.add(new CtuluArrayBoolean((Boolean[]) res.getMarqueurs().toArray(
                new Boolean[res.getMarqueurs().size()])));
          } else {
            dest.add(new CtuluListDouble(res.getVarValues().get(v)));
          }

        }
      }

    }
    if (poly.isEmpty()) {
      _analyse.addFatalError(TrLib.getString("Aucune ligne � tracer"));
    }
    if (nbRemove > 0) {
      if (param.isTrajectoire()) {
        _analyse.addWarn(TrResource.getS("{0} trajectoire(s) enlev�e(s) car vide(s)", CtuluLibString
            .getString(nbRemove)), -1);
      } else {
        _analyse.addWarn(TrResource.getS("{0} ligne(s) de courant enlev�e(s) car vide(s)", CtuluLibString
            .getString(nbRemove)), -1);
      }
    }
    TrPostTrajectoireGisDataModel model = new TrPostTrajectoireGisDataModel();
    model.att_ = _att;
    model.ligne_ = (GISPolyligne[]) poly.toArray(new GISPolyligne[poly.size()]);
    model.values_ = values;
    return model;

  }

  private TrPostTrajectoireGisDataModel() {

  }

  @Override
  public GISAttributeInterface getAttribute(int _idxAtt) {
    return att_.get(_idxAtt);
  }

  @Override
  public double getDoubleValue(int _idxAtt, int _idxGeom) {
    FuLog.error("non possible");
    return 0;
  }

  @Override
  public Envelope getEnvelopeInternal() {
    FuLog.error("pas calcule");
    return null;
  }
  
  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if(xyToAdd==null){
      return this;
    }
    TrPostTrajectoireGisDataModel model = new TrPostTrajectoireGisDataModel();
    model.att_ = this.att_;
    model.ligne_ = new GISPolyligne[ligne_.length];
    for(int i=0;i<ligne_.length;i++){
      model.ligne_[i]=ligne_[i].createTranslateGeometry(xyToAdd);
    }
    model.values_ = this.values_;
    return model;
  }

  @Override
  public Geometry getGeometry(int _idxGeom) {
    return ligne_[_idxGeom];
  }

  @Override
  public int getIndiceOf(GISAttributeInterface _att) {
    return att_.indexOf(_att);
  }

  @Override
  public int getNbAttributes() {
    return att_.size();
  }

  @Override
  public int getNumGeometries() {
    return ligne_.length;
  }

  @Override
  public Object getValue(int _idxAtt, int _idxGeom) {
    GISAttributeInterface att = att_.get(_idxAtt);
    return values_.get(att).get(_idxGeom);
  }

  @Override
  public void preload(GISAttributeInterface[] _att, ProgressionInterface _prog) {}

}
