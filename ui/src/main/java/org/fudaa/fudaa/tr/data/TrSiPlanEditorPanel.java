/*
 *  @creation     6 avr. 2005
 *  @modification $Date: 2008-02-01 14:43:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuCheckBox;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.ebli.calque.edition.ZEditorLigneBriseePanel;

/**
 * Un panneau d'�dition d'un profil qui s'affiche lorsque l'outil ligne bris�e est s�lectionn�.
 * Ce panneau reprend le panneau standard d'edition de ligne bris�e auquel il ajoute un checkbox pour preciser s'il
 * s'agit d'un profil.
 * 
 * @author Fred Deniger
 * @version $Id: TrSiPlanEditorPanel.java,v 1.10.8.1 2008-02-01 14:43:26 bmarchan Exp $
 */
public class TrSiPlanEditorPanel extends ZEditorLigneBriseePanel /* implements H2DSiProfilListener */{

  protected class SiPlanTableModel extends CoordinatesModel {

    protected SiPlanTableModel() {}

    @Override
    public int getColumnCount() {
      return (cb_.isEnabled() && cb_.isSelected()) ? 4 : 3;
    }
  }

  @Override
  protected CoordinatesModel createValueModel() {
    if (isParametersEditable()) { return new SiPlanTableModel(); }
    return null;
  }

  boolean initValueForCb_;

  /**
   * @param _editor
   */
  public TrSiPlanEditorPanel(final TrGisProjectEditor _editor) {
    super(_editor);

    final TrSiProfilLayer layer = (TrSiProfilLayer) _editor.getTrPanel().getLayerSiProfiles();
    if (layer == null) {
      initValueForCb_ = true;
    } else {
      final TrSiProfilModel model = layer.getProfilModel();
      initValueForCb_ = model == null || !model.isProfilLineDefined();
    }
    if (cb_ != null) {
      cb_.setSelected(initValueForCb_);
    }
  }

  @Override
  public void objectAdded() {
    if (cb_.isSelected()) {
      cb_.setSelected(false);
    }
    super.objectAdded();
  }

  BuCheckBox cb_;

  public boolean isProfil() {
    return cb_.isEnabled() && cb_.isSelected();
  }

  protected void updateTz() {
    if (editorComps_ == null || editorComps_.size() == 0) { return; }
    editorComps_.get(1).setEnabled(cb_.isSelected());
    if (editorComps_.get(1).isEnabled()) {
      attributes_[1].getEditor().setValue(attributes_[1].getDefaultValue(), editorComps_.get(1));
    } else {
      attributes_[1].getEditor().setValue(CtuluLibString.EMPTY_STRING, editorComps_.get(1));
    }
  }

  @Override
  protected void updateFeaturePanel() {
    if (cb_ == null) {
      cb_ = new BuCheckBox(H2dResource.getS("Profil"));
      cb_.addItemListener(new ItemListener() {

        @Override
        public void itemStateChanged(final ItemEvent _e) {
          updateTz();
          final AbstractTableModel model = TrSiPlanEditorPanel.this.getTableModel();
          if (model != null) {
            if (cb_.isSelected()) {
              model.fireTableRowsInserted(2, 2);
            } else {
              model.fireTableRowsDeleted(2, 2);
            }
          }
        }
      });
      cb_.setSelected(initValueForCb_);
    }
    add(cb_);
    super.updateFeaturePanel();
    updateTz();
  }

}
