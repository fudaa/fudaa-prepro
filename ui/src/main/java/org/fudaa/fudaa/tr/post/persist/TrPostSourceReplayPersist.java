package org.fudaa.fudaa.tr.post.persist;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;

/**
 * @author deniger
 *
 */
public interface TrPostSourceReplayPersist {

  public abstract TrPostSource replay(TrPostProjet projet, CtuluAnalyze log);

}