package org.fudaa.fudaa.tr.post;

import java.util.Arrays;
import java.util.Collection;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGCourbePersistBuilder;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.courbe.EGModelTransformed;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTime;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;

public class TrPostCourbe extends FudaaCourbeTime {

  /**
   * @param m
   * @param model
   * @param timeModel
   */
  public TrPostCourbe(EGGroup m, EGModel model, FudaaCourbeTimeListModel timeModel) {
    super(m, model, timeModel);
  }

  @Override
  protected EGCourbePersistBuilder<FudaaCourbeTime> createPersistBuilder() {
    return new TrPostCourbePersistBuilder();
  }

  @Override
  public Collection<EGCourbe> getAssociatesCourbesForExport() {
    EGModel model = getModel();
    if (model instanceof TrPostCourbeModel) {
      TrPostCourbeModel postModel = (TrPostCourbeModel) model;
      double x = postModel.getPtX();
      double y = postModel.getPtY();
      EGCourbe courbeX = new EGCourbeSimple(getAxeY(), new FixedYModel(postModel, x, postModel.getTitle() + " X"));
      EGCourbe courbeY = new EGCourbeSimple(getAxeY(), new FixedYModel(postModel, y, postModel.getTitle() + " Y"));
      return Arrays.asList(courbeX, courbeY);
    }
    return super.getAssociatesCourbesForExport();
  }

  private static class FixedYModel extends EGModelTransformed {

    private final double y;
    private final String title;

    public FixedYModel(EGModel modeleInitial, double y, String title) {
      super(modeleInitial);
      this.y = y;
      this.title = title;
    }

    @Override
    public String getTitle() {
      return title;
    }

    @Override
    public double getY(int _idx) {
      return y;
    }

    @Override
    public double getYMax() {
      return y;
    }

    @Override
    public double getYMin() {
      return y;
    }

  }

}
