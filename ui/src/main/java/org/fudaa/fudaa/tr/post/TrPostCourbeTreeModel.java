/*
 * @creation 17 ao�t 2005
 *
 * @modification $Date: 2007-05-04 14:01:52 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuDialogChoice;
import com.memoire.bu.BuLib;
import com.memoire.fu.FuComparator;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPrecision;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.courbe.*;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeModel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrCourbeImporter.Target;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.actions.TrPostCourbeAddVariableAction;
import org.fudaa.fudaa.tr.post.persist.TrPostReloadParameter;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.*;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.*;

@SuppressWarnings("serial")
public class TrPostCourbeTreeModel extends EGGrapheTreeModel implements TrPostTimeContentListener, Target, TrPostTimeFormatListener {
  private static int cptGraphe = 0;

  public static String getNewName() {
    cptGraphe++;
    return TrLib.getString("Graphe") + " " + cptGraphe++;
  }

  FudaaCommonImplementation impl_;
  TrPostProjet projet_;
  protected EGGroup updating_;
  final Map varGroup_;
//  Set varSupported_;

  /**
   * NE PAS UTILISER POUR L'INSTANT SAUVEGARDE.
   */
  public TrPostCourbeTreeModel() {
    // src_ = null;
    varGroup_ = new HashMap();
    impl_ = null;
  }

  /**
   * @param _projet
   */
  public TrPostCourbeTreeModel(final TrPostProjet _projet) {
    varGroup_ = new HashMap();
    projet_ = _projet;
    impl_ = projet_.getImpl();
  }


  protected void updateTimeFmt(final TrPostSource _src) {
    if (_src.getTimeFormatter() != null) {
      updateAxeXFormatter(_src.getTimeFormatter());
    }
    if (!_src.getTime().containListener(this)) {
      _src.getTime().addListener(this);
    }
  }



  /**
   * Rejoue les donn�es du point avec les nouvelles infos. Cree syst�matiquement une nouvelle courbe avec le mod�le qui convient. Dans le cas ou
   * ecraser est � true, on supprime le modele pass� en paramtere. Version r�serv�s aux points r�els
   *
   * @param _model
   * @param _src
   * @param _idxToAdd
   * @param _cmd
   * @param _prog
   * @param newVariable
   * @param ecraser
   */
  public void replayPoints(TrPostCourbeModel _model, final TrPostSource _src, final int _idxToAdd, final CtuluCommandContainer _cmd,
                           final ProgressionInterface _prog, H2dVariableType newVariable, boolean ecraser) {
    EGCourbeChild courbeToRemove = null;
    if (ecraser) {
      courbeToRemove = getCourbeWithModel(_model);
    }
    final EGGroup g = getGroupFor(newVariable);
    EGCourbeChild courbe = TrPostCourbeModelBuilder.buildCourbeModelOnNewSource(newVariable, _idxToAdd, g, _src, _model.getSource());
    ((TrPostCourbeModel) courbe.getModel()).updateY();
    if (courbeToRemove != null) {
      courbeToRemove.copyGraphicsConfiguration(courbe);
      getGroupFor(_model.getVar()).removeEGComponent(courbeToRemove);
    }
    fireStructureChanged();
    fireAxeAspectChanged(courbe.getAxeY());
    updateTimeFmt(_src);
  }

  public void replayBilans(TrPostCommonImplementation impl, TrPostVisuPanel vue2d, TrPostCourbeBilanModel _model,
                           final TrPostSource _src, final int[] _idxToAdd, final CtuluCommandContainer _cmd,
                           final ProgressionInterface _prog, H2dVariableType newVariable, int startTimeStep, int endTimeStep, boolean ecraser, String title) {
    // -- creation de la polyligne --//
    GISZoneCollectionPoint points = new GISZoneCollectionPoint();
    for (int i = 0; i < _idxToAdd.length; i++) {
      points.add(_src.getGrid().getPtX(_idxToAdd[i]), _src.getGrid().getPtY(_idxToAdd[i]), 0);
    }
    replayBilans(impl, vue2d, _model, _src, points, _cmd, _prog, newVariable, startTimeStep, endTimeStep, ecraser, title);
  }

  public void replayBilans(TrPostCommonImplementation impl, TrPostVisuPanel vue2d, TrPostCourbeBilanModel _model,
                           final TrPostSource _src, final List<GrPoint> _idxToAdd, final CtuluCommandContainer _cmd,
                           final ProgressionInterface _prog, H2dVariableType newVariable, int startTimeStep, int endTimeStep, boolean ecraser, String title) {
    // -- creation de la polyligne --//
    GISZoneCollectionPoint points = new GISZoneCollectionPoint();
    for (GrPoint point : _idxToAdd) {
      points.add(point.x_, point.y_, point.z_);
    }

    replayBilans(impl, vue2d, _model, _src, points, _cmd, _prog, newVariable, startTimeStep, endTimeStep, ecraser, title);
  }

  public void replayBilans(TrPostCommonImplementation impl, TrPostVisuPanel vue2d, TrPostCourbeBilanModel _model,
                           final TrPostSource _src, final GISZoneCollectionPoint points, final CtuluCommandContainer _cmd,
                           final ProgressionInterface _prog, H2dVariableType newVariable, int startTimeStep, int endTimeStep, boolean ecraser, String title) {
    LineString polyligne = new LineString(points.getAttachedSequence(), GISGeometryFactory.INSTANCE);
    // -- ajout de la variable --//
    final EGGroup groupVar = getGroupFor(newVariable);
    TrPostCourbeBilanModel bilanModel = new TrPostCourbeBilanModel();

    bilanModel.source_ = _src;
    bilanModel.var_ = newVariable;
    bilanModel.setTitle(title);
    bilanModel.setMinTimeIdx(startTimeStep);
    bilanModel.setMaxTimeIdx(endTimeStep);
    bilanModel.setLine(polyligne);
    bilanModel.setSeuil(_model.getSeuil());
    bilanModel.setVectoriel(_model.isVectoriel());
    bilanModel.setComputation(_model.getComputation());
    bilanModel.updateData(_prog);
    final TrPostCourbe newCourbe = new TrPostCourbe(groupVar, bilanModel, _src.getNewTimeListModel());
    groupVar.addEGComponent(newCourbe);
    // -- on cree la nouvelle courbe pour la variable correspondante --//
    // -- si ecraser, on supprime la courbe --//
    if (ecraser) {
      removeModele(_model, _cmd);
    }

    fireStructureChanged();
  }

  /**
   * Rejoue les donn�es du point avec les nouvelles infos. Cree syst�matiquement une nouvelle courbe avec le mod�le qui convient. Dans le cas ou
   * ecraser est � true, on supprime le modele pass� en paramtere. Version r�serv�s aux points r�els
   *
   * @param _model
   * @param _src
   * @param _idxToAdd
   * @param _cmd
   * @param _prog
   * @param newVariable
   * @param ecraser
   */
  public void replayPoints(TrPostCourbeModel _model, final TrPostSource _src, final TrPostInterpolatePoint _idxToAdd,
                           final CtuluCommandContainer _cmd, final ProgressionInterface _prog, H2dVariableType newVariable, boolean ecraser) {

    // -- ajout de la variable --//
//    createVarSupported();
//    varSupported_.add(newVariable);
    final EGGroup g = getGroupFor(newVariable);
    EGCourbeChild courbeToRemove = null;
    if (ecraser) {
      courbeToRemove = getCourbeWithModel(_model);
    }
    EGCourbeChild courbe = TrPostCourbeModelBuilder.buildCourbeModelOnNewSource(newVariable, _idxToAdd, g, _src, _model.getSource());
    ((TrPostCourbeModel) courbe.getModel()).updateY();
    if (courbeToRemove != null) {
      courbeToRemove.copyGraphicsConfiguration(courbe);
      getGroupFor(_model.getVar()).removeEGComponent(courbeToRemove);
    }
    fireStructureChanged();
    fireAxeAspectChanged(courbe.getAxeY());
    updateTimeFmt(_src);
  }

  /**
   * Methode qui supprime la courbe pour son modele du profil spatial. Supprime �galement le groupe de variable si ce dernier ne contient pas d'autres
   * courbes.
   *
   * @param model
   * @param mng
   */
  public void removeModele(AbstractPostCourbeModel model, CtuluCommandContainer mng) {

    // -- on recupere le groupe du modele --//
    EGGroup group = (EGGroup) varGroup_.get(model.var_);
    EGCourbeChild[] liste = new EGCourbeChild[1];
    boolean trouve = false;
    for (int i = group.getChildCount() - 1; !trouve && i >= 0; i--) {
      final AbstractPostCourbeModel res = (AbstractPostCourbeModel) group.getCourbeAt(i).getModel();
      if (res == model) {
        liste[0] = group.getCourbeAt(i);
        trouve = true;
      }
    }
    if (trouve) {
      removeCurves(liste, mng);
    }

    // -- test si le groupe ne contient rien, on le degage --//
    if (group.getEGChilds() == null || group.getEGChilds().length == 0) {
      this.remove(group);
      this.varGroup_.remove(model.var_);
    }
  }

  /**
   * Refonte de la methode addpoints pour gerer les fusions de graphes. fusionne les variables des 2 graphes model afin de recuperer toutes les
   * variabels.
   *
   * @author Adrien Hadoux
   */
  public void addPoints(final TrPostSource _src, final TrPostInterpolatePoint _idxToAdd, final CtuluCommandContainer _cmd,
                        final ProgressionInterface _prog, final Set variablesGrapheMerge) {
    startUpdating();
    // pour le undo
    final List modelToUpdate = new ArrayList();
    final List courbeToMem = _cmd == null ? null : new ArrayList();
    final List groupToMem = _cmd == null ? null : new ArrayList();
//    this.intepolPt_.add(_idxToAdd);

    // -- completer les variables du graphe avec celles du graphe merg� --//
//    if (variablesGrapheMerge != null) {
//      for (final Iterator it = variablesGrapheMerge.iterator(); it.hasNext();) {
//        final H2dVariableType t = (H2dVariableType) it.next();
//        varSupported_.add(t);
//      }
//    }

    for (final Iterator it = variablesGrapheMerge.iterator(); it.hasNext(); ) {
      final H2dVariableType t = (H2dVariableType) it.next();
      final EGGroup g = getGroupFor(t);
      final TrPostCourbeModel c = containsCourbeFor(t, g, _idxToAdd);
      if (c == null) {
        final EGCourbeChild courbe = TrPostCourbeModelBuilder.buildCourbeModel(t, _idxToAdd, g, _src);
        if (courbeToMem != null) {
          courbeToMem.add(courbe);
          groupToMem.add(g);
        }
        modelToUpdate.add(courbe.getModel());
      } else if (c.isYNull()) {
        modelToUpdate.add(c);
      }
    }
    if (courbeToMem != null && _cmd != null) {
      _cmd.addCmd(new CommandAddCourbesMulti((EGCourbeChild[]) courbeToMem.toArray(new EGCourbeChild[courbeToMem.size()]), (EGGroup[]) groupToMem
          .toArray(new EGGroup[groupToMem.size()])) {
        @Override
        public void redo() {
          super.redo();
//          intepolPt_.add(_idxToAdd);
          reupdateGroup();
        }

        @Override
        public void undo() {
          super.undo();
//          intepolPt_.remove(_idxToAdd);
          reupdateGroup();
        }
      });
    }

    updateCurves((AbstractPostCourbeModel[]) modelToUpdate.toArray(new AbstractPostCourbeModel[modelToUpdate.size()]), _prog);
    reupdateGroup();
  }

  /**
   * Ajoute une variable par rapport a une courbe evolution temporelle selectionnee. La variable a ete choisie ainsi que le fichier source.
   *
   * @param modelCourbe
   * @param _src
   * @param _var
   * @param _prog
   * @param _cmd
   */
  public void addVariables(TrPostCourbeModel modelCourbe, final TrPostSource _src, final List _var, final ProgressionInterface _prog,
                           final CtuluCommandContainer _cmd) {

    startUpdating();

    // fusion graphes
    final TrPostCourbeModel[] model = new TrPostCourbeModel[_var.size()];
    int idx = 0;
    final List courbeToMem = new ArrayList();
    final List groupToMem = new ArrayList();
    for (int i = 0; i < _var.size(); i++) {
      final H2dVariableType var = (H2dVariableType) _var.get(i);
      final EGGroup g = getGroupFor(var);

      // -- cas reel --//
      if (modelCourbe instanceof TrPostCourbeModel.Node) {
        final EGCourbeChild c = TrPostCourbeModelBuilder.buildCourbeModel(var, ((Integer) modelCourbe.getSelectionPoint()).intValue(), g, _src);
        courbeToMem.add(c);
        groupToMem.add(g);
        model[idx++] = (TrPostCourbeModel) c.getModel();
      } else {
        // -- cas point interpol� --//

        final TrPostInterpolatePoint element = (TrPostInterpolatePoint) modelCourbe.getSelectionPoint();
        final EGCourbeChild c = TrPostCourbeModelBuilder.buildCourbeModel(var, element, g, _src);
        courbeToMem.add(c);
        groupToMem.add(g);
        model[idx++] = (TrPostCourbeModel) c.getModel();
      }
    }
    updateCurves(model, _prog);
    reupdateCourbes();
    if (_cmd != null) {
      final CtuluCommandComposite cmp = new CtuluCommandComposite() {
        @Override
        protected void actionToDoAfterUndoOrRedo() {
          reupdateGroup();
        }

        @Override
        protected boolean isActionToDoAfterUndoOrRedo() {
          return true;
        }
      };
      cmp.addCmd(new CommandAddCourbesMulti((EGCourbeChild[]) courbeToMem.toArray(new EGCourbeChild[0]), (EGGroup[]) groupToMem
          .toArray(new EGGroup[0])));

      _cmd.addCmd(cmp);
    }
  }

  private void buildUpdating() {
    if (updating_ == null) {
      updating_ = new EGGroup();
      updating_.setTitle(TrResource.getS("Mise � jour"));
    }
  }

  @Override
  public boolean canDeleteCourbes() {
    return true;
  }

  protected TrPostCourbeModel containsCourbeFor(final H2dVariableType _t, final EGGroup _g, final int _idx) {
    for (int i = _g.getChildCount() - 1; i >= 0; i--) {
      EGModel model = _g.getCourbeAt(i).getModel();
      if (model instanceof TrPostCourbeModel) {
        final TrPostCourbeModel res = (TrPostCourbeModel) model;
        if (_t == res.getVar() && res.isCourbeForObject(_idx)) {
          return res;
        }
      }
    }
    return null;
  }

  protected TrPostCourbeModel containsCourbeFor(final H2dVariableType _t, final EGGroup _g, final TrPostInterpolatePoint _idx) {
    for (int i = _g.getChildCount() - 1; i >= 0; i--) {
      EGModel model = _g.getCourbeAt(i).getModel();
      final TrPostCourbeModel res = (TrPostCourbeModel) model;
      if (_t == res.getVar() && res.isCourbeForInterpolation(_idx)) {
        return res;
      }
    }
    return null;
  }

  /**
   * Il n'y a rien a faire.
   */
  public void dataAdded(final boolean _isFleche) {
  }

  public void dataChanged(final H2dVariableType _old, final H2dVariableType _new, final boolean _contentChanged, final boolean _isFleche,
                          final Set _varsUsing, final ProgressionInterface _prog) {
    if (_isFleche) {
      return;
    }
//    varSupported_.remove(_old);
//    varSupported_.add(_new);
    // si le contenu n'est pas modifie, il faut juste changer les pointeurs des variables
    if (_contentChanged || _old == _new) {
      final EGCourbe[] cs = getCourbes();
      if (cs == null) {
        return;
      }
      // rebuild a true si on doit reconstruire le tout
      final List modelToUpdate = new ArrayList();
      for (int i = cs.length - 1; i >= 0; i--) {
        final TrPostCourbeModel model = (TrPostCourbeModel) cs[i].getModel();
        if (model.getVar() == _old) {
          model.setY(null);
          model.setVar(_new);
          modelToUpdate.add(model);
        } else if (_varsUsing != null && _varsUsing.contains(model.getVar())) {
          model.setY(null);
          modelToUpdate.add(model);
          startUpdating();
        }
      }
      // mise a jour des modeles dans un thread s�par�.
      if (modelToUpdate.size() > 0) {

        updateCurves((TrPostCourbeModel[]) modelToUpdate.toArray(new TrPostCourbeModel[modelToUpdate.size()]), _prog);
        reupdateGroup();
        setUpdating(false);
      }
    } else {
      final ArrayList crbUsingOld = new ArrayList();
      getCourbeFor(_old, crbUsingOld);
      for (final Iterator iter = crbUsingOld.iterator(); iter.hasNext(); ) {
        final TrPostCourbeModel model = (TrPostCourbeModel) ((EGCourbe) iter.next()).getModel();
        // on change le pointeur
        model.setVar(_new);
      }
    }
  }

  public void dataRemoved(final H2dVariableType[] _vars, final boolean _isFleche) {
    if (_isFleche || _vars == null) {
      return;
    }
//    varSupported_.removeAll(Arrays.asList(_vars));
    final List courbeToRemove = new ArrayList();
    for (int i = _vars.length - 1; i >= 0; i--) {
      getCourbeFor(_vars[i], courbeToRemove);
    }
    removeCurves((EGCourbeChild[]) courbeToRemove.toArray(new EGCourbeChild[courbeToRemove.size()]), null);
  }

  @Override
  protected EGCourbeChild duplicateCourbe(final EGCourbeChild _c) {
    return null;
  }

  /**
   * OVERIDE. methode qui demande a l user de choisir sa courbe puis genere une copie avec la source en cours.
   *
   * @param _child
   */
  @Override
  protected EGCourbeChild duplicateWithChooseSrc(final EGCourbeChild _child) {

    // -- mise en place du chooser des projets sources disponibles --//

    // -- recuperation du source voulu --//
    // -- recuperation de la liste des src sous forme de liste --//
    final String[] values = projet_.getListSources();

    // -- ouverture du chooser --//
    final BuDialogChoice chooser = new BuDialogChoice(projet_.impl_.getApp(), projet_.impl_.getInformationsSoftware(),
        TrResource.getS("Cr�ation d'un calque"), TrResource.getS("S�lectionnez le jeux de donn�es "), values);

    final int response = chooser.activate();

    if (response == 0) {

      final int indiceSOURCESelect = chooser.getSelectedIndex();

      final TrPostSource srcChoisie = projet_.getSource(indiceSOURCESelect);

      EGCourbeChild newCurve = null;

      // -- recuperation du point interpol� ou non selon le cas --//

      // -- CAS COURBE REELLE --//
      final TrPostCourbeModel modelBase = (TrPostCourbeModel) _child.getModel();
      final H2dVariableType var = modelBase.getVar();

      // -- verification si le point du model choisi est reel ou interpol�
      // dans la nouvelle source --//
      //
      // // -- CREATION DE LA COURBE REELE DEPUIS COURBE REELLE --//
      // -- on recupere le point le plus proche pour interpoler --//
      final GrPoint pointAInterpole = new GrPoint(modelBase.getPtX(), modelBase.getPtY(), 0);
      final int newPtIdx = TrIsoLayerDefault.sondeSelection(pointAInterpole, srcChoisie.getGrid());

      if (newPtIdx != -1) {

        final TrPostInterpolatePoint interpolatePoint = new TrPostInterpolatePoint(newPtIdx, pointAInterpole.x_, pointAInterpole.y_,
            new GISPrecision());

        // -- CREATION DE LA COURBE INTERPOLEE DEPUIS COURBE REELLE --//
        newCurve = TrPostCourbeModelBuilder.buildCourbeModel(modelBase.getVar(), interpolatePoint, _child.getParentGroup(), srcChoisie);
      }

      // -- recuperation des y de son duplicata --//
      if (newCurve != null) {
        ((TrPostCourbeModel) newCurve.getModel()).setY(new double[modelBase.getY().length]);

        final double[] ds = modelBase.getTime();
        for (int i = 0; i < ds.length; i++) {
          ((TrPostCourbeModel) newCurve.getModel()).updateY();
        }

        newCurve.setTitle(newCurve.getTitle() + " " + TrLib.formatName(projet_.getSource(indiceSOURCESelect).getTitle()));
      }
      return newCurve;
    }
    return null;
  }

  /**
   * Determine les courbes qui correspondent a la variable et les stocke dans la liste _l.
   *
   * @param _var
   * @param _l contient toutes les courbes qui appartiennent a la variable
   */
  void getCourbeFor(final H2dVariableType _var, final List _l) {
    final EGGroup g = getGroupFor(_var);
    for (int i = g.getChildCount() - 1; i >= 0; i--) {
      if (((TrPostCourbeModel) g.getCourbeAt(i).getModel()).getVar() == _var) {
        _l.add(g.getCourbeAt(i));
      }
    }
  }

  EGCourbeChild getCourbeWithModel(final AbstractPostCourbeModel model) {
    final EGGroup g = getGroupFor(model.getVar());
    for (int i = g.getChildCount() - 1; i >= 0; i--) {
      if (g.getCourbeAt(i).getModel() == model) {
        return g.getCourbeAt(i);
      }
    }
    return null;
  }

  /**
   * Recherche ou cree un groupe correspondant a la variable pour eviter les doublons.
   *
   * @param _t
   */
  public EGGroup getGroupFor(final H2dVariableType _t) {
    if (_t == null) {
      return null;
    }
    H2dVariableType parent = _t.getParentVariable();
    if (parent == null) {
      parent = _t;
    }
    EGGroup group = (EGGroup) varGroup_.get(parent);
    if (group == null) {
      final EGAxeVertical axeV = new EGAxeVertical();
      axeV.setTitre(parent.getName());
      if (_t == H2dVariableType.SANS) {
        axeV.setTitre(TrLib.getString("Import"));
      }
      axeV.setUnite(parent.getCommonUnit());
      group = new EGGroup();
      group.setAxeY(axeV);

      if (getAxeX() == null) {
        setAxeX(new EGAxeHorizontal());
      }

      group.setTitle(parent.getName());
      varGroup_.put(parent, group);
      add(group);
    }
    return group;
  }

  private String lastDurationFormatter;

  public String getLastDurationFormatter() {
    return lastDurationFormatter;
  }

  protected void updateAxeXFormatter(CtuluNumberFormatI timeFormatter) {
    if (getAxeX() == null) {
      setAxeX(new EGAxeHorizontal());
    }
    EGAxeHorizontal axeX = getAxeX();
    axeX.setSpecificFormat(timeFormatter);
    fireAxeContentChanged(axeX);
    if (timeFormatter != null) {
      lastDurationFormatter = timeFormatter.toLocalizedPattern();
    }
  }

  public boolean existGroupFor(final H2dVariableType _t) {
    if (_t == null) {
      return false;
    }
    H2dVariableType parent = _t.getParentVariable();
    if (parent == null) {
      parent = _t;
    }
    EGGroup group = (EGGroup) varGroup_.get(parent);
    if (group == null) {
      return false;
    }
    return true;
  }

  public Set getVarSupported() {
    return varGroup_.keySet();
  }

  @Override
  public void importCourbes(final EvolutionReguliereInterface[] _crb, final CtuluCommandManager _mng, final ProgressionInterface _prog) {
    if (_crb == null) {
      return;
    }
    final EGGroup gr = getGroupFor(H2dVariableType.SANS);
    final List<EGCourbeChild> childs = new ArrayList<EGCourbeChild>(_crb.length);
    for (int i = 0; i < _crb.length; i++) {

      EGModel modelCourbe = null;
      if ((_crb[i] instanceof EvolutionReguliere) && ((EvolutionReguliere) _crb[i]).isScope_) {
        modelCourbe = new TrPostScopeCourbeModel((EvolutionReguliere) _crb[i]);
      } else {
        modelCourbe = new FudaaCourbeModel(new EvolutionReguliere(_crb[i]));
      }

      final EGCourbeChild child = new EGCourbeChild(gr, modelCourbe);
      childs.add(child);

      // -- on met a jour si la courbe est un nuage de points ou non --//
      child.setNuagePoints(_crb[i].isNuagePoints());

      gr.addEGComponent(child);
    }

    if (_mng != null) {
      _mng.addCmd(new CommandAddCourbesMulti(childs.toArray(new EGCourbeChild[childs.size()]), new EGGroup[]{gr}));
    }
    fireStructureChanged();
  }

  // protected boolean isAllTimeStep() {
  // return isAllTimeStep_;
  // }
  @Override
  public boolean isContentModifiable() {
    return false;
  }

  @Override
  public boolean isSpatial() {
    return false;
  }

  @Override
  public boolean isStructureModifiable() {
    return false;
  }

  /**
   * Methode qui permet de fusionner le model courant avec un autre model. Utilsier poru la fusion de courbes tempo et l ajout dans d autres courbes.
   *
   * @param anotherModel
   * @author Adrien Hadoux
   */
  public void mergeWithAnotherTreeModel(final TrPostCourbeTreeModel anotherModel) {

    // -- parcours de la liste des variables du graphe a fusionner --//

    for (final Iterator<Object> it = anotherModel.getVarSupported().iterator(); it.hasNext(); ) {
      final H2dVariableType var = (H2dVariableType) it.next();

      // -- on recherche le group associe a la variale dans l autre graphe sinon
      // on le cree --//
      final EGGroup group = this.getGroupFor(var);

      // -- on recupere toutes les courbes associees a la var pour le graphe
      // merges
      final EGGroup g = anotherModel.getGroupFor(var);
      for (int i = g.getChildCount() - 1; i >= 0; i--) {
        if (g.getCourbeAt(i).getModel() instanceof TrPostCourbeModel) {
          if (((TrPostCourbeModel) g.getCourbeAt(i).getModel()).getVar() == var) {

            // -- duplication de la courbe dans le groupe --//

            group.addEGComponent((EGCourbeChild) g.getCourbeAt(i).duplicate(group, new EGGrapheDuplicator()));
          }
        }
      }
    }
    this.fireStructureChanged();
  }

  /**
   * Methode qui permet de fusionner le model courant avec un autre model de type SCOPE. Utilsier poru la fusion de courbes tempo et SCOPE ajout dans
   * d autres courbes.
   *
   * @param anotherModel
   * @author Adrien Hadoux
   */
  public void mergeWithAScopeTreeModel(final TrPostScopCourbeTreeModel anotherModel) {

    // -- parcours de la liste des variables SANS du graphe SCOPE a fusionner --//
    final EGGroup group = this.getGroupFor(H2dVariableType.SANS);

    // -- on recupere toutes les courbes associees a la var pour le graphe
    // merges
    final EGGroup g = anotherModel.getGroupFor(H2dVariableType.SANS);
    for (int i = g.getChildCount() - 1; i >= 0; i--) {
      if (g.getCourbeAt(i).getModel() instanceof TrPostScopeCourbeModel) {

        group.addEGComponent((EGCourbeChild) g.getCourbeAt(i).duplicate(group, new EGGrapheDuplicator()));
      }
    }

    this.fireStructureChanged();
  }

  /**
   * Redefinie pour la visibilit�.
   */
  @Override
  protected void removeAllGroups() {
    super.removeAllGroups();
  }

  public void removeVariables(final List _var, final CtuluCommandContainer _cmd) {
    final List courbeToRemove = new ArrayList();
    for (int i = _var.size() - 1; i >= 0; i--) {
      getCourbeFor((H2dVariableType) _var.get(i), courbeToRemove);
    }
//    varSupported_.removeAll(_var);
    final CtuluCommandComposite cmp = _cmd == null ? null : new CtuluCommandComposite() {
      @Override
      protected void actionToDoAfterUndoOrRedo() {
        reupdateGroup();
      }

      @Override
      protected boolean isActionToDoAfterUndoOrRedo() {
        return true;
      }
    };
    removeCurves((EGCourbeChild[]) courbeToRemove.toArray(new EGCourbeChild[courbeToRemove.size()]), cmp);
    reupdateGroup();

  }

  private void reupdateCourbes() {
    try {
      EventQueue.invokeAndWait(new Runnable() {
        @Override
        public void run() {
          reupdateGroup();
        }
      });
    } catch (final InterruptedException e) {
      Thread.currentThread().interrupt();
    } catch (final InvocationTargetException e) {
    }
  }

  protected void reupdateGroup() {
    BuLib.invokeNow(new Runnable() {
      @Override
      public void run() {
        removeAllGroups();
        final Set groups = new HashSet();
        for (final Iterator it = varGroup_.values().iterator(); it.hasNext(); ) {
          final EGGroup g = (EGGroup) it.next();
          if (g.getChildCount() > 0) {
            groups.add(g);
          }
        }
        final int nb = groups.size();
        final EGGroup[] gs = (EGGroup[]) groups.toArray(new EGGroup[nb]);
        Arrays.sort(gs, FuComparator.STRING_COMPARATOR);
        for (int i = 0; i < nb; i++) {
          add(gs[i]);
        }
        fireStructureChanged();
      }
    });
  }

  protected void startUpdating() {
    setUpdating(true);
    super.removeAllGroups();
    buildUpdating();
    super.add(updating_);
    BuLib.invokeNow(new Runnable() {
      @Override
      public void run() {
        fireStructureChanged();
      }
    });
  }

  protected void updateCurves(final AbstractPostCourbeModel[] _models, final ProgressionInterface _prog) {
    // Etape 1: on initialise les modeles, et on les tries selon les variables
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(4, _models.length);
    for (int idxModel = _models.length - 1; idxModel >= 0; idxModel--) {
      _models[idxModel].updateY();
      up.majAvancement();
    }
  }

  @Override
  public void updateTimeStep(final int[] _idx, final ProgressionInterface _prog) {
    fireCourbeContentChanged(null);
  }

  @Override
  public EGGrapheModel duplicate(final EGGrapheDuplicator _duplicator) {
    final TrPostCourbeTreeModel duplic = new TrPostCourbeTreeModel();
    duplic.setAxeX(this.getAxeX().duplicate());

    duplic.getSelectionModel().setSelectionMode(this.getSelectionModel().getSelectionMode());

    final GrapheTreeNode root = this.getGrapheTreeNode();
    final GrapheTreeNode rootDuplique = duplic.getGrapheTreeNode();

    for (int i = 0; i < root.components_.size(); i++) {

      // -- ajout du groupe duplique --//
      duplic.add(root.getGroup(i).duplicate(_duplicator));
    }

    // -- recuperation de la reference vers le meme projet --//
    duplic.projet_ = this.projet_;
    return duplic;
  }

  /**
   * parcours la liste de ses courbes et range les bons groupes de variables correspondant
   */
  @Override
  public void finalizePersistance() {
    for (int i = 0; i < this.getNbEGObject(); i++) {

      if (this.getEGObject(i) instanceof EGGroup) {

        EGGroup groupe = (EGGroup) this.getEGObject(i);
        for (int k = 0; k < groupe.getChildCount(); k++) {
          if (groupe.getCourbeAt(k) != null) {
            EGCourbeChild courbe = groupe.getCourbeAt(k);
            if (courbe.getModel() != null && (courbe.getModel() instanceof AbstractPostCourbeModel)) {
              AbstractPostCourbeModel model = (AbstractPostCourbeModel) courbe.getModel();
              if (model.var_ != null) {
                varGroup_.put(model.var_, groupe);
              }
            } else if (courbe.getModel() != null) {
              varGroup_.put(H2dVariableType.SANS, groupe);
            }
          }
        }
      }
    }
  }

  /**
   * Methode qui affiche les infos principales de la courbe choisie
   */
  public String getSelectedSpecificCourbeInfos() {
    if (this.getSelectedObject() instanceof EGCourbe) {
      EGModel model = ((EGCourbe) this.getSelectedObject()).getModel();
      return model.savePersistSpecificDatas().toString();
    } else {
      return null;
    }
  }

  @Override
  public Object getSpecificPersitDatas(Map Params) {
    TrPostCourbeTreeModelPersist dataPersist = new TrPostCourbeTreeModelPersist();
    dataPersist.fillDataWithModel(this);
    return dataPersist;
  }

  @Override
  public void setSpecificPersitDatas(final Object specPersitData, Map infos) {
    // TODO Auto-generated method stub
    TrPostCourbeTreeModelPersist data = (TrPostCourbeTreeModelPersist) specPersitData;

    // -- remplissage du model avec les data persistantes --//
    data.fillModelWith(this);

    // -- recuperation du trpostprojet specifique au chargement --//
    TrPostProjet projet = (TrPostProjet) infos.get(TrPostReloadParameter.POST_PROJET);
    this.projet_ = projet;
    this.impl_ = projet.impl_;
    CtuluNumberFormatI durationFormatter = data.getDurationFormatter();
    if (durationFormatter != null) {
      updateAxeXFormatter(durationFormatter);
    } else {
      EGCourbe[] courbes = getCourbes();
      if (!CtuluLibArray.isEmpty(courbes)) {
        EGCourbe egCourbe = courbes[courbes.length - 1];
        EGModel egModel = egCourbe.getModel();
        if (egModel instanceof TrPostCourbeModel) {
          TrPostCourbeModel model = ((TrPostCourbeModel) egModel);
          updateAxeXFormatter(model.getSource().getTimeFormatter());
        }
      }
    }
  }

  /**
   * Les actions des courbes specifiques: l'ajout de variable.
   */
  @Override
  public List<EbliActionInterface> getSpecificsActionsCurvesOnly(EGGraphe _target, CtuluUI ui) {
    List<EbliActionInterface> listeActions = new ArrayList<EbliActionInterface>();
    // -- action d'ajout des variables --//
    listeActions.add(new TrPostCourbeAddVariableAction(projet_.impl_, _target));
    // -- action replay data --//
    listeActions.add(new EGActionReplayDataCourbe.CourbeOnly(this, this.getSelectedComponent().getModel(), ui));
    return listeActions;
  }

  @Override
  public void timeFormatChanged(TrPostTimeModel src, CtuluNumberFormatI oldFmt, CtuluNumberFormatI newFmt) {
    EGCourbe[] courbes = getCourbes();
    if (!containsCourbeWithModel(src)) {
      src.removeListener(this);
    }
    updateAxeXFormatter(src.getTimeFormatter());
  }

  private boolean containsCourbeWithModel(TrPostTimeModel timeModel) {
    EGCourbe[] courbes = getCourbes();
    for (int i = 0; i < courbes.length; i++) {
      EGCourbe egCourbe = courbes[i];
      EGModel egModel = egCourbe.getModel();
      if (egModel instanceof TrPostCourbeModel) {
        TrPostCourbeModel model = ((TrPostCourbeModel) egModel);
        if (model.getSource().getTime() == timeModel) {
          return true;
        }
      }
    }
    return false;
  }

  public void replayVolume(TrPostCommonImplementation implementation, TrPostVisuPanel vue2d, TrPostCourbeVolumeModel modele, TrPostSource source, int[] selectedElementIdx,
                           CtuluCommandManager cmdMng, ProgressionInterface mainProgression, H2dVariableType newVariable, int startTime, int endTime, boolean ecraser,
                           String title) {
    // -- ajout de la variable --//
    final EGGroup groupVar = getGroupFor(newVariable);
    TrPostCourbeVolumeModel bilanModel = new TrPostCourbeVolumeModel();

    bilanModel.source_ = source;
    bilanModel.var_ = newVariable;
    bilanModel.setTitle(title);
    bilanModel.setMinTimeIdx(startTime);
    bilanModel.setMaxTimeIdx(endTime);
    if (selectedElementIdx != null) {
      FastBitSet selectedElts = new FastBitSet();
      for (int i : selectedElementIdx) {
        selectedElts.set(i);
      }
      bilanModel.setSelectedMeshes(selectedElts);
    }
    bilanModel.setSeuil(modele.getSeuil());
    bilanModel.setComputation(modele.getComputation());
    bilanModel.updateData(mainProgression);
    final TrPostCourbe newCourbe = new TrPostCourbe(groupVar, bilanModel, source.getNewTimeListModel());
    groupVar.addEGComponent(newCourbe);
    if (ecraser) {
      removeModele(modele, cmdMng);
    }
    fireStructureChanged();
  }
}
