package org.fudaa.fudaa.tr.post.persist;

import org.fudaa.fudaa.tr.common.TrResource;

import java.io.Serializable;

/**
 * Classe qui contient un commentaire, est ecrit mais non lu
 * @author Adrien Hadoux
 *
 */
public class XmlCommentaire implements Serializable {

	String readme;

	public XmlCommentaire(String readme) {
		super();
		this.readme = readme;
	}
	
	public static XmlCommentaire getCommentaireSources(){
		String buff=TrResource.getS("Fichier listant tous les fichiers r�sultats. Pour modifier un fichier, modifier le chemin du path relatif et absolu contenu dans balise pathSource et mettre le booleen a TRUE pour indiquer qu'il faut rejouer les donnees.")+
		TrResource.getS("Pour ajouter un fichier de r�sultat dans le projet, copier/coller dans le fichier le contenu entre les balises SOURCE (balises incluses) et modifier le chemin absolu vers le fichier de r�sultats d�sir�.Attention, il faut saisir un ID unique entre les balises idSource. Si on veut que les frames calques utilisent ce fichier de r�sultat, il faut aller dans leur r�pertoire Calques et inscrire dans leur fichier descriptor l'id de source correspondant.")+
		TrResource.getS("Pour supprimer un fichier de r�sultat dans le projet, s�lectionner dans le fichier le contenu entre les balises SOURCE (balises incluses) correspondant au fichier � enlever et supprimer  le puis sauvegarder le fichier")
		;
		return new XmlCommentaire(buff);
	}
	
	public static XmlCommentaire getCommentaireLayout(){
		String buff=TrResource.getS("Fichier listant toutes les fen�tre layout. Cela pointe vers les fichiers de d�scription des layout � l'int�rieur du r�pertoire de m�me nom.")+
		TrResource.getS("Pour ajouter une fenetre de layout d'un autre projet dans ce projet, copier/coller dans le fichier le contenu entre les balises LAYOUTDESCRIPTOR (balises incluses) et modifier le chemin relatif vers le fichier de description de la fenetre  d�siree. Il faut ensuite incr�menter l'indice entre la balise int et copier le r�pertoire correspondant au layout � l'int�rieur de ce repertoire projet.")+
		TrResource.getS("Pour supprimer une fen�tre de layout dans le projet, s�lectionner dans le fichier le contenu entre les balises LAYOUTDESCRIPTOR (balises incluses) correspondant a la fenetre de layout � enlever et supprimer  le puis sauvegarder le fichier");
		return new XmlCommentaire(buff);
	}
	
	public static XmlCommentaire getCommentaireName(){
		String buff=TrResource.getS("Fichier listant les noms de toutes les frames. Pour modifier un nom de frame, il suffit de modifier le nom a l interieur de la balise nom");
		return new XmlCommentaire(buff);
	}
	
}
