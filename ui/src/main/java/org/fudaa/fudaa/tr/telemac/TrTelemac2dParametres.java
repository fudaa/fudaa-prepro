/**
 * @creation 16 avr. 2003
 * @modification $Date: 2007-05-04 14:01:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.fu.FuLog;
import gnu.trove.TDoubleArrayList;
import gnu.trove.TDoubleHashSet;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluAnalyzeGUI;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.h2d.H2dEvolutionFrontiereLiquide;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacDicoParams;
import org.fudaa.dodico.h2d.telemac.H2dTelemacEvolutionFrontiere;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.telemac.io.TelemacLiquideAbstract;
import org.fudaa.dodico.telemac.io.TelemacLiquideFileFormat;
import org.fudaa.dodico.telemac.io.TelemacSeuilSiphonContainer;
import org.fudaa.dodico.telemac.io.TelemacSeuilSiphonFileFormat;
import org.fudaa.fudaa.tr.common.TrImplementationEditorAbstract;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrTelemac2dParametres.java,v 1.32 2007-05-04 14:01:46 deniger Exp $
 */
public final class TrTelemac2dParametres extends TrTelemacCommunParametres {

  TrTelemac2dParametres(final TrImplementationEditorAbstract _ui, final H2dTelemacDicoParams _p, final File _f, final File _gridFile,
      final TrTelemacCommunProjectListener _l) {
    super(_ui, _p, _f, _gridFile, _l);
    params_.getSourceSiphonMng();
  }

  @Override
  protected boolean loadAllAction(final boolean _newProject, final boolean _b, final ProgressionInterface _prg) {
    final boolean r = super.loadAllAction(_newProject, _b, _prg);
    if (r) {
      loadSeuilSiphon(_prg);
    }

    return r;
  }

  @Override
  public boolean isAllLoaded() {
    if (!super.isAllLoaded()) {
      return false;
    }
    return isLiquidTransientConditionPresent();
  }

  private boolean loadSeuilSiphon(final ProgressionInterface _prg) {
    // les mot-cles definissant le nombre de seuils et de siphons
    // Key words defining number of thresholds and sumps
    final DicoEntite seuilKw = getTelemacFileFormatVersion().getSeuilNombreMotCle();
    final DicoEntite siphonKw = getTelemacFileFormatVersion().getSiphonNombreMotCle();

    int nbSeuil = 0;
    int nbSiphon = 0;
    if (dicoParams_.isValueSetFor(seuilKw)) {
      nbSeuil = Integer.parseInt(dicoParams_.getValue(seuilKw));
    }
    if (dicoParams_.isValueSetFor(siphonKw)) {
      nbSiphon = Integer.parseInt(dicoParams_.getValue(siphonKw));
    }

    if (nbSeuil == 0 && nbSiphon == 0) {
      if (FuLog.isWarning()) {
        FuLog.debug("FTR: No weir / culvert");
      }
      return true;
    }
    if (FuLog.isWarning()) {
      FuLog.debug("FTR: Load weir / culvert");
    }
    final DicoEntite entFile = getTelemacFileFormatVersion().getSeuilSiphonFile();
    if ((entFile == null) || (!isValueSetFor(entFile))) {
      return false;
    }
    // le fichier des seuils siphons.
    // the list of sump thresholds
    final File f = getFileSet(entFile);
    final CtuluIOOperationSynthese op = new TelemacSeuilSiphonFileFormat().read(f, nbSeuil > 0, _prg);
    getUI().manageErrorOperationAndIsFatal(op);
    final TelemacSeuilSiphonContainer result = (TelemacSeuilSiphonContainer) op.getSource();
    if (result == null) {
      return false;
    }
    // chargement des siphons
    // loading of sumps
    final CtuluAnalyze analyze = new CtuluAnalyze();
    getTelemacParametres().getSourceSiphonMng().loadSiphons(result, analyze);
    getUI().manageAnalyzeAndIsFatal(analyze);
    analyze.clear();
    // chargement des seuils
    // loading of thresholds
    getTelemacParametres().getSeuil().loadSeuil(result, analyze);
    getUI().manageAnalyzeAndIsFatal(analyze);
    if (analyze.containsWarnings()) {
      CtuluAnalyzeGUI.showDialogErrorFiltre(analyze, getImpl(), H2dResource.getS("Seuil"), false);
    }
    getState().setLoaded(entFile, f, false);
    return true;
  }

  /**
   * @return true if the file can be saved.
   */
  protected boolean saveClLiquidFile(final ProgressionInterface _prog, final File _f) {
    if (CtuluLibMessage.INFO) {
      CtuluLibMessage.info("save Liquid file in " + _f);
    }
    final List<H2dTelemacEvolutionFrontiere> liquidEvolutions = getTelemacParametres().getTelemacCLManager().getLiquideEvolution();
    if ((liquidEvolutions == null) || (liquidEvolutions.isEmpty())) {
      return true;
    }
    final H2dEvolutionFrontiereLiquide first = liquidEvolutions.get(0).getTelemacEvolution();
    final TDoubleHashSet hashSet = new TDoubleHashSet(first.getPasTempNb());
    boolean mustResize = false;
    // true si les courbes ne couvrent pas la dur�e de la simulation
    // true if curves don't cover the length of the simulation
    boolean timeResize = false;
    for (int i = liquidEvolutions.size() - 1; i > 0; i--) {
      mustResize |= !(first.isEvolutionWithSameX(liquidEvolutions.get(i).getTelemacEvolution(), hashSet));
    }
    double[] newTimes = null;
    final long max = getTelemacParametres().getComputationDuration();
    if (mustResize) {
      CtuluLibMessage.info("LIQUID CURVES ARE NOT BASED ON SAME TIMES STEP");
      newTimes = hashSet.toArray();
      Arrays.sort(newTimes);
      mustResize = false;
      if (newTimes[0] != 0) {
        mustResize = true;
        hashSet.add(0d);
      }
      if (((long) (newTimes[newTimes.length - 1])) < max) {
        mustResize = true;
        hashSet.add(max);
      }
      if (mustResize) {
        newTimes = hashSet.toArray();
        Arrays.sort(newTimes);
      }
    } else {
      if ((first.getPasDeTempsAt(0) != 0) || (first.getPasDeTempsAt(first.getPasTempNb() - 1) < max)) {
        CtuluLibMessage.info("LIQUID CURVES ARE NOT WIDTH ENOUGH");
        timeResize = true;
        final TDoubleArrayList l = new TDoubleArrayList(first.getTimeSteps());
        if (first.getPasDeTempsAt(0) != 0) {
          l.insert(0, 0);
        }
        if (first.getPasDeTempsAt(first.getPasTempNb() - 1) < max) {
          l.add(max);
        }
        newTimes = l.toNativeArray();
      }
    }
    if (newTimes != null) {
      for (H2dTelemacEvolutionFrontiere d : liquidEvolutions) {
        H2dEvolutionFrontiereLiquide evolution = d.getTelemacEvolution();
        EvolutionReguliere createEvolutionFromInterpolation = evolution.createEvolutionFromInterpolation(newTimes);
        H2dEvolutionFrontiereLiquide liq = new H2dEvolutionFrontiereLiquide(evolution.getVariableType(), evolution.getIndexFrontiere(),
            createEvolutionFromInterpolation);
        d.setEvolution(liq);

      }
    }
    String version = getDico().getVersion();
    boolean newVersion = version.toUpperCase().compareTo("V6P2") >= 0;
    if (!newVersion) {
      reindexLiquidFrontiers(liquidEvolutions);
    }
    Collections.sort(liquidEvolutions);
    final TelemacLiquideAbstract inter = new TelemacLiquideAbstract();
    inter.setEvolutionsFrontieresLiquides(liquidEvolutions.toArray(new H2dTelemacEvolutionFrontiere[liquidEvolutions.size()]));
    final CtuluIOOperationSynthese op = TelemacLiquideFileFormat.getInstance().writeClLiquid(_f, inter, _prog);
    final boolean isOk = !getUI().manageAnalyzeAndIsFatal(op.getAnalyze());
    // message si les courbes ont �t� �tendues
    // message if curves have been widespread.
    if (isOk && timeResize) {
      getUI().warn(
          H2dResource.getS("Fichier des fronti�res liquides tansitoires"),
          TrResource
              .getS("Des courbes ne couvrent pas la dur�e da la simulation.\nDes pas de temps ont �t� ajout�s au fichier pour corriger ce probl�me"),
          false);
    }
    return isOk;
  }

  private void reindexLiquidFrontiers(final List<H2dTelemacEvolutionFrontiere> liquidEvolutions) {
    int nbTraceurs = getTelemacParametres().getTelemacCLManager().getNbTraceurs();
    for (H2dTelemacEvolutionFrontiere evolution : liquidEvolutions) {
      if (evolution.isIdxVariableDefined()
          && evolution.getTelemacEvolution().getVariableType().equals(H2dVariableTransType.TRACEUR)) {
        int idxTracer=evolution.getIdxVariable();
        evolution.setIdxVariable(-1);
        int idxFrontier=evolution.getTelemacEvolution().getIndexFrontiere();
        evolution.getTelemacEvolution().setIndexFrontiere(idxFrontier*nbTraceurs+idxTracer);

      }

    }
  }

  /**
   *
   */
  @Override
  protected boolean saveDicoEntiteFileAction(final DicoEntite _entFile, final File _f, final ProgressionInterface _interface) {
    if (_entFile == getDicoFileFormatVersion().getCLLiquideEntiteFile()) {
      return saveClLiquidFile(_interface, _f);
    }
    return super.saveDicoEntiteFileAction(_entFile, _f, _interface);
  }

}