/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.replay;

import com.memoire.bu.BuWizardDialog;
import java.io.File;
import org.fudaa.ebli.courbe.EGGrapheTreeModel;
import org.fudaa.fudaa.tr.common.TrCourbeImporter;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostScopeCourbeModel;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostWizardImportScope;

/**
 *
 * @author Frederic Deniger
 */
public class TrReplayScope {

  /**
   * Constructeur d'une dialog qui affiche la vue 2d avec le point initial qui a servi a la creation du modele. Ajoute �galement la liste des
   * variables � choisir. Il est possible dans la vue de s�lectionner un autre point ou plusieurs. LA validation de la frame permettra de r�cup�rer
   * les nouveaux point ainsi que les variables qui seront utilis�es pour refaire le calcul.
   *
   * @param modele WARNING: le treemodele pour un courbe scopegene peut etre a la fois: - un ScopGeneTeeModel - un trpostcourbeTreeModel (ajout de
   * courbe scop dans une evol temporelle) - un MvTreeModel (ajout idem) Il faut donc traiter avec un EgGrapheTreeModel g�n�rique pour eviter les
   * problemes de cast.
   */
  public void getScopGeneReplayData(final EGGrapheTreeModel treeModel, final TrPostScopeCourbeModel modele,
          final TrPostCommonImplementation implementation) {

    final File fichierScop = modele.getFichierGenerateur();
    if (fichierScop == null || !fichierScop.canRead()) {
      if (fichierScop != null) {
        implementation.error(TrResource.TR.getString("Impossible de r�cup�rer le fichier scop au cehmin suivant: \n {0}",
                fichierScop.getAbsolutePath()));
      } else {
        implementation.error(TrLib.getString("Impossible de r�cup�rer le fichier scop"));
      }

      return;
    }

    // -- pr�chargement de l'interface avec les infos du fichier --//

    final TrPostWizardImportScope wizard = new TrPostWizardImportScope(implementation.getCurrentProject(),
            (TrCourbeImporter.Target) treeModel, true, fichierScop);
    final BuWizardDialog DialogWizard = new BuWizardDialog(implementation.getFrame(), wizard);
    // --affichage du wizard --//
    DialogWizard.setSize(600, 600);
    DialogWizard.setLocationRelativeTo(implementation.getFrame());
    DialogWizard.setVisible(true);

  }
}
