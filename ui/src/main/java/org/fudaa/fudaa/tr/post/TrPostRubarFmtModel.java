package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuResource;
import java.io.File;
import java.util.Map;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.fileformat.FileFormat;

/**
 * @author fred deniger
 * @version $Id: TrPostRubarFmtModel.java,v 1.3 2006-07-27 13:35:33 deniger Exp $
 */
class TrPostRubarFmtModel extends AbstractTableModel {

  final boolean[] fileToLoad_;
  final Map fmtLoadable_;
  final FileFormat[] fmtToLoad_;

  public TrPostRubarFmtModel(final FileFormat[] _fmtToLoad, final boolean[] _fileToLoad, final Map _fmtLoadable) {
    super();
    fmtToLoad_ = _fmtToLoad;
    fileToLoad_ = _fileToLoad;
    fmtLoadable_ = _fmtLoadable;
  }

  protected boolean[] getFileToLoad() {
    return fileToLoad_;
  }

  protected FileFormat[] getFmtToLoad() {
    return fmtToLoad_;
  }

  @Override
  public Class getColumnClass(final int _columnIndex) {
    if (_columnIndex == 0) {
      return Boolean.class;
    }
    return String.class;
  }

  @Override
  public int getColumnCount() {
    return 2;
  }

  @Override
  public String getColumnName(final int _column) {
    if (_column == 0) {
      return CtuluLib.getS("Charg�");
    }
    return BuResource.BU.getString("Fichier");
  }

  @Override
  public int getRowCount() {
    return fmtToLoad_ == null ? 0 : fmtToLoad_.length;
  }

  @Override
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {
    if (_columnIndex == 0) {
      return Boolean.valueOf(fileToLoad_[_rowIndex]);
    }
    final FileFormat fileFormat = fmtToLoad_[_rowIndex];
    final String[] ext = fileFormat.getExtensions();
    final File file = (File) fmtLoadable_.get(fileFormat);
    String res = (file).getName();
//    if (!CtuluLibArray.isEmpty(ext) && "OUT".equals(ext[0].toUpperCase())) {
//      File fileMas = CtuluLibFile.changeExtension(file, "mas");
//      if (!fileMas.exists()) {
//        final File upperfileMas = CtuluLibFile.changeExtension(file, "MAS");
//        if (upperfileMas.exists()) {
//          fileMas = upperfileMas;
//        }
//      }
//      if (fileMas.exists()) {
//        res += " / " + fileMas.getName();
//      }
//    }

    return res;

  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    return _columnIndex == 0;
  }

  @Override
  public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
    if (_columnIndex == 0) {
      fileToLoad_[_rowIndex] = ((Boolean) _value).booleanValue();
    }
  }

  void unselectAll() {
    for (int i = fileToLoad_.length - 1; i >= 0; i--) {
      fileToLoad_[i] = false;
    }
    fireTableRowsUpdated(0, getRowCount() - 1);
  }

  void selectAll() {
    for (int i = fileToLoad_.length - 1; i >= 0; i--) {
      fileToLoad_[i] = true;
    }
    fireTableRowsUpdated(0, getRowCount() - 1);

  }
}