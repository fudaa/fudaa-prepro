/**
 * @creation 29 avr. 2003
 * @modification $Date: 2007-05-04 14:01:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.telemac;

import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * @author deniger
 */
public class TrTelemac2dProject extends TrTelemacCommunProjet {
  /**
   * @param _params les param du projet / the param of the project
   */
  public TrTelemac2dProject(final TrTelemacCommunParametres _params) {
    super(_params);
  }

  /**
   * @return l'identifiant de la chaine de calcul / the id of the computing chain
   */
  @Override
  public String getID() {
    return FileFormatSoftware.TELEMAC_IS.name;
  }
}
