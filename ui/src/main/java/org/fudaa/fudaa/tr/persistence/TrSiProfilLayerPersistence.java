/*
 *  @creation     26 ao�t 2005
 *  @modification $Date: 2007-01-17 10:44:32 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.persistence;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISDataModelZoneAdapter;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.exporter.GISDataStoreZoneExporter;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueSaverInterface;
import org.fudaa.ebli.calque.BCalqueSaverTargetInterface;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.fudaa.sig.persistence.FSigLayerLinePersistence;
import org.fudaa.fudaa.sig.persistence.FSigLayerPointPersistence;
import org.fudaa.fudaa.tr.data.TrSIZone;
import org.fudaa.fudaa.tr.data.TrSiProfilLayer;
import org.fudaa.fudaa.tr.data.TrSiProfilModel;

/**
 * @author fred deniger
 * @version $Id: TrSiProfilLayerPersistence.java,v 1.8 2007-01-17 10:44:32 deniger Exp $
 */
public class TrSiProfilLayerPersistence extends FSigLayerLinePersistence {
  public static String getLevelUptodateId() {
    return "siLevelUptoDate";
  }

  public static String getZoneModelId() {
    return "siZoneModel";
  }

  public static String getZoneUIId() {
    return "siZoneUI";
  }

  public TrSiProfilLayerPersistence() {

  }

  @Override
  protected GISDataStoreZoneExporter createExporter() {
    final GISDataStoreZoneExporter res = super.createExporter();
    res.setUseIdAsName(true);
    return res;
  }

  @Override
  public BCalqueSaverInterface save(final BCalque calque, final ProgressionInterface _prog) {
    final BCalqueSaverInterface res = super.save(calque, _prog);
    final TrSiProfilLayer siLayer = (TrSiProfilLayer) calque;
    siLayer.getProfilModel().prepareExport();
    res.getUI().put(FSigLayerPointPersistence.getGeomId(), siLayer.getProfilModel().getGeomData());
    res.getUI().put(getLevelUptodateId(), siLayer.isLevelUpdateToDate());
    if (siLayer.getPlans() != null) {
      res.getUI().put(getZoneModelId(), siLayer.getPlans().getZone());
      res.getUI().put(getZoneUIId(), siLayer.getPlans().saveUIProperties());
    }
    return res;
  }

  @Override
  protected EbliUIProperties createPropForArk(final BCalque _cqToSave) {
    final EbliUIProperties res = super.createPropForArk(_cqToSave);
    final TrSiProfilLayer siLayer = (TrSiProfilLayer) _cqToSave;
    res.put(getLevelUptodateId(), siLayer.isLevelUpdateToDate());
    if (siLayer.getPlans() != null) {
      res.put(getZoneModelId(), siLayer.getPlans().getZone());
      res.put(getZoneUIId(), siLayer.getPlans().saveUIProperties());
    }
    return res;
  }

  @Override
  protected BCalque findCalque(final BCalqueSaverInterface _saver, final BCalqueSaverTargetInterface _pn,
                               final BCalque _parent) {
    return _parent.getCalqueParNom(TrSiProfilLayer.getDefaultName());
  }

  @Override
  public String restore(final BCalqueSaverInterface _saver, final BCalqueSaverTargetInterface _pn,
                        final BCalque _parent, final ProgressionInterface _prog) {
    final String name = _saver.getLayerName();
    if (name == null) {
      return null;
    }
    final TrSiProfilLayer thisCalque = (TrSiProfilLayer) _parent.getCalqueParNom(TrSiProfilLayer.getDefaultName());
    if (thisCalque == null) {
      FuLog.error("si layer can't be found", new Throwable());
      return name;
    }
    final EbliUIProperties ui = _saver.getUI();
    final GISZoneCollection readProfils = (GISZoneCollection) ui.get(FSigLayerPointPersistence.getGeomId());
    if (readProfils != null) {
      TrSiProfilModel.Zone zones = (TrSiProfilModel.Zone) ui.get(getZoneModelId());
      if (zones != null && zones.getNbZone() > 0) {
        final TrSIZone zone = (TrSIZone) zones.getGeometry(0);
        // c'est une ancienne version qui ne stocke pas l'indice de la zone associ�e
        if (zone.getProfileIdx() < 0) {
          zones = null;
        }
      }
      thisCalque.setLevelUptodate(ui.getBoolean(getLevelUptodateId()));
      if (readProfils.getNbAttributes() != thisCalque.getModelePoly().getGeomData().getNbAttributes()) {
        readProfils.setAttributes(thisCalque.getModelePoly().getGeomData().getAttributes(), null);
      }
      if (readProfils.getNumGeometries() > 0) {
        thisCalque.getModelePoly().getGeomData().addAll(new GISDataModelZoneAdapter(readProfils, null), null, true);
      }
      if (zones != null) {
        thisCalque.buildPlanLayer(zones);
      }
      if (ui.get(getZoneUIId()) instanceof EbliUIProperties) {
        final EbliUIProperties zonesUI = (EbliUIProperties) ui.get(getZoneUIId());
        if (zonesUI != null && thisCalque.getPlans() != null) {
          thisCalque.getPlans().initFrom(zonesUI);
        }
      }
    }
    return name;
  }
}
