/**
 * @file TrApplicationManager.java
 * @creation 8 mars 2004
 * @modification $Date: 2007-05-04 14:01:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuDynamicMenu;
import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuResource;
import com.memoire.vfs.VfsFile;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFilter;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.reflux.io.INPFileFormat;
import org.fudaa.dodico.rubar.io.RubarPARFileFormat;
import org.fudaa.dodico.telemac.TelemacDicoFileFormat;
import org.fudaa.dodico.telemac.io.ScopeGENEFileFormat;
import org.fudaa.dodico.telemac.io.ScopeSFileFormat;
import org.fudaa.dodico.telemac.io.ScopeTFileFormat;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaUI;
import org.fudaa.fudaa.commun.exec.FudaaAppliManagerImpl;
import org.fudaa.fudaa.commun.exec.FudaaEditor;
import org.fudaa.fudaa.commun.exec.FudaaExec;
import org.fudaa.fudaa.commun.impl.FudaaPanelTask;
import org.fudaa.fudaa.meshviewer.gridprocess.GridProcessTaskModel;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.tr.TrEditorImplementation;
import org.fudaa.fudaa.tr.post.TrPostAnalyze;
import org.fudaa.fudaa.tr.post.persist.TrPostFileFilter;
import org.fudaa.fudaa.tr.post.persist.TrPostFileFormat;
import org.fudaa.fudaa.tr.rubar.TrRubarImplHelper;
import org.fudaa.fudaa.tr.telemac.TrMatisseConvertGUI;

/**
 * @author Fred DENIGER
 * @version $Id: TrApplicationManager.java,v 1.32 2007-05-04 14:01:46 deniger Exp $
 */
public abstract class TrApplicationManager extends FudaaAppliManagerImpl {

  private static class FudaaExecConsole extends FudaaExec {

    final transient TrLauncher launch_;

    /**
     * @param _l le lanceur / the launcher
     */
    FudaaExecConsole(final TrLauncher _l) {
      super(TrResource.getS("console"));
      launch_ = _l;
    }

    @Override
    public void execInDir(final File _dir, final FudaaUI _ui, final Runnable _nexProcess) {
      launch_.openLogFrame(null);
    }

    @Override
    public void execOnFile(final File _target, final FudaaUI _ui, final Runnable _nexProcess) {
      launch_.openLogFrame(_target);
    }

    @Override
    public Icon getIcon() {
      return FudaaResource.FUDAA.getIcon("tail");
    }

    @Override
    public String getShownName() {
      return TrResource.getS("Console");
    }
  }

  private static class FudaaExecTexte extends FudaaExec {

    FudaaExecTexte() {
      super(TrResource.getS("�diteur texte"));
    }

    @Override
    public void execInDir(final File _dir, final FudaaUI _ui, final Runnable _nexProcess) {

    }

    @Override
    public void execOnFile(final File _target, final FudaaUI _ui, final Runnable _nexProcess) {
      FudaaEditor.getInstance().edit(_target);
    }

    @Override
    public Icon getIcon() {
      return BuResource.BU.getIcon("texte");
    }

    @Override
    public String getShownName() {
      return TrResource.getS("Editeur texte");
    }
  }

  /**
   * Exe pour creer un projet. Exe to create a project
   * 
   * @author Fred Deniger
   * @version $Id: TrApplicationManager.java,v 1.32 2007-05-04 14:01:46 deniger Exp $
   */
  public static class FudaaExecCreateProjectH2d extends FudaaExec {

    private transient final TrLauncher launch_;

    /**
     * @param _name le nom a donner a l'appli / the name to give to the application
     * @param _launch le lanceur / the launcher
     */
    public FudaaExecCreateProjectH2d(final String _name, final TrLauncher _launch) {
      super(_name);
      launch_ = _launch;
    }

    @Override
    public void execInDir(final File _dir, final FudaaUI _ui, final Runnable _r) {
      launch_.createHydProjet(null);
    }

    @Override
    public void execOnFile(final File _target, final FudaaUI _ui, final Runnable _r) {
      launch_.createHydProjet(_target);
    }

    @Override
    public Icon getIcon() {
      return TrResource.getEditorIcon();
    }
  }

  /**
   * L'exe pour lancer le visualisateur de maillage. The exe to throw the vizualiser of the mesh
   * 
   * @author Fred Deniger
   * @version $Id: TrApplicationManager.java,v 1.32 2007-05-04 14:01:46 deniger Exp $
   */
  public static class FudaaExecMeshView extends FudaaExec {

    private transient final TrApplicationManager launch_;

    /**
     * @param _launch le lanceur d'appli / the application launcher
     */
    public FudaaExecMeshView(final TrApplicationManager _launch) {
      super("meshview");
      launch_ = _launch;
    }

    @Override
    public void execInDir(final File _dir, final FudaaUI _ui, final Runnable _nexProcess) {
      launch_.buildMv(null);
    }

    @Override
    public void execOnFile(final File _target, final FudaaUI _ui, final Runnable _nexProcess) {
      launch_.buildMv(_target);
    }

    @Override
    public Icon getIcon() {
      return TrResource.getMeshIcon();
    }

    @Override
    public String getShownName() {
      return TrResource.getMeshName();
    }
  }

  /**
   * L'exe pour lancer le post / The exe to launch the post
   * 
   * @author Fred Deniger
   * @version $Id: TrApplicationManager.java,v 1.32 2007-05-04 14:01:46 deniger Exp $
   */
  public static class FudaaExecPostView extends FudaaExec {

    final transient TrLauncher launch_;

    /**
     * @param _launcher le lanceur / the launcher
     */
    public FudaaExecPostView(final TrLauncher _launcher) {
      super("post");
      launch_ = _launcher;
    }

    @Override
    public void execInDir(final File _dir, final FudaaUI _ui, final Runnable _nexProcess) {
      launch_.openPost(null);
    }

    @Override
    public void execOnFile(final File _target, final FudaaUI _ui, final Runnable _nexProcess) {
      launch_.openPost(_target);
    }

    @Override
    public Icon getIcon() {
      return TrResource.getPostIcon();
    }

    @Override
    public String getShownName() {
      return TrResource.getPostName();
    }
  }

  public static class FudaaExecPostAnalyzeView extends FudaaExec {

    final transient TrLauncher launch_;

    /**
     * @param _launcher le lanceur
     */
    public FudaaExecPostAnalyzeView(final TrLauncher _launcher) {
      super("analyzePost");
      launch_ = _launcher;
    }

    @Override
    public void execInDir(final File _dir, final FudaaUI _ui, final Runnable _nexProcess) {
    }

    @Override
    public void execOnFile(final File _target, final FudaaUI _ui, final Runnable _nexProcess) {
      TrPostAnalyze.active(_target, launch_, _ui.getParentComponent());
    }

    @Override
    public Icon getIcon() {
      return BuResource.BU.getIcon("analyser");
    }

    @Override
    public String getShownName() {
      return TrResource.getS("Analyser un fichier de r�sultats");
    }
  }

  public static class FudaaMatisseAnalyzeView extends FudaaExec {

    // final transient TrLauncher launch_;

    /**
     */
    public FudaaMatisseAnalyzeView() {
      super("analyzeMatisse");
      // launch_ = _launcher;
    }

    public boolean isOkFile(final File _file) {
      return _file != null && _file.isFile();
    }

    public boolean isOkDir(final File _file) {
      return _file != null && _file.isDirectory() && new File(_file, "BATHYGEO").exists();
    }

    @Override
    public void execInDir(final File _dir, final FudaaUI _ui, final Runnable _nexProcess) {
      if (_dir != null && _dir.isDirectory()) {
        final File f = new File(_dir, "BATHYGEO");
        if (f.exists()) {
          execOnFile(f, _ui);
        }
        return;
      }
      _ui.error(TrResource.getS("Fichier BATHYGEO non trouv�"));

    }

    @Override
    public void execOnFile(final File _target, final FudaaUI _ui, final Runnable _nexProcess) {
      if (isOkFile(_target)) {
        TrMatisseConvertGUI.active(_target, _ui.getParentComponent());
      } else {
        _ui.error(TrResource.getS("Fichier BATHYGEO non trouv�"));
      }
    }

    @Override
    public Icon getIcon() {
      return BuResource.BU.getIcon("analyser");
    }

    @Override
    public String getShownName() {
      return TrResource.getS("Analyser le fichier Matisse BATHYGEO");
    }
  }

  /**
   * L'exe pour lancer l'editeur / the exe to launch the publisher
   * 
   * @author Fred Deniger
   * @version $Id: TrApplicationManager.java,v 1.32 2007-05-04 14:01:46 deniger Exp $
   */
  public class FudaaExecEditor extends FudaaExec {

    /**
     */
    public FudaaExecEditor() {
      super("Crue");
    }

    @Override
    public void execInDir(final File _dir, final FudaaUI _ui, final Runnable _nexProcess) {
      launcher_.ouvrirHydEditor(launcher_.getCurrentImplHelper().getSoftwareID(), null);
    }

    @Override
    public void execOnFile(final File _target, final FudaaUI _ui, final Runnable _nexProcess) {
      launcher_.ouvrirHydEditor(launcher_.getCurrentImplHelper().getSoftwareID(), _target);
    }

    @Override
    public Icon getIcon() {
      return TrResource.getEditorIcon();
    }

    @Override
    public String getShownName() {
      return TrResource.getEditorName();
    }
  }

  public static class FudaaExecGridManagement extends FudaaExec {

    private FileFormat[] fileFormat;

    /**
     * Constructeur par defaut. default constructor
     */
    public FudaaExecGridManagement(FileFormat[] fileFormat) {
      super(TrLib.getString("Traitement du maillage"));

      this.fileFormat = fileFormat;
    }

    @Override
    public void execInDir(final File _dir, final FudaaUI _ui, final Runnable _nexProcess) {
    }

    @Override
    public void execOnFile(final File _target, final FudaaUI _ui, final Runnable _nexProcess) {
      FudaaPanelTask task = new FudaaPanelTask(_ui, new GridProcessTaskModel(TrFileFormatManager.getAllGridFormat(), _target));
      task.afficheDialog();
    }

    @Override
    public Icon getIcon() {
      return BuResource.BU.getIcon("executer");
    }

  }

  private transient FudaaExecConsole consoleExec_;
  // private BuMenu mainMenu_;
  private transient BuMenu openWith_;
  protected transient BuMenuItem newProject_;
  protected transient Map ftFilters_;
  protected transient FileFormat[] gridFt_;
  protected transient final TrLauncher launcher_;
  protected transient FudaaExecMeshView meshExec_;
  protected transient FudaaExecPostView postExec_;
  protected transient FudaaMatisseAnalyzeView bathygeoAnalyzeExec_;
  protected JMenuItem bathygeoFile_;
  protected JMenuItem bathygeoDir_;

  protected transient BuMenuItem postItem_;
  protected transient FudaaExecEditor preproExec_;
  transient FileFormat[] postFt_;
  transient FileFormat[] projectFt_;

  transient FudaaExecTexte texteExec_;
  transient BuFileFilter txtFileFilter_;

  /**
   * @param _launcher le lanceur d'appli
   */
  protected TrApplicationManager(final TrLauncher _launcher) {
    super(TrPreferences.TR);
    launcher_ = _launcher;
  }

  BuMenuItem itemOpenScop_ = null;
  private BuMenuItem execGridManage_;

  private BuMenu buildMenuOuvrirAvec(final TrExplorer _explorer) {
    return new BuDynamicMenu(TrResource.getS("Ouvrir avec..."), "LAUNCH", BuResource.BU.getIcon("aucun")) {

      @Override
      public void build() {
        if (getMenuComponentCount() == 0) {
          _explorer.getAppliManager().buildCmdForMenuFileOpenWith(this, _explorer);
          addSeparator();
          add(_explorer.createFileAction(_explorer.getAppliManager().getConsoleExec()));
          add(_explorer.createFileAction(new FudaaExecTexte()));
          final int nb = _explorer.getAppliManager().getActionsNb();
          if (nb > 0) {
            addSeparator();
          }
          for (int i = 0; i < nb; i++) {
            add(_explorer.createFileAction(_explorer.getAppliManager().getExec(i)));
          }

          // -- ajout du format scop --//
          //--add of scop format
          add(addScopExe(_explorer), 0);

        }
      }

      @Override
      public boolean isActive() {
        return isEnabled();
      }
    };
  }

  /**
   * Action qui ajoute une action pour executer un fichier en mode scop. Tres utile car les ficheirs scop disposent
   * souvant d'extensions non reconnues Action whih add an action to execute a list in scop map. Very useful for the
   * lists scop are often unknown extensions
   * 
   * @param _explorer
   * @return
   */
  private BuMenuItem addScopExe(final TrExplorer _explorer) {
    // -- on ajoute l'option ouvrir avec le format scop --//
    // -- we add the option open with the format scop--//
    EbliActionSimple action = new EbliActionSimple(TrResource.getS("Format SCOP"), EbliResource.EBLI.getIcon("curves"), "OPENSCOP") {
      @Override
      public void actionPerformed(ActionEvent event) {

        ouvrirScopSansVerif(_explorer.getSelectedFile(), null);
      }
    };
    return new BuMenuItem(action);

  }

  protected void buildCmdForMenuFileOpenWith(final BuMenu _m, final TrExplorer _explor) {
    /*
     * _m.add(_explor.createFileAction(getTextExec())); _m.add(_explor.createFileAction(getConsoleExec()));
     * _m.addSeparator();
     */
    _m.add(_explor.createFileAction(getMvExec()));
    _m.add(_explor.createFileAction(getEditorExec()));
    _m.add(_explor.createFileAction(getPostExec()));
    _m.add(_explor.createFileAction(getPostAnalyzeExec()));
    if (bathygeoAnalyzeExec_ != null) {
      if (bathygeoFile_ == null) {
        bathygeoFile_ = _explor.createFileAction(bathygeoAnalyzeExec_);
      }
      _m.add(bathygeoFile_);
    }

  }

  protected static BuMenu buildDirMenuItem(final TrExplorer _explorer) {
    return new BuDynamicMenu(TrResource.getS("Ex�cuter..."), "LAUNCH", BuResource.BU.getIcon("executer")) {

      @Override
      public void build() {
        final TrApplicationManager mng = _explorer.appli_;
        if (mng.bathygeoAnalyzeExec_ != null && mng.bathygeoDir_ == null) {
          mng.bathygeoDir_ = _explorer.createDirAction(mng.bathygeoAnalyzeExec_);
        }
        if (mng.bathygeoDir_ != null) {
          mng.bathygeoDir_.setEnabled(mng.bathygeoAnalyzeExec_.isOkDir(_explorer.getSelectedDirectory()));
        }
        if (getMenuComponentCount() == 0) {

          final int nb = _explorer.getAppliManager().getActionsNb();
          for (int i = 0; i < nb; i++) {
            add(_explorer.createDirAction(_explorer.getAppliManager().getExec(i)));
          }
          if (mng.bathygeoDir_ != null) {
            add(mng.bathygeoDir_);
          }
        }
      }

      @Override
      public boolean isActive() {
        return true;
      }
    };
  }

  /**
   * Appele pour ajouter des menus au debut du menu principal. Calls to add menus at the top of the main menu
   * 
   * @param _m le menu de dest / the menu of dest
   */
  protected void buildMainMenuFirstItems(final BuMenu _m) {
  }

  protected void buildMv(final File _file) {
    launcher_.openMeshView(_file);
  }

  public boolean isEditorAlreadyOpen(final File _f) {
    return getEditorOpenedFor(_f) != null;
  }

  public TrEditorImplementation getEditorOpenedFor(final File _f) {
    return launcher_.findImplWithOpenedFile(_f);
  }

  public boolean isPostAlreadyOpen(final File _f) {
    return launcher_.findPostWithOpenedFile(_f) != null;
  }

  protected String ouvrirEditor(final VfsFile _file, final AbstractButton _lb) {
    final String name = _file.getName();
    final BuFileFilter filter = getFileFilterFor(TelemacDicoFileFormat.getEmptyFormat());
    if (filter.accept(null, name)) {
      final String res = TrResource.getS("Projet T�l�mac");
      if (_lb == null) {
        launcher_.ouvrirHydEditor(FileFormatSoftware.TELEMAC_IS.name, _file);
      } else {
        _lb.setIcon(TrResource.getEditorIcon());
      }
      return res;
    }
    if (getFileFilterFor(INPFileFormat.getInstance()).accept(null, name)) {
      final String res = TrResource.getS("Projet Reflux");
      if (_lb == null) {
        launcher_.ouvrirHydEditor(FileFormatSoftware.REFLUX_IS.name, _file);
      } else {
        _lb.setIcon(TrResource.getEditorIcon());
      }
      return res;
    }
    if (getFileFilterFor(RubarPARFileFormat.getInstance()).accept(null, name)) {
      final String res = TrResource.getS("Projet Rubar");
      if (_lb == null) {
        launcher_.ouvrirHydEditor(FileFormatSoftware.RUBAR_IS.name, _file);
      } else {
        _lb.setIcon(TrResource.getEditorIcon());
      }
      return res;
    }
    // if (getFileFilterFor(TrPostFileFormat.getInstance()).accept(null, name)) {
    // final String res = TrResource.getS("Projet Post");
    // if (_lb == null) {
    // //launcher_.ouvrirHydEditor(FileFormatSoftware.RUBAR_IS.name, _file);
    // } else {
    // _lb.setIcon(getFileFilterFor(TrPostFileFormat.getInstance()).getIcon());
    // }
    // return res;
    // }

    return null;
  }

  protected String ouvrirMesh(final VfsFile _f, final AbstractButton _lb) {
    if (isGridFormat(_f) && (isNotSerafinResFile(_f))) {
      final String res = TrResource.getMeshName();
      if (_lb == null) {
        buildMv(_f);
      } else {
        // _lb.setText(r);
        _lb.setIcon(TrResource.getMeshIcon());
      }
      return res;
    }
    return null;
  }

  /**
   * Methode appelee pour ouvrir un fichier layout Method called to open a layout file
   * 
   * @param _f
   * @param _lb
   * @return
   */
  protected String ouvrirPostLayout(final VfsFile _f, final AbstractButton _lb, boolean forceRecompute) {
    // --si le format est du type *.POST
    // -- if the format is of type *POST
    if (isPostLayoutFormat(_f)) {
      final String res = TrResource.getPostName();
      if (_lb == null) {
        launcher_.openLayoutPost(_f, forceRecompute);
      } else {
        _lb.setIcon(TrResource.getPostLayoutIcon());
      }
      return res;
    } else if (TrPostFileFilter.acceptDirectoryPOST(_f)) {
      final String res = TrResource.getPostName();
      if (_lb == null) {
        doOpenPostLayout(_f, forceRecompute);
      } else {
        _lb.setIcon(TrResource.getPostLayoutIcon());
      }
      return res;

    }
    return null;
  }

  public void doOpenPostLayout(final File _f, boolean forceRecompute) {
    launcher_.openLayoutPost(TrPostFileFilter.getSetupFormDirectoryPOST(_f), forceRecompute);
  }

  protected String ouvrirScop(final VfsFile _f, final AbstractButton _lb) {
    // --si le format est du type *.POST
    // -- if the format is of type *POST
    if (isScopFormat(_f)) {
      return ouvrirScopSansVerif(_f, _lb);
    }
    return null;
  }

  /**
   * Ouvre un fichier de type scop sans verifier son extension. Necessaire pour l'execution de l'option ouvrir avec
   * format scop car la plupart des fichiers scop ont des extensions exotiques, donc non reconnues... To open a file of
   * type scop without checking its extension. Necesary to the execution of the option open with scop format for most of
   * scop lists have exotic extensions therefore no recognized
   * 
   * @param _f
   * @param _lb
   * @return
   */
  protected String ouvrirScopSansVerif(final VfsFile _f, final AbstractButton _lb) {
    final String res = TrResource.getPostName();
    if (_lb == null) {
      launcher_.openScopPost(_f);
    } else {
      _lb.setIcon(TrResource.getScopIcon());
    }
    return res;
  }

  protected String ouvrirPost(final VfsFile _f, final AbstractButton _lb) {
    if (isPostFormat(_f)) {
      final String res = TrResource.getPostName();
      if (_lb == null) {
        launcher_.openPost(_f);
      } else {
        _lb.setIcon(TrResource.getPostIcon());
      }
      return res;
    }
    return null;
  }

  /**
   * @param _f le fichier a ouvrir / the file to open
   */
  protected String ouvrir(final VfsFile _f, final AbstractButton _lb) {
    String res = ouvrirEditor(_f, _lb);
    if (res != null) {
      return res;
    }

    // -- tentative ouverture fichier type layout --//
    // --attempt of opening file layout
    res = ouvrirPostLayout(_f, _lb,false);
    if (res != null) {
      return res;
    }

    // -- tentative ouverture fichier scop --//
    // --  attempt opening scop file
    res = ouvrirScop(_f, _lb);
    if (res != null) {
      return res;
    }

    res = ouvrirMesh(_f, _lb);
    if (res != null) {
      return res;
    }
    res = ouvrirPost(_f, _lb);
    if (res != null) {
      return res;
    }

    final String name = _f.getName();
    if (getTxtFileFilter().accept(null, name)) {
      if (_lb != null) {
        _lb.setIcon(BuResource.BU.getIcon("texte"));
        return TrResource.getS("texte");
      }
      if (_f.length() > 1E5) {
        CtuluLibDialog.showError(_lb, FudaaLib.getS("Ouverture du fichier refus�e"), FudaaLib.getS("Le fichier est trop gros"));
      }
      FudaaEditor.getInstance().edit(_f);

    }
    if (_lb != null) {
      _lb.setIcon(BuResource.BU.getIcon("ouvrir"));
      _lb.setText(BuResource.BU.getString("Ouvrir"));

    }
    return null;
  }

  /**
   * Methode a surcharger pour ajouter des actions au menu contextuel des rep. Method to surchage to add actions to the
   * menu contextual of directories
   * 
   * @param _m le menu contextuel a modifier / the menu contextual to modify
   * @param _explor l'explorateur parent / the relative explorer
   * @return true si menus ajoutes / true if added menus
   */
  public boolean buildCmdForMenuDir(final JPopupMenu _m, final TrExplorer _explor) {
    _m.add(buildDirMenuItem(_explor));
    return true;
  }

  /**
   * Methode a surcharger pour ajouter des actions au menu contextuel des fichiers. Method o add actions to the
   * contextual menu of files
   * 
   * @param _m le menu de dest / the menu of dest
   * @param _explor l'explorateur parent / the relative explorer
   */
  public void buildCmdForMenuFile(final JPopupMenu _m, final TrExplorer _explor) {
    execGridManage_ = _explor.createFileAction(new TrApplicationManager.FudaaExecGridManagement(TrFileFormatManager.getAllGridFormat()));
    _m.add(execGridManage_);
    openWith_ = buildMenuOuvrirAvec(_explor);

    _m.add(openWith_);
  }

  /**
   * Ajoute les applications par defaut pour le prepro. add the applications by default for the prepro
   * 
   * @param _m
   */
  public final void buildTrItem(final BuMenu _m) {
    buildMainMenuFirstItems(_m);
    BuMenuItem item = new BuMenuItem();
    item.setAction(getEditorExec().getAction());
    _m.add(item);
    item = new BuMenuItem();
    item.setAction(getPostExec().getAction());
    _m.add(item);
    item = new BuMenuItem();
    item.setAction(getMvExec().getAction());
    _m.add(item);
    _m.addSeparator();
    item = new BuMenuItem();
    item.setAction(getConsoleExec().getAction());
    _m.add(item);
  }

  /**
   * @param _f le fichier / the file
   * @return les actions pour ce type de fichier / the actions for this kind of file
   */
  public FudaaExec[] getActionForFile(final File _f) {
    return null;
  }

  /**
   * @return l'exe pour la console / the exe for the console
   */
  public FudaaExecConsole getConsoleExec() {
    if (consoleExec_ == null) {
      consoleExec_ = new FudaaExecConsole(launcher_);
    }
    return consoleExec_;
  }

  /**
   * @param _ft le format
   * @return le filtre a utiliser / the filter to use
   */
  public final BuFileFilter getFileFilterFor(final FileFormat _ft) {
    BuFileFilter res = null;
    if (ftFilters_ == null) {
      ftFilters_ = new HashMap(10);
    } else {
      res = (BuFileFilter) ftFilters_.get(_ft);
    }
    if (res == null) {
      if (this.getSoftId().equals(TrRubarImplHelper.getID()) && _ft.getID() == SerafinFileFormat.getInstance().getID()) {
        res = SerafinFileFormat.getInstance().createFileFilterStrict();
      } else {
        res = _ft.createFileFilter();
      }
      ftFilters_.put(_ft, res);
    }
    return res;
  }

  /**
   * @param _fts les formats a parcourir / the formats to browse
   * @param _file le fichier pour lequel on cherche le format / the file for which we look or the format
   * @return le format correspondant au fichier _f. null sinon / the format corresponding to the _f file null otherwise
   */
  public FileFormat findFileFormat(final FileFormat[] _fts, final File _file) {
    final int nb = _fts.length;
    for (int i = 0; i < nb; i++) {
      final BuFileFilter filter = getFileFilterFor(_fts[i]);
      if (filter.accept(_file)) {
        return _fts[i];
      }
    }
    return null;
  }

  /**
   * @param _fToSearch
   * @return le format correspondant a un format de maillage et utilisable pour le fichier / the format corresponding to
   *         a mesh format and usable for the file
   */
  public FileFormat findGridFileFormat(final File _fToSearch) {
    if (gridFt_ == null) {
      gridFt_ = TrFileFormatManager.getGridFormat(this.getSoftId());
    }
    return findFileFormat(gridFt_, _fToSearch);

  }

  /**
   * @return l'exe pour le visu de maillage / the exe for the visu of mesh
   */
  public FudaaExecMeshView getMvExec() {
    if (meshExec_ == null) {
      meshExec_ = new FudaaExecMeshView(this);
    }
    return meshExec_;
  }

  /**
   * @return l'exe pour lancer le post / the exe to launch the post
   */
  public FudaaExecPostView getPostExec() {
    if (postExec_ == null) {
      postExec_ = new FudaaExecPostView(launcher_);
    }
    return postExec_;
  }

  public FudaaExecPostAnalyzeView getPostAnalyzeExec() {
    return new FudaaExecPostAnalyzeView(launcher_);
  }

  public FudaaMatisseAnalyzeView getBathygeoAnalyzeExec() {
    if (bathygeoAnalyzeExec_ == null) {
      bathygeoAnalyzeExec_ = new FudaaMatisseAnalyzeView();
    }
    return bathygeoAnalyzeExec_;
  }

  /**
   * @param _f le fichier pour lequel on recherche un format utilisable par le post / the file for which we look for a
   *          format usable for the post
   * @return un format correspondant a _f et utilisable par le post / a format corresponding a_f and usable for the post
   */
  public FileFormat findPostFormat(final File _f) {
    if (postFt_ == null) {
      postFt_ = TrFileFormatManager.getPostFormat(this.getSoftId());
    }
    return findFileFormat(postFt_, _f);

  }

  /**
   * @return l'exe pour l'editeur de projets / the exe for the project editor
   */
  public FudaaExecEditor getEditorExec() {
    if (preproExec_ == null) {
      preproExec_ = new FudaaExecEditor();
    }
    return preproExec_;
  }

  /**
   * @param _file le fichier pour lequel on recherche un format utilisable par l'editeur de projet / the list for which
   *          we look for a usable format by the project editor
   * @return un format correspondant a _f et utilisable par l'editeur de projet / a format corresponding to f and usable
   *         by the project editor
   */
  public FileFormat findEditorFormat(final File _file) {
    if (projectFt_ == null) {
      projectFt_ = TrFileFormatManager.getProjectFormat();
    }
    return findFileFormat(projectFt_, _file);

  }

  /**
   * @return l'identifiant du systeme / the identifiant of the system
   */
  public abstract String getSoftId();

  /**
   * @return l'exe pour ouvrir des fichiers textes / the exe to open text files
   */
  public FudaaExecTexte getTextExec() {
    if (texteExec_ == null) {
      texteExec_ = new FudaaExecTexte();
    }
    return texteExec_;
  }

  /**
   * @return les actions a ajouter a la tool bar / the actions to add to the tool bar
   */
  public FudaaExec[] getToolBarExecs() {
    return null;
  }

  /**
   * @return le filtre pour les fichiers textes / the filter for the text files
   */
  public BuFileFilter getTxtFileFilter() {
    if (txtFileFilter_ == null) {
      txtFileFilter_ = new BuFileFilter(new String[] { "txt", "csv", "bat", "sh" }, BuResource.BU.getString("Texte"));
    }
    return txtFileFilter_;
  }

  /**
   * @param _fts les formats a parcourir / the formats to browse
   * @param _file le fichier a tester / the file to test
   * @return true si le fichier _f est support� par l'un des formats du tableau _fts / true if the _f file is supported
   *         by one of the format of the picture_fts
   */
  public boolean isFileSupportedBy(final FileFormat[] _fts, final File _file) {
    return findFileFormat(_fts, _file) != null;
  }

  /**
   * @param _file le fichier a tester / the file to test
   * @return true si c'est un format utilisable pour l'editeur de maillage / true if it is a usable format for the mesh
   *         editor
   */
  public boolean isGridFormat(final File _file) {
    return findGridFileFormat(_file) != null;
  }

  /**
   * @param _file le fichier a tester / the file to test
   * @return true si ce n'est pas un fichier serafin / true if it is not a serafin file
   */
  public boolean isNotSerafinResFile(final VfsFile _file) {
    final SerafinFileFilter filter = (SerafinFileFilter) getFileFilterFor(SerafinFileFormat.getInstance());
    return !filter.accept(_file) || (!filter.isResFile(_file));
  }

  /**
   * @param _file le fichier a tester / the file to test
   * @return true si c'est un fichier supporter par le post / true if it is a file supported by the post
   */
  public boolean isPostFormat(final File _file) {
    FileFormat format = findPostFormat(_file);
    return format != null;
  }

  /**
   * Retourne true si il s'agit d'un format layout Return true if it a layout format
   * 
   * @param _file
   * @return
   */
  public boolean isPostLayoutFormat(final File _file) {
    return TrPostFileFormat.getInstance().createFileFilter().accept(_file);
  }

  /**
   * Test si le format est scope. Test if the format is scope
   * 
   * @param _file
   * @return
   */
  public static boolean isScopFormat(final File _file) {
    boolean ok = false;
    BuFileFilter scopeSFilter = ScopeSFileFormat.getInstance().createFileFilter();
    BuFileFilter scopeTFilter = ScopeTFileFormat.getInstance().createFileFilter();
    BuFileFilter scopeGENEFilter = ScopeGENEFileFormat.getInstance().createFileFilter();

    ok = scopeSFilter.accept(_file);
    if (ok)
      return ok;
    ok = scopeTFilter.accept(_file);
    if (ok)
      return ok;
    ok = scopeGENEFilter.accept(_file);

    return ok;

  }

  /**
   * @param _file le fichier a tester / the file to test
   * @return true si supporte par l'editeur / true if supported by the editor
   */
  public boolean isProjectFormat(final VfsFile _file) {
    return findEditorFormat(_file) != null;
  }

  /**
   * @param _file le fichier a ouvrir / the file to open
   */
  public void ouvrir(final VfsFile _file) {
    ouvrir(_file, null);
  }

  /**
   * Mis a jour des pr�f�rences Update preferences
   */
  public void prefChanged() {
  }

  /**
   * @param _nbFiles le nombre de fichier selectionne / the number of selected files
   * @param _file le fichier selectionne / the selected file
   */
  public void updateMenuFiles(final int _nbFiles, final File _file) {
    openWith_.setEnabled(_nbFiles == 1);
    execGridManage_.setEnabled((_nbFiles == 1));
    if (postItem_ != null) {
      postItem_.setEnabled(isPostFormat(_file));
    }
    if (bathygeoFile_ != null) {
      bathygeoFile_.setEnabled(_nbFiles == 1 && bathygeoAnalyzeExec_.isOkFile(_file));
    }
  }

  /**
   * Permet de mettre a jour l'etat du menu ouvrir. Allow to update the open menu
   * 
   * @param _openItem
   * @param _nbFiles
   * @param _file
   */
  public void updateOuvrirMenu(final BuMenuItem _openItem, final int _nbFiles, final VfsFile _file) {
    if (_nbFiles == 1) {
      final String s = ouvrir(_file, _openItem);
      _openItem.setText(BuResource.BU.getString("Ouvrir") + CtuluLibString.ESPACE + (s == null ? CtuluLibString.EMPTY_STRING : "(" + s + ")"));
      _openItem.setEnabled(true);
      if (bathygeoFile_ != null) {
        bathygeoFile_.setEnabled(bathygeoAnalyzeExec_.isOkFile(_file));
      }
    } else {
      _openItem.setText(BuResource.BU.getString("Ouvrir"));
      _openItem.setEnabled(false);
    }
  }
}
