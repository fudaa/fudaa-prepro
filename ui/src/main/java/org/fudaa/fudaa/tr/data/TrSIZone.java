/*
 *  @creation     12 avr. 2005
 *  @modification $Date: 2006-09-19 15:07:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolygone;

/**
 * @author Fred Deniger
 * @version $Id: TrSIZone.java,v 1.8 2006-09-19 15:07:28 deniger Exp $
 */
public class TrSIZone extends GISPolygone {

  private int ptAmont_ = -1;
  boolean ptAmontIsExt_;
  boolean ptAvalIsExt_;
  private int ptAval_ = -1;
  final private int profileIdx_;

  public final boolean isPtAvalIsExt() {
    return ptAvalIsExt_;
  }

  public final void setPtAvalIsExt(final boolean _ptAvalIsExt) {
    ptAvalIsExt_ = _ptAvalIsExt;
  }

  public final boolean isPtAmontIsExt() {
    return ptAmontIsExt_;
  }

  public final void setPtAmontIsExt(final boolean _ptAmontIsExt) {
    ptAmontIsExt_ = _ptAmontIsExt;
  }

  // ce point est nécessaire s'il y a un seul point amont,aval: cela sert a orienter
  // la zone

  public boolean isGeomContainsDirectorPoint(final LineString _profil, final Geometry _g) {
    if (ptAmont_ < 0 && ptAval_ < 0) { return false; }
    if (ptAmont_ < 0 || ptAmontIsExt_) { return _g.contains(_profil.getPointN(ptAval_)); }
    if (ptAval_ < 0 || ptAvalIsExt_) { return _g.contains(_profil.getPointN(ptAmont_)); }
    final Coordinate[] c = new Coordinate[2];
    c[0] = _profil.getCoordinateSequence().getCoordinateCopy(ptAmont_);
    c[1] = _profil.getCoordinateSequence().getCoordinateCopy(ptAval_);
    return _g.contains(GISGeometryFactory.INSTANCE.createLineString(c));
  }

  GISPolygone lit_;

  public void setLit(final GISPolygone _l) {
    lit_ = _l;
  }

  /*
   * public TrSIZone createNew(CoordinateSequence _seq){ TrSIZone r = new TrSIZone(_seq); r.setPtAmont(ptAmont_);
   * r.setPtAval(ptAval_); r.setPtAmontIsExt(ptAmontIsExt); r.setPtAvalIsExt(ptAvalIsExt); return r; }
   */

  /**
   * @param _points
   */
  public TrSIZone(final CoordinateSequence _points, final int _profileIdx) {
    super(_points);
    profileIdx_ = _profileIdx;
  }

  public TrSIZone() {
    super(GISGeometryFactory.INSTANCE.createLinearRingDefaultSequence());
    profileIdx_ = -1;
  }

  public final int getPtAmont() {
    return ptAmont_;
  }

  public final int getPtAval() {
    return ptAval_;
  }

  public final void setPtAmont(final int _ptAmont) {
    ptAmont_ = _ptAmont;
  }

  public final void setPtAval(final int _ptAval) {
    ptAval_ = _ptAval;
  }

  public int getProfileIdx() {
    return profileIdx_;
  }

}
