/*
 *  @creation     27 nov. 2003
 *  @modification $Date: 2007-06-28 09:28:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.bu.BuVerticalLayout;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.reflux.H2dRefluxElementProperty;
import org.fudaa.dodico.h2d.reflux.H2dRefluxElementPropertyMngAbstract;
import org.fudaa.dodico.h2d.reflux.H2dRefluxValue;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrCourbeTemporelleManager;

/**
 * @author deniger
 * @version $Id: TrRefluxPropEditor.java,v 1.16 2007-06-28 09:28:18 deniger Exp $
 */
public class TrRefluxPropEditor extends CtuluDialogPanel {

  TrRefluxValueEditorPanel[] datas_;

  public TrRefluxPropEditor(final H2dRefluxElementPropertyMngAbstract _propMng,
      final TrCourbeTemporelleManager _evolMng, final int[] _selectedIdx) {
    setPreferredSize(new Dimension(450, 400));
    setLayout(new BuVerticalLayout(10));
    final CtuluPermanentList l = _propMng.getVarList();
    final int n = l.size();
    datas_ = new TrRefluxValueEditorPanel[n];
    int idx = 0;
    final Map m = _propMng.getVariableCommonValues(_selectedIdx);
    for (final Iterator it = m.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      final H2dVariableType v = (H2dVariableType) e.getKey();
      final H2dRefluxElementProperty p = _propMng.getEltProp(v);
      if (idx < datas_.length) {
        datas_[idx++] = new TrRefluxValueEditorPanel(_evolMng, this, v, (H2dRefluxValue) e.getValue(), p
            .isFreeAllowed());
      }
    }
    doLayout();
    final Dimension commun = datas_[n - 1].lbTitle_.getPreferredSize();
    for (int i = n - 2; i >= 0; i--) {
      final Dimension d = datas_[i].lbTitle_.getPreferredSize();
      if (d.height > commun.height) {
        commun.height = d.height;
      }
      if (d.width > commun.width) {
        commun.width = d.width;
      }
    }
    for (int i = n - 1; i >= 0; i--) {
      datas_[i].lbTitle_.setPreferredSize(commun);
    }
    revalidate();
  }

  /**
   * @return la table variable -> H2dRefluxValue
   */
  public Map getNewValues() {
    final Map r = new HashMap(datas_.length);
    for (int i = datas_.length - 1; i >= 0; i--) {
      final TrRefluxValueEditorPanel d = datas_[i];
      if (d.isEnable()) {
        r.put(d.v_, d.getValue());
      }
    }
    return r;
  }

  @Override
  public boolean isDataValid() {
    for (int i = datas_.length - 1; i >= 0; i--) {
      if (!datas_[i].validate()) { return false; }
    }
    return true;
  }
}