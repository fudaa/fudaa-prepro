/**
 * @creation 15 d�c. 2004
 * @modification $Date: 2007-05-04 14:01:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarApportListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarApportSpatialMng;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.palette.BPalettePlageAbstract;
import org.fudaa.ebli.palette.BPalettePlageDiscret;
import org.fudaa.ebli.palette.BPlageDiscret;
import org.fudaa.ebli.trace.BPlageInterface;
import org.fudaa.fudaa.meshviewer.layer.MvGridLayerGroup;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;
import org.fudaa.fudaa.meshviewer.model.MvIsoPainter;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarApportIsoPainter.java,v 1.15 2007-05-04 14:01:53 deniger Exp $
 */
public class TrRubarApportIsoPainter implements MvIsoPainter, H2dRubarApportListener {

  BPalettePlageDiscret pal_;

  H2dRubarParameters param_;

  /**
   * @param _param les parametres
   */
  public TrRubarApportIsoPainter(final H2dRubarParameters _param) {
    param_ = _param;
    param_.getAppMng().addAppListener(this);
  }

  @Override
  public boolean isPaletteInitialized() {
    return pal_ != null;
  }

  @Override
  public void setPalette(final BPalettePlageAbstract _palette) {
    if (pal_ == null) {
      initPal();
    }
    pal_.initFrom(_palette);

  }

  private void updatePlage() {
    if (!isPaletteInitialized()) { return; }
    final EvolutionReguliereInterface[] e = param_.getAppMng().getUsedCourbes();
    if (e == null) {
      pal_.clear();
      return;
    }
    final List newPlage = new ArrayList(e.length);
    Color min = Color.BLUE;
    for (int i = 0; i < e.length; i++) {
      final BPlageInterface p = pal_.getPlageFor(e[i]);
      if (p == null) {
        final BPlageDiscret newP = new BPlageDiscret(e[i]);
        newP.setCouleur(min.brighter());
        min = newP.getCouleur();
        newPlage.add(newP);
      } else {
        newPlage.add(p);
        min = p.getCouleur();
      }
    }
    final BPlageInterface[] np = new BPlageInterface[newPlage.size()];
    newPlage.toArray(np);
    pal_.setPlages(np);

  }

  protected void initPal() {
    if (pal_ == null) {
      pal_ = new BPalettePlageDiscret(param_.getAppMng().getUsedCourbes());
      pal_.setTitre(getNom());
      pal_.setSousTitre(CtuluLibString.EMPTY_STRING);
    }
  }

  @Override
  public void apportChanged(final H2dRubarApportSpatialMng _mng) {}

  @Override
  public void apportEvolutionContentChanged(final H2dRubarApportSpatialMng _mng, final EvolutionReguliereInterface _dest) {

  }

  @Override
  public void apportEvolutionUsedChanged(final H2dRubarApportSpatialMng _mng, final EvolutionReguliereInterface _dest) {
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("update plage for apport");
    }
    updatePlage();
  }

  @Override
  public double getMax() {
    return 0;
  }

  @Override
  public double getMin() {
    return 0;
  }

  @Override
  public String getNom() {
    return H2dResource.getS("Chronique d'apport");
  }

  @Override
  public BPalettePlageAbstract getPalette() {
    initPal();
    return pal_;
  }

  @Override
  public boolean isDiscrete() {
    return true;
  }

  @Override
  public void paint(final Graphics2D _g, final GrMorphisme _versEcran, final GrBoite _clipReel, final int _alpha) {
    final GrBoite d = MvGridLayerGroup.getDomaine(param_.getMaillage());
    if (!d.intersectXY(_clipReel)) { return; }
    final GrPolygone p = new GrPolygone();
    final GrBoite pBoite = new GrBoite();
    final Color old = _g.getColor();
    initPal();
    final EfGridInterface g = param_.getMaillage();
    for (int i = g.getEltNb() - 1; i >= 0; i--) {
      final EvolutionReguliereInterface reg = param_.getAppMng().getEvol(i);
      if (reg != null) {
        MvElementModelDefault.initGrPolygone(g, g.getElement(i), p);
        p.boite(pBoite);
        if (pBoite.intersectXY(_clipReel)) {
          final BPlageInterface plage = pal_.getPlageFor(reg);
          if (plage != null) {
            _g.setColor(EbliLib.getAlphaColor(plage.getCouleur(), _alpha));
          }
          p.autoApplique(_versEcran);
          _g.fill(p.polygon());
        }
      }
    }
    _g.setColor(old);

  }

  @Override
  public String toString() {
    return getNom();
  }
}