/*
 * @creation 1 sept. 06
 * @modification $Date: 2007-06-14 12:01:40 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuUndoRedoInterface;
import java.io.File;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluExportDataInterface;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluUndoRedoInterface;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluFileChooserTestWritable;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;

class TrPostDataCreationFille extends BuInternalFrame implements CtuluUndoRedoInterface, BuUndoRedoInterface,
    CtuluExportDataInterface {

  TrPostDataCreationFille(final TrPostDataCreationPanel _pn) {
    super(CtuluLib.getS("Gestion des variables"), true, true, true, true);
    setContentPane(_pn);
    setTitle(CtuluLib.getS("Variables"));
    _pn.setName("ifPostVariable");
  }

  @Override
  public void clearCmd(final CtuluCommandManager _source) {
    if (_source != getCmdMng()) {
      getCmdMng().clean();
    }
  }

  @Override
  public CtuluCommandManager getCmdMng() {
    return ((TrPostDataCreationPanel) getContentPane()).cmd_;
  }

  @Override
  public String[] getEnabledActions() {
    return new String[] { CtuluExportDataInterface.EXPORT_CMD };
  }

  @Override
  public void redo() {
    getCmdMng().redo();
  }

  @Override
  public void setActive(final boolean _b) {}

  @Override
  public void startExport(final CtuluUI _impl) {
    final TrPostDataCreationPanel pn = (TrPostDataCreationPanel) getContentPane();
    if (pn.getNbVar() == 0) {
      _impl.warn(BuResource.BU.getString("Exporter"), CtuluLib.getS("Pas de variables � exporter"), false);
      return;
    }
    final CtuluFileChooser ch = new CtuluFileChooser();
    ch.setTester(new CtuluFileChooserTestWritable(_impl));
    final File f = FudaaGuiLib.chooseFile(_impl.getParentComponent(), true, ch);
    if (f != null) {
      pn.export(f);
    }

  }

  @Override
  public void undo() {
    getCmdMng().undo();
  }

}