/*
 * @creation 14 mars 2006
 * @modification $Date: 2007-05-04 14:01:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;
import java.io.File;
import java.util.*;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluOptionPane;
import org.fudaa.dodico.dico.DicoDataType;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.dico.DicoParamsChangeStateInterface;
import org.fudaa.dodico.dico.DicoParamsPrevalid;
import org.fudaa.dodico.ef.io.serafin.SerafinInterface;
import org.fudaa.dodico.h2d.telemac.H2dTelemacDicoFileFormatVersion;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author fred deniger
 * @version $Id: TrTelemacSiActivator.java,v 1.12 2007-05-04 14:01:46 deniger Exp $
 */
public class TrTelemacSiActivator extends DicoParamsPrevalid {

  final TrTelemacCommunParametres params_;

  /**
   * le mot-cl� definissant le booleen de reprise.
   */
  final DicoEntite repriseBooleanKw_;

  /**
   * le mot-cl� definissant le fichier de reprise.
   */
  final DicoEntite repriseFileKw_;

  final DicoEntite gridFileKw_;

  public TrTelemacSiActivator(final TrTelemacCommunParametres _params) {
    super(_params.getDicoParams());
    params_ = _params;
    final H2dTelemacDicoFileFormatVersion version = _params.getTelemacParametres().getTelemacVersion();
    gridFileKw_ = version.getMaillageEntiteFile();
    repriseBooleanKw_ = version.getRepriseBooleanKeyword();
    repriseFileKw_ = version.getRepriseFileKeyword();
  }

  DicoParams getDico() {
    return params_.getDicoParams();
  }

  DicoParamsChangeStateInterface getState() {
    return params_.getState().getProjectState();
  }

  /**
   * @return mot-cl� utilis� pour activer la reprise
   */
  public final DicoEntite getRepriseBooleanKw() {
    return repriseBooleanKw_;
  }

  public File getRepriseFile(final File _dir) {
    if (isValueSetAndValidForFile()) { return CtuluLibFile.getAbsolutePath(_dir, getDico().getValue(repriseFileKw_)); }
    return null;
  }

  /**
   * @return true si le fichier est deja charg�
   */
  public boolean isLoaded() {
    return getState().isLoaded(repriseFileKw_);
  }

  /**
   * @return true si le mot-cl� boolean de reprise est activ�
   */
  public final boolean isRepriseActivatedByUser() {
    return getDico().isValueSetFor(repriseBooleanKw_)
        && DicoDataType.Binaire.getValue(getDico().getValue(repriseBooleanKw_));
  }

  public boolean isSolutionInitialesActivated() {
    return isRepriseActivatedByUser();
  }

  /**
   * @return true si le mot-cl� du boolean est correctement initialis�
   */
  public final boolean isValueSetAndValidForBoolean() {
    return getDico().isValueSetFor(repriseBooleanKw_) && getDico().isValueValideFor(repriseBooleanKw_);
  }

  /**
   * @return true si le mot-cl� du fichier est correctement initialis�
   */
  public final boolean isValueSetAndValidForFile() {
    return getDico().isValueSetFor(repriseFileKw_) && getDico().isValueValideFor(repriseFileKw_);
  }

  /**
   * @return le fichier utilise pour le reprise.
   */
  public final DicoEntite getRepriseFileKeyword() {
    return repriseFileKw_;
  }

  @Override
  public boolean isChecked(final DicoEntite _ent) {
    return _ent == getSiBoolean();
  }

  /**
   * Enleve le mot-cl� definissant le fichier de reprise de calcul.
   * 
   * @param _cmd le receveur de commande
   */
  public final void removeFileKw(final CtuluCommandContainer _cmd) {
    getDico().removeValue(repriseFileKw_, _cmd);
  }

  DicoEntite getSiBoolean() {
    return repriseBooleanKw_;
  }

  public DicoEntite getGridFileKw() {
    return gridFileKw_;
  }

  @Override
  public boolean isChecked(final DicoEntite[] _ent) {
    return CtuluLibArray.containsObject(_ent, getSiBoolean());
  }

  /**
   * @author fred deniger
   * @version $Id: TrTelemacSiActivator.java,v 1.12 2007-05-04 14:01:46 deniger Exp $
   */
  protected static final class UndoRedoActivateSi implements CtuluCommand {
    private final boolean isGridModified_;
    private final boolean isRepModified_;
    private final TrTelemacSiActivator si_;

    /**
     * @param _isGridModified
     * @param _isRepModified
     */
    protected UndoRedoActivateSi(final TrTelemacSiActivator _si, final boolean _isGridModified,
        final boolean _isRepModified) {
      isGridModified_ = _isGridModified;
      isRepModified_ = _isRepModified;
      si_ = _si;

    }

    @Override
    public void redo() {
      final DicoEntite rep = si_.getRepriseFileKeyword();
      if (!si_.params_.getDicoParams().isValueSetFor(rep)) {
        si_.getDico().setValue(rep, TrTelemacSerafinHelper.getNewRepriseName());
      }
      si_.getState().setLoadedModified(rep, isRepModified_);

      rep.setRequired(true);
      si_.getState().setUnloaded(si_.getGridFileKw());
    }

    @Override
    public void undo() {
      si_.getState().setLoadedModified(si_.getGridFileKw(), isGridModified_);
      si_.getState().setUnloaded(si_.getRepriseFileKeyword());
      si_.getRepriseFileKeyword().setRequired(false);
    }
  }

  private abstract class SiAcceptAction {
    final CtuluCommandContainer mainCmd_;

    public SiAcceptAction(final CtuluCommandContainer _mainCmd) {
      mainCmd_ = _mainCmd;
    }

    public abstract void accept(CtuluCommandContainer _cmd);

    public abstract void refuse();

    public final void ok(final CtuluCommandComposite _cmp) {
      accept(_cmp);
      mainCmd_.addCmd(_cmp);
      params_.getImpl().unsetMainMessage();
      params_.getImpl().unsetMainProgression();
    }
  }

  private class SiAcceptActionChange extends SiAcceptAction {
    final String newVal_;

    final DicoEntite keywd_;

    public SiAcceptActionChange(final DicoEntite _keywd, final String _val, final CtuluCommandContainer _mainCmd) {
      super(_mainCmd);
      newVal_ = _val;
      keywd_ = _keywd;
    }

    @Override
    public void accept(final CtuluCommandContainer _cmd) {
      TrTelemacSiActivator.this.doSet(keywd_, newVal_, _cmd);
    }

    @Override
    public void refuse() {}
  }

  private class SiAcceptActionRemove extends SiAcceptAction {
    final DicoEntite[] keywd_;

    public SiAcceptActionRemove(final DicoEntite[] _keywd, final CtuluCommandContainer _mainCmd) {
      super(_mainCmd);
      keywd_ = _keywd;
    }

    @Override
    public void accept(final CtuluCommandContainer _cmd) {
      TrTelemacSiActivator.this.doRemove(keywd_, _cmd);
    }

    @Override
    public void refuse() {
      final List l = new ArrayList(Arrays.asList(keywd_));
      l.remove(repriseBooleanKw_);
      if (l.size() > 0) {
        TrTelemacSiActivator.this.doRemove((DicoEntite[]) l.toArray(new DicoEntite[l.size()]), mainCmd_);
      }

    }
  }

  @Override
  public void preAcceptSet(final DicoEntite _ent, final String _newVal, final CtuluCommandContainer _cmd) {
    if (_ent == getSiBoolean() && params_.getDicoParams().willChanged(_ent, _newVal)) {
      if (DicoDataType.Binaire.getValue(_newVal)) {
        activeSiDialog(new SiAcceptActionChange(_ent, _newVal, _cmd), true);
      } else {
        unactiveSI(new SiAcceptActionChange(_ent, _newVal, _cmd));
      }
    }
  }

  protected void saveFile(final ProgressionInterface _prg, final boolean _isActivatingRep) {
    final DicoEntite ent = _isActivatingRep ? gridFileKw_ : repriseBooleanKw_;
    if (getState().isLoadedAndModified(ent)) {
      final String txt = TrResource.getS(_isActivatingRep ? "Le fichier de g�om�trie est modifi�"
          : "Le fichier de reprise est modifi�")
          + CtuluLibString.LINE_SEP
          + TrResource
              .getS("Si vous ne le sauvegardez pas, vos derni�res modifications ne seront pas prises en compte.")
          + CtuluLibString.LINE_SEP + TrResource.getS("Voulez-vous le sauvegarder ?");
      if (params_.getImpl().question(_isActivatingRep ? TrTelemacSerafinHelper.getRepriseTitle() : getUnactiveTitle(),
          txt)) {
        params_.saveDicoEntiteFileAction(ent, params_.getFileSet(ent), _prg);
      }
    }
  }

  private boolean copy(final Map _src, final Map _dest) {
    boolean mod = false;
    for (final Iterator it = _src.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      if (!_dest.containsKey(e.getKey())) {
        _dest.put(e.getKey(), e.getValue());
        mod = true;
      }
    }
    return mod;
  }

  void loadReprise(final ProgressionInterface _prog, final File _f, final SiAcceptAction _act) {
    saveFile(_prog, true);
    final TrTelemacSerafinHelper builder = new TrTelemacSerafinHelper(params_);
    final HashMap repriseVals = new HashMap();
    final TrTelemacSerafinHelper.RepriseResults repriseModified = builder.loadRepriseData(_prog, _f, repriseVals);
    // il y a un probleme avec le fichier
    // on r�affiche la boite de dialogue
    if (repriseModified == null || repriseVals.size() == 0) {
      BuLib.invokeNow(new Runnable() {
        @Override
        public void run() {
          activeSiDialog(_act, false);
        }
      });
    } else {
      // on arrange
      final Map geoValues = params_.getTelemacParametres().getNodalProperties().getVarValues();
      repriseModified.isModified_ |= builder.determineBathyAndHForCI(repriseVals, geoValues);
      final VarConflictResult res = manageVarConflicts(geoValues, repriseVals, true);
      if (res.isCancelled_) {
        _act.refuse();
      } else {
        repriseModified.isModified_ |= res.isDestModified_;
        final CtuluCommandComposite cmp = new CtuluCommandCompositeInverse();
        params_.getTelemacParametres().activeSi(repriseVals, cmp);
        params_.getTelemacParametres().getSolutionsInit().setFirstTimeStep(repriseModified.timeStep_, cmp);
        cmp.addCmd(ciActivatedFileAct(repriseModified.isModified_));
        builder.setRepriseLoaded(repriseModified.isModified_);
        _act.ok(cmp);

      }
    }

  }

  String getUnactiveTitle() {
    return TrResource.getS("D�sactivation de la reprise de calcul");
  }

  static class VarConflictResult {
    boolean isCancelled_;

    boolean isDestModified_;

  }

  private VarConflictResult manageVarConflicts(final Map _src, final Map _dest, final boolean _isReprise) {
    final H2dVariableType[] vars = getVarWithConflict(_src, _dest);
    final VarConflictResult res = new VarConflictResult();
    if (!CtuluLibArray.isEmpty(vars)) {
      Arrays.sort(vars);
      final ChooseVar choose = new ChooseVar(vars, _isReprise);
      final CtuluDialog dial = createDialog(choose, true);
      dial.setTitle(_isReprise ? TrTelemacSerafinHelper.getRepriseTitle() : getUnactiveTitle());
      if (dial.afficheAndIsOk()) {
        // on r�cup�re les valeur de la source si demandee
        for (int i = 0; i < vars.length; i++) {
          if (choose.isKept(i)) {
            _dest.put(vars[i], _src.get(vars[i]));
            res.isDestModified_ = true;
          }
        }

      } else {
        res.isCancelled_ = true;
      }
    }
    // si l'action n'a pas �t� annul� par l'utilisateur
    if (!res.isCancelled_) {
      // on ajoute les variables non pr�sentes dans la cible
      res.isDestModified_ |= copy(_src, _dest);
    }
    return res;
  }

  private double[] getValues(final Object _o) {
    if (_o instanceof double[]) { return (double[]) _o; }
    return ((CtuluCollectionDouble) _o).getValues();
  }

  private CtuluDialog createDialog(final CtuluDialogPanel _pn, final boolean _isActivatingReprise) {
    return new CtuluDialog(params_.getImpl().getFrame(), _pn) {
      @Override
      protected JButton construireCancel() {
        final JButton bt = super.construireCancel();
        if (_isActivatingReprise) {
          bt.setToolTipText(TrResource.getS("Annuler la reprise de calcul"));
        } else {
          bt.setText(TrResource.getS("Annuler la d�sacivation de la reprise de calcul"));
        }
        return bt;
      }
    };
  }

  private static class ChooseVar extends CtuluDialogPanel {
    final H2dVariableType[] vars_;

    final boolean[] keepSrc_;

    final String keepMsg_;

    final String notKeepMsg_;

    public boolean isKept(final int _i) {
      return keepSrc_[_i];

    }

    class DataTableModel extends AbstractTableModel {

      @Override
      public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
        return _columnIndex == 1;
      }

      @Override
      public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
        if (_columnIndex == 1) {
          keepSrc_[_rowIndex] = keepMsg_.equals(_value);
        }
        super.setValueAt(_value, _rowIndex, _columnIndex);
      }

      @Override
      public int getColumnCount() {
        return 2;
      }

      @Override
      public String getColumnName(final int _column) {
        if (_column == 0) { return CtuluLib.getS("Variable"); }
        return CtuluLib.getS("Action");
      }

      @Override
      public int getRowCount() {
        return vars_.length;
      }

      @Override
      public Object getValueAt(final int _rowIndex, final int _columnIndex) {
        if (_columnIndex == 0) { return vars_[_rowIndex].toString(); }
        if (isKept(_rowIndex)) { return keepMsg_; }
        return notKeepMsg_;
      }

    }

    public ChooseVar(final H2dVariableType[] _var, final boolean _isActivatingRep) {
      keepMsg_ = TrResource.getS(_isActivatingRep ? "Conserver les valeurs du fichier de g�om�trie"
          : "Conserver les valeurs du fichier de reprise");
      notKeepMsg_ = TrResource.getS(_isActivatingRep ? "R�cup�rer les valeurs du fichier de reprise"
          : "R�cup�rer les valeurs du fichier de g�om�trie");
      keepSrc_ = new boolean[_var.length];
      vars_ = _var;
      Arrays.fill(keepSrc_, false);
      final BuTable table = new BuTable(new DataTableModel());
      table.getColumnModel().getColumn(1).setCellEditor(
          new DefaultCellEditor(new JComboBox(new String[] { notKeepMsg_, keepMsg_ })));
      setLayout(new BuBorderLayout());
      add(new BuScrollPane(table), BuBorderLayout.CENTER);
      final CtuluHtmlWriter writer = new CtuluHtmlWriter(true);
      writer.setDefaultResource(TrResource.TR);
      writer
          .addi18n(
              "Des variables avec des valeurs diff�rentes sont pr�sentes dans le fichier de reprise et le fichier de g�om�trie.")
          .nl().addi18n("Vous pouvez choisir ci-dessous la source � utiliser pour ces variables.");
      add(new BuLabel(writer.getHtml()), BuBorderLayout.NORTH);
    }

  }

  H2dVariableType[] getVarWithConflict(final Map _src, final Map _dest) {
    if (_src == null) { return null; }
    final Set l = new HashSet(_src.keySet());
    // on r�cupere les variables communes
    l.retainAll(_dest.keySet());
    if (l.size() == 0) { return null; }
    // on va enlever les variables qui ont les memes valeurs dans les 2 tables
    for (final Iterator it = l.iterator(); it.hasNext();) {
      final H2dVariableType v = (H2dVariableType) it.next();
      if (CtuluLibArray.isDoubleEquals(getValues(_src.get(v)), getValues(_dest.get(v)), 1E-15)) {
        it.remove();
      }
    }
    return l.isEmpty() ? null : (H2dVariableType[]) l.toArray(new H2dVariableType[l.size()]);

  }

  CtuluCommand ciActivatedFileAct(final boolean _isRepModified) {
    final boolean isGridModified = getState().isLoadedAndModified(getGridFileKw());
    final CtuluCommand r = new UndoRedoActivateSi(this, isGridModified, _isRepModified);
    r.redo();
    return r;
  }

  CtuluCommand ciUnactivatedFileAct(final boolean _isGridModified) {
    final DicoEntite repriseFileKeyword = getRepriseFileKeyword();
    final DicoParams dicoParams = params_.getDicoParams();
    final boolean isRepriseModified = getState().isLoadedAndModified(repriseFileKeyword);
    final String oldValue = dicoParams.getSetValue(repriseFileKeyword);
    final CtuluCommand r = new CtuluCommand() {

      @Override
      public void redo() {
        getState().setLoadedModified(getGridFileKw(), _isGridModified);
        repriseFileKeyword.setRequired(false);
        getState().setUnloaded(repriseFileKeyword);
        dicoParams.removeValue(repriseFileKeyword);

      }

      @Override
      public void undo() {
        getState().setLoadedModified(repriseFileKeyword, isRepriseModified);
        getState().setUnloaded(getGridFileKw());
        if (oldValue != null) {
          dicoParams.setValue(repriseFileKeyword, oldValue);
        }
        repriseFileKeyword.setRequired(true);
      }
    };
    // pour lancer l'action demandee
    r.redo();
    return r;
  }

  protected void unactiveSI(final SiAcceptAction _act) {
    final String message = "<html><body>" + TrResource.getS("Vous allez d�sactiver la reprise de calcul.") + "<br>"
        + TrResource.getS("Les donn�es du fichier de reprise seront ajout�es au fichier de g�om�trie.") + "<br>"
        + TrResource.getS("Voulez-vous r�cup�rer des donn�es du fichier de g�om�trie ?");
    final CtuluOptionPane p = new CtuluOptionPane(message, JOptionPane.QUESTION_MESSAGE);
    final String[] opt = new String[] { TrResource.getS("Ne pas r�cup�rer de donn�es"),
        TrResource.getS("R�cup�rer des donn�es"), TrResource.getS("Annuler la d�sactivation") };
    p.setOptions(opt);
    CtuluOptionPane.showDialog(params_.getImpl().getFrame(), getUnactiveTitle(), p);
    final Object res = p.getValue();
    if (res == null || opt[2].equals(res)) {
      _act.refuse();
    } else {
      final CtuluTaskDelegate task = params_.getImpl().createTask(getUnactiveTitle());
      Runnable r = null;
      if (opt[0].equals(res)) {
        r = new Runnable() {
          @Override
          public void run() {
            // on sauvegarde si n�cessaire le fichier de reprise avant la basculement
            saveFile(task.getStateReceiver(), false);
            final CtuluCommandComposite cmp = new CtuluCommandCompositeInverse();
            final Map geoValues = params_.getTelemacParametres().getSolutionsInit().getVarValues();
            params_.getTelemacParametres().getSolutionsInit().removeRepriseValues(geoValues);
            params_.getTelemacParametres().unactiveSi(geoValues, cmp);
            cmp.addCmd(ciUnactivatedFileAct(true));
            _act.ok(cmp);
          }
        };

      } else if (opt[1].equals(res)) {
        r = new Runnable() {
          @Override
          public void run() {
            saveFile(task.getStateReceiver(), false);
            unactiveSI(_act, task.getStateReceiver());
          }
        };
      }
      task.start(r);
    }

  }

  protected void unactiveSI(final SiAcceptAction _act, final ProgressionInterface _prg) {
    final TrTelemacSerafinHelper helper = new TrTelemacSerafinHelper(params_);
    final SerafinInterface ser = helper.readGridFile(helper.getFileSet(gridFileKw_), _prg);
    final Map geoValues = helper.buildVariables(ser, helper.getTimeStepToUse(ser), null);
    final Map repriseValues = helper.getH2DParams().getSolutionsInit().getVarValues();
    helper.getH2DParams().getSolutionsInit().removeRepriseValues(repriseValues);
    final VarConflictResult res = manageVarConflicts(repriseValues, geoValues, false);
    if (res.isCancelled_) {
      _act.refuse();
    } else {
      final CtuluCommandComposite cmp = new CtuluCommandCompositeInverse();
      params_.getTelemacParametres().unactiveSi(geoValues, cmp);
      cmp.addCmd(ciUnactivatedFileAct(res.isDestModified_));
      _act.ok(cmp);

    }

  }

  @Override
  public void preAcceptRemoved(final DicoEntite[] _ent, final CtuluCommandContainer _cmd) {
    if (CtuluLibArray.containsObject(_ent, getSiBoolean()) && params_.getDicoParams().willRemoved(getSiBoolean())) {
      unactiveSI(new SiAcceptActionRemove(_ent, _cmd));
    }
  }

  public File getFileSetInDico() {
    return params_.getFileSet(repriseFileKw_);
  }

  public double getCoteInitial() {
    final DicoEntite e = params_.getTelemacParametres().getTelemacVersion().getInitialElevationKw();
    double coteInitValue = 0.5 * (params_.getTelemacParametres().getMaillage().getMaxZ() + params_
        .getTelemacParametres().getMaillage().getMinZ());
    try {
      if (params_.getTelemacParametres().getDicoParams().isValueSetFor(e)) {
        final String coteInit = params_.getTelemacParametres().getDicoParams().getValue(e);
        coteInitValue = Double.parseDouble(coteInit);
      }
    } catch (final NumberFormatException _evt) {

    }
    return coteInitValue;
  }

  protected void activeSiDialog(final SiAcceptAction _act, final boolean _isFirst) {
    final boolean allowFile = !params_.isOlbUsed();
    File f = null;
    if (_isFirst && allowFile) {
      f = params_.getFileSet(repriseFileKw_);
      if (f != null && (!f.exists() || f.length() == 0)) {
        f = null;
      }
    }
    final TrTelemacSiActivateDialog dialog = new TrTelemacSiActivateDialog(this, allowFile, f, _isFirst);

    if (dialog.afficheModaleOk(params_.getImpl().getFrame(), TrTelemacSerafinHelper.getRepriseTitle())) {
      final CtuluTaskDelegate task = params_.getImpl().createTask(TrTelemacSerafinHelper.getRepriseTitle());
      if (dialog.isInitFromH()) {
        task.start(new Runnable() {
          @Override
          public void run() {
            saveFile(task.getStateReceiver(), true);
            final CtuluCommandComposite cmp = new CtuluCommandCompositeInverse();
            params_.getTelemacParametres().activeSi(dialog.getCoteInitiale(),
                params_.getTelemacParametres().getNodalProperties().getVarValues(), cmp);
            cmp.addCmd(ciActivatedFileAct(true));
            _act.ok(cmp);
          }

        });
      } else {
        task.start(new Runnable() {
          @Override
          public void run() {
            loadReprise(task.getStateReceiver(), dialog.getRepFile(), _act);
          }
        });
      }
    } else {
      _act.refuse();
    }

  }
}
