/**
 * @creation 16 juin 2004
 * @modification $Date: 2007-03-02 13:01:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextArea;
import org.apache.commons.collections.CollectionUtils;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.*;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryFlowrateGroupType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrCourbeTemporelleManager;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFlowrateGroupEditorPanel.java,v 1.28 2007-03-02 13:01:34 deniger Exp $
 */
public final class TrRubarFlowrateGroupEditorPanel extends JPanel implements CtuluUndoRedoInterface, H2dRubarBcListener {
  class BooleanEditor extends CtuluCellBooleanEditor {
    @Override
    public boolean getBooleanValue(final Object _o) {
      return mng_.isFlowrateGroupDefined((H2dRubarBoundaryFlowrateGroupType) _o);
    }
  }

  private class EnabledCellRenderer extends CtuluCellBooleanRenderer {
    public EnabledCellRenderer() {
      setText(CtuluLibString.EMPTY_STRING);
      revalidate();
    }

    @Override
    public void setValue(final Object _value) {
      setSelected(mng_.isFlowrateGroupDefined((H2dRubarBoundaryFlowrateGroupType) _value));
    }
  }

  private class EvolutionEditor extends DefaultCellEditor {
    JComboBox cb_;

    public EvolutionEditor() {
      super(new BuComboBox());
    }

    @Override
    public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _isSelected, final int _row,
                                                 final int _column) {
      cb_ = (JComboBox) editorComponent;
      final ComboBoxModel cb = getModel(tableModel_.getVar(_column));
      cb_.setModel(CtuluListModelEmpty.EMPTY);
      cb_.setModel(cb);
      return super.getTableCellEditorComponent(_table, _value, _isSelected, _row, _column);
    }
  }

  class FlowrateGroupModel extends AbstractTableModel implements H2dRubarBcListener {
    @Override
    public void areteTypeChanged() {
      //nothing to do
    }

    @Override
    public void bathyChanged() {
      //nothing to do
    }

    @Override
    public void nodeInGridChanged() {
      //nothing to do
    }

    @Override
    public void fondDurChanged() {
      //nothing to do
    }

    @Override
    public void projectTypeChanged() {
      //nothing to do
    }

    @Override
    public void numberOfConcentrationChanged() {
      //nothing to do
    }

    @Override
    public void flowrateGroupChanged(final H2dRubarBoundaryFlowrateGroupType _t) {
      table_.clearSelection();
      final int row = table_.getSelectedRow();
      if (row >= 0) {
        table_.addRowSelectionInterval(row, row);
      }
      table_.addColumnSelectionInterval(1, 1);
      fireTableDataChanged();
    }

    @Override
    public void timeClChanged() {
      //nothing to do
    }

    @Override
    public int getColumnCount() {
      if (!parameter.isTransport()) {
        return 5;
      }
      return 5 + H2dRubarOuvrage.getNbVariableTransport() * parameter.getBcMng().getNbConcentrationBlocks();
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) {
        if (!(_value instanceof Boolean)) {
          return;
        }
        final boolean isActivated = (Boolean) _value;
        if (isActivated) {
          final H2dRubarTimeCondition tc = new H2dRubarTimeCondition(parameter.getBcMng().getNbConcentrationBlocks());
          ComboBoxModel m = getModel(H2dVariableType.DEBIT_NORMAL);
          tc.setEvolution(H2dVariableType.DEBIT_NORMAL, (H2dRubarEvolution) m.getElementAt(0), 0, null, false);
          m = getModel(H2dVariableType.DEBIT_TANGENTIEL);
          tc.setEvolution(H2dVariableType.DEBIT_TANGENTIEL, (H2dRubarEvolution) m.getElementAt(0), 0, null, false);
          m = getModel(H2dVariableType.COTE_EAU);
          tc.setEvolution(H2dVariableType.COTE_EAU, (H2dRubarEvolution) m.getElementAt(0), 0, null, false);

          H2dRubarEvolution hcat = null;
          H2dRubarEvolution diam = null;
          H2dRubarEvolution et = null;
          if (parameter.isTransport()) {
            for (int idxBlock = 0; idxBlock < parameter.getBcMng().getNbConcentrationBlocks(); idxBlock++) {
              m = getModel(H2dVariableTransType.CONCENTRATION);
              tc.setEvolution(H2dVariableTransType.CONCENTRATION, (H2dRubarEvolution) m.getElementAt(0), idxBlock, null, false);
              m = getModel(H2dVariableTransType.DIAMETRE);
              tc.setEvolution(H2dVariableTransType.DIAMETRE, (H2dRubarEvolution) m.getElementAt(0), idxBlock, null, false);
              m = getModel(H2dVariableTransType.ETENDUE);
              tc.setEvolution(H2dVariableTransType.ETENDUE, (H2dRubarEvolution) m.getElementAt(0), idxBlock, null, false);
            }
          }

          // on prend le seul format qui est commun au trois variable
          final List ls = tc.getInvalidEvol();
          if (CollectionUtils.isNotEmpty(ls)) {
            String msg = null;
            if (ls.size() == 1) {
              msg = TrResource.getS("La courbe {0} contient des valeurs erron�es. Ces valeurs seront supprim�es.\nVoulez-vous continuer ?", ls.get(0)
                  .toString());
            } else {
              final StringBuilder b = TrLib.join(ls);
              msg = TrResource.getS("Les courbes {0} contiennent des valeurs erron�es. " + "Ces valeurs seront supprim�es.\nVoulez-vous continuer ?",
                  b.toString());
            }
            if (!CtuluLibDialog.showConfirmation(TrRubarFlowrateGroupEditorPanel.this, TrResource.getS("Attention"), msg)) {
              return;
            }
          }
          addCmd(mng_.setDefined(groupType_[_rowIndex], tc));
        } else {
          addCmd(mng_.setUndefined(groupType_[_rowIndex]));
        }
      } else if (_columnIndex > 1) {
        if (_value == null) {
          return;
        }
        H2dRubarEvolution e = (H2dRubarEvolution) _value;
        if (e == H2dRubarParameters.NON_TORENTIEL) {
          e = null;
        }
        final CtuluNumberFormater fmt = H2dRubarTimeCondition.getFmt(H2dVariableType.DEBIT_NORMAL);
        if (e != null && e.isIdxErrorForFmt(H2dRubarEvolution.getDefaultTimeFmt(), fmt)) {
          final String msg = TrResource.getS("La courbe {0} contient des valeurs erron�es. Ces valeurs seront supprim�es.\nVoulez-vous continuer ?",
              e.getNom());
          if (!CtuluLibDialog.showConfirmation(TrRubarFlowrateGroupEditorPanel.this, TrResource.getS("Attention"), msg)) {
            return;
          }
        }
        addCmd(mng_.setEvolution(groupType_[_rowIndex], e, getVar(_columnIndex), getBlockIdx(_columnIndex)));
      }
    }

    protected H2dVariableType getVar(final int _column) {
      return TrRubarBlockHelper.getVar(_column);
    }

    public int getBlockIdx(final int column) {
      return TrRubarBlockHelper.getBlockIdx(column);
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return CtuluLibString.ESPACE;
      }
      if (_column == 1) {
        return TrResource.getS("Groupe");
      }
      int block = getBlockIdx(_column);
      String variableName = getVar(_column).getName();
      if (block > 0) {
        variableName += CtuluLibString.getEspaceString(block + 1);
      }

      return variableName;
    }

    @Override
    public int getRowCount() {
      return groupType_.length;
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex <= 1) {
        return groupType_[_rowIndex];
      }
      final H2dRubarTimeCondition t = mng_.getGroupTimeCondition(groupType_[_rowIndex]);
      if (t == null || t.getQnEvol() == null || t.getQtEvol() == null) {
        return CtuluLibString.EMPTY_STRING;
      }
      if (_columnIndex == 2) {
        return t.getQnEvol().getNom();
      }
      if (_columnIndex == 3) {
        return t.getQtEvol().getNom();
      }
      if (_columnIndex == 4) {
        H2dRubarEvolution h = t.getHEvol();
        if (h == null) {
          h = H2dRubarParameters.NON_TORENTIEL;
        }
        return h.getNom();
      }
      return t.getEvol(getVar(_columnIndex), getBlockIdx(_columnIndex));
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) {
        return evolMng_.getNbEvolCommonAndFor(H2dVariableType.DEBIT_M3) > 0;
      } else if (_columnIndex == 1) {
        return false;
      }
      return mng_.isFlowrateGroupDefined(groupType_[_rowIndex]);
    }
  }

  protected void addCmd(final CtuluCommand _m) {
    cmd_.addCmd(_m);
  }

  final CtuluCommandManager cmd_;
  H2dRubarBoundaryFlowrateGroupType[] groupType_;
  H2dRubarBcMng mng_;
  TrCourbeTemporelleManager evolMng_;
  JTable table_;
  Map varModel_;

  protected ComboBoxModel getModel(final H2dVariableType _t) {
    if (varModel_ == null) {
      varModel_ = new HashMap(3);
    }
    ComboBoxModel r = (ComboBoxModel) varModel_.get(_t);
    if (r == null) {
      if (_t == H2dVariableTransType.CONCENTRATION) {
        r = evolMng_.createComboBoxModel(H2dVariableTransType.CONCENTRATION, true);
      } else if (_t == H2dVariableTransType.DIAMETRE) {
        r = evolMng_.createComboBoxModel(H2dVariableTransType.DIAMETRE, true);
      } else if (_t == H2dVariableTransType.ETENDUE) {
        r = evolMng_.createComboBoxModel(H2dVariableTransType.ETENDUE, true);
      } else if (_t == H2dVariableType.COTE_EAU) {
        r = evolMng_.createComboBoxModel(_t, H2dRubarParameters.NON_TORENTIEL);
      } else {
        r = evolMng_.createComboBoxModel(H2dVariableType.DEBIT_M3, true);
      }
      varModel_.put(_t, r);
    }
    return r;
  }

  FlowrateGroupModel tableModel_;
  final H2dRubarParameters parameter;

  /**
   * @param _para les parametres.
   */
  public TrRubarFlowrateGroupEditorPanel(final H2dRubarParameters _para, final TrCourbeTemporelleManager _courbeMng) {
    super();
    cmd_ = new CtuluCommandManager();
    evolMng_ = _courbeMng;
    this.parameter = _para;

    // evolMng_.addListener(this);
    mng_ = _para.getBcMng();
    mng_.addListener(this);
    groupType_ = mng_.getDebitGroupType();
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    table_ = new JTable();
    setLayout(new BuBorderLayout(5, 5));
    tableModel_ = new FlowrateGroupModel();
    mng_.addListener(tableModel_);
    table_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    table_.setModel(tableModel_);
    final TableColumnModel c = table_.getColumnModel();
    c.getColumn(0).setCellRenderer(new EnabledCellRenderer());
    c.getColumn(0).setCellEditor(new BooleanEditor());
    c.getColumn(0).setMaxWidth(25);
    TableColumn col = c.getColumn(1);
    col.setCellRenderer(new NameCellRenderer());
    initCourbeCellRenderer();
    table_.revalidate();
    add(new BuScrollPane(table_));
    final BuTextArea txt = new BuTextArea(5, 80);
    txt.setEditable(false);
    txt.setLineWrap(true);
    txt.setWrapStyleWord(true);
    txt.setText(getHelpText());
    txt.setMargin(new Insets(3, 3, 3, 3));
    add(txt, BuBorderLayout.SOUTH);
  }

  private void initCourbeCellRenderer() {
    final TableColumnModel c = table_.getColumnModel();
    final EvolutionEditor evolEditor = new EvolutionEditor();
    final CtuluCellTextRenderer r = new CtuluCellTextRenderer();

    for (int i = 2; i < tableModel_.getColumnCount(); i++) {
      TableColumn column = c.getColumn(i);
      column.setCellEditor(evolEditor);
      column.setCellRenderer(r);
    }
    CtuluLibSwing.packJTable(table_);
  }

  /**
   * @return l'aide.
   */
  public String getHelpText() {
    if (CtuluLib.isFrenchLanguageSelected()) {
      return getFrHelpText();
    }
    return "The flowrate groupes can be specified only if curves are available. You can import"
        + " curves thanks to the menu File>Import.\n\nIf the name of a group is displayed in blue,"
        + " it means that this group is used by an edge and can't be cleared";
  }

  /**
   * @return l'aide en francais.
   */
  public String getFrHelpText() {
    return "Les groupes sont modifiables seulement si des courbes sont disponibles. Vous pouvez "
        + "les importer gr�ce au menu Fichier>Import.\n\nSi un groupe est affich� en bleu, cela signifie "
        + "qu'il est utilis� par une ar�te et il ne peut pas �tre enlev�";
  }

  class NameCellRenderer extends CtuluCellTextRenderer {
    @Override
    protected void setValue(final Object _value) {
      if (mng_.isUsedBoundaryType((H2dRubarBoundaryFlowrateGroupType) _value)) {
        setForeground(Color.blue);
      } else {
        setForeground(Color.black);
      }
      setText(_value.toString());
    }
  }

  @Override
  public void nodeInGridChanged() {

  }

  @Override
  public void bathyChanged() {

  }

  @Override
  public void fondDurChanged() {

  }

  @Override
  public void numberOfConcentrationChanged() {
    rebuildTable();
  }

  @Override
  public void projectTypeChanged() {
    rebuildTable();
  }

  public void rebuildTable() {
    this.tableModel_.fireTableStructureChanged();
    if (parameter.isTransport()) {
      initCourbeCellRenderer();
    }
  }

  @Override
  public void timeClChanged() {

  }

  @Override
  public void areteTypeChanged() {

  }

  @Override
  public void flowrateGroupChanged(H2dRubarBoundaryFlowrateGroupType _t) {

  }

  @Override
  public void clearCmd(final CtuluCommandManager _source) {
    if ((cmd_ != null) && (_source != cmd_)) {
      cmd_.clean();
    }
  }

  @Override
  public CtuluCommandManager getCmdMng() {
    return cmd_;
  }

  @Override
  public void setActive(final boolean _b) {
    if (table_.getCellEditor() != null) {
      table_.getCellEditor().stopCellEditing();
    }
  }
}
