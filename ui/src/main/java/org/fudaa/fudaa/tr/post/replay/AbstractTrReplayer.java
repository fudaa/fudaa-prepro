package org.fudaa.fudaa.tr.post.replay;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuBorders;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuList;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuToolBar;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.calque.BCalquePaletteInfo;
import org.fudaa.ebli.calque.action.EbliCalqueActionTimeChooser;
import org.fudaa.ebli.calque.edition.BPaletteEdition;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.controle.BSelecteurListComboBox;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.find.EbliFindDialog;
import org.fudaa.ebli.find.EbliFindableItem;
import org.fudaa.ebli.palette.BPaletteInfo;
import org.fudaa.fudaa.tr.common.TrDataSourceNomme;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourceStringComparator;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.post.profile.MVProfileCourbeModel;

/**
 * Classe qui se charge d'afficher et rejouer les donn�es d'origine des courbes TR. C'set a dire les courbes spatiales et evolutions temporelles.
 *
 * @author Adrien Hadoux
 */
public class AbstractTrReplayer {

  /**
   * Genere une interface qui liste les fichiers disponibles et retourne l'entier correspondant au choix de l'utilisateur. Cet entier correspond a
   * l'indice du fichier dans l'arrayliste des src du trpostprojet.
   *
   * @param _init
   * @param _title
   * @param _parent
   * @return
   */
  public static TrPostSource selectSource(final TrPostProjet projet, final String _title, final Component _parent,
          TrPostSource toAvoid) {
    List<TrPostSource> lstSources = new ArrayList<TrPostSource>(projet.getSources().getLstSources());
    if (toAvoid != null) {
      lstSources.remove(toAvoid);
    }
    TrPostSource[] sources = (TrPostSource[]) lstSources.toArray(new TrPostSource[lstSources.size()]);
    Arrays.sort(sources, new TrPostSourceStringComparator());
    final BuList l = CtuluLibSwing.createBuList(sources);
    l.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    l.setCellRenderer(new CtuluCellTextRenderer() {
      @Override
      protected void setValue(Object value) {
        super.setValue(((TrPostSource) value).getFormatedTitle());
      }
    });
    final CtuluDialogPanel pn = new CtuluDialogPanel(false) {
      @Override
      public boolean isDataValid() {
        return !l.isSelectionEmpty();
      }
    };
    pn.setLayout(new BuBorderLayout());
    pn.setBorder(BuBorders.EMPTY3333);
    pn.add(new BuLabel(CtuluLib.getS("S�lectionner le fichier r�sultat")), BuBorderLayout.NORTH);
    pn.add(new BuScrollPane(l));
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_parent, _title))) {
      return sources[l.getSelectedIndex()];
    }
    return null;
  }
  protected final BuRadioButton ajouter_ = new BuRadioButton(TrLib.getString("Cr�er une nouvelle courbe"));
  protected EbliCalqueActionTimeChooser chooserT;
  protected BSelecteurListComboBox comboTime;
  protected BSelecteurListComboBox comboVar_ = null;
  protected final BuRadioButton ecraser_ = new BuRadioButton(TrLib.getString("Ecraser la courbe existante"));
  protected final ButtonGroup group_ = new ButtonGroup();
  protected BuButton rejouerSrc_ = null;
  protected final TrPostSource source;
  protected final TrPostCommonImplementation implementation;

  public AbstractTrReplayer(TrPostSource source,
          final TrPostCommonImplementation implementation) {
    group_.add(ecraser_);
    group_.add(ajouter_);
    ecraser_.setSelected(true);
    this.source = source;
    this.implementation = implementation;
  }
//  CtuluNumberFormatI formatter;

  public Callable<Boolean> getTrueCallable() {
    return new Callable<Boolean>() {
      @Override
      public Boolean call() throws Exception {
        return Boolean.TRUE;
      }
    };
  }

  /**
   * Cree la dialog qui affiche tout le contenu d'origine du graphe.
   *
   * @param vue2d
   * @param implementation
   * @param modele
   */
  protected TrReplayDialog constructDialog(Callable<Boolean> actionOnOk, TrPostVisuPanel vue2d, final EGModel modele,
          final JComponent composantAdditionnel, final boolean modeEdition, final String title, final EbliActionSimple genereVue2d) {

    implementation.createTask(TrResource.getS("Origine ") + " " + modele.getTitle());
    final TrReplayDialog pn = new TrReplayDialog(false, implementation.getParentComponent(), actionOnOk);
    pn.setLayout(new BuBorderLayout(5, 15));
    pn.setBorder(BuBorders.EMPTY3333);
    pn.add(vue2d, BuBorderLayout.CENTER);
    if (composantAdditionnel != null) {
      pn.add(composantAdditionnel, BuBorderLayout.SOUTH);
    }

    final JPanel panelSouth = new JPanel(new FlowLayout(FlowLayout.LEFT));
    final JPanel panelSouthReplay = new JPanel(new BuGridLayout(2, 5, 5, false, false));
    panelSouth.add(vue2d.getLabelSuiviSouris());
    pn.add(panelSouthReplay, BorderLayout.NORTH);

    final JPanel panelTotalSouth = new JPanel(new GridLayout(3, 1));

    if (title != null) {
      panelTotalSouth.add(new JLabel(title));
    } else {
      panelTotalSouth.add(new JLabel(""));
    }

    panelTotalSouth.add(panelSouth);
    pn.add(panelTotalSouth, BuBorderLayout.SOUTH);

    // -- ajout de la toolbar --//
    pn.add(constructToolBarVue2d(vue2d, modeEdition), BorderLayout.NORTH);

    if (modeEdition) {
      // -- ajout de la palette d'infos --//
      final Collection<EbliActionPaletteAbstract> acts = vue2d.getController().getTabPaletteAction();
      BPaletteInfo comp = null;
      vue2d.getController().buildActions();
      final JPanel compEast = new JPanel(new BorderLayout());
      for (final EbliActionPaletteAbstract pals : acts) {
        final JComponent component = pals.getPaletteContent();
        if (component instanceof BCalquePaletteInfo) {
          comp = (BPaletteInfo) component;
          comp.setAvailable(true);
          comp.updateState();
          pals.setEnabled(true);
          pals.updateBeforeShow();
          compEast.add(comp, BuBorderLayout.CENTER);

        } else if ((modele instanceof MVProfileCourbeModel) && (component instanceof BPaletteEdition)) {
          compEast.add(comp, BuBorderLayout.SOUTH);
        }
      }
      final EbliFindDialog dialog = new EbliFindDialog(vue2d);
      dialog.setSelectedFindAction((EbliFindableItem) vue2d.getCalqueActif());
      final JTabbedPane panelEAST = new JTabbedPane();
      panelEAST.addTab(TrLib.getString("Rechercher"), new JScrollPane(dialog));
      panelEAST.addTab(TrLib.getString("Infos Points, Variables"), compEast);
      pn.add(panelEAST, BuBorderLayout.EAST);
      panelSouth.add(this.ecraser_);
      panelSouth.add(this.ajouter_);
      if (this.rejouerSrc_ != null) {
        final TrDataSourceNomme postModel = (TrDataSourceNomme) modele;
        final JPanel pnEtat = new JPanel(new BuGridLayout(2, 5, 3));
        pnEtat.add(new JLabel(TrResource.TR.getString("Le fichier r�sultat utilis� actuellement:")));
        pnEtat.add(new JLabel(postModel.getSourceName()));
        pnEtat.add(new JLabel(TrResource.TR.getString("Le fichier r�sultat qui sera utilis�:")));
        pnEtat.add(new JLabel(vue2d.getSource().getMainFile().getName()));
        panelSouthReplay.add(pnEtat);
        panelSouthReplay.add(rejouerSrc_);
      }

    } else if (genereVue2d != null) {

      // -- ajout de l'action de cr�ation de la vue 2d correspondante --//
      panelSouthReplay.add(genereVue2d.buildButton(EbliComponentFactory.INSTANCE));
    }
    pn.setPreferredSize(new Dimension(950, 700));
    pn.doLayout();
    vue2d.restaurer();
    return pn;

  }

  protected TrReplayDialog constructEditionDialog(final Callable<Boolean> actionOnOk, TrPostVisuPanel vue2d, final EGModel modele, final JComponent composantAdditionnel,
          final String title) {
    return constructDialog(actionOnOk, vue2d, modele, composantAdditionnel, true, title, null);

  }

  public JToolBar constructToolBarVue2d(final TrPostVisuPanel vue2d, final boolean modeEdition) {
    final JToolBar toolbarCalque = new BuToolBar();
    AbstractButton boutonSelection = null;
    final List<EbliActionInterface> actions = vue2d.getController().getNonSpecificsActions();// getSelectedNavigationAndStandardActionGroup();
    int i = 0;
    for (final Iterator<EbliActionInterface> iterator = actions.iterator(); iterator.hasNext();) {
      final EbliActionInterface object = iterator.next();// actions[i];
      if (object == null) {
        toolbarCalque.addSeparator();
      } else {

        if (i == 0) {
          boutonSelection = object.buildToolButton(EbliComponentFactory.INSTANCE);
          toolbarCalque.add(boutonSelection);
        } else {
          toolbarCalque.add(object.buildToolButton(EbliComponentFactory.INSTANCE));
        }
      }
      i++;
    }
    if (modeEdition) {

      toolbarCalque.add(vue2d.getSondeAction().buildToolButton(EbliComponentFactory.INSTANCE));

      // -- activer par d�faut l'action selection de points --//
      if (boutonSelection != null) {
        boutonSelection.doClick();
      }

    } else {
      toolbarCalque.remove(0);
      toolbarCalque.remove(0);
    }

    // -- ajout des variables + pas de temps --//
    if (comboVar_ != null) {
      vue2d.getArbreCalqueModel().getTreeSelectionModel().removeTreeSelectionListener(comboVar_);
    }
    if (chooserT != null) {
      vue2d.getArbreCalqueModel().getTreeSelectionModel().removeTreeSelectionListener(chooserT);
    }

    comboVar_ = new BSelecteurListComboBox();
    vue2d.getArbreCalqueModel().getTreeSelectionModel().addTreeSelectionListener(comboVar_);
    comboVar_.setPalettePanelTarget(vue2d.getArbreCalqueModel().getSelectedCalque());
    comboVar_.setPreferredSize(new Dimension(Math.max(comboVar_.getPreferredSize().width, 100), comboVar_.getPreferredSize().height));
    comboVar_.getCb().setMaximumSize(
            new Dimension(Math.max(comboVar_.getPreferredSize().width, 100), comboVar_.getPreferredSize().height));
    comboVar_.getCb().setMinimumSize(
            new Dimension(Math.max(comboVar_.getPreferredSize().width, 100), comboVar_.getPreferredSize().height));
    if (modeEdition) {
      toolbarCalque.add(comboVar_);
    }
    chooserT = new EbliCalqueActionTimeChooser(vue2d.getArbreCalqueModel().getTreeSelectionModel(), true);
    // pour activer l'action
    chooserT.setSelected(true);
    comboTime = (BSelecteurListComboBox) chooserT.getPaletteContent();
    chooserT.updateBeforeShow();
    comboTime.setPreferredSize(new Dimension(Math.max(comboTime.getPreferredSize().width, 100), comboVar_.getPreferredSize().height));
    comboTime.getCb().setMaximumSize(
            new Dimension(Math.max(comboTime.getPreferredSize().width, 100), comboVar_.getPreferredSize().height));
    comboTime.getCb().setMinimumSize(
            new Dimension(Math.max(comboTime.getPreferredSize().width, 100), comboVar_.getPreferredSize().height));
    if (modeEdition) {
      toolbarCalque.add(comboTime);
    }
    return toolbarCalque;

  }

  protected Runnable createRestaurerRunnable(final TrPostVisuPanel vue2d) {
    return new Runnable() {
      @Override
      public void run() {
        vue2d.restaurer();
      }
    };
  }

  /**
   * Methode qui epure la toolbar de toutes les options qui ne sont pas utiles. Utilis�es pour les modes origines qui n'ont besoin
   *
   * @param toolBar
   */
  public void epurerToolBar(final JToolBar toolBar) {
  }

  public String format(final double value) {
    return source == null ? Double.toString(value) : source.getPrecisionModel().getFormatter().format(value);
  }

  public String format(final double x, final double y) {
    return CtuluNumberFormatDefault.formatPoint(x, y, source == null ? null : source.getPrecisionModel().getFormatter());
  }

  TrPostVisuPanel buildVue2d(final TrPostCommonImplementation implementation, final TrPostSource src,
          final TrReplayVisuCustomizer customizer) {
    return TrReplayVue2dBuilder.constructVue2d(implementation, src, customizer);

  }
}
