/**
 * @creation 21 ao�t 2003
 * @modification $Date: 2007-06-14 12:01:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.data;

import com.db4o.ObjectContainer;
import com.memoire.bu.BuUndoRedoInterface;
import java.awt.Frame;
import javax.swing.Action;
import javax.swing.JMenu;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluHelpComponent;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.image.CtuluImageImporter;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZEbliFilleCalques;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.save.FudaaFilleVisuPersistence;
import org.fudaa.fudaa.commun.save.FudaaSavable;
import org.fudaa.fudaa.tr.common.TrFilleVisuInterface;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrFilleVisu.java,v 1.29 2007-06-14 12:01:41 deniger Exp $
 */
public class TrFilleVisu extends ZEbliFilleCalques implements BuUndoRedoInterface, TrFilleVisuInterface,
    CtuluExportDataInterface, CtuluHelpComponent, FudaaSavable, CtuluImageImporter {

  /**
   * @param _pn le panneau
   */
  public TrFilleVisu(final TrVisuPanel _pn) {
    super(_pn, _pn.getImpl(), null);
    setClosable(true);
    setTitle(TrResource.getS("Vue 2D"));
    super.setName("ifView2D");
  }

  private void fillWithHelp(final BCalque _c, final CtuluHtmlWriter _buf, final boolean _top) {
    final String desc = (String) _c.getClientProperty(Action.SHORT_DESCRIPTION);

    final String layerTitle = _c.getTitle();
    if (_top) {
      _buf.h2Styled(layerTitle, "padding-left:2px;background-color:#CFDCED");
    } else {
      _buf.h3Styled(layerTitle, "padding-left:2px;margin-left:5px;background-color:#EFEEFE");
    }
    final String helplink = CtuluLibSwing.buildLinkToHelp(_c);
    if (helplink != null) {
      _buf.close(_buf.addStyledTag("div", "padding-left:5px"), helplink);
    }
    if (desc != null) {
      _buf.h3Styled(CtuluLib.getS("Description"), "padding-left:2px;margin-left:10px");
      _buf.close(_buf.paraStyled("margin-top:0px;margin-left:10px"), desc);
    }
    final EbliActionInterface[] acs = ((TrVisuPanel) pn_).getController().getCalqueActions(_c);
    if (acs != null && acs.length > 0) {
      _buf.h3Styled(CtuluLib.getS("Actions disponibles:"), "margin-left:10px;padding-left:2px");
      _buf.append("<table border=\"1\" style=\"margin-left:10px\" cellpadding=\"3px\">");
      _buf.append("<tr><th align=\"left\">").append(CtuluResource.CTULU.getString("Action")).append(
          "</th><th align=\"left\">").append(CtuluResource.CTULU.getString("Description")).append(
          "</th><th align=\"left\">").append(CtuluResource.CTULU.getString("Condition d'utilisation")).append(
          "</th></tr>");
      for (int i = 0; i < acs.length; i++) {
        if (acs[i] != null) {
          _buf.append("<tr><td>").append((String) acs[i].getValue(Action.NAME)).append("</td><td>");
          String tl = acs[i].getDefaultTooltip();
          if (tl.equals(acs[i].getValue(Action.NAME))) {
            _buf.append("&nbsp;");
          } else {
            _buf.append(tl);
          }
          _buf.append("</td><td>");
          tl = acs[i].getEnableCondition();
          if (tl == null) {
            _buf.append("&nbsp;");
          } else {
            _buf.append(tl);
          }
          _buf.append("</td></tr>");
        }
      }
      _buf.append("</table>");

    }
  }

  protected String buildShortHelp() {

    final CtuluHtmlWriter writer = new CtuluHtmlWriter();
    writer.setDefaultResource(TrResource.TR);
    writer.para();
    writer.addi18n("Affiche le maillage du projet et permet de modifier les param�tres physiques.");
    writer.nl();
    writer.addStyledTag("ul", "margin-left:10px");
    writer.close(writer.addTag("li"), getImpl().buildLink(TrResource.getS("Fonctionnement global de la vue 2D"),
        "common-layers"));
    writer.close(writer.addTag("li"), getImpl().buildLink(TrResource.getS("Exporter des donn�es"),
        "common-layers-export"));
    writer.close(writer.addTag("li"), getImpl().buildLink(TrResource.getS("Rechercher, s�lectionner des objets"),
        "common-layers-selection-find"));
    return writer.getHtml();

  }

  @Override
  public void clearCmd(final CtuluCommandManager _source) {
    getTrVisuPanel().clearCmd(_source);
  }

  @Override
  public CtuluCommandManager getCmdMng() {
    return getTrVisuPanel().getCmdMng();
  }

  /**
   * @return le menu pour afficher le numero des elements
   */
  public final JMenu getElementNumberDisplayMenu() {
    return getTrVisuPanel().getElementNumberDisplayMenu();
  }

  @Override
  public final String[] getEnabledActions() {
    return new String[] { "RECHERCHER", "IMPRIMER", "MISEENPAGE", "PREVISUALISER", CtuluExportDataInterface.EXPORT_CMD,
        CtuluLibImage.SNAPSHOT_COMMAND, "TOUTSELECTIONNER", "INVERSESELECTION", "CLEARSELECTION", "IMPORT_IMAGE" };
  }

  /**
   * @return la fenetre de l'impl
   */
  public Frame getFrame() {
    return getImpl().getFrame();
  }

  /**
   * @return l'imp parent
   */
  public FudaaCommonImplementation getImpl() {
    return getTrVisuPanel().getImpl();
  }

  @Override
  public String getShortHtmlHelp() {
    final BCalque[] cs = pn_.getDonneesCalque().getCalques();
    if (cs == null || cs.length == 0) { return null; }
    final CtuluHtmlWriter r = new CtuluHtmlWriter();
    r.h1Center(getTitle(), true);
    final int nb = cs.length;
    final String shortHelp = buildShortHelp();
    if (shortHelp != null) {
      r.h2(CtuluResource.CTULU.getString("Description"));
      r.append(shortHelp);
    }

    final BCalque c = getCalqueActif();
    if (c != null) {
      r.nl();
      r.h1Center(TrResource.getS("Calque s�lectionn�"), true);
      fillWithHelp(c, r, true);
    }
    r.nl();
    r.nl();
    r.h1Center(TrResource.getS("Liste des calques"), true);
    for (int i = 0; i < nb; i++) {
      fillWithHelp(cs[i], r, true);
      final BCalque[] souscq = cs[i].getCalques();
      if (souscq != null && souscq.length > 0) {
        for (int j = 0; j < souscq.length; j++) {
          fillWithHelp(souscq[j], r, false);
        }
      }

    }
    return r.toString();
  }

  public final TrVisuPanel getTrVisuPanel() {
    return (TrVisuPanel) pn_;
  }

  @Override
  public void importImage() {
    getTrVisuPanel().importImage();

  }

  /**
   * @return true si les elements sont editables
   */
  public boolean isGridElementEditable() {
    return getTrVisuPanel().isGridElementEditable();
  }

  /**
   * @return true si les points sont editables
   */
  public boolean isGridPointEditable() {
    return getTrVisuPanel().isGridPointEditable();
  }

  /**
   *
   */
  @Override
  public void redo() {
    getTrVisuPanel().redo();
  }

  /**
   * @param _cq mise a jour de l'arbre
   */
  public void refreshArbreCalque(final BArbreCalque _cq) {
    final BArbreCalqueModel arbre = getArbreCalqueModel();
    _cq.setModel(arbre);
    _cq.refresh();
  }

  @Override
  public void replace() {}

  @Override
  public void saveIn(final CtuluArkSaver _writer, final ProgressionInterface _prog) {
    new FudaaFilleVisuPersistence(this).saveIn(_writer, _prog);
  }

  @Override
  public void saveIn(final ObjectContainer _db, final ProgressionInterface _prog) {
    new FudaaFilleVisuPersistence(this).saveIn(_db, _prog);
  }

  @Override
  public void setActive(final boolean _b) {
    getTrVisuPanel().setActive(_b);

  }

  @Override
  public void startExport(final CtuluUI _impl) {
    getTrVisuPanel().startExport(_impl);
  }

  /**
   *
   */
  @Override
  public void undo() {
    getTrVisuPanel().undo();
  }

  /**
   * Mis a jour des infos.
   */
  @Override
  public final void updateInfoComponentAndIso() {
    if (pn_ != null) {
      ((TrVisuPanel) pn_).updateInfoAndIso();
    }
  }

  @Override
  public void updateIsoVariables() {
    ((TrVisuPanel) pn_).updateIsoVariables();
  }
}