/**
 * @creation 19 sept. 2003
 * @modification $Date: 2007-06-11 13:08:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.fu.FuLib;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Collection;
import java.util.List;
import javax.swing.JInternalFrame;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZEbliFilleCalques;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.save.FudaaSaveLib;
import org.fudaa.fudaa.commun.save.FudaaSaveMainData;
import org.fudaa.fudaa.commun.save.FudaaSaveProject;
import org.fudaa.fudaa.sig.FSigProjectPersistence;

/**
 * @author deniger
 * @version $Id: TrLib.java,v 1.31 2007-06-11 13:08:20 deniger Exp $
 */
public final class TrLib {

  private TrLib() {}

  public static String getString(final String _s) {
    return TrResource.TR.getString(_s);
  }

  /**
   * @param _vars la collection de variables
   * @param _id l'identifiant a rechercher
   * @return true si la collection contient une variable dont l'identifiant est egale � _id.
   */
  public static boolean containId(Collection<H2dVariableType> _vars, String _id) {
    if (_vars == null || _id == null) return false;
    for (H2dVariableType variableType : _vars) {
      if (_id.equals(variableType.getID())) return true;
    }
    return false;
  }

  public static Runnable restoreEditorMainFille(final BuCommonImplementation _impl, final File _param,
      final ZEbliFilleCalques _fille, final ProgressionInterface _prog) {
    return FudaaSaveLib.restoreMainFille(_impl, getPreSaveFile(_param), FSigProjectPersistence.getPreSaveFileOld(_param), _fille, _prog);
  }

  public static FudaaSaveMainData getMainDataOldProject(final File _f) {
    return FudaaSaveLib.getProjectDataOld(FSigProjectPersistence.getPreSaveFileOld(_f));
  }

  public static FileFormatSoftware findPreMainVersion(final File _f) {
    if (_f == null) { return null; }
    final File zip = getPreSaveFile(_f);
    if (zip.exists()) { return FudaaSaveProject.getProjectVersion(zip); }
    final File old = FSigProjectPersistence.getPreSaveFileOld(_f);
    if (old.exists()) { return (FileFormatSoftware) FudaaSaveLib.getUniqueDataInDb(old, FileFormatSoftware.class); }
    return null;
  }


  public static File getPreSaveFile(final File _file) {
    if (_file == null) { return null; }
    return new File(_file.getAbsolutePath() + TrProjectPersistence.getPreProjectExt());
  }

  /**
   * @return le prefixe pour les solutions initiales
   */
  public static String getSIDisplayName() {
    return (CtuluLib.isFrenchLanguageSelected() ? "CI" : "IC");
  }

  public static void initFrameDimensionWithPref(final JInternalFrame _frame, final Dimension _proposedDim) {
  }

  public static void initFrameDimensionWithPref(final JInternalFrame _frame, final String _name,
      final Dimension _proposedDim) {
  }

  public static void closeInternalFrame(final BuCommonImplementation _impl, final JInternalFrame _f) {
    if (_f != null) {
      closeInternalFrame(_f.getName(), _impl, _f);
    }
  }

  public static void closeInternalFrame(final String _name, final BuCommonImplementation _impl, final JInternalFrame _f) {
    if (_f != null) {
      if (_name != null) {
        TrLib.saveFrameDimensionInPref(_name, _f);
      }
      _f.setVisible(false);
      _impl.removeInternalFrame(_f);
      _f.dispose();
    }
  }

  /**
   * Ferme la fenetre interne et sauvegarde les dimensions dans les preferences grace au nom pass�. en parametres
   * Close the interior window and save the dimensions in he preference with the help of the name in parameter
   */
  public static void saveFrameDimensionInPref(final JInternalFrame _frame) {
    if (_frame == null) { return; }
    saveFrameDimensionInPref(_frame.getName(), _frame);
  }

  public static void saveFrameDimensionInPref(final String _name, final JInternalFrame _frame) {

  }

  /**
   * Renvoie la duree en seconde.
   * 
   * @param _y nb annees
   * @param _m nb mois
   * @param _d nb jours
   * @param _h nb heures
   * @param _min nb minutes
   * @param _s nb secondes
   * @return nb seconde totale
   */
  public static long getDuration(final int _y, final int _m, final int _d, final int _h, final int _min, final int _s) {
    return 86400L * _y * 365L + _s + _min * 60L + _h * 3600L + _d * 86400L + _m * 86400L * 30L;
  }

  /**
   * initialise <code>_containter</code> avec [year,month,day,hour,minut,sec]: le tableau DOIT contenir au moins 6
   * rangs.
   * 
   * @param _container le tableau dans lequel les donnees seront mises
   * @param _s le nombre de secondes a convertir
   */
  public static void getYearMonthDayTime(final int[] _container, final long _s) {
    long d = 365L * 86400L;
    long timeRest = _s;
    long value = timeRest / d;
    _container[0] = (int) value;
    if (value > 0) {
      timeRest += -value * d;
    }
    d = 30L * 86400L;
    value = timeRest / d;
    _container[1] = (int) value;
    if (value > 0) {
      timeRest += -value * d;
    }
    d = 86400;
    value = timeRest / d;
    _container[2] = (int) value;
    if (value > 0) {
      timeRest += -value * d;
    }
    d = 3600;
    value = timeRest / d;
    _container[3] = (int) value;
    if (value > 0) {
      timeRest += -value * d;
    }
    d = 60;
    value = timeRest / d;
    _container[4] = (int) value;
    if (value > 0) {
      timeRest += -value * d;
    }
    _container[5] = (int) timeRest;
  }

  public static int chooseTimeStepToUse(final String[] _timeStep, final FudaaCommonImplementation _imp) {
    final String warn = "<html><p>"
        + TrResource.getS("Le fichier de g�om�trie comporte plusieurs pas de temps.<br>"
            + "Si ce fichier est modifi�, seules les valeurs correspondantes au dernier "
            + "pas de temps seront sauvegard�es.") + "</p></html>";
    final BuComboBox cb = new BuComboBox(_timeStep);
    cb.setSelectedIndex(_timeStep.length - 1);
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuVerticalLayout(6));
    pn.add(new BuLabel(warn));
    pn.add(new BuLabel(TrResource.getS("Choisir le pas de temps � utiliser")));
    pn.add(cb);
    final CtuluDialog s = new CtuluDialog(_imp.getFrame(), pn);
    s.setOption(CtuluDialog.OK_OPTION);
    s.setTitle(TrResource.getS("Chargement fichier de g�om�trie"));
    s.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    s.afficheDialogModal();
    s.dispose();
    return cb.getSelectedIndex();
  }

  public static BuMenuItem buildAideContextItem(final ActionListener _l) {
    final BuMenuItem it = new BuMenuItem(BuResource.BU.getIcon("aide"), BuResource.BU.getString("Aide"));
    it.setActionCommand("AIDE");
    it.setEnabled(true);
    it.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0, false));
    if (_l != null) {
      it.addActionListener(_l);
    }
    return it;

  }

  public static String getMajURl(final BuInformationsSoftware _soft) {
    String url = _soft.update;
    if (isJnlp()) {
      url += "&jws=true";
    }
    if (CtuluLib.isFrenchLanguageSelected()) {
      url += "&lang=fr";
    }
    return url;
  }

  public static String getJavaws() {
    return FuLib.getJavaHome() + File.separator + "bin" + File.separator + "javaws";
  }

  public static boolean isJnlp() {
    return "true".equals(FuLib.getSystemProperty("fudaa.jnlp.mode"));
  }

  public static void addJavawsForJnlp(final BuMenu _menu) {
    if (isJnlp()) {
      _menu.addMenuItem(FudaaLib.getS("Gestionnaire des applications"), "LAUNCH_JAVAWS", BuResource.BU.getIcon("java"),
          true);
    }
  }

  public static void addJava3DJMFTest(final BuMenu _r) {
    _r.addSeparator();
    _r.addMenuItem(TrResource.getS("Tester Java 3D"), "JAVA3D", EbliResource.EBLI.getIcon("3d"), true);
  }

  public static String getMessageAlreadyOpen(final File _f) {
    return TrResource.getS("Le fichier {0} est d�j� ouvert", _f == null ? "?" : _f.getName());
  }

  /**
   * formatte le titre
   * 
   * @param file
   * @return
   */
  public static String formatName(String title) {
    return  title.replaceAll("  ", "");
  }

  public static String formatFichier(final File file) {
    if (file == null) { return CtuluLibString.EMPTY_STRING; }
    return file.getAbsolutePath();
  }

  public  static StringBuilder join(List ls) {
    final StringBuilder b = new StringBuilder();
    b.append(ls.get(0).toString());
    for (int i = 1; i < ls.size(); i++) {
      b.append(',').append(ls.get(i).toString());
    }
    return b;
  }
}
