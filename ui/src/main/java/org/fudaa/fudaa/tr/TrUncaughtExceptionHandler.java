/*
 *  @creation     8 mars 2005
 *  @modification $Date: 2007-05-04 14:01:52 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr;

import com.memoire.bu.BuLib;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.fudaa.tr.common.TrBugCommentGenerator;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * Le thread parent permettant de r�cup�rer toutes les exceptions.
 *
 * @author Fred Deniger
 * @version $Id: TrUncaughtExceptionHandler.java,v 1.3 2007-05-04 14:01:52 deniger Exp $
 */
public final class TrUncaughtExceptionHandler {
  private TrUncaughtExceptionHandler() {
  }

  public static void uncaughtExceptionOccured(final Thread _thread, final Throwable _evt) {
    final StackTraceElement[] elts = _evt.getStackTrace();
    final int max = Math.min(3, elts == null ? 0 : elts.length);
    final StringBuffer mes = new StringBuffer(100);
    mes.append(_evt.toString());
    if (max >= 0 && elts != null) {
      for (int i = 0; i < max; i++) {
        mes.append('\n').append(elts[i].toString());
      }
    }
    _evt.printStackTrace(System.err);
    if (CtuluLibMessage.DEBUG) {
      CtuluLibDialog.showError(BuLib.HELPER, _thread.getName(), mes.toString());
    } else {
      try {
        final String message = "<html><body><p>"
            + TrResource.getS("Pour nous aider � am�liorer cette application, vous pouvez nous signaler l'erreur.")
            + "<br><br>" + TrResource.getS("Pour ce faire, vous allez �tre redirig� vers le gestionnaire de bogues Fudaa, o� vous pourrez,") + "<br>" + TrResource
            .getS("une fois inscrit, cr�er une nouvelle demande et suivre l'avancement de son traitement.")
            + "<br><br>" + TrResource.getS("Le d�tail technique de l'erreur va �tre plac� dans le presse-papiers.") + "<br>" + TrResource
            .getS("Vous n'aurez plus qu'� le coller (Ctrl+V) dans la description de la demande.") + "</p><p><br>"
            + TrResource.getS("Souhaitez-vous acc�der au gestionnaire de bogues Fudaa ") + "?</p></body></html>";
        final boolean b = CtuluLibDialog.showErrorConfirmation(BuLib.HELPER,
            TrResource.getS("Une erreur est survenue") + '!', message);
        if (b) {
          TrBugCommentGenerator.sendACommentWithBugTitle();
        }
      } catch (Exception ex) {

      }
    }
  }
}
