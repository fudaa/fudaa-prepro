package org.fudaa.fudaa.tr.post;

import java.util.Set;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreatedDefault;

public class TrPostRubarSedimentFondVariable extends TrPostDataCreatedDefault {

  private final H2dRubarSedimentVariableType variable;
  private final H2dRubarSedimentInterface sedimentData;

  private final String name;

  public TrPostRubarSedimentFondVariable(TrPostSourceRubar _src, H2dRubarSedimentVariableType variable, H2dRubarSedimentInterface sedimentData) {
    super(_src);
    this.variable = variable;
    this.sedimentData = sedimentData;
    name = TrResource.TR.getString("Altitude du fond couche {0}", CtuluLibString.getString(variable.getCoucheIdx() + 1));
  }

  @Override
  public String getDescription() {
    return name;
  }

  @Override
  public void fillWhithAllUsedVar(Set _res) {
    _res.add(H2dVariableType.BATHYMETRIE);
    _res.add(variable);
  }

  @Override
  public EfData buildDataFor(int _idxTime) {
    double[] values = new double[sedimentData.getNbNoeuds()];
    EfData bathy = src_.getData(H2dVariableType.BATHYMETRIE, _idxTime);
    if (bathy == null) {
      bathy = new EfDataNode(src_.getGrid().getPtsNb());
    }
    if (bathy.isElementData()) {
      if (src_.getGrid().getNeighbors() == null) {
        src_.getGrid().computeNeighbord(null, null);
      }
      bathy = src_.getGrid().getNeighbors().getDataNode(variable, bathy, null, null);
    }

    for (int i = 0; i < values.length; i++) {
      values[i] = bathy.getValue(i);
      int maxCouches = variable.getCoucheIdx();
      for (int j = 0; j <= maxCouches; j++) {
        values[i] = values[i] - sedimentData.getCoucheContainer(_idxTime, i).getCoucheItem(j).getValue(variable.getParentVariable());
      }
    }
    return new EfDataNode(values);
  }

  @Override
  public double buildDataFor(int _idxTime, int _idxObject) {
    return sedimentData.getCoucheContainer(_idxTime, _idxObject).getCoucheItem(variable.getCoucheIdx()).getValue(variable.getParentVariable());
  }

}
