package org.fudaa.fudaa.tr.post.persist;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * Manager qui indique toutes les erreurs qui se sont produites pendant le chargement.
 *
 * @author Adrien Hadoux
 *
 */
public class TrPostPersistanceErrorManager {

  java.util.List<String> listeMessageError = new ArrayList<String>();

  public void addMessageError(String message) {
    listeMessageError.add(message);
  }

  public void clear() {

    listeMessageError.clear();

  }

  private static class ModeleTableError implements TableModel {

    java.util.List<String> liste_;

    public ModeleTableError(List<String> listeMessage) {
      super();
      liste_ = listeMessage;
    }

    @Override
    public int getColumnCount() {
      return 1;
    }

    @Override
    public String getColumnName(int column) {
      return "Messages";
    }

    @Override
    public int getRowCount() {
      return liste_.size();
    }

    @Override
    public Object getValueAt(int row, int column) {
      return liste_.get(row);
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return false;
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
    }
  }

  public void showDialog(CtuluUI ui) {
    if (listeMessageError.isEmpty()) {
//			ui.message(TrResource.getS("Chargement termin�"), TrResource.getS("Chargement termin� avec succ�s"), false);
      return;
    }

    JTable table = new JTable(new ModeleTableError(listeMessageError));
    final Frame f = CtuluLibSwing.getFrameAncestorHelper(ui.getParentComponent());
    final JDialog dialog_ = new JDialog(f);
    dialog_.setModal(true);
    dialog_.setTitle("Chargement termin�");
    final JPanel container = new JPanel(new BorderLayout());
    container.add(new JScrollPane(table), BorderLayout.CENTER);

    container.add(new JLabel(TrResource.getS("Chargement termin� avec erreur(s)")), BorderLayout.NORTH);

    JButton ajouter_ = new JButton("Valider", EbliResource.EBLI.getIcon("crystal_valider"));
    ajouter_.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        dialog_.dispose();
      }
    });
    container.add((new JPanel()).add(ajouter_), BorderLayout.SOUTH);

    dialog_.setContentPane(container);
    dialog_.setLocationRelativeTo(ui.getParentComponent());
    //dialog_.pack();
    dialog_.setSize(600, 250);
    dialog_.setVisible(true);

  }
}
