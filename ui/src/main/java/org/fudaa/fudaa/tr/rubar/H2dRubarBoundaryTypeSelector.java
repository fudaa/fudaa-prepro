/**
 * 
 */
package org.fudaa.fudaa.tr.rubar;

import java.util.List;
import javax.swing.JComboBox;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryType;

/**
 * @author CANEL Christophe (Genesis)
 *
 */
public class H2dRubarBoundaryTypeSelector extends CtuluDialogPanel {
  
  private JComboBox combo;
  public H2dRubarBoundaryTypeSelector(List<H2dRubarBoundaryType> types)
  {
    combo = new JComboBox(types.toArray());
    
    this.add(combo);
  }
  
  public H2dRubarBoundaryType getSelectedType()
  {
    return (H2dRubarBoundaryType)combo.getSelectedItem();
  }
}
