package org.fudaa.fudaa.tr.post;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.CalqueGISTreeModel;
import org.fudaa.ebli.calque.CalqueGISTreeModel.LayerNode;
import org.fudaa.ebli.calque.ZCalqueMultiPoint;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.tr.TrPostImplementation;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostWizardCourbeTemporelle.ModelListePoints;

/**
 * Construit un composant arbre g�n�rique qui permet de r�cup�rer les points des calques d'une vue 2d. Utiliser surtout
 * pour l'assistant de cr�ation d'�volutions temporelles.<br />
 * 
 * @author Adrien Hadoux
 */
public abstract class TrEvolutionBuilderFromTree {

  public static class PostEvolutionTemporelles extends TrEvolutionBuilderFromTree {
    /**
     * le modele du tableau a mettre a jour
     */
    ModelListePoints modeletableau_;
    /**
     * la liste des points. Peut etre des points reels mais aussi interpol�s
     */
    @SuppressWarnings("unchecked")
    List listePoints_;

    public PostEvolutionTemporelles(TrPostVisuPanel panel, ModelListePoints modeletableau, List liste) {
      super(panel);
      modeletableau_ = modeletableau;
      listePoints_ = liste;

    }

    @Override
    protected void apply() {

      // -- mise a jour des donn�es du tableau--//
      modeletableau_.fireTableDataChanged();
    }

    @Override
    public boolean ajouterSelection(ZCalqueMultiPoint cq) {
      boolean modifierStructure = false;

      TrPostInterpolatePoint newPoint = null;
      if (cq == null || cq.modeleDonnees() == null) return false;

      int nb = cq.modeleDonnees().getNombre();

      for (int i = 0; i < nb; i++) {
        GrPoint point = (GrPoint) cq.modeleDonnees().getObject(i);
        if (point != null) {
          final int idxElt = TrIsoLayerDefault.sondeSelection(point, panel_.getIsoLayer().getIsoModel());
          if (idxElt > -1) {
            newPoint = new TrPostInterpolatePoint(idxElt, point.x_, point.y_, panel_.getSource().getPrecisionModel());
            modifierStructure = true;
            listePoints_.add(newPoint);
          }
        }
      }
      return modifierStructure;

    }

    @Override
    public boolean ajouterSelection(ZCalquePoint cq) {
      boolean modifierStructure = false;
      TrPostInterpolatePoint newPoint = null;
      if (cq == null || cq.modeleDonnees() == null) return false;

      int nb = cq.modeleDonnees().getNombre();

      for (int i = 0; i < nb; i++) {
        GrPoint point = cq.getPoint(i);
        if (point != null) {
          final int idxElt = TrIsoLayerDefault.sondeSelection(point, panel_.getIsoLayer().getIsoModel());
          if (idxElt > -1) {
            newPoint = new TrPostInterpolatePoint(idxElt, point.x_, point.y_, panel_.getSource().getPrecisionModel());
            modifierStructure = true;
            if (canAdd(newPoint)) listePoints_.add(newPoint);
          }
        }
      }
      return modifierStructure;
    }

    @Override
    public boolean ajouterSelection(ZCalquePoint cq, int i) {
      boolean modifierStructure = false;
      TrPostInterpolatePoint newPoint = null;
      if (cq == null || cq.modeleDonnees() == null) return false;
      GrPoint point = cq.getPoint(i);
      if (point != null) {
        final int idxElt = TrIsoLayerDefault.sondeSelection(point, panel_.getIsoLayer().getIsoModel());
        if (idxElt > -1) {
          newPoint = new TrPostInterpolatePoint(idxElt, point.x_, point.y_, panel_.getSource().getPrecisionModel());
          modifierStructure = true;
          if (canAdd(newPoint)) listePoints_.add(newPoint);
        }
      }

      return modifierStructure;
    }

    private boolean canAdd(TrPostInterpolatePoint pt) {
      for (Object ob : listePoints_) {
        if (ob instanceof TrPostInterpolatePoint) {
          TrPostInterpolatePoint tr = (TrPostInterpolatePoint) ob;
          if (tr.equals(pt)) return false;
        }
      }
      return true;

    }
  }

  /**
   * Liste des points selectionn�s.
   */
  protected GISZoneCollectionPoint pointsSelectionnees_;

  final TrPostVisuPanel panel_;

  protected TrPostImplementation impl_;

  /**
   * l'arbre des donn�es representant le calque
   */
  public JTree tree_;

  /**
   * le linetreemodel aui filtre uniquement les type d'objets g�om�triaues recherch�s.
   */
  final CalqueGISTreeModel lineTreeModel_;

  public TrEvolutionBuilderFromTree(TrPostVisuPanel panel) {
    this(panel, null);
  }

  public TrEvolutionBuilderFromTree(TrPostVisuPanel panel, TrPostImplementation impl) {

    this.panel_ = panel;
    this.impl_ = impl;
    pointsSelectionnees_ = null;
    lineTreeModel_ = new CalqueGISTreeModel(null, panel.getDonneesCalque());
    lineTreeModel_.setMask(GISLib.MASK_POINT);
  }

  public GISZoneCollectionPoint getSelection() {

    if (tree_ == null) return null;

    return null;
  }

  public JTree buildTree() {
    if (tree_ == null) {
      tree_ = lineTreeModel_.createView(false, false);
      // -- gestion double click pour r�cup�rer l'information
      tree_.addMouseListener(new MouseAdapter() {

        @Override
        public void mouseClicked(MouseEvent e) {
          if (e.getClickCount() < 2) return;
          final TreePath clickedElement = tree_.getPathForLocation(e.getX(), e.getY());
          LayerNode treeNode = null;
          if (clickedElement != null && clickedElement.getLastPathComponent() instanceof LayerNode) treeNode = (LayerNode) clickedElement
              .getLastPathComponent();

          if (treeNode == null) return;
          boolean apply = false;
          if (treeNode.getUserObject() instanceof ZCalquePoint && treeNode.isLeaf())

          apply = ajouterSelection((ZCalquePoint) treeNode.getUserObject(), treeNode.getIdxGeom());
          else if (treeNode.getUserObject() instanceof ZCalquePoint) apply = ajouterSelection((ZCalquePoint) treeNode
              .getUserObject());
          else if (treeNode.getUserObject() instanceof ZCalqueMultiPoint) apply = ajouterSelection((ZCalqueMultiPoint) treeNode
              .getUserObject());
          else if (treeNode.getUserObject() instanceof BGroupeCalque) {

            if (treeNode.getChildCount() > 0 && treeNode.getChildAt(0) instanceof LayerNode) {

              LayerNode treeNodeFils = (LayerNode) treeNode.getChildAt(0);
              if (treeNodeFils.getChildCount() > 0 && treeNodeFils.getChildAt(0) instanceof LayerNode
                  && !treeNodeFils.isLeaf()) treeNodeFils = (LayerNode) treeNodeFils.getChildAt(0);
              if (treeNodeFils.getUserObject() instanceof ZCalquePoint) {
                // -- on recupere le calque global et on l'ajoute direct --//
                ZCalquePoint zcq = (ZCalquePoint) treeNodeFils.getUserObject();
                if (zcq != null) apply = ajouterSelection(zcq);
              }

            }

          }
          // -- on fait appel a pply qui met a jour les composants voulues --//
          if (apply) apply();
        }
      });
    }
    return tree_;
  }

  /**
   * est appel�e lors du double clic sur le tree.
   * 
   * @param objectSelection
   * @return
   */
  public abstract boolean ajouterSelection(ZCalqueMultiPoint cq);

  public abstract boolean ajouterSelection(ZCalquePoint cq);

  public abstract boolean ajouterSelection(ZCalquePoint cq, int indice);

  /**
   * applique les modifications (listener a appeler ou autre)...
   */
  protected abstract void apply();

}
