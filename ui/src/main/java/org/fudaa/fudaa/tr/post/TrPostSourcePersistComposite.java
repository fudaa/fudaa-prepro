package org.fudaa.fudaa.tr.post;

import java.util.ArrayList;
import java.util.List;

/**
 * Persistance du TrPostSourceReader composite utilis� pour les sauvegardes suites de calcul. 
 * Contient plusieurs ids vers les TrPostSourceReaderInterface correspondants.
 * 
 * @author Adrien Hadoux
 */
public class TrPostSourcePersistComposite {

	/**
	 * Liste des id des fichiers qui composent la suite.
	 */
	public List<String> listeIdFicResu;

	public String Id;
	
	/**
	 * Utiliser par la serialization.
	 */
	TrPostSourcePersistComposite(){
		listeIdFicResu=new ArrayList<String>();
	}

	/**
	 * Constructeur qui se charge de remplir les donn�es persistantes.
	 * @param composite
	 */
	public 	TrPostSourcePersistComposite(TrPostSourceReaderInterface srccomposite,String id){
		listeIdFicResu=new ArrayList<String>();
		TrPostSourceReaderComposite composite=(TrPostSourceReaderComposite)srccomposite;
		for(TrPostSourceReaderInterface interf: composite.listeSourceInterface_)
			listeIdFicResu.add(interf.getId());
		Id=id;
	}



	/**
	 * Reconstruit la suite de calcul ou composite a partir du projet et de la liste d id.
	 * @param projet
	 * @return
	 */
	public TrPostSource generateComposite(TrPostProjet projet){
		return TrPostBuilderSuiteCalcul.createSuiteCalculFromPersistance(listeIdFicResu,projet,this.Id);
	}


}
