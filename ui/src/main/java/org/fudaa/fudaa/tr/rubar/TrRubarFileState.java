/**
 * @creation 1 oct. 2004
 * @modification $Date: 2007-06-29 15:09:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.fileformat.FileFormat;

import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFileState.java,v 1.12 2007-06-29 15:09:41 deniger Exp $
 */
public abstract class TrRubarFileState implements Comparable<TrRubarFileState> {
  FileFormat fmt_;
  private boolean isMarkRemoved_;
  private boolean isModified_;
  protected File initFile_;
  protected long lastModifiedTime_;
  TrRubarFileStateListener l_;

  /**
   * @return le fichier stockant les donn�es.
   */
  public final File getInitFile() {
    return initFile_;
  }

  /**
   * @return l'extension du format.
   */
  public String getExtension() {
    return fmt_.getExtensions()[0];
  }

  /**
   * @param _dir le repertoire de dest
   * @param _projectName le nom du projet (nom du fichier)
   * @param _progression la progression
   * @param _projet le projet source
   * @return la synthese de l'operation
   */
  public abstract CtuluIOOperationSynthese save(File _dir, String _projectName, ProgressionInterface _progression,
                                                TrRubarProject _projet);

  /**
   * @param _initFile le fichier source
   */
  public final void setInitFile(final File _initFile) {
    initFile_ = _initFile;
    if (initFile_ == null) {
      lastModifiedTime_ = 0;
    } else {
      lastModifiedTime_ = initFile_.lastModified();
    }
  }

  /**
   * @return true si le fichier est a jour
   */
  public final boolean isLoadedFileUpToDate() {
    return (initFile_ != null) && (lastModifiedTime_ >= initFile_.lastModified());
  }

  /**
   * @param _f le format a considerer
   * @param _l le listener
   */
  public TrRubarFileState(final FileFormat _f, final TrRubarFileStateListener _l) {
    fmt_ = _f;
    l_ = _l;
  }

  @Override
  public boolean equals(final Object _obj) {
    if (_obj == null) {
      return false;
    }
    if (_obj == this) {
      return true;
    }
    if (_obj.getClass().equals(getClass())) {
      return fmt_.equals(((TrRubarFileState) _obj).fmt_);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return fmt_.hashCode();
  }

  @Override
  public int compareTo(final TrRubarFileState _o) {
    return fmt_.compareTo(_o.fmt_);
  }

  /**
   * @return Description de l'etat
   */
  public String getDescription() {
    if (isMarkRemoved_) {
      return CtuluLib.getS("A supprimer");
    }
    if (isModified_) {
      return CtuluLib.getS("Modifi�");
    }
    return CtuluLibString.ESPACE;
  }

  /**
   * @return le format attache
   */
  public final FileFormat getFmt() {
    return fmt_;
  }

  /**
   * @return true si ce fichier devra etre efface
   */
  public boolean isMarkRemoved() {
    return isMarkRemoved_;
  }

  /**
   * @return true si les donnees de ce fichier ont ete modifiees
   */
  public boolean isModified() {
    return isModified_;
  }

  protected CtuluIOOperationSynthese loadIfNeeded(final File _initFile, final ProgressionInterface _prog) {
    // le fichier est a jour : inutile
    if ((initFile_ != null) && (!initFile_.equals(_initFile))) {
      FuLog.error(new Throwable(initFile_.getName() + " loaded " + _initFile.getName()));
      return null;
    }
    if (initFile_ == null) {
      initFile_ = _initFile;
    }
    if (isLoadedFileUpToDate()) {
      CtuluLibMessage.info("RELOAD VF2M: " + _initFile.getName() + " uptodate");

      return null;
    }
    CtuluLibMessage.info("RELOAD VF2M: " + _initFile.getName() + " will be reloaded");
    lastModifiedTime_ = _initFile.lastModified();
    return fmt_.getLastVersionInstance(_initFile).read(_initFile, _prog);
  }

  /**
   * @param _isMarkRemoved true si doit etre enleve
   */
  public void setMarkRemoved(final boolean _isMarkRemoved) {
    if (isMarkRemoved_ != _isMarkRemoved) {
      isMarkRemoved_ = _isMarkRemoved;
      l_.fileStateChanged(this);
    }
  }

  /**
   * @param _isModified true si modifie
   */
  public void setModified(final boolean _isModified) {
    if (isModified_ != _isModified) {
      isModified_ = _isModified;
      l_.fileStateChanged(this);
    }
  }

  public void fileFormatDigitsChanged() {
    //do nothing by default
  }
}
