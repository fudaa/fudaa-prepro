/*
 * @creation 25 avr. 07
 * 
 * @modification $Date: 2007-04-30 14:22:44 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.export;

import java.util.ArrayList;
import java.util.List;
import javax.swing.ListSelectionModel;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.fudaa.meshviewer.MvSelectionNodeOrEltData;
import org.fudaa.fudaa.meshviewer.export.MvExportOperationItem;
import org.fudaa.fudaa.meshviewer.export.MvExportPanelFilter;
import org.fudaa.fudaa.meshviewer.export.MvExportPanelFilterDefault;
import org.fudaa.fudaa.meshviewer.export.MvExportTaskSkeleton;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrVisuPanel;

/**
 * @author fred deniger
 * @version $Id: TrExportPanelFilterFactory.java,v 1.2 2007-04-30 14:22:44 deniger Exp $
 */
public final class TrExportPanelFilterFactory {
  private TrExportPanelFilterFactory() {}

  public static MvExportPanelFilter buildFilter(final CtuluUI ui, EfGridInterface grid,
      final MvSelectionNodeOrEltData _selection, final ListSelectionModel _timeSelection, final boolean _hauteurFilter,
      TrVisuPanel visu) {
    final List<MvExportOperationItem> item = new ArrayList<MvExportOperationItem>();
    // item[0] = new MvExportPanelFilterDefault.ExportFilterItemNone();
    boolean isSelection = _selection != null && _selection.idxSelected_ != null;
    if (isSelection) {
      item.add(new MvExportPanelFilterDefault.ExportFilterItemSelection(_selection));
    }
    if (_hauteurFilter) {
      item.add(new TrExportFilterItemHauteurTime(_timeSelection));
    }
    item.add(TrExportOperationMeshesInClosedLine.build(TrResource
        .getS("Exporter uniquement les �l�ments appartenant � un polygone"), ui, grid, visu));
    item.add(new TrExportOperationRefineItem(TrResource.getS("Rafiner tout le maillage et exporter tout le maillage")));
    item.add(new TrExportOperationRefineInClosedLine(TrResource
        .getS("Rafiner les �l�ments appartenant� un polygone et exporter tout le maillage"), visu));
    if (isSelection) {
      item.add(new TrExportOperationRefineItem(TrResource
          .getS("Rafiner uniquement les �l�ments s�lectionn�s et exporter tout le maillage"),
          new MvExportPanelFilterDefault.ExportFilterItemSelection(_selection)));
    }
    if (_hauteurFilter) {
      TrExportFilterItemHauteurTime hauteurTime = new TrExportFilterItemHauteurTime(_timeSelection);
      hauteurTime.setTextForStrict(TrResource.getS("Raffiner uniquement les �l�ments compl�tement mouill�s"));
      TrExportOperationRefineItem refineMouille = new TrExportOperationRefineItem(TrResource
          .getS("Rafiner uniquement les �l�ments mouill�s et exporter tout le maillage"), hauteurTime);
      item.add(refineMouille);
    }
    TrExportOperationProjectItem project = TrExportOperationProjectItem.build(TrResource
        .getS("Extraire le maillage et projeter sur le polygone"), ui, grid, visu);
    item.add(project);
    final MvExportPanelFilterDefault panel = new MvExportPanelFilterDefault((MvExportOperationItem[]) item
        .toArray(new MvExportOperationItem[item.size()]));
    panel.setBorder(MvExportTaskSkeleton.getFilterBorder());
    return panel;
  }

  public static MvExportPanelFilter buildFilterForEachTimeStep(final MvSelectionNodeOrEltData _selection,
      final EfGridData _data) {
    final List<MvExportOperationItem> item = new ArrayList<MvExportOperationItem>();
    // item[0] = new MvExportPanelFilterDefault.ExportFilterItemNone();
    if (_selection != null && _selection.idxSelected_ != null) {
      item.add(new MvExportPanelFilterDefault.ExportFilterItemSelection(_selection));
    }
    item.add(new TrExportFilterItemHauteur(_data));
    final MvExportPanelFilterDefault panel = new MvExportPanelFilterDefault((MvExportOperationItem[]) item
        .toArray(new MvExportOperationItem[item.size()]));
    panel.setBorder(MvExportTaskSkeleton.getFilterBorder());
    return panel;

  }

}
