/*
 * @creation 4 oct. 2004
 *
 * @modification $Date: 2007-05-04 14:01:53 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEditAbstract;
import org.fudaa.ctulu.collection.CtuluContainerModelDouble;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluValuesEditorPanel;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.h2d.H2dParallelogrammeData;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.rubar.H2dRubarDonneesBrutes.OrganizationType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarDonneesBrutesLayerGroup.java,v 1.33 2007-05-04 14:01:53 deniger Exp $
 */
public final class TrRubarDonneesBrutesLayerGroup extends BGroupeCalque implements H2dRubarDonneesBrutesListener {
  protected EbliActionInterface[] getNuageActions(final TrRubarDonneesBrutesNuageLayer _layer) {
    final ArrayList acts = new ArrayList();
    acts.add(new EditVarAction(panel_, _layer, _layer.getBr()));
    acts.add(panel_.getEditor().getActionDelete());
    acts.add(null);
    acts.add(panel_.getEditor().getExportAction());
    acts.add(panel_.getEditor().getImportAction());
    if (_layer.getBr().getID() == H2dRubarDonneesBrutesMng.INI_ID) {
      acts.add(null);
      acts.add(new ActiveCoteDeau(_layer, true, mng_, panel_.getCmdMng()));
      acts.add(new ActiveCoteDeau(_layer, false, mng_, panel_.getCmdMng()));
    }
    return (EbliActionInterface[]) acts.toArray(new EbliActionInterface[acts.size()]);
  }

  private static class ActiveCoteDeau extends EbliActionSimple {
    final ZCalqueAffichageDonnees layer_;
    final boolean cote_;
    final CtuluCommandManager cmd_;
    final H2dRubarDonneesBrutesMng rubMng_;

    protected ActiveCoteDeau(final ZCalqueAffichageDonnees _layer, final boolean _cote, final H2dRubarDonneesBrutesMng _rubMng,
                             final CtuluCommandManager _cmd) {
      super(_cote ? TrResource.getS("Utiliser la cote d'eau") : TrResource.getS("Utiliser la hauteur d'eau"), null, null);
      cote_ = _cote;
      layer_ = _layer;
      cmd_ = _cmd;
      rubMng_ = _rubMng;
    }

    @Override
    public void actionPerformed(final ActionEvent _evt) {
      if (layer_.isSelectionEmpty()) {
        return;
      }
      final H2dRubarDonneesBrutesIni ini = (H2dRubarDonneesBrutesIni) rubMng_.get(H2dRubarDonneesBrutesMng.INI_ID);
      final OrganizationType org = ini.getTypeOrganisation();
      final int coteIdx = ini.getCoteIdx();
      final int hauteurIdx = ini.getHauteurIdx();
      if (org == H2dRubarDonneesBrutes.GRILLE) {
        final H2dRubarDonneesBrutes.GridData grid = ini.getGrid();
        doInvert(grid.getDoubleValues(coteIdx), grid.getDoubleValues(hauteurIdx), null);
      } else if (org == H2dRubarDonneesBrutes.PARALL) {
        final H2dRubarDonneesBrutes.ParallelMng paral = (H2dRubarDonneesBrutes.ParallelMng) ini.getParall();
        final int[] selectedId = layer_.getSelectedIndex();
        final CtuluCommandComposite cmp = new CtuluCommandComposite();
        final int nb = selectedId.length;
        for (int i = 0; i < nb; i++) {
          final int idx = selectedId[i];
          final H2dParallelogrammeData d = paral.getParall(idx);
          final CtuluCollectionDoubleEdit cote = d.getDoubleData(coteIdx);
          final CtuluCollectionDoubleEdit h = d.getDoubleData(hauteurIdx);
          doInvert(cote, h, cmp);
        }
        cmd_.addCmd(cmp.getSimplify());
      } else {
        final H2dRubarDonneesBrutes.NuagePoint pt = ini.getNuage();
        doInvert(pt.getDoubleValues(coteIdx), pt.getDoubleValues(hauteurIdx), null);
      }
    }

    protected void doInvert(final CtuluCollectionDoubleEdit _cote, final CtuluCollectionDoubleEdit _h, final CtuluCommandComposite _cmp) {
      final H2dRubarDonneesBrutesIni ini = (H2dRubarDonneesBrutesIni) rubMng_.get(H2dRubarDonneesBrutesMng.INI_ID);
      final CtuluCollectionDouble bathy = ini.getBathy();
      final int[] selectedId = layer_.getSelectedIndex();
      final CtuluCommandComposite cmp = _cmp == null ? new CtuluCommandComposite() : _cmp;
      final int nb = selectedId.length;
      final double[] newValues = new double[nb];
      final double inderteminedValue = ini.getInderteminedValue();
      if (cote_) {
        for (int i = 0; i < nb; i++) {
          final int idx = selectedId[i];
          newValues[i] = bathy.getValue(idx) + _h.getValue(idx);
          if (newValues[i] > inderteminedValue) {
            newValues[i] = bathy.getValue(idx);
          }
        }
        _cote.set(selectedId, newValues, cmp);
        _h.set(selectedId, inderteminedValue, cmp);
      } else {
        for (int i = 0; i < nb; i++) {
          final int idx = selectedId[i];
          newValues[i] = _cote.getValue(idx) - bathy.getValue(idx);
          if (newValues[i] < 0) {
            newValues[i] = 0;
          }
        }
        _cote.set(selectedId, inderteminedValue, cmp);
        _h.set(selectedId, newValues, cmp);
      }
      if (_cmp == null) {
        cmd_.addCmd(cmp.getSimplify());
      }
    }

    public void doParall() {
    }

    @Override
    public void updateStateBeforeShow() {
      setEnabled(!layer_.isSelectionEmpty());
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: TrRubarDonneesBrutesLayerGroup.java,v 1.33 2007-05-04 14:01:53 deniger Exp $
   */
  private static class EditVarAllGrilleAction extends EditVarAction {
    /**
     * @param _layer
     * @param _br
     */
    public EditVarAllGrilleAction(final TrRubarVisuPanel _pn, final ZCalqueAffichageDonnees _layer, final H2dRubarDonneesBrutes _br) {
      super(_pn, _layer, _br);
      final String s = EbliLib.getS("Editer tous les points");
      setDefaultToolTip(s);
      putValue(Action.NAME, s);
    }

    @Override
    public void updateStateBeforeShow() {
      super.setEnabled(!br_.isOrganisationEmpty());
    }

    @Override
    public String getEnableCondition() {
      return EbliLib.getS("Définir la grille");
    }

    @Override
    public void actionPerformed(final ActionEvent _arg) {
      final int nb = br_.getGrid().getNbPoint();
      if (nb > 0) {
        final int[] idx = new int[nb];
        for (int i = nb - 1; i >= 0; i--) {
          idx[i] = i;
        }
        edit(idx);
      } else {
        pn_.getImpl().message(EbliLib.getS("Aucune donnée à éditer"));
      }
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: TrRubarDonneesBrutesLayerGroup.java,v 1.33 2007-05-04 14:01:53 deniger Exp $
   */
  private static class EditVarAction extends EbliActionSimple {
    ZCalqueAffichageDonnees layer_;
    H2dRubarDonneesBrutes br_;
    final TrRubarVisuPanel pn_;
    boolean isNuage_;

    protected EditVarAction(final TrRubarVisuPanel _pn, final ZCalqueAffichageDonnees _layer, final H2dRubarDonneesBrutes _br) {
      super(EbliLib.getS("Editer les points"), null, "EDIT_VARS");
      layer_ = _layer;
      pn_ = _pn;
      if (layer_ instanceof TrRubarDonneesBrutesNuageLayer) {
        isNuage_ = true;
      }
      setDefaultToolTip(EbliLib.getS("Editer les points sélectionnés"));
      br_ = _br;
    }

    @Override
    public void updateStateBeforeShow() {
      super.setEnabled(!layer_.isSelectionEmpty());
    }

    @Override
    public String getEnableCondition() {
      return EbliLib.getS("Sélectionner au moins un point");
    }

    protected void edit(final int[] _idx) {
      final int nbMax = 2 + br_.getNbVariable();
      final List names = new ArrayList(nbMax);
      final List editeurs = new ArrayList(nbMax);
      final List models = new ArrayList(nbMax);
      CtuluContainerModelDouble data = br_.getGrid();
      if (isNuage_) {
        data = br_.getNuage();
        names.add("X");
        final CtuluNumberFormater fmt = br_.getFmt().getXYDb();
        final CtuluValueEditorDouble ed = new CtuluValueEditorDouble();
        ed.setFormatter(fmt.getNumberFormat());
        ed.setVal(fmt.getValueValidator());
        editeurs.add(ed);
        models.add(br_.getNuage().createEditForX());
        names.add("Y");
        editeurs.add(ed);
        models.add(br_.getNuage().createEditForY());
      } else if (br_.getGrid() != null) {
        data = br_.getGrid();
        names.add("X");
        final CtuluNumberFormater fmt = br_.getFmt().getXYDb();
        final CtuluValueEditorDouble ed = new CtuluValueEditorDouble();
        ed.setEditable(false);
        ed.setFormatter(fmt.getNumberFormat());
        editeurs.add(ed);
        CtuluCollectionDoubleEditAbstract modelXY = new CtuluCollectionDoubleEditAbstract() {
          @Override
          public double getValue(final int _i) {
            return br_.getGrid().getX(_i);
          }

          @Override
          public int getSize() {
            return br_.getGrid().getNbPoint();
          }

          @Override
          protected void internalSetValue(final int _i, final double _newV) {
          }
        };
        models.add(modelXY);
        names.add("Y");
        editeurs.add(ed);
        modelXY = new CtuluCollectionDoubleEditAbstract() {
          @Override
          public double getValue(final int _i) {
            return br_.getGrid().getY(_i);
          }

          @Override
          public int getSize() {
            return br_.getGrid().getNbPoint();
          }

          @Override
          protected void internalSetValue(final int _i, final double _newV) {
          }
        };
        models.add(modelXY);
      }
      int variableBlockLength = br_.getVariableBlockLength();

      int idxBlock = 1;
      int idxTransport = 0;
      Class blockClass = null;
      if (variableBlockLength >= br_.getNbVariable()) {
        variableBlockLength = 0;
      }
      String prefix = TrLib.getString("Block") + " ";
      if (variableBlockLength > 0) {
        blockClass = br_.getVariableId(br_.getNbVariable() - 1).getClass();
      }

      for (int i = 0; i < br_.getNbVariable(); i++) {
        final Object variableId = br_.getVariableId(i);
        models.add(data.getDoubleValues(i));
        String name = variableId.toString();
        if (variableId == H2dVariableType.DEBIT_X) {
          name = name + " / " + H2dVariableType.VITESSE_U;
        } else if (variableId == H2dVariableType.DEBIT_Y) {
          name = name + " / " + H2dVariableType.VITESSE_V;
        }
        names.add(name);
        CtuluValueEditorDouble ed = null;
        final CtuluNumberFormater fmt = br_.getFmt().getFormatterFor((H2dVariableType) variableId);
        if (br_.isVariableActive(i)) {
          ed = new CtuluValueEditorDouble();
          ed.setVal(fmt.getValueValidator());
        } else {
          ed = new CtuluValueEditorDouble();
          ed.setEditable(false);
        }
        if (i == 0) {
          ed.setSeparator(TrLib.getString("Valeurs"));
        }
        if (variableBlockLength > 0) {
          if (blockClass.equals(variableId.getClass()) || blockClass.isInstance(variableId)) {
            if (idxTransport % variableBlockLength == 0) {
              ed.setSeparator(prefix + idxBlock);
              idxBlock++;
            }
            idxTransport++;
          }
        }

        ed.setFormatter(fmt.getNumberFormat());
        editeurs.add(ed);
      }
      final CtuluValuesEditorPanel panel = new CtuluValuesEditorPanel((String[]) names.toArray(new String[names.size()]), _idx,
          (CtuluValueEditorI[]) editeurs.toArray(new CtuluValueEditorI[editeurs.size()]),
          (CtuluCollectionDoubleEdit[]) models.toArray(new CtuluCollectionDoubleEdit[models.size()]), pn_.getCmdMng());
      String suffixTitle = "";
      if (layer_.isOnlyOneObjectSelected()) {
        suffixTitle = "[" + CtuluLibString.getEspaceString(layer_.getSelectedIndex()[0] + 1) + "]";
      }
      panel.afficheModale(pn_.getFrame(), layer_.getTitle() + suffixTitle);
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if (layer_.isSelectionEmpty()) {
        pn_.getImpl().message(EbliLib.getS("Sélectionner au moins un point"));
        return;
      }
      final int[] idx = layer_.getSelectedIndex();
      edit(idx);
    }
  }

  private static class FillActionArray implements H2dRubarDonneesBrutesVisitorClient {
    final List l_;
    final TrRubarDonneesBrutesLayer layer_;
    final TrRubarVisuPanel rubPanel_;

    FillActionArray(final TrRubarVisuPanel _panel, final List _l, final TrRubarDonneesBrutesLayer _layer) {
      l_ = _l;
      rubPanel_ = _panel;
      layer_ = _layer;
    }

    @Override
    public void visitGrid(final H2dRubarDonneesBrutes _b) {
      final EbliActionSimple s = new EbliActionSimple(EbliLib.getS("Editer la grille"), null, "GRID_EDIT") {
        @Override
        public void actionPerformed(final ActionEvent _ae) {
          final TrRubarRegularGridEditor ed = new TrRubarRegularGridEditor(_b.getGrid().getGridInterface());
          if (CtuluDialogPanel.isOkResponse(ed.afficheModale(rubPanel_.getImpl().getFrame()))) {
            _b.getGrid().setGrid(ed.getResult(), rubPanel_.getCmdMng());
          }
        }
      };
      l_.add(s);
      l_.add(new EditVarAction(rubPanel_, layer_, layer_.br_));
      l_.add(new EditVarAllGrilleAction(rubPanel_, layer_, layer_.br_));
    }

    @Override
    public void visitParall(final H2dRubarDonneesBrutes _b) {

      EbliActionSimple ac = new EbliActionSimple(TrResource.getS("Editer parallèlogramme"), null, "EDIT_PARALL") {
        @Override
        public void actionPerformed(final ActionEvent _e) {
          if (layer_.isOnlyOneObjectSelected()) {
            editParallel(rubPanel_, layer_.getSelectedIndex()[0], layer_);
          } else {
            rubPanel_.getImpl().message(TrResource.getS("Sélectionner un seul parallèlogramme"));
          }
        }

        @Override
        public String getEnableCondition() {
          return TrResource.getS("Sélectionner un seul parallèlogramme");
        }

        @Override
        public void updateStateBeforeShow() {
          super.setEnabled(layer_.isOnlyOneObjectSelected());
        }
      };
      ac.setDefaultToolTip(TrResource.getS("Editer le parallèlogramme sélectionné"));
      l_.add(ac);
      l_.add(new EbliActionSimple(TrResource.getS("Ajouter"), null, "ADD_PARALL") {
        @Override
        public void actionPerformed(final ActionEvent _e) {
          editParallel(rubPanel_, -1, layer_);
        }
      });
      ac = new EbliActionSimple(TrResource.getS("Supprimer"), null, "REMOVE_PARALL") {
        @Override
        public String getEnableCondition() {
          return TrResource.getS("Sélectionner au moins un parallèlogramme");
        }

        @Override
        public void updateStateBeforeShow() {
          super.setEnabled(!layer_.isSelectionEmpty());
        }

        @Override
        public void actionPerformed(final ActionEvent _e) {
          if (layer_.isSelectionEmpty()) {
            rubPanel_.getImpl().message(TrResource.getS("Sélectionner au moins un parallèlogramme"));
          } else {
            layer_.br_.getParall().removeParallel(layer_.getSelectedIndex(), rubPanel_.getCmdMng());
            layer_.clearSelection();
          }
        }
      };
      ac.setDefaultToolTip(TrResource.getS("Enlever les parallèlogrammes sélectionnés"));
      l_.add(ac);
    }
  }

  protected static void editParallel(final TrRubarVisuPanel _panel, final int _i, final TrRubarDonneesBrutesLayer _l) {

    final BuTable table = new CtuluTable();
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    H2dParallelogrammeData d = null;
    if (_i >= 0) {
      d = _l.br_.getParall().getParall(_i);
    }
    final TrRubarParallelogrammeTableModel model = new TrRubarParallelogrammeTableModel(_l.br_, d);
    table.setModel(model);
    final TableColumnModel m = table.getColumnModel();
    final TableCellEditor editor = model.getTableCellEditor();
    final TableCellRenderer renderer = model.getTableCellRenderer();
    for (int i = m.getColumnCount() - 1; i >= 0; i--) {
      m.getColumn(i).setCellEditor(editor);
      m.getColumn(i).setCellRenderer(renderer);
    }
    final CtuluDialogPanel pn = new CtuluDialogPanel() {
      @Override
      public boolean isDataValid() {
        // pour prendre en compte la valeur editee.
        if (table.getCellEditor() != null) {
          table.getCellEditor().stopCellEditing();
        }
        final boolean r = model.isValid();
        if (!r) {
          setErrorText(TrResource.getS("Des points sont identiques"));
        }
        return r;
      }
    };
    pn.setLayout(new BuBorderLayout());
    CtuluLibSwing.packJTable(table);
    pn.add(new BuScrollPane(table));
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_panel.getImpl().getFrame()))) {
      if (table.getCellEditor() != null) {
        table.getCellEditor().stopCellEditing();
      }
      if (_i < 0) {
        model.createParallel(_l.br_.getParall(), _panel.getCmdMng());
        _l.setSelection(new int[]{_l.br_.getParall().getNbParall() - 1});
      } else {
        model.updateParallel(_panel.getCmdMng());
      }
    }
  }

  class ChangeTypeAction extends EbliActionSimple {
    TrRubarDonneesBrutesLayer layer_;

    public ChangeTypeAction(final TrRubarDonneesBrutesLayer _layer) {
      super(TrResource.getS("Changer le type des données"), null, "DB_CHANGE_TYPE");
      layer_ = _layer;
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {

      final BuComboBox cb = new BuComboBox();
      cb.setModel(H2dRubarDonneesBrutes.TYPE_DONNEES_BRUTES.createComboBoxModel(TrResource.getS("Aucun")));
      final CtuluDialogPanel pn = new CtuluDialogPanel();
      pn.add(cb);
      if (layer_.br_.getTypeOrganisation() == null) {
        cb.setSelectedIndex(0);
      } else {
        cb.setSelectedItem(layer_.br_.getTypeOrganisation());
      }
      if (CtuluDialogPanel.isOkResponse(pn.afficheModale(TrRubarDonneesBrutesLayerGroup.this))) {
        final Object o = cb.getSelectedItem();
        if (o != null) {
          if (cb.getSelectedIndex() == 0) {
            layer_.br_.setTypeOrganisation(null, panel_.getCmdMng());
          } else {
            layer_.br_.setTypeOrganisation((H2dRubarDonneesBrutes.OrganizationType) o, panel_.getCmdMng());
          }
        }
      }
    }
  }

  /**
   * @param _mng le manager de donnees brutes
   * @param _panel le panel contenant ce groupe de calque
   * @return le groupe des donnees brutes.
   */
  public static BGroupeCalque createDonneesBrutesGroup(final H2dRubarDonneesBrutesMng _mng, final TrRubarVisuPanel _panel) {
    final TrRubarDonneesBrutesLayerGroup grLayer = new TrRubarDonneesBrutesLayerGroup(_mng, _panel);
    _mng.addListener(grLayer);
    createLayers(_mng, _panel, grLayer, H2dRubarDonneesBrutesMng.BATY_ID, H2dResource.getS("bathymétrie"));

    // le calque frt
    createLayers(_mng, _panel, grLayer, H2dRubarDonneesBrutesMng.FRT_ID, H2dResource.getS("frottement"));

    // le calque dif
    createLayers(_mng, _panel, grLayer, H2dRubarDonneesBrutesMng.DIF_ID, H2dResource.getS("diffusion"));

    // le calque Ini
    createLayers(_mng, _panel, grLayer, H2dRubarDonneesBrutesMng.INI_ID, H2dResource.getS("conditions initiales"));

    grLayer.setName("gcInitialData");
    grLayer.setTitle(H2dResource.getS("Données brutes"));
    return grLayer;
  }

  private static void createLayers(final H2dRubarDonneesBrutesMng _mng, final TrRubarVisuPanel _panel, final TrRubarDonneesBrutesLayerGroup _grLayer,
                                   final String _id, final String _name) {
    final TrRubarDonneesBrutesLayer c = new TrRubarDonneesBrutesLayer(_mng, _id);
    final String title = (CtuluLib.isFrenchLanguageSelected() ? "DB: " : "UD: ") + _name;
    c.setTitle(title);
    _grLayer.add(c);
    _panel.addCalqueActions(c, _grLayer.getCalqueAction(_id));
    // nuage
    final TrRubarDonneesBrutesNuageLayer cn = new TrRubarDonneesBrutesNuageLayer(_mng, _id, _panel.getEditor());
    cn.setTitle(title + " XY");
    _grLayer.add(cn);
    _panel.addCalqueActions(cn, _grLayer.getNuageActions(cn));
  }

  TrRubarVisuPanel panel_;
  H2dRubarDonneesBrutesMng mng_;

  private TrRubarDonneesBrutesLayerGroup(final H2dRubarDonneesBrutesMng _mng, final TrRubarVisuPanel _pn) {
    mng_ = _mng;
    panel_ = _pn;
    final String buf = TrResource.getS("Permet de paramétrer les données brutes (avant interpolation)") + "<br>&nbsp;&nbsp;"
        + panel_.getImpl().buildLink(TrResource.getS("Comment éditer les données brutes"), "rubar-editor-donnees-brutes");
    putClientProperty(Action.SHORT_DESCRIPTION, buf);
    setVisible(false);
  }

  protected EbliActionInterface[] getCalqueAction(final String _id) {
    final List rtemp = new ArrayList();
    final TrRubarDonneesBrutesLayer layer = (TrRubarDonneesBrutesLayer) getCalqueParNom(TrRubarDonneesBrutesLayer.getLayerName(_id));
    if (layer == null) {
      FuLog.error("FPR: probleme with layers");
    } else if ((layer.br_ != null) && (layer.br_.getTypeOrganisation() != null)) {
      layer.br_.getTypeOrganisation().visit(new FillActionArray(panel_, rtemp, layer), layer.br_);
    }
    if (rtemp.size() > 0) {
      rtemp.add(null);
    }
    if (_id == H2dRubarDonneesBrutesMng.INI_ID) {
      rtemp.add(new ActiveCoteDeau(layer, true, mng_, panel_.getCmdMng()));
      rtemp.add(new ActiveCoteDeau(layer, false, mng_, panel_.getCmdMng()));
    }
    rtemp.add(new ChangeTypeAction(layer));
    return (EbliActionInterface[]) rtemp.toArray(new EbliActionInterface[rtemp.size()]);
  }

  @Override
  public void donneesBrutesDataChanged(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesDataNombreChanged(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesSupportAddedOrRemoved(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesSupportChanged(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesTypeChanged(final H2dRubarDonneesBrutes _source) {
    final TrRubarDonneesBrutesLayer c = (TrRubarDonneesBrutesLayer) getCalqueParNom(TrRubarDonneesBrutesLayer.getLayerName(_source.getID()));
    panel_.addCalqueActions(c, getCalqueAction(_source.getID()));
  }

  @Override
  public void donneesBrutesNuageDataChanged(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesNuageDataNombreChanged(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesNuageSupportAddedOrRemoved(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesNuageSupportChanged(final H2dRubarDonneesBrutes _source) {
  }
}
