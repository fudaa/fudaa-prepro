/*
 * @creation 17 nov. 06
 *
 * @modification $Date: 2007-05-04 14:01:53 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrage;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireApport;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireBreche;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireValuablesInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.fudaa.tr.common.TrLib;
import org.jdesktop.swingx.JXTitledSeparator;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author fred deniger
 * @version $Id: TrRubarOuvrageEditorFactory.java,v 1.5 2007-05-04 14:01:53 deniger Exp $
 */
public final class TrRubarOuvrageEditorFactory {
  private TrRubarOuvrageEditorFactory() {
  }

  protected static boolean editBreche(final Object _o, final Component _f) {
    final H2dRubarOuvrageElementaireBreche b = (H2dRubarOuvrageElementaireBreche) _o;
    final String[] lb = H2dRubarOuvrageElementaireBreche.getValuesTitle();
    final BuValueValidator first = FortranLib.getValidator(10, 2);
    final BuValueValidator last = FortranLib.getValidator(6, 2);
    final BuTextField[] fts = new BuTextField[lb.length];
    final BuTextField ftNbPasDeTemps = BuTextField.createIntegerField();
    ftNbPasDeTemps.setValueValidator(BuValueValidator.MINMAX(0, 9000));
    ftNbPasDeTemps.setText(CtuluLibString.getString(b.getNbPasDeTempsMax()));

    // on met a jour le type surverse/renard
    final BuComboBox cbSurverse = new BuComboBox();
    cbSurverse.setModel(new DefaultComboBoxModel(b.getSurverseChoices()));
    cbSurverse.setSelectedIndex(b.getSelectedSurverseChoice());

    final BuComboBox cbTypeOfBreaching = new BuComboBox();
    cbTypeOfBreaching.setModel(new DefaultComboBoxModel(b.getTypeOfBreaching()));
    cbTypeOfBreaching.setSelectedIndex(b.getBrecheType());
    final CtuluDialogPanel pn = new CtuluDialogPanel() {
      @Override
      public boolean apply() {
        b.setTimeStep(Integer.parseInt(ftNbPasDeTemps.getText()), null);
        b.setSurverseId(H2dRubarOuvrageElementaireBreche.getChoiceId(cbSurverse.getSelectedIndex()), null);
        b.setTypeOfBreaching(cbTypeOfBreaching.getSelectedIndex(), null);
        final double[] val = new double[fts.length];
        for (int i = val.length - 1; i >= 0; i--) {
          val[i] = Double.parseDouble(fts[i].getText());
        }
        b.setAllValues(val, null);
        return true;
      }

      @Override
      public boolean isDataValid() {
        boolean r = true;
        if (ftNbPasDeTemps.getValueValidator().isValueValid(ftNbPasDeTemps.getValue())) {
          ftNbPasDeTemps.setForeground(Color.BLACK);
        } else {
          r = false;
          ftNbPasDeTemps.setForeground(Color.RED);
        }
        for (int i = 0; i < fts.length; i++) {
          if (fts[i].getValueValidator().isValueValid(fts[i].getValue())) {
            fts[i].setForeground(Color.BLACK);
          } else {
            r = false;
            fts[i].setForeground(Color.RED);
          }
        }
        return r;
      }
    };

    pn.setLayout(new BuGridLayout(2, 5, 5));
    pn.addLabel(H2dResource.getS("Type de rupture"));
    pn.add(cbTypeOfBreaching);
    for (int i = 0; i < lb.length; i++) {
      fts[i] = pn.addLabelDoubleText(lb[i]);
      BuValueValidator val = first;
      // les 2 dernieres valeurs ont une validation differentes
      if (i >= lb.length - 2) {
        val = last;
      }
      fts[i].setValueValidator(val);
      fts[i].setValue(CtuluLib.getDouble(b.getValue(i)));
    }

    pn.addLabel(H2dResource.getS("Nombre de pas de temps maximal"));
    pn.add(ftNbPasDeTemps);
    pn.addLabel(H2dResource.getS("Renard") + '/' + H2dResource.getS("Surverse"));
    pn.add(cbSurverse);
    return CtuluDialogPanel.isOkResponse(pn.afficheModale(_f));
  }

  public static boolean editValuablesOuvrages(final H2dRubarOuvrageElementaireValuablesInterface ouvrage, final Component _f) {
    final String[] lb = ouvrage.getValuesName();
    final BuValueValidator first = FortranLib.getValidator(10, 3);
    final BuValueValidator firstPos = FortranLib.getValidator(10, 3, true);
    final BuTextField[] fts = new BuTextField[lb.length];
    final CtuluDialogPanel pn = new CtuluDialogPanel() {
      @Override
      public boolean apply() {
        final double[] val = new double[fts.length];
        for (int i = val.length - 1; i >= 0; i--) {
          val[i] = Double.parseDouble(fts[i].getText());
        }
        ouvrage.setAllValues(val, null);
        return true;
      }

      /**
       * @see org.fudaa.ctulu.gui.CtuluDialogPanel#isDataValid()
       */
      @Override
      public boolean isDataValid() {
        boolean r = true;
        for (int i = 0; i < fts.length; i++) {
          if (fts[i].getValueValidator().isValueValid(fts[i].getValue())) {
            fts[i].setForeground(Color.BLACK);
          } else {
            r = false;
            fts[i].setForeground(Color.RED);
          }
        }
        return r;
      }
    };
    pn.setLayout(new BuGridLayout(2, 5, 5));
    for (int i = 0; i < lb.length; i++) {
      fts[i] = pn.addLabelDoubleText(lb[i]);
      if (i == 0 || i == 3) {
        fts[i].setValueValidator(firstPos);
      } else {
        fts[i].setValueValidator(first);
      }
      fts[i].setValue(CtuluLib.getDouble(ouvrage.getValue(i)));
    }
    return CtuluDialogPanel.isOkResponse(pn.afficheModale(_f));
  }

  protected static boolean editTransfert(final Object _o, final Component _f, TrRubarEvolutionManager evolManager, TrRubarTarageCourbesManager tarageEvolManager) {
    List<H2dVariableType> h2dVariableTypes = H2dRubarOuvrage.getTarageVariables();
    return editOuvrage(_o, _f, evolManager, tarageEvolManager, h2dVariableTypes);
  }

  public static boolean editApport(final Object _o, final Component _f, TrRubarEvolutionManager evolManager, TrRubarTarageCourbesManager tarageEvolManager) {
    List<H2dVariableType> h2dVariableTypes = ((H2dRubarOuvrageElementaireApport) _o).getVariables();
    return editOuvrage(_o, _f, evolManager, tarageEvolManager, h2dVariableTypes);
  }

  public static boolean editOuvrage(final Object _o, final Component _f, TrRubarEvolutionManager evolManager, TrRubarTarageCourbesManager tarageEvolManager,
                                    List<H2dVariableType> h2dVariableTypes) {
    final H2dRubarOuvrageElementaireApport ouvrage = (H2dRubarOuvrageElementaireApport) _o;
    Map<H2dVariableType, EvolutionReguliere> evols = ouvrage.getEvols();

    List<JComboBox> comboBoxes = new ArrayList<JComboBox>();
    List<H2dVariableType> variables = new ArrayList<H2dVariableType>();
    for (H2dVariableType h2dVariableType : h2dVariableTypes) {
      EvolutionReguliere evol = evols.get(h2dVariableType);

      List<EvolutionReguliereInterface> evolsToUsed = new ArrayList<EvolutionReguliereInterface>();
      if (H2dVariableType.LOI_TARAGE.equals(h2dVariableType)) {
        for (int i = 0; i < tarageEvolManager.getNbEvol(); i++) {
          evolsToUsed.add(tarageEvolManager.getEvol(i));
        }
      } else {
        ComboBoxModel initModele = evolManager.createComboBoxModel(h2dVariableType);
        boolean emptyAdded = false;
        for (int i = 0; i < initModele.getSize(); i++) {
          final EvolutionReguliere evolution = (EvolutionReguliere) initModele.getElementAt(i);
          boolean isEmpty = evolution.getNbValues() == 0;
          if (!isEmpty || (!emptyAdded && isEmpty)) {
            evolsToUsed.add(evolution);
            if (isEmpty) {
              emptyAdded = true;
            }
          }
        }
      }

      JComboBox cb = new JComboBox();
      cb.setModel(new DefaultComboBoxModel(evolsToUsed.toArray()));
      if (evol != null) {
        cb.setSelectedItem(evol);
      }
      comboBoxes.add(cb);
      variables.add(h2dVariableType);
    }
    CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuGridLayout(2, 5, 5));
    final int nbVariableNonTransport = H2dRubarOuvrage.getNbVariableNonTransport();
    final int nbVariableTransport = H2dRubarOuvrage.getNbVariableTransport();
    boolean useSeparator = h2dVariableTypes.size() > nbVariableNonTransport;
    for (int i = 0; i < comboBoxes.size(); i++) {
      if (i >= nbVariableNonTransport && ((i - nbVariableNonTransport) % nbVariableTransport) == 0) {
        int block = 1 + ((i - nbVariableNonTransport) / nbVariableTransport);
        final JXTitledSeparator comp = new JXTitledSeparator(TrLib.getString("Block") + CtuluLibString.getEspaceString(block), SwingConstants.LEFT);
        comp.setFont(CtuluLibSwing.getMiniFont());
        pn.add(comp);
        pn.add(new JLabel());
      }
      pn.add(new JLabel(variables.get(i).getName()));
      pn.add(comboBoxes.get(i));
    }
    if (pn.afficheModaleOk(_f, ouvrage.getType().getName())) {
      for (int i = 0; i < comboBoxes.size(); i++) {
        ouvrage.updateEvol(variables.get(i), (EvolutionReguliere) comboBoxes.get(i).getSelectedItem(), null);
      }
      return true;
    }
    return false;
  }
}
