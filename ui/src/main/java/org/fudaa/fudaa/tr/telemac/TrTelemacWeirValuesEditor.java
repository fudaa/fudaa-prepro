/**
 * @creation 7 d�c. 2004
 * @modification $Date: 2006-07-27 13:35:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuilMng;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacWeirValuesEditor.java,v 1.8 2006-07-27 13:35:37 deniger Exp $
 */
public class TrTelemacWeirValuesEditor extends CtuluDialogPanel {

  BuTextField coefDebit_;
  BuTextField cote_;
  H2dTelemacSeuilMng mng_;
  int[] seuildIdx_;
  CtuluCommandContainer cmd_;

  protected TrTelemacWeirValuesEditor(final TrTelemacWeirLayer _l) {
    mng_ = _l.getM().getSeuils();
    seuildIdx_ = _l.getSelection().getIdxSelected();
    cmd_ = _l.pn_.getCmdMng();
    build();
  }

  protected TrTelemacWeirValuesEditor(final TrTelemacWeirLayer _l, final int[] _idx, final CtuluCommandContainer _cmd) {
    mng_ = _l.getM().getSeuils();
    seuildIdx_ = _idx;
    cmd_ = _cmd;
    build();
  }

  protected final void build() {
    if (seuildIdx_ == null || seuildIdx_.length == 0) { return; }
    setLayout(new BuGridLayout(2, 5, 5));
    Double d = mng_.getCoteCommonValue(seuildIdx_);
    addLabel(H2dResource.getS("Cote du seuil"));
    cote_ = addDoubleText(d == null ? CtuluLibString.EMPTY_STRING : d.toString());
    add(cote_);
    d = mng_.getCoefDebitCommonValue(seuildIdx_);
    addLabel(H2dResource.getS("Coefficient de d�bit"));
    coefDebit_ = addDoubleText(d == null ? CtuluLibString.EMPTY_STRING : d.toString());
    coefDebit_.setValueValidator(BuValueValidator.MIN(0));
    add(coefDebit_);
  }

  @Override
  public boolean apply() {
    String txt = coefDebit_.getText().trim();
    if (txt.length() > 0) {
      mng_.setValueForDebit(seuildIdx_, Double.parseDouble(txt), cmd_);
    }
    txt = cote_.getText().trim();
    if (txt.length() > 0) {
      mng_.setValueForCote(seuildIdx_, Double.parseDouble(txt), cmd_);
    }
    return true;
  }

  @Override
  public boolean isDataValid() {
    final String txt = coefDebit_.getText().trim();
    if (txt.length() > 0) {
      final double d = Double.parseDouble(txt);
      if (d < 0) { return false; }
    }
    return true;
  }
}
