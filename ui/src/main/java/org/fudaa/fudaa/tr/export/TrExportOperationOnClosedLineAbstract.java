package org.fudaa.fudaa.tr.export;

import org.locationtech.jts.geom.LineString;
import java.awt.Color;
import javax.swing.JComponent;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.commun.EbliFormatter;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.meshviewer.export.MvExportOperationItem;
import org.fudaa.fudaa.meshviewer.layer.MvFrontierPolygonLayer;
import org.fudaa.fudaa.meshviewer.layer.MvNodeLayer;
import org.fudaa.fudaa.meshviewer.layer.MvVisuPanel;
import org.fudaa.fudaa.meshviewer.model.MvFrontierModelDefault;
import org.fudaa.fudaa.meshviewer.model.MvInfoDelegateAbstract;
import org.fudaa.fudaa.meshviewer.model.MvNodeModelDefault;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrVisuPanel;
import org.fudaa.fudaa.tr.post.profile.MvLineChooser;

/**
 * @author deniger
 */
public abstract class TrExportOperationOnClosedLineAbstract extends MvExportOperationItem {

  String title;
  CtuluUI ui;
  MvVisuPanel panel;
  protected MvLineChooser mvLineChooser;
  EfGridInterface grid;

  public TrExportOperationOnClosedLineAbstract(final String title, final TrVisuPanel panel) {
    this.title = title;
    this.panel = panel;
  }

  public TrExportOperationOnClosedLineAbstract(CtuluUI ui, EfGridInterface grid, final String title) {
    this.title = title;
    this.grid = grid;
    this.ui = ui;
    assert ui != null;
    assert grid != null;
  }

  @Override
  public void close() {
    if (mvLineChooser != null) mvLineChooser.close();
  }

  @Override
  public JComponent getConfigureComponent() {
    if (panel == null) {
      panel = new MvVisuPanel(ui);
      panel.addCqMaillage(grid, new MvInfoDelegateAbstract() {

        @Override
        public EbliFormatterInterface getXYFormatter() {
          return new EbliFormatter();
        }

        @Override
        public EfGridInterface getGrid() {
          return grid;
        }
      });
      mvLineChooser = new MvLineChooser(null, grid, ui, GISLib.MASK_POLYGONE);
    } else {
      final BCalque selectedCalque = panel.getArbreCalqueModel().getSelectedCalque();
      LineString selected = null;
      if (selectedCalque instanceof ZCalqueAffichageDonneesInterface) {
        selected = ((ZCalqueAffichageDonneesInterface) selectedCalque).getSelectedLine();
      }
      mvLineChooser = new MvLineChooser(selected, grid, ui, GISLib.MASK_POLYGONE);
    }
    final MvFrontierPolygonLayer frontier = new MvFrontierPolygonLayer(new MvFrontierModelDefault(panel.getGridGroup()
        .getGrid()));
    // final MvElementLayer element = new MvElementLayer(new MvElementModelDefault(panel.getGridGroup().getGrid()));
    final MvNodeLayer node = new MvNodeLayer(new MvNodeModelDefault(panel.getGridGroup().getGrid()));
    node.setIconModel(0, new TraceIconModel(TraceIcon.CROIX, 2, Color.BLACK));
    mvLineChooser.setCanCreateNewLine(new BCalque[] { frontier, node });
    return mvLineChooser.getPanel(TrResource.getS("Choisir une ligne ferm�e"));
    // return cp;
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public boolean isEnable() {
    return true;
  }

  @Override
  public String getErrorMessage() {
    return mvLineChooser.getSelectedLine() == null ? TrResource.getS("Choisir une ligne ferm�e") : null;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

}
