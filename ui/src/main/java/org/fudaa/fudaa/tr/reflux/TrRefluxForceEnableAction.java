/*
 * @creation 27 avr. 07
 * @modification $Date: 2007-06-14 12:01:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.bu.BuResource;
import java.io.File;
import org.fudaa.ctulu.*;
import org.fudaa.dodico.fortran.FortranDoubleReaderResultInterface;
import org.fudaa.dodico.h2d.reflux.H2dRefluxNodalPropertiesMng;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.reflux.io.RefluxSollicitationFileFormat;
import org.fudaa.ebli.commun.EbliActionChangeState;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author fred deniger
 * @version $Id: TrRefluxForceEnableAction.java,v 1.3 2007-06-14 12:01:39 deniger Exp $
 */
public class TrRefluxForceEnableAction extends EbliActionChangeState {

  final CtuluCommandManager cmd_;
  final H2dRefluxNodalPropertiesMng.ForceItem parameters_;
  final CtuluUI ui_;
  final TrRefluxProjet proj_;

  public TrRefluxForceEnableAction(final TrRefluxProjet _proj, final H2dRefluxNodalPropertiesMng.ForceItem _parameters,
      final CtuluCommandManager _cmd, final CtuluUI _ui) {
    super(TrResource.getS("Utiliser la force:") + CtuluLibString.ESPACE + _parameters.getMainVar().getName(), null,
        "REFLUX_FORCE");
    cmd_ = _cmd;
    proj_ = _proj;
    ui_ = _ui;
    parameters_ = _parameters;
  }

  @Override
  public void updateStateBeforeShow() {
    setSelected(parameters_.isActivated());
  }

  @Override
  public void changeAction() {
    if (parameters_.isActivated()) {
      boolean save = false;
      if (proj_.getState().isParamsModified()) {
        save = ui_.question(parameters_.getMainVar().getName(), TrResource
            .getS("Les donn�es concernant la variable ne seront pas enregistr�es.")
            + CtuluLibString.LINE_SEP_SIMPLE
            + TrResource.getS("Voulez-vous enregistrer vos donn�es avant l'inactivation ?"));
      }
      if (save) {
        new CtuluRunnable(BuResource.BU.getString("Enregistrement"), ui_) {
          @Override
          public boolean run(final ProgressionInterface _proj) {
            proj_.save(_proj);
            parameters_.setUnable(cmd_);
            return true;
          }
        }.run();
      } else {
        parameters_.setUnable(cmd_);
      }
    } else {
      File f = getDestFile();
      if (f != null && !f.exists()) {
        f = null;
      }
      final TrRefluxRadiationVentPanel pn = new TrRefluxRadiationVentPanel(parameters_.getVar(), f);
      if (pn.afficheModaleOk(ui_.getParentComponent())) {
        final File res = pn.getSelectedFile();
        if (res == null) {
          initForce(pn);
        } else {
          final CtuluTaskDelegate task = ui_.createTask(parameters_.getMainVar().getName());
          task.start(new Runnable() {
            @Override
            public void run() {
              readFile(res, task.getStateReceiver(), pn);
            }
          });
        }
      }

    }
  }

  private File getDestFile() {
    final File dest = proj_.getParamsFile();
    if (dest != null) { return CtuluLibFile.changeExtension(dest, proj_.getRefluxParametres().getINPVersion()
        .getForceFileExtension(parameters_.getMainVar(), proj_.getH2dRefluxParametres().getProjetType())); }
    return null;
  }

  protected void readFile(final File _f, final ProgressionInterface _prog, final TrRefluxRadiationVentPanel _pn) {
    final RefluxSollicitationFileFormat fmt = new RefluxSollicitationFileFormat(parameters_.getNbVar(),
        CtuluLibString.EMPTY_STRING);
    final CtuluIOOperationSynthese op = fmt.read(_f, _prog);
    boolean ok = false;
    if (!ui_.manageErrorOperationAndIsFatal(op)) {
      final FortranDoubleReaderResultInterface res = (FortranDoubleReaderResultInterface) op.getSource();
      final CtuluAnalyze analyze = new CtuluAnalyze();
      if (parameters_.setEnable(res, analyze, cmd_)) {
        ok = true;
      }

    }

    if (!ok) {
      ui_.message(parameters_.getMainVar().getName(), H2dResource
          .getS("Des valeurs par d�faut seront utilis�es pour initialiser les donn�es"), false);
      initForce(_pn);
    }

  }

  protected void initForce(final TrRefluxRadiationVentPanel _pn) {
    parameters_.setEnable(_pn.getValues(), cmd_);
  }
}
