/**
 * 
 */
package org.fudaa.fudaa.tr.rubar;

import java.io.File;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.dodico.h2d.rubar.H2dRubarBcListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryFlowrateGroupType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.rubar.io.RubarDURFileFormat;
import org.fudaa.dodico.rubar.io.RubarDURResult;

/**
 * @author CANEL Christophe (Genesis)
 */
//TODO Voir comment d�t�cter les modifications.
public class TrRubarFileStateDUR extends TrRubarFileState implements H2dRubarBcListener {

  final H2dRubarParameters parameter;

  //TODO Voir quoi mettre comme listener.
  public TrRubarFileStateDUR(TrRubarFileStateListener _l, final H2dRubarParameters _p) {
    super(new RubarDURFileFormat(), _l);
    this.parameter = _p;
    _p.getBcMng().addListener(this);
    projectTypeChanged();
    setModified(false);
  }

  @Override
  public void bathyChanged() {

  }

  @Override
  public void nodeInGridChanged() {

  }

  @Override
  public void fondDurChanged() {
    setModified(true);
  }

  @Override
  public void numberOfConcentrationChanged() {

  }

  @Override
  public void projectTypeChanged() {
    if (parameter.getProjetType().equals(H2dRubarProjetType.COURANTOLOGIE_2D)) {
      ((TrRubarFileStateMng) super.l_).remove(this);
    } else {
      ((TrRubarFileStateMng) super.l_).add(this);
      setModified(true);
    }

  }

  @Override
  public void timeClChanged() {
  }

  @Override
  public void areteTypeChanged() {
  }

  @Override
  public void flowrateGroupChanged(H2dRubarBoundaryFlowrateGroupType _t) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CtuluIOOperationSynthese save(File _dir, String _projectName, ProgressionInterface _progression, TrRubarProject _projet) {
    //TODO L'objet a pass� doit �tre un RubarDURResult.
    final File f = fmt_.getFileFor(_dir, _projectName);
    CtuluCollectionDouble values = _projet.getH2dRubarParameters().getNodalData().getValues(H2dVariableTransType.FOND_INERODABLE);
    return ((FileFormatUnique) fmt_).write(f, new RubarDURResult(values), _progression);
  }
}
