/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.fudaa.tr.post.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.calque.action.CalqueActionTable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;
import org.fudaa.fudaa.sig.layer.FSigVisuPanelController;

/**
 * Un ActionController spécifique qui permet de preciser les palettes qui doivent rester en tant que JDialog et celle qui seront
 * dockes dans la frame Flexdock.
 *
 * @author deniger
 */
public class TrPostActionController extends FSigVisuPanelController {

  /**
   * Contient chaque palette enlevee sous la forme l'actionCommand-> l'action
   */
  Map<String, EbliActionPaletteAbstract> mapTabPalettes_ = new HashMap<String, EbliActionPaletteAbstract>(2);
  Collection<EbliActionPaletteAbstract> tabPalettes_ = null;
  Collection<String> useAsPalette = Arrays.asList("CONFIGURE", "CHANGE_REFERENCE", "GLOBAL_MOVE", "GLOBAL_ROTATE",
                                                  "PALETTE_DISTANCE");
  Collection<String> useAsTab = Arrays.asList("INFOS", "PALETTE_EDTION");

  /**
   * @param _ui l'ui
   */
  public TrPostActionController(final CtuluUI _ui) {
    super(_ui);
  }

  @Override
  protected void buildButtonGroupNavigation() {
    if (navigationActionGroup_ != null) {
      return;
    }
    super.buildButtonGroupNavigation();
    final EbliActionInterface[] actions = navigationActionGroup_;
    navigationActionGroup_ = customizePaletteActions(actions);
  }

  @Override
  protected void buildButtonGroupSelection() {
    // pas de palette dans la selection
    super.buildButtonGroupSelection();
  }

  @Override
  protected void buildButtonGroupStandard() {
    if (standardActionGroup_ != null) {
      return;
    }
    super.buildButtonGroupStandard();
    standardActionGroup_ = customizePaletteActions(standardActionGroup_);
  }

  protected void buildButtonGroupSpecifiques() {
    if (standardActionGroup_ != null) {
      return;
    }
    super.buildButtonGroupStandard();
    standardActionGroup_ = customizePaletteActions(standardActionGroup_);

  }

  /**
   * recupere les palettes et initialise les actions
   */
  @Override
  public Collection<EbliActionPaletteAbstract> getTabPaletteAction() {
    // si deja fait on retourne de suite
    if (tabPalettes_ != null) {
      return tabPalettes_;
    }
    initSpecificActions();
    tabPalettes_ = new ArrayList<EbliActionPaletteAbstract>(3);
    for (final String str : useAsTab) {
      final EbliActionPaletteAbstract o = mapTabPalettes_.get(str);
      if (o != null) {
        tabPalettes_.add(o);
      }
    }
    return tabPalettes_;
  }

  @Override
  public FSigVisuPanel getVisuPanel() {
    return super.getVisuPanel();
  }

  @Override
  public final void setInfoPaletteActive() {
    JComponent paletteContent = mapTabPalettes_.get("INFOS").getPaletteContent();
    if (paletteContent != null) {
      JTabbedPane parent = (JTabbedPane) SwingUtilities.getAncestorOfClass(JTabbedPane.class, paletteContent);
      if (parent != null) {
        parent.setSelectedComponent(paletteContent);
      }
    }

  }

  @Override
  protected EbliActionInterface[] getApplicationActions() {
    return customizePaletteActions(pn_.getApplicationActions());
  }
  private List<EbliActionPaletteAbstract> toHide = new ArrayList<EbliActionPaletteAbstract>();

  /**
   * @param actions les actions a trier
   * @return la listes des actions a ajouter dans les menus et autres. Les autres ( les palettes) seront visible tout le temps
   * dans des dock.
   */
  private EbliActionInterface[] customizePaletteActions(final EbliActionInterface[] actions) {

    final List<EbliActionInterface> acts = new ArrayList<EbliActionInterface>(actions.length);

    for (int i = 0; i < actions.length; i++) {
      final EbliActionInterface action = actions[i];
      if (action == null) {
        continue;
      }
      
      if(action instanceof CalqueActionTable) {
    	  CalqueActionTable actionCalque = (CalqueActionTable)action;
    	  TrPostActionCalqueTable overideCalqueAction = new TrPostActionCalqueTable(actionCalque.getModel(),actionCalque.getUi(),actionCalque.getCalque());
    	  acts.add(overideCalqueAction);
          TrPostActionChooseCalqueSelectionTable actionCalqueInterpolation = new 
          TrPostActionChooseCalqueSelectionTable(actionCalque.getModel(),actionCalque.getUi(),actionCalque.getCalque()) ;
          acts.add(actionCalqueInterpolation);
    	  continue;
      }
      
      final String value = (String) action.getValue(Action.ACTION_COMMAND_KEY);
      final boolean isPalette = action instanceof EbliActionPaletteAbstract;
      if ("GLOBAL_MOVE".equals(value) || "GLOBAL_ROTATE".equals(value)) {
        if (toHide.add((EbliActionPaletteAbstract) action)) ;
        ((EbliActionPaletteAbstract) action).setDialogIsModal(false);
        action.updateStateBeforeShow();
      }
      if (!isPalette || useAsPalette.contains(value)) {
        acts.add(action);
        if (isPalette) {
          ((EbliActionPaletteAbstract) action).setDialogIsModal(false);
        }
      }
      if (isPalette && useAsTab.contains(value)) {
        mapTabPalettes_.put(value, (EbliActionPaletteAbstract) action);
        ((EbliActionPaletteAbstract) action).setUsedAsTab(true);
      }
    }
    return acts.toArray(new EbliActionInterface[acts.size()]);
  }

  public void editStop() {
    for (EbliActionPaletteAbstract action : toHide) {
      action.setSelected(false);

    }

  }
}
