/*
 * @creation 19 janv. 2005
 *
 * @modification $Date: 2007-05-04 14:01:51 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuScrollPane;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import gnu.trove.*;
import org.apache.commons.lang.StringUtils;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.rubar.io.*;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetControllerCalque;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetCreatorVueCalque;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostSourceRubarMaxContainer.ElementContent;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreatedConstant;
import org.fudaa.fudaa.tr.post.data.TrPostDataVecteur;
import org.fudaa.fudaa.tr.post.data.TrPostKey;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: TrPostRubarLoader.java,v 1.37 2007-05-04 14:01:51 deniger Exp $
 */
public class TrPostRubarLoader {
  public static String getDesc(final File _f) {
    if (_f == null) {
      return FudaaLib.getS("Lecture");
    }
    return FudaaLib.getS("Lecture") + CtuluLibString.ESPACE + _f.getName();
  }

  FileFormat[] availFmt_;
  File dir_;
  TDoubleObjectHashMap envResult_;
  TIntIntHashMap exposeTimeInitTime_;
  boolean[] fileToLoad_;
  final boolean first_;
  Map fmtLoadable_;
  FileFormat[] fmtToLoad_;
  RubarFileFormatDefault ftENV_;
  RubarFRTFileFormat ftFrt_;
  RubarFileFormatDefault ftHYC_;
  RubarFileFormatDefault ftOUT_;
  RubarFileFormatDefault ftMAS_;
  RubarFileFormatDefault ftTPC_;
  RubarFileFormatDefault ftTPS_;
  RubarFileFormatDefault ftTRC_;
  RubarFileFormatDefault ftZFN_;
  RubarFileFormatDefault ftSED_;
  RubarFileFormatDefault ftNUA_;
  RubarFileFormatDefault ftDZF_;
  TIntHashSet indexOfTPCFileCopied = new TIntHashSet();
  boolean isTpsCopied_;
  boolean isWarnTrucFileDone_;
  boolean isZfnCopied_;
  boolean mustCalculateCoteEauMax_;
  TrPostProjet proj_;
  TrPostSourceRubar src_;
  // les pas de temps
  RubarTimeIndexResolver timeIndexResolver_ = new RubarTimeIndexResolver();
  double[] timeStepAll_;
  final Map<H2dVariableType, TrPostKey> varInitIDx_ = new HashMap<>();

  public TrPostRubarLoader(final TrPostProjet _r, final TrPostSource _src, final File _selectedFile, final boolean _first) {
    this((TrPostSourceRubar) _src, _selectedFile, _first);
    proj_ = _r;
  }

  /**
   * @param _r le projet a charger
   */
  public TrPostRubarLoader(final TrPostSourceRubar _r, final File _selectedFile, final boolean _first) {
    first_ = _first;
    if (_r == null) {
      return;
    }

    ftTPC_ = new RubarFileFormatDefault("TPC");
    ftTPS_ = new RubarFileFormatDefault("TPS");
    ftENV_ = new RubarFileFormatDefault("ENV");
    ftTRC_ = new RubarFileFormatDefault("TRC");
    ftHYC_ = new RubarFileFormatDefault("HYC");
    ftZFN_ = new RubarFileFormatDefault("ZFN");
    ftOUT_ = new RubarFileFormatDefault("OUT");
    ftMAS_ = new RubarFileFormatDefault("MAS");
    ftSED_ = new RubarFileFormatDefault("SED");
    ftNUA_ = new RubarFileFormatDefault("NUA");
    ftDZF_ = new RubarFileFormatDefault("DZF");
    ftFrt_ = RubarFRTFileFormat.getInstance();
    src_ = _r;
    exposeTimeInitTime_ = src_.exposeTimeInitTime_;
    final File saveFile = src_.getMainFile();
    dir_ = saveFile.getParentFile();
    availFmt_ = new FileFormat[]{ftTPS_, ftTPC_, ftENV_, ftTRC_, ftHYC_, ftOUT_, ftMAS_, ftZFN_, ftFrt_, ftSED_, ftNUA_, ftDZF_};
    timeIndexResolver_.setTimeStep(src_.getTime().getInitTimeSteps());
    envResult_ = src_.initEnvResultat_;
    buildFmtLoadable(CtuluLibFile.getSansExtension(saveFile.getName()), _selectedFile);
  }

  private void correctFile(final RubarSolutionReader _reader, final File _f, final CtuluUI _impl) {
    if (_f == null) {
      return;
    }
    _impl.error(
      CtuluUIAbstract.getDefaultErrorTitle(),
      TrResource.getS("Il semble que le fichier {0} soit corrompu. Le post-processeur va essayer de le corriger" + CtuluLibString.LINE_SEP
        + TrResource.getS("L'ancien fichier {0} sera sauvegarder avec un extension .old"), _f.getName()),
      false);
    final File old = new File(_f.getAbsolutePath() + ".old");
    boolean copied = CtuluLibFile.copyFile(_f, old);
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FTR: the file " + _f.getName() + " is copied " + copied);
    }
    if (copied) {
      final RubarSolutionCorrection correction = new RubarSolutionCorrection();
      try {
        correction.setOutFile(_f);
      } catch (final IOException _evt) {
        FuLog.error(_evt);
        copied = false;
      }
      correction.setNbElt(_reader.getNbElt());
      correction.setNbValues(_reader.getNbValues());
      correction.setNbValueByLigne((int) _reader.getNbValueByLigne());
      correction.read(old, _reader.getProg());
    }
    if (!copied) {
      _impl.warn(TrPostRubarLoader.getDesc(_f), TrResource.getS("Le fichier n'a pas pu �tre corrig�"), false);
    }
  }

  /**
   * @param _initTimeIntPos
   * @param _tAjoutMax
   */
  private void envAjouteMax(final TDoubleIntHashMap _initTimeIntPos, final TDoubleObjectHashMap _tAjoutMax) {
    final double[] timeStep = this.timeIndexResolver_.getTimeStep();
    src_.exposeTimeInitTime_ = new TIntIntHashMap(timeStep.length);
    src_.exposeTimeExtrapolValues_ = new TIntObjectHashMap(_tAjoutMax.size());
    final TDoubleArrayList l = new TDoubleArrayList(timeStep.length + _tAjoutMax.size());
    l.add(timeStep);
    l.add(_tAjoutMax.keys());
    timeStepAll_ = l.toNativeArray();
    Arrays.sort(timeStepAll_);
    for (int i = timeStepAll_.length - 1; i >= 0; i--) {
      if (_tAjoutMax.containsKey(timeStepAll_[i])) {
        src_.exposeTimeExtrapolValues_.put(i, _tAjoutMax.get(timeStepAll_[i]));
      } else if (_initTimeIntPos.containsKey(timeStepAll_[i])) {
        src_.exposeTimeInitTime_.put(i, _initTimeIntPos.get(timeStepAll_[i]));
      } else {
        FuLog.warning(new Throwable());
        src_.exposeTimeInitTime_ = null;
        src_.exposeTimeExtrapolValues_ = null;
        break;
      }
    }
  }

  private boolean loadTime(final CtuluUI _impl, final CtuluIOOperationSynthese _s, final FileFormat ft) {
    final RubarSolutionSequentielResult seqResult = (RubarSolutionSequentielResult) _s.getSource();
    final double[] timeStepRead = new double[seqResult == null ? 0 : seqResult.getTimeStepNb()];
    if (seqResult != null) {
      for (int i = timeStepRead.length - 1; i >= 0; i--) {
        timeStepRead[i] = seqResult.getTimeStep(i);
      }
    }

    if (ft == ftTPC_) {
      this.timeIndexResolver_.setTpcTimeStep(timeStepRead);
    } else if (ft == ftTPS_) {
      this.timeIndexResolver_.setTpsTimeStep(timeStepRead);
    } else if (ft == ftZFN_) {
      this.timeIndexResolver_.setZfnTimeStep(timeStepRead);
    }

    return true;
  }

  void loadDTR(final File _trcHyc, final ProgressionInterface _prog) {
    final File dir = _trcHyc.getParentFile();
    final String name = CtuluLibFile.getSansExtension(_trcHyc.getName());
    final RubarDTRFileFormat dtr = new RubarDTRFileFormat();
    final File f = dtr.getFileExistFor(dir, name);
    if (f != null) {
      final TrPostRubarLimniMng mng = src_.createLimni();
      mng.loadLimni(f, src_.getGrid(), _prog);
    }
  }

  File loadEnv(final ProgressionInterface _prog, final CtuluUI _impl) {
    final File env = (File) fmtLoadable_.get(ftENV_);
    if (env == null) {
      return null;
    }
    final String desc = getDesc(env);
    _prog.setDesc(desc);
    _prog.setProgression(0);
    final RubarEnvReader envReader = new RubarEnvReader();
    envReader.setFile(env);
    envReader.setNbElt(src_.getGrid().getEltNb());
    final CtuluIOOperationSynthese s = envReader.read();
    if (_impl.manageErrorOperationAndIsFatal(s)) {
      return null;
    }
    envResult_ = (TDoubleObjectHashMap) s.getSource();
    src_.initEnvResultat_ = envResult_;
    if (envReader.isTrunc()) {
      warnResTrunc(_impl, desc, -1);
    }
    final double[] tempsHMax = new double[src_.getGrid().getEltNb()];
    final double[] hMax = new double[tempsHMax.length];
    final double[] coteMax = new double[tempsHMax.length];
    final double[] tempsQMax = new double[tempsHMax.length];
    final double[] quMax = new double[tempsHMax.length];
    final double[] qvMax = new double[tempsHMax.length];
    final TDoubleObjectIterator doubleIt = envResult_.iterator();
    for (int i = envResult_.size(); i-- > 0; ) {
      doubleIt.advance();
      final double t = doubleIt.key();
      final TIntObjectHashMap idxMax = (TIntObjectHashMap) doubleIt.value();
      final TIntObjectIterator idxEltMax = idxMax.iterator();
      for (int j = idxMax.size(); j-- > 0; ) {
        idxEltMax.advance();
        final int elt = idxEltMax.key();
        final RubarMaxContainer max = (RubarMaxContainer) idxEltMax.value();
        if (max.isHSet()) {
          tempsHMax[elt] = t;
          hMax[elt] = max.getH();
          coteMax[elt] = max.getH() + src_.g_.getMoyCentreZElement(elt);
        }
        if (max.isQSet()) {
          tempsQMax[elt] = t;
          quMax[elt] = max.getQu();
          qvMax[elt] = max.getQv();
        }
      }
    }
    final Map<H2dVariableType, EfData> varEnveloppData = new TreeMap<>();
    H2dVariableTypeCreated v = H2dVariableType.createTempVar(TrResource.getS("Temps de la hauteur maximale"), null, "s",
      src_.getShortNameCreateVar());
    v.setParent(H2dVariableType.TEMPS);
    varEnveloppData.put(v, new EfDataElement(tempsHMax));
    v = H2dVariableType.createTempVar(TrResource.getS("Temps de la vitesse maximale"), null, "s", src_.getShortNameCreateVar());
    v.setParent(H2dVariableType.TEMPS);
    varEnveloppData.put(v, new EfDataElement(tempsQMax));
    varEnveloppData.put(H2dVariableType.HAUTEUR_EAU, new EfDataElement(hMax));
    varEnveloppData.put(H2dVariableType.COTE_EAU, new EfDataElement(coteMax));
    final EfDataElement qx = new EfDataElement(quMax);
    varEnveloppData.put(H2dVariableType.VITESSE_U, qx);
    final EfDataElement qy = new EfDataElement(qvMax);
    varEnveloppData.put(H2dVariableType.VITESSE_V, qy);
    varEnveloppData.put(H2dVariableType.VITESSE, new TrPostDataVecteur(qx, qy, getSrc().getGrid()));
    src_.setEnvResultats(varEnveloppData);
    src_.setEnvTime(envReader.getTfinal());
    checkEnvTime();
    return env;
  }

  protected void checkEnvTime() {
    if (src_.getEnvTMax() >= 0 && !src_.getTime().isUseTempTimeStep() && src_.getNbTimeStep() > 0) {
      // le test est fait: on l'ignore pour la suite
      src_.clearEnvTime();
      if (Math.abs(src_.getTime().getLastIniTimeStep() - src_.getEnvTMax()) > 1e-3) {
        this.proj_.getImpl().error(TrResource.getS("Le pas de temps du fichier ENV semble incompatible avec les autres r�sultats"));
      }
    }
  }

  void loadFRT(final ProgressionInterface _prog, final CtuluUI _impl) {
    final File frt = (File) fmtLoadable_.get(ftFrt_);
    if (frt == null) {
      return;
    }
    final String desc = getDesc(frt);
    _prog.setDesc(desc);
    _prog.setProgression(0);
    final CtuluIOOperationSynthese op = RubarFRTFileFormat.getInstance().read(frt, _prog, src_.getGrid().getEltNb());
    if (op != null && !_impl.manageAnalyzeAndIsFatal(op.getAnalyze())) {
      final EfDataElement elt = new EfDataElement((CtuluCollectionDouble) op.getSource());
      src_.frt_ = new TrPostDataCreatedConstant(src_, elt, H2dVariableType.COEF_FROTTEMENT_FOND);
    }
  }

  void loadOUTMAS(final ProgressionInterface _prog, final CtuluUI _impl, boolean readOut, boolean readMas) {
    if (!readOut && !readMas) {
      return;
    }
    File ftOut = (File) fmtLoadable_.get(ftOUT_);
    File ftMas = (File) fmtLoadable_.get(ftMAS_);
    if (ftOut == null && ftMas == null) {
      return;
    }
    File f = readOut ? ftOut : ftMas;
    TIntObjectHashMap results = null;
    if (readOut) {
      _prog.setDesc(getDesc(ftOut));
      _prog.setProgression(0);
      RubarOutReader reader = new RubarOutReader();
      CtuluIOOperationSynthese s = reader.read(ftOut, _prog);
      if (_impl.manageErrorOperationAndIsFatal(s)) {
        return;
      }
      results = (TIntObjectHashMap) s.getSource();
    }
    if (readMas) {
      RubarOutReader reader = new RubarOutReader();
      reader.setRes(results);
      reader.setReadConcentration(true);
      _prog.setDesc(getDesc(ftMas));
      _prog.setProgression(0);
      CtuluIOOperationSynthese read = reader.read(ftMas, _prog);
      if (_impl.manageErrorOperationAndIsFatal(read)) {
        return;
      }
      results = (TIntObjectHashMap) read.getSource();
    }
    final int[] idx = results.keys();
    Arrays.sort(idx);
    final RubarOutEdgeResult[] edges = new RubarOutEdgeResult[idx.length];
    for (int i = 0; i < idx.length; i++) {
      edges[i] = (RubarOutEdgeResult) results.get(idx[i]);
    }
    initOuvrageIfNeeded(_prog, _impl, f);
    src_.setEdgesResults(idx, edges);
    final String err = src_.getEdgesRes().checkOutMasTime(src_);
    if (err != null) {
      _impl.error(err);
    }
  }

  private void initOuvrageIfNeeded(final ProgressionInterface _prog, final CtuluUI _impl, File f) {
    if (src_.getEdgesRes() == null || !src_.getEdgesRes().isOuvrageSet()) {
      H2dRubarOuvrageContainer ouvrages = readOuvrages(_prog, _impl, f);
      src_.setOuvrages(ouvrages);
    }
  }

  private H2dRubarOuvrageContainer readOuvrages(final ProgressionInterface _prog, final CtuluUI _impl, File f) {
    // lecture du fichier ouv.
    final RubarOUVFileFormat ouvFmt = new RubarOUVFileFormat();
    H2dRubarOuvrageContainer ouvrages = null;
    final File ouvFile = ouvFmt.getFileExistFor(f.getParentFile(), CtuluLibFile.getSansExtension(f.getName()));
    if (ouvFile != null) {
      CtuluIOOperationSynthese s = ouvFmt.read(ouvFile, _prog);
      if (!_impl.manageErrorOperationAndIsFatal(s)) {
        ouvrages = (H2dRubarOuvrageContainer) s.getSource();
      }
    }
    return ouvrages;
  }

  void loadTPC(final ProgressionInterface _prog, final CtuluUI _impl) {
    final File tpc = (File) fmtLoadable_.get(ftTPC_);
    if (tpc == null) {
      return;
    }
    loadTPCFile(tpc, _prog, _impl, 0);

    //we try to load other tpc files
    File dir = tpc.getParentFile();
    final File[] files = dir.listFiles((dir1, name) -> getTPCIndex(name) > 0);
    if (files != null) {
      for (File file : files) {
        loadTPCFile(file, _prog, _impl, getTPCIndex(file.getName()));
      }
    }
  }

  public static int getTPCIndex(String fileName) {
    String extension = StringUtils.lowerCase(CtuluLibFile.getExtension(fileName));
    final String prefix = "tp";
    if (extension != null && extension.startsWith(prefix)) {
      String idx = StringUtils.substringAfterLast(extension, prefix);
      try {
        return Integer.parseInt(idx);
      } catch (Exception ex) {

      }
    }
    return -1;
  }

  private void loadTPCFile(File tpc, ProgressionInterface _prog, CtuluUI _impl, int idxTPC) {
    //we have to fin
    final String desc = getDesc(tpc);
    _prog.setDesc(desc);
    _prog.setProgression(0);
    final RubarSolutionReader tpcReader = new RubarSolutionReader();
    tpcReader.setNbElt(src_.getGrid().getEltNb());
    tpcReader.setNbValues(3);
    final CtuluIOOperationSynthese s = tpcReader.read(tpc, _prog);
    boolean isTpcCopied = indexOfTPCFileCopied.contains(idxTPC);

    //manage errors:
    if (s.containsSevereError()) {
      if (tpcReader.isLineError() && !isTpcCopied) {
        correctFile(tpcReader, tpc, _impl);
        indexOfTPCFileCopied.add(idxTPC);
        loadTPCFile(tpc, _prog, _impl, idxTPC);
      } else {
        _impl.manageErrorOperationAndIsFatal(s);
      }
      return;
    }

    if (tpcReader.isTruncatedResultats()) {
      warnResTrunc(_impl, desc, tpcReader.getTimeRemoved());
    }
    loadTime(_impl, s, ftTPC_);

    //variables
    //variable 1
    H2dVariableTransType varConcentration = H2dVariableTransType.CONCENTRATION;
    //variable 2
    H2dVariableTransType varDepot = H2dVariableTransType.DIAMETRE;
    //variable 3
    H2dVariableTransType varFrottement = H2dVariableTransType.ETENDUE;
    if (idxTPC > 0) {
      varConcentration = H2dVariableTransType.createIndexedVar(varConcentration, idxTPC);
      varDepot = H2dVariableTransType.createIndexedVar(varDepot, idxTPC);
      varFrottement = H2dVariableTransType.createIndexedVar(varFrottement, idxTPC);
    }

    //key
    final TrPostKey tpcKeyConcentration = TrPostRubarKeys.createTPCKey(0, idxTPC);
    varInitIDx_.put(varConcentration, tpcKeyConcentration);

    final TrPostKey tpcKeyDepot = TrPostRubarKeys.createTPCKey(1, idxTPC);
    varInitIDx_.put(varDepot, tpcKeyDepot);

    final TrPostKey tpcKeyFrottement = TrPostRubarKeys.createTPCKey(2, idxTPC);
    varInitIDx_.put(varFrottement, tpcKeyFrottement);

    try {
      final RubarSolutionSequentielReader tpcSequentialReader = new RubarSolutionSequentielReader(tpc, (RubarSolutionSequentielResult) s.getSource());
      src_.tpcReadersByKeys.put(tpcKeyConcentration, tpcSequentialReader);
      src_.tpcReadersByKeys.put(tpcKeyDepot, tpcSequentialReader);
      src_.tpcReadersByKeys.put(tpcKeyFrottement, tpcSequentialReader);
    } catch (final IOException e) {
      _impl.error(CtuluUIAbstract.getDefaultErrorTitle(), e.getLocalizedMessage(), false);
    }
  }

  void loadTPS(final ProgressionInterface _prog, final CtuluUI _impl) {
    final File tps = (File) fmtLoadable_.get(ftTPS_);
    if (tps == null) {
      return;
    }
    final String desc = getDesc(tps);
    _prog.setDesc(desc);
    _prog.setProgression(0);
    final RubarSolutionReader solReader = new RubarSolutionReader();
    solReader.setNbElt(src_.getGrid().getEltNb());
    solReader.setNbValues(3);
    final CtuluIOOperationSynthese s = solReader.read(tps, _prog);
    if (s.containsSevereError()) {
      if (solReader.isLineError() && !isTpsCopied_) {
        correctFile(solReader, tps, _impl);
        isTpsCopied_ = true;
        loadTPS(_prog, _impl);
      } else {
        _impl.manageErrorOperationAndIsFatal(s);
      }
      return;
    }
    if (solReader.isTruncatedResultats()) {
      warnResTrunc(_impl, desc, solReader.getTimeRemoved());
    }
    loadTime(_impl, s, ftTPS_);
    varInitIDx_.put(H2dVariableType.HAUTEUR_EAU, TrPostRubarKeys.createTPSKey(0));
    varInitIDx_.put(H2dVariableType.DEBIT_X, TrPostRubarKeys.createTPSKey(1));
    varInitIDx_.put(H2dVariableType.DEBIT_Y, TrPostRubarKeys.createTPSKey(2));
    try {
      src_.tpsReader_ = new RubarSolutionSequentielReader(tps, (RubarSolutionSequentielResult) s.getSource());
    } catch (final IOException e) {
      _impl.error(desc, e.getLocalizedMessage(), false);
    }
  }

  void loadTrcOrHyc(final File _trcHyc, final boolean _isHyc, final ProgressionInterface _prog, final CtuluUI _impl) {
    if (!src_.isLimniLoaded()) {
      loadDTR(_trcHyc, _prog);
    }
    final TrPostRubarLimniMng mng = src_.getLimni();
    if (mng != null) {
      final String name = CtuluLibFile.getSansExtension(_trcHyc.getName());
      CtuluIOOperationSynthese s = null;
      if (_isHyc) {
        s = mng.loadHYC(ftHYC_.getFileExistFor(dir_, name), _prog, this);
      } else {
        s = mng.loadTRC(ftTRC_.getFileExistFor(dir_, name), _prog, this);
      }
      if (s != null) {
        _impl.manageErrorOperationAndIsFatal(s);
      }
    }
  }

  void loadZFN(final ProgressionInterface _prog, final CtuluUI _impl) {
    final File zfn = (File) fmtLoadable_.get(ftZFN_);
    if (zfn == null) {
      return;
    }
    final String desc = getDesc(zfn);
    _prog.setDesc(desc);
    _prog.setProgression(0);
    final RubarSolutionReader solReader = new RubarSolutionReader();
    solReader.setNbValueByLigne(10);
    solReader.setNbElt(src_.getGrid().getPtsNb());
    solReader.setNbValues(1);
    final CtuluIOOperationSynthese s = solReader.read(zfn, _prog);
    if (s.containsSevereError()) {
      if (solReader.isLineError() && !isZfnCopied_) {
        correctFile(solReader, zfn, _impl);
        isZfnCopied_ = true;
        loadZFN(_prog, _impl);
      } else {
        _impl.manageErrorOperationAndIsFatal(s);
      }
      return;
    }
    if (!_impl.manageErrorOperationAndIsFatal(s)) {
      if (solReader.isTruncatedResultats()) {
        warnResTrunc(_impl, desc, solReader.getTimeRemoved());
      }
      loadTime(_impl, s, ftZFN_);

      varInitIDx_.put(H2dVariableType.BATHYMETRIE, TrPostRubarKeys.createZFNKey(0));
      try {
        src_.zfnReader_ = new RubarSolutionSequentielReader(zfn, (RubarSolutionSequentielResult) s.getSource());
        src_.zfnReader_.setDoubleLength(10, 8);
        // si les resultats max (fichier env) sont charg�s, il faut recalculer la cote max
        mustCalculateCoteEauMax_ = true;
      } catch (final IOException e) {
        _impl.error(getDesc(zfn), e.getMessage(), false);
      }
    }
  }

  /**
   * @return true is SED file can be loaded.
   */
  private boolean isSedLoadable() {
    return fmtLoadable_.get(ftTPC_) != null || fmtLoadable_.get(ftZFN_) != null || fmtLoadable_.get(ftHYC_) != null || fmtLoadable_.get(ftMAS_) != null;
  }

  void loadSED(final ProgressionInterface _prog, final CtuluUI _impl) {
    if (!isSedLoadable()) {
      return;
    }
    final File file = (File) fmtLoadable_.get(ftSED_);

    if (file == null) {
      return;
    }

    _prog.setDesc(getDesc(file));
    _prog.setProgression(0);

    final RubarSEDReaderDirect reader = (RubarSEDReaderDirect) new RubarSEDFileFormat().createReader();

    reader.setNbNoeuds(src_.getGrid().getPtsNb());
    reader.setFile(file);

    final CtuluIOOperationSynthese s = reader.read();

    if (s.containsSevereError()) {
      _impl.manageErrorOperationAndIsFatal(s);
      return;
    }

    H2dRubarSedimentInterface result = (H2dRubarSedimentInterface) s.getSource();
    //if a sed file has only 1 time steps it should not be loaded.
    if (result.getNbTimes() <= 1) {
      return;
    }
    double[] timeStep = new double[result.getNbTimes()];
    for (int i = 0; i < timeStep.length; i++) {
      timeStep[i] = result.getTimes(i);
    }

    this.timeIndexResolver_.setSedTimeStep(timeStep);
    List<H2dRubarSedimentVariableType> variables = H2dRubarSedimentMng.getVariables();
    int idx = 0;
    for (H2dRubarSedimentVariableType h2dRubarSedimentVariableType : variables) {
      varInitIDx_.put(h2dRubarSedimentVariableType, TrPostRubarKeys.createSEDKey(idx));
      src_.sedimentVariable.put(h2dRubarSedimentVariableType, new TrPostRubarSedimentVariable(src_, h2dRubarSedimentVariableType,
        result));
      idx++;
      if (h2dRubarSedimentVariableType.getParentVariable().equals(H2dVariableTransType.EPAISSEUR)) {
        TrPostRubarSedimentFondVariable value = new TrPostRubarSedimentFondVariable(src_, h2dRubarSedimentVariableType, result);
        H2dVariableTypeCreated variableFond = new H2dVariableTypeCreated(value.getDescription(), src_.getShortNameCreateVar());
        src_.sedimentVariable.put(variableFond, value);
        varInitIDx_.put(variableFond, TrPostRubarKeys.createSEDKey(idx));
        idx++;
      }
    }
  }

  void loadNUA(final ProgressionInterface _prog, final CtuluUI _impl) {
    final File file = (File) fmtLoadable_.get(ftNUA_);

    if (file == null) {
      return;
    }

    _prog.setDesc(getDesc(file));
    _prog.setProgression(0);

    final RubarNUAReader reader = (RubarNUAReader) new RubarNUAFileFormat().createReader();

    reader.setFile(file);

    final CtuluIOOperationSynthese s = reader.read();

    if (s.containsSevereError()) {
      _impl.manageErrorOperationAndIsFatal(s);
      return;
    }

    RubarNUAResult result = (RubarNUAResult) s.getSource();
    src_.setNuaResults(result);
    final String err = src_.getEdgesRes().checkNuaTime(src_);
    if (err != null) {
      _impl.error(err);
    }
  }

  void loadDZF(final ProgressionInterface _prog, final CtuluUI _impl) {
    final File file = (File) fmtLoadable_.get(ftDZF_);

    if (file == null) {
      return;
    }

    _prog.setDesc(getDesc(file));
    _prog.setProgression(0);

    final RubarDZFReader reader = (RubarDZFReader) new RubarDZFFileFormat().createReader();

    reader.setNbNoeuds(src_.getGrid().getPtsNb());
    reader.setFile(file);

    final CtuluIOOperationSynthese s = reader.read();

    if (s.containsSevereError()) {
      _impl.manageErrorOperationAndIsFatal(s);
      return;
    }

    RubarDFZResult result = (RubarDFZResult) s.getSource();
    if (result != null) {
      this.varInitIDx_.put(H2dVariableTransType.VARIATION_COTE_FOND, TrPostRubarKeys.createDZFKey(0));
      src_.dzfResult = result;
      final double[] timeSteps = result.getTimeSteps();
      this.timeIndexResolver_.setZfnTimeStep(timeSteps);
    }
  }

  void loadRES(final ProgressionInterface _prog, final CtuluUI _impl, final FileFormat ft_) {
    final File file = (File) fmtLoadable_.get(ft_);

    if (file == null) {
      return;
    }
    initOuvrageIfNeeded(_prog, _impl, file);
    //pas d'ouvrages charg�s.
    if (!src_.getEdgesRes().isOuvrageSet()) {
      return;
    }
    String name = file.getName();
    String[] list = file.getParentFile().list(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        // TODO Auto-generated method stub
        return name.startsWith(file.getName());
      }
    });

    TIntObjectHashMap<Map<H2dVariableType, EvolutionReguliereInterface>> resultats = new TIntObjectHashMap<Map<H2dVariableType, EvolutionReguliereInterface>>();
    for (String string : list) {
      File resFile = new File(file.getParentFile(), string);
      String extension = CtuluLibFile.getExtension(resFile.getName());
      int idx = -1;
      if ("res".equals(extension)) {
        idx = 1;
      } else if (extension.length() > 3 && extension.startsWith("res")) {
        try {
          idx = Integer.parseInt(extension.substring(3));
        } catch (NumberFormatException e) {
        }
      }
      if (idx < 0) {
        _impl.error(TrLib.getString("Le nom du fichier {0} ne contient pas l'indice de l'ouvrage B"));
        return;
      }
      //indices starts at 0...
      idx--;

      _prog.setDesc(getDesc(resFile));
      _prog.setProgression(0);

      final RubarRESReader reader = (RubarRESReader) new RubarRESFileFormat().createReader();

      reader.setFile(resFile);

      final CtuluIOOperationSynthese s = reader.read();

      if (s.containsSevereError()) {
        _impl.manageErrorOperationAndIsFatal(s);
        return;
      }

      RubarRESResult result = (RubarRESResult) s.getSource();
      _impl.manageAnalyzeAndIsFatal(s.getAnalyze());
      if (result != null) {
        resultats.put(idx, result.getEvolutions(" " + (idx + 1)));
      }
    }
    TrPostRubarOuvrageModel model = src_.getEdgesRes().getModel();
    int idxOuvrage = 0;
    Map<H2dRubarOuvrageElementaireInterface, Map<H2dVariableType, EvolutionReguliereInterface>> resByOuvrageElementaire = new HashMap<H2dRubarOuvrageElementaireInterface, Map<H2dVariableType, EvolutionReguliereInterface>>();
    H2dRubarOuvrageI[] ouvrages = model.getOuvrages();
    for (H2dRubarOuvrageI h2dRubarOuvrageI : ouvrages) {
      if (h2dRubarOuvrageI.containsOuvrageElementaire()) {
        int nbOuvrageElementaires = h2dRubarOuvrageI.getNbOuvrageElementaires();
        for (int j = 0; j < nbOuvrageElementaires; j++) {
          H2dRubarOuvrageElementaireInterface ouvrageElementaire = h2dRubarOuvrageI.getOuvrageElementaire(j);
          if (H2dRubarOuvrageType.BRECHE.equals(ouvrageElementaire.getType()) && resultats.contains(idxOuvrage)) {
            resByOuvrageElementaire.put(ouvrageElementaire, resultats.get(idxOuvrage));
            idxOuvrage++;
          }
        }
      }
    }
    src_.getEdgesRes().setOuvrageBResultat(resByOuvrageElementaire);

    //TODO Mettre result dans bon container.
  }

  void updateValues(final ProgressionInterface _prog, final CtuluUI _impl) {
    this.timeIndexResolver_.computeTimeSteps();

    if (!timeIndexResolver_.isOriginalTimeStep()) {
      _impl.message("", timeIndexResolver_.getTimeStepModificationExplanation(), false);
    }

    final double[] timeStep = this.timeIndexResolver_.getTimeStep();
    _prog.setDesc(FudaaLib.getS("Mise � jour"));
    _prog.setProgression(0);
    // les pas de temps ont �t� charg�s et env aussi : mises � jours de tous les pas de temps
    if (timeStep != null && envResult_ != null) {
      if (envResult_.size() > 0) {
        // on enregistre les positions initiales des pas de temps de bases
        final TDoubleIntHashMap initTimeIntPos = new TDoubleIntHashMap(timeStep.length);
        for (int i = timeStep.length - 1; i >= 0; i--) {
          initTimeIntPos.put(timeStep[i], i);
        }
        double[] coteEau = null;
        BitSet set = null;
        if (mustCalculateCoteEauMax_) {
          coteEau = new double[src_.g_.getEltNb()];
          set = new BitSet(coteEau.length);
        }

        TDoubleObjectHashMap tAjoutMax = new TDoubleObjectHashMap(envResult_);
        // on construit les donn�es
        double[] times = envResult_.keys();
        Arrays.sort(times);
        try {
          for (int i = 0; i < times.length; i++) {
            double time = times[i];
            // on ajoute le pas de temps que s'il n'est pas contenu dans les pas de temps de TPS,TPC
            if (!initTimeIntPos.containsKey(time)) {
              final TrPostSourceRubarMaxContainer max = TrPostSourceRubarMaxContainer.compute(time,
                (TIntObjectHashMap) envResult_.get(time),
                src_.tpsReader_, timeStep);
              tAjoutMax.put(time, max);
              if (coteEau != null) {
                for (int ie = max.getNbContent() - 1; ie >= 0; ie--) {
                  final ElementContent elementContent = max.getContentAt(ie);
                  final double zFond = max.getValueZFNForElement(src_.getGrid().getElement(elementContent.idxElt), src_.zfnReader_);
                  coteEau[elementContent.idxElt] = elementContent.h + zFond;
                  set.set(elementContent.idxElt);
                }
              }
            }
          }
          TrPostSourceRubarMaxContainer.clearLoadingCache();
          if (coteEau != null) {
            // verif a virer
            src_.setNewCoteEau(coteEau);
          }
        } catch (final Exception e1) {
          tAjoutMax = null;
          _impl.error(FudaaLib.getS("Mise � jour"), e1.getLocalizedMessage(), false);
        }
        if (tAjoutMax != null) {
          envAjouteMax(initTimeIntPos, tAjoutMax);
        }
      }
      // on en a plus besoin
      src_.initEnvResultat_ = null;
    }
    if (varInitIDx_ != null && varInitIDx_.size() > 0) {
      src_.addInitData(varInitIDx_);
    } else {
      src_.buildDefaultVectors();
    }

    src_.setTimeIndexResolver(timeIndexResolver_);

    if (timeStep != null) {
      src_.getTime().setTimeSteps(timeStep);
    } // pour avoir un pas de temps null qui permet d'afficher les variables env
    else if (envResult_ != null) {
      src_.getTime().setUseTempTimeStep(true);
    }
    /*
     * else { src_.updateTimeStep(); }
     */
    if (timeStepAll_ != null) {
      src_.allTimeStep_ = timeStepAll_;
    }
    if (first_ && proj_ != null) {
      TrPostSourceBuilder.loadData(_impl, _prog, proj_);
    }
    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {
        if (proj_ != null) {
          // proj_.updateComponents();
          src_.updateVarList();
          src_.fireFlecheListModelChanged();
          updateRubarVisuPanelForSource(proj_, src_);
        }
      }
    });
  }

  protected void updateRubarVisuPanelForSource(TrPostProjet projet, TrPostSourceRubar rubar) {
    List<TrPostLayoutFille> allLayoutFille = projet.getImpl().getAllLayoutFille();
    for (TrPostLayoutFille trPostLayoutFille : allLayoutFille) {
      Collection<EbliNode> nodes = new ArrayList<EbliNode>(trPostLayoutFille.getScene().getNodes());
      boolean refresh = false;
      for (EbliNode ebliNode : nodes) {
        if (ebliNode.getCreator() instanceof EbliWidgetCreatorVueCalque) {
          EbliWidgetCreatorVueCalque createVue = (EbliWidgetCreatorVueCalque) ebliNode.getCreator();
          if (createVue.getCalquesPanel() instanceof TrPostVisuPanelRubar) {
            TrPostVisuPanelRubar panelRubar = (TrPostVisuPanelRubar) createVue.getCalquesPanel();
            if (panelRubar.getSource() == rubar) {
              panelRubar.rubarDataUpdated();
              ((EbliWidgetControllerCalque) ebliNode.getWidget().getIntern().getController()).refreshCalqueWidget();
              refresh = true;
            }
          }
        }
        if (refresh) {
          trPostLayoutFille.getScene().refresh();
        }
      }
    }
  }

  protected void active(TrPostProjet projet, final CtuluUI _f, final ProgressionInterface _inter) {
    proj_ = projet;
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuBorderLayout(5, 5, true, true));
    final BuLabel lb = new BuLabel(TrResource.getS("S�lectionner les fichiers � charger"));
    lb.setHorizontalTextPosition(SwingConstants.CENTER);
    pn.add(lb, BuBorderLayout.NORTH);
    final JTable tb = new JTable();
    final TrPostRubarFmtModel trPostRubarFmtModel = new TrPostRubarFmtModel(fmtToLoad_, fileToLoad_, fmtLoadable_);
    tb.setModel(trPostRubarFmtModel);
    final BuScrollPane sc = new BuScrollPane(tb);
    sc.setColumnHeaderView(null);
    pn.add(sc, BuBorderLayout.CENTER);
    JPanel pnButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));
    JButton btSelectAll = new JButton(TrLib.getString("Tout s�lectionner"));
    JButton btUnSelectAll = new JButton(TrLib.getString("Tout d�s�lectionner"));
    pnButtons.add(btSelectAll);
    pnButtons.add(btUnSelectAll);
    btSelectAll.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        trPostRubarFmtModel.selectAll();
      }
    });
    btUnSelectAll.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        trPostRubarFmtModel.unselectAll();
      }
    });
    pn.add(pnButtons, BorderLayout.SOUTH);

    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_f.getParentComponent()))) {
      if (_inter == null) {
        final CtuluTaskDelegate task = _f.createTask(TrResource.getS("Charger"));
        final ProgressionInterface prog = task.getStateReceiver();
        task.start(new Runnable() {
          @Override
          public void run() {
            loadFiles(prog, _f);
          }
        });
      } else {
        loadFiles(_inter, _f);
      }
    }
  }

  protected final void buildFmtLoadable(final String _name, final File _selectedFile) {
    final String name = _name;
    final List fileLoadable = new ArrayList(availFmt_.length);
    fmtLoadable_ = new HashMap();
    FileFormat selectedFile = null;
    for (int i = 0; i < availFmt_.length; i++) {
      // si non d�j� charg�
      if (!src_.isLoaded(availFmt_[i].getID())) {
        final File f = availFmt_[i].getFileExistFor(dir_, name);
        if (f != null) {
          if (_selectedFile != null && f.equals(_selectedFile)) {
            selectedFile = availFmt_[i];
          }
          // on verifie que le fichier DTR est pr�sent
          if (availFmt_[i] == ftHYC_ || availFmt_[i] == ftTRC_) {
            final File dtrFile = new RubarDTRFileFormat().getFileExistFor(dir_, name);
            if (dtrFile == null) {
              continue;
            }
          }
          //the file sed should be loadable only if there are other transport result files.
          if (availFmt_[i] == ftSED_ && !isSedLoadable()) {
            continue;
          }
          fileLoadable.add(availFmt_[i]);
          fmtLoadable_.put(availFmt_[i], f);
        }
      }
    }
    //Cas particulier, les fichier RES, RE2, RE3, ...
    RubarFileFormatDefault resFileFormat = new RubarFileFormatDefault("RES");
    int fileIndex = 1;

    while (resFileFormat.getFileExistFor(dir_, name) != null) {
      final File f = resFileFormat.getFileExistFor(dir_, name);

      if (_selectedFile != null && f.equals(_selectedFile)) {
        selectedFile = resFileFormat;
      }

      fileLoadable.add(resFileFormat);
      fmtLoadable_.put(resFileFormat, f);

      resFileFormat = new RubarFileFormatDefault("RE" + (++fileIndex));
    }

    fmtToLoad_ = new FileFormat[fileLoadable.size()];
    fileToLoad_ = new boolean[fileLoadable.size()];

    fileLoadable.toArray(fmtToLoad_);
    for (int i = 0; i < fileToLoad_.length; i++) {
      fileToLoad_[i] = true;
    }
  }

  protected TrPostSourceRubar getSrc() {
    return src_;
  }

  protected void setSrc(final TrPostSourceRubar _src) {
    src_ = _src;
  }

  protected void warnResTrunc(final CtuluUI _impl, final String _titre, final double _d) {
    if (!isWarnTrucFileDone_) {
      isWarnTrucFileDone_ = true;
      _impl.warn(
        _titre,
        TrResource.getS("Les r�sultats sont tronqu�s")
          + (_d >= 0 ? (CtuluLibString.LINE_SEP + TrResource.getS("Les pas de temps sont ignor�s � partir de:") + CtuluLibString.ESPACE + _d)
          : CtuluLibString.EMPTY_STRING), false);
    }
  }

  public boolean isManaged(final File _f) {
    final String name = CtuluLibFile.getSansExtension(src_.getMainFile().getName());
    for (int i = 0; i < availFmt_.length; i++) {
      final File f = availFmt_[i].getFileExistFor(dir_, name);
      if (f != null && f.equals(_f)) {
        return true;
      }
    }
    return false;
  }

  public void loadFiles(final ProgressionInterface _prog, final CtuluUI _impl) {
    boolean loadOut = false;
    boolean loadMas = false;
    for (int i = 0; i < fileToLoad_.length; i++) {
      if (fileToLoad_[i]) {
        final FileFormat ft = fmtToLoad_[i];
        if (ft == ftENV_) {
          loadEnv(_prog, _impl);
        } else if (ft == ftTPC_) {
          loadTPC(_prog, _impl);
        } else if (ft == ftTPS_) {
          loadTPS(_prog, _impl);
        } else if (ft == ftZFN_) {
          loadZFN(_prog, _impl);
        } else if (ft == ftTRC_ || ft == ftHYC_) {
          final File f = (File) fmtLoadable_.get(ft);
          loadTrcOrHyc(f, ft == ftHYC_, _prog, _impl);
        } else if (ft == ftOUT_) {
          loadOut = true;
        } else if (ft == ftMAS_) {
          loadMas = true;
        } else if (ft == ftFrt_) {
          loadFRT(_prog, _impl);
        } else if (ft == ftSED_) {
          loadSED(_prog, _impl);
        } else if (ft == ftNUA_) {
          loadNUA(_prog, _impl);
        } else if (ft == ftDZF_) {
          loadDZF(_prog, _impl);
        } else {
          loadRES(_prog, _impl, ft);
        }
        if (ft != ftOUT_ && ft != ftMAS_) {
          src_.setLoaded(ft.getID(), (File) fmtLoadable_.get(ft));
        }
      }
    }
    if (loadMas || loadOut) {
      loadOUTMAS(_prog, _impl, loadOut, loadMas);
      if (loadOut) {
        src_.setLoaded(ftOUT_.getID(), (File) fmtLoadable_.get(ftOUT_));
      }
      if (loadMas) {
        src_.setLoaded(ftMAS_.getID(), (File) fmtLoadable_.get(ftMAS_));
      }
    }

    //we load work data in any case
    initOuvrageIfNeeded(_prog, _impl, getSrc().getMainFile());

    updateValues(_prog, _impl);
    //files have been loaded: we send an event
    if (proj_ != null) {
      proj_.notifyObservers();
    }
  }

  public void setLoadAll() {
    if (fileToLoad_ != null) {
      Arrays.fill(fileToLoad_, true);
    }
  }

  public boolean setToLoad(final File _f) {
    for (int i = 0; i < fmtToLoad_.length; i++) {
      if (fmtToLoad_[i].createFileFilter().accept(_f)) {
        fileToLoad_[i] = true;
        return true;
      }
    }
    return false;
  }

  public void setLoadNone() {
    if (fileToLoad_ != null) {
      Arrays.fill(fileToLoad_, false);
    }
  }
}
