package org.fudaa.fudaa.tr.post.actions;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionAbstract;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostLayoutPanelController;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;

/**
 * Action qui permet de cr�er un widget calque en choisissant un jeu de donn�es. Le jeux de donn�es est donn� par le
 * chooser. Il est choisi par l utilisateur. Le calque correspondant est alors cr��.
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public class TrPostActionChooseAndCreateCalque extends EbliWidgetActionAbstract {

  TrPostProjet projet_;

  /**
   * Chooser associ� au projet.
   */
  public org.fudaa.fudaa.tr.post.TrPostProjetChooser filleProjetctChooser_;
  TrPostLayoutPanelController controller_;

  public TrPostActionChooseAndCreateCalque(final TrPostProjet _projet, final TrPostLayoutPanelController _controller) {
    super(_controller.getSceneCourante(), EbliResource.EBLI.getString("Ins�rer une vue 2D"), BuResource.BU
        .getToolIcon("crystal_graphe"), "WIDGETRECALQUE");

    projet_ = _projet;
    controller_ = _controller;

  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    // -- ouverture du chooser --//
    final TrPostSource srcChoisie = projet_.getChooserMultiSources(-1);
    if (srcChoisie == null) {
      projet_.impl_.error(TrResource.getS(TrResource.getS("Aucune source choisie")));
      return;
    }
    controller_.addSource(srcChoisie);
  }

}
