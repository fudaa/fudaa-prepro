/*
 * @creation 30 mai 2005
 * 
 * @modification $Date: 2007-06-28 09:28:18 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuLib;
import com.memoire.fu.FuLog;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import javax.swing.AbstractListModel;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluDurationFormatter;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.CtuluParser;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.tr.common.TrResource;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id: TrPostTimeModel.java,v 1.27 2007-06-28 09:28:18 deniger Exp $
 */
public class TrPostTimeModel {

  /**
   * Un modele pour les pas de temps.
   * 
   * @author Fred Deniger
   * @version $Id: TrPostTimeModel.java,v 1.27 2007-06-28 09:28:18 deniger Exp $
   */
  public class TimeListModel extends AbstractListModel implements FudaaCourbeTimeListModel {

    protected double[] getParentValues() {
      return timeStep_;
    }

    /**
     * Envoie un evt "tout le contenu a ete modifiee".
     */
    @Override
    public void fireContentChanged() {
      super.fireContentsChanged(this, 0, getNbTimeStep() - 1);
    }

    @Override
    public void fireStructureChanged() {
      super.fireIntervalAdded(this, 0, getNbTimeStep() - 1);
    }

    /**
     * envoie un evt pour avertir que toute la structure du modele a ete modifiee.
     */
    public void fireStructureChanged(final int _old) {
      super.fireIntervalRemoved(this, 0, _old - 1);
      super.fireIntervalAdded(this, 0, getNbTimeStep() - 1);
    }

    @Override
    public Object getElementAt(final int _index) {
      if (timeFmt_ != null) {
        return timeFmt_.format(getTimeStep(_index));
      }
      return CtuluLib.DEFAULT_NUMBER_FORMAT.format(getTimeStep(_index));
    }

    @Override
    public int getSize() {
      return getNbTimeStep();
    }

    @Override
    public CtuluNumberFormatI getTimeFmt() {
      return timeFmt_;
    }

    @Override
    public double getTimeInSec(final int _i) {
      return getTimeStep(_i);
    }

    @Override
    public double[] getTimesInSec() {
      final double[] res = new double[getSize()];
      for (int i = res.length - 1; i >= 0; i--) {
        res[i] = getTimeInSec(i);
      }
      return res;
    }

    @Override
    public void setTimeFmt(final CtuluNumberFormatI _fmt) {
      TrPostTimeModel.this.setTimeFormat(_fmt);
    }

  }

  public static String getVarTimeName() {
    return "t";
  }

  private CtuluParser expr_;

  double[] modifiedTimes_;

  CtuluNumberFormatI timeFmt_ = CtuluDurationFormatter.DEFAULT_TIME;

  TimeListModel timeModel_;

  double[] timeStep_;

  boolean useTempTimeStep_;

  public TrPostTimeModel() {
    super();
  }

  /**
   * @return
   * @see org.fudaa.fudaa.tr.post.TrPostTimeModel.TimeListModel#getTimesInSec()
   */
  public double[] getTimesInSec() {
    return getTimeListModel().getTimesInSec();
  }

  CtuluExpr buildExpr() {
    final CtuluExpr r = expr_ == null ? new CtuluExpr() : new CtuluExpr(expr_);
    r.addVar(getVarTimeName(), TrResource.getS("Le pas de temps lu dans le fichier de r�sultats"));
    try {
      if (expr_ != null) {
        r.getParser().parse(expr_.getLastExpr());
      }
    } catch (final ParseException _e) {
      FuLog.warning(_e);
    }
    return r;
  }

  boolean isExprAvailable() {
    return expr_ != null;
  }

  void setTimeSteps(final double[] _d) {
    useTempTimeStep_ = false;
    final int nbOld = getNbTimeStep();
    if (!Arrays.equals(timeStep_, _d)) {
      timeStep_ = CtuluLibArray.copy(_d);
      if (nbOld == getNbTimeStep() && expr_ == null) {
        updateTimeStepValues();
      } else {
        updateTimeStepStructure(nbOld);
      }
      if (expr_ != null) {
        setExpr(expr_, null);
      }
    }
  }

  private Collection<WeakReference<TimeListModel>> toUpdate;
  private Collection<WeakReference<TrPostTimeFormatListener>> listener = new ArrayList<WeakReference<TrPostTimeFormatListener>>();

  /**
   * Add a listener in a weakreference
   * 
   * @param newListener
   */
  public void addListener(TrPostTimeFormatListener newListener) {
    listener.add(new WeakReference<TrPostTimeFormatListener>(newListener));
  }

  public void removeListener(TrPostTimeFormatListener toRemove) {
    for (Iterator<WeakReference<TrPostTimeFormatListener>> iterator = listener.iterator(); iterator.hasNext();) {
      WeakReference<TrPostTimeFormatListener> next = iterator.next();
      if (next.get() == null || next.get().equals(toRemove)) {
        iterator.remove();
      }
    }
  }

  public boolean containListener(TrPostTimeFormatListener toFind) {
    for (Iterator<WeakReference<TrPostTimeFormatListener>> iterator = listener.iterator(); iterator.hasNext();) {
      WeakReference<TrPostTimeFormatListener> next = iterator.next();
      if (next.get() == null) {
        iterator.remove();
      } else {
        if (next.get().equals(toFind)) {
          return true;
        }
      }
    }
    return false;
  }

  private void fireChanged(CtuluNumberFormatI oldFmt) {
    for (Iterator<WeakReference<TrPostTimeFormatListener>> iterator = listener.iterator(); iterator.hasNext();) {
      WeakReference<TrPostTimeFormatListener> next = iterator.next();
      if (next.get() == null) {
        iterator.remove();
      } else {
        next.get().timeFormatChanged(this, oldFmt, this.timeFmt_);
      }
    }

  }

  protected FudaaCourbeTimeListModel createNewTimeModel() {
    if (toUpdate == null)
      toUpdate = new ArrayList<WeakReference<TimeListModel>>();
    TimeListModel timeListModel = new TimeListModel();
    toUpdate.add(new WeakReference<TimeListModel>(timeListModel));
    return timeListModel;
  }

  protected void initWith(final String _expr, final double[] _modifiedTime, final ProgressionInterface _prog) {
    if (_expr != null) {
      final CtuluExpr expr = buildExpr();
      expr.getParser().parseExpression(_expr);
      if (!expr.getParser().hasError()) {
        // les pas de temps ont �t� enregistr�s et sont a jour
        if (_modifiedTime != null && timeStep_ != null && timeStep_.length == _modifiedTime.length) {
          modifiedTimes_ = _modifiedTime;
          expr_ = expr.getParser();
        } else if (timeStep_ != null) {
          setExpr(expr.getParser(), _prog);
        }
      }
    }

  }

  protected void setExpr(final CtuluParser _expr, final ProgressionInterface _prog) {
    expr_ = _expr;
    if (expr_ == null || expr_.hasError()) {
      modifiedTimes_ = null;
    } else {
      modifiedTimes_ = new double[getNbTimeStep()];
      final ProgressionUpdater up = new ProgressionUpdater(_prog);
      up.setValue(4, getNbTimeStep());
      final Variable t = expr_.getVar(getVarTimeName());
      for (int i = getNbTimeStep() - 1; i >= 0; i--) {
        if (t != null) {
          t.setValue(CtuluLib.getDouble(timeStep_[i]));
        }
        modifiedTimes_[i] = expr_.getValue();
        up.majAvancement();
      }
    }
    BuLib.invokeNow(new Runnable() {

      @Override
      public void run() {
        updateTimeStepValues();
      }
    });
  }

  protected void updateTimeStepStructure(final int _old) {
    if (timeModel_ != null) {
      timeModel_.fireStructureChanged(_old);
    }
  }

  protected void updateTimeStepValues() {
    if (timeModel_ != null) {
      timeModel_.fireContentChanged();
    }
  }

  public double getInitTimeStep(final int _i) {
    return timeStep_[_i];
  }

  public double[] getInitTimeSteps() {
    return CtuluLibArray.copy(timeStep_);
  }

  public double getLastIniTimeStep() {
    return timeStep_[timeStep_.length - 1];
  }

  public int getNbTimeStep() {
    return timeStep_ == null ? (useTempTimeStep_ ? 1 : 0) : timeStep_.length;
  }

  public CtuluNumberFormatI getTimeFormatter() {
    if (timeFmt_ == null) {
      setDefaultTimeFormatter();
    }
    return timeFmt_;
  }

  /**
   * @return un model representant les pas de temps
   */
  public TimeListModel getTimeListModel() {
    if (timeModel_ == null) {
      timeModel_ = new TimeListModel();
    }
    return timeModel_;
  }

  public int findTimeStep(double value, double eps) {
    for (int i = 0; i < timeStep_.length; i++) {
      if (CtuluLib.isEquals(value, getTimeStep(i), eps)) {
        return i;
      }

    }
    return -1;
  }

  public double getTimeStep(final int _i) {
    if (useTempTimeStep_ || _i < 0 || _i >= getNbTimeStep()) {
      return 0;
    }

    return modifiedTimes_ == null ? timeStep_[_i] : modifiedTimes_[_i];
  }

  public String getUsedFormule() {
    return expr_ == null ? null : expr_.getLastExpr();
  }

  public boolean isUseTempTimeStep() {
    return useTempTimeStep_;
  }

  public void setDefaultTimeFormatter() {
    setTimeFormat(CtuluDurationFormatter.DEFAULT_TIME);
  }

  /**
   * @param _model le modele a utiliser pour recuperer le format et la formule de modif
   * @param _prog le barre de progression.
   */
  public void setExprAndFmt(final TrPostTimeModel _model, final ProgressionInterface _prog) {
    if (_model != null) {
      timeFmt_ = _model.timeFmt_;
      if (_model.expr_ != null) {
        setExpr(new CtuluParser(_model.expr_), _prog);
      }
    }
  }

  protected void checkTime(final CtuluAnalyze _analyze) {
    if (timeStep_ != null && timeStep_.length > 1) {
      double last = timeStep_[0];
      int idx = -1;
      for (int i = 1; i < timeStep_.length && idx < 0; i++) {
        final double current = timeStep_[i];
        if (current <= last) {
          idx = i;
        }
        last = current;
      }
      if (idx >= 0) {
        _analyze.addError(TrResource.getS("Les pas de temps ne sont rang�s dans un ordre croissant � partir de t={0}", getTimeListModel()
            .getElementAt(idx).toString()), idx);
      }
    }
  }

  public boolean setTimeFormat(final CtuluNumberFormatI _fmt) {
    if (_fmt != timeFmt_) {
      CtuluNumberFormatI oldFmt = timeFmt_;
      timeFmt_ = _fmt==null?null:_fmt.getCopy();
      if (timeModel_ != null) {
        timeModel_.fireContentChanged();
      }
      fireChanged(oldFmt);

      boolean mustUpdate = false;
      if (toUpdate == null)
        return true;
      for (WeakReference<TimeListModel> it : this.toUpdate) {
        TimeListModel timeListModel = it.get();
        if (timeListModel != null) {
          timeListModel.fireContentChanged();
        } else {
          mustUpdate = true;
        }
      }
      if (mustUpdate) {
        for (Iterator<WeakReference<TimeListModel>> it = toUpdate.iterator(); it.hasNext();) {
          TimeListModel timeListModel = it.next().get();
          if (timeListModel == null) {
            it.remove();
          }
        }
      }
      return true;
    }
    return false;
  }

  public void setUseTempTimeStep(final boolean _useTempTimeStep) {
    useTempTimeStep_ = _useTempTimeStep;
  }

}
