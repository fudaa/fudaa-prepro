/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.fudaa.tr.post.save;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ext.DatabaseFileLockedException;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.fudaa.commun.save.FudaaSaveLib;
import org.fudaa.fudaa.commun.save.FudaaSaveZipLoader;
import org.fudaa.fudaa.commun.save.FudaaSaveZipWriter;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostProjet;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @author deniger
 */
public class TrPostProjetSaver {
  final TrPostProjet projet_;

  public TrPostProjetSaver(final TrPostProjet _projet) {
    super();
    projet_ = _projet;
  }

  private void showError(final CtuluUI _ui, final File _dbFile, final String _err) {
    final String mess = TrResource.getS("Les sauvegardes ne seront pas effectu�es")
        + CtuluLibString.LINE_SEP
        + TrResource.getS("Le fichier de sauvegarde {0} n'est pas accessible:", (_dbFile == null ? "?" : _dbFile
        .getName())) + (_err == null ? CtuluLibString.EMPTY_STRING : (CtuluLibString.LINE_SEP + _err));
    _ui.warn(FudaaSaveLib.getActionSaveTitle(), mess, false);
  }

  public File getSaveFile(final CtuluUI _ui) {
    final File dbFile = getSaveFile();
    final String err = CtuluLibFile.canWrite(dbFile);
    if (err != null) {
      showError(_ui, dbFile, err);
      return null;
    }
    return dbFile;
  }

  File getSaveFile() {
    return null;
  }

  public ObjectContainer getDbSave(final CtuluUI _ui, final File _f) {
    FudaaSaveLib.configureDb4o();
    if (_f == null) {
      return null;
    }
    ObjectContainer db = null;
    try {
      db = Db4o.openFile(_f.getAbsolutePath());
    } catch (final DatabaseFileLockedException _evt) {
      showError(_ui, _f, _evt.getMessage());
    }
    return db;
  }

  public boolean openSrcDataAndIsModified(final CtuluUI _ui, final ProgressionInterface _prog) {
    final File zip = getSaveFile();
    final CtuluAnalyze ana = new CtuluAnalyze();
    final boolean res = true;
    if (zip != null && zip.exists()) {
      FudaaSaveZipLoader loader = null;
      try {
        loader = new FudaaSaveZipLoader(zip);
      } catch (final IOException _evt) {
        FuLog.error(_evt);
      } finally {
        if (loader != null) {
          try {
            loader.close();
          } catch (final IOException _evt) {
            FuLog.error(_evt);
          }
        }
      }
    }
    if (ana != null && _ui != null) {
      _ui.manageAnalyzeAndIsFatal(ana);
    }
    return res;
  }

  public void save(final CtuluUI _ui, final ProgressionInterface _prog) {
    final File zipFile = getSaveFile(_ui);
    final String err = CtuluLibFile.canWrite(zipFile);
    if (err != null) {
      _ui.error(err);
    }
    if (zipFile == null) {
      return;
    }
    if (zipFile.exists()) {
      try {
        Files.delete(zipFile.toPath());
      } catch (IOException e) {
        FuLog.error(e);
      }
    } else {
      try {
        boolean created=zipFile.createNewFile();
        if(!created){
          FuLog.error("can't create file "+zipFile.getAbsolutePath());
        }
      } catch (final IOException exception) {
        FuLog.error(exception);
        _ui.error(CtuluLib.getS("Le fichier {0} ne peut pas �tre �crit", zipFile.getName()));
        return;
      }
    }
    FudaaSaveZipWriter writer = null;
    try {
      writer = new FudaaSaveZipWriter(zipFile);
      FSigLib.addReadmeFile(writer);
    } catch (final IOException _evt) {
      FuLog.error(_evt);
    } finally {
      if (writer != null) {
        try {
          writer.close();
        } catch (final IOException _evt) {
          FuLog.error(_evt);
        }
      }
    }
    projet_.setProjectNotModified();
    // On efface les anciennes versions

  }
}
