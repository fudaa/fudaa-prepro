/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post;

import java.util.Set;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeModel;
import org.fudaa.fudaa.tr.common.TrDataSourceNomme;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 *
 * @author Frederic Deniger
 */
public abstract class AbstractPostCourbeModel extends FudaaCourbeTimeModel implements TrDataSourceNomme {

  /**
   * source utilisee par la courbe
   */
  public TrPostSource source_;
  H2dVariableType var_;

  public AbstractPostCourbeModel(final double[] _timeIdx) {
    super(_timeIdx);
  }

  public void setSource(TrPostSource source) {
    this.source_ = source;
  }

  public AbstractPostCourbeModel() {
    super();
  }

  @Override
  public boolean addValue(double[] _x, double[] _y, CtuluCommandContainer _cmd) {
    time_ = CtuluLibArray.copy(_x);
    y_ = CtuluLibArray.copy(_y);
    return true;
  }

  public boolean clearCache(final H2dVariableType _varChanged, final Set _varUsingModifiedVar) {
    if (var_ == _varChanged || (_varUsingModifiedVar != null) && _varUsingModifiedVar.contains(var_)) {
      clearCache();
      return true;
    }
    return false;
  }

  protected void createY() {
    if (y_ == null || y_.length != this.source_.getNbTimeStep()) {
      y_ = new double[source_.getNbTimeStep()];
    }
  }

  @Override
  public boolean deplace(final int[] _selectIdx, final double _deltaX, final double _deltaY, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public String getTitle() {
    return nom_ == null ? var_.getName() : nom_;
  }

  public final H2dVariableType getVar() {
    return var_;
  }

  @Override
  public void fillWithInfo(InfoData _table, CtuluListSelectionInterface _selectedPt) {
    TrPostSourceAbstractFromIdx.fillWithSourceInfo(_table, source_);
    _table.put(TrResource.getS("Titre Fichier r�sultat"), this.source_.getTitle());
    _table.put(TrResource.getS("Variable"), this.getVar().getName() + " [" + this.getVar().getCommonUnitString() + "]");
  }

  /**
   * @return the source
   */
  public TrPostSource getSource() {
    return source_;
  }

  @Override
  public String getSourceName() {
    return source_.getSourceName();
  }

  public abstract boolean isCourbeForInterpolation(TrPostInterpolatePoint _int);

  public abstract boolean isCourbeForObject(int _idxObj);

  @Override
  public boolean isDuplicatable() {
    return true;
  }

  @Override
  public boolean isGenerationSourceVisible() {
    return true;
  }

  @Override
  public boolean isRemovable() {
    return true;
  }

  @Override
  public boolean isReplayable() {
    return true;
  }

  public abstract boolean isValid();

  public final void setVar(final H2dVariableType _t) {
    var_ = _t;
  }

  protected abstract void updateY();
}
