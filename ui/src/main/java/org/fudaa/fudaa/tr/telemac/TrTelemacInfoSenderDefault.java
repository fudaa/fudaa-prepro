/**
 * @creation 9 d�c. 2003
 * @modification $Date: 2007-03-30 15:40:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcManager;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcParameter;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBordParFrontiere;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBoundary;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBoundaryCondition;
import org.fudaa.dodico.h2d.telemac.H2dTelemacParameters;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrInfoSenderH2dDelegate;

/**
 * @author deniger
 * @version $Id: TrTelemacInfoSenderDefault.java,v 1.34 2007-03-30 15:40:30 deniger Exp $
 */
public class TrTelemacInfoSenderDefault extends TrInfoSenderH2dDelegate {

  /**
   * @param _params
   */
  public TrTelemacInfoSenderDefault(final TrTelemacCommunParametres _params, final EbliFormatterInterface _fmt) {
    super(_fmt, _params.getH2dParametres());
  }

  protected H2dTelemacParameters getTel() {
    return (H2dTelemacParameters) params_;
  }

  @Override
  public void fillWithElementInfo(final InfoData _m, final CtuluListSelectionInterface _selection, final String _title) {
    fillWithElementInfo(_m, _selection, getGrid(), getXYFormatter().getXYFormatter(), false, _title);
    if (_selection != null && _selection.isOnlyOnIndexSelected()) {
      final int idx = _selection.getMinIndex();
      fillMeshAverageValuesParametersInfo(_m, idx, getTel().getNodalData());
      // fillMeshAverageValuesParametersInfo(_m, idx, getTel().getSolutionsInit(), TrLib.getSIDisplayName() + ": ");
    }

  }

  @Override
  public void fillWithBoundaryBlockInfo(final InfoData _receiver, final int _frIdx, final int _idxOnFr) {

    final H2dTelemacBcManager bcMng = getTel().getTelemacCLManager();
    final H2dTelemacBordParFrontiere fr = (H2dTelemacBordParFrontiere) bcMng.getBlockFrontier(_frIdx);
    final H2dTelemacBoundary b = (H2dTelemacBoundary) bcMng.getBoundary(_frIdx, _idxOnFr);
    if (b == null) {
      return;
    }
    // Titre pour bord solide
    // Title for solid boundary
    if (b.getType().isSolide()) {
      _receiver.setTitle(b.getType().getName());
    } else {
      _receiver.setTitle(TrResource.getS("Bord liquide n�") + CtuluLibString.ESPACE + CtuluLibString.getString(bcMng.getIdxLiquidBord(b) + 1));
      _receiver.put(TrResource.getS("Type du bord"), b.getType().toString());
    }
    final int temp = b.getNPointInBord(fr.getNbPt());
    _receiver.put(EbliLib.getS("Nombre de noeuds"), CtuluLibString.getString(temp));
    _receiver.put(TrResource.getS("Indice du premier noeud"), CtuluLibString.getString(b.getIdxDeb() + 1));
    _receiver.put(TrResource.getS("Indice du dernier noeud"), CtuluLibString.getString(b.getIdxFin() + 1));

    if (b.getType().isSolide()) {
      if (fr.isValueConstantOnPoint(b, H2dVariableType.COEF_FROTTEMENT)) {
        _receiver.put(H2dVariableType.COEF_FROTTEMENT.getName(), CtuluLib.DEFAULT_NUMBER_FORMAT.format(fr.getCl(b.getIdxDeb()).getFriction()));
      }
      return;
    }
   
    final H2dTelemacBcParameter[] par = bcMng.getLimiteParameterFor(b.getType());
    if (par != null) {
      int nb = par.length;
      for (int i = 0; i < nb; i++) {
        H2dTelemacBcParameter bcParameter = par[i];
        List<EvolutionReguliereAbstract> evolutions = b.getEvolutions(bcParameter.getVariable());
        if ((bcParameter.isValueFixedFor(bcMng, b))) {
          int nbIntern = bcParameter.getNbInternValues();
          if (nbIntern == 1) {
            String nom = bcParameter.getEntite().getNom();
            if (evolutions.size() == 1) {
              _receiver.put(nom, TrResource.getS("Courbe") + ": " + (evolutions.get(0)).getNom());
            } else {
              _receiver.put(nom, bcParameter.getValueFromBord(b, 0));
            }
          } else {
            for (int intern = 0; intern < nbIntern; intern++) {
              EvolutionReguliereAbstract evol = evolutions.get(intern);
              if (evol == null) {
                _receiver.put(bcParameter.getInternValueName(intern), bcParameter.getValueFromBord(b, intern));
              } else {
                _receiver.put(bcParameter.getInternValueName(intern), TrResource.getS("Courbe") + ": " + evol.getNom());
              }
            }
          }
        }
      }
    }
  }

  @Override
  public void fillWithPointInfo(final InfoData _m, final int _idxGlobal, final int _idxGlobOnFr, final int _frIdx, final int _idxOnFr) {
    super.fillWithPointInfo(_m, _idxGlobal, _idxGlobOnFr, _frIdx, _idxOnFr);
    if (_idxGlobOnFr >= 0) {
      final H2dTelemacBcManager bcMng = (H2dTelemacBcManager) getTel().getCLManager();
      final H2dTelemacBordParFrontiere fr = (H2dTelemacBordParFrontiere) bcMng.getBlockFrontier(_frIdx);
      final H2dTelemacBoundaryCondition bc = fr.getTelemacCl(_idxOnFr);
      // on recupere variables Transposees->variables reelles
      // we get transposed variables -> real variables
      final H2dTelemacBoundary b = (H2dTelemacBoundary) fr.getBordContainingIdx(_idxOnFr);
      final H2dVariableType[] l = bcMng.getVariablesForPoint(b);
      _m.put(TrResource.getS("Type du bord"), b.getType().toString());
      if (l != null) {
        Arrays.sort(l);
        final Map m = bcMng.getVariablesForPointInfo((H2dTelemacBoundary) fr.getBordContainingIdx(_idxOnFr));
        final int n = l.length;
        for (int i = 0; i < n; i++) {
          final H2dVariableType vReel = l[i];
          // cas particulier
          // particular case
          String s = null;
          if (vReel == H2dVariableType.COEF_FROTTEMENT) {
            s = H2dResource.getS("Frottement de bord");
          }
          // si demande, on ajoute une info supplementaire ( utile ?)
          // if asked, we add supplementary information (usefull ?)         
          else if (m != null && m.containsKey(vReel)) {
            s = vReel.getName() + " (" + m.get(vReel) + ")";
          } else {
            s = vReel.getName();
          }
          _m.put(s, CtuluLib.DEFAULT_NUMBER_FORMAT.format(bc.getValue(vReel)));
        }
      }
    }
    if (getTel().getSolutionsInit().isSolutionInitialesActivated()) {
      fillValuesParametersInfo(_m, _idxGlobal, getTel().getSolutionsInit());
    } else {
      fillNodalParametersInfo(_m, _idxGlobal);
    }

  }

  /**
   *
   */
  @Override
  public EfGridInterface getGrid() {
    return params_.getMaillage();
  }

}