/**
 * @creation 13 oct. 2004
 * @modification $Date: 2007-03-15 17:01:13 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.dodico.h2d.rubar.H2dRubarApportListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarApportSpatialMng;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.mesure.EvolutionListener;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.rubar.io.RubarAPPFileFormat;

import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFileStateAPP.java,v 1.9 2007-03-15 17:01:13 deniger Exp $
 */
public class TrRubarFileStateAPP extends TrRubarFileState implements H2dRubarApportListener, EvolutionListener {
  private final H2dRubarApportSpatialMng appMng;

  /**
   * @param _l le listener
   * @param _p les parametres
   */
  public TrRubarFileStateAPP(final TrRubarFileStateMng _l, final H2dRubarParameters _p) {
    super(new RubarAPPFileFormat(), _l);
    appMng = _p.getAppMng();
    appMng.addAppListener(this);
    _p.getBcMng().addEvolutionListener(this);
    if (_p.getAppMng().isSet()) {
      _l.add(this);
    }
  }

  @Override
  public void evolutionChanged(EvolutionReguliereInterface _e) {
    if (appMng.isUsed(_e)) {
      setModified(true);
    }
  }

  @Override
  public void evolutionUsedChanged(EvolutionReguliereInterface _e, int _old, int _new, boolean _isAdjusting) {

  }

  @Override
  public void apportChanged(final H2dRubarApportSpatialMng _mng) {

    if (_mng.isSet()) {
      setMarkRemoved(false);
      if (((TrRubarFileStateMng) l_).contains(fmt_) == null) {
        ((TrRubarFileStateMng) l_).add(this);
      }
    } else {
      setMarkRemoved(true);
    }
    setModified(true);
  }

  @Override
  public void apportEvolutionContentChanged(final H2dRubarApportSpatialMng _mng, final EvolutionReguliereInterface _dest) {
    setModified(true);
  }

  @Override
  public void apportEvolutionUsedChanged(final H2dRubarApportSpatialMng _mng, final EvolutionReguliereInterface _dest) {

  }

  @Override
  public CtuluIOOperationSynthese save(final File _dir, final String _projectName,
                                       final ProgressionInterface _progression, final TrRubarProject _projet) {
    final File f = fmt_.getFileFor(_dir, _projectName);
    return ((FileFormatUnique) fmt_).write(f, _projet.getH2dRubarParameters().getAppMng().getSavedInterface(),
        _progression);
  }
}
