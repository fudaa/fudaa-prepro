/*
 *  @creation     11 mai 2005
 *  @modification $Date: 2007-05-04 14:01:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuList;
import com.memoire.bu.BuScrollPane;
import com.memoire.fu.FuComparator;
import com.memoire.fu.FuSort;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListModel;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrPostFilterVarPanel.java,v 1.10 2007-05-04 14:01:51 deniger Exp $
 */
public class TrPostFilterVarPanel {

  private final JList lsAvoidVar_;
  // les variables précedemment enregistrées qui ne sont pas gérees par la source actuelle
  private List otherVar_;

  /**
   * @param _src la source
   */
  public TrPostFilterVarPanel(final TrPostSource _src) {
    final String[] oldVar = TrPostFilterLayer.getVarFromPref();
    final Set s = new HashSet(Arrays.asList(_src.getAllVariablesNonVec()));
    if (oldVar != null) {
      final Map shortNameCreateVar = _src.getShortNameCreateVar();
      for (int i = oldVar.length - 1; i >= 0; i--) {
        final H2dVariableType t = H2dVariableType.getVarFromShortName(oldVar[i], shortNameCreateVar);
        if (t == null) {
          if (otherVar_ == null) {
            otherVar_ = new ArrayList();
          }
          otherVar_.add(oldVar[i]);
        } else {
          s.add(t);
        }
      }
    }
    final DefaultListModel model = new DefaultListModel();
    final int nb = s.size();
    model.ensureCapacity(nb);
    final H2dVariableType[] vars = new H2dVariableType[nb];
    s.toArray(vars);
    Arrays.sort(vars);
    for (int i = 0; i < nb; i++) {
      model.addElement(vars[i]);
    }
    model.trimToSize();
    lsAvoidVar_ = new BuList(model);
  }

  JPanel buildPanel() {
    final JPanel r = new JPanel();
    final JComponent c = new BuScrollPane(lsAvoidVar_);
    c
        .setToolTipText(TrResource
            .getS("Les variables sélectionnées ne seront pas affectées par les filtres d'affichage"));
    r.add(c, BuBorderLayout.CENTER);
    return r;
  }

  public void updateFor(final H2dVariableType[] _src) {
    final H2dVariableType[] var = _src;
    lsAvoidVar_.clearSelection();
    if (var != null) {
      Arrays.sort(var, FuComparator.STRING_COMPARATOR);
      final ListModel model = lsAvoidVar_.getModel();
      // on parcourt les valeurs de la liste et
      // les variables d'une condition sont toujours dans l'ordre d'ou le binary search
      for (int i = model.getSize() - 1; i >= 0; i--) {
        if (Arrays.binarySearch(var, model.getElementAt(i), FuComparator.STRING_COMPARATOR) >= 0) {
          lsAvoidVar_.getSelectionModel().addSelectionInterval(i, i);
        }
      }
    }
  }

  /**
   * @return les variables a ignorer: peut etre null.
   */
  public H2dVariableType[] getVar() {
    final Set r = new HashSet();
    if (!lsAvoidVar_.isSelectionEmpty()) {
      final int[] idx = lsAvoidVar_.getSelectedIndices();
      for (int i = idx.length - 1; i >= 0; i--) {
        r.add(lsAvoidVar_.getModel().getElementAt(idx[i]));
      }
    }
    /*
     * // les variables précedemment enregistrées qui ne sont pas gérees par la source actuelle if (otherVar_ != null) {
     * r.addAll(otherVar_); }
     */
    final H2dVariableType[] rf = (H2dVariableType[]) r.toArray(new H2dVariableType[r.size()]);
    if (rf != null) {
      FuSort.sort(rf, FuComparator.STRING_COMPARATOR);
    }
    return rf;
  }

  public String[] getAvoidVarForPref() {
    final Set r = new HashSet();
    if (!lsAvoidVar_.isSelectionEmpty()) {
      final int[] idx = lsAvoidVar_.getSelectedIndices();
      for (int i = idx.length - 1; i >= 0; i--) {
        r.add(((H2dVariableType) lsAvoidVar_.getModel().getElementAt(idx[i])).getShortName());
      }
    }
    // les variables précedemment enregistrées qui ne sont pas gérees par la source actuelle
    if (otherVar_ != null) {
      r.addAll(otherVar_);
    }
    final String[] rf = (String[]) r.toArray(new String[r.size()]);
    if (rf != null) {
      FuSort.sort(rf, FuComparator.STRING_COMPARATOR);
    }
    return rf;
  }

}
