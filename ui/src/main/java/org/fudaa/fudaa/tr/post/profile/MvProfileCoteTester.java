/*
 * @creation 13 juin 07
 * @modification $Date: 2007-06-20 12:23:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import com.memoire.fu.FuLog;
import java.util.List;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.operation.EfIndexVisitorNearestElt;
import org.fudaa.dodico.ef.operation.EfIndexVisitorNearestNode;
import org.fudaa.dodico.ef.operation.EfLineIntersection;
import org.fudaa.dodico.ef.operation.EfLineIntersectionParent;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsCorrectionTester;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * Permet de corriger les effets de bords constat�s avec les cotes d'eau.
 * 
 * @author fred deniger
 * @version $Id: MvProfileCoteTester.java,v 1.2 2007-06-20 12:23:43 deniger Exp $
 */
public class MvProfileCoteTester implements EfLineIntersectionsCorrectionTester {

  final CtuluVariable bathy_;
  final CtuluVariable cote_;
  final double eps_ = 1E-3;

  public MvProfileCoteTester() {
    super();
    bathy_ = H2dVariableType.BATHYMETRIE;
    cote_ = H2dVariableType.COTE_EAU;
  }
  
  

  private boolean addIntersection(final int _tidx, final EfLineIntersection _sup1, final EfLineIntersection _min2,
      final List _setNewIn) {
    final double b1 = _sup1.getValue(bathy_, _tidx);
    final double b2 = _min2.getValue(bathy_, _tidx);
    final double c1 = _sup1.getValue(cote_, _tidx);
    if (c1 > b2) { return false; }
    final double alpha = (c1 - b1) / (b2 - b1);
    final double x1 = _sup1.getX();
    final double y1 = _sup1.getY();
    if (alpha >= 1 || alpha <= 0) { return false; }
    final double nx = x1 + (_min2.getX() - x1) * alpha;
    final double ny = y1 + (_min2.getY() - y1) * alpha;
    final EfLineIntersectionParent parent = _sup1.getParent();
    final EfIndexVisitorNearestElt elt = new EfIndexVisitorNearestElt(parent.getGrid(), nx, ny, 1E-3);
    parent.getGrid().getIndex().query(EfIndexVisitorNearestNode.getEnvelope(nx, ny, 1E-2), elt);
    if (elt.getSelected() >= 0) {
      final SpecIntersection specIntersection = new SpecIntersection(elt.getSelected(), nx, ny, _tidx, c1);
      specIntersection.setParent(_sup1.getParent());
      _setNewIn.add(specIntersection);
      return true;
    }
    FuLog.error("Erreur lors de la correction du profil pour la cote");
    return false;

  }

  @Override
  public boolean createNews(final int _tidx, final EfLineIntersection _i1, final EfLineIntersection _i2,
      final List _setNewIn) {
    if(!_i1.isRealIntersection()|| !_i2.isRealIntersection()) return false;
    // les b repr�sentent la bathy
    final double b1 = _i1.getValue(bathy_, _tidx);
    final double b2 = _i2.getValue(bathy_, _tidx);
    // les b repr�sentent la cote d'eau
    final double c1 = _i1.getValue(cote_, _tidx);
    final double c2 = _i2.getValue(cote_, _tidx);
    // h = hauteur d'eau
    final double h1 = c1 - b1;
    final double h2 = c2 - b2;
    final boolean isZero1 = CtuluLib.isZero(h1, eps_);
    final boolean isZero2 = CtuluLib.isZero(h2, eps_);
    // ok si 2 points � zero
    if (isZero1 && isZero2) { return false; }
    // ok si les 2 points sont strictement positifs ou strictement n�gatifs
    if (!isZero1 && !isZero2 && h1 * h2 > 0) { return false; }
    if (b1 < b2) return addIntersection(_tidx, _i1, _i2, _setNewIn);
    return addIntersection(_tidx, _i2, _i1, _setNewIn);
  }

  protected static class SpecIntersection extends EfLineIntersection.ItemMesh {

    final int tidx_;
    final double cote_;

    public SpecIntersection(final int _ielt, final double _x, final double _y, final int _tidx, final double _cote) {
      super(_ielt, _x, _y);
      tidx_ = _tidx;
      cote_ = _cote;
    }

    @Override
    public double getValue(final CtuluVariable _var, final int _tIdx) {
      if (_tIdx == tidx_) {
        if (_var == H2dVariableType.HAUTEUR_EAU || _var == H2dVariableType.VITESSE_U
            || _var == H2dVariableType.VITESSE_V || _var == H2dVariableType.VITESSE) return 0;
        if (_var == H2dVariableType.COTE_EAU || _var == H2dVariableType.BATHYMETRIE) { return cote_; }
      }

      return super.getValue(_var, _tIdx);
    }

  }

  @Override
  public boolean isEnableFor(EfLineIntersectionsResultsI _res) {
    return _res!=null && _res.isDataAvailable(bathy_) && _res.isDataAvailable(cote_);
  }
}
