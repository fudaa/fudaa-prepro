/*
 * @creation 24 mars 2004
 *
 * @modification $Date: 2007-06-28 09:28:18 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import gnu.trove.TObjectIntHashMap;
import gnu.trove.TObjectIntIterator;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

import java.io.IOException;

/**
 * @author Fred Deniger
 * @version $Id: TrPostSourceAbstract.java,v 1.52 2007-06-28 09:28:18 deniger Exp $
 */
public abstract class TrPostSourceAbstractFromIdx extends TrPostSourceAbstract {
    TObjectIntHashMap<H2dVariableType> initVarIdx_;

    protected TrPostSourceAbstractFromIdx(final String _titre, final EfGridInterface _g, final double[] _time,
                                          final H2dVariableType[] _v, final CtuluUI _impl) {
        super(_titre, _g, _time, _impl);
        setInitVar(_v);
    }

    @Override
    public void activate() {
    }

    public abstract EfData getInitData(int _varIdx, int _timeIdx);

    public abstract double getInitData(int _varIdx, int _timeIdx, int _ptIdx) throws IOException;

    protected final int getInitDataIndex(final H2dVariableType _t) {
        if (initVarIdx_ != null && _t != null && initVarIdx_.contains(_t)) {
            return initVarIdx_.get(_t);
        }
        return -1;
    }

    @Override
    protected final void setInitVar(final H2dVariableType[] _v) {
        super.setInitVar(_v);
        buildInitVarIdx(_v);
    }

    private void buildInitVarIdx(H2dVariableType[] initVariables) {
        if (initVariables.length > 0) {
            initVarIdx_ = new TObjectIntHashMap(initVariables.length);
            for (int i = initVariables.length - 1; i >= 0; i--) {
                initVarIdx_.put(initVariables[i], i);
            }
        }
    }

    public final H2dVariableType getVariableWithInitIdx(final int _idx) {
        final TObjectIntIterator it = initVarIdx_.iterator();
        for (int i = initVarIdx_.size(); i-- > 0; ) {
            it.advance();
            if (it.value() == _idx) {
                return (H2dVariableType) it.key();
            }
        }
        return null;
    }

    @Override
    public double getInitData(H2dVariableType _varIdx, int _timeIdx, int _ptIdx) throws IOException {
        final int init = getInitDataIndex(_varIdx);
        if (init >= 0) {
            return getInitData(init, _timeIdx, _ptIdx);
        }
        return 0;
    }

    @Override
    public EfData getInitData(H2dVariableType _varIdx, int _timeIdx) {
        final int init = getInitDataIndex(_varIdx);
        if (init >= 0) {
            return getInitData(init, _timeIdx);
        }
        return null;
    }
}
