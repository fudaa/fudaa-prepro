/*
 *  @creation     15 juin 2005
 *  @modification $Date: 2007-04-30 14:22:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.telemac;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dNodalPropertiesMngI;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.fudaa.meshviewer.model.MvInfoDelegate;
import org.fudaa.fudaa.meshviewer.model.MvNodeModelDefault;
import org.fudaa.fudaa.tr.data.TrExpressionSupplierForData;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacNodeModel.java,v 1.6 2007-04-30 14:22:37 deniger Exp $
 */
public class TrTelemacNodeModel extends MvNodeModelDefault {

  H2dNodalPropertiesMngI params_;

  /**
   * @param _g
   * @param _d
   */
  public TrTelemacNodeModel(final EfGridInterface _grid, final H2dNodalPropertiesMngI _g, final MvInfoDelegate _d) {
    super(_grid, _d);
    params_ = _g;
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return new TrExpressionSupplierForData.Node(params_, TrTelemacNodeModel.this);
  }
}
