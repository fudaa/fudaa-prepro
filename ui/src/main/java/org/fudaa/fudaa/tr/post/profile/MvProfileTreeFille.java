/*
 * @creation 23 nov. 06
 * 
 * @modification $Date: 2007-06-13 12:58:12 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import com.memoire.bu.BuInformationsDocument;
import java.util.List;
import java.util.Set;
import javax.swing.JMenu;
import org.fudaa.ctulu.CtuluNumberFormat;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTime;
import org.fudaa.fudaa.commun.courbe.FudaaGrapheTimeAnimatedFille;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author fred deniger
 * @version $Id: MvProfileTreeFille.java,v 1.9 2007-06-13 12:58:12 deniger Exp $
 */
public class MvProfileTreeFille extends FudaaGrapheTimeAnimatedFille {

  public MvProfileTreeFille(final MvProfileFillePanel _g, final String _titre, final FudaaCommonImplementation _appli,
      final BuInformationsDocument _id) {
    super(_g, _titre, _appli, _id);
    setFrameIcon(MvResource.MV.getFrameIcon("profile"));
    setTitle(_titre);
  }

  @Override
  public final void setTitle(final String _title) {
    super.setTitle(_title);
    getProfilePanel().putClientProperty("title", _title);
  }

  /**
   * @param _old l'ancienne variable
   * @param _new la nouvelle variable remplacant l'ancienne
   * @param _contentChanged true si le contenu a ete modifie. si false, seul le nom a �t� modifie
   * @param _varsUsing la liste des variables concern�es par la modification du contenu de _old
   */
  public void updateCache(final CtuluVariable _old, final CtuluVariable _new, final boolean _contentChanged,
      final Set _varsUsing) {
    getProfilePanel().getProfileTreeModel().updateCache(_old, _new, _contentChanged, _varsUsing);
  }

  public void varUpdated(final CtuluVariable _var) {
    getProfilePanel().getProfileTreeModel().varUpdated(_var);
  }

  /**
   * Pour la visibilit�.
   */
  @Override
  protected void getSpecificComponent(final List _l) {
    super.getSpecificComponent(_l);
  }

  protected MvProfileFillePanel getProfilePanel() {
    return (MvProfileFillePanel) super.p_;
  }

  protected MvProfileTreeModel getProfileModel() {
    return getProfilePanel().getProfileTreeModel();
  }

  @Override
  protected void addTimeMenuItem(final JMenu _m) {
    if (getProfilePanel().getProfileTreeModel().containsTime()) {
      super.addTimeMenuItem(_m);
    }
  }

  public void setTimeFmt(final CtuluNumberFormat _timeFmt) {
    if (timeFmt_ != _timeFmt) {
      timeFmt_ = _timeFmt;
      getProfilePanel().getProfileTreeModel().getTimeModel().setTimeFmt(timeFmt_);
      List<EGCourbeChild> allCourbesChild = getProfilePanel().getProfileTreeModel().getAllCourbesChild();
      for (EGCourbeChild egCourbeChild : allCourbesChild) {
        if (egCourbeChild instanceof FudaaCourbeTime) {
          ((FudaaCourbeTime) egCourbeChild).getTimeModel().setTimeFmt(_timeFmt);
        }
      }
    }
  }

  public static void updateName(final MvProfileTreeFille _fille) {
    _fille.setName("PROFIL_SPATIAL_" + (MvProfileBuilder.idx++));
  }

}
