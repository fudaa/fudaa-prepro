package org.fudaa.fudaa.tr.export;

import org.locationtech.jts.algorithm.CGAlgorithms;
import org.locationtech.jts.algorithm.ConvexHull;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Polygon;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.operation.EfOperation;
import org.fudaa.dodico.ef.operation.projection.EfOperationProjectionOrtho;
import org.fudaa.fudaa.meshviewer.export.MvExportOperationBuilderInterface;
import org.fudaa.fudaa.tr.data.TrVisuPanel;

/**
 * @author deniger
 */
public class TrExportOperationProjectItem extends TrExportOperationOnClosedLineAbstract {
  protected class Builder implements MvExportOperationBuilderInterface {

    LineString line;
    final EfGridData grid;

    private Builder(final LineString line, final EfGridData grid) {
      super();
      this.line = line;
      this.grid = grid;
    }

    @Override
    public void buildOperation(final ProgressionInterface progression) {
      Geometry convexHull = new ConvexHull(line).getConvexHull();
      if (convexHull instanceof LineString) {
        line = (LineString) convexHull;
      } else if (convexHull instanceof Polygon) {
        line = ((Polygon) convexHull).getExteriorRing();
      }
      Coordinate[] coordinates = line.getCoordinates();
      if (!CGAlgorithms.isCCW(coordinates)) {
        CtuluLibArray.invert(coordinates, 0);
        line = GISGeometryFactory.INSTANCE.createLinearRing(coordinates);
      }

    }

    @Override
    public EfOperation getOperation(final int tidx) {
      final EfOperationProjectionOrtho res = new EfOperationProjectionOrtho();
      res.setPolygone((GISPolygone) GISGeometryFactory.INSTANCE.createLinearRing(line.getCoordinates()));
      return res;
    }

    @Override
    public void stop() {

    }

  }

  public static TrExportOperationProjectItem build(String title, CtuluUI ui, EfGridInterface grid, TrVisuPanel visu) {
    if (visu == null) { return new TrExportOperationProjectItem(title, ui, grid); }
    return new TrExportOperationProjectItem(title, visu);
  }

  /**
   * @param title
   * @param ui
   * @param grid
   */
  private TrExportOperationProjectItem(String title, CtuluUI ui, EfGridInterface grid) {
    super(ui, grid, title);
  }

  /**
   * @param title
   * @param panel
   */
  private TrExportOperationProjectItem(String title, TrVisuPanel panel) {
    super(title, panel);
  }

  @Override
  public MvExportOperationBuilderInterface getBuilder(final EfGridData src) {
    return new Builder(mvLineChooser.getSelectedLine(), src);
  }

}
