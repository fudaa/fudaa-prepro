/**
 *  @creation     10 juin 2003
 *  @modification $Date: 2007-04-30 14:22:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.bu.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluUndoRedoInterface;
import org.fudaa.ctulu.gui.CtuluHelpComponent;
import org.fudaa.dodico.h2d.reflux.H2dRefluxParameters;
import org.fudaa.dodico.h2d.type.H2dProjetType;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.fudaa.commun.FudaaProjectStateSupportLabelListener;
import org.fudaa.fudaa.commun.exec.FudaaEditor;
import org.fudaa.fudaa.commun.undo.FudaaUndoPaneFille;
import org.fudaa.fudaa.fdico.FDicoEntitePanel;
import org.fudaa.fudaa.fdico.FDicoEntiteTableModel;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrRefluxFilleProjet.java,v 1.34 2007-04-30 14:22:39 deniger Exp $
 */
public class TrRefluxFilleProjet extends FudaaUndoPaneFille implements ItemListener, CtuluHelpComponent {

  BuComboBox cbProjectType_;

  TrRefluxProjet proj_;
  JComponent[] specificComponents_;
  JLabel txtDate_;
  JLabel txtFile_;
  JLabel txtSave_;

  /**
   * @param _proj le projet a afficher
   */
  public TrRefluxFilleProjet(final TrRefluxProjet _proj) {
    super(_proj.getTitle(), true, true, true, true, _proj.getImpl().getUndoCmdListener());
    proj_ = _proj;
    final BuPanel princ = new PrincipalPanel();
    princ.setLayout(new BuBorderLayout(5, 15));
    final BuPanel centre = new BuPanel();
    centre.setLayout(new BuGridLayout(2, 5, 5, true, true));
    centre.add(new BuLabel(BuResource.BU.getString("Fichier:")));
    txtFile_ = new BuLabel();
    centre.add(txtFile_);
    centre.add(new BuLabel(TrResource.getS("Dernier enregistrement:")));
    txtDate_ = new BuLabel();
    centre.add(txtDate_);
    centre.add(new BuLabel(TrResource.getS("Etat:")));
    txtSave_ = new BuLabel();
    centre.add(txtSave_);
    centre.add(new BuLabel(TrResource.getS("Type:")));
    final H2dRefluxParameters para = proj_.getRefluxParametres().getH2dRefluxParametres();
    final H2dProjetType[] enablePrType = para.getEnableChangeProjectType();
    if (enablePrType == null) {
      final JTextField txt = new BuTextField();
      txt.setEditable(false);
      txt.setText(para.getProjetType().getName());
      centre.add(txt);
    } else {
      cbProjectType_ = new BuComboBox();
      cbProjectType_.setModel(new DefaultComboBoxModel(enablePrType));
      cbProjectType_.setSelectedItem(para.getProjetType());
      cbProjectType_.addItemListener(this);
      centre.add(cbProjectType_);
    }
    princ.add(centre, BuBorderLayout.CENTER);
    final JButton e = new BuButton(BuResource.BU.getIcon("editer"));
    e.setText(BuResource.BU.getString("Editer"));
    e.addActionListener(this);
    e.setActionCommand("EDITER");
    final BuPanel pnEst = new BuPanel();
    pnEst.setLayout(new BuButtonLayout(5, SwingConstants.TOP));
    pnEst.add(e);
    princ.add(pnEst, BuBorderLayout.EAST);
    princ.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    addTab(BuResource.BU.getString("Projet"), null, (CtuluUndoRedoInterface) princ, BuResource.BU.getString("Projet"));
    final CtuluCommandManager cmd = new CtuluCommandManager();
    addTab(TrResource.getS("Propri�t�s du calcul"), null, new FDicoEntitePanel(new FDicoEntiteTableModel(proj_, cmd),
        false, false, false), TrResource.getS("Propri�t�s du calcul"));
    final FudaaProjectStateSupportLabelListener listener = new FudaaProjectStateSupportLabelListener(txtSave_,
        txtDate_, this, txtFile_);
    listener.setSupport(_proj);
    listener.initWith(_proj.getState());
  }

  @Override
  public String getShortHtmlHelp() {
    final CtuluHtmlWriter buf = new CtuluHtmlWriter();
    buf.h2(CtuluResource.CTULU.getString("Description"));
    buf.close(buf.para(), TrResource.getS("le premier onglet donne des informations g�n�rales sur le projet"));
    buf.close(buf.para(), TrResource.getS("le deuxi�me onglet permet de modifier"
        + " les propri�t�s g�n�rales du projet"));
    buf.close(buf.para(), TrResource.getS("le dernier onglet est utilis� pour d�finir les groupes de d�bit"));
    buf.h2(TrResource.getS("Documents associ�s"));
    buf.addTag("ul");
    buf.addTag("li");
    buf.append(proj_.getImpl().buildLink(TrResource.getS("Description de l'�diteur des param�tres g�n�raux"),
        "reflux-editor-params-gen"));
    return buf.toString();
  }

  @Override
  public void actionPerformed(final ActionEvent _ae) {
    if ("EDITER".equals(_ae.getActionCommand())) {
      FudaaEditor.getInstance().edit(proj_.getRefluxParametres().getFile());
    }
  }

  @Override
  public JComponent[] getSpecificTools() {
    if (specificComponents_ == null) {
      specificComponents_ = new JComponent[3];
      specificComponents_[0] = proj_.getCalculActions().getCalcul().buildToolButton(EbliComponentFactory.INSTANCE);
      specificComponents_[1] = proj_.getVisuFilleAction().buildToolButton(EbliComponentFactory.INSTANCE);
      specificComponents_[2] = proj_.getCourbeFilleAction().buildToolButton(EbliComponentFactory.INSTANCE);
    }
    return specificComponents_;
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if ((_e.getStateChange() == ItemEvent.SELECTED) && (_e.getSource() == cbProjectType_)) {
      proj_.getRefluxParametres().getH2dRefluxParametres().setProjetType(
          (H2dProjetType) cbProjectType_.getSelectedItem());
    }
  }

  static final class PrincipalPanel extends BuPanel implements CtuluUndoRedoInterface {

    @Override
    public CtuluCommandManager getCmdMng() {
      return null;
    }

    @Override
    public void setActive(final boolean _b) {}

    @Override
    public void clearCmd(final CtuluCommandManager _source) {}
  }
}