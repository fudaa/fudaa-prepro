/**
 * @creation 1 oct. 2004
 * @modification $Date: 2006-09-19 15:07:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.dodico.fortran.FortranDoubleReaderResultInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarDonneesBrutes;
import org.fudaa.dodico.h2d.rubar.H2dRubarDonneesBrutesListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.rubar.io.RubarDonneesBrutesFileFormat;
import org.fudaa.fudaa.commun.FudaaUI;

import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFileBrutesStateNuage.java,v 1.11 2006-09-19 15:07:26 deniger Exp $
 */
public final class TrRubarFileBrutesStateNuage extends TrRubarFileState implements H2dRubarDonneesBrutesListener {
  protected static void readFile(final File _dir, final String _projectName, final H2dRubarParameters _p,
                                 final RubarDonneesBrutesFileFormat _nuages, final String _id, final TrRubarFileStateMng _l, final FudaaUI _ui,
                                 final ProgressionInterface _progress) {
    final File nuage = new File(_dir, CtuluLibFile.getFileName(_projectName, _nuages.getExtensions()[0]));
    final boolean nuageExists = nuage.exists();
    final TrRubarFileBrutesStateNuage nuageFileState = new TrRubarFileBrutesStateNuage(_id, _nuages, _l);
    if (nuageExists) {
      final CtuluIOOperationSynthese s = _nuages.read(nuage, _progress);
      if (!_ui.manageErrorOperationAndIsFatal(s)) {
        nuageFileState.setInitFile(nuage);
        final FortranDoubleReaderResultInterface i = (FortranDoubleReaderResultInterface) s.getSource();
        final H2dRubarDonneesBrutes b = _p.createDonneesBrutes().get(_id);
        b.initDonneesBrutes(i.createXYValuesInterface());
        if (!b.isNuageEmpty()) {
          _l.add(nuageFileState);
        }
      }
    }
    // permet d'ecouter les evt et de garder en memoire le gestionnaire du format
    _p.createDonneesBrutes().addListener(nuageFileState);
  }

  @Override
  public CtuluIOOperationSynthese save(final File _dir, final String _projectName,
                                       final ProgressionInterface _progression, final TrRubarProject _projet) {
    final H2dRubarParameters param = _projet.getH2dRubarParameters();
    if (param.getDonneesBrutes() == null) {
      return null;
    }
    final H2dRubarDonneesBrutes db = param.getDonneesBrutes().get(brutesId_);
    final File dest = new File(_dir, CtuluLibFile.getFileName(_projectName, fmt_.getExtensions()[0]));
    ((RubarDonneesBrutesFileFormat) fmt_).setNewFormat(_projet.isNewFormatForNumberOfDigits());
    return ((FileFormatUnique) fmt_).write(dest, db.getFileNuageModel(), _progression);
  }

  String brutesId_;

  /**
   * @param fileFormat
   * @param trRubarFileStateMng
   */
  private TrRubarFileBrutesStateNuage(final String _brutesID, final RubarDonneesBrutesFileFormat fileFormat,
                                      final TrRubarFileStateMng trRubarFileStateMng) {
    super(fileFormat, trRubarFileStateMng);
    brutesId_ = _brutesID;
  }

  private void eventModified(final H2dRubarDonneesBrutes _source) {
    if (_source.getID() == brutesId_) {
      setModified(true);
      l_.fileStateChanged(this);
    }
  }

  @Override
  public void donneesBrutesNuageDataChanged(final H2dRubarDonneesBrutes _source) {
    eventModified(_source);
  }

  @Override
  public void donneesBrutesNuageDataNombreChanged(final H2dRubarDonneesBrutes _source) {
    eventModified(_source);
  }

  @Override
  public void donneesBrutesDataChanged(final H2dRubarDonneesBrutes _source) {
    eventModified(_source);
  }

  @Override
  public void donneesBrutesDataNombreChanged(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesSupportAddedOrRemoved(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesSupportChanged(final H2dRubarDonneesBrutes _source) {
  }

  @Override
  public void donneesBrutesNuageSupportAddedOrRemoved(final H2dRubarDonneesBrutes _source) {
    if (_source.getID() == brutesId_) {
      final TrRubarFileStateMng mng = (TrRubarFileStateMng) l_;
      if (_source.isNuageEmpty()) {
        if (mng.contains(this)) {
          setMarkRemoved(true);
        }
      } else {
        setMarkRemoved(false);
        setModified(true);
        if (mng.contains(this)) {
          l_.fileStateChanged(this);
        } else {
          mng.add(this);
        }
      }
    }
  }

  @Override
  public void fileFormatDigitsChanged() {
    setModified(true);
  }

  @Override
  public void donneesBrutesNuageSupportChanged(final H2dRubarDonneesBrutes _source) {
    eventModified(_source);
  }

  @Override
  public void donneesBrutesTypeChanged(final H2dRubarDonneesBrutes _source) {
  }
}
