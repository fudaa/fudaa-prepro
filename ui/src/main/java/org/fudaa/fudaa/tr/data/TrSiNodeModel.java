/*
 *  @creation     20 sept. 2005
 *  @modification $Date: 2007-01-19 13:14:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuTable;
import java.util.Arrays;
import java.util.List;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.h2d.H2dSiSourceInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.model.MvNodeModelDefault;

/**
 * @author Fred Deniger
 * @version $Id: TrSiNodeModel.java,v 1.10 2007-01-19 13:14:09 deniger Exp $
 */
public class TrSiNodeModel extends MvNodeModelDefault {

  final H2dSiSourceInterface src_;

  public TrSiNodeModel(final H2dSiSourceInterface _src, final TrInfoSenderDelegate _info) {
    super(_src.getMaillage(), _info);
    src_ = _src;
  }

  @Override
  public void fillWithInfo(final InfoData _m, final ZCalqueAffichageDonneesInterface _selection) {
    ((TrInfoSenderDelegate) getDelegate()).fillWithSiNodeInfo(_m, (TrSiNodeLayer) _selection);
  }

  protected CtuluNumberFormatI getXYFormat() {
    return super.getDelegate().getXYFormatter().getXYFormatter();
  }

  public H2dSiSourceInterface getSource() {
    return src_;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    if (src_.isSolutionInitialesActivated()) {
      final List r = Arrays.asList(new H2dVariableType[] { H2dVariableType.COTE_EAU, H2dVariableType.HAUTEUR_EAU });
      return new CtuluTable(new TrVarTableModel(getSource().getVariableDataProvider(), r));
    }
    return getDelegate().createValuesTableForNodeLayer();
  }

}
