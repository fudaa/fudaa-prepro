/**
 *  @creation     7 f�vr. 2005
 *  @modification $Date: 2006-09-19 15:07:27 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import gnu.trove.TIntArrayList;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.dodico.ef.EfSegment;
import org.fudaa.dodico.h2d.rubar.H2dRubarArete;
import org.fudaa.dodico.h2d.rubar.H2dRubarGrid;
import org.fudaa.dodico.h2d.type.H2dRubarBcTypeList;
import org.fudaa.fudaa.tr.data.TrInfoSenderDelegate;
import org.fudaa.fudaa.tr.rubar.TrRubarAreteModelDefault;
import org.fudaa.fudaa.tr.rubar.TrRubarBcAreteModel;

/**
 * @author Fred Deniger
 * @version $Id: TrPostRubarBcEdgeModel.java,v 1.11 2006-09-19 15:07:27 deniger Exp $
 */
public final class TrPostRubarBcEdgeModel extends TrRubarAreteModelDefault implements TrRubarBcAreteModel {

  transient H2dRubarArete[] externArete_;
  transient int[] idx_;
  transient CtuluPermanentList bordType_;

  /**
   * @param _grid le maillage
   * @param _info le delegue pour envoye les infos
   * @return le model
   */
  public static TrPostRubarBcEdgeModel buildModel(final H2dRubarGrid _grid, final TrInfoSenderDelegate _info) {
    final int nbArete = _grid.getNbAretes();
    final List extEdges = new ArrayList(nbArete / 2);
    final TIntArrayList lidx = new TIntArrayList(nbArete / 2);
    H2dRubarArete edge;
    final Set bord = new HashSet();
    for (int i = 0; i < nbArete; i++) {
      edge = _grid.getRubarArete(i);
      if (edge.isExtern()) {
        extEdges.add(edge);
        lidx.add(i);
        bord.add(edge.getType());
      }
    }
    final TrPostRubarBcEdgeModel model = new TrPostRubarBcEdgeModel(_grid);
    model.externArete_ = new H2dRubarArete[extEdges.size()];
    extEdges.toArray(model.externArete_);
    model.idx_ = lidx.toNativeArray();
    if (bord.size() > 0) {
      model.bordType_ = new CtuluPermanentList(CtuluLibArray.sort(bord.toArray()));
    } else {
      model.bordType_ = new CtuluPermanentList(new Object[0]);
    }
    // externAH2dRubarArete
    return model;
  }

  /**
   * @param _grid
   */
  private TrPostRubarBcEdgeModel(final H2dRubarGrid _grid) {
    super(_grid,null);
  }

  @Override
  public List getBordList() {
    return H2dRubarBcTypeList.getList();
  }
  
  

  @Override
  public int getGlobalIdx(final int _frIdx) {
    return idx_[_frIdx];
  }

  @Override
  public int[] getGlobalIdx(final int[] _frIdx) {
    if (_frIdx == null) { return null; }
    final int[] globIdx = new int[_frIdx.length];
    for (int i = _frIdx.length - 1; i >= 0; i--) {
      globIdx[i] = getGlobalIdx(_frIdx[i]);
    }
    return globIdx;
  }

  @Override
  public int getNbTypeBord() {
    return bordType_.size();
  }

  @Override
  public EfSegment getArete(final int _idx) {
    return externArete_[_idx];
  }

  @Override
  public int getNombre() {
    return externArete_.length;
  }

  @Override
  public List getUsedBoundaryType() {
    return bordType_;
  }
}