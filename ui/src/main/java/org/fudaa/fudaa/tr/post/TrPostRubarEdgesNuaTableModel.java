package org.fudaa.fudaa.tr.post;

import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.rubar.io.RubarNUAResult;

import javax.swing.table.AbstractTableModel;

public class TrPostRubarEdgesNuaTableModel extends AbstractTableModel {
  private final RubarNUAResult results;
  final RubarNUAResult.RubarNUAResultBloc bloc;

  public TrPostRubarEdgesNuaTableModel(RubarNUAResult results, int idxTime) {
    this.results = results;
    bloc = results.getBloc(idxTime);
  }

  @Override
  public int getRowCount() {
    return bloc.getLines().size();
  }

  @Override
  public int getColumnCount() {
    return 4;
  }

  @Override
  public Class<?> getColumnClass(int columnIndex) {
    if (columnIndex == 0) {
      return Integer.class;
    }
    return Double.class;
  }

  @Override
  public String getColumnName(int columnIndex) {
    if (columnIndex == 0) {
      return "Index";
    }
    if (columnIndex == 1) {
      return "X";
    }
    if (columnIndex == 2) {
      return "Y";
    }
    if (columnIndex == 3) {
      return H2dVariableTransType.COEF_DIFFUSION.getName();
    }
    return "";
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    final RubarNUAResult.RubarNUAResultLine rubarNUAResultLine = bloc.getLines().get(rowIndex);
    if (columnIndex == 0) {
      return rubarNUAResultLine.getAreteIdx();
    }
    if (columnIndex == 1) {
      return rubarNUAResultLine.getPoint().getX();
    }
    if (columnIndex == 2) {
      return rubarNUAResultLine.getPoint().getY();
    }
    if (columnIndex == 3) {
      return rubarNUAResultLine.getCoeff();
    }
    return null;
  }
}
