/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.replay;

import org.locationtech.jts.geom.Coordinate;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.sig.layer.FSigLayerPointEditable;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCourbeModel;
import org.fudaa.fudaa.tr.post.TrPostInterpolatePoint;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;

/**
 *
 * @author Frederic Deniger
 */
public final class TrReplayTemporelleEditCustomizer implements TrReplayVisuCustomizer {
  TrPostCourbeModel modele;

  /**
   * @param listePoints
   */
  /**
   * @param listePoints
   */
  public TrReplayTemporelleEditCustomizer(final TrPostCourbeModel modele) {
    super();
    this.modele = modele;
  }

  @Override
  public void customizeVisu(final TrPostVisuPanel vue2d) {
    final GrPoint replayPOint = new GrPoint();
    final boolean isInterpolated = modele instanceof TrPostCourbeModel.Interpolated;
    // -- on affiche sur la vue 2d les points initialement choisis --//
    final GISZoneCollectionPoint collectionPoint = new GISZoneCollectionPoint();
    final Object point = modele.getSelectionPoint();
    Coordinate coordonneesPoint = null;
    if (point instanceof Integer) {
      // point reel
      final int indiceNoeud = ((Integer) point).intValue();
      // recherche du point cot� grille
      coordonneesPoint = vue2d.getSource().getGrid().getCoor(indiceNoeud);
    } else {
      final TrPostInterpolatePoint pt = (TrPostInterpolatePoint) point;
      coordonneesPoint = new Coordinate(pt.getX(), pt.getY(), 0);
    }
    // -- ajout des coordonnees --//
    collectionPoint.add(coordonneesPoint.x, coordonneesPoint.y, 0);
    replayPOint.setCoordonnees(coordonneesPoint.x, coordonneesPoint.y, 0);
    final ZModelePointEditable modelePointEdit = new ZModelePointEditable(collectionPoint);
    // -- creation du calque
    final FSigLayerPointEditable layer2 = new FSigLayerPointEditable(modelePointEdit, vue2d.getEditor());
    layer2.setVisible(true);
    layer2.setTitle(TrResource.TR.getString("Origine de la courbe"));
    layer2.setIconModel(0, TrReplayVue2dBuilder.createTraceIconModel());
    layer2.setEnabled(true);
    final BCalque resultatsCalque = vue2d.getCalqueActif();
    vue2d.getGroupGIS().add(layer2);
    vue2d.setCalqueActif(resultatsCalque);
    // -- on initialise la sonde --//
    // -- PREPRO 7 --//
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        vue2d.getSondeAction().setSelected(true);
        vue2d.addSondeToCalque(replayPOint);
      }
    });
  }
  
}
