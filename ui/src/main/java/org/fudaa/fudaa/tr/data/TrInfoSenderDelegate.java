/*
 * @creation 9 d�c. 2003
 * @modification $Date: 2007-04-16 16:35:33 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.h2d.H2dSiSourceInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.model.MvInfoDelegateAbstract;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrInfoSenderDelegate.java,v 1.22 2007-04-16 16:35:33 deniger Exp $
 */
public abstract class TrInfoSenderDelegate extends MvInfoDelegateAbstract {

  EbliFormatterInterface fmt_;

  /**
   * @param _fmt
   */
  public TrInfoSenderDelegate(final EbliFormatterInterface _fmt) {
    super();
    fmt_ = _fmt;
  }

  @Override
  public EbliFormatterInterface getXYFormatter() {
    return fmt_;
  }

  public void fillWithBoundaryBlockInfo(final InfoData _m, final EbliListeSelectionMultiInterface _s) {
    final int frIdx = _s == null ? -1 : _s.isSelectionInOneBloc();
    if (_s != null && frIdx >= 0) {
      final CtuluListSelectionInterface l = _s.getSelection(frIdx);
      if (l.isOnlyOnIndexSelected()) {
        fillWithBoundaryBlockInfo(_m, frIdx, l.getMaxIndex());
        return;
      }
    }
    _m.setTitle(TrResource.getS("Bords"));
    _m.put(TrResource.getS("Nombre de bords s�lectionn�s"), _s == null ? CtuluLibString.ZERO : CtuluLibString
        .getString(_s.getNbSelectedItem()));
  }

  public void fillWithBoundaryMiddleInfo(final InfoData _m, final EbliListeSelectionMultiInterface _s) {
    final int frIdx = _s == null ? -1 : _s.isSelectionInOneBloc();
    if (_s != null && frIdx >= 0) {
      final CtuluListSelectionInterface l = _s.getSelection(frIdx);
      if (l.isOnlyOnIndexSelected()) {
        fillWithBoundaryMiddleInfo(_m, frIdx, EfLib.getIdxFromIdxMid(l.getMaxIndex()));
        return;
      }
    }
    _m.setTitle(TrResource.getS("Bords"));
    final int nbSelection = _s == null ? 0 : _s.getNbSelectedItem();
    _m.put(TrResource.getS("Nombre de bords s�lectionn�s"), CtuluLibString.getString(nbSelection));
  }

  @Override
  public void fillWithElementInfo(final InfoData _m, final CtuluListSelectionInterface _selection, final String _title) {
    super.fillWithElementInfo(_m, _selection, _title);
    if (_selection != null && _selection.isOnlyOnIndexSelected()) {
      fillWithElementInfo(_m, _selection.getMaxIndex());
    }
  }

  public void fillWithSiNodeInfo(final InfoData _m, final TrSiNodeLayer _l) {
    final int nbSelected = super.fillWithPointInfoDist(_m, _l.getLayerSelection(), _l.getTitle());
    _m.put(MvResource.getS("Nombre de noeuds s�lectionn�s"), CtuluLibString.getString(nbSelected));
    if (nbSelected == 1) {
      final int i = _l.getLayerSelection().getMaxIndex();
      _m.setTitle(MvResource.getS("Noeud n� {0}", CtuluLibString.getString(i + 1)));
      final H2dSiSourceInterface src = _l.getSource();
      _m.put("X", fmt_.toString(CtuluLib.getDouble(src.getMaillage().getPtX(i))));
      _m.put("Y", fmt_.toString(CtuluLib.getDouble(src.getMaillage().getPtY(i))));
      if (!src.isSolutionInitialesActivated()) { return; }
      final List r = new ArrayList(src.getVariables());
      if (r.size() == 0) { return; }
      if (!r.contains(H2dVariableType.COTE_EAU)) {
        r.add(H2dVariableType.COTE_EAU);
      }
      if (!r.contains(H2dVariableType.HAUTEUR_EAU)) {
        r.add(H2dVariableType.HAUTEUR_EAU);
      }
      Collections.sort(r);
      final int nb = r.size();

      for (int j = 0; j < nb; j++) {
        final H2dVariableType t = (H2dVariableType) r.get(j);
        _m.put(t.getName(), Double.toString(src.getModifiableModel(t).getValue(i)));
      }
    } else {
      _m.setTitle(_l.getTitle());
    }
  }

  public void fillWithElementInfo(final InfoData _m, final int _i) {}

  public void fillWithBoundaryMiddleInfo(final InfoData _m, final int _frIdx, final int _idxOnFr) {}

  public void fillWithBoundaryBlockInfo(final InfoData _m, final int _frIdx, final int _idxOnFr) {}

}