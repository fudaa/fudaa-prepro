/*
 * @creation 18 sept. 06
 * @modification $Date: 2006-10-19 13:55:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import org.fudaa.dodico.h2d.H2dBcFrontierInterface;
import org.fudaa.dodico.h2d.H2dBoundary;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcListener;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.data.TrBcBoundaryBlockModelDefault;

public final class TrTelemacBoundaryBlockLayer extends TrTelemacBcBoundaryBlockLayer implements H2dTelemacBcListener {

  /**
   * Construit avec le modele.
   * Built with the model
   */
  public TrTelemacBoundaryBlockLayer(final TrTelemacVisuPanel _panel) {
    super(new TrBcBoundaryBlockModelDefault(_panel.getBcMng()), _panel.getCqLegend());
    ((TrBcBoundaryBlockModelDefault) m_).setDelegate(_panel.delegateInfo_);
  }

  @Override
  public void bcBoundaryTypeChanged(final H2dBoundary _b, final H2dBoundaryType _old) {
    repaint();
    updateLegende();
  }

  @Override
  public void bcFrontierStructureChanged(final H2dBcFrontierInterface _b) {
    repaint();
    updateLegende();
  }

  @Override
  public void bcParametersChanged(final H2dBoundary _b, final H2dVariableType _t) {
    repaint();
  }

  @Override
  public void bcPointsParametersChanged(final H2dVariableType _t) {}

}