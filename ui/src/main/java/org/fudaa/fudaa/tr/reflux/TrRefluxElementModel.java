/**
 * @creation 12 janv. 2005
 * @modification $Date: 2007-01-19 13:14:33 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.bu.BuTable;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;

/**
 * @author Fred Deniger
 * @version $Id: TrRefluxElementModel.java,v 1.7 2007-01-19 13:14:33 deniger Exp $
 */
public class TrRefluxElementModel extends MvElementModelDefault {

  /**
   * @param _g
   * @param _d
   */
  public TrRefluxElementModel(final EfGridInterface _g, final TrRefluxInfoSenderDefault _d) {
    super(_g, _d);
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    return ((TrRefluxInfoSenderDefault) delegate_).createElementTable();
  }
}