package org.fudaa.fudaa.tr.post;


import org.fudaa.ctulu.CtuluDurationDateFormatter;
import org.fudaa.ctulu.CtuluNumberFormatI;

/**
 * Donn�es persistantes de l'objet TrPostCourbeTreeModel. Ces donn�es serviront a charger/sauvegarder les don�nes
 * sp�cifique au trpostcourbe. Cette classe doit etre s�rializable.
 * 
 * @author Adrien Hadoux
 */
public class TrPostCourbeTreeModelPersist {

  String type;

  //variables
//  Set varSupported;

  //-- points reels ou alors interpol�s ou les 2
//  int[] objIdx;
//  Collection intepolPt;

  String timeFormat;

  public TrPostCourbeTreeModelPersist() {
  }

  /**
   * Methode qui remplit le model en param d'entr�es avec les datas de cette classe.
   * 
   * @param model
   */
  public void fillModelWith(TrPostCourbeTreeModel model) {
    type = "Evolution temporelle";

//    model.varSupported_ = varSupported;
//    model.objIdx_ = objIdx;
//    model.intepolPt_ = intepolPt==null?new ArrayList():new  ArrayList(intepolPt);
  }

  protected CtuluNumberFormatI getDurationFormatter() {
    if (timeFormat == null) {
      return null;
    }
    return CtuluDurationDateFormatter.buildFromPattern(timeFormat);
  };

  /**
   * Methode qui remplit cetet classe a partir du modele fourni en entree.
   */
  public void fillDataWithModel(TrPostCourbeTreeModel model) {
    type = "Evolution temporelle";

//    varSupported = model.varSupported_;
//    objIdx = model.objIdx_;
//    intepolPt = model.intepolPt_;
    timeFormat=model.getLastDurationFormatter();
    
  }

}
