/*
 * @creation 26 oct. 06
 * 
 * @modification $Date: 2007-05-04 14:01:52 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.*;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluUIDialog;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.export.MvExportFactory;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;
import org.fudaa.fudaa.tr.common.TrLauncher;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.export.TrExportFactory;
import org.fudaa.fudaa.tr.reflux.TrRefluxImplHelper;
import org.fudaa.fudaa.tr.rubar.TrRubarImplHelper;

/**
 * @author fred deniger
 * @version $Id: TrPostAnalyze.java,v 1.12 2007-05-04 14:01:52 deniger Exp $
 */
public final class TrPostAnalyze implements ActionListener {

  public static void active(final File _f, final TrLauncher _launch, final Component _frame) {
    final TrPostAnalyze ana = new TrPostAnalyze();
    ana.activeFor(_f, _launch, _frame);
  }

  JFrame dial_;
  int[] iparams_;
  int[] ipobo_;
  String prefId_;

  TrPostSource src_;

  CtuluUIDialog ui_;

  private TrPostAnalyze() {}

  protected void loadPost(final File _f) {
    final Map m = new HashMap();
    src_ = TrPostSourceBuilder.activeSourceAction(_f, ui_, prefId_, ui_.getMainProgression(), m);
    ipobo_ = (int[]) m.get("IPOBO");
    iparams_ = (int[]) m.get("IPARAMS");
    if (src_ == null) {
      dial_.dispose();
      return;
    }
    final MvElementLayer layer = new MvElementLayer(new MvElementModelDefault(src_.getGrid()));
    final ZEbliCalquesPanel pn = new ZEbliCalquesPanel(ui_);
    pn.addCalque(layer);
    pn.setPreferredSize(new Dimension(200, 200));
    final BuPanel info = new BuPanel();
    info.setBorder(CtuluLibSwing.createTitleBorder(TrResource.getS("Informations")));
    pn.setBorder(CtuluLibSwing.createTitleBorder(TrResource.getS("Vue")));
    info.setLayout(new BuGridLayout(2, 5, 5));
    boolean isTelemac3D = false;
    int nbPlan = 0;
    if (src_ instanceof TrPostSourceTelemac3D) {
      isTelemac3D = true;
      nbPlan = ((TrPostSourceTelemac3D) src_).getNbPlan();
      if (CtuluLibDialog.showConfirmation(ui_.getParentComponent(), "TELEMAC 3D", TrResource
          .getS("Le fichier lu est un r�sultat 3D.\nVoulez-vous le transformer en r�sultat 2D ?"), TrLib
          .getString("Exporter en 3D"), TrLib.getString(" Transformer en 2D"))) {
        src_ = ((TrPostSourceTelemac3D) src_).ser_;
      }

    }
    if (isTelemac3D) {
      info.add(new JLabel(TrResource.getS("Attention:")));
      info.add(new JLabel(TrResource.getS("MAILLAGE 3D")));
      info.add(new JLabel(TrResource.getS("Nombre de plans:")));
      info.add(new JLabel(CtuluLibString.getString(nbPlan)));
    }
    info.add(new JLabel(MvResource.getS("Nombre de noeuds:")));
    info.add(new JLabel(Integer.toString(src_.getGrid().getPtsNb())));
    info.add(new JLabel(MvResource.getS("Nombre d'�l�ments:")));
    info.add(new JLabel(Integer.toString(src_.getGrid().getEltNb())));
    info.add(new JLabel(MvResource.getS("Nombre de pas de temps:")));
    info.add(new JLabel(Integer.toString(src_.getNbTimeStep())));
    info.add(new JLabel(MvResource.getS("Nombre de variables:")));
    info.add(new JLabel(Integer.toString(src_.getVariableNb())));
    final BuPanel vert = new BuPanel(new BuVerticalLayout(5));
    final BuPanel pnFin = new BuPanel(new BuGridLayout(2));
    final BuList buList = new BuList(src_.getNewVarListModel());
    final BuList buList2 = new BuList(src_.getNewTimeListModel());
    buList.setEnabled(false);
    buList2.setEnabled(false);
    final BuScrollPane scrollVariables = new BuScrollPane(buList);
    scrollVariables.setPreferredHeight(50);
    pnFin.add(scrollVariables);
    final BuScrollPane scrollTemps = new BuScrollPane(buList2);
    scrollVariables.setBorder(CtuluLibSwing.createTitleBorder(TrResource.getS("Variables")));
    scrollTemps.setBorder(CtuluLibSwing.createTitleBorder(TrResource.getS("Pas de temps")));
    pnFin.add(scrollTemps);
    vert.add(info);
    vert.add(pn);
    vert.add(pnFin);
    final BuButton buButton = new BuButton(BuResource.BU.getIcon("exporter"), TrResource.getS("Exporter"));
    buButton.setHorizontalAlignment(SwingConstants.CENTER);
    final BuPanel btpn = new BuPanel();
    btpn.setLayout(new BuButtonLayout(1, SwingConstants.CENTER));
    btpn.add(buButton);
    vert.add(btpn);

    buButton.addActionListener(this);
    BuLib.invokeLater(new Runnable() {

      @Override
      public void run() {
        refreshPanel(vert);
        pn.restaurer();
      }

    });
  }

  protected void refreshPanel(final JPanel _main) {
    final JPanel pn = (JPanel) dial_.getContentPane();
    pn.removeAll();
    ui_.clearMainProgression();
    pn.add(_main, BuBorderLayout.CENTER);
    dial_.doLayout();
    dial_.pack();
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    String id = getIdOfSource(src_);
    final MvExportFactory fac = new TrExportFactory(new CtuluUIDialog(dial_),id, src_, null);
    fac.setInitIpobo(ipobo_);
    fac.setIparams(iparams_);
    MvExportFactory.startExport(fac, ui_, null);
  }

  public static String getIdOfSource(TrPostSource src) {
    String id = null;
    if (src != null) {
      if (src.containsElementVar()) id = TrRubarImplHelper.getID();
      else if (src instanceof TrPostSourceReflux || src instanceof TrPostSourceReaderReflux) {
        id = TrRefluxImplHelper.getID();
      }
    }
    return id;
  }

  public void activeFor(final File _f, final TrLauncher _launch, final Component _frame) {
    dial_ = new JFrame(_f.getName());
    prefId_ = _launch.getCurrentPrefHydId();
    dial_.setIconImage(BuResource.BU.getImage("analyser"));
    dial_.setResizable(true);
    ui_ = new CtuluUIDialog(dial_);
    final BuPanel pn = new BuPanel(new BuBorderLayout());

    pn.add(ui_.getBar(), BuBorderLayout.NORTH);
    final BuLabel buLabel = new BuLabel(TrResource.getS("Chargement"));
    buLabel.setPreferredSize(new Dimension(200, 250));
    buLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    buLabel.setVerticalTextPosition(SwingConstants.CENTER);
    buLabel.setHorizontalAlignment(SwingConstants.CENTER);
    buLabel.setVerticalAlignment(SwingConstants.CENTER);
    pn.add(buLabel, BuBorderLayout.CENTER);
    dial_.setTitle(_f.getName());
    ui_.createTask(TrResource.getS("Chargement")).start(new Runnable() {
      @Override
      public void run() {
        loadPost(_f);
      }
    });
    dial_.setContentPane(pn);
    dial_.setLocationRelativeTo(_frame);
    dial_.setLocation(dial_.getLocation().x, _frame.getLocation().y);
    dial_.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    dial_.pack();
    dial_.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(final WindowEvent _e) {
        if (src_ != null) {
          src_.close();
        }
      }
    });
    dial_.setVisible(true);

  }
}
