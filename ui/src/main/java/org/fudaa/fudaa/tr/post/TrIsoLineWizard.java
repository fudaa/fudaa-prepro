/*
 * @creation 23 avr. 07
 * @modification $Date: 2007-06-11 13:08:18 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.*;
import gnu.trove.TDoubleHashSet;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluValueValidator;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.gui.CtuluComboBoxModelAdapter;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.controle.BSelecteurListTarget;
import org.fudaa.ebli.controle.BSelecteurListTimeTarget;
import org.fudaa.fudaa.meshviewer.export.MvExportPanelFilter;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.export.TrExportPanelFilterFactory;

/**
 * @author fred deniger
 * @version $Id: TrIsoLineWizard.java,v 1.4 2007-06-11 13:08:18 deniger Exp $
 */
public class TrIsoLineWizard extends BuWizardTask {

  BuCheckBox cbOneLayer_;

  BuComboBox cbVariables_;

  CtuluListEditorPanel ctuluListEditorPanel_;
  CtuluVariable currentVariable_;

  final TrPostVisuPanel dest_;

  MvExportPanelFilter filter_;

  BuLabel lbTime_;

  BuList listTimes_;
  BuPanel pnFinish_;
  BuPanel pnValues_;
  BuPanel pnVars_;

  BuTextField tfPrec_;

  final TrIsoLineWizardListValue values_;

  Map<CtuluVariable, List> varIsoValues_ = new HashMap<CtuluVariable, List>();

  public TrIsoLineWizard(final TrPostVisuPanel _dest) {
    super();
    dest_ = _dest;
    final ListModel newVarListModel = dest_.getSource().getNewVarListModel();
    // rien !
    Object selected = getSelectedVarInLayer();
    // on recherche si la variable selectionn� dans le calque courant est bien dispo
    boolean found = false;
    for (int i = newVarListModel.getSize() - 1; i >= 0 && !found; i--) {
      found = newVarListModel.getElementAt(i) == selected;
    }
    final CtuluComboBoxModelAdapter cbModel = new CtuluComboBoxModelAdapter(newVarListModel);
    if (!found) {
      selected = newVarListModel.getElementAt(0);
    }

    cbModel.setSelectedItem(selected);
    cbVariables_ = new BuComboBox(cbModel);
    final ListModel time = dest_.getSource().getNewTimeListModel();
    final int idx = getSelectedTimeInLayer();
    listTimes_ = new BuList(time);
    listTimes_.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    if (idx >= 0) {
      listTimes_.setSelectedIndex(idx);
      listTimes_.setAutoscrolls(true);
    }

    values_ = new TrIsoLineWizardListValue(this);
    final List values = values_.getSelectedValues();
    if (values == null) {
      values_.updateOrCreateValues();
    } else {
      values_.setData(values);
    }
    currentVariable_ = (CtuluVariable) cbVariables_.getSelectedItem();

  }

  private void buildFinishPn() {
    if (pnFinish_ != null) { return; }
    pnFinish_ = new BuPanel(new BuBorderLayout(0, 10));
    BuPanel pnCommon = new BuPanel();
    pnCommon.setLayout(new BuGridLayout(2, 5, 5));
    tfPrec_ = BuTextField.createDoubleField();
    final CtuluValueValidator min = new CtuluValueValidator.DoubleMin(0);
    tfPrec_.setValueValidator(min);
    tfPrec_.setToolTipText(min.getDescription());
    tfPrec_.setValue(new Double(1E-4));
    pnCommon.add(new BuLabel(TrLib.getString("Pr�cision des isolignes")));
    pnCommon.add(tfPrec_);
    pnCommon.add(new BuLabel(TrLib.getString("Cr�er un calque par valeur")));
    cbOneLayer_ = new BuCheckBox();
    pnCommon.add(cbOneLayer_);
    pnFinish_.add(pnCommon, BuBorderLayout.NORTH);
    pnCommon = new BuPanel();
    pnCommon.setLayout(new BuVerticalLayout(1));
    pnCommon.add(new BuLabel(TrLib
        .getString("Utiliser un filtre pour extraire les isoslignes que sur une partie du maillage:")));
    filter_ = TrExportPanelFilterFactory.buildFilterForEachTimeStep(dest_.getCurrentSelection(), dest_.getSource());
    pnCommon.add(filter_.getComponent());
    pnFinish_.add(pnCommon, BuBorderLayout.CENTER);

  }

  private void buildFirstPn() {
    if (pnVars_ != null) { return; }
    pnVars_ = new BuPanel();
    pnVars_.setLayout(new BuVerticalLayout(5));
    BuPanel pn = new BuPanel(new BuBorderLayout());
    pn.setBorder(CtuluLibSwing.createTitleBorder(TrLib.getString("Variable")));
    pn.add(cbVariables_);
    pnVars_.add(pn);
    cbVariables_.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent _e) {
        varChanged();
      }
    });
    pn = new BuPanel(new BuBorderLayout());
    pn.setBorder(CtuluLibSwing.createTitleBorder(TrLib.getString("Pas de temps")));
    lbTime_ = new BuLabel(CtuluLibString.ESPACE);
    lbTime_.setForeground(Color.RED);
    pn.add(lbTime_, BuBorderLayout.NORTH);
    pn.add(new BuScrollPane(listTimes_));
    pnVars_.add(pn);

    pnVars_.setPreferredSize(new Dimension(550, 450));

  }

  private void buildValuesPn() {
    if (pnValues_ != null) { return; }
    pnValues_ = new BuPanel();
    pnValues_.setLayout(new BuBorderLayout(5, 5, false, true));
    final BuButton bt = new BuButton(BuResource.BU.getIcon("effacer"));
    bt.setText(TrLib.getString("Tout enlever"));
    bt.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _e) {
        values_.removeAll();

      }

    });
    pnValues_.add(bt, BuBorderLayout.SOUTH);
    ctuluListEditorPanel_ = new CtuluListEditorPanel(values_);
    pnValues_.add(ctuluListEditorPanel_, BuBorderLayout.CENTER);

  }

  @Override
  public void doTask() {
    final H2dVariableType var = (H2dVariableType) cbVariables_.getSelectedItem();
    final int[] times = listTimes_.getSelectedIndices();
    final TDoubleHashSet set = new TDoubleHashSet(values_.getRowCount());
    for (int i = 0; i < values_.getRowCount(); i++) {
      set.add(Double.parseDouble((String) values_.getValueAt(i)));
    }
    final double[] values = set.toArray();
    Arrays.sort(values);
    final TrIsoLineAdder trIsoLineAdder = new TrIsoLineAdder(dest_.getSource(), dest_, values, times, var,
        ((Double) tfPrec_.getValue()).doubleValue());
    trIsoLineAdder.setUseOnValueByLayer(cbOneLayer_.isSelected());
    trIsoLineAdder.setFilter(filter_.getOperation(dest_.getSource()));
    trIsoLineAdder.go();
    done_ = true;
  }

  /**
   * @return
   * @see org.fudaa.fudaa.tr.post.TrPostVisuPanel#getProjet()
   */
  public TrPostProjet getProjet() {
    return dest_.getProjet();
  }

  private int getSelectedTimeInLayer() {
    final BCalque cq = dest_.getCalqueActif();
    if (cq instanceof BSelecteurListTimeTarget) { return ((BSelecteurListTimeTarget) cq).getTimeListSelectionModel()
        .getMaxSelectionIndex(); }
    return dest_.getSource().getNbTimeStep() - 1;
  }

  private Object getSelectedVarInLayer() {
    final BCalque cq = dest_.getCalqueActif();
    if (cq instanceof BSelecteurListTarget) {
      final BSelecteurListTarget selecteurListTarget = (BSelecteurListTarget) cq;
      final int idx = selecteurListTarget.getListSelectionModel().getMaxSelectionIndex();
      if (idx >= 0) { return selecteurListTarget.getListModel().getElementAt(idx); }
    }
    return H2dVariableType.HAUTEUR_EAU;
  }

  /**
   * @return
   * @see org.fudaa.fudaa.tr.post.TrPostVisuPanel#getSource()
   */
  public TrPostSource getSource() {
    return dest_.getSource();
  }

  @Override
  public JComponent getStepComponent() {
    // pour prendre en compte les modifications dans le tableau au cas ou la cellule serait toujours en �dition
    if (ctuluListEditorPanel_ != null) {
      ctuluListEditorPanel_.stopCellEditing();
    }
    if (current_ == 0) {
      buildFirstPn();
      final int idx = listTimes_.getSelectedIndex();
      if (idx >= 0) {
        listTimes_.scrollRectToVisible(listTimes_.getCellBounds(idx, idx));
      }

      return pnVars_;

    } else if (current_ == 1) {
      buildValuesPn();
      return pnValues_;
    }
    buildFinishPn();
    return pnFinish_;
  }

  @Override
  public int getStepCount() {
    return 3;
  }

  @Override
  public String getStepText() {
    if (current_ == 0) { return TrLib.getString("Vous devez choisir une variable et un ou plusieurs pas de temps"); }
    if (current_ == 1) { return TrLib.getString("Choisir les valeurs � utiliser pour construire les isolignes"); }
    if (current_ == 2) { return "+ "
        + TrLib.getString("La pr�cision permet de prendre en compte les impr�cisions num�riques.")
        + CtuluLibString.LINE_SEP_SIMPLE
        + "       "
        + TrLib
            .getString("2 valeurs seront suppos�es �gales si leur diff�rence est inf�rieure � la pr�cision ci-dessus.")
        + CtuluLibString.LINE_SEP_SIMPLE + CtuluLibString.ESPACE + CtuluLibString.LINE_SEP_SIMPLE + "+ "
        + TrLib.getString("La case � cocher permet de cr�er un calque par valeurs sp�cifi�es pr�c�demment."); }

    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public String getStepTitle() {
    if (current_ == 0) {
      return TrLib.getString("Choisir la variable et les pas de temps");
    } else if (current_ == 1) {
      return TrLib.getString("Choisir les valeurs des isolignes");

    } else if (current_ == 2) { return TrLib.getString("Pr�cision et calques"); }
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public String getTaskTitle() {
    return TrLib.getString("Rechercher les isolignes");
  }

  boolean isCurrentValide() {
    if (current_ == 0) {
      final boolean res = !listTimes_.isSelectionEmpty();
      if (res) {
        lbTime_.setText(CtuluLibString.ESPACE);
      } else {
        lbTime_.setText(TrLib.getString("S�lectionner au moins un pas de temps"));
      }
      return res;
    }
    return true;
  }

  @Override
  public void nextStep() {
    if (isCurrentValide()) {
      super.nextStep();
    }
  }

  void varChanged() {
    // on sauvegarde les anciennes valeurs
    varIsoValues_.put(currentVariable_, values_.getValuesInList());
    currentVariable_ = (CtuluVariable) cbVariables_.getSelectedItem();
    final List saved = varIsoValues_.get(currentVariable_);
    if (saved == null) {
      values_.updateOrCreateValues();
    } else {
      values_.setData(saved);
    }
  }

}
