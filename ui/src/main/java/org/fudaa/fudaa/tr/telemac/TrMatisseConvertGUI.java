/*
 * @creation 2 avr. 2004
 * 
 * @modification $Date: 2007-06-28 09:28:19 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuBorders;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuButtonLayout;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuProgressBar;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuToolButton;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluProgressionBarAdapter;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISZone;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.telemac.io.MatisseReader;
import org.fudaa.dodico.telemac.io.SinusxFileFormat;
import org.fudaa.dodico.telemac.io.SinusxWriterV21;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrMatisseConvertGUI.java,v 1.10 2007-06-28 09:28:19 deniger Exp $
 */
public class TrMatisseConvertGUI extends CtuluDialogPanel implements ActionListener, CtuluActivity {

  public static void active(final File _f, final Component _frame) {
    final TrMatisseConvertGUI gui = new TrMatisseConvertGUI(false);
    gui.in_.setText(_f.getAbsolutePath());
    gui.in_.setToolTipText(_f.getAbsolutePath());
    gui.afficheModale(_frame, TrResource.getS("Analyseur fichier matisse BATHYGEO"));
  }

  BuButton btCharger_;
  BuToolButton btStop_;
  BuButton btOut_;
  // BuComboBox cbFmt_;
  boolean containsData_;
  CtuluActivity current_;
  JTextField in_;
  GISZone mnt_;
  JTextField out_;
  ProgressionInterface progress_;
  BuTextField txtPoints_;
  BuTextField txtpolygones_;

  BuTextField txtpolylignes_;

  BuTextField txtZones_;

  public TrMatisseConvertGUI(final boolean _fileEditable) {
    super();

    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    final BuPanel pnCharger = new BuPanel();
    final CompoundBorder border = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(),
        BuBorders.EMPTY2222);
    pnCharger.setBorder(BorderFactory.createTitledBorder(border, DodicoLib.getS("Fichiers")));
    pnCharger.setLayout(new BuGridLayout(2, 5, 5));
    final String string = TrResource.getS("Fichier") + " BATHYGEO : ";
    if (_fileEditable) {
      in_ = addLabelFileChooserPanel(pnCharger, string, null, false, false);
    } else {
      pnCharger.add(new BuLabel(string));
      in_ = new BuTextField();
      pnCharger.add(in_);
      in_.setEditable(false);
    }
    out_ = addLabelFileChooserPanel(pnCharger, TrResource.getS("Fichier de sortie") + ": ", null, false, true);
    final BuPanel info = new BuPanel();

    info.setBorder(BorderFactory.createTitledBorder(border, DodicoLib.getS("Contenu du fichier BATHYGEO")));
    info.setLayout(new BuGridLayout(2, 5, 5));
    txtZones_ = addLabelDoubleText(info, EbliLib.getS("Nombre de zones"));
    txtZones_.setEditable(false);
    txtPoints_ = addLabelDoubleText(info, EbliLib.getS("Nombre de points total"));
    txtPoints_.setEditable(false);
    txtpolygones_ = addLabelDoubleText(info, EbliLib.getS("Nombre de lignes ferm�es"));
    txtpolygones_.setEditable(false);
    txtpolylignes_ = addLabelDoubleText(info, EbliLib.getS("Nombre de lignes"));
    txtpolylignes_.setEditable(false);
    btCharger_ = new BuButton(CtuluLib.getS("Charger le fichier BATHYGEO"));
    btCharger_.setToolTipText("<html><body>"
        + TrLib.getString("<b>Si un fichier BATHYGEO est sp�cifi�</b>,<br> cette action permet de le charger"));
    btCharger_.setIcon(BuResource.BU.loadMenuCommandIcon("analyser"));
    btCharger_.addActionListener(this);
    btOut_ = new BuButton(DodicoLib.getS("Charger et exporter"));
    btOut_
        .setToolTipText("<html><body>"
            + TrLib
                .getString("<b>Si les fichiers sont sp�cifi�s</b>,<br> cette action permet d'exporter le fichier BATHYGEO au format sinusx"));
    btOut_.setIcon(BuResource.BU.loadMenuCommandIcon("enregistrer"));
    btOut_.addActionListener(this);
    final DocumentListener outListener = new DocumentListener() {

      @Override
      public void changedUpdate(final DocumentEvent _e) {
        updateSave();
      }

      @Override
      public void insertUpdate(final DocumentEvent _e) {
        updateSave();
      }

      @Override
      public void removeUpdate(final DocumentEvent _e) {
        updateSave();

      }

    };
    final DocumentListener inListener = new DocumentListener() {

      @Override
      public void changedUpdate(final DocumentEvent _e) {
        updateIn();
      }

      @Override
      public void insertUpdate(final DocumentEvent _e) {
        updateIn();
      }

      @Override
      public void removeUpdate(final DocumentEvent _e) {
        updateIn();

      }

    };
    out_.getDocument().addDocumentListener(outListener);
    in_.getDocument().addDocumentListener(inListener);
    setLayout(new BuBorderLayout(10, 10));
    final BuPanel main = new BuPanel();
    main.setLayout(new BuBorderLayout(10, 10));
    main.add(pnCharger, BuBorderLayout.NORTH);
    main.add(info, BuBorderLayout.CENTER);
    final BuProgressBar progress = new BuProgressBar();
    progress_ = new CtuluProgressionBarAdapter(progress);

    final BuPanel pn = new BuPanel();
    pn.setLayout(new BuBorderLayout());
    btStop_ = new BuToolButton(BuResource.BU.getToolIcon("arreter"));
    btStop_.addActionListener(this);
    btStop_.setEnabled(false);
    pn.add(progress, BuBorderLayout.CENTER);
    pn.add(btStop_, BuBorderLayout.EAST);
    main.add(pn, BuBorderLayout.SOUTH);
    add(main, BuBorderLayout.CENTER);
    final BuPanel bt = new BuPanel(new BuButtonLayout(4, SwingConstants.CENTER));
    bt.add(btCharger_);
    bt.add(btOut_);

    add(bt, BuBorderLayout.SOUTH);
    updateIn();
    updateSave();
  }

  private void load() {
    final String file = in_.getText().trim();
    if (file.length() == 0) { return; }
    final File f = new File(file);
    if (!f.exists()) {
      CtuluLibDialog.showError(this, "BATHYGEO", DodicoLib.getS("Fichier non trouv�"));
    }

    new Thread() {

      @Override
      public void run() {
        loadMatisseAct(f);
      }
    }.start();
  }

  private void save() {

    final String file = out_.getText().trim();
    if (file.length() == 0) { return; }
    File f = new File(file);
    if (!f.isAbsolute()) {
      final String txt = in_.getText().trim();
      if (txt != null) {
        final File dir = new File(txt).getParentFile();
        if (dir.exists()) {
          f = new File(dir, file);
          out_.setText(f.getAbsolutePath());
          out_.setToolTipText(out_.getText());
        }
      }

    }
    final File sx = f;
    final String err = CtuluLibFile.canWrite(sx);
    if (err != null) {
      CtuluLibDialog.showError(this, TrResource.getS("Fichier de sortie") + ":" + sx.getName(), err);
      return;
    }

    new Thread() {

      @Override
      public void run() {
        if (mnt_ == null) {
          loadMatisseAct(new File(in_.getText()));
          if (mnt_ == null) {
            CtuluLibDialog.showError(TrMatisseConvertGUI.this, DodicoLib.getS("Pas de donn�es"), TrResource
                .getS("Avant d'exporter un projet, vous devez analyser un fichier matisse BATHYGEO"));
            return;
          }
        }
        btStop_.setEnabled(true);
        progress_.setDesc(CtuluLib.getS("Export"));
        final SinusxWriterV21 sxW = new SinusxWriterV21(SinusxFileFormat.getInstance());
        current_ = sxW;
        sxW.write(mnt_, sx, progress_);
        CtuluLibDialog.showMessage(btCharger_, "OK", TrResource.getS("Export r�ussie"));
        progress_.reset();
        btStop_.setEnabled(false);

      }
    }.start();

  }

  protected final void updateIn() {
    final String in = in_.getText();
    btCharger_.setEnabled(!CtuluLibString.isEmpty(in) && new File(in).exists());
    mnt_ = null;
    updateTxtInfo();

  }

  protected final void updateSave() {
    btOut_.setEnabled(out_.getText().trim().length() > 0 && (btCharger_.isEnabled() || mnt_ != null));

  }

  @Override
  public boolean cancel() {
    stop();
    return super.cancel();
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final Object s = _e.getSource();
    if (s == btCharger_) {
      load();
    } else if (s == btOut_) {
      save();
    } else if (s == btStop_) {
      stop();
    }
  }

  @Override
  public void stop() {
    if (current_ != null) {
      current_.stop();
    }

  }

  void loadMatisseAct(final File _f) {
    final MatisseReader r = new MatisseReader();
    btStop_.setEnabled(true);
    btOut_.setEnabled(false);
    current_ = r;
    progress_.setDesc("BATHYGEO");
    r.setProgressReceiver(progress_);
    // r.setMachineId(NativeBinarySystem.getIdFromName((String) cbFmt_.getSelectedItem()));
    r.setFile(_f);
    final CtuluIOOperationSynthese s = r.read();
    if (s.containsSevereError()) {
      CtuluLibDialog.showError(TrMatisseConvertGUI.this, TrLib.getString("Lecture du fichier BATHYGEO"), s.getAnalyze()
          .getFatalError());
    }
    mnt_ = (GISZone) s.getSource();
    // cbFmt_.setSelectedItem(r.getMachineId());
    updateTxtInfo();
    updateSave();
    progress_.reset();
    btStop_.setEnabled(false);
    btCharger_.setEnabled(false);
    current_ = null;
  }

  private void updateTxtInfo() {
    txtPoints_.setText(mnt_ == null ? CtuluLibString.EMPTY_STRING : Double.toString(mnt_.getNumPoints()));
    txtpolygones_.setText(mnt_ == null ? CtuluLibString.EMPTY_STRING : Double.toString(mnt_.getNumPolyligones()));
    txtpolylignes_.setText(mnt_ == null ? CtuluLibString.EMPTY_STRING : Double.toString(mnt_.getNumPolylignes()));
    txtZones_.setText(CtuluLibString.EMPTY_STRING);
  }
}