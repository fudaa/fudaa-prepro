/*
 * @creation 2 f�vr. 07
 * 
 * @modification $Date: 2007-04-30 14:22:38 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.data;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourcesManager;

/**
 * @author fred deniger
 * @version $Id: TrPostDataCreatedCstTimeSaver.java,v 1.2 2007-04-30 14:22:38 deniger Exp $
 */
public class TrPostDataCreatedCstTimeSaver implements TrPostDataCreatedSaver {

  String shortName_;

  int tidx_;

  public TrPostDataCreatedCstTimeSaver() {
    super();
  }

  @Override
  public Collection<String> getVarNameUsed() {
    return shortName_ == null ? Collections.<String>emptyList() : Arrays.asList(shortName_);
  }

  public TrPostDataCreatedCstTimeSaver(final TrPostDataCreatedCstTime _data) {
    shortName_ = _data.var_.getShortName();
    tidx_ = _data.getTimeIdx();
  }

  @Override
  public TrPostDataCreated restore(H2dVariableType _newVar, final TrPostSource _src, final CtuluUI _ui, final Map _shortName,
      TrPostSourcesManager srcMng, CtuluAnalyze log, double[] savedValues) {
    return TrPostDataCreatedCstTime.createFrom(_newVar, _src, this, _shortName);
  }

  @Override
  public double[] getValuesToPersist(TrPostDataCreated dataCreated) {
    return null;
  }
  
  

}
