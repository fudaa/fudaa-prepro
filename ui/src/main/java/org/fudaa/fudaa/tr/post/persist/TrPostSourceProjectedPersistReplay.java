package org.fudaa.fudaa.tr.post.persist;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourceProjected;

public class TrPostSourceProjectedPersistReplay implements TrPostSourceReplayPersist {

  String id;
  final String refSourceId;

  final String destSourceId;
  final boolean gridEquals;

  /**
   * @param refSourceId
   * @param destSourceId
   * @param gridEquals
   */
  public TrPostSourceProjectedPersistReplay(String refSourceId, String destSourceId, boolean gridEquals) {
    super();
    this.refSourceId = refSourceId;
    this.destSourceId = destSourceId;
    this.gridEquals = gridEquals;
  }

  public String getId() {
    return id;
  }

  @Override
  public TrPostSource replay(TrPostProjet projet, CtuluAnalyze log) {
    TrPostSource init = projet.getSources().getSource(refSourceId);
    TrPostSource dest = projet.getSources().getSource(destSourceId);
    boolean error=false;
    if (init == null) {
      log.addError(TrResource.getS("La source {0} n'a pas �t� trouv�e", refSourceId));
      error=true;
    }
    if (dest == null) {
      log.addError(TrResource.getS("La source {0} n'a pas �t� trouv�e", destSourceId));
      error=true;
    }
    if(error) return null;
    TrPostSourceProjected res = new TrPostSourceProjected(init, dest.getGrid(), dest.getTime().getInitTimeSteps(), gridEquals, projet.getImpl(), this);
    res.setId(id);
    return res;
  }

  public void setId(String id) {
    this.id = id;
  }

}
