package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import java.io.IOException;
import jxl.write.WriteException;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.table.CtuluTableModelInterface;
import org.fudaa.ctulu.table.CtuluWriter;
import org.fudaa.dodico.telemac.io.ScopeGENEFileFormat;
import org.fudaa.dodico.telemac.io.ScopeStructure;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGExportData;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGObject;

/**
 * Writer, passe de la structure Eggraphe a la structure scopeGENE.
 * 
 * @author Adrien Hadoux
 * 
 */
public class TrWriterScopeGENE implements CtuluWriter {

  ScopeGENEFileFormat format_;
  File destination_;
  ScopeStructure.Gene structure_;
  String extension_ = ".scopGENE";
  BuFileFilter filter_;
  
  
  public TrWriterScopeGENE(){
    format_ = ScopeGENEFileFormat.getInstance();
    filter_ = format_.createFileFilter();
  
  }
  
  @Override
  public BuFileFilter getFilter() {
    return filter_;
  }

  @Override
  public String getMostPopularExtension() {
    return extension_;
  }

  @Override
  public void setFile(File _f) {
    destination_ = _f;
    
  }

  @Override
  public void setModel(CtuluTableModelInterface _model) {
    
    structure_ = new ScopeStructure.Gene();
    
    //-- data qui contient les courbes choisies --//
	//--data which contains the choosen plots
    EGExportData data = (EGExportData) _model;
    EGGraphe graphe_ = data.getGraphe();
    EGCourbe[] listeChoixUser = data.getCourbes();
    
    
   
  
    
    //--on ajoute tous les x donn�s de la courbe= UNION DE TOUTES LES COURBES--//
//    final EGAxeHorizontal h = graphe_.getTransformer().getXAxe();
//    for (int i = listeChoixUser.length - 1; i >= 0; i--) {
//      final EGCourbe c = listeChoixUser[i];
//      for (int j = c.getModel().getNbValues() - 1; j >= 0; j--) {
//        final double x = c.getModel().getX(j);
//        // if ( (h.containsPoint(x))) {
//        // -- on ajoute le x pour les variables x si il n existe pas deja --//
//        structure_.insertNewX(x,keyPDt);
//        // }
//      }
//    }
    
    //on recherche toutes les variables pr�sentes dans le graphe
	//we look for all the variables present in the graph
    for (int i = 0; i < graphe_.getModel().getNbEGObject(); i++) {
        final EGObject ci = graphe_.getModel().getEGObject(i);
        if (ci instanceof EGGroup) {
          EGGroup groupe = (EGGroup) ci;
          structure_.addVariable(groupe.getTitle());
         
        }
    }
    
    
    
    
    
    //-- ON Cree un separator pour chaque courbe de chaque variables--//
	//--We create a separator for each curve of each variable
    for (int i = 0; i < graphe_.getModel().getNbEGObject(); i++) {
      final EGObject ci = graphe_.getModel().getEGObject(i);
      if (ci instanceof EGGroup) {
        EGGroup groupe = (EGGroup) ci;
        
        for (int k = 0; k < groupe.getChildCount(); k++) {
          EGCourbeChild courbe = groupe.getCourbeAt(k);
  
            boolean selected = false;
          for (int j = 0; !selected && j < listeChoixUser.length; j++)
            if (listeChoixUser[j] == courbe)
              selected = true;
        
        if (selected) {
          
        	//-- on ajoute un pas de temps qui decrit le graphe --//
			//--we add a  foot which describes the graph
            String keyPDt="X= "+groupe.getTitle()+": "+courbe.getTitle();
            structure_.addSeparator(keyPDt);
        	
        	          
            // --ajout d toutes les valeurs X et Y--/
            //--add d all the values X and Y
            for(int g = 0; g < courbe.getModel().getNbValues(); g++) {
            
            	//on ajoute le x
            	//we add the x
            	structure_.insertNewX(courbe.getModel().getX(g), keyPDt);
            	//on ajoute le y associ� a la bonne variable
            	int variableEnCours=structure_.getVariablePosition(groupe.getTitle());
            	structure_.addValueForVariableAtSeparator(courbe.getModel().getY(g),variableEnCours , keyPDt);
          
            	
//            	//-- on ajoute la valeur interpol�e pour les x pr�cemment saisis --//
//              double x = structure_.getX(g,keyPDt);
//              
//              //-- on v�rifie que le x existe pour la courbe actuelle --//
//              boolean xExistForCourbe = false;
//              int emplacementYcorrespondant = -1;
//              for (int l = 0; !xExistForCourbe && l < courbe.getModel().getNbValues(); l++) {
//                if (courbe.getModel().getX(l) == x) {
//                  xExistForCourbe = true;
//                emplacementYcorrespondant = l;
//                }
//              }
              
            //double yInterpole = courbe.interpol(x);
              
              // -- si le x existe dans la courbe alors on inscrit son y
              // correspondant pour la bonne variable--//
//              if (xExistForCourbe)
//                structure_.addValueForVariableAtSeparator(courbe.getModel().getY(emplacementYcorrespondant), structure_.getNbVariables()-1, keyPDt);
//              else
//                // -- on enregistre une valeure foireuse.
//                structure_.addValueForVariableAtSeparator(ScopeKeyWord.VALUE_UNDEFINED, structure_.getNbVariables()-1, keyPDt);
          }
          
        }
        }
        
        
        
      }

    }
    
    
   
  }

  @Override
  public void write(ProgressionInterface _p) throws IOException, WriteException {
    format_.getLastVersionInstance(null).write(destination_, structure_, null);
    
  }

  @Override
  public boolean allowFormatData() {
    return false;
  }
  
}
