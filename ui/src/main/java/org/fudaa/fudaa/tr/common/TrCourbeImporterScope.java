package org.fudaa.fudaa.tr.common;

import java.io.File;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.telemac.io.ScopeStructure;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGCourbeModelDefault;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheTreeModel;
import org.fudaa.ebli.courbe.EGGroup;

/**
 * Gere l'importation de donn�es scope s ou t et cr�e un graphe temporel ou spatial selon le format
 * Manage the importation of datas scope s or t and create a temporaly or spatial graph according to the format
 * S'utilise avec le wizard FudaaCourbeWizardImportScope
 * Is used with the wizard FudaaCourbeWizardImportScope 
 * @author Adrien Hadoux
 */
public class TrCourbeImporterScope {

	
	/**
	 * Action qui genere un wizard pour le parametrage des structures import�es.
	 * Action which generates a  wizard  for the parametric of the parametrising imported structures 
	 * @author Adrien Hadoux
	 *
	 */
	
	public static void createGraphe(File fichier,TrCourbeImporter.Target target,ScopeStructure _data, CtuluCommandManager _mng,ProgressionInterface _prog,boolean nuagePoints){
		
		
		createGraphe(target,_data.returnEvolReguliere(fichier), _mng, _prog,nuagePoints);
	}
	
	public static void createGraphe(TrCourbeImporter.Target target,EvolutionReguliere[] _data, CtuluCommandManager _mng,ProgressionInterface _prog,boolean nuagePoints){
		
		//-- on indique si il faut prendre en compte comme �tant un nuage de points --//
		//We indicate if we must take into account as being a cloud of points 
		if(nuagePoints){
			for(int i=0;i<_data.length;i++){
				_data[i].setNuagePoints_(nuagePoints);
			}
		}
		
		 target.importCourbes(_data, _mng, _prog);
	}
	

	
	/**
	 * genere un graphe spatial
	 * to generate a spatial graph 
	 * @param data_
	 * @return
	 */
	public static void createGrapheSpatialGrapheOLD(TrCourbeImporter.Target target,ScopeStructure.SorT data_,ProgressionInterface prog){
		
		EGGrapheTreeModel model=new EGGrapheTreeModel();
		double[] tabX=new double[data_.getAllX().size()];
		for(int i=0;i<data_.getAllX().size();i++)
			tabX[i]=data_.getX(i);
		
		
		for(int i=0;i<data_.getNbVariables();i++){
			
			EGGroup varg=new EGGroup();
			varg.setTitle(data_.getVariable(i));
			model.add(varg);
			
			//--remplissage de valeurs --//
			//Allocating values
			//tableau des y
			//picture of y
			double[] tabY=new double[tabX.length];
			
			for(int j=0;j<tabX.length;j++)
				tabY[j]=data_.getValueOfvariable(i, j);
			EGCourbeModelDefault modelCourbe=new EGCourbeModelDefault(tabX,tabY);
			EGCourbeChild courbe=new EGCourbeChild(varg,modelCourbe);
			
			varg.addEGComponent(courbe);
			
		}
		
		
		EGGraphe graphe=new EGGraphe(model);
		
		
		
	}

}
