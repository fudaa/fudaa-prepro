/**
 *  @creation     28 mai 2004
 *  @modification $Date: 2006-12-05 10:18:20 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr;

/**
 * @author Fred Deniger
 * @version $Id: TrEditor.java,v 1.11 2006-12-05 10:18:20 deniger Exp $
 */
public final class TrEditor {

  private TrEditor() {

  }

  /**
   * @param _args les arguments normaux de fudaa + le fichier a ouvrir si necessaire.
   */
  public static void main(final String[] _args) {
    TrInvocationHandler.initThreaExceptionHandler();
    TrLauncherDefault.init();
    TrLauncherDefault.launch(_args, null, new TrEditorImplementation());
  }

}