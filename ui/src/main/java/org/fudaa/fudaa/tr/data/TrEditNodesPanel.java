package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.gui.CtuluAnalyzeGUI;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.operation.EfIndexHelper;
import org.fudaa.dodico.ef.operation.translate.EfStructureAllGridValidator;
import org.fudaa.dodico.ef.operation.translate.TranslatePointValidator;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.jdesktop.swingx.JXTreeTable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TrEditNodesPanel extends CtuluDialogPanel {
  /**
   * We build the panel containing the X,Y and bathymetry values.
   *
   * @param visuPanelEditor
   * @param bathyModel
   * @param undefinedValue
   */
  public static TrEditNodesPanel build(TrVisuPanelEditor visuPanelEditor, Data data,
                                       final CtuluCollectionDoubleEdit bathyModel, final Double undefinedValue) {
    final TrEditNodesPanel panel = new TrEditNodesPanel(visuPanelEditor);
    final EfGridInterface grid = visuPanelEditor.getGrid();
    panel.xyEditor = new CtuluValueEditorDouble();
    panel.bathyModel = bathyModel;
    boolean useBathymetry = bathyModel != null;
    if (useBathymetry) {
      panel.bathymetryEditor = new CtuluValueEditorDouble();
      panel.bathymetryEditor.setFormatter(visuPanelEditor.getParams().getNodalData().getFormater(H2dVariableType.BATHYMETRIE));
    }

    panel.data = data;
    final int nbPointToEdit = data.getNbSelectedPoint();
    panel.xEditors = new JComponent[nbPointToEdit];
    panel.yEditors = new JComponent[nbPointToEdit];
    if (useBathymetry) {
      panel.zEditors = new JComponent[nbPointToEdit];
    }
    int nbColumn = 3;
    if (useBathymetry) {
      nbColumn++;
      if (undefinedValue != null) {
        nbColumn++;
      }
    }
    boolean useHorizontalLayout = nbPointToEdit == 1;
    if (useHorizontalLayout) {
      nbColumn = 2;
    }
    panel.setLayout(new BuGridLayout(nbColumn, 5, 5));
    if (!useHorizontalLayout) {
      panel.addLabel("Index");
      panel.addLabel("X");
      panel.addLabel("Y");
      if (useBathymetry) {
        panel.addLabel(H2dVariableType.BATHYMETRIE.getName());
        if (undefinedValue != null) {
          panel.addLabel("");
        }
      }
    }
    for (int i = 0; i < nbPointToEdit; i++) {
      int pointIndex = data.selectedPoint[i];
      JComponent xEditor = panel.xyEditor.createEditorComponent();
      JComponent yEditor = panel.xyEditor.createEditorComponent();
      double oldX = data.oldX[i];
      double oldY = data.oldY[i];
      xEditor.setToolTipText(TrResource.getS("Valeur initiale: {0}", Double.toString(oldX)));
      yEditor.setToolTipText(TrResource.getS("Valeur initiale: {0}", Double.toString(oldY)));
      panel.xyEditor.setValue(data.newX[i], xEditor);
      panel.xyEditor.setValue(data.newY[i], yEditor);
      panel.xEditors[i] = xEditor;
      panel.yEditors[i] = yEditor;
      if (useHorizontalLayout) {
        panel.addLabel("Index: ");
      }
      panel.addLabel(CtuluLibString.getString(pointIndex + 1));
      if (useHorizontalLayout) {
        panel.addLabel("X: ");
      }
      panel.add(xEditor);
      if (useHorizontalLayout) {
        panel.addLabel("Y: ");
      }
      panel.add(yEditor);
      if (useBathymetry) {
        final JComponent zEditor = panel.bathymetryEditor.createEditorComponent();
        final double oldZ = data.oldZ[i];
        panel.bathymetryEditor.setValue(data.newZ[i], zEditor);
        zEditor.setToolTipText(TrResource.getS("Valeur initiale: {0}", Double.toString(oldZ)));
        panel.zEditors[i] = zEditor;
        if (useHorizontalLayout) {
          panel.addLabel(H2dVariableType.BATHYMETRIE.getName() + ": ");
        }
        panel.add(zEditor);
        if (undefinedValue != null) {
          JButton bt = new JButton(TrResource.getS("Utiliser la valeur ind�finie"));
          bt.setToolTipText(TrResource.getS("Remplace la valeur de la bathym�trie par {0}", undefinedValue.toString()));
          bt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              panel.bathymetryEditor.setValue(undefinedValue.doubleValue(), zEditor);
            }
          });
          if (useHorizontalLayout) {
            panel.addLabel("");
          }
          panel.add(bt);
        }
      }
    }
    return panel;
  }

  private final TrVisuPanelEditor visuPanelEditor;

  private TrEditNodesPanel(TrVisuPanelEditor visuPanelEditor) {

    this.visuPanelEditor = visuPanelEditor;
    setBorder(BuBorders.EMPTY3333);
  }

  private Data data;
  private CtuluCollectionDoubleEdit bathyModel;
  private JComponent[] xEditors;
  private JComponent[] yEditors;
  private JComponent[] zEditors;
  private CtuluValueEditorDouble xyEditor;
  private CtuluValueEditorDouble bathymetryEditor;

  /**
   * Apply modifications on grid
   *
   * @param cmd
   */
  public void checkAndApplyModifications(final CtuluCommandContainer cmd) {
    final CtuluCommandComposite ctuluCommandComposite = new CtuluCommandComposite();

    cmd.addCmd(ctuluCommandComposite);
    final CtuluTaskDelegate task = visuPanelEditor.getImpl().createTask(TrResource.getS("Validation des modifications"));
    final ProgressionInterface prog = task.getStateReceiver();
    task.start(new Runnable() {
      @Override
      public void run() {

        //Fill the arrays
        final EfGridInterface grid = visuPanelEditor.getGrid();
        for (int i = 0; i < data.oldX.length; i++) {
          double newX = (Double) xyEditor.getValue(xEditors[i]);
          double newY = (Double) xyEditor.getValue(yEditors[i]);
          CtuluListSelection selection = EfIndexHelper.getNearestNode(grid, newX, newY, 0.01, prog);
          if (selection != null && selection.isOnlyOnIndexSelected()) {
            newX = grid.getPtX(selection.getMinIndex());
            newY = grid.getPtY(selection.getMinIndex());
          }
          data.newX[i] = newX;
          data.newY[i] = newY;

          if (data.newZ != null) {
            data.newZ[i] = (Double) bathymetryEditor.getValue(zEditors[i]);
          }
        }

        TranslatePointValidator validator = new TranslatePointValidator(grid, TrEditNodesPanel.this.data.selectedPoint, data.newX, data.newY);
        final CtuluLog ctuluLog = validator.canTranslate(prog);
        if (ctuluLog.isNotEmpty()) {
          visuPanelEditor.getImpl().manageAnalyzeAndIsFatal(ctuluLog);
        } else {
          final CtuluLogGroup ctuluLogClean = validator.shouldBeCleaned(prog);
          final boolean isGridInvalid = ctuluLogClean.containsSomething();
          if (isGridInvalid) {
            CtuluDialogPanel panel = new CtuluDialogPanel();
            panel.setErrorText(TrLib
                .getString("Les modifications apport�es vont modifier la structure du maillage. Vous ne pourrez pas sauvegarder le projet par la suite mais seulement l'exporter"));
            panel.setLayout(new BuBorderLayout(5, 5));
            final JXTreeTable logTable = CtuluAnalyzeGUI.createLogTable(ctuluLogClean, visuPanelEditor.getCtuluUI(), false);
            logTable.expandAll();
            logTable.packAll();
            panel.add(new JScrollPane(logTable));
            panel.add(new JLabel(
                "<html><body>" + TrLib.getString("En cliquant sur Ok, vous ne pourrez pas sauvegarder le projet et seulement l'exporter")
                    + "<br>" + TrLib.getString("Il est possible de retester la structure avec l'action \"Tester la structure du maillage\"")
                    + "</body></html>"
            ), BorderLayout.SOUTH);
            if (!CtuluDialogPanel.isOkResponse(panel.afficheModale(visuPanelEditor.getCtuluUI().getParentComponent(), TrLib.getString("Structure du maillage modifi�e")))) {
              return;
            }
          }
          apply(ctuluCommandComposite, data);
          boolean wasInvalid = visuPanelEditor.getParams().isGridInvalid();
          if (isGridInvalid) {
            if (!wasInvalid) {
              TrValidGridAction.setGridInvalid(visuPanelEditor, true);
              ctuluCommandComposite.addCmd(new CtuluCommand() {
                @Override
                public void undo() {
                  TrValidGridAction.setGridInvalid(visuPanelEditor, false);
                }

                @Override
                public void redo() {
                  TrValidGridAction.setGridInvalid(visuPanelEditor, true);
                }
              });
            }
          } else if (wasInvalid) {
            EfStructureAllGridValidator wholeGridValidator = new EfStructureAllGridValidator(visuPanelEditor.getGrid());
            final CtuluLogGroup ctuluLogGroup = wholeGridValidator.isValid(prog);
            if (!ctuluLogGroup.containsSomething()) {
              TrValidGridAction.setGridInvalid(visuPanelEditor, false);
              ctuluCommandComposite.addCmd(new CtuluCommand() {
                @Override
                public void undo() {
                  TrValidGridAction.setGridInvalid(visuPanelEditor, true);
                }

                @Override
                public void redo() {
                  TrValidGridAction.setGridInvalid(visuPanelEditor, false);
                }
              });
            }
          }
        }
      }
    });
  }

  public void apply(CtuluCommandContainer cmd, final Data data) {
    //Apply modifications
    CtuluCommandComposite ctuluCommandComposite = new CtuluCommandComposite();
    if (bathymetryEditor != null) {
      bathyModel.set(this.data.selectedPoint, data.newZ, ctuluCommandComposite);
    }
    modifyXY(this.data.selectedPoint, data.newX, data.newY, data.newZ);
    ctuluCommandComposite.addCmd(new CtuluCommand() {
      @Override
      public void undo() {
        modifyXY(TrEditNodesPanel.this.data.selectedPoint, data.oldX, data.oldY, data.oldZ);
      }

      @Override
      public void redo() {
        modifyXY(TrEditNodesPanel.this.data.selectedPoint, data.newX, data.newY, data.newZ);
      }
    });
    if (cmd != null && !ctuluCommandComposite.isEmpty()) {
      cmd.addCmd(ctuluCommandComposite.getSimplify());
    }
  }

  private void modifyXY(int[] selectedPt, double[] xValues, double[] yValues, double[] zValues) {
    for (int i = 0; i < selectedPt.length; i++) {
      visuPanelEditor.getGrid().setPt(selectedPt[i], xValues[i], yValues[i], zValues[i]);
    }
    EfLib.orienteGrid(visuPanelEditor.getGrid(),null,true,null);
    visuPanelEditor.getGrid().createIndexRegular(null);

    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        visuPanelEditor.updateInfoAndIso();
        visuPanelEditor.fireGridPointModified();
        visuPanelEditor.getGridGroup().getPointLayer().repaint();
      }
    });
  }

  public static class Data {
    public int getNbSelectedPoint() {
      return selectedPoint.length;
    }

    public final int[] selectedPoint;
    public final double[] oldX;
    public final double[] oldY;
    public final double[] oldZ;
    //new Values
    public final double[] newX;
    public final double[] newY;
    public final double[] newZ;

    public Data(int[] selectedPoint) {
      this.selectedPoint = selectedPoint;
      oldX = new double[selectedPoint.length];
      oldY = new double[selectedPoint.length];
      oldZ = new double[selectedPoint.length];
      newX = new double[selectedPoint.length];
      newY = new double[selectedPoint.length];
      newZ = new double[selectedPoint.length];
    }
  }
}
