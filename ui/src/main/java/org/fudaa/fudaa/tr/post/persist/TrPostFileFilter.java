package org.fudaa.fudaa.tr.post.persist;

import com.memoire.bu.BuFileFilter;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Filter pour le projet trpost
 *
 * @author Adrien Hadoux
 */
public class TrPostFileFilter extends BuFileFilter {
  /**
   * Utilis� pour l'extension de tous les fichiers, repertoire et ficheirs inclus.
   */
  public final static String DEFAULT_EXTENSION = "POST";

  /**
   * Filtre utilis� pour les chooser de selection de projet .POST.
   *
   * @author Adrien Hadoux
   */
  public static class DirectoryPOSTFilter extends FileFilter {
    @Override
    public boolean accept(File f) {
      if (f.isDirectory()) {
        return true;
      }
      // TODO Auto-generated method stub

      String s = f.getName();

      if (s.contains("." + DEFAULT_EXTENSION)) {
        return true;
      }

      return false;
    }

    @Override
    public String getDescription() {
      // TODO Auto-generated method stub
      return DEFAULT_EXTENSION;
    }
  }

  public TrPostFileFilter(final TrPostFileFormat _ft) {
    super(new String[]{DEFAULT_EXTENSION}, _ft.getName());
  }

  @Override
  public boolean accept(final File _d, final String _fn) {
    if (_d == null) {
      return accept(_fn);
    }
    return accept(_d.getAbsolutePath());
  }

  @Override
  public boolean accept(final String _f) {
    return _f != null && TrPostPersistenceFileFinder.MAIN_SETUP_FILENAME.equals(_f);
  }

  @Override
  public boolean accept(final File file) {
    return file != null && TrPostPersistenceFileFinder.MAIN_SETUP_FILENAME.equals(file.getName());
  }

  /**
   * Indique si le repertoire concern� est bien un r�pertoire de type POST
   *
   * @param _f
   * @author Adrien Hadoux
   */
  public static boolean acceptDirectoryPOST(final File _f) {

    if (_f == null) {
      return false;
    }
    if (!_f.isDirectory() || !_f.getName().contains("." + DEFAULT_EXTENSION)) {
      return false;
    }

    //-- test du contenu si il est bien form�: --//
    File[] children = _f.listFiles();

    boolean setupFound = false;
    for (int i = 0; !setupFound && i < children.length; i++) {
      if (children[i] == null) {
        setupFound = false;
      } else {
        final String name = children[i].getName();
        if (name == null) {
          setupFound = false;
        } else if (name.equals(TrPostPersistenceFileFinder.MAIN_SETUP_FILENAME)) {
          setupFound = true;
        }
      }
    }

    return setupFound;
  }

  /**
   * Recupere le fichier setup depuis le un r�pertoire de type POST
   *
   * @param _f
   * @author Adrien Hadoux
   */
  public static File getSetupFormDirectoryPOST(final File _f) {
    File[] children = _f.listFiles();
    for (int i = 0; i < children.length; i++) {
      if (children[i] == null) {
      } else {
        final String name = children[i].getName();
        if (name == null) {
        } else if (name.equals(TrPostPersistenceFileFinder.MAIN_SETUP_FILENAME)) {
          return children[i];
        }
      }
    }
    return null;
  }
}
