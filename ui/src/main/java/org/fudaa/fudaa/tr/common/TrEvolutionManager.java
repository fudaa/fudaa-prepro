/**
 * @creation 1 sept. 2004
 * @modification $Date: 2007-04-26 14:38:21 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuComboBox;
import org.apache.commons.collections.CollectionUtils;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.courbe.*;
import org.fudaa.ebli.palette.BPalettePlageDefault;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeCreatorPanel;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeModel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: TrEvolutionManager.java,v 1.25 2007-04-26 14:38:21 deniger Exp $
 */
public abstract class TrEvolutionManager extends EGGrapheSimpleModel implements TrCourbeImporter.Target {
  private final class EvolutionComboBoxModel extends AbstractListModel implements ComboBoxModel {
    Object first_;
    Object selected_;

    EvolutionComboBoxModel(final Object _first) {
      first_ = _first;
    }

    @Override
    public Object getElementAt(final int _index) {
      if (first_ != null) {
        if (_index == 0) {
          return first_;
        }
        return getEvol(_index - 1);
      }
      return getEvol(_index);
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public int getSize() {
      return first_ == null ? getNbEGObject() : getNbEGObject() + 1;
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != selected_) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }
  }

  transient EGAxeVertical axeV_;

  private void createCourbe(final CtuluCommandManager _mng, final Component _parent, final double _xmin, final double _xmax, final EGGraphe _dest) {
    final FudaaCourbeCreatorPanel pn = new FudaaCourbeCreatorPanel(getDefaultNom(), _xmin, _xmax, _dest, null);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_parent))) {
      ajouteCourbe(pn.getEvol(), _mng);
    }
  }

  private EGCourbeSimple createEGCourbe(final EvolutionReguliere _e) {
    if (axeV_ == null) {
      axeV_ = buildAxeV();
    }
    return createEGCourbe(_e, axeV_);
  }

  private EGCourbeSimple createEGCourbe(final EvolutionReguliere _e, final EGAxeVertical _v) {
    final FudaaCourbeModel m = new FudaaCourbeModel(_e);
    testEvolutionAdded(_e);
    final EGCourbeSimple c = new EGCourbeSimple(_v, m);

    //-- on met a jour si la courbe est un nuage de points ou non --//
    //--We update if the curve is a  scatter of points or not
    c.setNuagePoints(_e.isNuagePoints());

    // c.setTitre(m.e_.getNom());
    c.setAspectContour(BPalettePlageDefault.getColor(getNbEGObject()));
    return c;
  }

  protected void ajouteCourbe(final EvolutionReguliere _e, final CtuluCommandManager _cmd) {
    if (axeV_ == null) {
      axeV_ = buildAxeV();
    }
    final EGCourbeSimple r = createEGCourbe(_e, axeV_);
    super.internalAddElement(r);
    super.createCmdForCourbeAdd(r, _cmd, -1);
  }

  protected EGAxeVertical buildAxeV() {
    return new EGAxeVertical();
  }

  @Override
  protected EGCourbeSimple doDuplicateCourbe(final EGCourbe _c) {
    final EvolutionReguliere m = ((FudaaCourbeModel) _c.getModel()).getEvolution();
    final EvolutionReguliere r = new EvolutionReguliere(m);
    r.setNom(getDefaultNom());
    return createEGCourbe(r);
  }

  protected String getDefaultNom() {
    return TrResource.getS("courbe") + CtuluLibString.ESPACE + getNbEvol();
  }

  public EvolutionReguliereInterface getEvol(final int _i) {

    EGCourbe egCourbe = (EGCourbe) element_.get(_i);
    return ((FudaaCourbeModel) egCourbe.getModel()).getEvolution();
  }

  protected void testEvolutionAdded(final EvolutionReguliereInterface _e) {
    if (!_e.isNomSet()) {
      _e.setNom(FudaaLib.getS("Inconnu"));
    }
  }

  public void add(final EvolutionReguliereInterface[] _evol, final CtuluCommandManager _mng) {
    if (_evol != null) {
      final EGCourbeSimple[] cs = new EGCourbeSimple[_evol.length];
      for (int i = _evol.length - 1; i >= 0; i--) {
        cs[i] = createEGCourbe(new EvolutionReguliere(_evol[i]));
      }
      super.addCourbes(cs, _mng);
    }
    internFireStructureChanged();
  }

  @Override
  public void addNewCourbe(final CtuluCommandManager _cmd, final Component _p, final EGGraphe _graphe) {
    double min = 0;
    double max = 1;
    final int n = getNbEGObject();
    if (n > 0) {
      EGModel c = ((EGCourbe) element_.get(0)).getModel();
      min = c.getXMin();
      max = c.getXMax();
      for (int i = getNbEGObject() - 1; i > 0; i--) {
        c = ((EGCourbe) element_.get(i)).getModel();
        final double minT = c.getXMin();
        final double maxT = c.getXMax();
        if (minT < min) {
          min = minT;
        }
        if (maxT > max) {
          max = maxT;
        }
      }
    }
    createCourbe(_cmd, _p, min, max, _graphe);
  }

  /**
   * @param _first le premier objet a utiliser dans la liste / the first thing to use in the list
   * @return le combobox qui va bien / the combat which fits well
   */
  public final JComboBox buildCombobox(final Object _first) {
    final JComboBox r = new BuComboBox();
    r.setEditable(false);
    r.setModel(new EvolutionComboBoxModel(_first));
    TrCourbeComboBoxRenderer aRenderer = new TrCourbeComboBoxRenderer();
    aRenderer.setFirstSelectionIsEmpty(getNbEGObject() > 0);
    r.setRenderer(aRenderer);
    return r;
  }

  /**
   * @param _e l'evolution a tester / the evolution to test
   * @return l'indice de la courbe utilisant cette evolution / the indice of the curve using this evolution
   */
  public int getCourbeWithEvol(final EvolutionReguliereInterface _e) {
    if (element_ != null) {
      for (int i = getNbEGObject() - 1; i >= 0; i--) {
        if (getEvol(i) == _e) {
          return i;
        }
      }
    }
    return -1;
  }

  /**
   * @return le nombre d'evolution / the evolution number
   */
  public final int getNbEvol() {
    return super.getNbEGObject();
  }

  @Override
  public void importCourbes(final EvolutionReguliereInterface[] _crb, final CtuluCommandManager _mng, final ProgressionInterface _prog) {
    add(_crb, _mng);
  }

  /**
   * @param _e les courbes a ajouter / the plots to add
   */
  public final void initWithCurves(final Set _e) {

    axeV_ = buildAxeV();
    if (_e != null) {
      for (final Iterator it = _e.iterator(); it.hasNext(); ) {
        super.internalAddElement(createEGCourbe((EvolutionReguliere) it.next(), axeV_));
      }
    }
    if (CollectionUtils.isEmpty(_e)) {
      EvolutionReguliere empty = new EvolutionReguliere();
      empty.setNom(CtuluResource.CTULU.getString("Vide"));
      super.internalAddElement(createEGCourbe(empty, axeV_));
    }
    if (element_ != null) {
      fireIntervalAdded(this, 0, element_.size());
    }
  }

  /**
   * Enleve les evolutions utilisees. Take off the evolutions used
   */
  public final void removeUsedEvolution() {
    super.selection_.clearSelection();
    final List toRemove = new ArrayList();

    if (element_ != null) {
      final int oldSize = element_.size();
      for (int i = element_.size() - 1; i >= 0; i--) {
        if (getEvol(i).isUsed()) {
          toRemove.add(element_.get(i));
        }
      }
      element_.removeAll(toRemove);
      fireIntervalRemoved(this, 0, oldSize);
    }
  }
}
