/**
 * @file TrRefluxAppliManager.java
 * @creation 11 mars 2004
 * @modification $Date: 2006-12-22 14:38:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.bu.BuMenu;
import com.memoire.bu.BuResource;
import java.io.File;
import javax.swing.Icon;
import javax.swing.JPopupMenu;
import org.fudaa.dodico.calcul.CalculLauncher;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFilter;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.reflux.io.INPFileFormat;
import org.fudaa.fudaa.commun.FudaaUI;
import org.fudaa.fudaa.commun.exec.FudaaExec;
import org.fudaa.fudaa.tr.common.TrApplicationManager;
import org.fudaa.fudaa.tr.common.TrExplorer;
import org.fudaa.fudaa.tr.common.TrLauncher;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $id$
 */
public class TrRefluxAppliManager extends TrApplicationManager {

  private class FudaaExecLaunchReflux extends FudaaExec {

    /**
     * Executer reflux.
     */
    public FudaaExecLaunchReflux() {
      super(TrResource.getS("Ex�cuter Reflux"));
    }

    @Override
    public void execInDir(final File _dir, final FudaaUI _ui, final Runnable _nexProcess) {}

    @Override
    public void execOnFile(final File _target, final FudaaUI _ui, final Runnable _nexProcess) {
      final CalculLauncher l = TrRefluxProjet.launchCalcul(_ui, _target, getLauncher());
      if (l != null) {
        l.execute();
      }
    }

    @Override
    public Icon getIcon() {
      return BuResource.BU.getIcon("executer");
    }
  }

  TrExplorer.ExplorerMenuItem launchReflux_;

  /**
   * @param _l
   */
  public TrRefluxAppliManager(final TrLauncher _l) {
    super(_l);
    getBathygeoAnalyzeExec();
  }

  @Override
  protected void buildCmdForMenuFileOpenWith(final BuMenu _m, final TrExplorer _explor) {
    super.buildCmdForMenuFileOpenWith(_m, _explor);
    _m.addSeparator();
    _m.add(_explor.createFileAction(new FudaaExecCreateProjectH2d(TrResource.getS("Cr�er projet Reflux"), launcher_)));
  }

  protected TrLauncher getLauncher() {
    return launcher_;
  }

  @Override
  public void buildCmdForMenuFile(final JPopupMenu _m, final TrExplorer _explor) {
    newProject_ = _explor.createFileAction(new FudaaExecCreateProjectH2d(TrResource.getS("Cr�er projet Reflux"),
        launcher_));
    launchReflux_ = _explor.createFileAction(new FudaaExecLaunchReflux());
    _m.add(newProject_);
    _m.add(launchReflux_);
    postItem_ = _explor.createFileAction(new FudaaExecPostView(launcher_));
    _m.add(postItem_);
    super.buildCmdForMenuFile(_m, _explor);
  }

  @Override
  public FudaaExec[] getActionForFile(final File _f) {
    final SerafinFileFilter ft = (SerafinFileFilter) getFileFilterFor(SerafinFileFormat.getInstance());
    if (ft.accept(_f)) {
      if (ft.isResFile(_f)) { return new FudaaExec[] { meshExec_ }; }
      return new FudaaExec[] { postExec_ };
    } else if (getFileFilterFor(INPFileFormat.getInstance()).accept(_f)) { return new FudaaExec[] { launchReflux_
        .getExec() }; }
    return null;
  }

  @Override
  public String getSoftId() {
    return TrRefluxImplHelper.getID();
  }

  @Override
  public void updateMenuFiles(final int _nbFileChoosen, final File _f) {
    super.updateMenuFiles(_nbFileChoosen, _f);
    if (_f == null) {
      newProject_.setEnabled(false);
      launchReflux_.setEnabled(false);
      return;
    }
    final String n = _f.getName();
    newProject_.setEnabled((_nbFileChoosen == 1) && (isGridFormat(_f)));
    launchReflux_.setEnabled((_nbFileChoosen == 1) && (launcher_.getFileFormatManager().isRefluxFile(n)));
  }
}