/*
 *  @creation     20 avr. 2005
 *  @modification $Date: 2007-06-28 09:28:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuTable;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleAbstract;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataAdapter;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;
import org.fudaa.dodico.ef.operation.EfIndexVisitorNearestElt;
import org.fudaa.dodico.ef.operation.EfIndexVisitorNearestNode;
import org.fudaa.dodico.h2d.H2dSiSourceInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueFleche;
import org.fudaa.ebli.calque.ZModeleFleche;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author Fred Deniger
 * @version $Id: TrSiFlecheModel.java,v 1.16 2007-06-28 09:28:18 deniger Exp $
 */
public class TrSiFlecheModel implements ZModeleFleche {

  H2dSiSourceInterface src_;

  /**
   * @param _src la source du modele
   */
  public TrSiFlecheModel(final H2dSiSourceInterface _src) {
    src_ = _src;
  }

  private static class DoubleCollectionAdapter extends CtuluCollectionDoubleAbstract {

    final ZModeleFleche modele_;
    final boolean isVy_;

    public DoubleCollectionAdapter(final ZModeleFleche _modele, final boolean _isVy) {
      modele_ = _modele;
      isVy_ = _isVy;
    }

    @Override
    public int getSize() {
      return modele_.getNombre();
    }

    @Override
    public double getValue(final int _i) {
      return isVy_ ? modele_.getVy(_i) : modele_.getVx(_i);
    }

  }

  @Override
  public boolean interpolate(final GrSegment _seg, final double _x, final double _y) {
    return interpolate(_seg, _x, _y, this, src_.getMaillage());
  }
  
  @Override
  public void prepare() {
  }


  public static boolean interpolate(final GrSegment _seg, final double _x, final double _y, final ZModeleFleche _model,
      final EfGridInterface _grid) {
    final EfIndexVisitorNearestElt visitor = new EfIndexVisitorNearestElt(_grid, _x, _y, 0);
    visitor.setOnlyInSearch(true);
    if (_grid.getIndex() == null) return false;
    _grid.getIndex().query(EfIndexVisitorNearestNode.getEnvelope(_x, _y, 0), visitor);
    if (visitor.isIn()) {
      final int idxElt = visitor.getSelected();
      final EfData vxData = new EfDataAdapter(new DoubleCollectionAdapter(_model, false), false);
      final EfData vyData = new EfDataAdapter(new DoubleCollectionAdapter(_model, true), false);
      // interpolation sur la norme et l'ange puis retransposition en vx,vy
      final double vx = EfGridDataInterpolator.interpolate(idxElt, _x, _y, vxData, vyData, true, _grid);
      final double vy = EfGridDataInterpolator.interpolate(idxElt, _x, _y, vxData, vyData, false, _grid);
      _seg.e_.x_ = _x + vx;
      _seg.e_.y_ = _y + vy;
      return true;
    }
    return false;
  }

  @Override
  public double getNorme(final int _i) {
    final double vx = getVx(_i);
    final double vy = getVy(_i);
    return Math.hypot(vx, vy);
  }

  @Override
  public double getVx(final int _i) {
    final CtuluCollectionDouble u = src_.getModifiableModel(H2dVariableType.VITESSE_U);
    if (u == null) { return 0; }
    return u.getValue(_i);
  }

  @Override
  public double getVy(final int _i) {
    final CtuluCollectionDouble v = src_.getModifiableModel(H2dVariableType.VITESSE_V);
    if (v == null) { return 0; }
    return v.getValue(_i);
  }

  @Override
  public double getX(final int _i) {
    return src_.getMaillage().getPtX(_i);
  }

  @Override
  public double getY(final int _i) {
    return src_.getMaillage().getPtY(_i);
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
    final ZCalqueFleche.StringInfo info = new ZCalqueFleche.StringInfo();
    info.norme_ = H2dResource.getS("Vitesse");
    info.vx_ = H2dResource.getS("Vitesse selon X");
    info.vy_ = H2dResource.getS("Vitesse selon Y");
    info.titleIfOne_ = FudaaLib.getS("Noeud n�");
    info.title_ = FudaaLib.getS("Noeuds");
    ZCalqueFleche.fillWithInfo(_d, _layer.getLayerSelection(), this, info);
  }

  @Override
  public double getZ1(final int _i) {
    return 0;
  }

  @Override
  public double getZ2(final int _i) {
    return 0;
  }
  
  

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    final BuTable res = new CtuluTable();
    res.setModel(new ZCalqueFleche.ValueTableModel(this) {

      @Override
      public String getColumnName(final int _column) {
        if (_column == 0) { return EbliLib.getS("Indices"); }
        if (_column == 1) { return "X"; }
        // Y
        if (_column == 2) { return "Y"; }
        if (_column == 3) { return H2dResource.getS("Vitesse"); }
        if (_column == 4) { return H2dResource.getS("Vitesse selon X"); }
        if (_column == 5) { return H2dResource.getS("Vitesse selon Y"); }
        return CtuluLibString.EMPTY_STRING;
      }
    });
    return res;
  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  @Override
  public GrBoite getDomaine() {
    final EfGridInterface g = src_.getMaillage();
    final GrBoite b = new GrBoite();
    b.o_ = new GrPoint(g.getMinX(), g.getMinY(), 0);
    b.e_ = new GrPoint(g.getMaxX(), g.getMaxY(), 0);
    return b;
  }

  @Override
  public int getNombre() {
    final CtuluCollectionDouble u = src_.getModifiableModel(H2dVariableType.VITESSE_U);
    return u == null ? 0 : src_.getMaillage().getPtsNb();
  }

  @Override
  public Object getObject(final int _ind) {
    return null;
  }

  @Override
  public boolean segment(final GrSegment _s, final int _i, final boolean _force) {
    final CtuluCollectionDouble u = src_.getModifiableModel(H2dVariableType.VITESSE_U);
    if (u == null) { return false; }
    final CtuluCollectionDouble v = src_.getModifiableModel(H2dVariableType.VITESSE_V);
    if (v == null) { return false; }
    _s.o_.x_ = src_.getMaillage().getPtX(_i);
    _s.o_.y_ = src_.getMaillage().getPtY(_i);
    _s.e_.x_ = _s.o_.x_ + u.getValue(_i);
    _s.e_.y_ = _s.o_.y_ + v.getValue(_i);
    return true;
  }

  @Override
  public GrPoint getVertexForObject(int ind, int idVertex) {
    return new GrPoint(getX(ind),getY(ind),0);
  }

}
