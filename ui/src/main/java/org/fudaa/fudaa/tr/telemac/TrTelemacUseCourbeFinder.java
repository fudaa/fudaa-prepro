/*
 * @creation 15 mars 07
 * @modification $Date: 2007-04-30 14:22:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.fu.FuComparator;
import java.util.Arrays;
import java.util.Set;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.H2dParameters;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcManager;
import org.fudaa.dodico.h2d.telemac.H2dTelemacParameters;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.fudaa.tr.common.TrCourbeUseCounter;
import org.fudaa.fudaa.tr.common.TrCourbeUseFinderAbstract;
import org.fudaa.fudaa.tr.common.TrCourbeUseResultsAbstract;
import org.fudaa.fudaa.tr.common.TrCourbeUseResultsI;
import org.fudaa.fudaa.tr.data.TrVisuPanel;

/**
 * @author fred deniger
 * @version $Id: TrTelemacUseCourbeFinder.java,v 1.3 2007-04-30 14:22:37 deniger Exp $
 */
public class TrTelemacUseCourbeFinder extends TrCourbeUseFinderAbstract {

  public TrTelemacUseCourbeFinder(final H2dParameters _params, final TrVisuPanel _panel, final EGGrapheModel _graphe) {
    super(_params, _panel, _graphe);
  }

  @Override
  protected TrCourbeUseResultsI createResult() {
    return new TelemacResults(getPanel());
  }

  @Override
  protected void go(final TrCourbeUseResultsI _res, final ProgressionInterface _prog) {
    final H2dTelemacBcManager mng = ((H2dTelemacParameters) getParams()).getTelemacCLManager();
    mng.fillWithVariableEvol(((TelemacResults) _res).counter_);
  }

  private class TelemacResults extends TrCourbeUseResultsAbstract {

    public TelemacResults(final TrVisuPanel _visu) {
      super(_visu);
    }

    TrCourbeUseCounter counter_ = new TrCourbeUseCounter(H2dResource.getS("Conditions limites"));

    @Override
    public TrCourbeUseCounter getCounter(final int _rowIndex) {
      return counter_;
    }

    @Override
    public void selectInView(final EvolutionReguliereInterface _eve) {
      final int[] idx = counter_.getIdxSelected(_eve);
      if (idx == null) { return; }
      final TrTelemacVisuPanel pn = (TrTelemacVisuPanel) getPanel();
      final TrTelemacBoundaryBlockLayer layer = (TrTelemacBoundaryBlockLayer) pn.getGroupBoundary()
          .getBcBoundaryLayer();
      pn.getArbreCalqueModel().setSelectionCalque(layer);
      layer.setLiquidSelection(idx);
      super.activateVisuFrame();

    }

    @Override
    public void setFilter(final EvolutionReguliereInterface _evol) {
      counter_.setEvolToTest(_evol);

    }

    @Override
    public void validSearch() {
      final Set evols = counter_.getEvols();
      super.evols_ = (EvolutionReguliereInterface[]) evols.toArray(new EvolutionReguliereInterface[evols.size()]);
      Arrays.sort(evols_, FuComparator.STRING_COMPARATOR);

    }

  }

}
