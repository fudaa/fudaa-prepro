/**
 * @creation 6 d�c. 2004
 * @modification $Date: 2007-01-19 13:14:31 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPalette;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.Arrays;
import javax.swing.BorderFactory;
import javax.swing.JInternalFrame;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfFrontier;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacWeirInput.java,v 1.11 2007-01-19 13:14:31 deniger Exp $
 */
public final class TrTelemacWeirInput extends CtuluDialogPanel implements ZSelectionListener, InternalFrameListener,
    TreeSelectionListener, ActionListener {

  class VisuFrameListener implements InternalFrameListener {

    @Override
    public void internalFrameActivated(final InternalFrameEvent _e) {
      palette_.setVisible(true);
    }

    @Override
    public void internalFrameClosed(final InternalFrameEvent _e) {
      removePalette();
    }

    @Override
    public void internalFrameClosing(final InternalFrameEvent _e) {}

    @Override
    public void internalFrameDeactivated(final InternalFrameEvent _e) {
      palette_.setVisible(false);
    }

    @Override
    public void internalFrameDeiconified(final InternalFrameEvent _e) {}

    @Override
    public void internalFrameIconified(final InternalFrameEvent _e) {}

    @Override
    public void internalFrameOpened(final InternalFrameEvent _e) {}
  }

  protected class CoteSaisie {

    int frIdx_ = -1;
    int[] idx_;
    int pt1_ = -1;
    int pt2_ = -1;

    protected void paintTempo(final Graphics _g) {

      if (frIdx_ >= 0) {
        TraceIcon ic = null;
        EfGridInterface g = null;
        final GrMorphisme versEcran = layer_.getVersEcran();
        ic = new TraceIcon(TraceIcon.CROIX, 3);
        ic.setCouleur(Color.GREEN);
        g = layer_.getM().getSeuils().getGrid();
        TraceBox b = null;
        final GrPoint p = new GrPoint();
        for (int i = idx_.length - 1; i >= 0; i--) {
          final int idx = g.getFrontiers().getIdxGlobal(frIdx_, idx_[i]);
          p.setCoordonnees(g.getPtX(idx), g.getPtY(idx), 0);
          p.autoApplique(versEcran);
          ic.paintIconCentre(_g, p.x_, p.y_);
          if (pt1_ == i) {
            if (b == null) {
              b = new TraceBox();
              b.setHMargin(2);
              b.setVMargin(2);
              b.setHPosition(SwingConstants.CENTER);
              b.setVPosition(SwingConstants.CENTER);
            }
            if (pt1_ == pt2_) {
              b.paintBox((Graphics2D) _g, (int) p.x_, (int) p.y_, CtuluLibString.UN + "-" + CtuluLibString.DEUX);
            } else {
              b.paintBox((Graphics2D) _g, (int) p.x_, (int) p.y_, CtuluLibString.UN);
            }
          } else if ((pt2_ != pt1_) && (pt2_ == i)) {
            if (b == null) {
              b = new TraceBox();
              b.setHMargin(2);
              b.setVMargin(2);
              b.setHPosition(SwingConstants.CENTER);
              b.setVPosition(SwingConstants.CENTER);
            }
            b.paintBox((Graphics2D) _g, (int) p.x_, (int) p.y_, CtuluLibString.DEUX);

          }
        }
      }
    }
  }

  TrTelemacWeirLayer layer_;
  BuLabel lbEtape_;
  BuLabel nbSelect1_;
  BuLabel nbSelect2_;
  BuButton next_;
  BuPalette palette_;

  InternalFrameListener parentListener_;
  JInternalFrame pnParentFrame_;
  int ptExtremCote_;
  int step_;

  /**
   * @param _layer
   */
  public TrTelemacWeirInput(final TrTelemacWeirLayer _layer) {
    super(true);
    layer_ = _layer;
    final Container c = SwingUtilities.getAncestorOfClass(JInternalFrame.class, layer_.pn_);
    if (c instanceof JInternalFrame) {
      pnParentFrame_ = (JInternalFrame) c;
      parentListener_ = new VisuFrameListener();
      pnParentFrame_.addInternalFrameListener(parentListener_);
    }
    layer_.pn_.getArbreCalqueModel().addTreeSelectionListener(this);
    layer_.addSelectionListener(this);
    setLayout(new BuVerticalLayout(10, true, false));
    lbEtape_ = new BuLabel();
    lbEtape_.setHorizontalTextPosition(SwingConstants.CENTER);
    lbEtape_.setHorizontalAlignment(SwingConstants.CENTER);
    add(lbEtape_);
    setEtapeTxt();
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    next_ = new BuButton(TrResource.getS("Valider"));
    next_.setActionCommand("NEXT");
    next_.addActionListener(this);
    // le bouton next est valide si la selection est deja correcte
    next_.setEnabled(false);
    add(next_);
    final BuPanel pn = new BuPanel();
    pn.setLayout(new BuGridLayout(2, 5, 5));
    pn.add(new BuLabel(TrResource.getS("Nombre de noeuds pour le c�t� 1")));
    nbSelect1_ = new BuLabel();
    nbSelect1_.setText(CtuluLibString.ZERO);
    pn.add(nbSelect1_);
    pn.add(new BuLabel(TrResource.getS("Nombre de noeuds pour le c�t� 2")));
    nbSelect2_ = new BuLabel();
    nbSelect2_.setText(CtuluLibString.ZERO);
    pn.add(nbSelect2_);
    add(pn);
    this.selectionChanged(null);

  }

  protected void buildArrays() {
    final boolean tour1 = (layer_.c1Temp_.pt1_ == layer_.c1Temp_.pt2_);
    // on recherche le sens dans lequel dans �tre parcouru la cote 1
    boolean frReverse = false;
    // pas utile si tour complet !
    if (!tour1) {
      final CoteSaisie c1 = layer_.c1Temp_;
      final EfFrontier.FrontierIterator it = layer_.getM().getSeuils().getGrid().getFrontiers().getFrontierIterator(
          c1.frIdx_, c1.idx_[c1.pt1_]);
      int k = it.next();
      while (Arrays.binarySearch(c1.idx_, k) < 0) {
        k = it.next();
      }
      if (k == c1.idx_[c1.pt2_]) {
        frReverse = true;
      }
    }
    final int nbPt = layer_.c1Temp_.idx_.length;
    int nbPtTotal = nbPt;
    if (tour1) {
      nbPtTotal++;
    }
    final int[] cote1 = new int[nbPtTotal];
    final int[] cote2 = new int[nbPtTotal];
    int idx1 = 0;
    int idx2 = 0;
    final CoteSaisie c1 = layer_.c1Temp_;
    final CoteSaisie c2 = layer_.c2Temp_;
    final EfFrontier.FrontierIterator it1 = layer_.getM().getSeuils().getGrid().getFrontiers().getFrontierIterator(
        c1.frIdx_, c1.idx_[c1.pt1_]);
    it1.setReverse(frReverse);
    final EfFrontier.FrontierIterator it2 = layer_.getM().getSeuils().getGrid().getFrontiers().getFrontierIterator(
        c2.frIdx_, c2.idx_[c2.pt1_]);
    it2.setReverse(!it1.isReverse());
    cote1[idx1++] = it1.getGlobalFrIdx();
    while (idx1 < nbPt) {
      if (Arrays.binarySearch(c1.idx_, it1.next()) >= 0) {
        cote1[idx1++] = it1.getGlobalFrIdx();
      }
    }
    if (tour1) {
      cote1[idx1++] = cote1[0];
    }
    cote2[idx2++] = it2.getGlobalFrIdx();
    while (idx2 < nbPt) {
      if (Arrays.binarySearch(c2.idx_, it2.next()) >= 0) {
        cote2[idx2++] = it2.getGlobalFrIdx();
      }
    }
    if (tour1) {
      cote2[idx2++] = cote2[0];
    }
    layer_.unsetCoteTemp();
    removePalette();
    layer_.addSeuil(cote1, cote2);

  }

  protected String getEtapeTexte() {
    if (step_ == 0) {
      return TrResource.getS("S�lectionner les noeuds formant le c�t� 1 du seuil");
    } else if (step_ == 1) {
      return TrResource.getS("S�lectionner le noeud 1 du cot� 1");
    } else if (step_ == 2) {
      return TrResource.getS("S�lectionner le noeud 2 du cot� 1");
    } else if (step_ == 3) {
      return TrResource.getS("S�lectionner les noeuds formant le c�t� 2 du seuil");
    } else if (step_ == 4) {
      return TrResource.getS("S�lectionner le noeud 1 du cot� 2");
    } else if (step_ == 5) { return TrResource.getS("Cr�er"); }
    return null;

  }

  protected void removePalette() {
    if (SwingUtilities.isEventDispatchThread()) {
      try {
        palette_.setClosed(true);
      } catch (final PropertyVetoException _e) {
        FuLog.warning(_e);
      }
      layer_.pn_.getImpl().getMainPanel().getDesktop().removeInternalFrame(palette_);
    } else {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          try {
            palette_.setClosed(true);
          } catch (final PropertyVetoException _e) {
            FuLog.warning(_e);
          }
          layer_.pn_.getImpl().getMainPanel().getDesktop().removeInternalFrame(palette_);
        }
      });
    }
    // layer_.pn_.getImpl().getMainPanel().getDesktop().remove(palette_);
  }

  protected void setEtapeTxt() {
    lbEtape_.setText("<html><u><b>" + getEtapeTexte() + "</b></u></html>");
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    if (_evt.getSource() == next_) {

      if (step_ == 0) {

        final CtuluListSelectionInterface s = layer_.getSelection().getSelection(layer_.isSelectionInUniqueBloc());
        final CoteSaisie c = new CoteSaisie();
        c.idx_ = new int[s.getNbSelectedIndex()];
        c.frIdx_ = layer_.getSelection().isSelectionInOneBloc();
        int idx = 0;
        final EfFrontierInterface fr = layer_.getM().getSeuils().getGrid().getFrontiers();
        for (int i = s.getMaxIndex(); i >= s.getMinIndex(); i--) {
          if (s.isSelected(i)) {
            if (layer_.getM().getSeuils().isUsed(fr.getFrontiereIndice(c.frIdx_, i))) {
              layer_.pn_.getImpl().error(
                  TrResource.getS("Les noeuds s�lectionn�s sont d�j� utilis�s par un autre seuil"));
              next_.setEnabled(false);
              return;
            }
            c.idx_[idx++] = i;

          }
        }
        Arrays.sort(c.idx_);
        layer_.setC1Temp(c);
      } else if (step_ == 1) {
        layer_.c1Temp_.pt1_ = ptExtremCote_;
        layer_.repaint();
      } else if (step_ == 2) {
        layer_.c1Temp_.pt2_ = ptExtremCote_;
        layer_.repaint();
      } else if (step_ == 4) {
        layer_.c2Temp_.pt1_ = ptExtremCote_;
        layer_.repaint();
      } else if (step_ == 3) {
        final CtuluListSelectionInterface s = layer_.getSelection().getSelection(layer_.isSelectionInUniqueBloc());
        final CoteSaisie c = new CoteSaisie();
        c.idx_ = new int[s.getNbSelectedIndex()];
        c.frIdx_ = layer_.getSelection().isSelectionInOneBloc();
        int idx = 0;
        final EfFrontierInterface fr = layer_.getM().getSeuils().getGrid().getFrontiers();
        for (int i = s.getMaxIndex(); i >= s.getMinIndex(); i--) {
          if (s.isSelected(i)) {
            if (((c.frIdx_ == layer_.c1Temp_.frIdx_) && (Arrays.binarySearch(layer_.c1Temp_.idx_, i) >= 0))
                || layer_.getM().getSeuils().isUsed(fr.getFrontiereIndice(c.frIdx_, i))) {
              layer_.pn_.getImpl().error(
                  TrResource.getS("Les noeuds s�lectionn�s sont d�j� utilis�s par un autre seuil"));
              next_.setEnabled(false);
              return;
            }
            c.idx_[idx++] = i;

          }
        }
        Arrays.sort(c.idx_);
        layer_.setC2Temp(c);
      } else if (step_ == 5) {
        buildArrays();
      }

      step_++;
      next_.setEnabled(step_ == 5);
      setEtapeTxt();
      layer_.clearSelection();
    }
  }

  /**
   * Active la saisie des points.
   */
  public void activeSaisiePoint() {
    /*
     * if (getComponentCount() == 0) buildPanel();
     */
    if (palette_ == null) {
      palette_ = new BuPalette();
      palette_.setResizable(true);
      palette_.setContent(this);
      palette_.setClosable(true);
      palette_.addInternalFrameListener(this);
    }

    if (SwingUtilities.isEventDispatchThread()) {
      palette_.pack();
      layer_.pn_.getImpl().getMainPanel().getDesktop().addInternalFrame(palette_);
    } else {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          palette_.pack();
          layer_.pn_.getImpl().getMainPanel().getDesktop().addInternalFrame(palette_);
        }
      });
    }

  }

  @Override
  public void internalFrameActivated(final InternalFrameEvent _e) {}

  @Override
  public void internalFrameClosed(final InternalFrameEvent _e) {

  }

  @Override
  public void internalFrameClosing(final InternalFrameEvent _e) {
    if (parentListener_ != null) {
      pnParentFrame_.removeInternalFrameListener(parentListener_);
    }
    layer_.pn_.getArbreCalqueModel().removeTreeSelectionListener(this);
    palette_.removeInternalFrameListener(this);
    layer_.removeSelectionListener(this);
    layer_.unsetCoteTemp();
  }

  @Override
  public void internalFrameDeactivated(final InternalFrameEvent _e) {}

  @Override
  public void internalFrameDeiconified(final InternalFrameEvent _e) {}

  @Override
  public void internalFrameIconified(final InternalFrameEvent _e) {}

  @Override
  public void internalFrameOpened(final InternalFrameEvent _e) {}

  @Override
  public void selectionChanged(final ZSelectionEvent _evt) {
    if (step_ == 5) { return; }
    final EbliListeSelectionMultiInterface select = layer_.getSelection();
    if (select == null) { return; }
    final int idx = select.isSelectionInOneBloc();
    int nb = 0;
    if (idx >= 0) {
      nb = select.getSelection(idx).getNbSelectedIndex();
    } else {
      next_.setEnabled(false);
      return;
    }
    if (step_ == 0) {
      nbSelect1_.setText(CtuluLibString.getString(nb));
      if (nb > 1) {
        next_.setEnabled(true);
      }
    }
    // saisie de la deuxieme cote du seuil
    else if (step_ == 3) {
      nbSelect2_.setText(CtuluLibString.getString(nb));
      if (nb > 1) {
        next_.setEnabled(nb == layer_.c1Temp_.idx_.length);
      }
    }
    if (step_ == 1 || (step_ == 2) || (step_ == 4)) {
      if (nb != 1) {
        next_.setEnabled(false);
        return;
      }
      final int premierPointCote1 = select.getSelection(idx).getMaxIndex();
      final int[] tab = (step_ == 4 ? layer_.c2Temp_.idx_ : layer_.c1Temp_.idx_);
      final int k = Arrays.binarySearch(tab, premierPointCote1);
      if (k >= 0) {
        if ((step_ == 1) || (step_ == 4)) {
          ptExtremCote_ = k;
          next_.setEnabled(true);
        } else {
          final int firstPt = layer_.c1Temp_.pt1_;
          ptExtremCote_ = k;
          if (firstPt == ptExtremCote_) {
            next_.setEnabled(true);
          } else {
            int plusGrand = firstPt + 1;
            if (plusGrand == layer_.c1Temp_.idx_.length) {
              plusGrand = 0;
            }
            if (k == plusGrand) {
              next_.setEnabled(true);
            } else {
              plusGrand = firstPt - 1;
              if (plusGrand < 0) {
                plusGrand = layer_.c1Temp_.idx_.length - 1;
              }
              next_.setEnabled(k == plusGrand);
            }
          }
        }

      }

    }
  }

  @Override
  public void valueChanged(final TreeSelectionEvent _e) {
    if (layer_.pn_.getArbreCalqueModel().getSelectedCalque() != layer_) {
      removePalette();
    }
  }

}