/*
 * @creation 14 mars 07
 * @modification $Date: 2007-04-30 14:22:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.reflux;

import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.reflux.H2dRefluxParameters;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.fudaa.tr.common.TrCourbeUseFinderAbstract;
import org.fudaa.fudaa.tr.common.TrCourbeUseResultsI;

/**
 * @author fred deniger
 * @version $Id: TrRefluxCourbeUseFinder.java,v 1.3 2007-04-30 14:22:39 deniger Exp $
 */
public class TrRefluxCourbeUseFinder extends TrCourbeUseFinderAbstract {

  public TrRefluxCourbeUseFinder(final H2dRefluxParameters _params, final TrRefluxVisuPanel _panel,
      final EGGrapheModel _graphe) {
    super(_params, _panel, _graphe);
  }

  @Override
  protected TrCourbeUseResultsI createResult() {
    return new TrRefluxCourbeUseResult(getRefluxParams(), (TrRefluxVisuPanel) getPanel());
  }

  private H2dRefluxParameters getRefluxParams() {
    return (H2dRefluxParameters) getParams();
  }

  @Override
  protected void go(final TrCourbeUseResultsI _res, final ProgressionInterface _prog) {
    final H2dRefluxParameters params = getRefluxParams();
    final TrRefluxCourbeUseResult res = (TrRefluxCourbeUseResult) _res;
    if (isStop()) { return; }
    params.getRefluxClManager().fillWithEvolVar(res.usedInCl_);
    if (isStop()) { return; }
    params.getElementPropMng().fillWithEvolVar(res.usedInPn_);
    if (isStop()) { return; }
  }
}
