/**
 * @creation 10 janv. 2005
 * @modification $Date: 2007-05-04 14:01:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTableCellEditor;
import com.memoire.bu.BuTextField;
import gnu.trove.TObjectDoubleHashMap;
import gnu.trove.TObjectDoubleIterator;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.util.*;
import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumnModel;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.dodico.h2d.H2DLib;
import org.fudaa.dodico.h2d.H2dNodalPropertiesMngAbstract;
import org.fudaa.dodico.h2d.H2dNodalPropertiesMngI;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSIProperties;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacDefaultPropertiesEditor.java,v 1.27 2007-05-04 14:01:46 deniger Exp $
 */
public class TrTelemacDefaultPropertiesEditor extends CtuluDialogPanel {

  /**
   * @author fred deniger
   * @version $Id: TrTelemacDefaultPropertiesEditor.java,v 1.27 2007-05-04 14:01:46 deniger Exp $
   */
  static final class VarCellTextRenderer extends CtuluCellTextRenderer {

    final H2dNodalPropertiesMngI targetMng_;

    public VarCellTextRenderer(final H2dNodalPropertiesMngI _target) {
      super();
      targetMng_ = _target;
    }

    @Override
    protected void setValue(final Object _value) {
      if (_value == null) {
        setText(CtuluResource.CTULU.getString("ajouter"));
      } else {
        if (_value instanceof H2dVariableType && !targetMng_.canVariableBeenRemoved((H2dVariableType) _value)) {
          setForeground(Color.GRAY);
        } else {
          setForeground(Color.BLACK);
        }
        super.setValue(_value);
      }
    }
  }

  private class RemovedModel extends CtuluListEditorModel {

    /**
     * appelle le constructeur avec true.
     * call the constructor with true
     */
    public RemovedModel() {
      super(true);
    }

    /**
     * @return les variables a enlever
     * @return the variables to remove
     */
    public Set getVarToRemoved() {
      return new HashSet(v_);
    }

    /**
     * @return false.
     */
    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return false;
    }

    @Override
    public void remove(final ListSelectionModel _select) {
      if ((_select.isSelectionEmpty()) || (!canRemove())) { return; }
      final int min = _select.getMinSelectionIndex();
      int max = _select.getMaxSelectionIndex();
      max = max >= v_.size() ? v_.size() - 1 : max;
      for (int i = max; i >= min; i--) {
        if (_select.isSelectedIndex(i)) {
          final H2dVariableType var = (H2dVariableType) getValueAt(i);
          model_.setValueAt(var, model_.getRowCount() - 1, 1);
          v_.remove(i);
        }
      }
      fireTableRowsDeleted(min, max);
    }

    /**
     * Met a jour les valeurs.
     * Update the values	 
     */
    public void updateValue() {
      final int old = v_.size();
      super.v_.clear();
      if (old > 0) {
        super.fireTableRowsDeleted(0, old - 1);
      }
      for (final Iterator it = iniNodalProps_.iterator(); it.hasNext();) {
        final H2dVariableType var = (H2dVariableType) it.next();
        if (!model_.contains(var)) {
          super.v_.add(var);
        }
      }
      super.fireTableRowsInserted(0, super.v_.size());
    }
  }

  private class ValueComboBoxModel extends DefaultComboBoxModel implements TableModelListener {

    ValueComboBoxModel(final List _list) {
      super(CtuluLibArray.sort(_list.toArray()));
      model_.addTableModelListener(this);
    }

    void updateModel() {
      super.removeAllElements();
      final Object[] os = buildProposedValuesList().toArray();
      if (os != null && os.length > 0) {
        Arrays.sort(os);
        for (int i = 0; i < os.length; i++) {
          super.addElement(os[i]);
        }
      }
    }

    @Override
    public void tableChanged(final TableModelEvent _e) {
      updateModel();
    }

  }

  private class VarModel extends CtuluListEditorModel {

    // les variables qui dupliquent des anciennes
    // Variables which duplicate old ones
	transient Map newVarCopyOfOldVar_ = new HashMap();

    // les variables qui renomment des anciennes
    // Variables which rename old ones
	transient Map newVarOldVar_ = new HashMap();

    transient Map newVarDoubleValues_ = new HashMap();

    // les variables ajout�es
    // the added variables
	transient TObjectDoubleHashMap varDefault_ = new TObjectDoubleHashMap();

    /**
     * Construit le vecteur.
     * Build the vector     
	 */
    public VarModel() {
      super(iniNodalProps_.toArray(), false, 100);
      Collections.sort(super.v_);
    }

    @Override
    protected void actionBeforeRemove(final int _idx) {
      final Object o = getValueAt(_idx);
      varDefault_.remove(o);
      newVarCopyOfOldVar_.remove(o);
      newVarOldVar_.remove(o);
      newVarDoubleValues_.remove(o);
    }

    @Override
    public boolean actionAdd() {
      return false;
    }

    /**
     * @param _var la variable / the variable
     * @return true si contenue / true if contained
     */
    public boolean contains(final H2dVariableType _var) {
      return super.v_.contains(_var);
    }

    public void removeUsed(final List _l) {
      _l.removeAll(v_);
    }

    /**
     * @param _row l'indice de la variable � dupliquer / the index of the variable to duplicate
     */
    public void dupliquer(final int _row) {
      final H2dVariableType var = (H2dVariableType) getValueAt(_row);
      if (!iniNodalProps_.contains(var)) { return; }
      String newName = var.getName() + CtuluLibString.ESPACE + "C";
      H2dVariableType newT = H2dVariableType.createTempVar(newName, null);
      if (isNameUsed(newName) || isInitVar(newT) != null) {
        int idx = 1;
        final String prefixe = newName + "_";
        int max = 101;
        do {
          newName = prefixe + CtuluLibString.getString(idx++);
          newT = H2dVariableType.createTempVar(newName, null);
          max--;
        } while (max > 0 || isNameUsed(newName) || isInitVar(newT) != null);
        // on n'a pas pu ajouter la variable
		// we can't add the variable
        if (max < 0) { return; }
      }
      newVarCopyOfOldVar_.put(newT, var);
      setValueAt(newT, getRowCount() - 1, 1);
    }

    @Override
    public int getColumnCount() {
      return 3;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 1) { return H2dResource.getS("Variable"); }
      if (_column == 2) { return H2dResource.getS("Valeur"); }
      return CtuluLibString.ESPACE;
    }

    /**
     * @param _var la variable / the variable
     * @return la valeur d�finie par l'utilisateur / the value defined by the user
     */
    public double getDefinedValue(final H2dVariableType _var) {
      return varDefault_.get(_var);
    }

    /**
     * @param _name le nom de la variable / the name of the variable
     * @return la variable correspondant / the corresponding variable
     */
    public H2dVariableType getKnownVar(final String _name) {
      return CtuluLibString.isEmpty(_name) ? null : builder_.getUsedKnownVar(_name.trim());
    }

    /**
     * @return les nouvelles variables avec leurs valeurs / the new variables with their value
     */
    public TObjectDoubleHashMap getNewValues() {
      return varDefault_;
    }

    @Override
    public int getRowCount() {
      return super.getRowCount() + 1;
    }

    @Override
    public Object getValueAt(final int _row) {
      if (_row == getRowCount() - 1) { return null; }
      return super.getValueAt(_row);

    }

    @Override
    public Object getValueAt(final int _row, final int _col) {
      final Object var = getValueAt(_row);
      if (var == null) { return null; }
      if (_col == 1) { return var; }
      final boolean oldValue = iniNodalProps_.contains(var);
      if (_col == 0) { return Boolean.valueOf(oldValue); }
      if (!oldValue) {
        if (newVarOldVar_.containsKey(var)) {
          return TrResource.getS("Nouveau nom de la variable {0}", newVarOldVar_.get(var).toString());
        } else if (newVarCopyOfOldVar_.containsKey(var)) {
          return TrResource.getS("Copie de la variable {0}", newVarCopyOfOldVar_.get(var).toString());
        } else if (newVarDoubleValues_.containsKey(var)) { return TrResource.getS("Valeurs r�cup�r�es"); }

      }
      if (_col == 2 && varDefault_.containsKey(var)) { return CtuluLib.getDouble(varDefault_.get(var)); }
      return null;
    }

    /**
     * @return recup�re les nouvelles variables -> tableaux de double / the new variables -> array of double
     */
    public Map getVarCtuluDoubleArray() {
      final Map varArray = new HashMap();
      for (final Iterator it = newVarCopyOfOldVar_.entrySet().iterator(); it.hasNext();) {
        final Map.Entry e = (Map.Entry) it.next();
        varArray.put(e.getKey(), target_.getViewedModel((H2dVariableType) e.getValue()).getValues());
      }
      for (final Iterator it = newVarOldVar_.entrySet().iterator(); it.hasNext();) {
        final Map.Entry e = (Map.Entry) it.next();
        varArray.put(e.getKey(), target_.getViewedModel((H2dVariableType) e.getValue()).getValues());
      }
      for (final Iterator it = newVarDoubleValues_.entrySet().iterator(); it.hasNext();) {
        final Map.Entry e = (Map.Entry) it.next();
        varArray.put(e.getKey(), e.getValue());
      }
      return varArray;
    }

    /**
     * @return les variable a ajouter / the variables to add
     */
    public Set getVarToAdd() {
      final Set varToAdd = new HashSet();
      for (int i = v_.size() - 1; i >= 0; i--) {
        if (iniNodalProps_.contains(v_.get(i))) {
          varToAdd.add(v_.get(i));
        }
      }
      return varToAdd;
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return (_columnIndex == 1 && ((getValueAt(_rowIndex) == null) || target_
          .canVariableBeenRemoved((H2dVariableType) getValueAt(_rowIndex))))
          || (_columnIndex == 2 && !iniNodalProps_.contains(getValueAt(_rowIndex))
              && !newVarCopyOfOldVar_.containsKey(getValueAt(_rowIndex))
              && !newVarOldVar_.containsKey(getValueAt(_rowIndex)) && !newVarDoubleValues_
              .containsKey(getValueAt(_rowIndex)));
    }

    /**
          * @param _var la variable a tester
          * @return true si ce le nom de _var est deja utilisee par une variable
	* @param _var  the variable to test
	 * @return true if ce the name de_var is already used by a variable
     */
    public H2dVariableType isInitVar(final H2dVariableType _var) {
      if (iniNodalProps_.contains(_var)) { return _var; }
      for (final Iterator it = iniNodalProps_.iterator(); it.hasNext();) {
        final H2dVariableType var = (H2dVariableType) it.next();
        if (_var.getName().equals(var.getName())) { return var; }
      }
      return null;
    }

    /**
           * @param _name le nom
           * @return true si ce nom est deja utilise
	 * @param _name the name
	 * @return true if this name is already used
     */
    public boolean isNameUsed(final String _name) {
      for (int i = v_.size() - 1; i >= 0; i--) {
        if (((H2dVariableType) v_.get(i)).getName().equals(_name)) { return true; }
      }
      return false;
    }

    /**
           * LEs erreurs sont affich�es.
           * The mistakes are sticked	   
           * @param _initName le nom initial
           * @param _newVar la nouvelle variable
           * @param _initVar la variable remplacee
           * @return true si deja utilise ou erronee
	 * @param _initName the initial name  
	 * @param _newVar the new variable 
	 * @param _initVar the replaced variable
	 * @return true if already used or erroneous
     */
    public boolean isUsedOrWrong(final String _initName, final H2dVariableType _newVar, final H2dVariableType _initVar) {
      return isUsedOrWrong(_initName, _newVar, _initVar, true);
    }

    public int getRowOf(final H2dVariableType _t) {
      for (int i = getRowCount() - 1; i >= 0; i--) {
        if (getValueAt(i) == _t) { return i; }
      }
      return -1;
    }

    /**
     * @param _initName le nom initial / the initial name
     * @param _newVar la nouvelle variable / the new variable
     * @param _initVar la variable remplacee / the replaced  variable
     * @param _displayError true si l'erreur doit etre affichee / true if error must be shown
     * @return true si deja utilise ou erronee / true if already used or false
     */
    public boolean isUsedOrWrong(final String _initName, final H2dVariableType _newVar, final H2dVariableType _initVar,
        final boolean _displayError) {
      if (_newVar == null) { return true; }
      if (!target_.canVariableBeenRemoved(_initVar) || !target_.canVariableBeenAdd(_newVar)) { return true; }
      final StringBuffer buf = new StringBuffer();
      if (_newVar.getName() == null || _newVar.getName().trim().length() == 0) {
        buf.append(TrResource.getS("Le nom est vide"));
      } else if (_newVar != _initVar && contains(_newVar)) {
        if (buf.length() > 0) {
          buf.append(CtuluLibString.LINE_SEP);
        }
        if (!_initName.equals(_newVar.getName())) {
          buf.append(TrResource.getS("Ce nom correspond � la variable {0}", _newVar.getName()));
          buf.append(CtuluLibString.LINE_SEP);
        }
        buf.append(TrResource.getS("Cette variable est d�j� initialis�e"));

      } else if (model_.isNameUsed(_newVar.getName())) {
        if (buf.length() > 0) {
          buf.append(CtuluLibString.LINE_SEP);
        }
        buf.append(TrResource.getS("Ce nom est d�j� utilis�"));
      }
      if (buf.length() > 0 && _displayError) {
        TrTelemacDefaultPropertiesEditor.this.setErrorText(buf.toString());
        return true;
      }
      TrTelemacDefaultPropertiesEditor.this.setErrorText(null);
      return false;
    }

    @Override
    public void remove(final ListSelectionModel _model) {
      if (_model.isSelectionEmpty()) { return; }
      final int min = _model.getMinSelectionIndex();
      int max = _model.getMaxSelectionIndex();
      max = max >= v_.size() ? v_.size() - 1 : max;
      boolean isVitesseRemoved = false;
      for (int i = max; i >= min; i--) {
        if (_model.isSelectedIndex(i)) {
          final H2dVariableType t = (H2dVariableType) getValueAt(i);
          // si la variable ne peut pas etre enleve on l'enleve de la selection
		  // if the variable can't be removed, we remove it from the selection
          if (!target_.canVariableBeenRemoved(t)) {
            _model.removeIndexInterval(i, i);
          }
          if (isSi_ && (t == H2dVariableType.VITESSE_U || t == H2dVariableType.VITESSE_V)) {
            isVitesseRemoved = true;
          }
        }
      }
      // pour les vitesses, les 2 doivent etre enlevees.
      // for the speeds, both must be removed
      if (isVitesseRemoved) {
        int i = getRowOf(H2dVariableType.VITESSE_U);
        if (i >= 0) {
          _model.addSelectionInterval(i, i);
        }
        i = getRowOf(H2dVariableType.VITESSE_V);
        if (i >= 0) {
          _model.addSelectionInterval(i, i);
        }

      }
      super.remove(_model);
      removedValue_.updateValue();
    }

    public void addNewVar(final H2dVariableType _t, final double[] _values) {
      if (!iniNodalProps_.contains(_t) && !contains(_t)) {
        newVarDoubleValues_.put(_t, _values);
        super.addElement(_t);
      }
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      if (_value == null) { return; }
      if (_columnIndex == 2 && isCellEditable(_rowIndex, _columnIndex) && !iniNodalProps_.contains(_value)) {
        varDefault_.put(getValueAt(_rowIndex), Double.parseDouble(_value.toString()));
      }
      H2dVariableType var = null;
      final String valueTxt = _value.toString();
      var = getVar(_value, valueTxt);
      if (_columnIndex == 1) {
        if (_rowIndex == getRowCount() - 1) {
          var = setValueAtLast(var, valueTxt);
        } else {
          boolean updateRemovedValues = false;
          final H2dVariableType old = (H2dVariableType) getValueAt(_rowIndex);
          if (var == null || old == null) { return; }
          if (var.getName().equals(old.getName())) { return; }
          if (isUsedOrWrong(valueTxt, var, old)) { return; }
          final H2dVariableType supp = isInitVar(var);
          // cette variable etait supprimee
          // this variable was deleted
          if (supp != null) {
            var = supp;
            updateRemovedValues = true;
          }
          // cette varialbe etait une copie d'une autre
          // this variable was a copy of another one
          else if (newVarCopyOfOldVar_.containsKey(old)) {
            newVarCopyOfOldVar_.put(var, newVarCopyOfOldVar_.remove(old));
          }
          // cette variable renommait une autre
		  // this variable renamed another one
          else if (newVarOldVar_.containsKey(old)) {
            newVarOldVar_.put(var, newVarOldVar_.remove(old));
          }
          // Cette variable va renommer une autre
		  // This variable is going to rename another one
          else if (iniNodalProps_.contains(old)) {
            newVarOldVar_.put(var, old);
            updateRemovedValues = true;
          }
          // la nouvelle variable remplace l'ancienne
		  // the new variable replaces the old one
          else {
            varDefault_.put(var, varDefault_.remove(old));
          }
          super.v_.set(_rowIndex, var);
          if (updateRemovedValues) {
            removedValue_.updateValue();
          }
          fireTableRowsUpdated(_rowIndex, _rowIndex);
        }
      }
      if (isSi_) {
        setSiValue(var);
      }
    }

    private H2dVariableType setValueAtLast(final H2dVariableType _var, final String _valueTxt) {
      H2dVariableType res = _var;
      if (!isUsedOrWrong(_valueTxt, res, null)) {
        final H2dVariableType varToSupp = isInitVar(res);
        boolean restored = false;
        if (varToSupp != null) {
          res = varToSupp;
          restored = true;
        } else if (!newVarCopyOfOldVar_.containsKey(res)) {
          varDefault_.put(res, getDefaultValue(res));
        }
        super.v_.add(res);
        if (restored) {
          removedValue_.updateValue();
        }
        fireTableRowsInserted(0, getRowCount() - 1);
      }
      return res;
    }

    private H2dVariableType getVar(final Object _value, final String _valueTxt) {
      H2dVariableType var;
      if (_value instanceof H2dVariableType) {
        var = (H2dVariableType) _value;
      } else {
        var = getKnownVar(_valueTxt);
        if (var == null) {
          var = H2dVariableType.createTempVar(_valueTxt, null);
        }
      }
      return var;
    }

    private void setSiValue(final H2dVariableType _var) {
      if (_var == H2dVariableType.VITESSE_U) {
        final int i = getRowOf(H2dVariableType.VITESSE_V);
        if (i < 0) {
          setValueAt(H2dVariableType.VITESSE_V, getRowCount() - 1, 1);
        }
      } else if (_var == H2dVariableType.VITESSE_V) {
        final int i = getRowOf(H2dVariableType.VITESSE_U);
        if (i < 0) {
          setValueAt(H2dVariableType.VITESSE_U, getRowCount() - 1, 1);
        }
      }
    }

  }

  static class VarEditor extends DefaultCellEditor {

    VarEditor(final TrTelemacDefaultPropertiesEditor _ed) {
      super(new JComboBox());
      ((JComboBox) super.editorComponent).setEditable(true);
      _ed.buildProposedValues();
      ((JComboBox) super.editorComponent).setModel(_ed.proposedValue_);
    }

    @Override
    public boolean stopCellEditing() {
      final Object txt = ((JComboBox) super.editorComponent).getSelectedItem();
      if (txt == null || txt.toString().length() == 0) { return false; }
      return super.stopCellEditing();
    }

  }

  final TrTelemacSerafinHelper builder_;

  final Set iniNodalProps_;

  final boolean isSi_;

  BuLabel lbTime_;

  VarModel model_;

  TrTelemacVisuPanel panel_;

  ComboBoxModel proposedValue_;

  RemovedModel removedValue_;

  H2dNodalPropertiesMngI target_;

  private final BuTextField tftime_;

  H2dVariableType[] nodalVar_;

  /**
   * @param _panel la panneau des calques / the layer panel
   * @param _isSi true si on modifie les solutions initiales / true if we modify the initial solutions
   */
  public TrTelemacDefaultPropertiesEditor(final TrTelemacVisuPanel _panel) {
    super(true);
    isSi_ = _panel.proj_.getTrTelemacParams().isRepriseActivatedByUser();
    if (isSi_) {
      target_ = _panel.getTelemacParameters().getSolutionsInit();
    } else {
      target_ = _panel.getTelemacParameters().getNodalProperties();
    }
    panel_ = _panel;
    setLayout(new BuBorderLayout(2, 2));
    builder_ = new TrTelemacSerafinHelper(_panel.getTrParams());
    if (isSi_) {
      final BuPanel pnTime = new BuPanel();
      lbTime_ = addLabel(pnTime, TrResource.getS("Valeur du pas de temps dans le fichier de reprise"));
      tftime_ = addDoubleText(pnTime);
      tftime_.setText(Double.toString(((H2dTelemacSIProperties) target_).getTimeStep()));
      tftime_.setValueValidator(H2DLib.POS);
      add(pnTime, BuBorderLayout.NORTH);
      add(pnTime, BuBorderLayout.NORTH);
    } else {
      tftime_ = null;
    }
    final List varList = target_.getAllVariables();
    iniNodalProps_ = varList == null ? new HashSet() : new HashSet(varList);
    this.model_ = new VarModel();
    final CtuluListEditorPanel editor = new CtuluListEditorPanel(model_, false, true, false, true, false) {

      @Override
      protected void buildModifierButton() {
        btModifier_ = buildButton(BuResource.BU.getIcon("copier"), EbliLib.getS("Dupliquer"), "MODIFIER", EbliLib
            .getS("Modifier l'�l�ment s�lectionner"), KeyEvent.VK_D);
      }

      @Override
      protected void updateActionsState() {
        super.updateActionsState();
        int selectedRow = -1;
        H2dVariableType varSelected = null;
        if (table_.getSelectedRowCount() == 1) {
          selectedRow = table_.getSelectedRow();
          varSelected = (H2dVariableType) model_.getValueAt(selectedRow);
        }
        if (btModifier_.isEnabled()) {
          btModifier_.setEnabled(varSelected != null && iniNodalProps_.contains(varSelected));
        }
        if (btSupprimer_.isEnabled() && varSelected != null) {
          btSupprimer_.setEnabled(target_.canVariableBeenRemoved(varSelected));
        }
      }

      @Override
      public void actionModify() {
        if (table_.getSelectedRowCount() == 1) {
          model_.dupliquer(table_.getSelectedRow());
          final int insertIdx = model_.getRowCount() - 2;
          table_.getSelectionModel().setSelectionInterval(insertIdx, insertIdx);
        }

      }

    };
    final TableColumnModel col = editor.getTableColumnModel();
    final CtuluCellTextRenderer icone = new CtuluCellTextRenderer() {

      @Override
      protected void setValue(final Object _value) {
        if (_value == null || (((Boolean) _value).booleanValue())) {
          setIcon(BuResource.BU.getIcon("aucun"));
        } else {
          setIcon(BuResource.BU.getIcon("ajouter"));
        }
      }
    };
    col.getColumn(0).setCellRenderer(icone);
    col.getColumn(0).setMaxWidth(20);
    final CtuluCellTextRenderer addRenderer = new VarCellTextRenderer(target_);
    col.getColumn(1).setCellRenderer(addRenderer);
    col.getColumn(1).setCellEditor(new VarEditor(this));
    final BuTextField tf = BuTextField.createDoubleField();
    final BuTableCellEditor cellEditor = new BuTableCellEditor(tf) {

      @Override
      public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _selected,
          final int _row, final int _column) {
        final H2dVariableType var = (H2dVariableType) model_.getValueAt(_row);
        tf.setValueValidator(H2dNodalPropertiesMngAbstract.getValueValidator(var));
        return super.getTableCellEditorComponent(_table, _value, _selected, _row, _column);
      }
    };
    col.getColumn(2).setCellEditor(cellEditor);
    EGTableGraphePanel.setDefaultEnterAction(editor.getTable());

    editor.setState(false, true, false);
    editor.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), CtuluLib.getS("Variables")));
    add(editor, BuBorderLayout.CENTER);
    removedValue_ = new RemovedModel();
    final CtuluListEditorPanel removed = new CtuluListEditorPanel(removedValue_, false, true, false) {

      @Override
      protected void buildSupprimerButton() {
        btSupprimer_ = buildButton(BuResource.BU.getIcon("refaire"), CtuluLib.getS("Restaurer"), "SUPPRIMER", EbliLib
            .getS("Restaurer les �l�ments s�lectionn�s"), KeyEvent.VK_R);
      }

    };
    removed.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), CtuluLib
        .getS("Variables supprim�es")));
    add(removed, BuBorderLayout.SOUTH);
  }

  void buildProposedValues() {
    if (proposedValue_ == null) {
      proposedValue_ = new ValueComboBoxModel(buildProposedValuesList());
    }
  }

  List buildProposedValuesList() {
    final List list = new ArrayList();
    if (isSi_) {
      list.add(H2dVariableType.BATHYMETRIE);
      list.add(H2dVariableType.COEF_FROTTEMENT_FOND);
      list.add(H2dVariableType.VISCOSITE);
      list.add(H2dVariableType.VITESSE_U);
      list.add(H2dVariableType.VITESSE_V);
    } else {
      list.add(H2dVariableType.BATHYMETRIE);
      list.add(H2dVariableType.COEF_FROTTEMENT_FOND);
      list.add(H2dVariableType.VISCOSITE);
    }
    list.removeAll(iniNodalProps_);
    list.removeAll(model_.getValuesInList());
    return list;
  }

  @Override
  public boolean apply() {
    final Set removedSet = removedValue_.getVarToRemoved();
    final TObjectDoubleHashMap newVal = model_.getNewValues();
    final Map varCtuluDoubleArray = model_.getVarCtuluDoubleArray();
    final boolean isVarListChanged = (newVal.size() > 0) || (removedSet.size() > 0) || varCtuluDoubleArray.size() > 0;
    CtuluCommandComposite cmp = null;
    if (isVarListChanged) {
      cmp = new CtuluCommandComposite() {

        @Override
        protected void actionToDoAfterUndoOrRedo() {
          panel_.updateIsoVariables();
        }

        @Override
        protected boolean isActionToDoAfterUndoOrRedo() {
          return true;
        }
      };
    } else {
      cmp = new CtuluCommandComposite();
    }

    for (final Iterator it = removedSet.iterator(); it.hasNext();) {
      target_.removeValues((H2dVariableType) it.next(), cmp);
    }
    final TObjectDoubleIterator iterator = newVal.iterator();
    for (int i = newVal.size(); i-- > 0;) {
      iterator.advance();
      target_.initValues((H2dVariableType) iterator.key(), iterator.value(), cmp);
    }

    for (final Iterator it = varCtuluDoubleArray.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      target_.initValues((H2dVariableType) e.getKey(), (double[]) e.getValue(), cmp);
    }
    if (isSi_) {
      ((H2dTelemacSIProperties) target_).setFirstTimeStep(Double.parseDouble(tftime_.getText()), cmp);
    }
    if (panel_.getCmdMng() != null) {
      panel_.getCmdMng().addCmd(cmp.getSimplify());
    }
    return true;
  }

  /**
   * @param _var la variable / the variable
   * @return valeurs par defaut definie dans le dico / values defined  by  default in the dico
   */
  public double getDefaultValue(final H2dVariableType _var) {
    double defValue = 0;
    try {
      defValue = Double.parseDouble(panel_.getTelemacParameters().getDefaultValueForNodaleProperties(_var));
    } catch (final NumberFormatException e) {}
    return defValue;

  }

  @Override
  public boolean isDataValid() {
    boolean isValid = true;
    if (isSi_) {
      final String txt = tftime_.getText();
      boolean valide = true;
      if (CtuluLibString.isEmpty(txt) || Double.parseDouble(txt) < 0) {
        valide = false;
        isValid = false;
      }

      lbTime_.setForeground(valide ? Color.BLACK : Color.RED);
    }
    if (!isValid) {
      setErrorText(TrResource.getS("Des valeurs sont erron�es"));
    }
    return isValid;
  }
}
