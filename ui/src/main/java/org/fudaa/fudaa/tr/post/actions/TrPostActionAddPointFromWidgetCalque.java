package org.fudaa.fudaa.tr.post.actions;

import com.memoire.bu.BuDialogError;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionAbstract;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetCreatorVueCalque;
import org.fudaa.ebli.visuallibrary.graphe.EbliWidgetCreatorGraphe;
import org.fudaa.ebli.visuallibrary.graphe.GrapheCellRenderer;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.post.profile.MvProfileBuilderFromTree;
import org.fudaa.fudaa.tr.post.profile.MvProfileCoteTester;
import org.fudaa.fudaa.tr.post.profile.MvProfileTreeModel;

/**
 * Action qui: - recupere la liste des calques potentiels - recupere la liste des graphes potentiels - propose a l utilisateur une interface combo
 * pour choisir le calque cible - propose a l utilisateur une interface jlist pour choisir le(s) graphe(s) cible(s) realise la moulinette
 *
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public abstract class TrPostActionAddPointFromWidgetCalque extends EbliWidgetActionAbstract {

  /**
   * Action specialisee pour les courbes spatiales. NE propose que les widget graphes qui respectent les donnees patiales
   *
   * @author Adrien Hadoux
   */
  public static class Spatial extends TrPostActionAddPointFromWidgetCalque {

    public Spatial(final EbliScene _scene, final TrPostCommonImplementation _impl) {
      super(_scene, _impl);
    }

    @Override
    public void setTitleForFrame() {
      frame_.setTitle(TrResource.TR.getString("Ajout des points du calques dans les courbes spatiales"));
    }

    @Override
    public boolean isCorrectGraph(final EGGrapheModel _model) {
      // -- ATTENTION ICI ON AJOUTE JUSTE POUR LES COURBE TEMPORELLE --//
      return (_model instanceof MvProfileTreeModel);

    }

    @Override
    public void executeAction(final TrPostCommonImplementation _impl, final EGGraphe _graphe,
            final TrPostVisuPanel _calque) {
      /**
       * @see TrPostWizardProfilSpatiaux.java pour savoir comment fusionenr une courbe spatiale dans un graphe existant. methode doTask()
       */
    }

    @Override
    protected void createNewGraphe(final TrPostVisuPanel _calqueChoisi) {

      // creation automatique d un graphe avec les spatiales poru le calque
      // vhoisi

      new MvProfileBuilderFromTree(TrPostProfileAction.createProfileAdapter(_calqueChoisi), impl_,
              ((ZCalqueAffichageDonneesInterface) _calqueChoisi.getArbreCalqueModel().getSelectedCalque())
              .getSelectedLine(), new MvProfileCoteTester()).start();

    }

    @Override
    public void setToolTip() {
      this.setDefaultToolTip(TrResource.getS("Ajoute aux graphes selectionn�s les courbes s�lectionn�es du calque"));
    }
  }
  CtuluCommandContainer cmd_;
  public TrPostCommonImplementation impl_;
  JComboBox boxCalques_;
  JList jlisteGraphesChoix;
  JList jlisteGraphesSelections;
  JButton validation, select, unselect, creationNewGraphe, quitter;
  JDialog frame_;
  DefaultListModel modelGraphesPossibles;
  DefaultListModel modelGraphesChoisis;
  ArrayList<TrPostVisuPanel> listeCalquesPossibles;
  public ArrayList<EGGraphe> listeGraphesPossibles;
  public ArrayList<EGGraphe> listeGraphesChoisis;
  ArrayList<JLabel> listeObjetsCalques;

  public TrPostActionAddPointFromWidgetCalque(final EbliScene _scene, final TrPostCommonImplementation _impl) {
    super(_scene, TrResource.getS("Ajout des points du calque"), CtuluResource.CTULU.getIcon("cible"), "ADDPOINTWIDGET");

    cmd_ = _scene.getCmdMng();
    impl_ = _impl;

    setToolTip();

  }

  public abstract void setToolTip();

  @Override
  public void actionPerformed(final ActionEvent e) {

    if (e.getSource() == select) {
      if (jlisteGraphesChoix.getSelectedIndex() != -1) {
        final int indice = jlisteGraphesChoix.getSelectedIndex();
        final EGGraphe graphe = listeGraphesPossibles.get(indice);
        final JLabel title = (JLabel) modelGraphesPossibles.getElementAt(indice);
        modelGraphesChoisis.addElement(title);
        listeGraphesChoisis.add(graphe);
        modelGraphesPossibles.removeElementAt(indice);
        listeGraphesPossibles.remove(indice);
      }
    } else if (e.getSource() == unselect) {
      if (jlisteGraphesSelections.getSelectedIndex() != -1) {
        final int indice = jlisteGraphesSelections.getSelectedIndex();
        final EGGraphe graphe = listeGraphesChoisis.get(indice);
        final JLabel title = (JLabel) modelGraphesChoisis.getElementAt(indice);
        modelGraphesPossibles.addElement(title);
        listeGraphesPossibles.add(graphe);
        modelGraphesChoisis.removeElementAt(indice);
        listeGraphesChoisis.remove(indice);
      }
    } else if (e.getSource() == validation) {
      // validation de l action
      if (listeGraphesChoisis.size() == 0) {
        new BuDialogError(impl_.getApp(), impl_.getInformationsSoftware(), TrResource
                .getS("Il doit y avoir au moins un graphe s�lectionn�")).activate();
        return;
      }

      final TrPostVisuPanel calqueChoisi = listeCalquesPossibles.get(boxCalques_.getSelectedIndex());

      // -- on applique l'action � chaque widget graphe --//
      for (int i = 0; i < listeGraphesChoisis.size(); i++) {
        final EGGraphe graphe = listeGraphesChoisis.get(i);

        // -- execution de l action pour le graphe selectionne --//
        executeAction(impl_, graphe, calqueChoisi);

      }
      scene_.refresh();

    } else if (e.getSource() == quitter) {
      frame_.dispose();

    } else if (e.getSource() == creationNewGraphe) {

      // -- creation d un nouveau graphe --//
      if (boxCalques_ != null) {
        final TrPostVisuPanel calqueChoisi = listeCalquesPossibles.get(boxCalques_.getSelectedIndex());

        // -- methode appelee qui doit creer un graphe avec la selection --//
        createNewGraphe(calqueChoisi);
      } else {
        createNewGraphe(listeCalquesPossibles.get(0));
      }
      // -- on doit remettre a jour les infos --//
      // frame_.dispose();
      // rechercheGrapheAndCalque();

    } else {
      rechercheGrapheAndCalque();

      // -- affichage de al dialog --//
      constructDialog().setVisible(true);
    }

  }

  protected abstract void createNewGraphe(TrPostVisuPanel _calqueChoisi);

  public abstract void executeAction(TrPostCommonImplementation _impl, EGGraphe _graphe, TrPostVisuPanel _calque);

  private void rechercheGrapheAndCalque() {
    final Map params = new HashMap();
    CtuluLibImage.setCompatibleImageAsked(params);
    // -- recuperation de la liste des nodes de la scene --//
    final Set<EbliNode> listeNode = (Set<EbliNode>) scene_.getObjects();
    modelGraphesPossibles = new DefaultListModel();
    modelGraphesChoisis = new DefaultListModel();
    listeObjetsCalques = new ArrayList<JLabel>();
    listeCalquesPossibles = new ArrayList<TrPostVisuPanel>();
    listeGraphesPossibles = new ArrayList<EGGraphe>();
    listeGraphesChoisis = new ArrayList<EGGraphe>();
    // -- tentative de recuperation du calque --//
    for (final Iterator<EbliNode> it = listeNode.iterator(); it.hasNext();) {
      final EbliNode currentNode = it.next();
      if (currentNode.getCreator() instanceof EbliWidgetCreatorVueCalque) {
        final EbliWidgetCreatorVueCalque new_name = (EbliWidgetCreatorVueCalque) currentNode.getCreator();
        listeCalquesPossibles.add((TrPostVisuPanel) new_name.getCalquesPanel());

        final JLabel label = new JLabel();
        final BufferedImage image = new_name.getCalquesPanel().produceImage(80, 50, params);
        final Icon icone = new ImageIcon(image);
        label.setIcon(icone);
        label.setText(currentNode.getTitle());
        listeObjetsCalques.add(label);

      } else if (currentNode.getCreator() instanceof EbliWidgetCreatorGraphe) {
        final EbliWidgetCreatorGraphe new_name = (EbliWidgetCreatorGraphe) currentNode.getCreator();

        if (isCorrectGraph(new_name.getGraphe().getModel())) {

          final JLabel label = new JLabel();
          final BufferedImage image = new_name.getGraphe().produceImage(80, 50, params);
          final Icon icone = new ImageIcon(image);

          label.setIcon(icone);
          label.setText(currentNode.getTitle());
          modelGraphesPossibles.addElement(label);
          listeGraphesPossibles.add(new_name.getGraphe());
        }
      }
    }
    if (listeCalquesPossibles.size() == 0) {
      new BuDialogError(impl_.getApp(), impl_.getInformationsSoftware(), TrResource
              .getS("Il n'y a pas de calques disponibles")).activate();
    }

  }

  public abstract boolean isCorrectGraph(EGGrapheModel _model);

  /**
   * construit l interface
   *
   * @return
   */
  public JComponent constructInterface(final boolean chooserCalque, final boolean closeDialog,
          final boolean valideAjoutCourbe, final boolean canCreate) {

    final JPanel content = new JPanel(new BorderLayout());

    if (chooserCalque) {
      boxCalques_ = new JComboBox();
      for (final Iterator<JLabel> it = listeObjetsCalques.iterator(); it.hasNext();) {
        boxCalques_.addItem(it.next());
      }
      boxCalques_.setRenderer(new GrapheCellRenderer());
      boxCalques_.setBorder(BorderFactory.createTitledBorder(TrResource.TR.getString("Liste des calques de la sc�ne")));
      content.add(boxCalques_, BorderLayout.NORTH);

    }
    jlisteGraphesChoix = new JList(modelGraphesPossibles);
    jlisteGraphesChoix.setCellRenderer(new GrapheCellRenderer());

    jlisteGraphesChoix.setSize(250, 350);// FIXME a enlever car c'est inutile : a moins d'utiliser setPreferredSize
    jlisteGraphesChoix.setBorder(BorderFactory.createTitledBorder(TrResource.TR.getString("Graphes possibles")));
    jlisteGraphesSelections = new JList(modelGraphesChoisis);
    jlisteGraphesSelections.setSize(250, 350);
    jlisteGraphesSelections.setBorder(BorderFactory.createTitledBorder(TrResource.TR.getString("Graphes choisis")));
    jlisteGraphesSelections.setCellRenderer(new GrapheCellRenderer());

    final JPanel panelControle = new JPanel(new FlowLayout(FlowLayout.CENTER));

    select = new JButton(TrResource.TR.getString("Ajouter"), EbliResource.EBLI.getIcon("crystal22_avancervite"));
    unselect = new JButton(TrResource.TR.getString("Enlever"), EbliResource.EBLI.getIcon("crystal22_reculervite"));
    select.addActionListener(this);
    unselect.addActionListener(this);
    if (closeDialog) {
      quitter = new JButton(TrResource.TR.getString("Quitter"), EbliResource.EBLI.getIcon("crystal_detruire"));
      quitter.addActionListener(this);
      panelControle.add(quitter);
    }
    if (canCreate) {
      creationNewGraphe = new JButton(TrResource.TR.getString("Cr�er un nouveau graphe"), EbliResource.EBLI
              .getIcon("crystal_valider"));
      creationNewGraphe.addActionListener(this);
      panelControle.add(creationNewGraphe);
    }
    if (valideAjoutCourbe) {
      validation = new JButton(TrResource.TR.getString("Valider"), EbliResource.EBLI.getIcon("crystal_valider"));
      validation.addActionListener(this);
      panelControle.add(validation);
    }
    // Box box = Box.createHorizontalBox();
    final JPanel box = new JPanel(new GridLayout(1, 3));
    box.setSize(550, 350);// FIXME a enlever car c'est inutile : a moins d'utiliser setPreferredSize
    box.add(jlisteGraphesChoix);
    final Box boxButton = Box.createVerticalBox();
    JPanel panSelect = new JPanel(new FlowLayout(FlowLayout.CENTER));
    panSelect.add(select);
    boxButton.add(panSelect);
    boxButton.add(Box.createGlue());
    panSelect = new JPanel(new FlowLayout(FlowLayout.CENTER));
    panSelect.add(unselect);
    boxButton.add(panSelect);
    box.add(boxButton);
    box.add(jlisteGraphesSelections);

    content.add(box, BorderLayout.CENTER);

    content.add(panelControle, BorderLayout.SOUTH);

    return content;
  }

  public JDialog constructDialog() {

    frame_ = new JDialog();

    frame_.setContentPane(constructInterface(true, true, true, true));
    frame_.setModal(true);
    setTitleForFrame();

    // frame_.setIconImage(null);
    frame_.setSize(550, 400);
    frame_.pack();
    return frame_;
  }

  /**
   * Methode qui construit le panel de selection ou creation des graphes pour les points du calque selectionnes
   *
   * @return
   */
  public JComponent getInterfaceComplete(final boolean closeDialog, final boolean valideAjoutCourbe,
          final boolean canCreate) {
    rechercheGrapheAndCalque();
    return constructInterface(true, closeDialog, valideAjoutCourbe, canCreate);
  }

  public JComponent getInterfacePartielle(final boolean closeDialog, final boolean valideAjoutCourbe,
          final boolean canCreate) {
    rechercheGrapheAndCalque();

    return constructInterface(false, closeDialog, valideAjoutCourbe, canCreate);
  }

  @Override
  public void setEnabled(final boolean enabled) {
    if (this.boxCalques_ != null) {
      this.boxCalques_.setEnabled(enabled);
    }
    this.jlisteGraphesChoix.setEnabled(enabled);
    this.jlisteGraphesSelections.setEnabled(enabled);
    this.select.setEnabled(enabled);
    this.unselect.setEnabled(enabled);

  }

  public abstract void setTitleForFrame();
}
