/*
 *  @creation     11 mai 2005
 *  @modification $Date: 2007-04-26 14:40:34 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author Fred Deniger
 * @version $Id: TrPostFilterHauteurElement.java,v 1.1 2007-04-26 14:40:34 deniger Exp $
 */
public class TrPostFilterHauteurElement extends TrPostFilterHauteur {

  EfGridInterface grid_;
  EfData nodeData_;

  private EfData buildNodeData() {
    final double[] val = new double[grid_.getPtsNb()];
    final int[] nb = new int[val.length];
    for (int i = grid_.getEltNb() - 1; i >= 0; i--) {
      final EfElement el = grid_.getElement(i);
      for (int j = el.getPtNb() - 1; j >= 0; j--) {
        final int idxPt = el.getPtIndex(j);
        nb[idxPt]++;
        val[idxPt] = val[idxPt] + super.hauteur_.getValue(i);
      }
    }
    for (int i = val.length - 1; i >= 0; i--) {
      if (nb[i] > 1) {
        val[i] = val[i] / nb[i];
      }
    }
    return new EfDataNode(val);
  }

  /**
   *
   */
  public TrPostFilterHauteurElement() {
    super();
  }

  /**
   * @param _eps
   */
  public TrPostFilterHauteurElement(final double _eps) {
    super(_eps);
  }

  /**
   * @param _data
   */
  public TrPostFilterHauteurElement(final TrPostFilterHauteur _data) {
    super(_data);
  }

  @Override
  public boolean isActivated(final int _idxPt) {
    FuLog.trace("PST: rubar build value for node filter");
    if (nodeData_ == null) {
      nodeData_ = buildNodeData();
    }
    return nodeData_ != null && nodeData_.getValue(_idxPt) >= eps_;
  }

  @Override
  public boolean isActivatedElt(final int _idxElt) {
    if (hauteur_ == null) {
      return true;
    }
    if (isDelta) {
      return Math.abs(hauteur_.getValue(_idxElt)) > eps_;
    }
    return hauteur_.getValue(_idxElt) > eps_;
  }

  @Override
  public void updateTimeStep(final int _t, final EfGridData _src) {
    super.updateTimeStep(_t, _src);
    grid_ = _src.getGrid();
  }

}
