/*
 *  @file         TrBcboundaryMiddleModel.java
 *  @creation     21 nov. 2003
 *  @modification $Date: 2006-09-19 15:07:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr.data;

import java.util.List;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dBcFrontierMiddleInterface;
import org.fudaa.ebli.calque.ZModeleDonneesMulti;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * @author deniger
 * @version $Id: TrBcBoundaryMiddleModel.java,v 1.13 2006-09-19 15:07:28 deniger Exp $
 */
public interface TrBcBoundaryMiddleModel extends ZModeleDonneesMulti {
  /**
   * Le nombre de frontiere du maillage.
   */
  int getNbFrontier();

  /**
   * Renvoie les bords de la frontiere _i.
   */
  H2dBcFrontierMiddleInterface getFrontier(int _idxFrontier);

  /**
   * @return la liste des bords utilises.
   */
  List getUsedBoundaryType();

  EfGridInterface getGrid();

  /**
   * @return le nombre de type de bords geree.
   */
  int getNbBoundaryType();

  /**
   * Initialise <code>_p</code> a partir des donnees du point d'indice <code>_idxGlobal</code> sur le domaine.
   */
  void getPoint(GrPoint _p, int _idxFr, int _idxPtOnFrontier);

  int getNbTotalPt();

  int getGlobalIdx(int _idxFr, int _idxOnFrontier);

  int getFrontiereIndice(int _idxFr, int _idxOnFr);

  /**
   * @return la liste de tous les bords utilisables
   */
  List getBordList();
}
