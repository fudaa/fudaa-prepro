/*
 * @creation 30 avr. 07
 * 
 * @modification $Date: 2007-04-30 14:22:40 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import org.fudaa.dodico.h2d.H2dSIListener;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author fred deniger
 * @version $Id: TrRubarVisuPanelModelListener.java,v 1.1 2007-04-30 14:22:40 deniger Exp $
 */
public class TrRubarVisuPanelModelListener implements H2dSIListener, H2dRubarApportListener, H2dRubarVentListener, H2dRubarOuvrageListener,
    H2dRubarLimniListener, H2DRubarFrictionListener, H2DRubarDiffusionListener, H2dRubarSedimentListener {

  final TrRubarVisuPanel pn_;

  public TrRubarVisuPanelModelListener(final TrRubarVisuPanel _pn) {
    super();
    pn_ = _pn;
    final H2dRubarParameters params = _pn.getRubarParams();
    params.getSi().addListener(this);
    params.getAppMng().addAppListener(this);
    params.getVentMng().addVentListener(this);
    params.getSedimentMng().addSedimentListener(this);
    params.getOuvrageMng().addListener(this);
    params.getLimniMng().addListener(this);
    params.addDiffusionListener(this);
    params.getFriction().addListener(this);
  }

  public void clearListener() {
    final H2dRubarParameters params = pn_.getRubarParams();
    params.getSi().removeListener(this);
    params.getAppMng().removeAppListener(this);
    params.getVentMng().removeVentListener(this);
    params.getSedimentMng().removeListener(this);
    params.getOuvrageMng().removeListener(this);
    params.getLimniMng().removeListener(this);
    params.removeDiffusionListener(this);
    params.getFriction().removeListener(this);

  }

  @Override
  public void apportChanged(final H2dRubarApportSpatialMng _mng) {
    pn_.updateInfoAndIso();
  }

  @Override
  public void ventChanged(H2dRubarVentMng _mng) {
    pn_.updateInfoAndIso();
  }

  @Override
  public void sedimentChanged(H2dRubarSedimentMng _mng) {
    pn_.updateInfoAndIso();
  }

  @Override
  public void apportEvolutionContentChanged(final H2dRubarApportSpatialMng _mng, final EvolutionReguliereInterface _dest) {
  }

  @Override
  public void ventEvolutionContentChanged(H2dRubarVentMng _mng, EvolutionReguliereInterface _dest) {
  }

  @Override
  public void apportEvolutionUsedChanged(final H2dRubarApportSpatialMng _mng, final EvolutionReguliereInterface _dest) {
  }

  @Override
  public void ventEvolutionUsedChanged(H2dRubarVentMng _mng, EvolutionReguliereInterface _dest) {
  }

  @Override
  public void diffusionChanged(final boolean _addOrRemove) {
    pn_.updateInfoAndIso();
    if (_addOrRemove) {
      pn_.updateIsoVariables();
    }

  }

  @Override
  public void frictionChanged() {
    pn_.updateInfoAndIso();
  }

  @Override
  public void limniPointChanged(final H2dRubarLimniMng _mng) {
    pn_.updateInfoAndIso();
  }

  @Override
  public void limniTimeStepChanged(final H2dRubarLimniMng _mng) {
  }

  @Override
  public void ouvrageAdded(final H2dRubarOuvrageMng _mng) {
    pn_.updateInfoAndIso();

  }

  @Override
  public void ouvrageChanged(final H2dRubarOuvrageMng _mng, final H2dRubarOuvrage _ouv, final boolean _ouvrageElementaireAddedOrRemoved) {
    pn_.updateInfoAndIso();
  }

  @Override
  public void ouvrageElementaireChanged(final H2dRubarOuvrageMng _mng, final H2dRubarOuvrage _ouv, final H2dRubarOuvrageElementaireInterface _i) {
    pn_.updateInfoAndIso();

  }

  @Override
  public void ouvrageRemoved(final H2dRubarOuvrageMng _mng) {
    pn_.updateInfoAndIso();

  }

  @Override
  public void siAdded(final H2dVariableType _var) {
    pn_.siChanged();
  }

  @Override
  public void siChanged(final H2dVariableType _var) {
    pn_.siChanged();
  }

  @Override
  public void siRemoved(final H2dVariableType _var) {
    pn_.siChanged();
  }

}
