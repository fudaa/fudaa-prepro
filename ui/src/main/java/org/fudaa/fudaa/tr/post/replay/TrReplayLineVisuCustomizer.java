/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.replay;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.LineString;
import java.awt.Color;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeDefault;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.sig.layer.FSigLayerLine;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostWizardProfilSpatial;
import org.fudaa.fudaa.tr.post.profile.MvProfileCourbeModelInterface;

/**
 *
 * @author Frederic Deniger
 */
public final class TrReplayLineVisuCustomizer implements TrReplayVisuCustomizer {
  final MvProfileCourbeModelInterface modele;
  final String title;

  /**
   * @param listePoints
   */
  /**
   * @param listePoints
   */
  public TrReplayLineVisuCustomizer(final MvProfileCourbeModelInterface modele, String title) {
    super();
    this.modele = modele;
    this.title = title;
  }

  @Override
  public void customizeVisu(final TrPostVisuPanel vue2d) {
    final GISZoneCollectionLigneBrisee ligneBrisee = new GISZoneCollectionLigneBrisee();
    ligneBrisee.addCoordinateSequence(modele.getInitLine().getCoordinateSequence(), null, null);
    final ZModeleLigneBriseeDefault modeleDefault = new ZModeleLigneBriseeDefault(ligneBrisee);
    final ZCalqueLigneBrisee calqueZ = new FSigLayerLine(modeleDefault);
    calqueZ.setTitle(title);
    final FSigLayerGroup group = TrPostWizardProfilSpatial.getGroup(TrPostWizardProfilSpatial.getGroupTraceName(), vue2d);
    TraceLigneModel model = new TraceLigneModel(TraceLigne.LISSE, 1, Color.BLUE);
    calqueZ.setLineModel(0, model);
    TraceIconModel iconModel = TrReplayVue2dBuilder.createTraceIconModel();
    calqueZ.setIconModel(0, iconModel);
    calqueZ.setLineModel(1, model);
    calqueZ.setIconModel(1, iconModel);
    group.add(calqueZ);
    calqueZ.setVisible(true);
    calqueZ.setDestructible(true);
    vue2d.setCalqueActif(calqueZ);
  }

  public Object[][] getData() {
    final LineString line = modele.getInitLine();
    final int numPoints = line.getNumPoints();
    final Object[][] dataP = new Object[numPoints][2];
    final CoordinateSequence coordinateSequence = line.getCoordinateSequence();
    for (int i = 0; i < numPoints; i++) {
      dataP[i][0] = coordinateSequence.getX(i);
      dataP[i][1] = coordinateSequence.getY(i);
    }
    return dataP;
  }
  
}
