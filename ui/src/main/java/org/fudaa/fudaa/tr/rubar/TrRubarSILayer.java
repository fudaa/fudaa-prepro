/*
 *  @creation     14 juin 2004
 *  @modification $Date: 2007-05-04 14:01:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.rubar;

import java.awt.Graphics2D;
import org.fudaa.dodico.h2d.H2dSiSourceInterface;
import org.fudaa.dodico.h2d.H2dVariableProviderContainerInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.find.EbliFindable;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.meshviewer.layer.MvFindActionNodeElt;
import org.fudaa.fudaa.meshviewer.model.MvElementModel;
import org.fudaa.fudaa.tr.data.TrExpressionSupplierForData;
import org.fudaa.fudaa.tr.data.TrSiLayer;
import org.fudaa.fudaa.tr.data.TrVisuPanelEditor;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarSILayer.java,v 1.17 2007-05-04 14:01:53 deniger Exp $
 */
public class TrRubarSILayer extends MvElementLayer implements TrSiLayer {

  /**
   * @author fred deniger
   * @version $Id: TrRubarSILayer.java,v 1.17 2007-05-04 14:01:53 deniger Exp $
   */
  static final class SiFindContainer extends MvFindActionNodeElt {
    /**
     * @param _layer
     */
    SiFindContainer(final MvElementLayer _layer) {
      super(_layer);
    }

    @Override
    public String editSelected(final EbliFindable _parent) {
      return ((TrVisuPanelEditor) _parent).editSiNodeOrElement();
    }
  }

  final H2dRubarParameters param_;

  /**
   * @param _modele
   */
  public TrRubarSILayer(final MvElementModel _modele, final H2dRubarParameters _param) {
    super(_modele);
    param_ = _param;
  }

  @Override
  public H2dSiSourceInterface getSource() {
    return param_.getSi();
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return new TrExpressionSupplierForData.Elements(new H2dVariableProviderContainerInterface[] {
        param_.getSiProvider(), param_.getElementProvider() }, (MvElementModel) this.modele_);
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return new SiFindContainer(this);
  }

  @Override
  public boolean isFontModifiable() {
    return false;
  }

  @Override
  public boolean isPaletteModifiable() {
    return false;
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
      final GrBoite _clipReel) {}
}