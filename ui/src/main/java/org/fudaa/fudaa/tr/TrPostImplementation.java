/**
 * @creation 24 mai 2004
 * @modification $Date: 2006-09-19 15:07:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr;

import com.memoire.bu.BuIcon;
import com.memoire.bu.BuInformationsSoftware;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostLayoutFille;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.actions.TrPostActionOpenSrc;

/**
 * @author Fred Deniger
 * @version $Id: TrPostImplementation.java,v 1.16 2006-09-19 15:07:28 deniger Exp $
 */
public class TrPostImplementation extends TrPostCommonImplementation {

  @Override
  public BuIcon getSpecificIcon() {
    return TrResource.getPostIcon();
  }

  @Override
  public String getSpecificName() {
    return TrResource.getPostName();
  }

  public TrPostImplementation() {
    super();
  }

  @Override
  public void doImportProject() {
    
    TrPostLayoutFille currentLayoutFilleOrFirst = getCurrentLayoutFilleOrFirst();
    if (currentLayoutFilleOrFirst != null) {
      TrPostActionOpenSrc open=new TrPostActionOpenSrc(getCurrentProject());
      TrPostSource openSource = open.openSource();
      if(openSource!=null){
        currentLayoutFilleOrFirst.controller_.addSource(openSource);
      }

    }

  }

  @Override
  public void init() {
    super.init();
    buildTaskView();
    
  }

  @Override
  public BuInformationsSoftware getInformationsSoftware() {
    return TrLauncherDefault.getInfosSoftware();
  }

  @Override
  public void start() {
    super.start();
    setEnabledForAction("OUVRIR", true);
  }

}