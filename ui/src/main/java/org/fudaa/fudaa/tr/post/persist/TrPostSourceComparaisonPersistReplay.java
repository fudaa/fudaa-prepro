package org.fudaa.fudaa.tr.post.persist;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourceComparator;
import org.fudaa.fudaa.tr.post.TrPostSourceComparatorBuilder;

public class TrPostSourceComparaisonPersistReplay implements TrPostSourceReplayPersist {

  final String destSourceId;
  String id;

  final boolean isGridEquals;
  final String refSourceId;
  final boolean resulatInverse;
  final boolean useProjGrid;
  final boolean useProjTime;

  /**
   * @param id
   * @param refSourceId
   * @param destSourceId
   * @param resulatInverse
   * @param isGridEquals
   * @param useProjGrid
   * @param useProjTime
   */
  public TrPostSourceComparaisonPersistReplay(final String refSourceId, final String destSourceId,
      final boolean resulatInverse, final boolean isGridEquals, final boolean useProjGrid, final boolean useProjTime) {
    super();
    this.refSourceId = refSourceId;
    this.destSourceId = destSourceId;
    this.resulatInverse = resulatInverse;
    this.isGridEquals = isGridEquals;
    this.useProjGrid = useProjGrid;
    this.useProjTime = useProjTime;
  }

  /**
   * @return the destSourceId
   */
  public String getDestSourceId() {
    return destSourceId;
  }

  public String getId() {
    return id;
  }

  /**
   * @return the refSourceId
   */
  public String getRefSourceId() {
    return refSourceId;
  }

  /**
   * @return the isGridEquals
   */
  public boolean isGridEquals() {
    return isGridEquals;
  }

  /**
   * @return the resulatInverse
   */
  public boolean isResulatInverse() {
    return resulatInverse;
  }

  /**
   * @return the useProjGrid
   */
  public boolean isUseProjGrid() {
    return useProjGrid;
  }

  /**
   * @return the useProjTime
   */
  public boolean isUseProjTime() {
    return useProjTime;
  }

  @Override
  public TrPostSource replay(final TrPostProjet projet, final CtuluAnalyze log) {
    final TrPostSource init = projet.getSources().getSource(refSourceId);
    final TrPostSource dest = projet.getSources().getSource(destSourceId);
    final TrPostSourceComparator res = TrPostSourceComparatorBuilder.createSrcComparator(init, dest, this, projet
        .getImpl(), null);
    res.setId(id);
    return res;
  }

  public void setId(final String id) {
    this.id = id;
  }

}
