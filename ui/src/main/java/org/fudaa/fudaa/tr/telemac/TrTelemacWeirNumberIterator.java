/**
 * @creation 24 f�vr. 2005 @modification $Date: 2007-03-30 15:40:30 $ @license GNU General Public License 2 @copyright
 *     (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.telemac;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuil;

import javax.swing.table.TableCellEditor;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacWeirNumberIterator.java,v 1.7 2007-03-30 15:40:30 deniger Exp $
 */
public class TrTelemacWeirNumberIterator extends TrTelemacFrontierNumberIteratorAbstract {
  // H2dTelemacSeuilMng mng_;
  H2dTelemacSeuil seuil_;

  public TrTelemacWeirNumberIterator(final H2dTelemacSeuil _seuil, final double[] _x) {
    super(_x);
    // mng_ = _mng;
    seuil_ = _seuil;
  }



  @Override
  public String formatIntValue(final int _v) {
    return CtuluLibString.getString(seuil_.getPtIdx1(_v) + 1);
  }

  @Override
  public boolean isEditable() {
    return true;
  }

  @Override
  public CtuluNumberFormatI getCopy() {
    return this;
  }

  @Override
  public String getValidationMessage() {
    return null;
  }

  @Override
  public void setEditable(boolean editable) {
  }

  @Override
  public int getNbPt() {
    return seuil_.getNbPoint();
  }

  @Override
  boolean isIndiceValid(final int _idx) {
    return seuil_.isUsedIdx(_idx);
  }

  @Override
  public TableCellEditor createCommonTableEditorComponent() {
    return createTableEditorComponent();
  }
}
