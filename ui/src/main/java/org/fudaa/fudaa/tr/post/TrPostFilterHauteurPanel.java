/*
 *  @creation     3 mai 2005
 *  @modification $Date: 2007-04-16 16:35:32 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluValueValidator;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrPostFilterHauteurPanel.java,v 1.4 2007-04-16 16:35:32 deniger Exp $
 */
public class TrPostFilterHauteurPanel implements ActionListener {

  private final BuTextField tfEps_;
  BuLabel lbEps_;

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if ("SET_AS_DEF".equals(_e.getActionCommand()) && tfEps_.getText() != null && tfEps_.getText().trim().length() > 0) {
      final double d = ((Double) tfEps_.getValue()).doubleValue();
      if (d >= 0) {
        TrPostFilterHauteur.setEpsInPref(d);
      }
    }
  }

  /**
   * @param _src la source contenant les variables
   */
  public TrPostFilterHauteurPanel(final TrPostSource _src) {
    lbEps_ = new BuLabel(TrResource.getS("Valeur minimale pour la hauteur d'eau:"));
    final StringBuffer buf = new StringBuffer("<html>");
    if (_src.isElementVar(H2dVariableType.HAUTEUR_EAU)) {
      buf.append("<p>");
      buf
              .append(TrResource
              .getS("Les �l�ments pour lesquels la hauteur d'eau est inf�rieure � la valeur suivante ne seront pas pris en compte dans la vue 2D"));
      buf.append("</p>");
    } else {
      buf.append("<p>");
      buf
              .append(TrResource
              .getS("Les noeuds pour lesquels la hauteur d'eau est inf�rieure � la valeur suivante ne seront pas pris en compte dans la vue 2D"));
      buf.append("</p>");
    }

    buf.append("</html>");
    lbEps_.setToolTipText(buf.toString());
    tfEps_ = BuTextField.createDoubleField();
    tfEps_.setColumns(7);
    tfEps_.setValueValidator(new CtuluValueValidator.DoubleMin(0));
    tfEps_.setToolTipText(((CtuluValueValidator) tfEps_.getValueValidator()).getDescription());
  }

  public TrPostFilterHauteur getCondition() {
    final double eps = ((Double) tfEps_.getValue()).doubleValue();

    return new TrPostFilterHauteur(eps);
  }

  public void updateFor(final TrPostFilterHauteur _h) {
    if (_h != null) {
      tfEps_.setValue(CtuluLib.getDouble(_h.getEps()));
    }
  }

  public JPanel buildPanel() {
    final JPanel rValue = new BuPanel();
    rValue.setLayout(new BuGridLayout(2, 5, 5));
    rValue.add(lbEps_);
    rValue.add(tfEps_);
    return rValue;
  }

  /**
   * @param _b le nouvel etat d'activation
   */
  public void setEnable(final boolean _b) {
    tfEps_.setEnabled(_b);
  }

  /**
   * @return true si valid
   */
  public boolean isDataValid() {
    final boolean r = tfEps_.getValue() != null && tfEps_.getValueValidator().isValueValid(tfEps_.getValue());
    lbEps_.setForeground(r ? Color.BLACK : Color.RED);
    return r;
  }
}
