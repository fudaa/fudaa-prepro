package org.fudaa.fudaa.tr.post;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFilleWithComponent;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.impression.EbliPageFormat;
import org.fudaa.ebli.impression.EbliPageable;
import org.fudaa.ebli.impression.EbliPageableDelegate;
import org.fudaa.ebli.impression.EbliPrinter;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.*;
import org.fudaa.ebli.visuallibrary.actions.CommandUndoRedoCut;
import org.fudaa.ebli.visuallibrary.actions.CommandUndoRedoPaste;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionConfigure;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionDuplicate;
import org.fudaa.ebli.visuallibrary.calque.CalqueLegendeWidgetAdapter;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetCreatorLegende;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetCreatorVueCalque;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreatorTextEditor;
import org.fudaa.ebli.visuallibrary.graphe.EbliWidgetCreatorGraphe;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.TrLauncherDefault;
import org.fudaa.fudaa.tr.common.TrCourbeImporter;
import org.fudaa.fudaa.tr.common.TrCourbeImporter.Target;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.actions.TrPostActionBilan;
import org.fudaa.fudaa.tr.post.actions.TrPostActionChooseAndCreateCalque;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostWizardCreateScope;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostWizardImportScope;
import org.fudaa.fudaa.tr.post.profile.MvProfileTreeModel;
import org.netbeans.api.visual.widget.Widget;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.util.*;

/**
 * InternalFrame qui contient la scene EbliScene.
 *
 * @author Adrien Hadoux
 */
public class TrPostLayoutFille extends BuInternalFrame implements CtuluFilleWithComponent, CtuluUndoRedoInterface, BuUndoRedoInterface,
    BuCutCopyPasteInterface, CtuluSelectionInterface, EbliWidgetEditModeListener, CtuluImageProducer, EbliPageable, CtuluExportDataInterface {
  /**
   *
   */
  private static final long serialVersionUID = -2769884291563880931L;
  /**
   * scene de l internalFrame.
   */
  // final private EbliScene scene_;
  public TrPostLayoutPanelController controller_;
  final JComponent defaultSouth_ = new JLabel(TrResource.getS("Details:"));
  private EbliPageableDelegate delegueImpression_;
  boolean ecrasePrevious_ = true;
  JComponent editorEnCours_;
  BuInformationsDocument id_;
  BuMenu menuLayoutManager_;
  JMenuBar menuModifiable_ = new BuMenuBar();
  JMenu[] menus_;
  Dimension oldSize_;
  /**
   * Scroll qui contient le tree a droite. Ce panel est modifi� des que l on change de type d elements.
   */
  JComponent overview_;
  /**
   * palette d infos pour le calque associe.
   */
  JComponent palette_;
  /**
   * Panel qui contient le tree modifiable
   */
  // JPanel conteneurTree_;
  JComponent panelSouthSuiviSouris_;
  public String previousTitleFrame = getTitle();
  TrPostProjet projet_;
  JComponent right_;
  /**
   * Toolbar modifiable qui se met a jour selon la selection de la widget.
   */
  JToolBar toolBarModifiable_ = new BuToolBar();
  JComponent[] tools_;

  /**
   * Constructeur de la fenetre.
   */
  public TrPostLayoutFille(final TrPostProjet _projet) {
    super();
    setTitle(TrResource.getS("Vue 2D"));
    setFrameIcon(EbliResource.EBLI.getToolIcon("lissage"));
    previousTitleFrame = getTitle();
    projet_ = _projet;
    // creation de la scene EBLI
    controller_ = new TrPostLayoutPanelController(new TrPostScene(_projet), _projet);
    setContentPane(controller_.getPanel());
    controller_.getSceneCourante().getController().addEditListener(this);

    // -- ajout de la toolbar et du menu en haut --//
    setToolBar(toolBarModifiable_);
    this.setJMenuBar(menuModifiable_);
    changeToolbarScene();
    // -- init du panel de choix des trees --//
    overview_ = controller_.getSceneCourante().getController().getOverviewComponent();

    panelSouthSuiviSouris_ = new JLabel();
    this.add(panelSouthSuiviSouris_, BorderLayout.SOUTH);
  }

  public TrPostProjet getProject() {
    return projet_;
  }

  /**
   * @param _title
   * @param _preferredLocation
   * @param _preferedDimension
   * @param _calque
   * @see org.fudaa.fudaa.tr.post.TrPostLayoutPanelController#addCalque(java.lang.String, java.awt.Point, java.awt.Dimension,
   *     org.fudaa.ebli.calque.ZEbliCalquesPanel)
   */
  public EbliNode addCalque(final String _title, final Point _preferredLocation, final Dimension _preferedDimension, final TrPostVisuPanel _calque,
                            final CalqueLegendeWidgetAdapter _legende) {
    return controller_.addCalque(_title, _preferredLocation, _preferedDimension, _calque);
  }

  /**
   * @param _node
   * @see org.fudaa.fudaa.tr.post.TrPostLayoutPanelController#addNode(org.fudaa.ebli.visuallibrary.EbliNode)
   */
  public Widget addNode(final EbliNode _node) {
    return controller_.addNode(_node);
  }

  /**
   * change le component south de la frame. dans le cas du calque il s agit du suivi souris.
   *
   * @param _controller
   */
  private void changeSouth(final EbliWidgetInterface _controller) {
    if (panelSouthSuiviSouris_ != null) {
      this.remove(this.panelSouthSuiviSouris_);
    }
    this.panelSouthSuiviSouris_ = _controller.getController().getTracableComponent();
    if (panelSouthSuiviSouris_ == null) {
      panelSouthSuiviSouris_ = defaultSouth_;
    }
    this.add(panelSouthSuiviSouris_, BorderLayout.SOUTH);
    this.revalidate();
  }

  /**
   * Methode qui modifie la toolbar associee au calque selectionne
   *
   * @param _calque
   */
  private void changeToolbar(final EbliWidgetControllerInterface _controller) {

    // nettoyage de la toolbar
    this.remove(toolBarModifiable_);
    this.remove(menuModifiable_);
    // toolBarModifiable_.removeAll();
    toolBarModifiable_ = _controller.getToolbarComponent();
    menuModifiable_ = _controller.getMenubarComponent();

    // ajoutMenusLayoutTabbed(menuModifiable_);

    // mise a jour des params
    // toolBarModifiable_.revalidate();
    this.setToolBar(toolBarModifiable_);
    this.setJMenuBar(menuModifiable_);

    this.revalidate();
  }

  public void changeToolbarScene() {
    // nettoyage de la toolbar
    this.remove(toolBarModifiable_);
    this.remove(menuModifiable_);

    toolBarModifiable_ = new BuToolBar();
    menuModifiable_ = new BuMenuBar();

    toolBarModifiable_.add(new EbliWidgetActionConfigure(controller_.getSceneCourante()));
    final JMenu menu = new JMenu(TrResource.getS("Configurer"));
    menu.add(new EbliWidgetActionConfigure(controller_.getSceneCourante()));
    menuModifiable_.add(menu);
    final JMenu menuImporter = new JMenu(TrResource.getS("Importer"));
    menuImporter.add(new TrPostActionChooseAndCreateCalque(projet_, controller_));
    menuImporter.add(new TrPostWizardCreateScope.ImportAction(getScene().projet));
    menuModifiable_.add(menuImporter);
    // mise a jour des params

    // ajoutMenusLayoutTabbed(menuModifiable_);

    this.setToolBar(toolBarModifiable_);
    this.setJMenuBar(menuModifiable_);
    this.revalidate();
  }

  private void changeToolbarSpecifiqueCalque(final EbliWidgetController _controller, final EbliWidgetCreatorVueCalque creator) {

    changeToolbar(_controller);
    this.revalidate();
  }

  /**
   * Methode appelee pour les widget qui contiennent des graphes.
   *
   * @param _controller
   * @param creator
   */
  private void changeToolbarSpecifiqueGraphe(final EbliWidgetController _controller, final EbliWidgetCreatorGraphe creator) {

    // -- change la toolbar avec els actions generiques du controller --//
    changeToolbar(_controller);

    if (!_controller.fonctionsAlreadySpecified()) {
      // -- ajout des actions tr --//
      final ArrayList<EbliActionSimple> listeActions = new ArrayList<EbliActionSimple>();
      final EGFillePanel _graphe = creator.getGraphePanel();
      if (_graphe.getGraphe().getModel() instanceof MvProfileTreeModel) {
        listeActions.add(new TrPostActionBilan(
            creator.getWidget().getEbliScene(),
            (MvProfileTreeModel) _graphe.getGraphe().getModel(), projet_.getImpl()));
      }

      // -- ajout de l'importation dans les courbes de l'action --//
      if (creator.getGraphe().getModel() instanceof Target) {
        listeActions.add(new TrPostWizardImportScope.ImportAction(projet_, (Target) creator.getGraphe().getModel()));
      }

      listeActions.add(new TrCourbeImporter.ExportAction(creator.getGraphe(), projet_.impl_));

      _controller.addFonctionsSpecific(listeActions);
    }

    // mise a jour des params
    this.revalidate();
  }

  /**
   * Methode qui met a jour le tree de droite avec le tree du calque ou graphe selectionne.
   *
   * @param _widget
   */
  @SuppressWarnings("unchecked")
  private void changeTree(final EbliWidgetInterface _widget) {
    right_.remove(overview_);
    right_.revalidate();
    overview_ = _widget.getController().getOverviewComponent();
    right_.add(overview_);
    // conteneurTree_.add(panelTreeModifiable_, BorderLayout.CENTER);
    changeSouth(_widget);
    right_.revalidate();
    right_.repaint();
  }

  @Override
  public void clearCmd(final CtuluCommandManager _source) {
    if (getCmdMng() != null && _source != getCmdMng()) {
      getCmdMng().clean();
    }
  }

  @Override
  public void clearSelection() {
    if (editorEnCours_ instanceof CtuluSelectionInterface) {
      ((CtuluSelectionInterface) editorEnCours_).clearSelection();
    }
  }

  @Override
  public void copy() {

    // JOptionPane.showMessageDialog(null, "copy()");
    // on copie la widget selectionnee
    // ie on fait une duplication partielle
    // on ajoute pas tout de suite le noeud dans la scene

    // --recuperation des noeuds a copier --//
    projet_.nodesCopyied = new HashSet<EbliNode>();

    for (final Iterator<EbliNode> it = ((Set<EbliNode>) getScene().getSelectedObjects()).iterator(); it.hasNext(); ) {
      final EbliNode node = it.next();
      if (!(node.getCreator() instanceof EbliWidgetCreatorLegende)
          && !(node.getCreator() instanceof org.fudaa.ebli.visuallibrary.graphe.EbliWidgetCreatorLegende)) {
        projet_.nodesCopyied.add(node);
      }
    }
    // -- reinit de la commande cut si on voulait faire un ctrl+x
    projet_.nodesCutted = null;
  }

  /**
   * Methode qui permet de modifier le panel sur la droite.
   */
  @Override
  public JComponent createPanelComponent() {
    if (right_ == null) {

      right_ = new JPanel(new BorderLayout());
      right_.putClientProperty(FudaaCommonImplementation.RIGHT_COMPONENT_IN_SCROLL, Boolean.FALSE);

      overview_ = controller_.getSceneCourante().getController().getOverviewComponent();
      right_.add(overview_, BorderLayout.CENTER);

      if (controller_ != null) {
        final Set<EbliNode> noeudContenus = (Set<EbliNode>) controller_.getSceneCourante().getObjects();
        if (noeudContenus.size() == 1) {
          getScene().setSelectedObjects(noeudContenus);
        }
        getScene().refresh();
      }
    }

    return right_;
  }

  @Override
  public void cut() {
    // JOptionPane.showMessageDialog(null, "cut()");

    // --recuperation des noeuds a copier --//
    projet_.nodesCutted = new HashSet<EbliNode>();

    for (final Iterator<EbliNode> it = ((Set<EbliNode>) getScene().getSelectedObjects()).iterator(); it.hasNext(); ) {
      final EbliNode node = it.next();
      if (!(node.getCreator() instanceof EbliWidgetCreatorLegende)
          && !(node.getCreator() instanceof org.fudaa.ebli.visuallibrary.graphe.EbliWidgetCreatorLegende)) {
        projet_.nodesCutted.add(node);
      }
    }
    // -- suppression des nodes depuis cetet kliste pour eviter les concourant
    // -//exception
    for (final Iterator<EbliNode> it = projet_.nodesCutted.iterator(); it.hasNext(); ) {
      final EbliNode node = it.next();

      // enlever les nodes de la scene
      // le node existe toujours apres l'avoir enleve
      getScene().removeNode(node);
    }

    // reinit de la liste des noeuds a copier au cas ou on change d avis
    projet_.nodesCopyied = null;

    // --creation de la commande undo/redo --//
    if (projet_.nodesCutted != null && projet_.nodesCutted.size() != 0) {
      getScene().getCmdMng().addCmd(new CommandUndoRedoCut(projet_.nodesCutted));
    }
  }

  @Override
  public void duplicate() {
    throw new IllegalAccessError("not supported");
  }

  @Override
  public void editStart(final Widget _w, final JComponent _editor) {
    editorEnCours_ = _editor;
    final EbliNode node = (EbliNode) controller_.getSceneCourante().findObject(_w);
    if (node == null || !node.hasWidget() || node.getWidget().getController() == null) {
      editStop(null, null);
    }
    if (node == null) {
      return;
    }
    if (node.getCreator() instanceof EbliWidgetCreatorVueCalque) {

      // -- etape 1: ajout du tree dans fudaaImplementation --//
      changeTree(node.getWidget());

      // -- etape 2: rechargement de la toolbar specifique dans this --//
      changeToolbar(node.getWidget().getController());
      changeToolbarSpecifiqueCalque(node.getWidget().getController(), (EbliWidgetCreatorVueCalque) node.getCreator());
      setTitle(previousTitleFrame + " " + node.getDescription(), false);
    } else if (node.getCreator() instanceof EbliWidgetCreatorGraphe) {
      FuLog.debug("change tree GRAPHE");
      // -- etape 1: ajout du tree dans fudaaImplementation --//
      changeTree(node.getWidget());
      // -- etape 2: rechargement de la toolbar specifique dans this --//
      changeToolbarSpecifiqueGraphe(node.getWidget().getController(), (EbliWidgetCreatorGraphe) node.getCreator());

      setTitle(previousTitleFrame + " " + node.getDescription(), false);
    } else if (node.getCreator() instanceof EbliWidgetCreatorTextEditor) {
      FuLog.debug("texte editor");
      // ((EbliWidgetCreatorTextEditor)
      // node.getCreator()).getG().setFrame(projet_.impl_.getFrame());

      // -- etape 1: on fout eventuellement le tree des layouts --//
      changeTree(getScene());
      // -- etape 2: rechargement de la toolbar specifique dans this --//
      changeToolbar(node.getWidget().getController());
      setTitle(previousTitleFrame);
    } else {
      // -- etape 1: on fout eventuellement le tree des layouts --//
      changeTree(getScene());
      // -- etape 2: rechargement de la toolbar specifique dans this --//
      changeToolbarScene();
      setTitle(previousTitleFrame);
    }
  }

  @Override
  public void editStop(final Widget _w, final JComponent _editor) {
    editorEnCours_ = null;
    changeTree(getScene());
    changeToolbarScene();
    setTitle(previousTitleFrame);
  }

  @Override
  public void find() {
    if (editorEnCours_ instanceof CtuluSelectionInterface) {
      ((CtuluSelectionInterface) editorEnCours_).find();
    }
  }

  public ZEbliCalquesPanel getCalquePrincipal() {
    return controller_.getCalquePrincipal();
  }

  @Override
  public CtuluCommandManager getCmdMng() {
    return getScene().getCmdMng();
  }

  @Override
  public Class getComponentClass() {
    return JPanel.class;
  }

  @Override
  public String getComponentTitle() {
    return TrLib.getString("Layout");
  }

  @Override
  public EbliPageFormat getDefaultEbliPageFormat() {
    if (delegueImpression_ == null) {
      delegueImpression_ = new EbliPageableDelegate(this);
    }
    return delegueImpression_.getDefaultEbliPageFormat();
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return getScene().getDefaultImageDimension();
  }

  @Override
  public String[] getEnabledActions() {
    return new String[]{"TOUTSELECTIONNER", "INVERSESELECTION", "CLEARSELECTION", "IMPRIMER", "MISEENPAGE", "PREVISUALISER",
        CtuluLibImage.SNAPSHOT_COMMAND, "COLLER", "COPIER", "COUPER", CtuluExportDataInterface.EXPORT_CMD};
  }

  // ---methode du listener d ecoute de la scene
  // /**
  // * Called to notify that an object was added to an object scene. This is called when an object-widget mapping is
  // * registered in an ObjectScene only. At the moment of the call, the object is still not reqistered in the
  // Graph*Scene
  // * classes yet. Therefore do not use the methods of Graph*Scene.
  // *
  // * @param event
  // * @param addedObject
  // */
  // public void objectAdded(ObjectSceneEvent event, Object addedObject) {}
  //
  // /**
  // * Called to notify that an object was removed from an object scene. This is called when an object-widget mapping is
  // * unregistered in an ObjectScene and Graph*Scene classes. At the moment of the call, a widget (visual
  // representation
  // * of the object) is still in the scene. Therefore do not rely on a tree of widgets of the scene.
  // *
  // * @param event the object scene event
  // * @param removedObject the removed object
  // */
  // public void objectRemoved(ObjectSceneEvent event, Object removedObject) {}
  //
  // /**
  // * Called to notify that the object state of an object is changed. This method is always called before any other
  // * ObjectSceneListener method is called.
  // *
  // * @param event the object scene event
  // * @param changedObject the object with changed object state
  // * @param previousState the previous object state
  // * @param newState the new object state
  // */
  // public void objectStateChanged(ObjectSceneEvent event, Object changedObject, ObjectState previousState,
  // ObjectState newState) {
  //
  // }
  @Override
  public BuInformationsDocument getInformationsDocument() {
    if (id_ == null) {
      id_ = new BuInformationsDocument();
    }
    return id_;
  }

  @Override
  public BuInformationsSoftware getInformationsSoftware() {
    return TrLauncherDefault.getInfosSoftware();
  }

  @Override
  public int getNumberOfPages() {
    return 1;
  }

  @Override
  public PageFormat getPageFormat(final int _i) {
    if (delegueImpression_ == null) {
      delegueImpression_ = new EbliPageableDelegate(this);
    }
    return delegueImpression_.getPageFormat(_i);
  }

  @Override
  public Printable getPrintable(final int _i) {
    if (delegueImpression_ == null) {
      delegueImpression_ = new EbliPageableDelegate(this);
    }
    return delegueImpression_.getPrintable(_i);
  }

  public TrPostScene getScene() {
    return controller_.getSceneCourante();
  }

  @Override
  public JMenu[] getSpecificMenus() {
    if (menus_ == null) {
      menus_ = new JMenu[]{controller_.createMenu()};
      menus_[0].setText(getTitle());
    }
    return menus_;
  }

  @Override
  public JComponent[] getSpecificTools() {
    if (tools_ == null) {
      tools_ = controller_.createSpecificComponent(getDesktopPane(), this);
    }
    return tools_;
  }

  /**
   * Methode utilisee dans la classe fille TrPostFille pour recuperer le calque principal et enregistrer.
   */
  public final ZEbliCalquesPanel getVisuPanel() {
    return getCalquePrincipal();
  }

  @Override
  public void inverseSelection() {
    if (editorEnCours_ instanceof CtuluSelectionInterface) {
      ((CtuluSelectionInterface) editorEnCours_).inverseSelection();
    }
  }

  @Override
  public void majComponent(final Object _o) {
  }

  @Override
  public void paste() {

    // Point locationSouris = this.getMousePosition();

    // --recuperation des noeuds copies --//
    if (projet_.nodesCopyied != null) {
      final ArrayList<EbliNode> listeNodeUndo = EbliWidgetActionDuplicate.duplicateInScene(projet_.nodesCopyied, getScene());
      if (listeNodeUndo.size() != 0) {
        getScene().getCmdMng().addCmd(new CommandUndoRedoPaste(listeNodeUndo));
      }

      // reinitialisation de la liste
      projet_.nodesCopyied = null;

      getScene().refresh();
    } else if (projet_.nodesCutted != null) {

      for (final Iterator<EbliNode> it = projet_.nodesCutted.iterator(); it.hasNext(); ) {
        final EbliNode node = it.next();
        // --ajout du node --//

        getScene().addNode(node);
      }
      // --creation de la commande undo/redo --//
      if (projet_.nodesCutted != null && projet_.nodesCutted.size() != 0) {
        getScene().getCmdMng().addCmd(new CommandUndoRedoPaste(projet_.nodesCutted));
      }

      // reinit de la liste
      projet_.nodesCutted = null;

      getScene().refresh();
    }
  }

  @Override
  public int print(final Graphics _g, final PageFormat _format, final int _page) {
    if (_page != 0) {
      return Printable.NO_SUCH_PAGE;
    }
    final EbliScene scene = getScene();
    // Sauvegarde de Clip pour restauration finale.
    final Shape s = _g.getClip();
    // Utilisation de methode de Graphics2d.
    final Graphics2D g2d = (Graphics2D) _g;
    AffineTransform transform = g2d.getTransform();

    // Initialisation du graphics et translation de l 'origine.
    final double[] coord = EbliPrinter.initGraphics(g2d, _format);
    final Dimension dimension = scene.getDefaultImageDimension();

    // Calcul du facteur d'echelle.
    final int wComp = dimension.width;
    final int hComp = dimension.height;
    final double facteur = EbliPrinter.echelle(_format, wComp, hComp);
    final double wCompDessine = wComp * facteur;
    final double hCompDessine = hComp * facteur;
    // definition de la zone d'impression
    double xCentre = (_format.getImageableWidth() - wCompDessine) / 2;
    double yCentre = (_format.getImageableHeight() - hCompDessine) / 2;
    g2d.translate(xCentre, yCentre);
    try {
      Runnable runnable = new Runnable() {
        @Override
        public void run() {
          scene.produceImage(g2d, (int) wCompDessine, (int) hCompDessine, null);
        }
      };
      if (!EventQueue.isDispatchThread()) {
        EventQueue.invokeAndWait(runnable);
      } else {
        runnable.run();
      }
    } catch (Exception e) {
      FuLog.error(e);
    }
    g2d.setTransform(transform);
    g2d.setClip(s);
    return Printable.PAGE_EXISTS;
  }

  @Override
  public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    return getScene().produceImage(_w, _h, _params);
  }

  @Override
  public BufferedImage produceImage(final Map _params) {
    return getScene().produceImage(_params);
  }

  @Override
  public void redo() {
    final CtuluCommandManager c = getCmdMng();
    if (c != null) {
      c.redo();
    }
  }

  @Override
  public void replace() {
    if (editorEnCours_ instanceof CtuluSelectionInterface) {
      ((CtuluSelectionInterface) editorEnCours_).replace();
    }
  }

  @Override
  public void select() {
    if (editorEnCours_ instanceof CtuluSelectionInterface) {
      ((CtuluSelectionInterface) editorEnCours_).select();
    }
  }

  @Override
  public void setActive(final boolean _b) {
  }

  @Override
  public void setTitle(final String _title) {
    super.setTitle(_title);
    if (ecrasePrevious_) {
      previousTitleFrame = title;
      if (menus_ != null) {
        menus_[0].setText(_title);
      }
    } else {
      ecrasePrevious_ = true;
    }
  }

  public void setTitle(final String _title, final boolean ecrasePrevious) {
    ecrasePrevious_ = ecrasePrevious;
    this.setTitle(_title);
  }

  /**
   * Met en place la menubar et la rend floatable false.
   *
   * @param _toolBar
   */
  public void setToolBar(final JToolBar _toolBar) {
    _toolBar.setFloatable(false);
    this.add(_toolBar, BorderLayout.NORTH);
  }

  @SuppressWarnings("serial")
  @Override
  public void startExport(final CtuluUI impl) {
    final Collection<EbliNode> nodes = controller_.getSceneCourante().getNodes();
    final Collection<CtuluExportDataInterface> exportables = new ArrayList<CtuluExportDataInterface>();
    for (final EbliNode node : nodes) {
      if (node.getController().isDataExportable()) {
        exportables.add((CtuluExportDataInterface) node.getController());
      }
    }
    if (exportables.isEmpty()) {
      impl.message(TrResource.getS("Export"), TrResource.getS("Aucune donn�e exportable"), true);
      return;
    }
    if (exportables.size() == 1) {
      exportables.iterator().next().startExport(impl);
      return;
    }
    final BuComboBox cb = new BuComboBox(exportables.toArray());
    cb.setRenderer(new CtuluCellTextRenderer() {
      @Override
      protected void setValue(final Object value) {
        setText(((CtuluExportDataInterface) value).getTitle());
      }
    });
    final CtuluDialogPanel pn = new CtuluDialogPanel(false);

    pn.setLayout(new BorderLayout());
    pn.addLabel(TrResource.getS("Choisir la source de donn�es"));
    pn.add(cb);
    final boolean afficheModaleOk = pn.afficheModaleOk(impl.getParentComponent(), getTitle());
    if (afficheModaleOk) {
      ((CtuluExportDataInterface) cb.getSelectedItem()).startExport(impl);
    }
  }

  @Override
  public void undo() {
    final CtuluCommandManager c = getCmdMng();
    if (c != null) {
      c.undo();
    }
  }
}
