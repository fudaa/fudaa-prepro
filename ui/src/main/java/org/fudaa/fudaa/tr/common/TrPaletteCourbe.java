/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.tr.common;

import org.fudaa.ebli.courbe.PaletteCourbes;


/**
 *
 * @author Adrien Hadoux
 */
public class TrPaletteCourbe  {
    public static final String FILENAME="prepro.xml";
    
      public synchronized static PaletteCourbes getInstance() {
          return PaletteCourbes.getInstance(FILENAME);
      }
    
}
