/**
 * @creation 1 sept. 2004
 * @modification $Date: 2007-04-30 14:22:40 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuInformationsDocument;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluHelpComponent;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.rubar.H2dRubarTarageListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarTarageMng;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryTarageGroupType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.telemac.io.ScopeStructure;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.courbe.*;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.*;

import javax.swing.*;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarTarageCourbesManager.java,v 1.25 2007-04-30 14:22:40 deniger Exp $
 */
public class TrRubarTarageCourbesManager extends TrEvolutionManager implements H2dRubarTarageListener {
  private final static class RubarTarageFille extends EGFilleSimple implements CtuluHelpComponent {
    FudaaCommonImplementation impl_;

    /**
     * @param _g
     * @param _titre
     * @param _appli
     * @param _id
     * @param _t
     */
    public RubarTarageFille(final EGFillePanel _g, final String _titre, final FudaaCommonImplementation _appli,
                            final BuInformationsDocument _id, final EGTableGraphePanel _t) {
      super(_g, _titre, _appli, _id, _t);
      impl_ = _appli;
    }

    @Override
    public String getShortHtmlHelp() {
      final CtuluHtmlWriter help = new CtuluHtmlWriter();
      final CtuluHtmlWriter.Tag para = help.para();
      help.append(TrResource.getS("Cette fen�tre permet de cr�er et d'�diter des lois de tarage."));
      help.nl();
      help.append(impl_.buildLink(CtuluLib.getS("Voir la documentation..."), "common-curves"));
      para.close();
      help.para();
      help.append(TrResource.getS("Les lois de tarage sont utilis�es par {0}", impl_.buildLink(TrResource
          .getS("les conditions limites"), "rubar-editor-bc")));
      return help.toString();
    }
  }

  private

  H2dRubarTarageMng tarageMng_;

  /**
   * @param h2dRubarParameters le manager de courbes
   */
  public TrRubarTarageCourbesManager(final H2dRubarParameters h2dRubarParameters) {

    tarageMng_ = h2dRubarParameters.getTarageMng();
    tarageMng_.addTarageListener(this);
    initWithCurves(getAllTarage(h2dRubarParameters));
  }

  public static final Set getAllTarage(H2dRubarParameters h2dRubarParameters) {
    TreeSet set = new TreeSet();
    set.addAll(h2dRubarParameters.getTarageMng().getEvolutions());
    set.addAll(h2dRubarParameters.getOuvrageMng().getAllTarageEvolutions());
    return set;
  }

  @Override
  protected EGAxeVertical buildAxeV() {
    final EGAxeVertical v = new EGAxeVertical();
    v.setTitre(H2dVariableType.DEBIT_NORMAL.getName());
    return v;
  }

  public EGFilleSimple createFille(final TrRubarProject _project) {
    final EGGraphe g = new EGGraphe(this);
    final EGAxeHorizontal h = new EGAxeHorizontal();
    h.setGraduations(true);
    h.setTitre(H2dResource.getS("Cote"));
    h.setUnite("m");
    g.setXAxe(h);
    final EGFillePanel fillePanel = new EGFillePanel(g);
    final List actions = new ArrayList();
    final FudaaCommonImplementation impl = _project.getImpl();
    actions.add(new TrCourbeUseAction(new TrRubarCourbeTarageUseFinder(_project.getH2dRubarParameters(), _project
        .getVisuPanel(), g.getModel())));
    actions.addAll(Arrays.asList(TrCourbeImporter.getImportExportAction(this, g, impl)));

    fillePanel.setPersonnalAction((EbliActionInterface[]) actions.toArray(new EbliActionInterface[actions.size()]));
    final EGFilleSimple fille = new RubarTarageFille(fillePanel, H2dResource.getS("Lois de tarage"), impl, _project
        .getInformationsDocument(), new EGTableGraphePanel());
    fille.setFrameIcon(TrResource.TR.getToolIcon("tarage"));
    fille.setName("ifCurveTarage");
    return fille;
  }

  @Override
  public void decoreAddButton(final EbliActionInterface _b) {
    super.decoreAddButton(_b);
    _b.putValue(Action.SHORT_DESCRIPTION, TrResource.getS("Ajouter une nouvelle loi de tarage"));
  }

  @Override
  protected String getDefaultNom() {
    return H2dRubarTarageMng.getDefaultNom();
  }

  public void importCourbes(final ScopeStructure _crb, final CtuluCommandManager _mng, final ProgressionInterface _prog) {
  }

  @Override
  public boolean isContentModifiable() {
    return true;
  }

  @Override
  public boolean isSpatial() {
    return false;
  }

  @Override
  public boolean isStructureModifiable() {
    return true;
  }

  @Override
  public void tarageCourbeChanged(final H2dRubarTarageMng _mng, final EvolutionReguliereInterface _e) {
    final int i = getCourbeWithEvol(_e);
    if (i >= 0) {
      super.fireCourbeContentChanged(getCourbe(i));
    }
  }

  @Override
  public void TarageGroupeUsedChanged(H2dRubarTarageMng _mng, H2dRubarBoundaryTarageGroupType tarageGroupeType) {
  }

  @Override
  public void tarageUsedChanged(final H2dRubarTarageMng _mng) {
    fireStructureChanged();
  }

  @Override
  protected void testEvolutionAdded(final EvolutionReguliereInterface _e) {
    tarageMng_.initEvolutionForThis((EvolutionReguliereAbstract) _e);
  }
}
