/**
 *  @creation     20 janv. 2005
 *  @modification $Date: 2007-05-04 14:01:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.model.MvInfoDelegate;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrPostRubarEnvModel.java,v 1.17 2007-05-04 14:01:51 deniger Exp $
 */
public class TrPostRubarEnvModel extends TrIsoModelAbstract {

  /**
   * @author fred deniger
   * @version $Id: TrPostRubarEnvModel.java,v 1.17 2007-05-04 14:01:51 deniger Exp $
   */
  protected static final class EnvHauteurFilter extends TrPostFilterLayer {
    protected EnvHauteurFilter(final TrPostSource _src) {
      super(_src);
    }

    @Override
    protected TrPostFilterHauteur createPostDataConditionHauteur() {
      return new TrPostFilterHauteurEnvRubar();
    }

    @Override
    protected TrPostFilterHauteur createPostDataConditionHauteur(final TrPostFilterHauteur _h) {
      return new TrPostFilterHauteurEnvRubar(_h);
    }
  }

  /**
   * @param _s la source de donnees
   * @param _d le delegue pour afficher les infos
   */
  public TrPostRubarEnvModel(final TrPostSourceRubar _s, final MvInfoDelegate _d) {
    super(_s, _s.getEnvelopModel(), _d);
  }

  @Override
  public TrPostFilterLayer getCond() {
    if (cond_ == null) {
      cond_ = new EnvHauteurFilter(super.s_);
      cond_.updateTimeStep(0, s_);
    }
    return cond_;
  }

  @Override
  public void fillInterpolateInfo(final InfoData _m, final int _element, final double _x, final double _y,
      final String _layerTitle) {
    ((TrPostSourceRubar) s_).fillMaximaInterpolateInfo(_m, _element, _x, _y, true);
  }

  @Override
  public void fillWithInfo(final InfoData _m, final ZCalqueAffichageDonneesInterface _layer) {
    final CtuluListSelectionInterface s = _layer.getLayerSelection();
    if (delegate_ != null) {
      delegate_.fillWithElementInfo(_m, s, _layer.getTitle());
    }
    if (s != null && s.isOnlyOnIndexSelected()) {
      ((TrPostSourceRubar) s_).fillMaximaInterpolateInfo(_m, s.getMaxIndex(), 0, 0, false);
      _m.setTitle(MvResource.getS("El�ment {0}", CtuluLibString.getString(s.getMaxIndex() + 1)));
    }

  }

  @Override
  public void setVar(final int _idx) {
    final H2dVariableType t = (H2dVariableType) getVariableList().getElementAt(_idx >= 0 ? _idx : 0);
    if (t != var_) {
      var_ = t;
      updateCurrentValueCache();
      if (oldData_ == null) {
        FuLog.warning(new Throwable());
      }
    }
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    final BuTable r = new CtuluTable();
    if (var_ != null) {
      final String s = TrResource.getS("<u>Variable</u>: {0}", var_.getName());
      EbliTableInfoPanel.setTitle(r, "<html>" + s + "</html>");
    }
    final TrPostSourceRubar rubar = (TrPostSourceRubar) s_;
    final H2dVariableType[] vars = rubar.getEnvVar();
    final TrPostValueTableModel model = new TrPostValueTableModel(s_.getGrid(), vars, true) {

      @Override
      public EfData getData(final int _idx) {
        return ((TrPostSourceRubar) s_).getEnveloppData((H2dVariableType) super.var_[_idx]);
      }

    };
    if (oldData_ != null && var_ != null) {
      model.setInCache(var_, oldData_);
    }
    r.setModel(model);
    if (vars != null) {
      final int nb = model.getColumnCount();
      final TableColumnModel colModel = r.getColumnModel();
      final int idx = CtuluLibArray.findObject(vars, var_) + 3;
      final List cols = new ArrayList();
      for (int i = nb - 1; i >= 3; i--) {
        if (i != idx) {
          cols.add(colModel.getColumn(i));
        }
      }
      for (int i = cols.size() - 1; i >= 0; i--) {
        colModel.removeColumn((TableColumn) cols.get(i));
      }

    }
    return r;
  }

  @Override
  public void updateCurrentValueCache() {
    oldData_ = ((TrPostSourceRubar) s_).getEnveloppData(var_);
  }

  @Override
  public double getValueFor(final H2dVariableType _var, final int _idx) {
    if (_var == var_) { return oldData_.getValue(_idx); }
    return ((TrPostSourceRubar) s_).getEnveloppData(_var).getValue(_idx);
  }
}
