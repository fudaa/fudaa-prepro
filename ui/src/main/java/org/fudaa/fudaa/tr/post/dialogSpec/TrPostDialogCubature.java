package org.fudaa.fudaa.tr.post.dialogSpec;

import com.memoire.bu.*;
import org.locationtech.jts.geom.LinearRing;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluComboBoxModelAdapter;
import org.fudaa.ctulu.gui.CtuluDecimalFormatEditPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.ef.EfFilter;
import org.fudaa.dodico.ef.EfFilterNone;
import org.fudaa.dodico.ef.EfFilterSelectedElement;
import org.fudaa.dodico.ef.cubature.EfComputeVolumeSeuil;
import org.fudaa.dodico.ef.cubature.EfDataIntegrale;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.find.CalqueFindCourbeTreeModel;
import org.fudaa.ebli.commun.EbliSelectionMode;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.post.persist.CommonProperties;
import org.fudaa.fudaa.tr.post.profile.MvFilterHelper;
import org.fudaa.fudaa.tr.post.profile.MvProfileTreeModel;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class TrPostDialogCubature implements AbstractTrPostDialogAddToSceneDialogContent {
  /**
   * booleen qui indique si le wizard a �t�t lanc� depuis un calque
   */
  TrPostSource source_;
  TrPostVisuPanel calque_;
  MvProfileTreeModel modelGraphe_;
  TrPostCommonImplementation impl_;
  // variables et pdt
  private JPanel panelVariables_;
  CtuluComboBoxModelAdapter modelVariables_;
  JComboBox listVar_;
  CtuluComboBoxModelAdapter modelPdt_;
  JComboBox listPdt_;
  // seuil
  private JPanel panelSeuil_;
  BuTextField seuil_ = new BuTextField(5);
  // isolignes
  private JPanel panelIsolignes_;
  JTree treeLignes_;
  CalqueFindCourbeTreeModel lineTreeModel_;
  // results
  JPanel panelResults_;
  JLabel results_;
  JButton ajouter_ = new JButton(TrLib.getString("Calculer"), EbliResource.EBLI.getIcon("crystal_valider"));
  JButton btChangeFmt_ = new JButton(TrLib.getString("Changer le formattage des nombres"));
  // DefaultTableModel modelResult_;
  JTextField tfRes_;
  JTextField tfResMin_;
  JTextField tfResMax_;
  CtuluNumberFormatI currentFmt_ = CtuluNumberFormatDefault.DEFAULT_FMT;
  BuCheckBox cbAll_;
  BuCheckBox cbSelectedObject_;
  BuCheckBox cbChooseLine_;
  EfFilter currentFilter_;

  /**
   * Constructeur reserv� au calques
   */
  public TrPostDialogCubature(final TrPostVisuPanel _calque, final TrPostCommonImplementation impl) {
    impl_ = impl;
    calque_ = _calque;
    modelGraphe_ = null;
    source_ = calque_.getSource();
    // on recupere la selection courant
    currentFilter_ = MvFilterHelper.getSelectedFilter(_calque.getSource(), _calque);
    modelVariables_ = new CtuluComboBoxModelAdapter(source_.getNewVarListModel());
    modelPdt_ = new CtuluComboBoxModelAdapter(source_.getNewTimeListModel());
    lineTreeModel_ = new CalqueFindCourbeTreeModel(null, _calque.getDonneesCalque());
    lineTreeModel_.setOnlyLigneFerme(true);
  }

  /**
   * Methode qui enregistre les param�tres saisies pour les recharger � la prochaine ouverture.
   *
   * @author Adrien Hadoux
   */
  private void saveContextParams() {
    if (impl_ == null) {
      return;
    }
    CommonProperties properties = impl_.getProject().getCommonProperties();
    if (properties == null) {
      properties = new CommonProperties();
      impl_.getProject().setCommonProperties(properties);
    }

    properties.cubatureProperties = new CommonProperties.CubaturePropertie();
    CommonProperties.CubaturePropertie p = properties.cubatureProperties;

    p.allDomain = this.cbAll_.isSelected();
    p.selectedObject = this.cbSelectedObject_.isSelected();
    p.lineObject = this.cbChooseLine_.isSelected();
    p.variable = this.listVar_.getSelectedIndex();
    p.pdt = this.listPdt_.getSelectedIndex();
    p.seuil = seuil_.getText();

    p.createCurve1 = createGraphe.getCbResult().isSelected();
    p.createCurve2 = createGraphe.getCbPositive().isSelected();
    p.createCurve3 = createGraphe.getCbNegative().isSelected();

    p.pdtDepart = createGraphe.getStartTime().getSelectedIndex();
    p.pdtFin = createGraphe.getEndTime().getSelectedIndex();
    p.grapheCible = createGraphe.getTargetGraph().getSelectedIndex();
  }

  /**
   * Methode qui enregistre les param�tres saisies pour les recharger � la prochaine ouverture.
   *
   * @author Adrien Hadoux
   */
  private void loadContextParams() {
    if (impl_ == null || impl_.getProject().getCommonProperties() == null || impl_.getProject().getCommonProperties().cubatureProperties == null) {
      return;
    }
    CommonProperties properties = impl_.getProject().getCommonProperties();
    CommonProperties.CubaturePropertie p = properties.cubatureProperties;

    this.cbAll_.setSelected(p.allDomain);
    this.cbSelectedObject_.setSelected(p.selectedObject);
    this.cbChooseLine_.setSelected(p.lineObject);

    if (p.variable < listVar_.getItemCount()) {
      listVar_.setSelectedIndex(p.variable);
    }

    if (p.pdt < listPdt_.getItemCount()) {
      listPdt_.setSelectedIndex(p.pdt);
    }

    this.seuil_.setText(p.seuil);

    createGraphe.getCbResult().setSelected(p.createCurve1);
    createGraphe.getCbPositive().setSelected(p.createCurve2);
    createGraphe.getCbNegative().setSelected(p.createCurve3);

    if (p.pdtDepart < createGraphe.getStartTime().getItemCount()) {
      createGraphe.getStartTime().setSelectedIndex(p.pdtDepart);
    }
    if (p.pdtFin < createGraphe.getEndTime().getItemCount()) {
      createGraphe.getEndTime().setSelectedIndex(p.pdtFin);
    }
    if (p.grapheCible < createGraphe.getTargetGraph().getItemCount()) {
      createGraphe.getTargetGraph().setSelectedIndex(p.grapheCible);
    }
  }

  protected JTree buildTree() {
    return CalqueFindCourbeTreeModel.createCalqueTree(lineTreeModel_, true);
  }

  /**
   * Construit le panel des variables
   */
  private JPanel buildIsoLignes() {
    JPanel content = new JPanel(new BuGridLayout(2, 5, 5));
    cbAll_ = new BuCheckBox();
    content.add(new BuLabel(TrLib.getString("Sur tout le domaine")));
    content.add(cbAll_);
    cbSelectedObject_ = new BuCheckBox();
    content.add(new BuLabel(TrLib.getString("Sur les objets s�lectionn�s")));
    content.add(cbSelectedObject_);
    cbChooseLine_ = new BuCheckBox();
    content.add(new BuLabel(TrLib.getString("Sur les objets appartenant � la ligne")));
    content.add(cbChooseLine_);
//    JPanel res = new JPanel(new BorderLayout());
//    res.add(content, BorderLayout.SOUTH);
    treeLignes_ = buildTree();
    treeLignes_.setPreferredSize(new Dimension(300, 200));
//    final JScrollPane jScrollPane = new JScrollPane(treeLignes_);
//    res.add(jScrollPane);
    treeLignes_.setEnabled(false);
    cbChooseLine_.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent _e) {
        treeLignes_.setEnabled(cbChooseLine_.isSelected());
      }
    });

    if (currentFilter_ == null) {
      cbSelectedObject_.setEnabled(false);
      cbAll_.setSelected(true);
    } else {
      cbSelectedObject_.setSelected(true);
    }

    if (treeLignes_.getRowCount() == 0) {
      cbChooseLine_.setEnabled(false);
      cbChooseLine_.setToolTipText(TrLib.getString("Pas de ligne s�lectionnable"));
    } else {
      JPanel newPn = new JPanel(new BuBorderLayout());
      newPn.add(content, BorderLayout.NORTH);
      final JScrollPane jScrollPane = new JScrollPane(treeLignes_);
      jScrollPane.setPreferredSize(new Dimension(200, 150));
      newPn.add(jScrollPane, BorderLayout.CENTER);
      content = newPn;
    }
    ButtonGroup gp = new ButtonGroup();
    gp.add(cbSelectedObject_);
    gp.add(cbAll_);
    gp.add(cbChooseLine_);
    if (currentFilter_ == null && treeLignes_.getRowCount() > 0) {
      final BCalque calqueActif = getVisuPanel().getScene().getCalqueActif();
      if (calqueActif instanceof ZCalqueLigneBrisee && ((ZCalqueLigneBrisee) calqueActif).isOnlyOneObjectSelected()) {
        int idx = ((ZCalqueLigneBrisee) calqueActif).getSelectedIndex()[0];
        if (((ZCalqueLigneBrisee) calqueActif).modeleDonnees().isGeometryFermee(idx)) {
          TreePath treePath = lineTreeModel_.selectLayerAndLigne(calqueActif, idx);
          if (treePath != null) {
            cbChooseLine_.setSelected(true);
            treeLignes_.setSelectionPath(treePath);
            treeLignes_.expandPath(treePath);
          }
        }
      }
    }
    content.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Calculer la cubature sur")));
    return content;
  }

  private JPanel buildSeuil() {
    seuil_.setCharValidator(BuCharValidator.FLOAT);
    seuil_.setStringValidator(BuStringValidator.FLOAT);
    seuil_.setValueValidator(BuValueValidator.FLOAT);
    seuil_.setText(CtuluLibString.ZERO);

    final JPanel conteneur = new JPanel(new FlowLayout(FlowLayout.CENTER));
    conteneur.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Choix du seuil")));
    conteneur.add(seuil_);

    return conteneur;
  }

  private JPanel buildresults() {
    final JPanel conteneur = new JPanel(new BorderLayout());

    final JPanel content = new JPanel(new FlowLayout(FlowLayout.CENTER));

    results_ = new JLabel("");
    content.add(ajouter_);
    content.add(results_);

    conteneur.setBorder(BorderFactory.createTitledBorder(TrResource.getS("R�sultats")));

    // -- action relatives aux resultats --//
    ajouter_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        if (controleDataOk()) {
          computeResults();
        }
      }
    });
    conteneur.add(content, BorderLayout.NORTH);
    JPanel res = new BuPanel(new BuGridLayout(2, 5, 5));

    tfRes_ = new BuTextField();
    tfRes_.setEditable(false);
    tfRes_.setHorizontalAlignment(JTextField.RIGHT);
    tfResMax_ = new BuTextField();
    tfResMax_.setEditable(false);
    tfResMax_.setHorizontalAlignment(JTextField.RIGHT);
    tfResMin_ = new BuTextField();
    tfResMin_.setEditable(false);
    tfResMin_.setHorizontalAlignment(JTextField.RIGHT);
    res.add(new BuLabel(TrLib.getString("R�sultat")));
    res.add(tfRes_);
    res.add(new BuLabel(TrLib.getString("Positif")));
    res.add(tfResMax_);
    res.add(new BuLabel(TrLib.getString("N�gatif")));
    res.add(tfResMin_);
    res.add(new BuLabel(""));
    res.add(btChangeFmt_);
    btChangeFmt_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent _e) {
        CtuluNumberFormatI newFmt = CtuluDecimalFormatEditPanel.chooseNumberFormat(currentFmt_);
        if (newFmt != currentFmt_) {
          currentFmt_ = newFmt;
          if (!CtuluLibString.isEmpty(tfRes_.getText())) {
            updateResValues();
          }
        }
      }
    });
    conteneur.add(res, BorderLayout.CENTER);
    return conteneur;
  }

  public boolean controleDataOk() {

    if (cbChooseLine_.isSelected() && treeLignes_.getSelectionCount() == 0) {
      impl_.error(TrResource.getS("Il faut choisir une ligne bris�e"));
      return false;
    }
    if (seuil_.getText().equals("")) {
      impl_.error(TrResource.getS("Le seuil doit �tre d�fini par un r�el"));
      return false;
    }
    return true;
  }

  /**
   * retourne la variable selectionnee
   */
  @Override
  public H2dVariableType getSelectedVariable() {
    return (H2dVariableType) listVar_.getSelectedItem();
  }

  @Override
  public EbliScene getScene() {
    return getVisuPanel().getPostImpl().getCurrentLayoutFille().getScene();
  }

  @Override
  public JTextField getTxtSeuil() {
    return seuil_;
  }

  @Override
  public AbstractListModel getModelVariables() {
    return modelVariables_;
  }

  @Override
  public TrPostSource getSource() {
    return source_;
  }

  @Override
  public TrPostProjet getProject() {
    return impl_.getCurrentProject();
  }

  @Override
  public TrPostVisuPanel getVisuPanel() {
    return calque_;
  }

  /**
   * retourne l indice du pas de temps.
   */
  protected int getSelectedTimeStep() {
    return listPdt_.getSelectedIndex();
  }

  private JPanel buildVariables() {
    final JPanel content = new JPanel(new GridLayout(2, 2));
    listVar_ = new BuComboBox(modelVariables_);
    listVar_.setSelectedIndex(0);
    listPdt_ = new BuComboBox(modelPdt_);
    listPdt_.setSelectedIndex(0);
    content.add(new JLabel(TrLib.getString("Variable")));
    content.add(listVar_);
    content.add(new JLabel(TrLib.getString("Pas de temps")));
    content.add(listPdt_);
    content.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Choix de la variable et du pas de temps")));
    return content;
  }

  /**
   * Creation du panel graphique.
   */
  public JPanel buildContent() {

    JPanel content = new JPanel(new BuVerticalLayout(10));
    // -- panel des isolignes --//
    if (panelIsolignes_ == null) {
      panelIsolignes_ = buildIsoLignes();
    }
    content.add(panelIsolignes_);

    // -- panel des variables --//
    if (panelVariables_ == null) {
      panelVariables_ = buildVariables();
    }
    content.add(panelVariables_);

    // -- panelSeuil
    if (panelSeuil_ == null) {
      panelSeuil_ = buildSeuil();
    }
    content.add(panelSeuil_);

    // -- panel des resultats --//
    if (panelResults_ == null) {
      panelResults_ = buildresults();
    }
    content.add(panelResults_);
    if (createGraphe == null) {
      createGraphe = new TrPostDialogCubatureAddToScene(this);
    } else {
      createGraphe.updateTarget();
    }
    JPanel createPanel = createGraphe.createPanel();
    createPanel.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Construire courbes")));
    content.add(createPanel);

    loadContextParams();

    return content;
  }

  TrPostDialogCubatureAddToScene createGraphe = null;

  public void buildDialog() {

    // CtuluUIDialog ui_ = new
    // CtuluUIDialog(listeWidgetCalque_.get(0).getCalqueController
    // ().getVisuPanel());

    final Frame f = CtuluLibSwing.getFrameAncestorHelper(impl_.getCurrentInternalFrame());
    JDialog dialog_ = new JDialog(f);

    dialog_.setModal(true);
    dialog_.pack();
    dialog_.setTitle(getTitle());
    dialog_.setContentPane(buildContent());
    dialog_.pack();
    dialog_.setLocationRelativeTo(CtuluLibSwing.getFrameAncestor(impl_.getCurrentInternalFrame()));
    dialog_.setVisible(true);
  }

  public String getTitle() {
    return TrResource.getS("Cubatures");
  }

  /**
   * Recupere la ligne brisee du tree
   */
  public LinearRing getSelectedLineInTree() {
    if (treeLignes_.isSelectionEmpty()) {
      return null;
    }
    final CalqueFindCourbeTreeModel.LayerNode node = (CalqueFindCourbeTreeModel.LayerNode) treeLignes_
        .getSelectionPath().getLastPathComponent();
    final ZCalqueLigneBrisee cq = (ZCalqueLigneBrisee) node.getUserObject();
    return (LinearRing) cq.modeleDonnees().getGeomData().getGeometry(node.getIdxPoly());
  }

  @Override
  public double getSeuil() {
    try {
      return Double.parseDouble(seuil_.getText());
    } catch (NumberFormatException e) {
      System.err.print(e);
      return 0;
    }
  }

  public FastBitSet getSelectedMeshes() {
    if (cbAll_.isSelected()) {
      return null;
    }
    FastBitSet set = new FastBitSet();
    EfFilter filter = getFilter();
    int nb = source_.getGrid().getEltNb();
    for (int i = 0; i < nb; i++) {
      if (filter.isActivatedElt(i, EfComputeVolumeSeuil.USE_STRICT_FILTER)) {
        set.set(i);
      }
    }
    return set;
  }

  /**
   * Methode qui realise les calculs d'int�grale par rapport aux donn�es
   */
  private void computeResults() {

    new CtuluTaskOperationGUI(impl_, FudaaLib.getS("Calcul Bilan")) {
      @Override
      public void act() {
        final ProgressionInterface prog = getMainStateReceiver();

        saveContextParams();

        // -- calcul du ef filter --//
        impl_.setMainProgression(10);
        EfFilter selectedElt = getFilter();

        // -- calcul des efdata --//
        impl_.setMainProgression(30);
        impl_.setMainMessage(CtuluLib.getS("Calcul des donn�es pour les pas de temps et variables r�cup�r�s"));
        // final EfData data = source_.getData(getSelectedVariable(), getSelectedTimeStep());

        // -- calcul de l'int�grale --//
        impl_.setMainProgression(70);
        impl_.setMainMessage(CtuluLib.getS("Calcul de l'int�grale"));

        final EfComputeVolumeSeuil compute = EfComputeVolumeSeuil.getVolumeComputer(selectedElt, prog, source_,
            getSelectedVariable(), getSeuil(), new CtuluAnalyze());

        EfDataIntegrale resCalcul = compute.getVolume(getSelectedTimeStep());
        res_ = resCalcul.getResultat();
        resMax_ = resCalcul.getZonePlus();
        resMin_ = resCalcul.getZoneMoins();
        updateResValues();
        impl_.unsetMainMessage();
        impl_.unsetMainProgression();
      }
    }.start();
  }

  double res_;
  double resMax_;
  double resMin_;

  private void updateResValues() {
    tfRes_.setText(currentFmt_.format(res_));
    tfResMax_.setText(currentFmt_.format(resMax_));
    tfResMin_.setText("-" + currentFmt_.format(resMin_));
  }

  private EfFilter getFilter() {
    // impl_.setMainMessage(CtuluLib.getS("R�cup�ration des lignes bris�es"));
    EfFilter selectedElt = null;
    if (cbSelectedObject_.isSelected()) {
      selectedElt = currentFilter_;
    } else if (cbChooseLine_.isSelected()) {
      final MvElementLayer layer = calque_.getGridGroup().getPolygonLayer();
      final CtuluListSelection listSelect = layer.selection(getSelectedLineInTree(), EbliSelectionMode.MODE_ONE);
      selectedElt = new EfFilterSelectedElement(listSelect, source_.getGrid());
    } else {
      selectedElt = new EfFilterNone();
    }
    return selectedElt;
  }

  public TrPostVisuPanel getCalque_() {
    return calque_;
  }

  public void setCalque_(TrPostVisuPanel calque_) {
    this.calque_ = calque_;
  }

  public TrPostCommonImplementation getImpl_() {
    return impl_;
  }

  public void setImpl_(TrPostCommonImplementation impl_) {
    this.impl_ = impl_;
  }
}
