/*
 *  @creation     16 mars 2005
 *  @modification $Date: 2007-04-16 16:35:32 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrPostMinPaletteTableModel.java,v 1.9 2007-04-16 16:35:32 deniger Exp $
 */
public class TrPostMinPaletteTableModel extends AbstractTableModel {

  H2dVariableType[] vs_;
  boolean[] activated_;
  Double[] value_;
  TrPostSource s_;

  TrPostMinPaletteTableModel(final TrPostSource _s) {
    s_ = _s;
    vs_ = _s.getVarToDefinedMinPalette();
    activated_ = _s.getMinPaletteActived(vs_);
    final double[] tempV = _s.getMinPaletteValues(vs_);
    value_ = new Double[tempV.length];
    for (int i = value_.length - 1; i >= 0; i--) {
      value_[i] = CtuluLib.getDouble(tempV[i]);
    }
  }

  public void apply() {
    final double[] temp = new double[value_.length];
    for (int i = temp.length - 1; i >= 0; i--) {
      temp[i] = value_[i].doubleValue();
    }
    s_.setDefaultPaletteMinPalette(vs_, activated_, temp);
  }

  @Override
  public int getColumnCount() {
    return 3;
  }

  @Override
  public int getRowCount() {
    return vs_ == null ? 0 : vs_.length;
  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    return _columnIndex != 1;
  }

  @Override
  public Class getColumnClass(final int _columnIndex) {
    if (_columnIndex == 0) {
      return Boolean.class;
    } else if (_columnIndex == 2) { return Double.class; }
    return String.class;
  }

  @Override
  public String getColumnName(final int _columnIndex) {
    if (_columnIndex == 0) {
      return CtuluLibString.ESPACE;
    } else if (_columnIndex == 2) { return EbliLib.getS("Valeur minimale"); }
    return TrResource.getS("Variable");
  }

  @Override
  public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
    if (_value == null) { return; }
    if (_columnIndex == 0) {
      if (_value instanceof Boolean) {
        activated_[_rowIndex] = ((Boolean) _value).booleanValue();
      } else {
        activated_[_rowIndex] = Boolean.getBoolean(_value.toString());
      }
    } else if (_columnIndex == 2) {
      if (_value instanceof Double) {
        value_[_rowIndex] = (Double) _value;
      } else {
        try {
          value_[_rowIndex] = Double.valueOf(_value.toString());
        } catch (final NumberFormatException e) {
          // et bien vogue la galere

        }
      }
    }
  }

  @Override
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {
    if (_columnIndex == 0) { return Boolean.valueOf(activated_[_rowIndex]); }
    if (_columnIndex == 1) { return vs_[_rowIndex].getName(); }
    return value_[_rowIndex];
  }

}