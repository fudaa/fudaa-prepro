/*
 * @creation 9 ao�t 2005
 * 
 * @modification $Date: 2007-04-30 14:22:38 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.fu.FuLog;
import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JDialog;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluProgressionBarAdapter;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluComboBoxModelAdapter;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.ef.impl.EfDataStatisticEvaluator;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostSource;

/**
 * Permet l'edition des variables creees par l'utilisateur.
 * 
 * @author Fred Deniger
 * @version $Id: TrPostDataEditStatisticPanel.java,v 1.2 2007-04-30 14:22:38 deniger Exp $
 */
public class TrPostDataEditStatisticPanel extends TrPostDataEditCommonPanel {

  final BuComboBox cbTimeStart_;
  final BuComboBox cbTimeEnd_;
  final BuComboBox cbStat_;
  final BuComboBox cbVar_;
  final BuCheckBox cbTime_;
  final TrPostDataCreatedStatistic initStat_;

  final CtuluUI ui_;

  /**
   * @param _src la source
   * @param _var la variable a modifier: si null, phase de creation.
   */
  public TrPostDataEditStatisticPanel(final TrPostSource _src, final CtuluUI _ui, final H2dVariableTypeCreated _var) {
    super(_src, _var);
    ui_ = _ui;
    cbTimeStart_ = new BuComboBox();
    cbTimeStart_.setModel(new CtuluComboBoxModelAdapter(_src.getNewTimeListModel()));
    cbTimeEnd_ = new BuComboBox();
    cbTimeEnd_.setModel(new CtuluComboBoxModelAdapter(_src.getNewTimeListModel()));

    cbVar_ = new BuComboBox(super.varToAdd_.toArray());
    initStat_ = _var == null ? null : (TrPostDataCreatedStatistic) _src.getUserCreatedVar(_var);
    cbTime_ = new BuCheckBox();

    if (cbTimeStart_.getModel().getSize() > 0) {
      cbTimeStart_.setSelectedIndex(0);
      cbTimeEnd_.setSelectedIndex(cbTimeStart_.getModel().getSize() - 1);
    } else {
      cbTime_.setEnabled(false);
    }

    final Object[] toArray = TrPostDataCreatedStatistic.STAT_OPERATOR.toArray();
    cbStat_ = new BuComboBox(toArray);

    addLabel(TrResource.getS("Variable"));
    add(cbVar_);
    addLabel(TrResource.getS("Op�rateur"));
    add(cbStat_);
    addLabel(TrResource.getS("Temps"));
    cbTime_.setText(TrResource.getS("Calculer sur une partie des pas de temps"));
    add(cbTime_);
    final BuPanel pn = new BuPanel();
    pn.setLayout(new BuGridLayout(2, 2, 2, true, false));
    addLabel(pn, TrResource.getS("Temps de d�but")).setEnabled(false);
    pn.add(cbTimeStart_);
    addLabel(pn, TrResource.getS("Temps de fin")).setEnabled(false);
    pn.add(cbTimeEnd_);
    CtuluLibSwing.addActionListenerForCheckBoxTitle(pn, cbTime_);
    addLabel(CtuluLibString.EMPTY_STRING);
    add(pn);
    add(new BuLabel());
    // initialisation
    init(toArray);

  }

  private void init(final Object[] _toArray) {
    cbStat_.setSelectedIndex(0);
    cbTimeStart_.setEnabled(false);
    cbTimeEnd_.setEnabled(false);
    if (initStat_ != null) {
      cbVar_.setSelectedItem(initStat_.varToStat_);
      final int idx = CtuluLibArray.findObjectEgalEgal(_toArray, initStat_.evaluator_);
      if (idx >= 0) {
        cbStat_.setSelectedIndex(idx);
      }
      if (!initStat_.isForAll()) {
        cbTime_.setSelected(true);
        cbTimeStart_.setEnabled(true);
        cbTimeEnd_.setEnabled(true);
        cbTimeStart_.setSelectedIndex(initStat_.getTsBegin());
        cbTimeEnd_.setSelectedIndex(initStat_.getTsEnd());
      }

    }
  }

  H2dVariableTypeCreated newVar_;
  TrPostDataCreatedStatistic res_;

  @Override
  H2dVariableTypeCreated getNewVar() {
    newVar_ = super.getNewVar();
    if (res_ != null) {
      res_.setVarCreated(newVar_);
    }
    return newVar_;
  }

  @Override
  public TrPostDataCreated createData() {
    FuLog.debug("FTR: on recr�e une statistique");
    if (res_ == null) {
      res_ = new TrPostDataCreatedStatistic(src_, ui_, getSelectedVar(), (EfDataStatisticEvaluator) cbStat_.getSelectedItem());
    } else {
      res_.varToStat_ = getSelectedVar();
      res_.evaluator_ = (EfDataStatisticEvaluator) cbStat_.getSelectedItem();
    }
    if (cbTime_.isSelected()) {
      res_.setTsBegin(cbTimeStart_.getSelectedIndex());
      res_.setTsEnd(cbTimeEnd_.getSelectedIndex());
    }
    res_.setVarCreated(getNewVar());
    final JProgressBar progressBar = new JProgressBar();
    final ProgressionInterface prog = new CtuluProgressionBarAdapter(progressBar);
    prog.setDesc(res_.getVarCreated().getName());
    SwingWorker<Boolean, Void> worker = new SwingWorker<Boolean, Void>() {
      @Override
      protected Boolean doInBackground() throws Exception {
        res_.updateValues(prog);
        return Boolean.TRUE;
      }
    };
    class SwingWorkerCompletionWaiter implements PropertyChangeListener {
      private JDialog dialog;

      public SwingWorkerCompletionWaiter(JDialog dialog) {
        this.dialog = dialog;
      }

      @Override
      public void propertyChange(PropertyChangeEvent event) {
        if ("state".equals(event.getPropertyName()) && SwingWorker.StateValue.DONE == event.getNewValue()) {
          dialog.setVisible(false);
          dialog.dispose();
        }
      }
    };
    Frame frame = CtuluLibSwing.getFrameAncestor(this);
    JDialog dialog = new JDialog(frame, true);
    dialog.setContentPane(progressBar);

    worker.addPropertyChangeListener(new SwingWorkerCompletionWaiter(dialog));
    worker.execute();
    dialog.pack();
    dialog.setLocationRelativeTo(frame);
    dialog.setVisible(true);
    return res_;
  }

  public boolean isForAll() {
    return !cbTime_.isSelected();
  }

  public H2dVariableType getSelectedVar() {
    return (H2dVariableType) cbVar_.getSelectedItem();
  }

  /**
   * @return true si des modifications ont ete faites.
   */
  @Override
  public boolean isModified() {
    if (var_ == null || initStat_ == null || super.isVarModified()) {
      return true;
    }
    // test de l'operateur de stat et si tous les pas de temps sont s�lectionn�s pour les deux
    // pas le meme operateur: modifie
    // pas la meme emprise sur les pas de temps : modifie
    if (getSelectedVar() != initStat_.varToStat_ || cbStat_.getSelectedItem() != initStat_.evaluator_ || initStat_.isForAll() != isForAll()) {
      return true;
    }
    // sinon modifie si les pas de temps de deb ou de fin sont diff�rents.
    return !initStat_.isForAll()
        && (cbTimeEnd_.getSelectedIndex() != initStat_.getTsEnd() || cbTimeStart_.getSelectedIndex() != initStat_.getTsBegin());

  }
}
