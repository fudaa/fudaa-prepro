/**
 * @creation 24 mai 2004
 * @modification $Date: 2008-01-15 11:38:49 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr;

import com.memoire.bu.*;
import com.memoire.vfs.VfsFile;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import org.fudaa.ctulu.BuNetworkPreferencesPanel;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluFavoriteFiles;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.exec.FudaaExec;
import org.fudaa.fudaa.commun.impl.FudaaLookPreferencesPanel;
import org.fudaa.fudaa.commun.impl.FudaaStartupExitPreferencesPanel;
import org.fudaa.fudaa.commun.trace2d.FudaaPerformancePreferencesPanel;
import org.fudaa.fudaa.tr.common.TrApplicationManager;
import org.fudaa.fudaa.tr.common.TrCommonImplementation;
import org.fudaa.fudaa.tr.common.TrExplorer;
import org.fudaa.fudaa.tr.common.TrLauncher;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.reflux.TrRefluxExecPreferencesPanel;
import org.fudaa.fudaa.tr.reflux.TrRefluxImplHelper;
import org.fudaa.fudaa.tr.rubar.TrRubarExecPreferencesPanel;
import org.fudaa.fudaa.tr.rubar.TrRubarImplHelper;
import org.fudaa.fudaa.tr.telemac.TrTelemacExecPreferencesPanel;
import org.fudaa.fudaa.tr.telemac.TrTelemacImplHelper;

/**
 * @author Fred Deniger
 * @version $Id: TrSupervisorImplementation.java,v 1.38 2008-01-15 11:38:49 bmarchan Exp $
 */
public class TrSupervisorImplementation extends TrCommonImplementation implements Observer {

  @Override
  protected void ouvrir() {}

  private class TrAppliMainMenu extends BuDynamicMenu {

    /**
     * Appelle le constructeur avec "applications","APPLICATIONS".
     */
    public TrAppliMainMenu() {
      super(TrResource.getS("Applications"), "APPLICATIONS", BuResource.BU.getIcon("aucun"));
      setIcon(null);
    }

    @Override
    protected void build() {
      removeAll();
      final TrApplicationManager m = explorer_.getAppliManager();
      m.buildTrItem(this);
      add(new BuSeparator());
      final VfsFile selectedDirectory = TrSupervisorImplementation.this.explorer_.getCurrentDirectory();
      m.createMenuItems(this, TrSupervisorImplementation.this, selectedDirectory == null ? null : selectedDirectory
          .getAbsoluteFile());
    }

    @Override
    protected boolean isActive() {
      return true;
    }
  }

  @Override
  public void doImportProject() {}

  public static BuMenu buildFileMenu(final ActionListener _l) {
    final BuMenu r = new BuMenu(BuResource.BU.getString("Fichier"), "MENU_FICHIER");
    r.addMenuItem(BuResource.BU.getString("Quitter..."), "QUITTER", true, KeyEvent.VK_Q);
    r.addMenuItem(TrResource.getS("Fermer toutes les applications"), "CLOSE_ALL", BuResource.BU.getIcon("fermer"),
        true, 0);
    r.setIcon(null);
    r.addActionListener(_l);
    return r;
  }

  public static BuMenu buildHelpMenu(final ActionListener _l) {
    final BuMenu r = new BuMenu(BuResource.BU.getString("Aide"), "MENU_AIDE");
    r.setIcon(null);
    r.add(TrLib.buildAideContextItem(_l));
    r.addMenuItem(BuResource.BU.getString("Page principale"), "AIDE_INDEX", true, KeyEvent.VK_F1);
    r.addMenuItem(BuResource.BU.getString("A propos de..."), "APROPOSDE", true);
    r.addMenuItem(BuResource.BU.getString("Licence..."), "TEXTE_LICENCE", true);
    r.addSeparator();
    r.addMenuItem(BuResource.BU.getString("Site WWW"), "WWW_ACCUEIL", true);
    TrLib.addJavawsForJnlp(r);
    r.addMenuItem(TrResource.getS("Soumettre une demande d'am�lioration"), "SEND_COMMENT", BuResource.BU.getIcon("envoyer"), true);
    TrLib.addJava3DJMFTest(r);
    r.addActionListener(_l);
    return r;
  }

  TrAppliMainMenu appliMenu_;

  TrExplorer explorer_;

  public TrSupervisorImplementation() {
    super();
  }

  @Override
  public void actionPerformed(final ActionEvent event) {
    final String com = event.getActionCommand();
    if ("MANAGER".equals(com)) {
      this.changeAppli();
    } else if ("REFRESH".equals(com)) {
      explorer_.refresh();
    } else if ("ADD_FAVORITE".equals(com)) {
      CtuluFavoriteFiles.INSTANCE.addFile(explorer_.getCurrentDirectory().getAbsoluteFile());
    } else if (com.startsWith("SPEC_EXEC_")) {
      final int i = Integer.parseInt(com.substring("SPEC_EXEC_".length()));
      toolBarExecs_[i].execInDir(explorer_.getCurrentDirectory(), this, explorer_.getRefreshRunnable());
    } else {
      super.actionPerformed(event);
    }
  }

  public BuMenu buildEdition(final ActionListener _l) {
    final BuMenu r = new BuMenu(BuResource.BU.getString("Edition"), "MENU_EDITION");
    r.setIcon(null);
    r.addMenuItem(BuResource.BU.getString("Pr�f�rences"), "PREFERENCE", true, KeyEvent.VK_F2).addActionListener(_l);
    r.addMenuItem(FudaaLib.getS("Modifier les applications externes"), "MANAGER", BuResource.BU.getIcon("aucun"), true)
        .addActionListener(_l);
    addConsoleMenu(r).addActionListener(_l);
    return r;
  }

  @Override
  protected void buildPreferences(final List _prefs) {
    _prefs.add(new FudaaPerformancePreferencesPanel(this));
    _prefs.add(new BuUserPreferencesPanel(this));
    _prefs.add(new BuBrowserPreferencesPanel(this));
    _prefs.add(new FudaaStartupExitPreferencesPanel(true));
    _prefs.add(new BuLanguagePreferencesPanel(this));
    _prefs.add(new FudaaLookPreferencesPanel(this));
    _prefs.add(new TrChainePreferencePanel());
    final String soft = launcher_.getCurrentImplHelper().getSoftwareID();
    if (soft.equals(TrTelemacImplHelper.getID())) {
      _prefs.add(new TrTelemacExecPreferencesPanel());
    } else if (soft.equals(TrRefluxImplHelper.getID())) {
      _prefs.add(new TrRefluxExecPreferencesPanel());
    } else if (soft.equals(TrRubarImplHelper.getID())) {
      _prefs.add(new TrRubarExecPreferencesPanel());
    }
    _prefs.add(new BuNetworkPreferencesPanel());
  }

  protected void changeAppli() {
    explorer_.getAppliManager().createPanel().afficheModale(getFrame());
  }

  @Override
  protected JComponent createMainComponent() {
    return null;
  }

  @Override
  public void exit() {
    explorer_.saveLastDir();
    super.exit();
    if (CtuluFavoriteFiles.INSTANCE.getObserver() == explorer_) {
      CtuluFavoriteFiles.INSTANCE.setObserver(null);
    }
  }

  @Override
  public BuInformationsSoftware getInformationsSoftware() {
    return TrLauncherDefault.getInfosSoftware();
  }

  FudaaExec[] toolBarExecs_;


  @Override
  public void init() {
    buildMainPanel_ = false;
    super.init();
    explorer_ = new TrExplorer(this, launcher_.getCurrentImplHelper().getAppliMng());
    explorer_.setBorder(BorderFactory.createEmptyBorder(10, 5, 5, 5));
    setMainPanel(new TrSingleMainPanel(explorer_));

    content_.revalidate();
    main_toolbar_.removeAll();
    BuToolButton it = new BuToolButton(TrResource.TR.getToolIcon("bookmark-add"));
    it.setToolTipText(FudaaLib.getS("Ajouter le r�pertoire courant aux favoris"));
    it.setActionCommand("ADD_FAVORITE");
    it.addActionListener(this);
    main_toolbar_.add(it);
    it = new BuToolButton(BuResource.BU.getToolIcon("rafraichir"));
    it.setToolTipText(BuResource.BU.getString("Rafra�chir"));
    it.setActionCommand("RAFRAICHIR");
    it.addActionListener(explorer_);
    main_toolbar_.add(it);

    toolBarExecs_ = explorer_.getAppliManager().getToolBarExecs();
    if (toolBarExecs_ != null) {
      main_toolbar_.addSeparator();
      for (int i = 0; i < toolBarExecs_.length; i++) {
        final FudaaExec e = toolBarExecs_[i];
        it = new BuToolButton();
        it.setIcon(e.getIcon());
        it.setToolTipText(e.getViewedName());
        it.addActionListener(this);
        it.setActionCommand("SPEC_EXEC_" + i);
        main_toolbar_.add(it);
      }
    }
    main_toolbar_.addSeparator();
    it = new BuToolButton();
    it.setAction(explorer_.getAppliManager().getMvExec().getAction());
    it.setToolTipText(TrResource.getS("Lancer la visionneuse de maillage"));
    it.setActionCommand("PREPRO");
    it.setText(CtuluLibString.EMPTY_STRING);
    main_toolbar_.add(it);
    it = new BuToolButton();
    it.setAction(explorer_.getAppliManager().getEditorExec().getAction());
    it.setActionCommand("PREPRO");
    it.setToolTipText(TrResource.getS("Lancer l'�diteur de projet hydraulique"));
    it.setText(CtuluLibString.EMPTY_STRING);
    main_toolbar_.add(it);
    it = new BuToolButton();
    it.setAction(explorer_.getAppliManager().getPostExec().getAction());
    it.setToolTipText(TrResource.getS("Lancer le post-processeur"));
    it.setActionCommand("PREPRO");
    it.setText(CtuluLibString.EMPTY_STRING);
    main_toolbar_.add(it);
    final BuMenuBar mb = getMainMenuBar();

    mb.removeAll();
    mb.add(buildFileMenu(this));
    mb.add(buildEdition(this));
    explorer_.getAppliManager().addObserver(this);
    appliMenu_ = new TrAppliMainMenu();
    mb.add(appliMenu_);
    mb.add(explorer_.getFavoriteMenu());
    mb.add(buildHelpMenu(this));
    mb.computeMnemonics();
  }

  @Override
  public final boolean isSupervisor() {
    return true;
  }

  @Override
  public void setLauncher(final TrLauncher _l) {
    super.setLauncher(_l);
    if (_l instanceof TrLauncherDefault) {
      ((TrLauncherDefault) _l).supervisor_ = this;
    }
  }

  @Override
  public void update(final Observable _o, final Object _arg) {
    appliMenu_.removeAll();
    explorer_.applicationPreferencesChanged();
  }

  @Override
  public BuIcon getSpecificIcon() {
    return TrResource.getSupervisorIcon();
  }

  @Override
  public String getSpecificName() {
    return TrResource.getSupervisorName();
  }
}
