/**
 * @creation 13 oct. 2004
 * @modification $Date: 2007-05-04 14:01:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.H2dSIListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.rubar.io.RubarCINFileFormat;

import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFileStateCIN.java,v 1.10 2007-05-04 14:01:53 deniger Exp $
 */
public class TrRubarFileStateCIN extends TrRubarFileState implements H2dSIListener {
  /**
   * @param _l le listener
   */
  public TrRubarFileStateCIN(final TrRubarFileStateListener _l, final H2dRubarParameters _p) {
    super(RubarCINFileFormat.getInstance(), _l);
    _p.getSi().addListener(this);
  }

  @Override
  public void siAdded(final H2dVariableType _var) {
    setModified(true);
  }

  @Override
  public void siChanged(final H2dVariableType _var) {

    setModified(true);
  }

  @Override
  public void siRemoved(final H2dVariableType _var) {
    setModified(true);
  }

  @Override
  protected CtuluIOOperationSynthese loadIfNeeded(final File _initFile, final ProgressionInterface _prog) {
    FuLog.warning(new Throwable());
    return null;
  }

  protected CtuluIOOperationSynthese loadIfNeeded(final File _initFile, final ProgressionInterface _prog,
                                                  final int _nbElt, final int nbBlock) {
    // le fichier est a jour : inutile
    if ((initFile_ != null) && (!initFile_.equals(_initFile))) {
      FuLog.error(
      new Throwable(initFile_.getName() + " loaded " + _initFile.getName()));
      return null;
    }
    if (initFile_ == null) {
      initFile_ = _initFile;
    }
    if (isLoadedFileUpToDate()) {
      CtuluLibMessage.info("RELOAD VF2M: " + _initFile.getName() + " uptodate");

      return null;
    }
    CtuluLibMessage.info("RELOAD VF2M: " + _initFile.getName() + " will be reloaded");
    lastModifiedTime_ = _initFile.lastModified();
    return ((RubarCINFileFormat) fmt_).read(_initFile, _prog, _nbElt, nbBlock);
  }

  @Override
  public CtuluIOOperationSynthese save(final File _dir, final String _projectName,
                                       final ProgressionInterface _progression, final TrRubarProject _projet) {
    return RubarCINFileFormat.getInstance().write(RubarCINFileFormat.getInstance().getFileFor(_dir, _projectName),
        _projet.getH2dRubarParameters().getSi(), _progression);
  }
}
