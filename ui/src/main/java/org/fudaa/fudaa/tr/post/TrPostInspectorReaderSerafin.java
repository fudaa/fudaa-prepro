/*
 * @creation 18 avr. 2006
 * @modification $Date: 2007-05-04 14:01:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuLib;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import gnu.trove.TDoubleArrayList;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.io.serafin.SerafinInterface;
import org.fudaa.dodico.ef.io.serafin.SerafinNewReader;
import org.fudaa.dodico.ef.io.serafin.SerafinNewReaderInfo;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.telemac.TrTelemacAppliManager;

/**
 * @author fred deniger
 * @version $Id: TrPostInspectorReaderSerafin.java,v 1.7 2007-05-04 14:01:52 deniger Exp $
 */
public class TrPostInspectorReaderSerafin implements TrPostInspectorReader {

  /**
   * @author fred deniger
   * @version $Id: TrPostInspectorReaderSerafin.java,v 1.7 2007-05-04 14:01:52 deniger Exp $
   */
  static final class ResFileFilter implements FilenameFilter {
    @Override
    public boolean accept(final File _dir, final String _name) {
      return _name.endsWith("RES");
    }
  }

  final File casFile_;
  TrPostCommonImplementation impl_;
  SerafinNewReaderInfo info_;
  boolean isActivating_;
  long lastModified_;
  TrPostProjet prj_;
  TrPostSource src_;
  ProgressionInterface prog_;

  File serafin_;

  final File[] tmpDir_;

  public TrPostInspectorReaderSerafin(final File _casFile, final File[] _before) {
    super();
    casFile_ = _casFile;
    tmpDir_ = _before;
  }

  public TrPostInspectorReaderSerafin(final TrPostProjet _prj, final TrPostSource _src) {
    super();
    prj_ = _prj;
    src_ = _src;
    serafin_ = _src.getMainFile();
    tmpDir_ = null;
    casFile_ = null;
  }

  private void createPost() {
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FTR: create post");
    }
    isActivating_ = true;
    TrPostMultiSourceActivator.activeFile(serafin_, impl_);
    prj_ = impl_.getCurrentProject();
    if (prj_ != null) {
      impl_.setProjet(prj_);
    }
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FTR: create post FIN");
    }
  }

  private int createReader() {
    final SerafinNewReader reader = new SerafinNewReader();
    if (prj_ != null) {
      reader.setReadTimeStepFrom(src_.getNbTimeStep());
    }
    final CtuluIOOperationSynthese op = reader.read(serafin_, prog_);
    if (!op.containsSevereError()) {
      final SerafinInterface ser = (SerafinInterface) op.getSource();
      if (prj_ == null && ser.getGrid() != null && ser.getTimeStepNb() > 0) {
        createPost();
        return prj_ == null ? 0 : ser.getTimeStepNb();
      }
      info_ = ser.getReadingInfo();

      return majTimeStepInProj(ser);

    }
    // il y a eu une erreur de lecture
    FuLog.warning("FTR: " + op.getAnalyze().getFatalError().toString());
    return 0;
  }

  private int majTimeStepInProj(final SerafinInterface _ser) {
    int res = 0;
    try {
      final int timeStepAvailable = info_.getTimeStepAvailable();
      final int nb = timeStepAvailable - src_.getNbTimeStep();
      final TDoubleArrayList ts = new TDoubleArrayList(timeStepAvailable + src_.getNbTimeStep());
      ts.add(src_.getTime().getInitTimeSteps());

      for (int i = 0; i < nb; i++) {
        ts.add(_ser.getTimeStep(i));
        res++;
      }
      BuLib.invokeNowOrLater(new Runnable() {
        @Override
        public void run() {
          src_.getTime().setTimeSteps(ts.toNativeArray());
        }
      });
    } catch (final IOException _evt) {
      FuLog.error(_evt);

    }
    return res;
  }

  protected void searchRes() {
    final File[] newTmpDir = TrTelemacAppliManager.getTmpDirForSteeringFile(casFile_);
    if (CtuluLibArray.isEmpty(newTmpDir)) { return; }
    final int nbOld = tmpDir_ == null ? 0 : tmpDir_.length;
    if (nbOld < newTmpDir.length) {
      final Set newOne = new HashSet(Arrays.asList(newTmpDir));
      if (tmpDir_ != null) {
        newOne.removeAll(Arrays.asList(tmpDir_));
      }
      File dir = null;
      for (final Iterator it = newOne.iterator(); it.hasNext();) {
        final File f = (File) it.next();
        // premiere iteration: on affecte dir
        if (dir == null) {
          dir = f;
        } else if (f.lastModified() > dir.lastModified()) {
          dir = f;
        }
      }
      if (dir == null) { return; }
      if (new File(dir, "T3DRBI").exists()) {
        serafin_ = new File(dir, "T3DRBI");
      } else {
        final File[] res = dir.listFiles(new ResFileFilter());
        if (!CtuluLibArray.isEmpty(res)) {
          serafin_ = res[res.length - 1];
        }
      }

    }

  }

  @Override
  public void close() {
    if (info_ != null) {
      info_.close();
    }
  }

  public TrPostCommonImplementation getImpl() {
    return impl_;
  }

  @Override
  public TrPostProjet getPostActivatedProject() {
    return prj_;
  }

  public ProgressionInterface getProg() {
    return prog_;
  }

  @Override
  public boolean isPostActivated() {
    return prj_ != null;
  }

  public static boolean isVisuDisplayed(final TrPostProjet _prj) {
    return false;
    // return _prj != null && _prj.fille_ != null && _prj.getImpl() != null
    // && CtuluLibArray.containsObject(_prj.getImpl().getMainPanel().getDesktop().getAllFrames(), _prj.fille_);
  }

  @Override
  public boolean isPostActivating() {
    if (isActivating_ && isVisuDisplayed(prj_)) {
      isActivating_ = false;
    }
    return isActivating_;
  }

  @Override
  public int read() {
    if (serafin_ == null) {
      searchRes();
      return 0;
    }
    if (!serafin_.exists()) { return 0; }
    int res = 0;
    if (prog_ != null) {
      prog_.setDesc(TrResource.getS("Mise � jour"));
      prog_.setProgression(30);
    }
    final long las = serafin_.lastModified();
    // le fichier a ete modifie
    if (las > lastModified_) {
      try {
        if (info_ == null) {
          res = createReader();
        } else if (prj_ != null && info_.getTimeStepAvailable() > src_.getNbTimeStep()) {
          if (Fu.DEBUG && FuLog.isDebug()) {
            FuLog.debug("FTR: new time steps to read");
          }

          if (prog_ != null) {
            prog_.setDesc(TrResource.getS("Fichier modifi� ..."));
            prog_.setProgression(50);
          }
          final SerafinNewReader reader = new SerafinNewReader();
          reader.setReadTimeStepFrom(src_.getNbTimeStep());
          final CtuluIOOperationSynthese op = reader.read(serafin_, prog_);
          if (op.containsSevereError()) {
            FuLog.warning(op.getAnalyze().getFatalError().toString());
          } else {
            final SerafinInterface ser = (SerafinInterface) op.getSource();
            res = majTimeStepInProj(ser);

            if (prog_ != null) {
              prog_.setDesc(TrResource.getS("Modification envoy�e"));
              prog_.setProgression(70);
            }
          }

        }
      } catch (final IOException _evt) {
        FuLog.error(_evt);

      }

      lastModified_ = las;
      if (prog_ != null) {
        prog_.setDesc(TrResource.getS("Maj auto"));
        prog_.setProgression(0);
      }
    }
    return res;
  }

  @Override
  public void setImpl(final TrPostCommonImplementation _impl) {
    impl_ = _impl;

  }

  @Override
  public void setProgression(final ProgressionInterface _prog) {
    prog_ = _prog;
  }
}
