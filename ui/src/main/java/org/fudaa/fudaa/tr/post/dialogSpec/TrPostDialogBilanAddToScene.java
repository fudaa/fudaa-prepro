/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.dialogSpec;

import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.post.TrPostCourbe;
import org.fudaa.fudaa.tr.post.TrPostCourbeBilanModel;
import org.fudaa.fudaa.tr.post.TrPostCourbeTreeModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class TrPostDialogBilanAddToScene extends AbstractTrPostDialogAddToScene<TrPostDialogBilan> {
  public TrPostDialogBilanAddToScene(TrPostDialogBilan from) {
    super(from);
  }

  @Override
  protected void updateNames() {
    H2dVariableType selectedVariable = from.getSelectedVariable();
    boolean vectoriel = from.cbVectoriel.isEnabled() && from.cbVectoriel.isSelected();
    final String suffixe = vectoriel ? "/" + TrLib.getString("Calcul vectoriel") : "";
    final String prefix = TrLib.getString("Bilan") + " " + selectedVariable.getName();
    tfResult.setText(prefix + " " + suffixe);
    tfPositive.setText(prefix + " >= " + from.getSeuil() + suffixe);
    tfNegative.setText(prefix + " <= " + from.getSeuil() + suffixe);
  }

  @Override
  public JPanel createPanel() {
    if (panel == null) {
      super.createPanel();
      from.cbScalar.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          updateNames();
        }
      });
      from.cbVectoriel.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          updateNames();
        }
      });
    }
    return panel;
  }

  @Override
  protected void addCurves(ProgressionInterface prog) {
    final List<TrPostCourbeBilanModel> models = new ArrayList<TrPostCourbeBilanModel>();
    if (cbResult.isSelected()) {
      models.add(createModel(EnumThresholdCalculation.ALL, tfResult.getText(), prog));
    }
    if (cbNegative.isSelected()) {
      models.add(createModel(EnumThresholdCalculation.NEGATIVE, tfNegative.getText(), prog));
    }
    if (cbPositive.isSelected()) {
      models.add(createModel(EnumThresholdCalculation.POSITIVE, tfPositive.getText(), prog));
    }
    JLabel lb = (JLabel) targetGraph.getSelectedItem();
    final EGGraphe graphe = (EGGraphe) lb.getClientProperty("GRAPH");
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        TrPostDialogBilanAddToScene.this.buttonAddCurves.setEnabled(true);
        TrPostDialogBilanAddToScene.this.buttonAddCurves.setText(TrLib.getString("Ajouter les courbes"));
        TrPostCourbeTreeModel model = null;
        if (graphe == null) {
          model = new TrPostCourbeTreeModel(from.impl_.getProject());
          from.impl_.getProject().addFille(model, TrPostCourbeTreeModel.getNewName(), null, from.visuPanel);
        } else {
          model = (TrPostCourbeTreeModel) graphe.getModel();
        }
        EGGroup groupFor = model.getGroupFor(computeBilanVariableType());
        for (TrPostCourbeBilanModel courbeModel : models) {
          groupFor.addEGComponent(new TrPostCourbe(groupFor, courbeModel, from.source_.getNewTimeListModel()));
        }
        model.fireStructureChanged();
        getScene().stopEditing();
        getScene().refresh();
      }
    });
  }

  private H2dVariableType computeBilanVariableType() {

    final H2dVariableType selectedVariable = from.getSelectedVariable();

    if (selectedVariable == H2dVariableType.DEBIT || "m2/s".equals(selectedVariable.getCommonUnitString())) {
      return H2dVariableType.DEBIT_M3;
    }
    String title = TrLib.getString("Bilan") + "(" + selectedVariable.getName() + ")";
    String shortName = "intg(" + selectedVariable.getShortName() + ")";
    String unit = "m*" + selectedVariable.getCommonUnitString();
    if ("m".equals(selectedVariable.getCommonUnitString())) {
      unit = "m2";
    }
    return new H2dVariableTypeCreated(title, shortName, unit, null);
  }

  private TrPostCourbeBilanModel createModel(final EnumThresholdCalculation type, String nom, ProgressionInterface prog) {
    TrPostCourbeBilanModel model = new TrPostCourbeBilanModel();
    model.setLine(from.getSelectedLine());
    model.setMinTimeIdx(startTime.getSelectedIndex());
    model.setMaxTimeIdx(endTime.getSelectedIndex());
    model.setSeuil(from.getSeuil());
    model.setSource(from.source_);
    model.setVar(from.getSelectedVariable());
    model.setVectoriel(from.isVectorCalculation());
    model.setComputation(type);
    model.setNom(nom);
    model.updateData(prog);
    return model;
  }
};
