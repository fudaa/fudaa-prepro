/*
 * @creation 9 d�c. 2005
 * 
 * @modification $Date: 2007-04-30 14:22:38 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.fu.FuLog;
import java.io.IOException;
import java.util.Set;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.tr.post.TrPostFlecheContent;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id: TrPostDataCreatedImport.java,v 1.7 2007-04-30 14:22:38 deniger Exp $
 */
public class TrPostDataCreatedImport implements TrPostDataCreated {

  final TrPostSource imported_;
  H2dVariableType var_;
  String varId_;

  /**
   * @param _imported
   * @param _var
   */
  public TrPostDataCreatedImport(final TrPostSource _imported, final H2dVariableType _var) {
    super();
    imported_ = _imported;
    var_ = _var;
  }


  public TrPostDataCreatedImport(final TrPostSource _imported, final String _var) {
    super();
    imported_ = _imported;
    varId_ = _var;
  }

  @Override
  public void restore() {
  }

  protected void initVar() {
    if (var_ == null) {
      var_ = TrPostDataHelper.getShortNameVariable(imported_).get(varId_);
    }
  }

  @Override
  public TrPostFlecheContent isFleche() {
    initVar();
    return imported_.getFlecheContent(var_);
  }

  @Override
  public TrPostDataCreatedSaver createSaver() {
    initVar();
    return new TrPostDataCreatedImportSaver(imported_.getId(), var_.getID());
  }

  @Override
  public boolean updateConstantVar(final Variable _var) {
    return false;
  }

  @Override
  public boolean isExpr() {
    return false;
  }

  @Override
  public void clearCache() {
  }

  @Override
  public boolean isEditable() {
    return false;
  }

  @Override
  public void fillWhithAllUsedVar(final Set _res) {

  }

  @Override
  public EfData getDataFor(final int _idxTime) {
    initVar();
    return imported_.getData(var_, _idxTime);
  }

  public int compareTo(final Object _o) {
    if (_o == this) {
      return 0;
    }
    if (_o == null) {
      return 1;
    }
    final int i = getDescription().compareTo(((TrPostDataCreated) _o).getDescription());
    // normalement il sont egaux
    if (i == 0) {
      return hashCode() - _o.hashCode();
    }
    return i;
  }

  @Override
  public String getDescription() {
    initVar();
    return var_.toString() + CtuluLibString.ESPACE + FSigLib.getS("import�");
  }

  @Override
  public double getValue(final int _idxTime, final int _idxObject) {
    initVar();
    try {
      return imported_.getData(var_, _idxTime, _idxObject);
    } catch (final IOException _e) {
      FuLog.warning(_e);
    }
    return 0;
  }

}
