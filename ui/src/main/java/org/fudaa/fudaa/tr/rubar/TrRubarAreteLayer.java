/**
 * @creation 10 juin 2004
 * @modification $Date: 2007-05-04 14:01:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.fu.FuLog;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;
import org.fudaa.dodico.ef.EfSegment;
import org.fudaa.dodico.h2d.rubar.H2dRubarArete;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryType;
import org.fudaa.ebli.calque.BCalqueCacheManager;
import org.fudaa.ebli.calque.ZCalqueSegment;
import org.fudaa.ebli.calque.ZModeleSegment;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;
import org.fudaa.fudaa.tr.data.TrAreteExprSupplier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarAreteLayer.java,v 1.22 2007-05-04 14:01:53 deniger Exp $
 */
public class TrRubarAreteLayer extends ZCalqueSegment implements MvLayerGrid {
  /**
   * @param _m le modele du calque
   */
  public TrRubarAreteLayer(final TrRubarAreteModel _m) {
    super(_m);
    BCalqueCacheManager.installDefaultCacheManager(this);
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return new TrAreteExprSupplier(super.modele_);
  }

  @Override
  public int[] getSelectedEdgeIdx() {
    return getRubarModel().getGlobalIdx(getSelectedIndex());
  }

  @Override
  public boolean isSelectionEdgeEmpty() {
    return isSelectionEmpty();
  }

  public void selectEdgesFromSameType() {
    this.selectEdgesFromSameType(getRubarModel());
  }

  protected void selectEdgesFromSameType(TrRubarAreteModel model) {
    final H2dRubarBoundaryTypeSelector selector = new H2dRubarBoundaryTypeSelector(model.getUsedBoundaryType());

    if (selector.afficheModaleOk(this.getParent(), "")) {
      final H2dRubarBoundaryType type = selector.getSelectedType();
      final TIntArrayList aretes = new TIntArrayList();
      final int nombre = model.getNombre();

      for (int i = 0; i < nombre; i++) {
        final H2dRubarBoundaryType areteType = model.getRubarArete(i).getType();
        if ((areteType != null) && (areteType.equals(type))) {
          aretes.add(i);
        }
      }

      setSelection(aretes.toNativeArray());
    }
  }

  /**
   * @return le modele rubar
   */
  public final TrRubarAreteModel getRubarModel() {
    return (TrRubarAreteModel) modeleDonnees();
  }

  /**
   * @return les aretes selectionnees
   */
  public H2dRubarArete[] getSelectedArete() {
    if (isSelectionEmpty()) {
      return null;
    }
    final int max = selection_.getMaxIndex();
    final List l = new ArrayList(max);
    final TrRubarAreteModel m = getRubarModel();
    for (int i = selection_.getMinIndex(); i <= max; i++) {
      if (selection_.isSelected(i)) {
        l.add(m.getRubarArete(i));
      }
    }
    final H2dRubarArete[] r = new H2dRubarArete[l.size()];
    l.toArray(r);
    return r;
  }

  @Override
  public int[] getSelectedElementIdx() {
    return null;
  }

  @Override
  public int[] getSelectedObjectInTable() {
    return getSelectedIndex();
  }

  @Override
  public int[] getSelectedPtIdx() {
    if (!isSelectionEmpty()) {
      final int nb = selection_.getNbSelectedIndex();
      EfSegment a;
      final TrRubarAreteModel m = getRubarModel();
      final TIntHashSet set = new TIntHashSet((int) (nb * 1.8));
      final int max = selection_.getMaxIndex();
      for (int i = selection_.getMinIndex(); i <= max; i++) {
        if (selection_.isSelected(i)) {
          a = m.getRubarArete(i);
          if (a.getPt1Idx() >= 0) {
            set.add(a.getPt1Idx());
          }
          if (a.getPt2Idx() >= 0) {
            set.add(a.getPt2Idx());
          }
        }
      }
      final int[] res = set.toArray();
      Arrays.sort(res);
      return res;
    }
    return null;
  }

  public boolean isFontModifiable() {
    return false;
  }

  @Override
  public boolean isPaletteModifiable() {
    return false;
  }

  /**
   * @return true
   */
  @Override
  public boolean isSelectionElementEmpty() {
    return true;
  }

  @Override
  public boolean isSelectionPointEmpty() {
    return isSelectionEmpty();
  }

  /**
   * Affectation que si le modele est du type TrRubarAreteModel.
   */
  @Override
  public void setModele(final ZModeleSegment _s) {
    if (_s instanceof TrRubarAreteModel) {
      setRubarModele((TrRubarAreteModel) _s);
    } else {
      FuLog.warning(new Throwable());
    }
  }

  /**
   * @param _s le nouveau modele a considerer.
   */
  public final void setRubarModele(final TrRubarAreteModel _s) {
    super.setModele(_s);
  }
}
