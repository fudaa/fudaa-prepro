/**
 * @creation 13 oct. 2004
 * @modification $Date: 2006-09-08 16:53:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import gnu.trove.TIntObjectHashMap;
import java.io.File;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.rubar.H2dRubarTarageListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarTarageMng;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryTarageGroupType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.rubar.io.RubarTARFileFormat;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFileStateTAR.java,v 1.9 2006-09-08 16:53:09 deniger Exp $
 */
public class TrRubarFileStateTAR extends TrRubarFileState implements H2dRubarTarageListener {

  @Override
  public void tarageCourbeChanged(final H2dRubarTarageMng _mng, final EvolutionReguliereInterface _e) {
    if (_e.isUsed()) {
      setModified(true);
      tarageUsedChanged(_mng);
    }
  }

  @Override
  public void TarageGroupeUsedChanged(H2dRubarTarageMng _mng, H2dRubarBoundaryTarageGroupType tarageGroupeType) {
    tarageUsedChanged(_mng);

  }

  @Override
  public void tarageUsedChanged(final H2dRubarTarageMng _mng) {
    setModified(true);
    setMarkRemoved(_mng.getNbEvolution() == 0);
    if ((((TrRubarFileStateMng) super.l_).contains(RubarTARFileFormat.getInstance())) == null) {
      ((TrRubarFileStateMng) super.l_).add(this);
      setMarkRemoved(false);
    }
  }

  /**
   * @param _l le listener
   */
  public TrRubarFileStateTAR(final TrRubarFileStateMng _l, final H2dRubarParameters _p) {
    super(RubarTARFileFormat.getInstance(), _l);
    _p.getBcMng().getTarageMng().addTarageListener(this);
    if (_p.getBcMng().getTarageMng().getNbEvolution() > 0) {
      _l.add(this);
    }
  }

  @Override
  public CtuluIOOperationSynthese save(final File _dir, final String _projectName, final ProgressionInterface _progression,
      final TrRubarProject _projet) {
    final File f = RubarTARFileFormat.getInstance().getFileFor(_dir, _projectName);
    final TIntObjectHashMap m = _projet.getH2dRubarParameters().getTarageMng().getMapForWrite();
    if ((m == null) || (m.size() == 0)) {
      CtuluLibMessage.info("le fichier tar est conserv�");
      return null;
    }
    return RubarTARFileFormat.getInstance().write(f, m, _progression);
  }

}
