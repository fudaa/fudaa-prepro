/**
 * @creation 1 oct. 2004
 * @modification $Date: 2007-04-30 14:22:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.fdico.FDicoLib;
import org.fudaa.fudaa.fdico.FDicoProjectState;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFileStateMng.java,v 1.24 2007-04-30 14:22:39 deniger Exp $
 */
public class TrRubarFileStateMng extends AbstractTableModel implements TrRubarFileStateListener {
  class SecondColumnCellRenderer extends CtuluCellTextRenderer {
    @Override
    public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected,
                                                   final boolean _hasFocus, final int _row, final int _column) {
      final Component r = super.getTableCellRendererComponent(_table, _value, _isSelected, _hasFocus, _row, _column);
      final TrRubarFileState s = getFileState(_row);
      if (s != null && s.getInitFile() != null) {
        setToolTipText(s.getInitFile().getAbsolutePath());
      }
      return r;
    }
  }

  List<TrRubarFileState> fileState_;
  String projectName_;
  FDicoProjectState state_;

  /**
   * Initialisation de la liste des etats fichiers.
   */
  public TrRubarFileStateMng() {
    fileState_ = new ArrayList(20);
  }

  protected void add(final TrRubarFileState _l) {
    _l.l_ = this;
    int i = Collections.binarySearch(fileState_, _l);
    if (i < 0) {
      fileState_.add(_l);
      Collections.sort(fileState_);
      i = Collections.binarySearch(fileState_, _l);
      fireTableRowsInserted(i, i);
      if (getFileState(i).isModified() && state_ != null) {
        state_.setParamsModified(true);
      }
    }
  }

  public void fileFormatDigitsChanged() {
    for (TrRubarFileState fileState : fileState_) {
      fileState.fileFormatDigitsChanged();
    }
  }

  protected void addAll(final List _l) {
    fileState_.addAll(_l);
    Collections.sort(fileState_);
    if (isModified() && state_ != null) {
      state_.setParamsModified(true);
    }
  }

  protected boolean contains(final TrRubarFileState _l) {
    return Collections.binarySearch(fileState_, _l) >= 0;
  }

  /*
   * public TrRubarFileState create(FileFormat _f){ return new TrRubarFileState(_f,this); }
   */

  protected File getFileFor(final File _dirBase, final String _projet, final TrRubarFileState _state) {
    return new File(_dirBase, CtuluLibFile.getFileName(_projet, _state.getExtension()));
  }

  /**
   * @param _i l'indice demande
   * @return description du fichier d'indice i.
   */
  protected TrRubarFileState getFileState(final int _i) {
    if (_i >= 0 && _i <= fileState_.size()) {
      return (TrRubarFileState) fileState_.get(_i);
    }
    return null;
  }

  protected List getFormatToDelete() {
    final List r = new ArrayList(fileState_.size());
    TrRubarFileState fs;
    for (int i = fileState_.size() - 1; i >= 0; i--) {
      fs = getFileState(i);
      if (fs.isMarkRemoved()) {
        r.add(fs);
      }
    }
    return r;
  }

  boolean isModified() {
    for (int i = fileState_.size() - 1; i >= 0; i--) {
      if (getFileState(i).isModified()) {
        return true;
      }
    }
    return false;
  }

  protected void remove(final TrRubarFileState _l) {
    final int i = Collections.binarySearch(fileState_, _l);
    if (i >= 0) {
      fileState_.remove(i);
      fireTableRowsDeleted(i, i);
    }
  }

  protected boolean save(final TrRubarProject _impl, final File _dirBase, final String _nameFileSansExt,
                         final ProgressionInterface _progress, final boolean _updateFileState) {
    if (!_dirBase.exists()) {
      _impl.getImpl().error(
          _dirBase.getAbsolutePath() + CtuluLibString.LINE_SEP + FDicoLib.getS("n'est pas accessible en �criture"));
      return false;
    }
    // les format a enlever
    final List formatToRemove = getFormatToDelete();
    // les formats a garder
    final List formatToKeep = new ArrayList(fileState_);
    formatToKeep.removeAll(formatToRemove);
    final List fileToRemove = new ArrayList(formatToRemove.size());
    int n = formatToRemove.size();
    // on recupere et teste les fichiers a enlever
    for (int i = 0; i < n; i++) {
      final TrRubarFileState fmtToRemove = (TrRubarFileState) formatToRemove.get(i);
      fmtToRemove.setInitFile(null);
      final File f = getFileFor(_dirBase, _nameFileSansExt, fmtToRemove);
      if (f.exists()) {
        if (!f.canWrite()) {
          _impl.getImpl().error(
              FudaaLib.getS("Le fichier {0} ne peut pas �tre effac�. V�rifier les droits utilisateur de votre syst�me",
                  f.getName()));
          return false;
        }
        fileToRemove.add(f);
      }
    }

    // liste des fichiers qui devront etre ecrases (modifie)
    final Map<File, TrRubarFileState> fileToOverride = new HashMap(n);
    // liste des fichiers qui seront copi�s (non modifie)
    final Map<File, TrRubarFileState> fileToCopy = new HashMap(n);
    List<File> fileModified = new ArrayList(n);
    n = formatToKeep.size();
    // les fichiers conserves
    for (int i = 0; i < n; i++) {
      final TrRubarFileState fmt = (TrRubarFileState) formatToKeep.get(i);
      final File f = getFileFor(_dirBase, _nameFileSansExt, fmt);
      // si le fichier f existe, doit etre modifie mais ne veut pas->erreur
      if ((f.exists()) && (!f.canWrite())
          && (fmt.isModified() || (fmt.getInitFile() == null) || (!fmt.getInitFile().equals(f)))) {
        _impl.getImpl()
            .error(
                CtuluResource.CTULU.getString(
                    "Le fichier {0} ne peut pas �tre �crit. V�rifier les droits utilisateur de votre syst�me", f
                        .getName()));
        return false;
      }

      // si ce n'est pas le fichier d'origine
      final boolean isInitFile = f.equals(fmt.getInitFile());
      // le fichier existe et n'est pas le fichier initial -> avertissement
      if (f.exists() && !isInitFile) {
        fileModified.add(f);
      }
      // le format est modifie: il faudra ecrire
      if (fmt.isModified()) {
        fileToOverride.put(f, fmt);
      }
      // pas de modif mais le fichier n'est le fichier charg�
      else if (!isInitFile) {
        fileToCopy.put(f, fmt);
      }
    }
    // si des fichiers non projet sont modifies -> message de confirmation
    n = fileModified.size();
    if (n > 0) {
      final StringBuffer message = new StringBuffer(FudaaLib.getS("Voulez-vous remplacer les fichiers suivants"));
      message.append(CtuluLibString.LINE_SEP).append(FudaaLib.getS("R�pertoire"));
      message.append(": ").append(_dirBase.getAbsolutePath());
      for (int i = 0; i < n; i++) {
        message.append(CtuluLibString.LINE_SEP).append(fileModified.get(i).getName());
      }
      if (!_impl.getImpl().question(FudaaLib.getS("Fichiers"), message.toString())) {
        return false;
      }
    }
    // pour s'assurer que cette liste tempo ne sera plus utilisee....
    fileModified = null;
    _impl.getImpl().setMainMessage(TrResource.getS("Suppression fichiers"));
    // suppression des fichiers inutiles
    for (int i = fileToRemove.size() - 1; i >= 0; i--) {
      final File f = (File) fileToRemove.get(i);
      CtuluLibMessage.info("SAVE: file removed " + f.getAbsolutePath());
      try {
        Files.delete(f.toPath());
      } catch (IOException e) {
        FuLog.error(e);
      }
    }
    // si mise a jour, on enleve les formats effaces
    if (_updateFileState) {
      fileState_.removeAll(formatToRemove);
    }
    // Copie des fichiers
    _impl.getImpl().setMainMessage(TrResource.getS("Copie fichiers"));
    for (Map.Entry<File, TrRubarFileState> o : fileToCopy.entrySet()) {
      final File source = o.getValue().getInitFile();
      if (source == null) {
        CtuluLibMessage.info(o.getValue().getClass().getName() + " file does not exist / not modified");
      } else if (source.exists()) {
        final File dest = o.getKey();
        // message d'info
        CtuluLibMessage.info("SAVE: file copied from " + source.getAbsolutePath() + " to " + dest.getAbsolutePath());
        CtuluLibFile.copyFile(source, dest);
        if (_updateFileState) {
          o.getValue().setInitFile(dest);
        }
      } else {
        CtuluLibMessage.info(source.getName() + " file does not exist / not modified");
      }
    }
    final String s = TrResource.getS("Ecriture fichiers") + CtuluLibString.ESPACE;
    _impl.getImpl().setMainMessage(s);
    int progress = 10;
    _impl.getImpl().setMainProgression(progress);
    final int stepValue = 90 + fileToOverride.size();
    final CtuluAnalyze analyse = new CtuluAnalyze();
    for (final Map.Entry<File, TrRubarFileState> e : fileToOverride.entrySet()) {
      final File dest = e.getKey();
      final TrRubarFileState stat = e.getValue();
      _impl.getImpl().setMainMessage(s + stat.getExtension());
      final CtuluIOOperationSynthese save = stat.save(_dirBase, _nameFileSansExt, _progress, _impl);
      if (save != null) {
        analyse.merge(save.getAnalyze());
      }
      progress += stepValue;
      // on met a jour le format si necessaire
      if (_updateFileState) {
        stat.setInitFile(dest);
        stat.setModified(false);
      }
      CtuluLibMessage.info("SAVE: file overwritten " + dest.getAbsolutePath());
      _impl.getImpl().setMainProgression(progress);
    }
    if (_updateFileState) {
      projectName_ = _nameFileSansExt;
      // envoie evt pour mettre a jour la table
      fireTableDataChanged();
    }
    _impl.getImpl().unsetMainMessage();
    _impl.getImpl().unsetMainProgression();
    return !_impl.getImpl().manageAnalyzeAndIsFatal(analyse);
  }

  /**
   * @param _ft le format a rechercher
   * @return l'etat du format trouve ou null si aucun
   */
  public TrRubarFileState contains(final FileFormat _ft) {
    final int i = indexOf(_ft);
    if (i >= 0) {
      return getFileState(i);
    }
    return null;
  }

  @Override
  public void fileStateChanged(final TrRubarFileState _f) {
    final int i = Collections.binarySearch(fileState_, _f);
    if (i >= 0) {
      fireTableCellUpdated(i, 0);
      if (getFileState(i).isModified() && state_ != null) {
        state_.setParamsModified(true);
      }
    }
  }

  @Override
  public int getColumnCount() {
    return 2;
  }

  @Override
  public String getColumnName(final int _column) {
    return _column == 0 ? TrResource.getS("Etat") : TrResource.getS("Fichier");
  }

  /**
   * @return le temps du dernier fichier enregistre
   */
  public long getLastFileSave() {
    long r = 0;
    for (int i = fileState_.size() - 1; i >= 0; i--) {
      final TrRubarFileState state = getFileState(i);
      if (state.getInitFile() != null) {
        final long l = state.getInitFile().lastModified();
        if (l > r) {
          r = l;
        }
      }
    }
    return r;
  }

  /**
   * @return le nom du projet ( doublons avec TrRubarParameter)
   */
  public final String getProjectName() {
    return projectName_;
  }

  @Override
  public int getRowCount() {
    return fileState_.size();
  }

  /**
   * @return le cell renderer pour la deuxieme colonne
   */
  public TableCellRenderer getSecondCellRenderer() {
    return new SecondColumnCellRenderer();
  }

  @Override
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {
    if (_rowIndex >= fileState_.size()) {
      return CtuluLibString.ESPACE;
    }
    return _columnIndex == 0 ? ((TrRubarFileState) fileState_.get(_rowIndex)).getDescription() : CtuluLibFile
        .getFileName(projectName_, ((TrRubarFileState) fileState_.get(_rowIndex)).getFmt().getExtensions()[0]);
  }

  public int indexOf(final FileFormat _ft) {
    int low = 0;
    int high = fileState_.size() - 1;
    while (low <= high) {
      final int mid = (low + high) >> 1;
      final Object midVal = ((TrRubarFileState) fileState_.get(mid)).fmt_;
      final int cmp = ((Comparable) midVal).compareTo(_ft);
      if (cmp < 0) {
        low = mid + 1;
      } else if (cmp > 0) {
        high = mid - 1;
      } else {
        return mid; // key found
      }
    }
    return -(low + 1); // key not found
  }

  /**
   * @param _ft le format a rechercher
   * @return l'etat du format trouve ou null si aucun
   */
  public boolean isContained(final FileFormat _ft) {
    return indexOf(_ft) >= 0;
  }

  /**
   * @param _projectName le nouveau nom du projet
   */
  public final void setProjectName(final String _projectName) {
    if (_projectName != projectName_) {
      projectName_ = _projectName;
      fireTableDataChanged();
    }
  }

  public FDicoProjectState getState() {
    return state_;
  }

  public void setState(final FDicoProjectState _state) {
    state_ = _state;
    if (isModified()) {
      state_.setParamsModified(true);
    }
  }
}
