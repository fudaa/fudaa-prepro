/*
 *  @creation     3 mai 2005
 *  @modification $Date: 2007-05-04 14:01:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.fu.FuLog;
import java.io.IOException;
import java.util.Set;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.TrPostSource;

/**
 * Classe pour calculer la norme.
 * 
 * @author Fred Deniger
 * @version $Id: TrPostDataCreatedNorm.java,v 1.9 2007-05-04 14:01:51 deniger Exp $
 */
public final class TrPostDataCreatedNorm extends TrPostDataCreatedDefault {

  final H2dVariableType idxX_;
  final H2dVariableType idxY_;

  @Override
  public void fillWhithAllUsedVar(final Set _res) {
    _res.add(idxX_);
    _res.add(idxY_);

  }

  @Override
  public String getDescription() {
    return "norm(" + idxX_.toString() + ", " + idxY_.toString() + ')';
  }

  /**
   * @param _idxX la variable en x
   * @param _idxY la variable en y
   */
  public TrPostDataCreatedNorm(final TrPostSource _src, final H2dVariableType _idxX, final H2dVariableType _idxY) {
    super(_src);
    idxX_ = _idxX;
    idxY_ = _idxY;
  }

  @Override
  public EfData buildDataFor(final int _idxTime) {
    final EfData vx = src_.getData(idxX_, _idxTime);
    final EfData vy = src_.getData(idxY_, _idxTime);
    final double[] norm = new double[vx.getSize()];
    for (int i = norm.length - 1; i >= 0; i--) {
      final double vxi = vx.getValue(i);
      final double vyi = vy.getValue(i);
      norm[i] = Math.hypot(vxi, vyi);
    }
    return vx.isElementData() ? ((EfData) new EfDataElement(norm)) : ((EfData) new EfDataNode(norm));
  }

  @Override
  public double buildDataFor(final int _idxTime, final int _idxObject) {
    try {
      final double vx = src_.getData(idxX_, _idxTime, _idxObject);
      final double vy = src_.getData(idxY_, _idxTime, _idxObject);
      return Math.hypot(vx, vy);
    } catch (final IOException _e) {
      FuLog.warning(_e);
    }
    return 0;
  }

}