/*
 * @creation 23 nov. 06
 * @modification $Date: 2007-06-13 12:58:10 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLib;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JSplitPane;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.action.EbliCalqueActionTimeChooser;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.palette.BPalettePlageDefault;
import org.fudaa.fudaa.commun.courbe.FudaaGrapheTimeAnimatedVisuPanel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrLib;

/**
 * @author fred deniger
 * @version $Id: MvProfileFillePanel.java,v 1.11 2007-06-13 12:58:10 deniger Exp $
 */
@SuppressWarnings("serial")
public class MvProfileFillePanel extends FudaaGrapheTimeAnimatedVisuPanel {

  JComponent splitComp_;
  JSplitPane twoSpane_;
  final CtuluUI ui_;

  /**
   * @return the ui
   */
  public CtuluUI getCtuluUI() {
    return ui_;
  }

  @Override
  public EGFillePanel duplicate() {
    return new MvProfileFillePanel(getGraphe().duplicate(), ui_);
  }

  public MvProfileFillePanel(final EGGraphe _a, final CtuluUI _ui) {
    super(_a);
    ui_ = _ui;
    initActions((MvProfileTreeModel) _a.getModel(), _ui);

  }

  public MvProfileFillePanel(final MvProfileTreeModel _a, final CtuluUI _ui) {
    super(new EGGraphe(_a));
    ui_ = _ui;
    final EGAxeHorizontal h = new EGAxeHorizontal();
    h.setTitre(EbliLib.getS("Abscisse"));
    h.setUnite("m");
    getGraphe().setXAxe(h);

    initActions(_a, _ui);
  }

  private void initActions(final MvProfileTreeModel _a, final CtuluUI _ui) {
//    final EbliActionPaletteTreeModel act = new EbliActionPaletteTreeModel(MvResource.getS("Vue 2D"), MvResource.MV.getIcon("maillage"), "VUE2D") {
//
//      @Override
//      protected BPalettePanelInterface buildPaletteContent() {
//        return new MvProfileGridPalette(MvProfileFillePanel.this, _ui);
//      }
//    };
//    act.setResizable(true);
//    getTreeModel().getSelectionModel().addTreeSelectionListener(act);
//    acts.add(act);
    final List acts = new ArrayList(5);

    if (_a.containsTime()) {
      acts.add(new EbliCalqueActionTimeChooser(getTreeModel().getSelectionModel(), true));
      createVideo();

      acts.add(getVideo());
    }
    setPersonnalAction((EbliActionInterface[]) acts.toArray(new EbliActionInterface[acts.size()]));
  }

//  protected JMenu createProfileMenu() {
//    final BuMenu res = new BuMenu(MvResource.getS("Profil"), "PROFIL");
//    res.addMenuItem(MvResource.getS("Inverser le profil"), "INVERT", true, this);
////    res.addMenuItem(MvResource.getS("Supprimer les points extérieurs"), "DELETE_EXT", !getProfileTreeModel()
////        .isOutRemoved(), this);
//    final CtuluListSelectionInterface select = getSelection();
//    final boolean ok = select != null && !select.isEmpty() && !selection_.isAllSelected()
//            && select.getMinIndex() < select.getMaxIndex();
//    res.addMenuItem(MvResource.getS("Garder uniquement les points sélectionnés"), "KEEP_SELECTED", ok, this);
////    res.addMenuItem(MvResource.getS("Réinitialiser les intersections"), "INTER_INIT", !getProfileTreeModel().isInit(),
////        this);
//    return res;
//  }

  protected boolean isVueHorizontal() {
    return twoSpane_ != null && twoSpane_.getOrientation() == JSplitPane.HORIZONTAL_SPLIT;
  }

  protected void removeVueTop() {
    if (twoSpane_ != null) {
      remove(twoSpane_);
      add(vue_, BuBorderLayout.CENTER);
      revalidate();
      doLayout();
      twoSpane_ = null;
      splitComp_ = null;
    }
  }

  protected void swapVue() {
    if (twoSpane_ != null) {
      int or = twoSpane_.getOrientation();
      or = or == JSplitPane.VERTICAL_SPLIT ? JSplitPane.HORIZONTAL_SPLIT : JSplitPane.VERTICAL_SPLIT;
      remove(twoSpane_);
      twoSpane_ = new JSplitPane(or);
      if (or == JSplitPane.VERTICAL_SPLIT) {
        twoSpane_.setBottomComponent(vue_);
        twoSpane_.setTopComponent(splitComp_);
        twoSpane_.setDividerLocation(getHeight() * 1 / 3);
      } else {
        twoSpane_.setLeftComponent(vue_);
        twoSpane_.setRightComponent(splitComp_);
        twoSpane_.setDividerLocation(getWidth() * 2 / 3);
      }
      add(twoSpane_, BuBorderLayout.CENTER);
      revalidate();
      doLayout();
    }
  }

  protected void updateVueTop(final JComponent _top) {
    splitComp_ = _top;

    if (twoSpane_ != null) {
      remove(twoSpane_);
    }
    remove(vue_);
    twoSpane_ = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    twoSpane_.setTopComponent(_top);
    twoSpane_.setBottomComponent(vue_);
    twoSpane_.setOneTouchExpandable(true);
    twoSpane_.setDividerLocation(getHeight() * 1 / 3);
    add(twoSpane_, BuBorderLayout.CENTER);
    revalidate();
    doLayout();

  }

//  @Override
//  public void actionPerformed(final ActionEvent _evt) {
//    super.actionPerformed(_evt);
//  }
//
//  @Override
//  public void fillSpecificMenu(final JPopupMenu _popup) {
//    super.fillSpecificMenu(_popup);
//    _popup.add(createProfileMenu());
//
//  }

  @Override
  public Dimension getDefaultImageDimension() {
    return twoSpane_ == null ? super.getDefaultImageDimension() : twoSpane_.getSize();
  }

  public MvProfileTreeModel getProfileTreeModel() {
    return (MvProfileTreeModel) super.getTreeModel();
  }

  public boolean isVueIncrustee() {
    return twoSpane_ != null;
  }

  @Override
  public BufferedImage produceImage(final Map _params) {
    if (twoSpane_ == null) {
      return super.produceImage(_params);
    }
    final Dimension dim = twoSpane_.getSize();
    final BufferedImage res = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_INT_ARGB);
    final Graphics g = res.getGraphics();
    if (CtuluLibImage.mustFillBackground(_params)) {
      g.setColor(Color.WHITE);
      g.fillRect(0, 0, getWidth(), getHeight());
    }
    final Point p = new Point();
    vue_.getLocation(p);
    g.translate(p.x, p.y);
    vue_.paint(g);
    g.translate(-p.x, -p.y);
    splitComp_.getLocation(p);
    g.translate(p.x, p.y);
    splitComp_.paint(g);
    g.translate(-p.x, -p.y);
    g.dispose();
    res.flush();
    return res;

  }

  public static MvProfileCourbeGroup createGroupFor(final CtuluVariable _var) {
    final MvProfileCourbeGroup gri = new MvProfileCourbeGroup(_var);
    final EGAxeVertical yi = new EGAxeVertical();
    yi.setTitre(_var.toString());
    if (_var == H2dVariableType.SANS) {
      yi.setTitre(TrLib.getString("Import"));
    }

    yi.setUnite(_var.getCommonUnit());
    gri.setAxeY(yi);
    return gri;
  }

  public static void finishProfilCreation(final FudaaCommonImplementation _impl, final MvProfileFillePanel _panel,
          final MvProfileTreeFille _fille) {
    _fille.setPreferredSize(new Dimension(400, 400));
    MvProfileTreeFille.updateName(_fille);
    // _fille.setSize(new Dimension(400, 400));
    BuLib.invokeLater(new Runnable() {

      @Override
      public void run() {
        _impl.addInternalFrame(_fille);
        _panel.getGraphe().restore();

      }
    });
  }

  public static Color getColorFor(final int _i, final CtuluVariable _var) {
    Color c;
    if (_var == H2dVariableType.BATHYMETRIE) {
      c = Color.ORANGE.darker().darker();
    } else if (_var == H2dVariableType.COTE_EAU) {
      c = Color.BLUE;
    } else {
      c = BPalettePlageDefault.getColor(_i + 1);
    }
    return c;
  }

  public static void profilCreated(final FudaaCommonImplementation _impl, final MvProfileFillePanel _panel,
          final ProgressionInterface _prog, final String _name) {
    final MvProfileTreeFille fille = new MvProfileTreeFille(_panel, MvProfileBuilder.getProfileName(_name), _impl, null);
    finishProfilCreation(_impl, _panel, fille);

  }
}
