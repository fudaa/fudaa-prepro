/**
 * @creation 10 juin 2004
 * @modification $Date: 2007-01-19 13:14:12 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuTable;
import java.util.List;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarArete;
import org.fudaa.dodico.h2d.rubar.H2dRubarGrid;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.type.H2dRubarBcTypeList;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueSegment;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.model.MvEdgeModelDefault;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarAreteModelDefault.java,v 1.15 2007-01-19 13:14:12 deniger Exp $
 */
public class TrRubarAreteModelDefault extends MvEdgeModelDefault implements TrRubarAreteModel {

  private H2dRubarParameters parameter;

  /**
   * @param _g le maillage
   */
  public TrRubarAreteModelDefault(final H2dRubarGrid _g, H2dRubarParameters parameter) {
    super(_g);
    this.parameter = parameter;
  }

  @Override
  public void fillWithInfo(final InfoData _m, final ZCalqueAffichageDonneesInterface _layer) {
    super.fillWithInfo(_m, _layer, true);
  }

  public EfFrontierInterface getFr() {
    return getG().getFrontiers();
  }

  @Override
  public List getUsedBoundaryType() {
    return parameter == null ? H2dRubarBcTypeList.getList() : parameter.getBcMng().getUsedBoundaryType();
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    final BuTable b = new CtuluTable();
    b.setModel(new ZCalqueSegment.SegmentValueTableModel(this) {

      @Override
      public Object getValueAt(final int _rowIndex, final int _columnIndex) {
        if (_columnIndex == 0) {
          return new Integer(getGlobalIdx(_rowIndex) + 1);
        }
        return super.getValueAt(_rowIndex, _columnIndex);
      }
    });
    return b;
  }

  @Override
  public final H2dRubarArete getRubarArete(final int _idx) {
    return (H2dRubarArete) getArete(_idx);
  }

  @Override
  public double getXFromGlobalIdx(final int _ptIdx) {
    return getG().getPtX(_ptIdx);
  }

  @Override
  public double getYFromGlobalIdx(final int _ptIdx) {
    return getG().getPtY(_ptIdx);
  }

}