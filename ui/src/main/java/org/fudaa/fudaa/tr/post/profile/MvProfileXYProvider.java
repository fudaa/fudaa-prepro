package org.fudaa.fudaa.tr.post.profile;

import com.memoire.fu.FuEmptyArrays;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.FastBitSet;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.dodico.ef.operation.EfLineIntersection;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsBuilder;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;

/**
 * @author deniger ( genesis)
 */
public abstract class MvProfileXYProvider {
  private double yMin;
  private double yMax;

  private static abstract class Default extends MvProfileXYProvider {
    final EfLineIntersectionsResultsI res;

    public Default(EfLineIntersectionsResultsI res) {
      this.res = res;
    }

    @Override
    public double getX(final int _idx) {
      return res.getDistFromDeb(_idx);
    }

    @Override
    public double getXMax() {
      return res.getDistFromDeb(res.getNbIntersect() - 1);
    }

    @Override
    public double getXMin() {
      return 0;
    }

    @Override
    public int getNbData() {
      return res.getNbIntersect();
    }

    @Override
    public boolean isSegmentDrawn(int idx) {
      return true;
    }

    @Override
    public boolean isPointDrawn(int idx) {
      return true;
    }
  }

  private static class Values extends Default {
    private final int time_;
    private final CtuluVariable variable_;

    public Values(int time, CtuluVariable variable, EfLineIntersectionsResultsI res) {
      super(res);
      this.time_ = time;
      this.variable_ = variable;
    }

    @Override
    public boolean isSegmentDrawn(int idx) {
      return res.isSegmentIn(idx);
    }

    @Override
    public boolean isPointDrawn(int idx) {
      return res.getIntersect(idx).isRealIntersection();
    }

    @Override
    public double getY(int idx) {
      if (res.getIntersect(idx).isRealIntersection()) {
        return res.getIntersect(idx).getValue(variable_, time_);
      }
      return 0;
    }
  }

  public static class CoordinateY extends Default {
    public CoordinateY(EfLineIntersectionsResultsI res) {
      super(res);
    }

    @Override
    public double getY(final int _idx) {
      return res.getIntersect(_idx).getY();
    }
  }

  public static class CoordinateX extends Default {
    public CoordinateX(EfLineIntersectionsResultsI res) {
      super(res);
    }

    @Override
    public double getY(final int _idx) {
      return res.getIntersect(_idx).getX();
    }
  }

  public static MvProfileXYProvider.Cached createCoordinateProvider(final EfLineIntersectionsResultsI res, final boolean _isX) {
    final Cached cacheData = createCacheData(_isX ? new CoordinateX(res) : new CoordinateY(res));
    cacheData.setProfilelineString(createLineString(res));
    return cacheData;
  }

  private static MvProfileXYProvider.Cached createValueProvider(final EfLineIntersectionsResultsI res, LineString profileLineString, final int time, final CtuluVariable variable) {
    final MvProfileXYProvider.Cached cacheData = createCacheData(new Values(time, variable, res));
    cacheData.setRes(res);
    cacheData.setProfilelineString(profileLineString == null ? createLineString(res) : profileLineString);
    return cacheData;
  }

  public static MvProfileXYProvider.Cached change(MvProfileXYProvider.Cached init, final EfLineIntersectionsResultsBuilder builder, final int time, final CtuluVariable variable,
                                                  ProgressionInterface progression) {
    if (init.getRes() == null) {
      return createValueProvider(builder, time, variable, progression);
    }
    return createValueProvider(init.getRes(), init.getProfilelineString(), time, variable);
  }

  public static MvProfileXYProvider.Cached createValueProvider(final EfLineIntersectionsResultsBuilder builder, final int time, final CtuluVariable variable,
                                                               ProgressionInterface progression) {
    EfLineIntersectionsResultsI res = builder.createResults(time, progression);
    return createValueProvider(res, createLineString(res), time, variable);
  }

  private static LineString createLineString(EfLineIntersectionsResultsI in) {
    Coordinate[] cs = new Coordinate[in.getNbIntersect()];
    for (int i = 0; i < cs.length; i++) {
      EfLineIntersection intersect = in.getIntersect(i);
      cs[i] = new Coordinate(intersect.getX(), intersect.getY());
    }
    return GISGeometryFactory.INSTANCE.createLineString(cs);
  }

  public static MvProfileXYProvider.Cached createCacheData(MvProfileXYProvider in) {
    final int nbData = in.getNbData();
    double[] x = new double[nbData];
    double[] y = new double[in.getNbData()];
    FastBitSet hiddenSegment = new FastBitSet();
    FastBitSet hiddenPoint = new FastBitSet();
    for (int i = 0; i < nbData; i++) {
      x[i] = in.getX(i);
      y[i] = in.getY(i);
      if (!in.isSegmentDrawn(i)) {
        hiddenSegment.set(i);
      }
      if (!in.isPointDrawn(i)) {
        hiddenPoint.set(i);
      }
    }
    final Cached cached = new Cached(x, y, hiddenSegment.isEmpty() ? null : hiddenSegment, hiddenPoint.isEmpty() ? null
        : hiddenPoint);
    cached.recomputeYRange();
    return cached;
  }

  public static Cached createCachedData(double[] x, double[] y, FastBitSet hiddenSegment, FastBitSet hiddenPoint, LineString lineString) {
    Cached res = new Cached(x, y, hiddenSegment, hiddenPoint);
    res.setProfilelineString(lineString);
    res.recomputeYRange();
    return res;
  }

  public static class Cached extends MvProfileXYProvider {
    private final double[] x;
    private final double[] y;
    private FastBitSet hiddenSegment;
    private FastBitSet hiddenNode;
    private EfLineIntersectionsResultsI res;
    private LineString profilelineString;

    private Cached(double[] x, double[] y, FastBitSet hiddenSegment, FastBitSet hiddenPoint) {
      this.x = x == null ? FuEmptyArrays.DOUBLE0 : x;
      this.y = y == null ? FuEmptyArrays.DOUBLE0 : y;
      assert this.x.length == this.y.length;
      this.hiddenSegment = hiddenSegment;
      this.hiddenNode = hiddenPoint;
    }

    public int[] getHiddenSegments() {
      if (hiddenSegment == null) {
        return FuEmptyArrays.INT0;
      }
      return new CtuluListSelection(hiddenSegment).getSelectedIndex();
    }

    public int[] getHiddenPoints() {
      if (hiddenNode == null) {
        return FuEmptyArrays.INT0;
      }
      return new CtuluListSelection(hiddenNode).getSelectedIndex();
    }

    public EfLineIntersectionsResultsI getRes() {
      return res;
    }

    public void setRes(EfLineIntersectionsResultsI res) {
      this.res = res;
    }

    public LineString getProfilelineString() {
      return profilelineString;
    }

    public void setProfilelineString(LineString profilelineString) {
      this.profilelineString = profilelineString;
    }

    @Override
    public boolean isSegmentDrawn(int idx) {
      return hiddenSegment == null || !hiddenSegment.get(idx);
    }

    @Override
    public boolean isPointDrawn(int idx) {
      return hiddenNode == null || !hiddenNode.get(idx);
    }

    @Override
    public int getNbData() {
      return x.length;
    }

    @Override
    public double getX(int idx) {
      return x[idx];
    }

    @Override
    public double getXMax() {
      return x.length == 0 ? 0 : x[x.length - 1];
    }

    @Override
    public double getXMin() {
      return x.length == 0 ? 0 : x[0];
    }

    @Override
    public double getY(int idx) {
      return y[idx];
    }
  }

  void recomputeYRange() {
    yMin = 0;
    yMax = 0;
    for (int i = 0; i < getNbData(); i++) {
      double y = getY(i);
      if (i == 0) {
        yMin = y;
        yMax = y;
      } else {
        yMin = Math.min(yMin, y);
        yMax = Math.max(yMax, y);
      }
    }
  }

  public double getYMax() {
    return yMax;
  }

  public double getYMin() {
    return yMin;
  }

  public abstract double getX(int idx);

  public abstract double getY(int idx);

  public abstract double getXMin();

  public abstract double getXMax();

  public abstract int getNbData();

  public abstract boolean isSegmentDrawn(final int idx);

  public abstract boolean isPointDrawn(final int idx);
}
