/*
 *  @creation     20 d�c. 2005
 *  @modification $Date: 2007-06-20 12:23:42 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.export;

import com.memoire.fu.FuLog;
import gnu.trove.TIntObjectHashMap;
import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.collection.CtuluArrayDoubleUnique;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleAbstract;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.reflux.io.RefluxRefondeSolutionFileFormat;
import org.fudaa.dodico.reflux.io.RefluxRefondeSolutionSequentielResult;
import org.fudaa.dodico.reflux.io.RefluxSolutionInterface;

/**
 * @author Fred Deniger
 * @version $Id: TrExportRefluxSovAdapter.java,v 1.12 2007-06-20 12:23:42 deniger Exp $
 */
public class TrExportRefluxSovAdapter implements RefluxSolutionInterface {

  final double[] t_;
  final EfGridData data_;

  TIntObjectHashMap idxVar_;

  /**
   * @param _t
   * @param _data
   */
  public TrExportRefluxSovAdapter(final double[] _t, final EfGridData _data) {
    super();
    t_ = _t == null ? new double[] { 0 } : _t;
    data_ = _data;
    idxVar_ = new TIntObjectHashMap(8);
    final boolean vitesse = RefluxRefondeSolutionFileFormat.isOk2D(_data);
    final H2dVariableType[] var = vitesse ? RefluxRefondeSolutionSequentielResult.getReflux2DVars()
        : RefluxRefondeSolutionSequentielResult.getReflux2DSotVars();
    final int[] cols = vitesse ? RefluxRefondeSolutionSequentielResult.getReflux2DCols()
        : RefluxRefondeSolutionSequentielResult.getReflux2DSotCols();
    for (int i = var.length - 1; i >= 0; i--) {
      idxVar_.put(cols[i], var[i]);
    }
  }

  CtuluCollectionDouble zero_;

  private CtuluCollectionDouble getZero() {
    if (zero_ == null) {
      zero_ = new CtuluArrayDoubleUnique(0, getNbPt());
    }
    return zero_;
  }

  class XAdapter extends CtuluCollectionDoubleAbstract {

    @Override
    public int getSize() {
      return data_.getGrid().getPtsNb();
    }

    @Override
    public double getValue(final int _i) {
      return data_.getGrid().getPtX(_i);
    }
  }

  class YAdapter extends CtuluCollectionDoubleAbstract {

    @Override
    public int getSize() {
      return data_.getGrid().getPtsNb();
    }

    @Override
    public double getValue(final int _i) {
      return data_.getGrid().getPtY(_i);
    }
  }

  @Override
  public CtuluCollectionDouble getData(final int _idxVar, final int _time) {
    /*
     * if (_idxVar == 0) { return x_; } if (_idxVar == 1) { return y_; }
     */
    final CtuluVariable var = (CtuluVariable) idxVar_.get(_idxVar);
    try {
      if (var != null) {
        final EfData d = data_.getData(var, _time);
        return d == null ? getZero() : d;
      }
    } catch (final IOException _e) {
      FuLog.warning(_e);
    }
    return getZero();
  }

  @Override
  public int getNbPt() {
    return data_.getGrid().getPtsNb();
  }

  @Override
  public int getNbTimeStep() {
    return t_.length;
  }

  @Override
  public int getNbValue() {
    return 5;
  }

  @Override
  public double getTimeStep(final int _i) {
    return t_[_i];
  }

}
