/*
 * @creation 29 mars 2006
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.gis.*;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrSiProfilModel.Zone;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fred deniger
 * @version $Id: TrSiProfileSplitter.java,v 1.6 2007-06-28 09:28:18 deniger Exp $
 */
public class TrSiProfileSplitter {
  final PointOnGeometryLocator[] domainTester_;
  final TrSiProfilModel model_;
  final GISInfiniteLineIntersector intersector_ = new GISInfiniteLineIntersector();
  Coordinate tmp1_ = new Coordinate();
  Coordinate tmp2_ = new Coordinate();
  GISSegmentRingIntersect intersectSegRing_ = new GISSegmentRingIntersect();

  public TrSiProfileSplitter(final TrSiProfilModel _model) {
    super();
    model_ = _model;
    model_.createTesterZone();
    // domainEnv_ = model_.gridEnv_;
    domainTester_ = model_.testers_;
  }

  protected void computeZones(final int _idxProfil, final Zone _zones, final CtuluAnalyze _analyze) {
    final int domaine = model_.getDomaineIdx(_idxProfil);
    if (domaine < 0) {
      _analyze.addFatalError(TrLib.getString("Domaine non trouv�"));
      return;
    }
    final LineString profil = (LineString) model_.getGeomData().getGeometry(_idxProfil);
    final int nbProfilPts = profil.getNumPoints();
    // xi--------xii
    // | |
    // | |
    // pi pii
    final Coordinate xAval = new Coordinate();
    Coordinate pi = new Coordinate();
    final Coordinate pii = new Coordinate();
    final LinearRing frRing = model_.frRing_[domaine];
    intersectSegRing_.setFrRing(frRing);
    int oldTopIdx = -1;
    int oldBottomIdx = -1;
    final int nbPt = frRing.getNumPoints() - 1;
    Coordinate oldTop = null;
    Coordinate oldBottom = null;
    final Coordinate xNext = new Coordinate();
    final PointOnGeometryLocator included = domainTester_[domaine];
    int idxZone = 0;
    boolean lastIsInclude = true;
    for (idxZone = 0; idxZone < nbProfilPts; idxZone++) {
      profil.getCoordinateSequence().getCoordinate(idxZone, xAval);
      // le point n'est pas contenu: on ne le traite pas
      if (!GISLib.isInside(included, xAval)) {
        lastIsInclude = false;
        if (idxZone == 0) {
          continue;
        }
        break;
      }
      // calcul de pi
      // pour le premier et le dernier point, on prend la perpendiculaire
      if (idxZone == 0 || idxZone == nbProfilPts - 1) {
        if (idxZone == 0) {
          profil.getCoordinateSequence().getCoordinate(1, tmp1_);
        } else {
          profil.getCoordinateSequence().getCoordinate(idxZone - 1, tmp1_);
        }
        final double vx = -(tmp1_.y - xAval.y);
        final double vy = tmp1_.x - xAval.x;
        pi.x = xAval.x + vx;
        pi.y = xAval.y + vy;
      }
      // sinon, on recherche le point pi a partir de la bissectrice
      else {
        profil.getCoordinateSequence().getCoordinate(idxZone - 1, tmp1_);
        double vx = -(xAval.y - tmp1_.y);
        double vy = xAval.x - tmp1_.x;
        double norm1 = Math.hypot(vx, vy);
        tmp1_.x = xAval.x + (tmp1_.x - xAval.x) / norm1;
        tmp1_.y = xAval.y + (tmp1_.y - xAval.y) / norm1;
        pi.x = tmp1_.x + vx;
        pi.y = tmp1_.y + vy;
        profil.getCoordinateSequence().getCoordinate(idxZone + 1, tmp2_);
        vx = -(tmp2_.y - xAval.y);
        vy = tmp2_.x - xAval.x;
        norm1 = Math.hypot(vx, vy);
        tmp2_.x = xAval.x + (tmp2_.x - xAval.x) / norm1;
        tmp2_.y = xAval.y + (tmp2_.y - xAval.y) / norm1;
        pii.x = tmp2_.x + vx;
        pii.y = tmp2_.y + vy;
        intersector_.computeIntersectionLineLine(tmp1_, pi, tmp2_, pii);
        if (intersector_.hasIntersection()) {
          pi = new Coordinate(intersector_.getIntersectedPoint());
        } else {
          /*
           * vx = -(xii.y - xi.y); vy = xii.x - xi.x;
           */
          pi.x = xAval.x + vx;
          pi.y = xAval.y + vy;
        }
      }
      // on recherche les intersections sur l'enveloppe
      if (idxZone < nbProfilPts - 1) {
        profil.getCoordinateSequence().getCoordinate(idxZone + 1, xNext);
      } else {
        profil.getCoordinateSequence().getCoordinate(idxZone - 1, xNext);
        xNext.x = 2 * xAval.x - xNext.x;
        xNext.y = 2 * xAval.y - xNext.y;
      }
      intersectSegRing_.compute(xAval, pi, xAval, xNext);
      // on construit la partie de l'enveloppe contenant les point xi->xii
      final List coord = new ArrayList();
      if (intersectSegRing_.isCoorNull()) {
        _analyze.addFatalError(TrResource.getS("Impossible de cr�er les surfaces libres"));
        return;
      }
      // on doit tester que les deux indices ne sont pas deja pris dans la zone pr�c�dente
      // si une zone est deja d�finie
      // top est ok si le nouveau point n'appartient pas � la zone deja d�fini
      boolean topIsOk = true;
      boolean bottomIsOk = true;
      boolean isTopMerged = false;
      boolean isBottomMerged = false;
      if (oldTopIdx > 0) {
        topIsOk = !EfLib.isContained(intersectSegRing_.getIdx2(), oldTopIdx, oldBottomIdx, nbPt);
        // dans ce cas il faut voir quel point d'intersection est le plus proche
        if (intersectSegRing_.getIdx1() == oldTopIdx) {
          final Coordinate ci = frRing.getCoordinateN(oldTopIdx);
          final double dNew = CtuluLibGeometrie.getD2(intersectSegRing_.getCoordinate2(), ci);
          final double dOld = CtuluLibGeometrie.getD2(oldTop, ci);
          topIsOk = dNew < dOld;
        }
        bottomIsOk = !EfLib.isContained(intersectSegRing_.getIdx1(), oldTopIdx, oldBottomIdx, nbPt);
        // dans ce cas il faut voir quel point d'intersection est le plus proche
        if (intersectSegRing_.getIdx1() == oldBottomIdx) {
          final Coordinate ci = frRing.getCoordinateN(oldBottomIdx);
          final double dNew = CtuluLibGeometrie.getD2(intersectSegRing_.getCoordinate1(), ci);
          final double dOld = CtuluLibGeometrie.getD2(oldBottom, ci);
          bottomIsOk = dNew > dOld;
        }
      }
      if (!bottomIsOk && !topIsOk) {
        isAllZoneOk_ = false;
        continue;
      } else if (!topIsOk) {
        // on construit l'intersection a partir de l'ancien point sup.
        if (FuLog.isDebug()) {
          FuLog.debug("TSI: top point is not good: merge");
        }
        // computeIntersectPoints(intersector, xAval, oldTop, inter, envelopIdx, xAval, xNext);
        intersectSegRing_.compute(xAval, oldTop, xAval, xNext);
        if (intersectSegRing_.getIdx2() != oldTopIdx) {
          throw new IllegalArgumentException("error in methode");
        }
        isTopMerged = true;
        bottomIsOk = !EfLib.isContained(intersectSegRing_.getIdx1(), oldTopIdx, oldBottomIdx, nbPt);
        // dans ce cas il faut voir quel point d'intersection est le plus proche
        if (intersectSegRing_.getIdx1() == oldBottomIdx) {
          final Coordinate ci = frRing.getCoordinateN(oldBottomIdx);
          final double dNew = CtuluLibGeometrie.getD2(intersectSegRing_.getCoordinate1(), ci);
          final double dOld = CtuluLibGeometrie.getD2(oldBottom, ci);
          bottomIsOk = dNew > dOld;
        }
        if (!bottomIsOk) {
          isAllZoneOk_ = false;
          if (FuLog.isDebug()) {
            FuLog.debug("TSI: bottom point is not good after merging");
          }
          continue;
        }
      } else if (!bottomIsOk) {
        // on construit l'intersection a partir de l'ancien point sup.
        if (FuLog.isDebug()) {
          FuLog.debug("TSI: bottom point is not good: merge");
        }
        // computeIntersectPoints(intersector, xAval, oldBottom, inter, envelopIdx, xAval, xNext);
        intersectSegRing_.compute(xAval, oldBottom, xAval, xNext);
        if (intersectSegRing_.getIdx1() != oldBottomIdx) {
          throw new IllegalArgumentException("error in methode");
        }
        isBottomMerged = true;
        topIsOk = !EfLib.isContained(intersectSegRing_.getIdx2(), oldTopIdx, oldBottomIdx, nbPt);
        // dans ce cas il faut voir quel point d'intersection est le plus proche
        if (intersectSegRing_.getIdx2() == oldTopIdx) {
          final Coordinate ci = frRing.getCoordinateN(oldTopIdx);
          final double dNew = CtuluLibGeometrie.getD2(intersectSegRing_.getCoordinate2(), ci);
          final double dOld = CtuluLibGeometrie.getD2(oldTop, ci);
          topIsOk = dNew < dOld;
        }
        if (!topIsOk) {
          isAllZoneOk_ = false;
          if (FuLog.isDebug()) {
            FuLog.debug("TSI: top point is not good after merging");
          }
          continue;
        }
      }
      // test inutile ?
      if (!bottomIsOk && !topIsOk) {
        isAllZoneOk_ = false;
        continue;
      }

      coord.add(new Coordinate(intersectSegRing_.getCoordinate1()));
      coord.add(new Coordinate(intersectSegRing_.getCoordinate2()));
      // on cree l'envelop dans le sens trigo
      // l'indice de d�part
      final int stopIdx = intersectSegRing_.getIdx1() + 1;
      // cas particulier ou les point sont sur le meme segment.
      if (oldTopIdx == intersectSegRing_.getIdx2()) {
        coord.add(new Coordinate(oldTop));
      }
      for (int idx = intersectSegRing_.getIdx2() + 1; idx != stopIdx; idx = (idx + 1) % nbPt) {
        // le point du bas doit etre ajoute avant les points sur la frontieres
        // si le point bas a �t� merg�, il ne faut pas le prendre ( c'est le meme que inter[0]
        if (!isBottomMerged && (oldBottom != null) && (idx == oldBottomIdx)) {
          coord.add(new Coordinate(oldBottom));
        }
        // ajout des points de fronti�res
        if (oldBottomIdx < 0 // il n'y pas de zone pr�c�dente
            || (idx == oldTopIdx && oldTopIdx != intersectSegRing_.getIdx2() && !isTopMerged) // le point top est
            // particulier
            // il doit etre ajoute sauf si l'intersection de 2 zones se trouve sur le meme segment ou si le point
            // haut a �t� merge.
            || !EfLib.isContained(idx, oldTopIdx, oldBottomIdx, nbPt)) {
          coord.add(frRing.getCoordinateSequence().getCoordinateCopy(idx));
        }
        // Le point du haut doit etre ajoute apres
        if (!isTopMerged && (oldTop != null) && (idx == oldTopIdx)) {
          coord.add(new Coordinate(oldTop));
        }
      }
      coord.add(intersectSegRing_.getCoordinate1());
      final Coordinate[] cs = new Coordinate[coord.size()];
      coord.toArray(cs);
      oldTop = new Coordinate(intersectSegRing_.getCoordinate2());
      oldTopIdx = intersectSegRing_.getIdx2();
      oldBottom = new Coordinate(intersectSegRing_.getCoordinate1());
      oldBottomIdx = intersectSegRing_.getIdx1();

      if (cs.length > 3) {
        final TrSIZone zone = new TrSIZone(GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(cs),
            _idxProfil);
        if (idxZone > 0) {
          zone.setPtAmont(idxZone - 1);
          zone.setPtAmontIsExt(!lastIsInclude);
          zone.setPtAval(idxZone);
        } else {
          zone.setPtAval(idxZone);
        }
        if (zone.isValid()) {
          _zones.add(zone);
        }
      } else {
        FuLog.debug("TSI: zone non ajout�e " + idxZone);
      }
      // si on est ici, le point est inclus
      lastIsInclude = true;
    }
    // on cree le dernier
    if (oldBottomIdx != oldTopIdx && oldTop != null && oldBottom != null) {
      final List coord = new ArrayList();
      coord.add(new Coordinate(oldTop));
      coord.add(oldBottom);
      // on cree l'envelop dans le sens trigo
      final int stopIdx = oldTopIdx + 1;
      for (int idx = oldBottomIdx + 1; idx != stopIdx; idx = (idx + 1) % nbPt) {
        if (idx == oldTopIdx || !EfLib.isContained(idx, oldTopIdx, oldBottomIdx, nbPt)) {
          coord.add(frRing.getCoordinateSequence().getCoordinateCopy(idx));
        }
      }

      coord.add(new Coordinate(oldTop));
      final Coordinate[] cs = new Coordinate[coord.size()];
      coord.toArray(cs);
      if (cs.length > 3) {
        final TrSIZone zone = new TrSIZone(GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(cs),
            _idxProfil);
        // le point amont
        zone.setPtAvalIsExt(true);
        zone.setPtAmont(idxZone - 1);
        // Si ce n'est pas le dernier point, on met a jour le point amont et le point aval
        if (idxZone <= (nbProfilPts - 1)) {

          zone.setPtAval(idxZone);
        }
        _zones.add(zone);
      }
    }
  }

  boolean isAllZoneOk_;

  private void computeBerge(final Zone _zone, final LineString _berge) {
    final int nb = _zone.getNbZone();
    for (int i = 0; i < nb; i++) {
      final TrSIZone ringi = (TrSIZone) _zone.getGeometry(i);
      // a faire que si la berge _s traverse la la zone
      if (_berge.crosses(ringi)) {
        Polygon zonei = null;
        Envelope envi = ringi.getEnvelopeInternal();
        // on cree un polygone representant la zone i
        if (ringi.lit_ == null) {
          zonei = GISGeometryFactory.INSTANCE.createPolygon(ringi, null);
        } else {
          final int idx = _zone.indexOf(ringi.lit_);
          _zone.removeGeometries(new int[]{idx}, null);
          zonei = GISGeometryFactory.INSTANCE.createPolygon(ringi.lit_, null);
          envi = ringi.lit_.getEnvelopeInternal();
        }
        // on construit l'envelope de la zone i en etant un peu plus large
        // afin d'�viter les erreur d'arrondi
        final double security = 0.01;

        final double minX = envi.getMinX() - security;
        final double maxX = envi.getMaxX() + security;
        final double minY = envi.getMinY() - security;
        final double maxY = envi.getMaxY() + security;
        final Polygon envelopPolyi = GISGeometryFactory.INSTANCE.createPolygon(GISGeometryFactory.INSTANCE
            .createLinearRing(minX, maxX, minY, maxY), null);
        // on obtient les fragments de la berge _s contenus par la zone pi
        final Geometry includeLines = envelopPolyi.intersection(_berge);
        // si au moins deux point
        if (includeLines != null && includeLines.getNumGeometries() > 0) {
          // on parcourt les lignes d'intersect
          final GISEnvelopeSplitter split = new GISEnvelopeSplitter(minX, maxX, minY, maxY);
          Polygon newPoly = null;
          for (int j = includeLines.getNumGeometries() - 1; j >= 0; j--) {
            final LineString lj = (LineString) includeLines.getGeometryN(j);
            final Polygon pj = split.split(lj);
            // normalement toujours non nul
            Polygon tmpPoly = null;
            if (pj != null) {
              if (ringi.isGeomContainsDirectorPoint((LineString) model_.getGeomData()
                  .getGeometry(ringi.getProfileIdx()), pj)) {
                tmpPoly = (Polygon) zonei.intersection(pj);
              } else {
                tmpPoly = (Polygon) zonei.difference(pj);
              }
            }
            if (newPoly == null) {
              newPoly = tmpPoly;
            } else {
              final Geometry g = newPoly.intersection(tmpPoly);
              if (g instanceof Polygon) {
                newPoly = (Polygon) g;
              } else {
                for (int polyR = g.getNumGeometries() - 1; polyR >= 0; polyR--) {
                  if (g.getGeometryN(polyR) instanceof Polygon) {
                    newPoly = (Polygon) g.getGeometryN(polyR);
                    break;
                  }
                }
              }
            }
          }
          if (newPoly != null) {
            final GISPolygone lit = (GISPolygone) GISGeometryFactory.INSTANCE.createLinearRing(newPoly
                .getExteriorRing().getCoordinateSequence());
            ringi.setLit(lit);
            _zone.addLitMineure(i, lit);
          }
        }
      }
    }
  }

  private void computeBerges(final Zone _zones) {
    // il y a seulement un profil ou pas de zones: pas la peine
    if (model_.getNombre() == 1 || _zones == null || _zones.getNumGeometries() == 0) {
      return;
    }
    // final LineString l = getProfilLine();
    for (int i = model_.getNombre() - 1; i >= 0; i--) {
      // on parcourt uniquement les berges
      if (!model_.isProfile(i)) {
        computeBerge(_zones, model_.getLine(i));
      }
    }
  }

  protected void computeZones(final Zone _zones, final CtuluAnalyze _analyze) {
    isAllZoneOk_ = true;
    if (_zones == null) {
      return;
    }
    _zones.clear();
    _zones.setEditable(true);
    if (_zones.getNumGeometries() > 0) {
      FuLog.warning(new Throwable());
    }
    if (model_.isBergesIntersectProfil()) {
      _analyze.addFatalError(TrResource.getS("Des berges coupent le profil"));
      return;
    }
    if (model_.isProfilPointsOut()) {
      _analyze.addFatalError(TrResource.getS("Des points internes au profil sont � l'ext�rieur du mod�le"));
      return;
    }
    if (model_.isProfilTwoPointOut()) {
      return;
    }
    final int[] idx = model_.getProfilLinesIdx();
    for (int i = 0; i < idx.length; i++) {
      computeZones(idx[i], _zones, _analyze);
    }

    if (!isAllZoneOk_) {
      _analyze.addWarn(TrResource.getS("Certaines zones non pas pu �tre d�termin�es"), -1);
    }
    _zones.zoneExternBuild();
    computeBerges(_zones);
    _zones.zoneBuildFinal();
    _zones.setEditable(false);
  }
}
