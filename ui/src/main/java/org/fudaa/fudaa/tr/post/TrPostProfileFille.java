/*
 * @creation 23 nov. 06
 * @modification $Date: 2007-05-04 14:01:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuInformationsDocument;
import java.util.Set;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.animation.EbliAnimationSourceInterface;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.post.data.TrPostDataListener;
import org.fudaa.fudaa.tr.post.profile.MvProfileFillePanel;
import org.fudaa.fudaa.tr.post.profile.MvProfileTreeFille;

public class TrPostProfileFille extends MvProfileTreeFille implements TrPostDataListener, TrPostTimeContentListener {

  public TrPostProfileFille(final MvProfileFillePanel _g, final String _titre, final FudaaCommonImplementation _appli,
      final BuInformationsDocument _id) {
    super(_g, _titre, _appli, _id);
  }

  @Override
  public void updateTimeStep(final int[] _idx, final ProgressionInterface _prog) {
    super.getProfileModel().updateTimeStep(_idx, _prog);

  }

  @Override
  public EbliAnimationSourceInterface getAnimationSrc() {
    return getProfilePanel().getAnimationSrc();
  }

  @Override
  public void dataAdded(final boolean _isFleche) {}

  @Override
  public void dataChanged(final H2dVariableType _old, final H2dVariableType _new, final boolean _contentChanged,
      final boolean _isFleche, final Set _varsUsing) {
    super.updateCache(_old, _new, _contentChanged, _varsUsing);

  }

  @Override
  public void dataRemoved(final H2dVariableType[] _vars, final boolean _isFleche) {}

}