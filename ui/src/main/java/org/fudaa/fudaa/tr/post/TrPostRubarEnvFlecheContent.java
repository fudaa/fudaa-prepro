/*
 *  @creation     20 mai 2005
 *  @modification $Date: 2007-06-05 09:01:13 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import java.util.Set;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreatedSaver;
import org.fudaa.fudaa.tr.post.data.TrPostDataVecteur;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id: TrPostRubarEnvFlecheContent.java,v 1.9 2007-06-05 09:01:13 deniger Exp $
 */
public class TrPostRubarEnvFlecheContent implements TrPostFlecheContent {

  private final transient TrPostSourceRubar src_;
  final H2dVariableType name_;
  TrPostDataVecteur val_;
  final H2dVariableType varVx_;
  final H2dVariableType varVy_;

  /**
   * @param _name
   * @param _idxX
   * @param _idxY
   */
  public TrPostRubarEnvFlecheContent(final TrPostSourceRubar _src, final H2dVariableType _name,
                                     final H2dVariableType _idxX, final H2dVariableType _idxY) {
    src_ = _src;
    varVx_ = _idxX;
    varVy_ = _idxY;
    name_ = _name;
  }

  @Override
  public void restore() {
  }

  @Override
  public H2dVariableType getVx() {
    return varVx_;
  }

  @Override
  public H2dVariableType getVy() {
    return varVy_;
  }

  @Override
  public TrPostFlecheContent changeVar(final H2dVariableType _newName) {
    return new TrPostRubarEnvFlecheContent(src_, _newName, varVx_, varVy_);
  }

  @Override
  public void clearCache() {
    val_ = null;
  }

  @Override
  public TrPostDataCreatedSaver createSaver() {
    return null;
  }

  @Override
  public TrPostFlecheContent duplicate(final TrPostSource _src) {
    return new TrPostRubarEnvFlecheContent((TrPostSourceRubar) _src, name_, varVx_, varVy_);
  }

  @Override
  public TrPostFlecheContent duplicate(final TrPostSource _src, final H2dVariableType _var) {
    return new TrPostRubarEnvFlecheContent((TrPostSourceRubar) _src, _var, varVx_, varVy_);
  }

  @Override
  public void fillWhithAllUsedVar(final Set _res) {
    _res.add(varVx_);
    _res.add(varVy_);

  }

  @Override
  public EfData getDataFor(final int _idxTime) {
    return getValues(_idxTime);
  }

  @Override
  public String getDescription() {
    return toString();
  }

  @Override
  public double getValue(final int _idxTime, final int _idxObject) {
    return getValues(_idxTime).getValue(_idxObject);
  }

  @Override
  public TrPostDataVecteur getValues(final int _tIdx) {
    if (val_ == null) {
      val_ = new TrPostDataVecteur(src_.getEnveloppData(varVx_), src_.getEnveloppData(varVy_), src_.getGrid());
    }
    return val_;
  }

  @Override
  public H2dVariableType getVar() {
    return name_;
  }

  @Override
  public String getVxDesc() {
    return varVx_.getName();
  }

  @Override
  public String getVyDesc() {
    return varVy_.getName();
  }

  @Override
  public boolean isEditable() {
    return false;
  }

  @Override
  public boolean isExpr() {
    return false;
  }

  @Override
  public TrPostFlecheContent isFleche() {
    return this;
  }

  @Override
  public String toString() {
    return name_.getName();
  }

  @Override
  public boolean updateConstantVar(final Variable _var) {
    return false;
  }
}
