/**
 *  @creation     7 f�vr. 2005
 *  @modification $Date: 2007-01-19 13:14:12 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.rubar;

import java.util.List;
import org.fudaa.dodico.ef.EfFrontierInterface;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarBcAreteModel.java,v 1.5 2007-01-19 13:14:12 deniger Exp $
 */
public interface TrRubarBcAreteModel extends TrRubarAreteModel {

  /**
   * @return la liste des bords utilises
   */
  @Override
  List getUsedBoundaryType();

  /**
   * @return la liste de tous les bords disponibles pour le projet en cours
   */
  List getBordList();

  /**
   * @return le nombre de bord diff utilise
   */
  int getNbTypeBord();

  EfFrontierInterface getFr();
}