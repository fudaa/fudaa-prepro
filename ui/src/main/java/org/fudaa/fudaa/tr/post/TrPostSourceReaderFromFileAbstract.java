/**
 * @creation 24 mars 2004
 * @modification $Date: 2007-05-04 14:01:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import gnu.trove.TObjectIntHashMap;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * @author Fred Deniger
 * @version $Id: TrPostSourceSerafin.java,v 1.15 2007-05-04 14:01:52 deniger Exp $
 */
public abstract class TrPostSourceReaderFromFileAbstract extends TrPostSourceReaderCommonAbstract {
  final TObjectIntHashMap varIdx_;

  protected TrPostSourceReaderFromFileAbstract(Collection<File> _file, final double[] _time, H2dVariableType[] _initVar) {
    super(_file, _time, _initVar);
    varIdx_ = new TObjectIntHashMap(_initVar == null ? 2 : _initVar.length);
    if (_initVar != null) {
      for (int i = 0; i < _initVar.length; i++) {
        H2dVariableType variableType = _initVar[i];
        varIdx_.put(variableType, i);
      }
    }
  }

  protected TrPostSourceReaderFromFileAbstract(final File _file, final double[] _time, H2dVariableType[] _initVar) {
    this(Collections.unmodifiableCollection(Arrays.asList(_file)), _time, _initVar);
  }

  @Override
  public EfData getInitData(CtuluVariable _varIdx, int _timeIdx) {
    int idx = getInitPostion(_varIdx);
    if (_timeIdx < 0 || idx < 0) {
      return null;
    }
    return getInitData(idx, _timeIdx);
  }

  @Override
  public double getInitData(CtuluVariable _varIdx, int _timeIdx, int _ptIdx) throws IOException {
    int idx = getInitPostion(_varIdx);
    if (_timeIdx < 0 || idx < 0) {
      return 0;
    }
    return getInitData(idx, _timeIdx, _ptIdx);
  }

  int getInitPostion(CtuluVariable _v) {
    if (_v == null || !varIdx_.contains(_v)) {
      return -1;
    }
    return varIdx_.get(_v);
  }

  @Override
  public String getTitle() {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * @param _varIdx
   * @param _timeIdx
   */
  public abstract EfData getInitData(int _varIdx, int _timeIdx);

  /**
   * @param _varIdx
   * @param _timeIdx
   * @param _ptIdx
   */
  public abstract double getInitData(int _varIdx, int _timeIdx, int _ptIdx) throws IOException;
}
