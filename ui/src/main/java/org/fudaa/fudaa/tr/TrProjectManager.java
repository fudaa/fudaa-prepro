/*
 *  @creation     29 avr. 2003
 *  @modification $Date: 2007-06-28 09:28:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.fudaa.fudaa.tr.common.TrProjet;

/**
 * @author deniger
 * @version $Id: TrProjectManager.java,v 1.15 2007-06-28 09:28:19 deniger Exp $
 */
public class TrProjectManager {

  Set projets_;
  TrProjet currentProjet_;

  public TrProjectManager() {
    projets_ = new HashSet(5);
  }

  /**
   * Renvoie le projet courant.
   */
  public TrProjet getCurrentProjet() {
    return currentProjet_;
  }

  public TrProjet containsProjectTitre(final String _s) {
    TrProjet p;
    for (final Iterator it = projets_.iterator(); it.hasNext();) {
      p = (TrProjet) it.next();
      if (p.getTitle().equals(_s)) { return p; }
    }
    return null;
  }

  /**
   * Ajoute un projet au manager.
   */
  public boolean addProjet(final TrProjet _pro) {
    final String n = _pro.getTitle();
    if (containsProjectTitre(n) != null) { return false; }
    projets_.add(_pro);
    currentProjet_ = _pro;
    return true;
  }

  public void closeProjet(final TrProjet _pro) {
    projets_.remove(_pro);
    currentProjet_ = null;
  }

  public TrProjet getProjet(final String _n) {
    return containsProjectTitre(_n);
  }
}