/*
 * @creation 19 sept. 06
 * @modification $Date: 2007-01-19 13:14:11 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuInformationsDocument;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrGrapheTreeTimeFille;

/**
 * @author fred deniger
 * @version $Id: TrPostRubarLiminFilleTree.java,v 1.2 2007-01-19 13:14:11 deniger Exp $
 */
public class TrPostRubarLiminFilleTree extends TrGrapheTreeTimeFille {

  public TrPostRubarLiminFilleTree(final EGGraphe _g, final String _titre, final FudaaCommonImplementation _appli,
      final BuInformationsDocument _id) {
    super(_g, _titre, _appli, _id);
    setName("ifPostLimniTree");
  }

  @Override
  protected String getMenuTitle() {
    return TrPostRubarLiminFilleSimple.getFilleTitle();
  }

  @Override
  public String getShortHtmlHelp() {
    return TrPostRubarLiminFilleSimple.getShortHtmlHelp(getTitle(), impl_);
  }
}