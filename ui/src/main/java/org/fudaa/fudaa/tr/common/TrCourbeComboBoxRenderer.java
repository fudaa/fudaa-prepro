/**
 *  @creation     25 juin 2004
 *  @modification $Date: 2006-09-19 15:07:30 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuResource;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;

/**
 * @author Fred Deniger
 * @version $Id: TrCourbeComboBoxRenderer.java,v 1.15 2006-09-19 15:07:30 deniger Exp $
 */
public class TrCourbeComboBoxRenderer extends CtuluCellTextRenderer {
  
  private boolean firstSelectionIsEmpty;

  public boolean isFirstSelectionIsEmpty() {
    return firstSelectionIsEmpty;
  }

  public void setFirstSelectionIsEmpty(boolean firstSelectionIsEmpty) {
    this.firstSelectionIsEmpty = firstSelectionIsEmpty;
  }

  @Override
  protected void setValue(final Object _value) {
    if (_value == null) {
      setText(CtuluLibString.ESPACE + getNoSelectionText());
      if (isEnabled()) {
        setIcon(BuResource.BU.getIcon("arreter"));
      }
    } else {
      setText(_value.toString());
      setIcon(null);
    }
  }

  private String getNoSelectionText() {
    return firstSelectionIsEmpty?TrResource.getS("Sélectionner une courbe"):TrResource.getS("Pas de courbes disponibles");
  }
}