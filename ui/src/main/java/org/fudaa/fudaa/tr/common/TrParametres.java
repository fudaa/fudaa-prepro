/**
 *  @creation     20 ao�t 2003
 *  @modification $Date: 2006-09-19 15:07:30 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr.common;

import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.H2dParameters;

/**
 * @author deniger
 * @version $Id: TrParametres.java,v 1.11 2006-09-19 15:07:30 deniger Exp $
 */
public interface TrParametres {
  boolean loadData(ProgressionInterface _interface);

  boolean isGeometrieLoaded();

  H2dParameters getH2dParametres();

  boolean canImportEvolution();

  TrCourbeTemporelleManager getEvolMng();
}
