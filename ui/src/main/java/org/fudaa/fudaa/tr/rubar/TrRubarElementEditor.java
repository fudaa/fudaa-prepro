/**
 * @creation 14 juin 2004
 * @modification $Date: 2007-05-04 14:01:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.gui.CtuluValuesCommonEditorPanel;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarEvolution;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.tr.common.TrCourbeTemporelleManager;
import org.fudaa.fudaa.tr.data.TrValuesEditorBuilder;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarElementEditor.java,v 1.22 2007-05-04 14:01:53 deniger Exp $
 */
public class TrRubarElementEditor extends CtuluValuesCommonEditorPanel {

  H2dRubarParameters params_;

  TrCourbeTemporelleManager evolMng_;

  H2dRubarEvolution mixte_;

  private BuComboBox cbVentX_;

  private BuComboBox cbVentY_;

  /**
   * @param _eltIdx les indices a modifier
   * @param _evolMng le gestionnaires des evolutions
   * @param _params les params
   * @param _mng le gestionnaire de commande
   */
  public TrRubarElementEditor(final int[] _eltIdx, final TrCourbeTemporelleManager _evolMng, final H2dRubarParameters _params,
      final CtuluCommandManager _mng) {
    super(TrValuesEditorBuilder.buildParameters(_params.getElementData(), _eltIdx,null));
    super.setCmd(_mng);
    evolMng_ = _evolMng;
    params_ = _params;

    H2dRubarEvolution apport = _params.getAppMng().getCommonEvol(_eltIdx);
    H2dRubarEvolution ventx = _params.getVentMng().getCommonEvolAlongX(_eltIdx);
    H2dRubarEvolution venty = _params.getVentMng().getCommonEvolAlongY(_eltIdx);
    cbApport_ = new BuComboBox();
    cbVentX_ = new BuComboBox();
    cbVentY_ = new BuComboBox();
    if (apport == null) {
      createMixte();
      apport = mixte_;
      evolMng_.createComboBoxModelWithEmpty(mixte_, H2dRubarEvolution.EMPTY_EVOL, H2dVariableTransType.APPORT_PLUIE, cbApport_);
    } else {
      evolMng_.createComboBoxModelWithEmpty(H2dRubarEvolution.EMPTY_EVOL, H2dVariableTransType.APPORT_PLUIE, cbApport_);
    }
    if (ventx == null) {
      createMixte();
      ventx = mixte_;
      evolMng_.createComboBoxModelWithEmpty(mixte_, H2dRubarEvolution.EMPTY_EVOL, H2dVariableTransType.VENT, cbVentX_);
    } else {
      evolMng_.createComboBoxModelWithEmpty(H2dRubarEvolution.EMPTY_EVOL, H2dVariableTransType.VENT, cbVentX_);
    }
    if (venty == null) {
      createMixte();
      venty = mixte_;
      evolMng_.createComboBoxModelWithEmpty(mixte_, H2dRubarEvolution.EMPTY_EVOL, H2dVariableTransType.VENT, cbVentY_);
    } else {
      evolMng_.createComboBoxModelWithEmpty(H2dRubarEvolution.EMPTY_EVOL, H2dVariableTransType.VENT, cbVentY_);
    }
    JPanel curves = new JPanel(new BuGridLayout(2, 5, 7));
    addLabel(curves, H2dResource.getS("Chronique d'apport"));
    if (apport != null) {
      cbApport_.setSelectedItem(apport);
    }
    curves.add(cbApport_);
    addLabel(curves, H2dVariableType.VENT_X.getName());
    if (ventx != null) {
      cbVentX_.setSelectedItem(ventx);
    }
    curves.add(cbVentX_);
    addLabel(curves, H2dVariableType.VENT_Y.getName());
    if (venty != null) {
      cbVentY_.setSelectedItem(venty);
    }
    curves.add(cbVentY_);
    add(curves, BorderLayout.SOUTH);

  }

  private void createMixte() {
    if (mixte_ == null)
      mixte_ = new H2dRubarEvolution('<' + FudaaLib.getS("S�lection mixte") + '>');
  }

  BuComboBox cbApport_;

  @Override
  public boolean apply() {
    final CtuluCommandComposite c = super.internApply();
    final Object apport = cbApport_.getSelectedItem();
    if (apport != null && apport != mixte_) {
      H2dRubarEvolution evol = (H2dRubarEvolution) apport;
      if (evol == H2dRubarEvolution.EMPTY_EVOL) {
        evol = null;
      }
      params_.getAppMng().set(idx_, evol, c);
    }
    final Object ventx = cbVentX_.getSelectedItem();
    if (ventx != null && ventx != mixte_) {
      H2dRubarEvolution evol = (H2dRubarEvolution) ventx;
      if (evol == H2dRubarEvolution.EMPTY_EVOL) {
        evol = null;
      }
      params_.getVentMng().setVentX(idx_, evol, c);
    }
    final Object venty = cbVentY_.getSelectedItem();
    if (venty != null && venty != mixte_) {
      H2dRubarEvolution evol = (H2dRubarEvolution) venty;
      if (evol == H2dRubarEvolution.EMPTY_EVOL) {
        evol = null;
      }
      params_.getVentMng().setVentY(idx_, evol, c);
    }
    cmd_.addCmd(c.getSimplify());
    return true;
  }
}
