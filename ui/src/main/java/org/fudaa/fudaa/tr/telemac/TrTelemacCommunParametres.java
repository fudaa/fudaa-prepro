/**
 * @creation 29 sept. 2003
 * @modification $Date: 2007-06-28 09:28:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.telemac;

import com.db4o.ObjectContainer;
import com.memoire.fu.FuLog;
import java.io.File;
import java.io.IOException;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluArkSaver;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dParameters;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcManager;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLSourceInterface;
import org.fudaa.dodico.h2d.telemac.H2dTelemacDicoParams;
import org.fudaa.dodico.h2d.telemac.H2dTelemacEvolutionFrontiere;
import org.fudaa.dodico.h2d.telemac.H2dTelemacParameters;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuilInterface;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuilSiphonContainer;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSiphonInterface;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSourceMng;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.telemac.TelemacDicoFileFormatVersion;
import org.fudaa.dodico.telemac.io.TelemacCLAdapter;
import org.fudaa.dodico.telemac.io.TelemacCLVersion;
import org.fudaa.dodico.telemac.io.TelemacLiquideInterface;
import org.fudaa.dodico.telemac.io.TelemacSeuilSiphonFileFormat;
import org.fudaa.fudaa.commun.save.FudaaSavable;
import org.fudaa.fudaa.fdico.FDicoParams;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.tr.common.TrCourbeTemporelleManager;
import org.fudaa.fudaa.tr.common.TrImplementationEditorAbstract;
import org.fudaa.fudaa.tr.common.TrParametres;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrTelemacCommunParametres.java,v 1.46 2007-06-28 09:28:19 deniger Exp $
 */
public class TrTelemacCommunParametres extends FDicoParams implements TrParametres, FudaaSavable {

  class TemporaireInterForSeuilSip implements H2dTelemacSeuilSiphonContainer {

    @Override
    public int getNbSeuil() {
      return getTelemacParametres().getSeuil().getNbSeuil();
    }

    @Override
    public int getNbSiphon() {
      return getTelemacParametres().getSourceSiphonMng().getNbSiphon();
    }

    @Override
    public double getRelaxation() {
      return getTelemacParametres().getSourceSiphonMng().getSiphonRelaxation();
    }

    @Override
    public H2dTelemacSeuilInterface getSeuil(final int _i) {
      return getTelemacParametres().getSeuil().getSeuil(_i);
    }

    @Override
    public int getSeuilVitesseTangentielle() {
      return getTelemacParametres().getSeuil().getSeuilVitesseTangentielle();
    }

    @Override
    public H2dTelemacSiphonInterface getSiphon(final int _i) {
      return getTelemacParametres().getSourceSiphonMng().getSiphon(_i);
    }
  }

  /**
   * true, si une copie du fichier de g�ometrie a �t� effectu�e. true if a copy of the file of geometry has been made
   */
  private boolean isGeomFileSaved_;

  /**
   * true, si une copie du fichier de reprise a �t� effectu�e. true if a copy of the file of reprised has been made
   */
  private boolean isRepriseFileSaved_;

  TrCourbeTemporelleManager courbeMng_;

  String oldLiquPath_;

  H2dTelemacParameters params_;

  /*
   * private static class TrTelemacParamsSavedData { boolean isGeomFileSaved_; boolean isRepriseFileSaved_; public
   * TrTelemacParamsSavedData() {} public TrTelemacParamsSavedData(final boolean _isGeomFileSaved, final boolean
   * _isRepriseFileSaved) { super(); isGeomFileSaved_ = _isGeomFileSaved; isRepriseFileSaved_ = _isRepriseFileSaved; } }
   */
  TrTelemacCommunParametres(final TrImplementationEditorAbstract _ui, final H2dTelemacDicoParams _dicoParams, final File _f, final File _gridFile,
          final TrTelemacCommunProjectListener _listener) {
    super(_ui, _f, _dicoParams);
    params_ = new H2dTelemacParameters((H2dTelemacDicoParams) getDicoParams(), _listener);
    if (_gridFile != null) {
      setGridFile(_gridFile);
    }
  }

  // TrTelemacSerafinBuilder serafinBuilder_;
  TrTelemacSiActivator siActivator_;

  private synchronized boolean internloadGeometrie(final boolean _newProject, final boolean _useOlb, final int[] _ipobo,
          final ProgressionInterface _prg) {
    final DicoEntite entFile = getDicoFileFormatVersion().getMaillageEntiteFile();
    if (entFile == null) {
      if (!getDicoFileFormatVersion().isGridRequired()) {
        return false;
      }
      getUI().error(MvResource.getS("Fichier de maillage"), TrResource.getS("D�finition du fichier g�om�trie non trouv�e"), false);
      return false;
    }
    final TrTelemacSerafinHelper helper = new TrTelemacSerafinHelper(this);
    final TrTelemacSerafinBuilder builder = new TrTelemacSerafinBuilder(helper);
    boolean r = true;
    if (_newProject && _useOlb) {
      olbUsed_ = true;
      r = builder.loadGeometryAndOptimize(getDirBase(), _prg);
    } else {
      r = builder.loadGeometrie(getDirBase(), _ipobo, _prg, _newProject);
    }
    if (r && helper.isRepriseAvailable()) {
      siActivator_ = new TrTelemacSiActivator(this);
      getDicoParams().setPrevalid(siActivator_);
    }
    return r;
  }

  boolean olbUsed_;

  private boolean loadAllAction(final ProgressionInterface _prg) {
    return loadAllAction(false, false, _prg);

  }

  private void loadConditionsLimite(final H2dTelemacCLSourceInterface _src, final ProgressionInterface _prg) {
    if (isBoundaryConditionLoaded()) {
      return;
    }
    final DicoEntite entFile = getClKeyWord();
    if (_src == null) {
      getUI().message(TrResource.getS("Conditions limites"), TrResource.getS("Les conditions limites vont �tre initialis�es"), false);
      final CtuluAnalyze a = new CtuluAnalyze();
      getTelemacParametres().setNoCL(a);
      getUI().manageAnalyzeAndIsFatal(a);
      getDicoParams().setValue(entFile, this.computeNewEntieFileToSave(entFile));
      getState().setLoaded(entFile, null, true);
    } else {
      boolean modified = false;
      final CtuluAnalyze an = new CtuluAnalyze();
      params_.initCL(_src, _prg, an);
      if (getUI().manageAnalyzeAndIsFatal(an)) {
        getImpl().message(TrResource.getS("Les conditions limites vont �tre intialis�es par defaut"));
        params_.setNoCL(an);
        modified = true;
      }
      // ne devrait jamais arriver
      // should never occur
      if (params_.getCLManager() == null) {
        params_.setNoCL(an);
      }
      if (params_.getCLManager() != null) {
        getState().setLoaded(entFile, getFileSet(entFile), modified);
      }
    }
    // on recherche les erreurs sur les mots-cles de cl
    // we look for the errors of key words of cl
    final CtuluAnalyze a = params_.getTelemacCLManager().getErrorMessageForBcParameter();
    if (a.containsErrors()) {
      getUI().message(TrResource.getS("Les erreurs suivantes vont �tre corrig�es") + " :", a.getResume(), false);
      params_.getTelemacCLManager().correctErrorForBcParameter();
    }
  }

  private TelemacCLAdapter loadConditionsLimiteFile(final ProgressionInterface _prg) {
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("load cl");
    }
    final DicoEntite entFile = getDicoFileFormatVersion().getCLEntiteFile();
    if (entFile == null) {
      CtuluLibMessage.info(TrResource.getS("D�finition du fichier de conditions limites non trouv�e"));
      return null;
    }
    final TelemacCLVersion v = getDicoFileFormatVersion().getDefaultTelemacCLFormat(getDicoFileFormatVersion().getCodeName());
    if (v == null) {
      CtuluLibMessage.error("boundary conditions not supported");
      return null;
    }
    final File f = getFileSet(entFile);
    if (f != null && f.exists()) {
      final CtuluIOOperationSynthese op = v.read(f, _prg);
      TelemacCLAdapter inter = (TelemacCLAdapter) op.getSource();
      if (getUI().manageErrorOperationAndIsFatal(op)) {
        inter = null;
      }
      return inter;
    }
    return null;
  }

  private boolean saveClFile(final ProgressionInterface _prog, final File _f) {
    if (CtuluLibMessage.INFO) {
      CtuluLibMessage.info("save Cl file in " + _f);
    }
    final CtuluIOOperationSynthese op = getDicoFileFormatVersion().getDefaultTelemacCLFormat(getDicoFileFormatVersion().getCodeName()).write(_f,
            getTelemacParametres().getTelemacCLManager().createCLSourceInterface(), _prog);
    return !ui_.manageErrorOperationAndIsFatal(op);
  }

  private boolean saveGeometryFile(final ProgressionInterface _prog, final File _f) {
    // DicoEntite ent = getDicoFileFormatVersion().getMaillageEntiteFile();
    final TrTelemacSerafinSaver serafin = new TrTelemacSerafinSaver(getTelemacParametres());
    isGeomFileSaved_ = serafin.saveGeometryFile(_f, _prog, getImpl(), isGeomFileSaved_);
    return true;
  }

  private void setGridFile(final File _f) {
    final DicoEntite entFile = getDicoFileFormatVersion().getMaillageEntiteFile();
    if (entFile == null) {
      getUI().error(MvResource.getS("Fichier de maillage"), TrResource.getS("D�finition du fichier g�om�trie non trouv�e"), false);
    }
    final String name = CtuluLibFile.getRelativeFile(_f, getDirBase(), 5);
    getDicoParams().setValue(entFile, name);
  }

  boolean saveSIFile(final ProgressionInterface _prog, final File _f) {
    final TrTelemacSerafinSaver serafin = new TrTelemacSerafinSaver(getTelemacParametres());
    isRepriseFileSaved_ = serafin.saveSiFile(_f, _prog, getImpl(), isRepriseFileSaved_, getTelemacParametres().getRepriseInitialDateInMillis());
    return true;
  }

  @Override
  protected String computeNewEntieFileToSave(final DicoEntite _e) {
    if (_e == getTelemacParametres().getTelemacVersion().getRepriseFileKeyword()) {
      return TrTelemacSerafinHelper.getNewRepriseName();
    }
    final FileFormat t = getDicoFileFormatVersion().getFileFormat(_e);
    String ext = CtuluLibString.DOT + _e.getNom().substring(0, 3).toLowerCase();
    if ((t != null) && (t.getExtensions() != null) && (t.getExtensions().length > 0)) {
      ext = CtuluLibString.DOT + t.getExtensions()[0];
    }
    final String casName = getCasFileName();
    final int idx = casName.indexOf('.');
    if (idx > 0) {
      return casName.substring(0, idx - 1) + ext;
    }
    return casName + ext;
  }

  protected boolean loadAllAction(final boolean _newProject, final boolean _olb, final ProgressionInterface _prg) {
    boolean ok = true;
    TelemacCLAdapter s = null;
    if (!isGeometrieLoaded()) {
      // si le fichier des conditions limites est present, on recuper la numerotation
      // des bords
      // if the file of boundaries conditions exists, we get the numeration of the boundaries
      int[] ipobo = null;
      if (isClSupported()) {
        s = loadConditionsLimiteFile(_prg);
        if (s != null) {
          ipobo = new int[s.getNbLines()];
          for (int i = ipobo.length - 1; i >= 0; i--) {
            ipobo[i] = s.getGlobalIdx(i);
          }
        }
      }
      ok = internloadGeometrie(_newProject, _olb, ipobo, _prg);
    }
    getUI().unsetMainMessage();
    if (!ok) {
      return false;
    }
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("geo loaded !");
    }
    if (isClSupported() && !isBoundaryConditionLoaded()) {
      loadConditionsLimite(s, _prg);
      // si cela c'est bien passe on charge le fichier des conditions limites liquides
      // if OK, we load the liquid boundaries file
      if (isBoundaryConditionLoaded()) {
        loadConditionsLimiteLiquide(_prg);
      }
    }
    return ok;
  }

  @Override
  protected boolean saveDicoEntiteFileAction(final DicoEntite _entFile, final File _file, final ProgressionInterface _progress) {
    if (_entFile == getDicoFileFormatVersion().getCLEntiteFile()) {
      return saveClFile(_progress, _file);
    } else if (_entFile == getDicoFileFormatVersion().getMaillageEntiteFile()) {
      return saveGeometryFile(_progress, _file);
    } else if (_entFile == getDicoFileFormatVersion().getSeuilSiphonFile()) {
      return saveSeuilSiphon(_file, _progress);
    } else if (_entFile == getTelemacParametres().getTelemacDicoParams().getTelemacDicoFileFormatVersion().getRepriseFileKeyword()) {
      return saveSIFile(_progress, _file);
    }
    return super.saveDicoEntiteFileAction(_entFile, _file, _progress);
  }

  protected boolean saveSeuilSiphon(final File _f, final ProgressionInterface _prog) {
    new TelemacSeuilSiphonFileFormat().write(_f, new TemporaireInterForSeuilSip(), _prog);
    return true;

  }

  @Override
  public boolean canImportEvolution() {
    return params_.isTransientAvailable();
  }

  public DicoEntite getClKeyWord() {
    return getDicoFileFormatVersion().getCLEntiteFile();
  }

  /**
   * @return le manager des conditions limites / the manager of boundary conditions
   */
  public H2dTelemacBcManager getClManager() {
    return params_.getTelemacCLManager();
  }

  /**
   * @return la version telemac utilisee / the telemac version used
   */
  public TelemacDicoFileFormatVersion getDicoFileFormatVersion() {
    return (TelemacDicoFileFormatVersion) getFileFormatVersion();
  }

  public TrImplementationEditorAbstract getEditorImpl() {
    return (TrImplementationEditorAbstract) getImpl();
  }

  /**
   * @return le manager des evolutions / the evolutions manager
   */
  @Override
  public TrCourbeTemporelleManager getEvolMng() {
    if (courbeMng_ == null) {
      courbeMng_ = new TrCourbeTemporelleManager(getH2dParametres());
    }
    return courbeMng_;
  }

  /**
   * @return le fichier cas / the case list
   */
  public File getFileCas() {
    return getMainFile();
  }

  /**
   * @return le repertoire contenant le maillage. Si impossible, renvoie le repertoire du projet / the folder containing the mesh. If impossible, send
   * the project folder
   */
  public File getGridBaseDir() {
    final DicoEntite entFile = getDicoFileFormatVersion().getMaillageEntiteFile();
    if (entFile != null) {
      final String path = getAbsolutePathSet(entFile);
      if ((path != null) && (path.length() > 0)) {
        return new File(path).getParentFile();
      }
    }
    return getDirBase();
  }

  @Override
  public H2dParameters getH2dParametres() {
    return getTelemacParametres();
  }

  /**
   * Renvoie le maillage. Si nul, il est necessaire de charger le maillage Send the meshing. If nought, it is necessary to modify the meshing
   *
   * @return le maillage / the mesh
   */
  public EfGridInterface getMaillage() {
    if (params_ == null) {
      return null;
    }
    return params_.getMaillage();
  }

  /**
   * @return les parametres telemac / the telemac parameters
   */
  public H2dTelemacParameters getTelemacParametres() {
    return params_;
  }

  @Override
  protected boolean isAlreadyOpened(final File _f) {
    return getEditorImpl().getLauncher().isAlreadyUsed(_f);
  }

  /**
   * @return true si les conditions limites sont chargees / true if boundary conditions are modified
   */
  public boolean isAllLoaded() {
    return isBoundaryConditionLoaded();
  }

  /**
   * @return true si les conditions limites sont chargees / true if boundary conditions are modified
   */
  public boolean isBoundaryConditionLoaded() {
    return getState().isLoaded(getDicoFileFormatVersion().getCLEntiteFile());
  }

  /**
   * @return true si un fichier de condition limite est utilise par ce logiciel / true if a file of boundary condition is used by this software
   */
  public boolean isClSupported() {
    return getDicoFileFormatVersion().getCLEntiteFile() != null;
  }

  @Override
  public boolean isGeometrieLoaded() {
    return getState().isLoaded(getDicoFileFormatVersion().getMaillageEntiteFile())
            || getState().isLoaded(getDicoFileFormatVersion().getRepriseFileKeyword());
  }

  public boolean isRepriseActivatedByUser() {
    return siActivator_ != null && siActivator_.isRepriseActivatedByUser();
  }

  public DicoEntite getRepriseBooleanKw() {
    return siActivator_ == null ? null : siActivator_.getRepriseBooleanKw();
  }

  public DicoEntite getRepriseFileKeyword() {
    return siActivator_ == null ? null : siActivator_.getRepriseFileKeyword();
  }

  /**
   * @return true si modifiable
   */
  public boolean isNodalPropertiesModifiable() {
    return params_.isNodalPropertiesModifiable();
  }

  /**
   * @return true if the parameters corresponding to the entity are loaded.
   * @param _ent l'entite a tester / the entity to check
   */
  public boolean isParametersLoaded(final DicoEntite _ent) {
    return getState().isLoaded(_ent);
  }

  /**
   * @param _prg le receveur pour la progression / the received for the progression
   */
  @Override
  public final boolean loadData(final ProgressionInterface _prg) {
    if (!isAllLoaded()) {
      return loadAllAction(_prg);
    }
    return true;
  }

  @Override
  public boolean saveAs(final File _n, final ProgressionInterface _progress) {
    final boolean res = super.saveAs(_n, _progress);
    if (res) {
      isGeomFileSaved_ = true;
      isRepriseFileSaved_ = true;
    }
    return res;
  }

  @Override
  public boolean willBackupBeDone(final DicoEntite _kw) {
    if (!isGeomFileSaved_) {
      return _kw == getDicoFileFormatVersion().getMaillageEntiteFile();
    }
    if (!isRepriseFileSaved_) {
      return _kw == getDicoFileFormatVersion().getRepriseFileKeyword();
    }
    return false;
  }

  public boolean verifyLiquidBoundarFile() {
    if (getTelemacParametres().isTransientAvailable()) {
      final H2dTelemacBcManager mng = getClManager();
      final boolean isCurveUsed = mng.isEvolutionUsed();
      final DicoEntite e = getDicoFileFormatVersion().getCLLiquideEntiteFile();
      if (e == null) {
        return false;
      }
      final DicoParams p = getDicoParams();
      final boolean isKwSet = p.isValueSetFor(e);
      if (isCurveUsed != isKwSet) {
        CtuluLibMessage.info("CURVES USED STATE CHANGED");
        if (isCurveUsed) {
          String path = oldLiquPath_;
          e.setModifiable(true);
          if (path == null) {
            path = computeNewEntieFileToSave(e);
          }
          p.setValue(e, path);
          e.setRequired(true);
          getState().setLoadedModified(e, true);
        } else {
          // on enleve le mot-cle qui ne sert a rien
          // we delete the key word, which is useless
          oldLiquPath_ = getAbsoluteFileSet(e).getName();
          e.setRequired(false);
          getState().setUnloaded(e);
          p.removeValue(e);
          e.setModifiable(false);
        }
        return true;
      }
    }
    return false;
  }

  public boolean isOlbUsed() {
    return olbUsed_;
  }

  public TrTelemacSiActivator getSiActivator() {
    return siActivator_;
  }

  public void loadSavedData(final File _zipDbFile, final ProgressionInterface _pg) {
    /*
     * final Object o = TrLib.findClass(_zipDbFile, TrTelemacParamsSavedData.class); if (o != null) { final
     * TrTelemacParamsSavedData d = (TrTelemacParamsSavedData) o; isGeomFileSaved_ = d.isGeomFileSaved_;
     * isRepriseFileSaved_ = d.isRepriseFileSaved_; }
     */
  }

  @Override
  public void saveIn(final CtuluArkSaver _writer, final ProgressionInterface _prog) {
    try {
      saveIn(_writer.getDb(), _prog);
    } catch (final IOException _evt) {
      FuLog.error(_evt);

    }

  }

  @Override
  public void saveIn(final ObjectContainer _db, final ProgressionInterface _prog) {
    /*
     * if (_db == null) { return; } if (Fu.DEBUG && FuLog.isDebug()) { FuLog.debug("FTR: load saved data for " +
     * getClass().getName()); } _db.set(new TrTelemacParamsSavedData(isGeomFileSaved_, isRepriseFileSaved_));
     */

  }

  protected boolean loadConditionsLimiteLiquide(final ProgressionInterface _prg) {
    if (isLiquidTransientConditionPresent()) {
      return false;
    }
    final DicoEntite entFile = getTelemacFileFormatVersion().getCLLiquideEntiteFile();
    if ((entFile == null) || (!isValueSetFor(entFile))) {
      return false;
    }
    final File f = getFileSet(entFile);
    final CtuluIOOperationSynthese op = getTelemacFileFormatVersion().getDefaultTelemacLiquideFormat().read(f, _prg);
    final TelemacLiquideInterface inter = (TelemacLiquideInterface) op.getSource();
    getUI().manageErrorOperationAndIsFatal(op);
    if (inter == null) {
      return false;
    }
    final H2dTelemacEvolutionFrontiere[] evolutions = inter.getEvolutionsFrontieresLiquides();
    int nbLiquideBoundaries = getTelemacParametres().getCLManager().getNbLiquideBoundaries();
    H2dTelemacSourceMng sourceSiphonMng = getTelemacParametres().getSourceSiphonMng();
    int nbTracer = sourceSiphonMng.getNbTraceurs();
    if (nbTracer > 1) {
      for (H2dTelemacEvolutionFrontiere evolution : evolutions) {
        if (H2dVariableTransType.TRACEUR.equals(evolution.getTelemacEvolution().getVariableType())) {
          if (!evolution.isIdxVariableDefined()) {
            int indexFrontiere = evolution.getTelemacEvolution().getIndexFrontiere();
            int idxTracer = indexFrontiere % nbTracer;
            int idxFr = (indexFrontiere - idxTracer) / nbTracer;
            evolution.setIdxVariable(idxTracer);
            evolution.getTelemacEvolution().setIndexFrontiere(idxFr);
          }
        }

      }

    }
    getTelemacParametres().initEvolutions(evolutions);
    final boolean modified = TrCourbeTemporelleManager.isCurveNotEnoughLarge(0, getTelemacParametres().getComputationDuration(), evolutions);
    getState().setLoaded(entFile, f, modified);
    if (modified) {
      getImpl().message(entFile.getNom(),
              TrResource.getS("Certaines courbes temporelles ne sont pas assez �tendues dans le temps" + "( cela sera automatiquement corrig�)"),
              false);
    }
    // on verifie l'utilite de ce fichier
    // we check the useful of this list 
    if (!verifyLiquidBoundarFile()) {
      if (getTelemacParametres().getTelemacCLManager().isEvolutionUsed()) {
        entFile.setRequired(true);
      } else {
        // dicoParams_.removeValue(entFile);
        entFile.setModifiable(false);
      }
    }
    return true;
  }

  public TelemacDicoFileFormatVersion getTelemacFileFormatVersion() {
    return (TelemacDicoFileFormatVersion) getFileFormatVersion();
  }

  public boolean isLiquidTransientConditionPresent() {
    final DicoEntite entFile = getTelemacFileFormatVersion().getCLLiquideEntiteFile();
    if ((entFile == null) || (!isValueSetFor(entFile))) {
      return false;
    }
    return getTelemacParametres().isTransientAvailable() && getTelemacParametres().getTelemacCLManager().isEvolutionUsed();
  }

}
