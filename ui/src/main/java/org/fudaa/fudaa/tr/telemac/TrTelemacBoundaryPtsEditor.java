/**
 * @creation 3 nov. 2003
 * @modification $Date: 2006-09-19 15:07:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import gnu.trove.TObjectDoubleHashMap;
import java.awt.Color;
import java.util.Arrays;
import java.util.Map;
import javax.swing.JLabel;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcManager;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBordParFrontiere;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBoundary;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrTelemacBoundaryPtsEditor.java,v 1.21 2006-09-19 15:07:29 deniger Exp $
 */
public class TrTelemacBoundaryPtsEditor extends CtuluDialogPanel {

  CtuluCommandManager cmdMng_;
  BuTextField[] tfValues_;
  String[] initValues_;
  int[] idx_;
  H2dTelemacBoundary bord_;
  H2dVariableType[] var_;
  H2dTelemacBordParFrontiere frontier_;
  H2dTelemacBcManager mng_;

  public TrTelemacBoundaryPtsEditor(final CtuluCommandManager _cmdMng, final H2dTelemacBordParFrontiere _frIdx,
      final H2dTelemacBoundary _b, final int[] _idx, final H2dTelemacBcManager _mng) {
    mng_ = _mng;
    cmdMng_ = _cmdMng;
    frontier_ = _frIdx;
    this.idx_ = _idx;
    this.bord_ = _b;
    setLayout(new BuGridLayout(2, 10, 10));
    addEmptyBorder(10);
    if (bord_.getName() != null) {
      addLabel(TrResource.getS("Bord"));
      addStringText(bord_.getName()).setEditable(false);
    }
    addLabel(TrResource.getS("Type du bord"));
    final BuTextField f = addStringText(bord_.getType().getName());
    f.setEditable(false);
    f.setColumns(bord_.getType().getName().length() + 1);
    var_ = mng_.getVariablesForPoint(bord_);
    if (var_ == null) { return; }
    Arrays.sort(var_);
    final int n = var_.length;
    initValues_ = new String[n];
    tfValues_ = new BuTextField[n];
    H2dVariableType v;
    final Map m = mng_.getVariablesForPointInfo(bord_);
    for (int i = 0; i < n; i++) {
      v = var_[i];
      final boolean fixed = frontier_.isVariableFixedOnBorder(_b, v);
      String lText = null;
      // si des infos sont donnees, on les ajoutes
	  // if info are given, we add them
      if (m != null && m.containsKey(v)) {
        lText = v.getName() + " (" + m.get(v) + ")";
      } else {
        lText = v.getName();
      }
      final JLabel l = addLabel(lText);
      tfValues_[i] = addDoubleText();
      if (frontier_.isValueConstantOnPoint(v, idx_)) {
        initValues_[i] = Double.toString(frontier_.getCl(idx_[0]).getValue(v));
        tfValues_[i].setValue(initValues_[i]);
      }
      if (fixed) {
        l.setForeground(Color.red);
        l.setToolTipText(TrResource.getS("Valeur fix�e sur le bord par mot-cl� ou par une courbe transitoire"));
      }
    }
  }

  /**
   * Test la variable hauteur d'eau.
   * Test the variable water heigth
   */
  @Override
  public boolean isDataValid() {
    // si la variable hauteur a ete utilise, l'indice sera positif
    // if the heigth variable has been used the indicator will be positif
    int idxH = -1;
    for (int i = var_.length - 1; i >= 0; i--) {
      if (var_[i] == H2dVariableType.HAUTEUR_EAU) {
        idxH = i;
        break;
      }
    }
    // variable h defini
	// variable h defined
    if (idxH >= 0) {
      final String str = tfValues_[idxH].getText();
      // valeur de h inchangee; c'est ok
      // value of h unchanged it is ok
      if (CtuluLibString.isEmpty(str)) { return true; }

      final double s = Double.parseDouble(str);
      // h est negatif -> erreur
      // h is negative _> error
      if (s < 0) {
        setErrorText(H2dResource.getS("La valeur de la variable '{0}' doit �tre positive", H2dVariableType.HAUTEUR_EAU
            .getName()));
        return false;
      }
    }
    return super.isDataValid();
  }

  /**
   * Recupere les valeurs.
   * Get the values
   */
  @Override
  public boolean apply() {
    if (var_ == null) { return true; }
    final int n = var_.length;
    final TObjectDoubleHashMap m = new TObjectDoubleHashMap(n);
    for (int i = n - 1; i >= 0; i--) {
      String newVal = tfValues_[i].getText();
      if (newVal != null) {
        newVal = newVal.trim();
        if ((newVal.length() > 0) && (!newVal.equals(initValues_[i]))) {
          m.put(var_[i], Double.parseDouble(newVal));
        }
      }
    }
    if (m.size() > 0) {
      final CtuluCommand c = frontier_.setValuesForBoundaryPoints(idx_, m);
      if (c != null) {
        cmdMng_.addCmd(c);
      }
    }
    return true;
  }
}
