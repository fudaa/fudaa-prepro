/**
 * @creation 9 nov. 2004
 * @modification $Date: 2007-05-04 14:01:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.fu.FuLog;
import java.io.File;
import org.fudaa.dodico.h2d.telemac.H2dTelemacDicoParams;
import org.fudaa.dodico.telemac.TelemacStbtelFileFormatVersion;
import org.fudaa.fudaa.tr.common.TrImplementationEditorAbstract;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacStbtelParameters.java,v 1.6 2007-05-04 14:01:50 deniger Exp $
 */
public class TrTelemacStbtelParameters extends TrTelemacCommunParametres {

  /**
   * @param _ui
   * @param _dicoParams
   * @param _f
   * @param _gridFile
   * @param _listener
   */
  public TrTelemacStbtelParameters(final TrImplementationEditorAbstract _ui, final H2dTelemacDicoParams _dicoParams,
      final File _f, final File _gridFile, final TrTelemacCommunProjectListener _listener) {
    super(_ui, _dicoParams, _f, _gridFile, _listener);
    if (!(_dicoParams.getDicoFileFormatVersion() instanceof TelemacStbtelFileFormatVersion)) {
      FuLog.warning(new Throwable());
    }
  }

  public boolean isBottomFrictionModifiable() {
    return false;
  }

  @Override
  public boolean isBoundaryConditionLoaded() {
    return false;
  }

  @Override
  public boolean isAllLoaded() {
    return isGeometrieLoaded();
  }

}