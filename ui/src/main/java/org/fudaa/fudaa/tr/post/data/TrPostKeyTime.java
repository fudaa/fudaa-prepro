package org.fudaa.fudaa.tr.post.data;

import java.util.Objects;

public class TrPostKeyTime {
  public final TrPostKey postKey;
  public final int time;

  public TrPostKeyTime(TrPostKey postKey, int time) {
    this.postKey = postKey;
    this.time = time;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TrPostKeyTime that = (TrPostKeyTime) o;
    return time == that.time &&
        Objects.equals(postKey, that.postKey);
  }

  @Override
  public int hashCode() {
    return Objects.hash(postKey, time);
  }
}
