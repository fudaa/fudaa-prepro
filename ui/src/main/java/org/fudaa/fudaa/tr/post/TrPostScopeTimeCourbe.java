package org.fudaa.fudaa.tr.post;

import org.fudaa.ebli.courbe.EGCourbePersistBuilder;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTime;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;

public class TrPostScopeTimeCourbe extends FudaaCourbeTime {

  public TrPostScopeTimeCourbe(EGGroup _m, EGModel _model, FudaaCourbeTimeListModel _timeModel) {
    super(_m, _model, _timeModel);
  }

  @Override
  protected EGCourbePersistBuilder<FudaaCourbeTime> createPersistBuilder() {
    return new TrPostScopeTimeCourbePersistBuilder();
  }

}
