/*
 *  @creation     3 mai 2005
 *  @modification $Date: 2007-04-16 16:35:31 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import java.util.Set;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.fudaa.tr.post.TrPostSource;

public final class TrPostDataCreatedPtY extends TrPostDataCreatedDefault {

  public TrPostDataCreatedPtY(final TrPostSource _src) {
    super(_src);
  }

  @Override
  public double buildDataFor(final int _idxTime, final int _idxObject) {
    final EfGridInterface grid = src_.getGrid();
    return grid.getPtY(_idxObject);
  }

  @Override
  public void fillWhithAllUsedVar(final Set _res) {
  }

  @Override
  public String getDescription() {
    return "Y";
  }

  @Override
  public EfData buildDataFor(final int _idxTime) {
    final EfGridInterface grid = src_.getGrid();
    final double[] bathy = new double[grid.getPtsNb()];
    for (int i = bathy.length - 1; i >= 0; i--) {
      bathy[i] = grid.getPtY(i);
    }
    return new EfDataNode(bathy);
  }
}
