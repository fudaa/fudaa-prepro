package org.fudaa.fudaa.tr.post;

import java.util.Set;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentVariableType;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreatedDefault;

public class TrPostRubarSedimentVariable extends TrPostDataCreatedDefault {

  private final H2dRubarSedimentVariableType variable;
  private final H2dRubarSedimentInterface sedimentData;

  public TrPostRubarSedimentVariable(TrPostSourceRubar _src, H2dRubarSedimentVariableType variable, H2dRubarSedimentInterface sedimentData) {
    super(_src);
    this.variable = variable;
    this.sedimentData = sedimentData;
  }

  @Override
  public String getDescription() {
    return variable.getName();
  }

  @Override
  public void fillWhithAllUsedVar(Set _res) {
    _res.add(variable);
  }

  @Override
  public EfData buildDataFor(int _idxTime) {
    double[] values = new double[sedimentData.getNbNoeuds()];
    for (int i = 0; i < values.length; i++) {
      values[i] = sedimentData.getCoucheContainer(_idxTime, i).getCoucheItem(variable.getCoucheIdx()).getValue(variable.getParentVariable());
    }
    return new EfDataNode(values);
  }

  @Override
  public double buildDataFor(int _idxTime, int _idxObject) {
    return sedimentData.getCoucheContainer(_idxTime, _idxObject).getCoucheItem(variable.getCoucheIdx()).getValue(variable.getParentVariable());
  }

}
