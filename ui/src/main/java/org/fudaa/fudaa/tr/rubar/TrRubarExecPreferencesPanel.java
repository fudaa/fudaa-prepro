/**
 * @creation 28 mai 2004
 * @modification $Date: 2006-09-08 16:53:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import org.fudaa.dodico.calcul.CalculExecBatch;
import org.fudaa.dodico.rubar.RubarRubarExec;
import org.fudaa.dodico.rubar.RubarRubarSedimentExec;
import org.fudaa.dodico.rubar.RubarVF2MExec;
import org.fudaa.fudaa.tr.TrChainePreferencePanel;
import org.fudaa.fudaa.tr.common.TrExecBatchPreferencesPanel;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarExecPreferencesPanel.java,v 1.9 2006-09-08 16:53:09 deniger Exp $
 */
public class TrRubarExecPreferencesPanel extends TrExecBatchPreferencesPanel {
  /**
   * Le panneau des exe pour rubar.
   */
  public TrRubarExecPreferencesPanel() {
    super(new CalculExecBatch[]{new RubarVF2MExec(), new RubarRubarExec(),
        new RubarRubarSedimentExec()});
  }

  @Override
  public String getCategory() {
    return TrChainePreferencePanel.getModelisationCategory();
  }

  @Override
  public String getTitle() {
    return "Rubar";
  }
}
