/*
 * @creation 30 nov. 06
 * @modification $Date: 2006-12-05 10:18:18 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

/**
 * @author fred deniger
 * @version $Id: MvProfileCourbeInterface.java,v 1.1 2006-12-05 10:18:18 deniger Exp $
 */
public interface MvProfileCourbeInterface {

  MvProfileCourbeModelInterface getProfileModel();

}
