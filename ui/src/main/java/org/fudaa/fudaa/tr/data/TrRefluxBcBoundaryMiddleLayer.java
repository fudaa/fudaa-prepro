/*
 *  @creation     21 nov. 2003
 *  @modification $Date: 2008-02-20 10:11:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuTable;
import gnu.trove.TIntHashSet;
import gnu.trove.TIntObjectIterator;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.h2d.H2dBcFrontierMiddleInterface;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBcManager;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBoundaryCondition;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.calque.ZModeleDonnees;
import org.fudaa.ebli.calque.ZSelectionTrace;
import org.fudaa.ebli.commun.EbliListeSelectionMulti;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;

import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author deniger
 * @version $Id: TrRefluxBcBoundaryMiddleLayer.java,v 1.26.6.1 2008-02-20 10:11:53 bmarchan Exp $
 */
public class TrRefluxBcBoundaryMiddleLayer extends TrBcBoundaryLayerAbstract {
  protected TrBcBoundaryMiddleModelDefault m_;
  protected TraceLigne tl_;
  final GrSegment seg_ = new GrSegment(new GrPoint(), new GrPoint());

  /**
   * @param _m le modele
   * @param _l la legende associee.
   */
  public TrRefluxBcBoundaryMiddleLayer(final TrBcBoundaryMiddleModelDefault _m, final BCalqueLegende _l) {
    super(_l);
    m_ = _m;
    initLegende();
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return null;
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return null;
  }

  @Override
  public List getAllBoundaryType() {
    return m_.getBordList();
  }

  @Override
  public int getNbBoundaryType() {
    return m_.getNbBoundaryType();
  }

  @Override
  public int[] getSelectedElementIdx() {
    return null;
  }

  @Override
  public BuTable createValuesTable() {
    return new CtuluTable(new FrTableModel());
  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  @Override
  public int[] getSelectedObjectInTable() {
    if (!isSelectionEmpty()) {
      final TIntObjectIterator itIntObj = selection_.getIterator();
      final TIntHashSet r = new TIntHashSet(m_.getNbTotalPt());
      int idxFr, max, temp;
      for (int i = selection_.getNbListSelected() - 1; i >= 0; i--) {
        itIntObj.advance();
        idxFr = itIntObj.key();
        final CtuluListSelection s = (CtuluListSelection) itIntObj.value();
        max = s.getMaxIndex();
        for (int j = s.getMinIndex(); j <= max; j++) {
          if (s.isSelected(j)) {
            temp = EfLib.getIdxFromIdxMid(j);
            temp = m_.getFrontiereIndice(idxFr, temp);
            r.add(m_.getFrontiereIndice(idxFr, EfLib.getIdxMidFromIdx(temp)));
          }
        }
      }
      return r.toArray();
    }
    return null;
  }

  @Override
  public LineString getSelectedLine() {
    if (isSelectionEmpty()) {
      return null;
    }
    // mode vertex
    final EbliListeSelectionMultiInterface select = getLayerSelectionMulti();
    // mode vertex
    // la selection se situe dans la meme ligne
    if (select.getNbListSelected() == 1) {
      final int i = select.getIdxSelected()[0];
      final CtuluListSelectionInterface selection = select.getSelection(i);
      final int nbSelected = selection.getNbSelectedIndex();
      // deux sommets sélectionnés: on choisit la ligne en les 2 sommets
      if (nbSelected >= 2) {
        final int[] res = isSelectionContiguous(i);
        if (res != null) {
          final List coordinate = new ArrayList();
          final int nb = m_.getFrontier(i).getNbPt() - 1;
          // selection normale
          if (res[0] < res[1]) {
            addCoordinate(i, coordinate, nb, res[0], res[1]);
          } else {
            addCoordinate(i, coordinate, nb, res[0], nb);
            addCoordinate(i, coordinate, nb, 0, res[1]);
          }
          return GISGeometryFactory.INSTANCE.createLineString((Coordinate[]) coordinate
              .toArray(new Coordinate[coordinate.size()]));
        }
      }
      if (nbSelected == 2) {
        int temp = EfLib.getIdxFromIdxMid(selection.getMinIndex());

        final Coordinate[] cs = new Coordinate[2];
        m_.getPoint(seg_.o_, i, temp);
        cs[0] = new Coordinate(seg_.o_.x_, seg_.o_.y_);
        temp = EfLib.getIdxFromIdxMid(selection.getMaxIndex());
        m_.getPoint(seg_.o_, i, temp);
        cs[1] = new Coordinate(seg_.o_.x_, seg_.o_.y_);
        return GISGeometryFactory.INSTANCE.createLineString(cs);
      }
    } else if (select.getNbListSelected() == 2) {
      final int[] idx = select.getIdxSelected();
      final CtuluListSelectionInterface selection1 = select.getSelection(idx[0]);
      final CtuluListSelectionInterface selection2 = select.getSelection(idx[1]);
      if (selection1.getNbSelectedIndex() == 1 && selection2.getNbSelectedIndex() == 1) {
        final Coordinate[] cs = new Coordinate[2];
        int temp = EfLib.getIdxFromIdxMid(selection1.getMinIndex());
        m_.getPoint(seg_.o_, idx[0], temp);
        cs[0] = new Coordinate(seg_.o_.x_, seg_.o_.y_);
        temp = EfLib.getIdxFromIdxMid(selection2.getMinIndex());
        m_.getPoint(seg_.o_, idx[1], temp);
        cs[1] = new Coordinate(seg_.o_.x_, seg_.o_.y_);
        return GISGeometryFactory.INSTANCE.createLineString(cs);
      }
    }
    return null;
  }

  private void addCoordinate(final int _frIdx, final List _coordinate, final int _nb, final int _idx1, final int _idx2) {
    for (int j = _idx1; j <= _idx2; j++) {
      final int temp = EfLib.getIdxFromIdxMid(j);
      m_.getPoint(seg_.o_, _frIdx, temp - 1);
      _coordinate.add(new Coordinate(seg_.o_.x_, seg_.o_.y_));
      m_.getPoint(seg_.o_, _frIdx, temp);
      _coordinate.add(new Coordinate(seg_.o_.x_, seg_.o_.y_));
      m_.getPoint(seg_.o_, _frIdx, temp == _nb ? 0 : temp + 1);
      _coordinate.add(new Coordinate(seg_.o_.x_, seg_.o_.y_));
    }
  }

  @Override
  public int[] getSelectedPtIdx() {
    if (!isSelectionEmpty()) {
      final TIntObjectIterator itIntObj = selection_.getIterator();
      final TIntHashSet r = new TIntHashSet(m_.getNbTotalPt());
      int idxFr, max, n, temp;
      for (int i = selection_.getNbListSelected() - 1; i >= 0; i--) {
        itIntObj.advance();
        idxFr = itIntObj.key();
        final CtuluListSelection s = (CtuluListSelection) itIntObj.value();
        max = s.getMaxIndex();
        n = m_.getFrontier(idxFr).getNbPt() - 1;
        for (int j = s.getMinIndex(); j <= max; j++) {
          if (s.isSelected(j)) {
            temp = EfLib.getIdxFromIdxMid(j);
            r.add(m_.getGlobalIdx(idxFr, temp));
            r.add(m_.getGlobalIdx(idxFr, temp - 1));
            r.add(m_.getGlobalIdx(idxFr, temp == n ? 0 : temp + 1));
          }
        }
      }
      return r.toArray();
    }
    return null;
  }

  @Override
  public List getUsedBoundaryType() {
    return m_.getUsedBoundaryType();
  }

  @Override
  public GrBoite getDomaineOnSelected() {
    return null;
  }

  @Override
  public boolean isSelectionElementEmpty() {
    return true;
  }

  @Override
  public boolean isSelectionPointEmpty() {
    return isSelectionEmpty();
  }

  @Override
  public ZModeleDonnees modeleDonnees() {
    return m_;
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
                           final GrBoite _clipReel) {
    if (tl_ == null) {
      tl_ = new TraceLigne();
      tl_.setEpaisseur(1f);
      tl_.setCouleur(Color.red);
    }
    final int n = m_.getNbFrontier();
    final GrPoint grpTemp = new GrPoint();
    // une boite tempo contenenant les points fin et init
    final GrBoite b = new GrBoite();
    H2dBoundaryType bType;
    boolean extrem;
    // on parcourt les frontiere du maillage
    for (int i = n - 1; i >= 0; i--) {
      final H2dBcFrontierMiddleInterface bd = m_.getFrontier(i);
      final int nbPt = bd.getNbPt();
      extrem = true;
      // on parcourt les different type de bord pour la frontiere i
      for (int j = nbPt - 1; j >= 0; j -= 2) {
        bType = bd.getBcType(j);
        m_.getPoint(seg_.o_, i, j - 1);
        m_.getPoint(seg_.e_, i, j);
        grpTemp.initialiseAvec(seg_.e_);
        seg_.boite(b);
        tl_.setModel(getTlData(bType));
        if (_clipReel.intersectXY(b)) {
          seg_.autoApplique(_versEcran);
          tl_.dessineTrait(_g, seg_.o_.x_, seg_.o_.y_, seg_.e_.x_, seg_.e_.y_);
        }
        seg_.o_.initialiseAvec(grpTemp);
        m_.getPoint(seg_.e_, i, extrem ? 0 : j + 1);
        seg_.boite(b);
        if (_clipReel.intersectXY(b)) {
          seg_.autoApplique(_versEcran);
          tl_.dessineTrait(_g, seg_.o_.x_, seg_.o_.y_, seg_.e_.x_, seg_.e_.y_);
        }
        extrem = false;
      }
    }
  }

  @Override
  public void doPaintSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
                               final GrBoite _clipReel) {
    if (isSelectionEmpty()) {
      return;
    }
    final GrBoite b = new GrBoite();
    int n;
    Color cs = _trace.getColor();
    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    final TraceLigne tlSelection = _trace.getLigne();
    final TraceIcon ic = _trace.getIcone();
    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    _g.setColor(cs);
    int max, temp, idxFr;
    final TIntObjectIterator itIntObj = selection_.getIterator();
    for (int i = selection_.getNbListSelected() - 1; i >= 0; i--) {
      itIntObj.advance();
      idxFr = itIntObj.key();
      final CtuluListSelection s = (CtuluListSelection) itIntObj.value();
      max = s.getMaxIndex();
      n = m_.getFrontier(idxFr).getNbPt() - 1;
      for (int j = s.getMinIndex(); j <= max; j++) {
        if (s.isSelected(j)) {
          temp = EfLib.getIdxFromIdxMid(j);
          m_.getPoint(seg_.o_, idxFr, temp - 1);
          m_.getPoint(seg_.e_, idxFr, temp == n ? 0 : temp + 1);
          seg_.boite(b);
          if (_clipReel.intersectXY(b)) {
            seg_.autoApplique(_versEcran);
            ic.paintIconCentre(this, _g, seg_.o_.x_, seg_.o_.y_);
            ic.paintIconCentre(this, _g, seg_.e_.x_, seg_.e_.y_);
            tlSelection.dessineTrait(_g, seg_.o_.x_, seg_.o_.y_, seg_.e_.x_, seg_.e_.y_);
          }
        }
      }
    }
  }

  class FrTableModel extends AbstractTableModel {
    H2dVariableType[] vars_ = H2dRefluxBcManager.getClBordVariables();

    @Override
    public int getColumnCount() {
      return vars_.length + 7;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return H2dResource.getS("Frontière");
      }
      if (_column == 1) {
        return MvResource.getS("Indice sur la frontière");
      }
      if (_column == 2) {
        return MvResource.getS("Indice global");
      }
      if (_column == 3) {
        return H2dResource.getS("Type");
      }
      if (_column == 4) {
        return "X";
      }
      if (_column == 5) {
        return "Y";
      }
      if (_column == 6) {
        return H2dResource.getS("Normale");
      }
      return vars_[getVarIdx(_column)].getName();
    }

    @Override
    public Class getColumnClass(final int _columnIndex) {
      if (_columnIndex < 3) {
        return Integer.class;
      }
      if (_columnIndex > 3 && _columnIndex < 7) {
        return Double.class;
      }
      return Object.class;
    }

    private int getVarIdx(final int _col) {
      return _col - 7;
    }

    @Override
    public int getRowCount() {
      return m_.getNbTotalPt() / 2;
    }

    int[] tmp_ = new int[2];

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      final int frIdx = EfLib.getIdxFromIdxMid(_rowIndex);
      m_.getGrid().getFrontiers().getIdxFrontierIdxPtInFrontierFromFrontier(frIdx, tmp_);
      if (_columnIndex == 0) {
        return new Integer(tmp_[0] + 1);
      }
      if (_columnIndex == 1) {
        return new Integer(tmp_[1] + 1);
      }
      final int idxOnFr = tmp_[1];
      final int glIdx = m_.getGlobalIdx(tmp_[0], idxOnFr);
      if (_columnIndex == 2) {
        return new Integer(glIdx + 1);
      }
      if (_columnIndex == 4) {
        return CtuluLib.getDouble(m_.getGrid().getPtX(glIdx));
      }
      if (_columnIndex == 5) {
        return CtuluLib.getDouble(m_.getGrid().getPtY(glIdx));
      }

      final H2dRefluxBcManager mng = (H2dRefluxBcManager) m_.getMng();
      final H2dRefluxBcManager.RefluxMiddleFrontier fr = mng.getRefluxMiddleFrontier(tmp_[0]);
      if (_columnIndex == 3) {
        return fr.getBcType(idxOnFr).toString();
      }
      final H2dRefluxBoundaryCondition bc = fr.getRefluxBc(tmp_[1]);
      if (_columnIndex == 6) {
        return CtuluLib.getDouble(bc.getNormale());
      }
      final int varIdx = getVarIdx(_columnIndex);
      final Set s = fr.getAvailablesVariables(idxOnFr);
      Object r = CtuluLibString.EMPTY_STRING;
      if (s.contains(vars_[varIdx])) {
        final H2dVariableType v = vars_[varIdx];
        final H2dBcType bcType = bc.getType(v);
        if (bcType == H2dBcType.LIBRE) {
          r = H2dBcType.LIBRE.getName();
        } else if (bcType == H2dBcType.PERMANENT) {
          r = CtuluLib.getDouble(bc.getValue(v));
        } else {
          r = bcType.getName() + ": " + bc.getEvolution(v);
        }
      }
      return r;
    }
  }

  @Override
  public EbliListeSelectionMulti selection(final GrPoint _pt, final int _tolerance) {
    GrBoite bClip = getDomaine();
    final double distanceReel = GrMorphisme.convertDistanceXY(getVersReel(), _tolerance);
    if ((!bClip.contientXY(_pt)) && (bClip.distanceXY(_pt) > distanceReel)) {
      return null;
    }
    bClip = getClipReel(getGraphics());
    seg_.e_ = new GrPoint();
    seg_.o_ = new GrPoint();
    final GrBoite boite = new GrBoite();
    for (int i = m_.getNbFrontier() - 1; i >= 0; i--) {
      // middle points ...
      m_.getPoint(seg_.e_, i, 0);
      for (int j = m_.getFrontier(i).getNbPt() - 1; j >= 0; j -= 2) {
        // on initialise le point du milieu.
        m_.getPoint(seg_.o_, i, j);
        seg_.boite(boite);
        if (boite.intersectXY(bClip) && seg_.distanceXY(_pt) < distanceReel) {
          final EbliListeSelectionMulti r = new EbliListeSelectionMulti(1);
          r.set(i, EfLib.getIdxMidFromIdx(j));
          return r;
        }
        m_.getPoint(seg_.e_, i, j - 1);
        seg_.boite(boite);
        if (boite.intersectXY(bClip) && seg_.distanceXY(_pt) < distanceReel) {
          final EbliListeSelectionMulti r = new EbliListeSelectionMulti(1);
          r.set(i, EfLib.getIdxMidFromIdx(j));
          return r;
        }
      }
    }
    return null;
  }

  /**
   *
   */
  @Override
  public EbliListeSelectionMulti selection(final LinearRing _poly) {
    if (m_.getNombre() == 0 || !isVisible()) {
      return null;
    }
    final Envelope polyEnv = _poly.getEnvelopeInternal();
    final GrBoite domaineBoite = getDomaine();
    final Envelope domaine = new Envelope(domaineBoite.e_.x_, domaineBoite.o_.x_, domaineBoite.e_.y_,
        domaineBoite.o_.y_);
    // si l'envelop du polygone n'intersect pas le domaine, il n'y a pas de
    // selection
    if (!polyEnv.intersects(domaine)) {
      return null;
    }
    final IndexedPointInAreaLocator tester = new IndexedPointInAreaLocator(_poly);
    final GrPoint pTemp = new GrPoint();
    final GrPoint pTemp2 = new GrPoint();
    final Coordinate c = new Coordinate();
    final EbliListeSelectionMulti r = new EbliListeSelectionMulti(m_.getNbFrontier());
    CtuluListSelection lBord = null;
    for (int i = m_.getNbFrontier() - 1; i >= 0; i--) {
      lBord = null;
      // middle points ...
      m_.getPoint(pTemp, i, 0);
      final int nbPt = m_.getFrontier(i).getNbPt();
      for (int j = nbPt - 1; j >= 0; j -= 2) {
        m_.getPoint(pTemp2, i, j - 1);
        // test j+1
        if (polyEnv.contains(pTemp.x_, pTemp.y_) && polyEnv.contains(pTemp2.x_, pTemp2.y_)) {
          c.x = pTemp.x_;
          c.y = pTemp.y_;
          if (GISLib.isInside(tester,c)) {
            c.x = pTemp2.x_;
            c.y = pTemp2.y_;
            if (GISLib.isInside(tester,c)) {
              m_.getPoint(pTemp, i, j);
              c.x = pTemp.x_;
              c.y = pTemp.y_;
              // test j
              if ((polyEnv.contains(c.x, c.y)) && (GISLib.isInside(tester,c))) {
                m_.getPoint(pTemp, i, j - 1);
                if (lBord == null) {
                  lBord = new CtuluListSelection(nbPt >> 1);
                }
                lBord.add(EfLib.getIdxMidFromIdx(j));
              }
            }
          }
        }
        pTemp.initialiseAvec(pTemp2);
      }
      if (lBord != null) {
        r.set(i, lBord);
      }
    }
    if (r.isEmpty()) {
      return null;
    }
    return r;
  }
}
