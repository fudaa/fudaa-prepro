/*
 * @creation 27 nov. 06
 * 
 * @modification $Date: 2007-06-13 12:58:08 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import java.util.Map;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsBuilder;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author fred deniger
 * @version $Id: MvProfileCoordinatesModel.java,v 1.8 2007-06-13 12:58:08 deniger Exp $
 */
public class MvProfileCoordinatesModel extends MVProfileCourbeModelDefault {

  final boolean isX_;
  boolean isDefinedOnMeshes;
  LineString initLine;
  EfGridData data_;

  int[] initLineRows = null;
  
  
  public MvProfileCoordinatesModel(final EfGridData data, final LineString initLine,
          final EfLineIntersectionsResultsI _res, final boolean _isX) {
    super();
    this.data_ = data;
    isDefinedOnMeshes = _res.isForMesh();
    this.initLine = initLine;
    xyProvider = MvProfileXYProvider.createCoordinateProvider(_res, _isX);
    this.isX_ = _isX;
    updateTitle();
    
    computeInitRows();
  }

  
	private void computeInitRows() {
		initLineRows = new int[initLine.getNumPoints()];
		int cpt = 0;
		double distX = 0;
		for (int x = 0; x < initLine.getNumPoints(); x++) {
			if (x > 0) {
				Coordinate precedent = initLine.getCoordinateN(x - 1);
				Coordinate current = initLine.getCoordinateN(x);
				distX += CtuluLibGeometrie.getDistance(precedent.x,
						precedent.y, current.x, current.y);
			}

			for (int i = 0; i < xyProvider.getNbData(); i++) {			
				if (CtuluLib.isEquals(distX, xyProvider.getX(i), 1E-3)) {
					initLineRows[cpt++] = i;
				}
			}
		}
	}
  
  
 
public MvProfileCoordinatesModel(final EfGridData data, final LineString initLine,
          final MvProfileXYProvider.Cached provider, final boolean _isX, boolean isDefinedOnMeshes) {
    super();
    this.data_ = data;
    this.isDefinedOnMeshes = isDefinedOnMeshes;
    this.initLine = initLine;
    this.xyProvider = provider;
    this.isX_ = _isX;
    updateTitle();
    computeInitRows();
  }

  @Override
  public LineString getInitLine() {
    return initLine;
  }

  @Override
  public EfGridData getData() {
    return data_;
  }

  private void updateTitle() {
    title_ = isX_ ? "X" : "Y";
  }

  @Override
  public boolean isDefinedOnMeshes() {
    return isDefinedOnMeshes;
  }

  @Override
  public String getSourceName() {
    return MvResource.getS("Coordonnées");
  }

  @Override
  public void replayData(EGGrapheModel _model, Map _infos, CtuluUI _impl) {
  }

  @Override
  public boolean isDuplicatable() {
    return false;
  }

  @Override
  public boolean isReplayable() {
    return false;
  }

  @Override
  public boolean isRemovable() {
    return true;
  }

  @Override
  public boolean isTitleModifiable() {
    return true;
  }

  @Override
  public void setRes(final EfLineIntersectionsResultsBuilder _res, final ProgressionInterface _prog) {
    xyProvider = MvProfileXYProvider.createCoordinateProvider(_res.getInitRes(), isX_);
    isDefinedOnMeshes = _res.getInitRes().isForMesh();

  }

  @Override
  public void fillWithInfo(final InfoData _table, final CtuluListSelectionInterface _selectedPt) {
  }

  @Override
  public EGModel duplicate() {

    final MvProfileCoordinatesModel duplic = new MvProfileCoordinatesModel(data_, initLine, this.xyProvider, isX_, isDefinedOnMeshes);
    duplic.title_ = this.title_;
    return duplic;
  }

  @Override
  public Object savePersistSpecificDatas() {
    return null;
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
  }

  @Override
  public boolean isGenerationSourceVisible() {
    return true;
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
  }

@Override
public int[] getInitRows() {
	return initLineRows;
}

public void setInitRows(int[] initLineRows) {
	this.initLineRows = initLineRows;
}
}
