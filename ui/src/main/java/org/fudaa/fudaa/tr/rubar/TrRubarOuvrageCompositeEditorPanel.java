package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageCompositeTypeControle;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.post.TrPostRubarOuvrageModel;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("serial")
public class TrRubarOuvrageCompositeEditorPanel extends CtuluDialogPanel {
  public static boolean edit(H2dRubarGridAreteSource grid, H2dRubarOuvrageElementaireComposite ouvrage, H2dRubarOuvrage parent,
                             CtuluUI ui, TrRubarEvolutionManager evolManager, TrRubarTarageCourbesManager trRubarTarageCourbesManager) {
    TrRubarOuvrageCompositeEditorPanel panel = new TrRubarOuvrageCompositeEditorPanel(grid, ouvrage, parent, evolManager, trRubarTarageCourbesManager);
    panel.setCtuluUi(ui);
    panel.build();
    return panel.afficheModaleOk(ui.getParentComponent());
  }

  private final H2dRubarGridAreteSource grid;
  private BuLabel lbAmont;
  private BuLabel lbAval;
  private H2dRubarOuvrage h2dRubarOuvrage;
  private H2dRubarOuvrageElementaireComposite ouvrage;
  CtuluUI ctuluUI;
  private BuButton editValues;
  private final TrRubarEvolutionManager evolManager;
  private final TrRubarTarageCourbesManager trRubarTarageCourbesManager;

  public TrRubarOuvrageCompositeEditorPanel(H2dRubarGridAreteSource grid, H2dRubarOuvrageElementaireComposite ouvrage,
                                            H2dRubarOuvrage parent, final TrRubarEvolutionManager evolManager, TrRubarTarageCourbesManager trRubarTarageCourbesManager) {
    this.grid = grid;
    this.evolManager = evolManager;
    this.trRubarTarageCourbesManager = trRubarTarageCourbesManager;
    this.ouvrage = ouvrage;
    this.h2dRubarOuvrage = parent;
  }

  protected void build() {
    setLayout(new BuGridLayout(3, 5, 5));

    addLabel(TrLib.getString("Contr�le"));
    final BuComboBox cb = new BuComboBox(
        new Object[]{H2dRubarOuvrageCompositeTypeControle.TIME, H2dRubarOuvrageCompositeTypeControle.ELEVATION});
    cb.setRenderer(new CtuluCellTextRenderer() {
      @Override
      protected void setValue(Object _value) {
        if (_value == null) {
          super.setValue(_value);
        } else {
          super.setValue(((H2dRubarOuvrageCompositeTypeControle) _value).getDesc());
        }
      }
    });
    add(cb);
    cb.setSelectedItem(ouvrage.getTypeControle());
    cb.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        ouvrage.setTypeControle((H2dRubarOuvrageCompositeTypeControle) cb.getSelectedItem());
      }
    });
    add(new JLabel());

    if (ouvrage.getAmontMesh() < 0) {
      ouvrage.setAmontMesh(h2dRubarOuvrage.getElt1());
    }
    if (ouvrage.getAvalMesh() < 0) {
      ouvrage.setAvalMesh(h2dRubarOuvrage.getElt2());
    }

    addLabel(TrLib.getString("Maille Amont"));
    lbAmont = addLabel(CtuluLibString.EMPTY_STRING);
    updateAmontLabel();
    BuButton editAmont = new BuButton(BuResource.BU.getString("Editer"));
    editAmont.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        editAmont();
      }
    });
    add(editAmont);
    addLabel(TrLib.getString("Maille Aval"));
    lbAval = addLabel(CtuluLibString.EMPTY_STRING);
    updateAvalLabel();
    BuButton editAval = new BuButton(BuResource.BU.getString("Editer"));
    editAval.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        editAval();
      }
    });
    add(editAval);
    addLabel(TrLib.getString("Valeurs Temps / Cote"));
    editValues = new BuButton(BuResource.BU.getString("Editer"));
    editValues.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        editValues();
      }
    });
    updateButtonValues();
    add(editValues);
    add(new JLabel());
    addLabel(TrLib.getString("Ouvrages �l�mentaires"));
    BuButton editOuvrages = new BuButton(BuResource.BU.getString("Editer"));
    editOuvrages.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        editOuvrages();
      }
    });
    add(editOuvrages);
    add(new JLabel());
  }

  @Override
  public boolean apply() {
    return super.apply();
  }

  @Override
  public boolean isDataValid() {
    int nbOuv = ouvrage.getNbOuvrages();
    if (nbOuv == 0) {
      setErrorText(TrLib.getString("Au moins un ouvrage doit �tre cr��"));
      return false;
    }
    setErrorText(null);
    return super.isDataValid();
  }

  public void editOuvrages() {
    H2dRubarOuvrageElementaireInterface[] structures = ouvrage.getOuvrages();
    H2dRubarOuvrageElementaireInterface[] copiedStructure = new H2dRubarOuvrageElementaireInterface[structures.length];
    for (int i = 0; i < copiedStructure.length; i++) {
      copiedStructure[i] = structures[i].getCopyWithoutEvt();
    }
    TrRubarOuvrageElementaireEditorPanel editorPanel = new TrRubarOuvrageElementaireEditorPanel(copiedStructure, grid, null, ctuluUI,
        evolManager, trRubarTarageCourbesManager);
    editorPanel.setOuvrage(this.h2dRubarOuvrage);
    List ouvragesTypes = new ArrayList(Arrays.asList(editorPanel.getOuvrageTypes()));
    ouvragesTypes.remove(H2dRubarOuvrageType.COMPOSITE);
    editorPanel.setOuvrageTypes(ouvragesTypes.toArray());
    CtuluDialogPanel panel = new CtuluDialogPanel();
    panel.setLayout(new BuBorderLayout());
    panel.add(editorPanel.getEditor());
    boolean ok = panel.afficheModaleOk(ctuluUI.getParentComponent(), TrLib.getString("Ouvrages �l�mentaires"), null);
    if (ok) {
      ouvrage.setOuvrages(editorPanel.getOuvrages());
      updateButtonValues();
    }
  }

  private static class DoubleTableModel extends AbstractTableModel {
    private final Double[] values;
    private final String colName;

    public DoubleTableModel(Double[] values, final String colName) {
      super();
      this.values = values;
      this.colName = colName;
    }

    @Override
    public int getRowCount() {
      return values.length;
    }

    @Override
    public int getColumnCount() {
      return 1;
    }

    @Override
    public String getColumnName(int column) {
      return colName;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return true;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      return values[rowIndex];
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      values[rowIndex] = (Double) aValue;
    }

    protected Double[] getValues() {
      return values;
    }
  }

  protected void editValues() {
    double[] valeurs = ouvrage.getValeurs();
    if (CtuluLibArray.isEmpty(valeurs)) {
      this.ctuluUI.message(TrLib.getString("Edition"), TrLib.getString("Pas de valeurs � �diter pour un ouvrage unique"), false);
      return;
    }
    Double[] values = new Double[valeurs.length];
    for (int i = 0; i < values.length; i++) {
      values[i] = valeurs[i];
    }
    DoubleTableModel tableModel = new DoubleTableModel(values, ouvrage.getTypeControle().getDesc());
    final JTable table = new JTable(tableModel);
    CtuluValueEditorDouble editor = new CtuluValueEditorDouble(false);
    editor.setFormatter(CtuluNumberFormatDefault.DEFAULT_FMT_3DIGITS);
    table.getColumnModel().getColumn(0).setCellEditor(editor.createTableEditorComponent());
    table.getColumnModel().getColumn(0).setCellEditor(editor.createTableEditorComponent());
    CtuluDialogPanel pn = new CtuluDialogPanel() {
      @Override
      public boolean isDataValid() {
        TableCellEditor cellEditor = table.getCellEditor();
        if (cellEditor != null) {
          cellEditor.stopCellEditing();
        }

        return true;
      }
    };
    pn.setLayout(new BorderLayout());
    final JScrollPane jScrollPane = new JScrollPane(table);
    jScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    jScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    pn.add(jScrollPane);
    boolean ok = pn.afficheModaleOk(this, ouvrage.getTypeControle().getDesc());
    if (ok) {
      ouvrage.setValues(tableModel.getValues());
    }
  }

  private void updateButtonValues() {
    double[] valeurs = ouvrage.getValeurs();
    editValues.setToolTipText(CtuluLibString.arrayToString(valeurs));
    H2dRubarOuvrageElementaireInterface[] ouvrages = ouvrage.getOuvrages();
    editValues.setEnabled(ouvrages != null && ouvrages.length > 1);
  }

  protected void editAmont() {
    int idx = selectMeshes(ouvrage.getAmontMesh(), TrLib.getString("Editer la maille amont"));
    ouvrage.setAmontMesh(idx);
    updateAmontLabel();
  }

  protected void editAval() {
    int idx = selectMeshes(ouvrage.getAvalMesh(), TrLib.getString("Editer la maille aval"));
    ouvrage.setAvalMesh(idx);
    updateAvalLabel();
  }

  private String formatXY(double xAmont, double yAmont) {
    return CtuluNumberFormatDefault.DEFAULT_FMT_3DIGITS.format(xAmont) + ", " + CtuluNumberFormatDefault.DEFAULT_FMT_3DIGITS.format(
        yAmont);
  }

  public CtuluUI getCtuluUI() {
    return ctuluUI;
  }

  private int selectMeshes(int initIdx, String title) {
    final H2dRubarOuvrageMng mng = new H2dRubarOuvrageMng(grid);
    mng.setProjetType(evolManager.getProjectType(), evolManager.getRubarParam().getBcMng().getNbConcentrationBlocks(), null);
    mng.addOuvrage(h2dRubarOuvrage, null);
    TrPostRubarOuvrageModel ouvrageLayerModel = new TrPostRubarOuvrageModel(grid, mng, null);
    TrRubarOuvrageLayer ouvrage = new TrRubarOuvrageLayer(ouvrageLayerModel);

    final ZEbliCalquesPanel cq = new ZEbliCalquesPanel(ctuluUI);
    cq.addCalque(ouvrage);
    final MvElementLayer element = new MvElementLayer(new MvElementModelDefault(grid.getGrid()));
    cq.addCalque(element);
    cq.setCalqueActif(element);
    if (initIdx >= 0) {
      element.setSelection(new int[]{initIdx});
    }

    final List l = cq.getController().getActions();
    final BuPanel tb = new BuPanel();
    tb.setOpaque(true);
    tb.setLayout(new BuVerticalLayout(0, false, false));
    BuPanel tbTool = new BuPanel();
    tbTool.setLayout(new BuButtonLayout(0, SwingConstants.LEFT));
    for (int i = 0; i < l.size() - 2; i++) {
      final EbliActionInterface ac = (EbliActionInterface) l.get(i);
      if (ac != null) {
        tbTool.add(ac.buildToolButton(EbliComponentFactory.INSTANCE));
      }
    }

    CtuluDialogPanel pn = new CtuluDialogPanel() {
      @Override
      public boolean isDataValid() {
        int[] selectedIndex = element.getSelectedIndex();
        boolean valid = selectedIndex != null && selectedIndex.length == 1;
        if (!valid) {
          setErrorText(TrLib.getString("S�lectionner au moins un �l�ment"));
        } else {
          setErrorText(null);
        }
        return valid;
      }
    };
    pn.setLayout(new BorderLayout());
    pn.setPreferredSize(new Dimension(500, 500));
    pn.add(cq);
    pn.add(tbTool, BorderLayout.NORTH);
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        if (element.isSelectionEmpty()) {
          cq.getController().restaurer();
        } else {
          cq.zoomOnSelected();
        }
        cq.getController().setCalqueSelectionActif();
      }
    });
    boolean ok = pn.afficheModaleOk(this, title);
    if (ok) {
      int[] selectedIndex = element.getSelectedIndex();
      if (selectedIndex != null && selectedIndex.length == 1) {
        return selectedIndex[0];
      }
    }
    return initIdx;
  }

  public void setCtuluUi(CtuluUI ui) {
    this.ctuluUI = ui;
  }

  private void updateAmontLabel() {
    if (ouvrage.getAmontMesh() >= 0) {
      String formatXY = formatXY(ouvrage.getXAmont(), ouvrage.getYAmont());
      lbAmont.setText(Integer.toString(ouvrage.getAmontMesh() + 1) + " (" + formatXY + ")");
    } else {
      lbAmont.setText(TrLib.getString("Aucune s�lection"));
    }
    lbAmont.setToolTipText(lbAmont.getText());
  }

  private void updateAvalLabel() {
    if (ouvrage.getAvalMesh() >= 0) {
      String formatXY = formatXY(ouvrage.getXAval(), ouvrage.getYAval());
      lbAval.setText(Integer.toString(ouvrage.getAvalMesh() + 1) + " (" + formatXY + ")");
    } else {
      lbAval.setText(TrLib.getString("Aucune s�lection"));
    }
    lbAval.setToolTipText(lbAval.getText());
    lbAval.setToolTipText(lbAval.getToolTipText());
  }
}
