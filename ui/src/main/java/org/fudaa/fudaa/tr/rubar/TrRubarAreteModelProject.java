/**
 * @creation 10 juin 2004
 * @modification $Date: 2006-10-24 12:50:49 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.rubar;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarArete;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.rubar.H2dRubarTimeConditionCommon;
import org.fudaa.dodico.h2d.rubar.H2dRubarTimeConditionConcentrationBlock;
import org.fudaa.dodico.h2d.type.H2dRubarBcTypeList;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarAreteModelProject.java,v 1.10 2006-10-24 12:50:49 deniger Exp $
 */
public class TrRubarAreteModelProject extends TrRubarAreteModelDefault {
  H2dRubarParameters p_;

  /**
   * @param _g le maillage
   */
  public TrRubarAreteModelProject(final H2dRubarParameters _g) {
    super(_g.getGridVolume(), _g);
    p_ = _g;
  }

  @Override
  public CtuluNumberFormatI getXYFormat() {
    return p_.getFmt().getXY().getNumberFormat();
  }

  @Override
  public final void fillWithInfo(final InfoData _m, final ZCalqueAffichageDonneesInterface _layer) {
    super.fillWithInfo(_m, _layer);
    final int selected = _layer.isOnlyOneObjectSelected() ? _layer.getLayerSelection().getMaxIndex() : -1;
    if (selected < 0) {
      return;
    }
    final H2dRubarArete a = selected < 0 ? null : getRubarArete(selected);

    if (a != null && a.isExtern()) {
      final int globalIdx = getGlobalIdx(selected);
      _m.put(TrResource.getS("Type"), a.getType().toString());
      // loi de tarage
      if (a.getType() == H2dRubarBcTypeList.TARAGE) {
        final EvolutionReguliereAbstract e = p_.getTarageMng().getEvol(globalIdx);
        if (e != null) {
          _m.put(H2dResource.getS("Loi de tarage"), e.getNom());
        }
      }
      // autre cl
      else {
        final H2dRubarTimeConditionCommon c = p_.getBcMng().getCommonTimeCondition(new int[]{globalIdx});
        final int nbBlocks = c.getConcentrationsBlock().size();
        boolean onlyOneBlock = nbBlocks == 1;
        final CtuluPermanentList test = p_.getBcMng().getAllTimeVar();
        for (int i = 0; i < test.size(); i++) {
          final H2dVariableType variableType = (H2dVariableType) test.get(i);
          if (!H2dRubarTimeConditionConcentrationBlock.isTransientVariable(variableType)
              || onlyOneBlock) {
            final EvolutionReguliereAbstract e = c.getEvol(variableType, 0);
            if (e != null) {
              _m.put((variableType).getName(), e.getNom());
            }
          } else {
            for (int idxBlock = 0; idxBlock < nbBlocks; idxBlock++) {
              final EvolutionReguliereAbstract e = c.getEvol(variableType, idxBlock);
              if (e != null) {
                _m.put((variableType).getName() + CtuluLibString.getEspaceString(idxBlock + 1), e.getNom());
              }
            }
          }
        }
      }
    }
  }
}
