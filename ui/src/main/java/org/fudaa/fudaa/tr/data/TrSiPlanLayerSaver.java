/*
 * @creation 3 juil. 2006
 * @modification $Date: 2006-09-01 14:56:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.data;

import org.fudaa.fudaa.sig.FSigLayerLineAddedSaver;
import org.fudaa.fudaa.tr.persistence.TrSiPlanLayerPersistence;

public final class TrSiPlanLayerSaver extends FSigLayerLineAddedSaver {

  public TrSiPlanLayerSaver() {
    setPersistenceClass(TrSiPlanLayerPersistence.class.getName());
  }

}