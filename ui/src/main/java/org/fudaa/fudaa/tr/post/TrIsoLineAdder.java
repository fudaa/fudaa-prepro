/*
 * @creation 23 avr. 07
 * 
 * @modification $Date: 2007-06-28 09:28:18 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuLib;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.io.IOException;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridDataInterpolationValuesAdapter;
import org.fudaa.dodico.ef.operation.EfIsoActivity;
import org.fudaa.dodico.ef.operation.EfIsoActivitySearcher;
import org.fudaa.dodico.ef.operation.EfIsoResultDefault;
import org.fudaa.dodico.ef.operation.EfOperation;
import org.fudaa.dodico.ef.operation.EfOperationResult;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.meshviewer.export.MvExportOperationBuilderInterface;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author fred deniger
 * @version $Id: TrIsoLineAdder.java,v 1.4 2007-06-28 09:28:18 deniger Exp $
 */
public class TrIsoLineAdder {

  MvExportOperationBuilderInterface filter_;
  FSigLayerGroup grVar_;
  final TrPostVisuPanel pn_;
  final double prec_;
  final TrPostSource src_;
  final int[] ts_;
  boolean useOnValueByLayer_;
  final CtuluArrayDouble values_;
  final H2dVariableType var_;

  public TrIsoLineAdder(final TrPostSource _src, final TrPostVisuPanel _pn, final double[] _values, final int[] _ts,
          final H2dVariableType _var, final double _prec) {
    super();
    src_ = _src;
    pn_ = _pn;
    prec_ = _prec;
    values_ = new CtuluArrayDouble(_values);
    ts_ = _ts;
    var_ = _var;
  }

  private void afficheRes(final EfIsoResultDefault _res, final int _tidx, final double _v) {
    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {

        String titre = "t=" + src_.getTimeListModel().getElementAt(_tidx).toString();
        if (useOnValueByLayer_) {
          titre += CtuluLibString.ESPACE + _v;
        }
        final GISZoneCollectionLigneBrisee lb = new GISZoneCollectionLigneBrisee();

        getDestGroup();
        if (grVar_ == null) {
          return;
        }
        lb.setAttributes(grVar_.getAttributes(), null);
        if (var_ == H2dVariableType.BATHYMETRIE) {
          GISAttributeInterface att = grVar_.getAtt().getAttribute(var_.getName());
          lb.setAttributeIsZ((GISAttributeDouble) att);
        }
        lb.addAll(_res, null, false);
        final ZCalqueLigneBrisee cq = getDestGroup().addLigneBriseeLayerAct(titre, lb);
        // on n'affiche pas les sommets: plus joli
        final TraceIconModel model = new TraceIconModel(TraceIcon.CARRE_PLEIN, 0, Color.BLACK);
        for (int i = 0; i < cq.getNbSet(); i++) {
          cq.setIconModel(i, model);
        }
      }
    });

  }

  private FSigLayerGroup findExistGroup() {
    final FSigLayerGroup gr = pn_.getGroupGIS();
    final BCalque[] cq = gr.getCalques();
    if (cq != null) {
      for (int i = 0; i < cq.length; i++) {
        final Object o = cq[i].getClientProperty("iso.type");
        if (o == var_ && cq[i] instanceof FSigLayerGroup) {
          return (FSigLayerGroup) cq[i];
        }
      }
    }
    return null;
  }

  FSigLayerGroup getDestGroup() {
    if (grVar_ == null) {
      grVar_ = findExistGroup();
    }
    if (grVar_ == null) {
      final FSigLayerGroup gr = pn_.getGroupGIS();
      GISAttributeInterface att = gr.getAtt().getAttribute(var_.getName());
      if (att == null || (att.isAtomicValue() || att.getDataClass() != Double.class)) {
        att = gr.getAtt().addDoubleAttribute(var_, false);
        if (att == null) {
          att = gr.getAtt().addDoubleAttribute("iso-" + var_.getName(), false);
        }
        if (att == null) {
          FuLog.error("FTR: impossible de cr�er un attribute pour " + var_);
          return null;

        }

      }

      grVar_ = gr.addGroupAct(TrResource.getS("isolignes pour {0}", var_.toString()), gr, true,
              new GISAttributeInterface[]{att});
      grVar_.putClientProperty("iso.type", var_);
    }
    return grVar_;
  }

  void goAct(final ProgressionInterface _prog) {

    final CtuluAnalyze an = new CtuluAnalyze();
    InterpolationVectorContainer vect = src_.getInterpolator().getVect();
    for (int t = 0; t < ts_.length; t++) {
      final int tidx = ts_[t];
      EfIsoResultDefault res = new EfIsoResultDefault();
      EfGridData src = src_;
      if (filter_ != null) {
        filter_.buildOperation(_prog);
        final EfOperation filter = filter_.getOperation(tidx);
        filter.setInitGridData(src);
        EfOperationResult process = filter.process(_prog);
        an.merge(process.getAnalyze());
        src = process.getGridData();
      }
      final EfIsoActivity act = new EfIsoActivity(src.getGrid(), vect);
      act.setEpsForValues(prec_);
      final int nbValues = values_.getSize();
      try {
        EfData data = src.getData(var_, tidx);
        final EfGridDataInterpolationValuesAdapter values = new EfGridDataInterpolationValuesAdapter(src, tidx);
        final EfIsoActivitySearcher searcher = act.search(var_, data, _prog, an, values);
        if (searcher == null) {
          return;
        }
        for (int v = 0; v < nbValues; v++) {
          final double value = values_.getValue(v);
          searcher.search(value, res, _prog, an);
          if (useOnValueByLayer_) {
            afficheRes(res, tidx, value);
            res = new EfIsoResultDefault();
          }
        }
        afficheRes(res, tidx, 0);
      } catch (IOException e) {
        FuLog.error(e);
        an.manageException(e);
      }
    }
    pn_.getImpl().manageAnalyzeAndIsFatal(an);

  }

  protected void error(final String _txt) {
    pn_.getImpl().error(TrLib.getString("Rechercher les isolignes"), _txt);
  }

  public MvExportOperationBuilderInterface getFilter() {
    return filter_;
  }

  public void go() {
    final CtuluTaskDelegate task = pn_.getImpl().createTask(TrLib.getString("Rechercher les isolignes"));
    task.start(new Runnable() {
      @Override
      public void run() {
        goAct(task.getStateReceiver());
      }
    });

  }

  public boolean isUseOnValueByLayer() {
    return useOnValueByLayer_;
  }

  public void setFilter(final MvExportOperationBuilderInterface _filter) {
    filter_ = _filter;
  }

  public void setUseOnValueByLayer(final boolean _useOnValueByLayer) {
    useOnValueByLayer_ = _useOnValueByLayer;
  }
}
