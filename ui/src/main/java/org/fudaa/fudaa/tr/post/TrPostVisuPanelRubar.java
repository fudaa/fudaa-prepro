/**
 * @creation 19 janv. 2005
 * @modification $Date: 2007-05-04 14:01:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarGrid;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.meshviewer.model.MvInfoDelegate;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrInfoSenderDelegate;
import org.fudaa.fudaa.tr.rubar.*;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: TrPostVisuPanelRubar.java,v 1.19 2007-05-04 14:01:52 deniger Exp $
 */
public final class TrPostVisuPanelRubar extends TrPostVisuPanel {
  /**
   * @param _impl
   * @param _projet
   */
  public TrPostVisuPanelRubar(final TrPostCommonImplementation _impl, final TrPostProjet _projet, final BCalqueLegende _leg,
                              final TrPostSourceRubar _rubar) {
    super(_impl, _projet, _leg, _rubar);
    rubarDataUpdated();
    getArbreCalqueModel().setSelectionCalque(getIsoLayer());
  }

  @Override
  public void addCqMaillage(final EfGridInterface _m, final MvInfoDelegate _d) {
    final TrRubarGridLayerGroup gc = TrRubarGridLayerGroup.createLayer3DGroup((H2dRubarGrid) getSource().getGrid(), (TrInfoSenderDelegate) _d,null);
    gc.getAreteLayer().setVisible(false);
    addCqGroupeMaillage(gc);

    final EbliActionSimple selectEdgesOfType = new EbliActionSimple(TrResource.getS("S�lectionner les ar�tes du m�me type"), null, "SELECT_BC_ARETE") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        getFrontierLayer().selectEdgesFromSameType();
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("S�lectionner au moins une ar�te");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(true);
        //        super.setEnabled(!getBcAreteLayer().isSelectionEmpty());
      }
    };
    selectEdgesOfType.setDefaultToolTip(TrResource.getS("S�lectionner les ar�tes du m�me type"));

    addCalqueActions(gc.getAreteLayer(), new EbliActionInterface[]{new EdgeAction(), selectEdgesOfType});
  }

  protected TrRubarAreteLayer getAreteLayer() {
    return ((TrRubarGridLayerGroup) getGridGroup()).getAreteLayer();
  }

  protected final void rubarDataUpdated() {
    envUpdated();
    limniUpdated();
    addOuvrageEdge();
    zfnUpdated();
    findDialog_ = null;
  }

  void envUpdated() {
    final TrPostSourceRubar r = (TrPostSourceRubar) super.getSource();
    // les donn�es sont disponibles et le calque envelop n'est pas encore present
    if (r.isEnvelopDataSet() && getEnvelopLayer() == null) {
      final TrIsoModelAbstract m = new TrPostRubarEnvModel(r, info_);
      final TrIsoLayerDefault lay = new TrIsoLayerDefault(m);
      lay.setVisible(false);
      lay.setName("cqEnvelop");
      lay.setTitle(TrResource.getS("Maxima"));
      lay.setLegende(getCqLegend());
      addCalque(lay);
      // new VisibleUpdater();
    }
  }

  void zfnUpdated() {
    final TrPostSourceRubar r = (TrPostSourceRubar) super.getSource();

    // les donn�es sont disponibles et le calque envelop n'est pas encore present
    if (r.isEnvelopDataSet() && getEnvelopLayer() == null) {
      final TrIsoModelAbstract m = new TrPostRubarEnvModel(r, info_);
      final TrIsoLayerDefault lay = new TrIsoLayerDefault(m);
      lay.setVisible(false);
      lay.setName("cqEnvelop");
      lay.setTitle(TrResource.getS("Maxima"));
      lay.setLegende(getCqLegend());
      addCalque(lay);
      // new VisibleUpdater();
    }
  }

  void limniUpdated() {
    final TrPostSourceRubar r = (TrPostSourceRubar) super.getSource();
    // les donn�es sont disponibles et le calque envelop n'est pas encore present
    if (r.isLimniLoaded() && getLimniLayer() == null) {
      final ZCalquePoint lay = new TrRubarLimniModel(r.getLimni().getLimniPoint()).buildLayer(false);
      lay.setName("cqLimni");
      lay.setTitle(H2dResource.getS("Limnigrammes"));
      addCalque(lay, true);
      addCalqueActions(lay, new EbliActionInterface[]{new LimniAction()});
      getGridGroup().enPremier();
    }
  }

  final class LimniAction extends EbliActionSimple {
    LimniAction() {
      super(TrResource.getS("Afficher les limnigrammes"), null, "DISPLAY_LIMNI_EVOL");
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      afficheLimniEvolution();
    }

    @Override
    public String getEnableCondition() {
      return TrResource.getS("S�lectionner au moins limnigramme");
    }

    @Override
    public void updateStateBeforeShow() {
      super.setEnabled(!getLimniLayer().isSelectionEmpty());
    }
  }

  final class EdgeAction extends EbliActionSimple {
    EdgeAction() {
      super(TrResource.getS("Afficher les �volutions sur les ar�tes s�lectionn�es"), null, "DISPLAY_EDGES_VALUES");
      setDefaultToolTip(TrResource.getS("Affiche les �volutions sur les ar�tes s�lectionn�es."
          + "<br>Pour les ouvrages, les ar�tes utilis�es sont les ar�tes amont et aval."));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      afficheEdgeEvolution();
    }

    @Override
    public String getEnableCondition() {
      return TrResource.getS("S�lectionner au moins une ar�te<br>Le fichier OUT doit �tre charg�");
    }

    @Override
    public void updateStateBeforeShow() {
      final ZCalqueAffichageDonnees c = (ZCalqueAffichageDonnees) getCalqueActif();

      boolean r = ((TrPostSourceRubar) getSource()).getEdgesRes() != null && c != null && !c.isSelectionEmpty()
          && (c == getFrontierLayer() || c == getOuvrageLayer() || c == getAreteLayer());
      if (r && c != null) {
        int[] idx = null;
        if (c == getFrontierLayer()) {
          final TrRubarAreteModel model = (TrRubarAreteModel) c.modeleDonnees();
          idx = model.getGlobalIdx(c.getSelectedIndex());
        } else if (c == getOuvrageLayer()) {
          final TrPostRubarOuvrageModel model = (TrPostRubarOuvrageModel) c.modeleDonnees();
          idx = model.getSelectedAreteGlobalIdx(c.getSelectedIndex());
        } else {
          idx = getAreteLayer().getSelectedEdgeIdx();
        }
        r = ((TrPostSourceRubar) getSource()).getEdgesRes().isLocalEdgeSet(idx);
      }
      super.setEnabled(r);
    }
  }

  final class WorkAction extends EbliActionSimple {
    WorkAction() {
      super(TrResource.getS("Afficher les �volutions sur les ouvrages br�ches s�lectionn�s"), null, "DISPLAY_WORKS_VALUES");
      setDefaultToolTip(TrResource.getS("Affiche les �volutions sur les ouvrages br�ches s�lectionn�s"));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      afficheOuvrageEvolution();
    }

    @Override
    public String getEnableCondition() {
      return TrResource
          .getS("S�lectionner au moins un ouvrage contenant un ouvrage �l�mentaire de type br�che.<br>Les fichiers RES doivent �tre charg�s");
    }

    @Override
    public void updateStateBeforeShow() {
      final ZCalqueAffichageDonnees c = (ZCalqueAffichageDonnees) getCalqueActif();
      super.setEnabled(c instanceof TrRubarOuvrageLayer && !c.isSelectionEmpty()
          && ((TrPostSourceRubar) getSource()).getEdgesRes().containResOnOuvrage());
    }
  }

  void afficheLimniEvolution() {
    final ZCalquePoint l = getLimniLayer();
    if (l != null && !l.isSelectionEmpty()) {
      TrPostRubarSpecificAction.addLimniFrame(getPostImpl(), getSource(), l.getLayerSelection().getSelectedIndex());
    }
  }

  TrIsoLayerDefault getEnvelopLayer() {
    return (TrIsoLayerDefault) getDonneesCalqueByName("cqEnvelop");
  }

  ZCalquePoint getLimniLayer() {
    return (ZCalquePoint) getDonneesCalqueByName("cqLimni");
  }

  protected void afficheOuvrageEvolution() {
    final ZCalqueAffichageDonnees cq = (ZCalqueAffichageDonnees) getCalqueActif();
    if (!(cq instanceof TrRubarOuvrageLayer)) {
      return;
    }
    final int[] idxSelected = cq.getSelectedIndex();
    if (idxSelected != null) {
      final TrPostRubarEdgesResults edgesRes = ((TrPostSourceRubar) getSource()).getEdgesRes();
      edgesRes.addWorkFrame(idxSelected, getImpl(), super.projet_, getSource());
    }
  }

  protected void afficheEdgeEvolution() {
    final ZCalqueAffichageDonnees cq = (ZCalqueAffichageDonnees) getCalqueActif();
    final int[] idxSelected = cq.getSelectedIndex();
    int[] idx = null;
    if (cq == getFrontierLayer()) {
      idx = ((TrRubarAreteModel) cq.modeleDonnees()).getGlobalIdx(cq.getLayerSelection().getSelectedIndex());
    } else if (cq == getOuvrageLayer()) {
      final TrPostRubarOuvrageModel m = (TrPostRubarOuvrageModel) cq.modeleDonnees();
      idx = m.getSelectedAreteGlobalIdx(idxSelected);
    } else {
      idx = getAreteLayer().getSelectedEdgeIdx();
    }
    if (idx != null) {
      final TrPostRubarEdgesResults edgesRes = ((TrPostSourceRubar) getSource()).getEdgesRes();
      final int[] correctIdx = edgesRes.getCorrectLocalEdgeSet(idx);
      if (CtuluLibArray.isEmpty(correctIdx)) {
        if (cq.isOnlyOneObjectSelected()) {
          TrPostRubarEdgeFille.getErrorSelectedEdge(getImpl());
        } else {
          TrPostRubarEdgeFille.getErrorSelectedEdges(getImpl());
        }
        return;
      }
      edgesRes.addEdgeFrame(correctIdx, getImpl(), super.projet_, getSource());
    }
  }

  @Override
  protected void buildFrontiereLayer() {
    final TrPostRubarBcEdgeModel m = ((TrPostSourceRubar) getSource()).getBcEdgeModel();
    final TrRubarBcAreteLayer def = new TrRubarBcAreteLayer(m, getCqLegend());
    def.setName("cqGridFrRubar");
    def.setTitle(TrResource.getS("fronti�res"));
    super.addCalque(def, true);

    final EbliActionSimple selectEdgesOfType = new EbliActionSimple(TrResource.getS("S�lectionner les ar�tes du m�me type"), null, "SELECT_BC_ARETE") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        getFrontierLayer().selectEdgesFromSameType();
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("S�lectionner au moins une ar�te");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(true);
      }
    };
    selectEdgesOfType.setDefaultToolTip(TrResource.getS("S�lectionner les ar�tes du m�me type"));

    def.setActions(new EbliActionInterface[]{new EdgeAction(), def.create3DAction(getImpl(), getCmdMng()), selectEdgesOfType});
    // demande Andr� Paquier 2006-01-12
    getCqLegend().setVisible(def, false);
  }

  public TrRubarBcAreteLayer getFrontierLayer() {
    return (TrRubarBcAreteLayer) getDonneesCalqueByName("cqGridFrRubar");
  }

  public TrRubarOuvrageLayer getOuvrageLayer() {
    return (TrRubarOuvrageLayer) getDonneesCalqueByName("cqOuv");
  }

  //  @Override
  //  protected int getModeForPolygonSelection() {
  //    return EbliSelectionMode.MODE_CENTER;
  //  }

  protected void addOuvrageEdge() {
    if (getOuvrageLayer() != null) {
      return;
    }
    final TrPostRubarEdgesResults src = ((TrPostSourceRubar) getSource()).getEdgesRes();
    if (src == null) {
      return;
    }
    final TrPostRubarOuvrageModel m = src.getModel();
    if (m == null || m.getNombre() == 0) {
      return;
    }
    m.setInfoDelegate(info_);
    final TrRubarOuvrageLayer layer = new TrRubarOuvrageLayer(m);
    layer.setName("cqOuv");
    layer.setTitle(H2dResource.getS("Ouvrages"));
    layer.setLegende(getCqLegend());
    super.addCalque(layer, true);

    List<EbliActionInterface> actions = new ArrayList<EbliActionInterface>();
    actions.add(new WorkAction());
    actions.add(new EdgeAction());
    actions.add(null);
    actions.add(new EbliActionSimple(TrResource.getS("S�lectionner les ouvrages avec r�f�rence -1"), null, "SELECT_WORKS_REF_1") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        getOuvrageLayer().selectWorksWithRubarRef(-1);
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("D�sactiver le mode \"�l�ment\"");
      }
    });
    actions.add(new EbliActionSimple(TrResource.getS("S�lectionner les ouvrages avec r�f�rence -2"), null, "SELECT_WORKS_REF_2") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        getOuvrageLayer().selectWorksWithRubarRef(-2);
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("D�sactiver le mode \"�l�ment\"");
      }
    });
    actions.add(new EbliActionSimple(TrResource.getS("S�lectionner les ouvrages avec une maille avale"), null, "SELECT_WORKS_DOWNSTREAM_MESH") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        getOuvrageLayer().selectWorksWithDownstreamMesh(true);
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("D�sactiver le mode \"�l�ment\"");
      }
    });
    actions.add(new EbliActionSimple(TrResource.getS("S�lectionner les ouvrages sans maille avale"), null, "SELECT_WORKS_NODOWNSTREAM_MESH") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        getOuvrageLayer().selectWorksWithDownstreamMesh(false);
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("D�sactiver le mode \"�l�ment\"");
      }
    });
    addCalqueActions(layer, actions.toArray(new EbliActionInterface[actions.size()]));
    getFrontierLayer().enPremier();
  }
}
