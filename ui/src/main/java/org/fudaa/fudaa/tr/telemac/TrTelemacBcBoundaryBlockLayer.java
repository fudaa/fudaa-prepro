/**
 * @creation 25 ao�t 2003
 * @modification $Date: 2008-02-20 10:11:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.telemac;

import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;
import gnu.trove.TIntObjectIterator;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.dodico.h2d.H2dBcFrontierBlockInterface;
import org.fudaa.dodico.h2d.H2dBoundary;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.calque.ZModeleDonnees;
import org.fudaa.ebli.calque.ZSelectionTrace;
import org.fudaa.ebli.commun.EbliListeSelectionMulti;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.fudaa.tr.data.TrBcBoundaryBlockModel;
import org.fudaa.fudaa.tr.data.TrBcBoundaryLayerAbstract;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Layer which manage the borders.
 *
 * @author deniger
 * @version $Id: TrTelemacBcBoundaryBlockLayer.java,v 1.14.6.1 2008-02-20 10:11:51 bmarchan Exp $
 */
public class TrTelemacBcBoundaryBlockLayer extends TrBcBoundaryLayerAbstract {
  protected transient TrBcBoundaryBlockModel m_;
  protected transient TraceLigne tl_;
  transient final GrSegment seg_ = new GrSegment(new GrPoint(), new GrPoint());

  /**
   * @param _m le modele correspondant / the corresponding model
   * @param _l la legende associee / the associated legend
   */
  public TrTelemacBcBoundaryBlockLayer(final TrBcBoundaryBlockModel _m, final BCalqueLegende _l) {
    super(_l);
    m_ = _m;
    initLegende();
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return new TrTelemacFindBoundary(this);
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return null;
  }

  @Override
  public List getAllBoundaryType() {
    return m_.getAllBoundaryType();
  }

  public TrBcBoundaryBlockModel getModeleBord() {
    return m_;
  }

  @Override
  public int getNbBoundaryType() {
    return m_.getNbBoundaryType();
  }

  @Override
  public int[] getSelectedElementIdx() {
    return null;
  }

  /**
   * @return les indices de frontieres selectionnes / the indexes of selected boundaries
   */
  @Override
  public int[] getSelectedObjectInTable() {
    if (!isSelectionEmpty()) {
      final TIntHashSet rTable = new TIntHashSet(m_.getNbTotalPt());
      final TIntObjectIterator itIntObj = selection_.getIterator();
      int max;
      final H2dBoundary.BordIndexIterator bordIt = new H2dBoundary.BordIndexIterator();
      for (int i = selection_.getNbListSelected() - 1; i >= 0; i--) {
        itIntObj.advance();
        final int fr = itIntObj.key();
        final H2dBcFrontierBlockInterface bd = m_.getFrontier(fr);
        final CtuluListSelection s = (CtuluListSelection) itIntObj.value();
        max = s.getMaxIndex();
        for (int j = s.getMinIndex(); j <= max; j++) {
          if (s.isSelected(j)) {
            final H2dBoundary b = bd.getBord(j);
            bordIt.set(bd.getNbPt(), b);
            while (bordIt.hasNext()) {
              rTable.add(m_.getFrontiereIndice(fr, bordIt.next()));
            }
          }
        }
      }
      return rTable.toArray();
    }
    return null;
  }

  /**
   *
   */
  @Override
  public int[] getSelectedPtIdx() {
    if (!isSelectionEmpty()) {
      final TIntHashSet r = new TIntHashSet(m_.getNbTotalPt());
      final TIntObjectIterator itIntObj = selection_.getIterator();
      int max;
      final H2dBoundary.BordIndexIterator bordIt = new H2dBoundary.BordIndexIterator();
      for (int i = selection_.getNbListSelected() - 1; i >= 0; i--) {
        itIntObj.advance();
        final int fr = itIntObj.key();
        final H2dBcFrontierBlockInterface bd = m_.getFrontier(fr);
        final CtuluListSelection s = (CtuluListSelection) itIntObj.value();
        max = s.getMaxIndex();
        for (int j = s.getMinIndex(); j <= max; j++) {
          if (s.isSelected(j)) {
            final H2dBoundary b = bd.getBord(j);
            bordIt.set(bd.getNbPt(), b);
            while (bordIt.hasNext()) {
              r.add(m_.getIdxGlobal(fr, bordIt.next()));
            }
          }
        }
      }
      return r.toArray();
    }
    return null;
  }

  @Override
  public List getUsedBoundaryType() {
    return m_.getUsedBoundaryType();
  }

  @Override
  public GrBoite getDomaineOnSelected() {
    if (isSelectionEmpty()) {
      return null;
    }
    final GrBoite r = new GrBoite();
    final GrPoint p = new GrPoint();
    final TIntObjectIterator it = selection_.getIterator();
    final H2dBoundary.BordIndexIterator bordIt = new H2dBoundary.BordIndexIterator();
    for (int i = selection_.getNbListSelected(); i-- > 0; ) {
      it.advance();
      final int frIdx = it.key();
      final H2dBcFrontierBlockInterface fr = m_.getFrontier(frIdx);
      final CtuluListSelection s = (CtuluListSelection) it.value();
      int max = s.getMaxIndex();
      if (max >= m_.getNbElementIn(frIdx)) {
        max = m_.getNbElementIn(frIdx) - 1;
      }
      for (int j = s.getMinIndex(); j <= max; j++) {
        if (s.isSelected(j)) {
          bordIt.set(fr.getNbPt(), fr.getBord(j));
          while (bordIt.hasNext()) {
            m_.getPoint(p, frIdx, bordIt.next());
            r.ajuste(p);
          }
        }
      }
    }
//    ajusteZoomOnSelected(r);
    return r;
  }

  @Override
  public boolean isFontModifiable() {
    return false;
  }

  @Override
  public boolean isPaletteModifiable() {
    return false;
  }

  @Override
  public boolean isSelectionElementEmpty() {
    return true;
  }

  @Override
  public boolean isSelectionPointEmpty() {
    return isSelectionEmpty();
  }

  @Override
  public ZModeleDonnees modeleDonnees() {
    return m_;
  }

  @Override
  public LineString getSelectedLine() {
    if (!isOnlyOneObjectSelected()) {
      return null;
    }
    final int idxFr = selection_.getIdxSelection().getMinIndex();
    final H2dBoundary.BordIndexIterator it = new H2dBoundary.BordIndexIterator();
    final H2dBcFrontierBlockInterface bd = m_.getFrontier(idxFr);
    final H2dBoundary boundary = bd.getBord(selection_.getSelection(idxFr).getMinIndex());
    it.set(bd.getNbPt(), boundary);
    final List cs = new ArrayList();
    while (it.hasNext()) {
      final int idxEnCours = it.next();
      m_.getPoint(seg_.e_, idxFr, idxEnCours);
      cs.add(new Coordinate(seg_.e_.x_, seg_.e_.y_));
    }
    return GISGeometryFactory.INSTANCE.createLineString((Coordinate[]) cs.toArray(new Coordinate[cs.size()]));
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
                           final GrBoite _clipReel) {
    if (tl_ == null) {
      tl_ = new TraceLigne();
      tl_.setEpaisseur(1f);
      tl_.setCouleur(Color.red);
    }
    final int n = m_.getNbFrontier();
    final GrPoint grpTemp = new GrPoint();
    // une boite tempo contenenant les points fin et init
    // a tempo box containing end and init points
    final GrBoite b = new GrBoite();
    int idxEnCours;
    final H2dBoundary.BordIndexIterator it = new H2dBoundary.BordIndexIterator();
    H2dBoundary bdElt;
    // on parcourt les frontiere du maillage
    // we browse the boundaries of mesh
    for (int i = 0; i < n; i++) {
      final H2dBcFrontierBlockInterface bd = m_.getFrontier(i);
      final int nbPt = bd.getNbPt();
      final int nbBord = bd.getNbBord();
      // on parcourt les different type de bord pour la frontiere i
      // we browse the different type of edge for the boundary i
      for (int j = 0; j < nbBord; j++) {
        bdElt = bd.getBord(j);
        tl_.setModel(getTlData(bdElt.getType()));
        it.set(nbPt, bdElt);

        if (it.hasNext()) {
          idxEnCours = it.next();
          m_.getPoint(seg_.o_, i, idxEnCours);
          while (it.hasNext()) {
            idxEnCours = it.next();
            m_.getPoint(seg_.e_, i, idxEnCours);
            seg_.boite(b);
            if (_clipReel.intersectXY(b)) {
              grpTemp.initialiseAvec(seg_.e_);
              seg_.autoApplique(_versEcran);

              tl_.dessineTrait(_g, seg_.o_.x_, seg_.o_.y_, seg_.e_.x_, seg_.e_.y_);
              seg_.o_.initialiseAvec(grpTemp);
            } else {
              seg_.o_.initialiseAvec(seg_.e_);
            }
          }
        }
        // pour faire le tour si bord unique.
        // to go round if single edge
        if (nbBord == 1) {
          idxEnCours = bdElt.getIdxDeb();
          m_.getPoint(seg_.e_, i, idxEnCours);
          seg_.boite(b);
          if (_clipReel.intersectXY(b)) {
            seg_.autoApplique(_versEcran);
            tl_.dessineTrait(_g, seg_.o_.x_, seg_.o_.y_, seg_.e_.x_, seg_.e_.y_);
          }
        }
      }
    }
  }

  @Override
  public void doPaintSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
                               final GrBoite _clipReel) {
    Color cs = _trace.getColor();
    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    final TraceLigne tlSelection = _trace.getLigneCopy();
    // TraceIconeNormal ic = _trace.getIcone();
    if (isAttenue()) {
      tlSelection.setCouleur(attenueCouleur(cs));
    }
    // _g.setColor(cs);
    int idxEnCours;
    final H2dBoundary.BordIndexIterator it = new H2dBoundary.BordIndexIterator();
    H2dBoundary bdElt;
    final GrBoite b = new GrBoite();
    final GrPoint grpTemp = new GrPoint();
    final TIntObjectIterator itIntObj = selection_.getIterator();
    int max;
    for (int i = selection_.getNbListSelected() - 1; i >= 0; i--) {
      itIntObj.advance();
      final int fr = itIntObj.key();
      final H2dBcFrontierBlockInterface bd = m_.getFrontier(fr);
      final CtuluListSelection s = (CtuluListSelection) itIntObj.value();
      max = s.getMaxIndex();
      for (int j = s.getMinIndex(); j <= max; j++) {
        if (s.isSelected(j)) {
          if (j >= bd.getNbBord()) {
            s.remove(j);
          } else {
            bdElt = bd.getBord(j);
            it.set(bd.getNbPt(), bdElt);
            idxEnCours = it.next();
            m_.getPoint(seg_.o_, fr, idxEnCours);
            while (it.hasNext()) {
              idxEnCours = it.next();
              m_.getPoint(seg_.e_, fr, idxEnCours);
              seg_.boite(b);
              if (_clipReel.intersectXY(b)) {
                grpTemp.initialiseAvec(seg_.e_);
                seg_.autoApplique(_versEcran);
                tlSelection.dessineTrait(_g, seg_.o_.x_, seg_.o_.y_, seg_.e_.x_, seg_.e_.y_);
                seg_.o_.initialiseAvec(grpTemp);
              } else {
                seg_.o_.initialiseAvec(seg_.e_);
              }
            }
            // pour faire le tour si bord unique.
            // to go round if the edge is unique           
            if (bdElt.isUnique()) {
              idxEnCours = bdElt.getIdxDeb();
              m_.getPoint(seg_.e_, fr, idxEnCours);
              seg_.boite(b);
              if (_clipReel.intersectXY(b)) {
                seg_.autoApplique(_versEcran);
                tlSelection.dessineTrait(_g, seg_.o_.x_, seg_.o_.y_, seg_.e_.x_, seg_.e_.y_);
              }
            }
          }
        }
      }
    }
  }

  /**
   *
   */
  @Override
  public EbliListeSelectionMulti selection(final GrPoint _pt, final int _tolerance) {
    GrBoite bClip = getDomaine();
    final double distanceReel = GrMorphisme.convertDistanceXY(getVersReel(), _tolerance);
    if ((!bClip.contientXY(_pt)) && (bClip.distanceXY(_pt) > distanceReel)) {
      return null;
    }
    bClip = getClipReel(getGraphics());
    H2dBoundary b;
    final GrSegment segTmp = new GrSegment();
    segTmp.e_ = new GrPoint();
    segTmp.o_ = new GrPoint();
    final GrPoint oReel = new GrPoint();
    final GrBoite boite = new GrBoite();
    final H2dBoundary.BordIndexIterator it = new H2dBoundary.BordIndexIterator();
    int idxEnCours;
    H2dBcFrontierBlockInterface frontierBord;
    for (int i = m_.getNbFrontier() - 1; i >= 0; i--) {
      frontierBord = m_.getFrontier(i);
      for (int j = frontierBord.getNbBord() - 1; j >= 0; j--) {
        b = frontierBord.getBord(j);
        // idxFrontiere= b.getIdxFrontiere();
        it.set(frontierBord.getNbPt(), b);
        // le premier indice
        // the first indication
        idxEnCours = it.next();
        // on initialise le point e.
        // we initialize the point e
        m_.getPoint(segTmp.e_, i, idxEnCours);
        while (it.hasNext()) {
          idxEnCours = it.next();
          // on recupere les coordonnees du point suivant dans seg.o
          // we get back the coodinates of the following point in seg.o
          m_.getPoint(segTmp.o_, i, idxEnCours);
          // le point e pour la prochaine iteration
          // the point e for the next iteration
          oReel.initialiseAvec(segTmp.o_);
          segTmp.boite(boite);
          if (boite.intersectXY(bClip) && segTmp.estSelectionne(distanceReel, _pt)) {
            final EbliListeSelectionMulti r = new EbliListeSelectionMulti(m_.getNbFrontier());
            r.set(i, j);
            return r;
          }
          // on initialise le point est avec les val sauv de o
          // we initialize the point est with the saved values of o  
          segTmp.e_.initialiseAvec(oReel);
        }
      }
    }
    return null;
  }

  /**
   *
   */
  @Override
  public EbliListeSelectionMulti selection(final LinearRing _poly) {
    if (m_.getNombre() == 0 || !isVisible()) {
      return null;
    }
    final Envelope polyEnv = _poly.getEnvelopeInternal();
    final GrBoite domaineBoite = getDomaine();
    final Envelope domaine = new Envelope(domaineBoite.e_.x_, domaineBoite.o_.x_, domaineBoite.e_.y_,
        domaineBoite.o_.y_);
    // si l'envelop du polygone n'intersect pas le domaine, il n'y a pas de selection
    // if the envelope of the polygon doesn't intersect the field, there is no selection
    if (!polyEnv.intersects(domaine)) {
      return null;
    }
    final IndexedPointInAreaLocator tester = new IndexedPointInAreaLocator(_poly);
    int idxEnCours;
    H2dBcFrontierBlockInterface frontierBord;
    H2dBoundary b;
    final Coordinate c = new Coordinate();
    final GrPoint pTemp = new GrPoint();
    final EbliListeSelectionMulti r = new EbliListeSelectionMulti(m_.getNbFrontier());
    CtuluListSelection lBord = null;
    boolean add;
    final H2dBoundary.BordIndexIterator it = new H2dBoundary.BordIndexIterator();
    for (int i = m_.getNbFrontier() - 1; i >= 0; i--) {
      frontierBord = m_.getFrontier(i);
      lBord = null;
      add = true;
      for (int j = frontierBord.getNbBord() - 1; j >= 0; j--) {
        b = frontierBord.getBord(j);
        // idxFrontiere= b.getIdxFrontiere();
        it.set(frontierBord.getNbPt(), b);
        add = true;
        while ((it.hasNext()) && add) {
          idxEnCours = it.next();
          m_.getPoint(pTemp, i, idxEnCours);
          c.x = pTemp.x_;
          c.y = pTemp.y_;
          if ((!polyEnv.contains(pTemp.x_, pTemp.y_)) || (!GISLib.isInside(tester, c))) {
            add = false;
          }
        }
        if (add) {
          if (lBord == null) {
            lBord = new CtuluListSelection(frontierBord.getNbBord());
          }
          lBord.add(j);
        }
      }
      if (lBord != null) {
        r.set(i, lBord);
      }
    }
    if (r.isEmpty()) {
      return null;
    }
    return r;
  }

  public int getNbFrontiereLiquide() {
    return m_.getNbLiquidFrontier();
  }

  public boolean setLiquidSelection(final int[] _idx) {
    if (_idx == null || _idx.length == 0) {
      return false;
    }
    if (selection_ == null) {
      selection_ = createSelection();
    } else {
      selection_.clear();
    }
    final H2dBcFrontierBlockInterface frontier = m_.getFrontier(0);
    int n = frontier.getNbBord();
    final TIntArrayList boundaries = new TIntArrayList(n);
    boolean r = false;
    for (int i = 0; i < n; i++) {
      if (frontier.getBord(i).getType().isLiquide()) {
        boundaries.add(i);
      }
    }
    n = boundaries.size();
    for (int i = _idx.length - 1; i >= 0; i--) {
      if ((_idx[i] >= 0) && (_idx[i] < n)) {
        selection_.add(0, boundaries.get(_idx[i]));
        r = true;
      }
    }
    fireSelectionEvent();
    return r;
  }
}
