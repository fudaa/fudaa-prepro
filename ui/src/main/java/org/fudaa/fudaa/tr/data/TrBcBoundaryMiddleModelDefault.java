/**
 * @creation 21 nov. 2003
 * @modification $Date: 2006-09-19 15:07:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuTable;
import org.locationtech.jts.geom.Envelope;
import java.util.List;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.h2d.H2dBcFrontierMiddleInterface;
import org.fudaa.dodico.h2d.H2dBcManagerMiddleInterface;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModeleDonnesAbstract;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * @author deniger
 * @version $Id: TrBcBoundaryMiddleModelDefault.java,v 1.18 2006-09-19 15:07:28 deniger Exp $
 */
public class TrBcBoundaryMiddleModelDefault extends ZModeleDonnesAbstract implements TrBcBoundaryMiddleModel {

  protected TrInfoSenderDelegate delegate_;
  protected H2dBcManagerMiddleInterface mng_;

  public final H2dBcManagerMiddleInterface getMng() {
    return mng_;
  }

  @Override
  public EfGridInterface getGrid() {
    return mng_.getGrid();
  }

  /**
   * @param _mng le manager de bord
   */
  public TrBcBoundaryMiddleModelDefault(final H2dBcManagerMiddleInterface _mng) {
    mng_ = _mng;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    return null;
  }

  @Override
  public final void fillWithInfo(final InfoData _m, final ZCalqueAffichageDonneesInterface _s) {
    if (delegate_ != null) {
      delegate_.fillWithBoundaryMiddleInfo(_m, _s.getLayerSelectionMulti());
    }
  }

  @Override
  public List getBordList() {
    return mng_.getAllowedBordTypeList();
  }

  /**
   * @return le delegue pour les infos
   */
  public TrInfoSenderDelegate getDelegate() {
    return delegate_;
  }

  @Override
  public GrBoite getDomaine() {
    final EfGridInterface f = mng_.getGrid();
    final GrBoite b = new GrBoite();
    final Envelope e = new Envelope();
    f.getEnvelope(e);
    b.o_ = new GrPoint(e.getMinX(), e.getMinY(), 0);
    b.e_ = new GrPoint(e.getMaxX(), e.getMaxY(), 0);
    return b;
  }

  @Override
  public H2dBcFrontierMiddleInterface getFrontier(final int _idxFrontier) {
    return mng_.getMiddleFrontier(_idxFrontier);
  }

  @Override
  public int getFrontiereIndice(final int _idxFr, final int _idxOnFr) {
    return mng_.getGrid().getFrontiers().getFrontiereIndice(_idxFr, _idxOnFr);
  }

  @Override
  public int getGlobalIdx(final int _idxFr, final int _idxOnFrontier) {
    return mng_.getGrid().getFrontiers().getIdxGlobal(_idxFr, _idxOnFrontier);
  }

  @Override
  public int getNbBoundaryType() {
    return mng_.getNbBoundaryType();
  }

  @Override
  public int getNbElementIn(final int _i) {
    return EfLib.getNbMiddlePt(mng_.getMiddleFrontier(_i).getNbPt());
  }

  @Override
  public int getNbFrontier() {
    return mng_.getGrid().getFrontiers().getNbFrontier();
  }

  @Override
  public int getNbTotalPt() {
    return mng_.getGrid().getFrontiers().getNbTotalPt();
  }

  @Override
  public int getNombre() {
    return getNbFrontier();
  }

  @Override
  public Object getObject(final int _ind) {
    return null;
  }

  @Override
  public void getPoint(final GrPoint _p, final int _idxFr, final int _idxPtOnFrontier) {
    final EfGridInterface g = mng_.getGrid();
    final int idx = g.getFrontiers().getIdxGlobal(_idxFr, _idxPtOnFrontier);
    _p.setCoordonnees(g.getPtX(idx), g.getPtY(idx), g.getPtZ(idx));
  }

  @Override
  public List getUsedBoundaryType() {
    return mng_.getUsedBoundaryType();
  }

  @Override
  public boolean isValuesTableAvailable() {
    return false;
  }

  /**
   * @param _delegate le delegue envoyeur d'infos.
   */
  public void setDelegate(final TrInfoSenderDelegate _delegate) {
    delegate_ = _delegate;
  }

}
