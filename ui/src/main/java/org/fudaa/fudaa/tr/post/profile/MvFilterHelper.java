/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.profile;

import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.dodico.ef.EfFilter;
import org.fudaa.dodico.ef.EfFilterSelectedElement;
import org.fudaa.dodico.ef.EfFilterSelectedNode;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;

/**
 *
 * @author Frederic Deniger
 */
public class MvFilterHelper {

  /**
   * @param _data les donnees
   * @param _pn les calques
   * @return le filtres en cours
   */
  public static EfFilter getSelectedFilter(final EfGridData _data, final ZEbliCalquesPanel _pn) {
    EfFilter filter = null;
    if (_pn != null && _pn.getCalqueActif() instanceof MvLayerGrid) {
      final MvLayerGrid cq = (MvLayerGrid) _pn.getCalqueActif();
      if (!cq.isSelectionElementEmpty()) {
        filter = new EfFilterSelectedElement(new CtuluListSelection(cq.getSelectedElementIdx()), _data.getGrid());
      } else if (!cq.isSelectionPointEmpty()) {
        filter = new EfFilterSelectedNode(new CtuluListSelection(cq.getSelectedPtIdx()), _data.getGrid());
      }
    }
    return filter;
  }
  
}
