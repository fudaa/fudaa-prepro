package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuGridLayout;
import java.awt.event.ActionEvent;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.dico.DicoEntite.Vecteur;
import org.fudaa.dodico.h2d.telemac.H2dTelemacTracerMng;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.fdico.FDicoEditorInterface;
import org.fudaa.fudaa.fdico.FDicoTableEditorChooser;
import org.fudaa.fudaa.tr.common.TrLib;

/**
 * @author genesis
 */
public class TrTelemacSourceAddTracerAction extends EbliActionSimple {
  private final H2dTelemacTracerMng source;
  private final CtuluCommandManager cmdManager;

  public TrTelemacSourceAddTracerAction(H2dTelemacTracerMng source, CtuluCommandManager cmdManager) {
    super(TrLib.getString("Ajouter un traceur"), null, "ADD_TRACER");
    this.source = source;
    this.cmdManager = cmdManager;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {

    final FDicoTableEditorChooser.EditorCreator creat = new FDicoTableEditorChooser.EditorCreator();
    Vecteur tracerNameDicoEntite = source.getTracerNameDicoEntite();
    final FDicoEditorInterface nameEditor = creat.createEditor(tracerNameDicoEntite.getType());

    Vecteur initialesValuesDicoEntite = source.getTracerInitialesValuesDicoEntite();
    FDicoEditorInterface initialesValuesEditor = creat.createEditor(initialesValuesDicoEntite.getType());

    CtuluDialogPanel pn = new CtuluDialogPanel() {
      @Override
      public boolean isDataValid() {
        String value = nameEditor.getValue();
        if (CtuluLibString.isEmpty(value)) {
          setErrorText(TrLib.getString("Choisir un nom de traceur non vide"));
          return false;
        }
        return true;

      }
    };
    pn.setLayout(new BuGridLayout(2));
    pn.addLabel(tracerNameDicoEntite.getNom());
    pn.add(nameEditor.getComponent());
    pn.addLabel(initialesValuesDicoEntite.getNom());
    pn.add(initialesValuesEditor.getComponent());
    if (pn.afficheModaleOk(null, getTitle())) {
      String initValue = initialesValuesEditor.getValue();
      source.addTracer(nameEditor.getValue(), initValue, cmdManager);
    }

  }
}
