package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuDialogMessage;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.actions.TrPostActionChooseAndCreateCalque;

/**
 * Classe qui gere le panel de configuration de fusion du projet actuel avec un nouveau projet.
 * CLASSE NON UTILISEE
 * @author Adrien Hadoux
 *
 */
public class TrPostFusionProjet extends BuDialogMessage{

	TrPostProjet projet_;
	
	public TrPostFusionProjet(TrPostProjet _projet) {
		super(_projet.impl_.getApp(), _projet.impl_.getInformationsSoftware(), TrResource
		        .getS("Ouvrir un projet"));
		// TODO Auto-generated constructor stub
		projet_=_projet;
		content_ = constructPanelOuvrir();
	    setContentPane(content_);
	    setSize(550, 250);
	    setModal(true);
	    setTitle(TrResource.getS("Ouvrir un projet"));
	    // setLocation(widgetGraphe_.getLocation());
	    activate();
		
	}

	//-- true si le projet a charger est de type post --//
	boolean typePost_=true;
	
	//-- indique si l'on conserve le precedent projet --//
	boolean conservPreviousProject_=false;
	
	
	/**
	 * Construit le panel d'ouverture du projet
	 * @return
	 */
	JComponent constructPanelOuvrir() {
	
		
		final JLabel labelPost=new JLabel("");
		final JLabel labelSource=new JLabel("");
		
		final JPanel content = new JPanel(new BorderLayout());
	   // content.add(new JLabel(TrResource.getS("Ouvrir un projet")), BorderLayout.NORTH);
	    JPanel container=new JPanel(new GridLayout(2,1));
	    content.add(container,BorderLayout.CENTER);
	   
	    //-- panel choix ouverture projet --//
	    final JPanel content1 = new JPanel(new GridLayout(2,1));
	    content1.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Type de projet")));
	    
	    ButtonGroup group = new ButtonGroup();
	    JRadioButton postButton = new JRadioButton(TrResource.getS("Projet POST"));
	    postButton.setSelected(true);
	    postButton.addActionListener(new ActionListener() {
      @Override
		      public void actionPerformed(final ActionEvent _e) {
		    	  typePost_=true;
		      }  });
	    JRadioButton sourceButton = new JRadioButton(TrResource.getS("Fichier r�sultat"));
	    sourceButton.setSelected(false);
	    sourceButton.addActionListener(new ActionListener() {
      @Override
		      public void actionPerformed(final ActionEvent _e) {
		    	  typePost_=false;
		      }  });
	    group.add(postButton);
	    group.add(sourceButton);
	  
	    
	    JPanel ligne1=new JPanel(new FlowLayout(FlowLayout.LEFT)); 
	    ligne1.add(postButton);
	    ligne1.add(labelPost);
	    
	    JPanel ligne2=new JPanel(new FlowLayout(FlowLayout.LEFT)); 
	    ligne2.add(sourceButton);
	    ligne2.add(labelSource);
	    content1.add(ligne1);
	    content1.add(ligne2);
	    
	    
	    final JPanel content2 =new JPanel(new GridLayout(3,1));
	    content2.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Propri�t�s")));
	    
	   final JCheckBox conservPrevious=new JCheckBox(TrResource.getS("Conserver le projet pr�c�dent ?"));
	    conservPrevious.setSelected(conservPreviousProject_);
	    conservPrevious.addActionListener(new ActionListener() {
      @Override
		      public void actionPerformed(final ActionEvent _e) {
		    	 if(conservPrevious.isSelected())
		    	  conservPreviousProject_=true;
		    	 else
		    		 conservPreviousProject_=false;
		      }  });
	    content2.add(conservPrevious);
	    content2.add(new JLabel(TrResource.getS("Dans le cas d'une ouverture d'un fichier r�sultat, le nom du projet sera demand� � la sauvegarde.")));
	    content2.add(new JLabel(TrResource.getS("Dans le cas d'une ouverture d'un fichier POST, le pr�c�dent projet sera sauv� avec celui-ci � la sauvegarde.")));
	    
	    
	    container.add(content1);
	    container.add(content2);
	    
	    
	    
	    final JButton valide = new JButton(TrResource.getS("Valider"), EbliResource.EBLI
	        .getIcon("crystal_valider"));
	    valide.addActionListener(new ActionListener() {
      @Override
	      public void actionPerformed(final ActionEvent _e) {
	    	  dispose();
	        //-- realisation des operations --//
	    	  if(!conservPreviousProject_)
	    		  projet_.getManager().clearProject();
	    	  if(typePost_){
	    		  projet_.getManager().loadProject(false, null, false);
	    	  }else{
	    		  
	    		  //-- verification qu'il existe une fenetre layout dispo sinon creation --//
	    		  if(projet_.getImpl().getCurrentLayoutFille()==null){
	    			  projet_.createNewLayoutFrame(); 
	    		  }
	    		  //-- ouverture d'un fichier r�sultat --//
	    		  TrPostActionChooseAndCreateCalque chooserSource=  new TrPostActionChooseAndCreateCalque(projet_,projet_.getImpl().getCurrentLayoutFille().controller_);
	    		  chooserSource.actionPerformed(null);
	    		  
	    		  
	    		  
	    	  }
	    	
	      } });
	    
	    final JButton annule = new JButton(TrResource.getS("Annuler"), EbliResource.EBLI
		        .getIcon("crystal_annuler"));
		    annule.addActionListener(new ActionListener() {
      @Override
		      public void actionPerformed(final ActionEvent _e) {
		        //--annulation --//
		    	  dispose();
		      }

		    });
	    final JPanel operations = new JPanel(new FlowLayout(FlowLayout.CENTER));
	    operations.add(valide);
	    operations.add(annule);
	    content.add(operations, BorderLayout.SOUTH);

	    return content;
	  }
	

	
	
}
