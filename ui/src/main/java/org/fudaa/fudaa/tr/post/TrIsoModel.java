/**
 * @creation 26 mars 2004 @modification $Date: 2007-05-22 14:20:37 $ @license GNU General Public License 2 @copyright (c)1998-2001
 * CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.gui.CtuluComboBoxModelAdapter;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.model.MvInfoDelegate;

/**
 * @author Fred Deniger
 * @version $Id: TrIsoModel.java,v 1.35 2007-05-22 14:20:37 deniger Exp $
 */
public class TrIsoModel extends TrIsoModelAbstract {

  ListModel timeModel_;
  int tIdx_;

  @Override
  public void fillInterpolateInfo(final InfoData _m, final int _element, final double _x, final double _y,
                                  final String _layerTitle) {

    if (oldData_ != null && oldData_.isElementData() && _element >= 0) {
      _m.put(MvResource.getS("Aire de l'�l�ment"), CtuluLib.DEFAULT_NUMBER_FORMAT.format(s_.getGrid().getAire(_element)));
    }
    s_.fillInterpolateInfo(_m, _element, _x, _y, tIdx_, _layerTitle);
  }

  /**
   * @param _s la source
   * @param _d le delegue pour les infos
   */
  public TrIsoModel(final TrPostSource _s, final MvInfoDelegate _d) {
    super(_s, _s.getVarListModel(), _d);
    timeModel_ = s_.getTimeListModel();
    if (timeModel_.getSize() > 0) {
      tIdx_ = timeModel_.getSize() - 1;
    }
  }

  @Override
  public CtuluRange getPaletteExtremaForSelectedValue() {
    return getPaletteExtremaForSelectedValue(null);
  }

  @Override
  public CtuluRange getPaletteExtremaForSelectedValue(final CtuluRange _r) {
    if (var_ == null) {
      return null;
    }
    final CtuluRange r = s_.getExtrema(_r, var_, getFilterForCurrentVar(), null);
    r.min_ = updateMinValue(r.min_);
    r.max_ = updateMaxValue(r.max_);
    return r;
  }

  /**
   * @return les variables r�elles uniquement
   */
  public H2dVariableType[] getAllVariableNonVec() {
    return s_.getAllVariablesNonVec();
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    final BuTable r = new CtuluTable();
    final H2dVariableType[] vars = s_.getAvailableVar();

    final TrPostValueTableModel.DefaultTimeModel model = new TrPostValueTableModel.DefaultTimeModel(super.s_, vars,
                                                                                                    oldData_ == null
                                                                                                    ? s_.isElementVar(var_) : oldData_.isElementData(),
                                                                                                    tIdx_);
    if (oldData_ != null && var_ != null) {
      model.setInCache(var_, oldData_);
    }
    final CtuluComboBoxModelAdapter cbModel = new CtuluComboBoxModelAdapter(s_.getTimeListModel());
    final BuComboBox cb = new BuComboBox();
    cb.setModel(cbModel);
    if (cbModel.getSize() > 0 && tIdx_ >= 0 && tIdx_ < cbModel.getSize()) {
      cb.setSelectedIndex(tIdx_);
    }
    cbModel.addListDataListener(new ListDataListener() {

      @Override
      public void intervalRemoved(final ListDataEvent _e) {
      }

      @Override
      public void intervalAdded(final ListDataEvent _e) {
      }

      @Override
      public void contentsChanged(final ListDataEvent _e) {
        model.setTime(cb.getSelectedIndex());
      }
    });
    // model.addTableModelListener(r);
    EbliTableInfoPanel.setComponent(r, cb);

    r.setModel(model);

    if (vars != null) {
      final TableColumnModel colModel = r.getColumnModel();
      final int idx = CtuluLibArray.findObject(vars, var_) + 3;
      final int nb = model.getColumnCount();
      final List cols = new ArrayList();
      for (int i = nb - 1; i >= 3; i--) {
        if (i != idx) {
          cols.add(colModel.getColumn(i));
        }
      }
      for (int i = cols.size() - 1; i >= 0; i--) {
        colModel.removeColumn((TableColumn) cols.get(i));
      }

    }
    return r;
  }

  @Override
  public void fillWithInfo(final InfoData _m, final ZCalqueAffichageDonneesInterface _layer) {
    final CtuluListSelectionInterface slct = _layer.getLayerSelection();
    int ptIdx = -1;
    if ((slct != null) && (slct.isOnlyOnIndexSelected())) {
      ptIdx = slct.getMaxIndex();
    }
    if (isCurrentDataElementType()) {
      delegate_.fillWithElementInfo(_m, slct, _layer.getTitle());
      if (ptIdx >= 0) {
        _m.put(MvResource.getS("Aire de l'�l�ment"), CtuluLib.DEFAULT_NUMBER_FORMAT.format(s_.getGrid().getAire(ptIdx)));
      }
    } else {
      delegate_.fillWithPointInfo(_m, slct, _layer.getTitle());
    }

    if (oldData_ != null && ptIdx >= 0) {
      ((TrPostInfoDelegate) delegate_).fillInfoWithVar(ptIdx, var_, tIdx_, _m, oldData_.getValue(ptIdx));
    }
  }

  public CtuluRange getPaletteExtremaForSelectedValueAndTimeStep() {
    return getPaletteExtremaForSelectedValueAndTimeStep(null);
  }

  /**
   * @return la valeur max pour la valeur selectionnee
   */
  public CtuluRange getPaletteExtremaForSelectedValueAndTimeStep(final CtuluRange _r) {
    final CtuluRange r = s_.getExtremaForTimeStep(_r, var_, tIdx_, getFilterForCurrentVar());
    if (r == null) {
      return new CtuluRange();
    }
    r.max_ = updateMaxValue(r.max_);
    r.min_ = updateMinValue(r.min_);
    return r;
  }

  @Override
  public boolean getDataRange(final CtuluRange _r) {
    getPaletteExtremaForSelectedValue(_r);
    return true;
  }

  public boolean getTimeDataRange(final CtuluRange _r) {
    if (tIdx_ < 0 || var_ == null) {
      return false;
    }
    getPaletteExtremaForSelectedValueAndTimeStep(_r);
    if (FuLog.isTrace()) {
      FuLog.trace("Time range " + var_.getName() + " t= " + tIdx_ + " res=" + _r.toString());
    }
    return true;
  }

  public boolean isTimeSelectedWrong() {
    return tIdx_ < 0 || tIdx_ >= getTimeList().getSize();
  }

  public String getTimeSelected() {
    if (isTimeSelectedWrong()) {
      return CtuluLibString.ESPACE;
    }
    return (String) getTimeList().getElementAt(tIdx_);
  }

  public int getTimeStep() {
    return tIdx_;
  }

  public void setT(final int _i) {
    if (_i < 0 || _i >= getTimeList().getSize()) {
      tIdx_ = -1;
      oldData_ = null;
      return;
    }
    if (_i != tIdx_) {
      tIdx_ = _i;
      oldData_ = s_.getData(var_, tIdx_);
      if (cond_ != null) {
        cond_.updateTimeStep(tIdx_, s_);
      }
    }
  }

  @Override
  public void restoreFilter(final EbliUIProperties _res) {
    if (_res != null) {
      final Object o = _res.get("iso.filter");
      if (o != null) {
        getCond().restoreFromSaver(o, s_);
        if (cond_ != null && tIdx_ >= 0 && s_ != null) {
          cond_.updateTimeStep(tIdx_, s_);
        }
      }
    }
  }

  @Override
  public TrPostFilterLayer getCond() {
    if (cond_ == null) {
      cond_ = new TrPostFilterLayer(s_);
      cond_.updateTimeStep(tIdx_, s_);
    }
    return cond_;
  }

  @Override
  public void setVar(final int _idx) {
    if (_idx < 0) {
      var_ = null;
      oldData_ = null;
    } else {
      final H2dVariableType t = (H2dVariableType) getVariableList().getElementAt(_idx);
      if (oldData_ == null || t != var_) {
        var_ = t;
        updateCurrentValueCache();
        if (oldData_ == null && _idx >= 0) {
          setVar(-1);
        }
      }
    }
  }

  @Override
  public void updateCurrentValueCache() {
    oldData_ = s_.getData(var_, tIdx_);
    if (oldData_ == null) {
      setVar(-1);
    }
  }

  @Override
  public double getValueFor(final H2dVariableType _var, final int _idx) {
    if (_var == var_) {
      return oldData_.getValue(_idx);
    }
    try {
      return s_.getData(_var, tIdx_, _idx);
    } catch (final IOException _e) {
      FuLog.warning(_e);
    }
    return -1;
  }

  public String getT(final int _i) {
    return (String) timeModel_.getElementAt(_i);
  }

  public double getTInSec(final int _i) {
    return s_.getTimeStep(_i);
  }

  public ListModel getTimeList() {
    return timeModel_;
  }

  public int getTimeStepNb() {
    return s_.getNbTimeStep();
  }
}