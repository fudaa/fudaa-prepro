/*
 * @creation 16 d�c. 2004
 * @modification $Date: 2007-06-20 12:23:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.reflux.io.RefluxSolutionSequentielReader;

import java.io.File;
import java.io.IOException;

/**
 * Un lecteur pour reflux et refonde.
 *
 * @author Fred Deniger
 * @version $Id: TrPostSourceReflux.java,v 1.17 2007-06-20 12:23:39 deniger Exp $
 */
public class TrPostSourceReaderReflux extends TrPostSourceReaderFromFileAbstract {
    final int[] cols_;
    RefluxSolutionSequentielReader reader_;
    //  int timeStep_ = -1;
    String id = "";

    public TrPostSourceReaderReflux(final File _file, final EfGridInterface _g, final double[] _time,
                                    final H2dVariableType[] _v, final int[] _cols, final RefluxSolutionSequentielReader _reader) {
        super(_file, _time, _v);
        reader_ = _reader;
        cols_ = _cols;
    }

    @Override
    public boolean isVolumique() {
        return false;
    }

    @Override
    public void close() {
        try {
            reader_.close();
        } catch (final IOException _e) {
            FuLog.warning(_e);
        }
    }

    @Override
    public EfData getInitData(final int _varIdx, final int _timeIdx) {
        double[][] values = new double[cols_.length][reader_.getNbPoint()];
        try {
            reader_.read(cols_, _timeIdx, values);
        } catch (final IOException e) {
            // if (!isInspected()) {
            // impl_.error(CtuluUIAbstract.getDefaultErrorTitle(), CtuluLib.getS("Le fichier {0} est corrompu", getFile()
            // .getName()), false);
            // }
            // FuLog.warning(e);
            if (Fu.DEBUG && FuLog.isDebug()) {
                FuLog.debug("FTR:  read error " + e.getMessage());
            }
        }
        return new EfDataNode(values[_varIdx]);
    }

    @Override
    public double getInitData(final int _varIdx, final int _timeIdx, final int _ptIdx) {
        try {
            return reader_.read(cols_[_varIdx], _timeIdx, _ptIdx);
        } catch (final IOException e) {
            // if (!isInspected()) {
            // impl_.error(CtuluUIAbstract.getDefaultErrorTitle(), e.getLocalizedMessage(), false);
            // }
            if (Fu.DEBUG && FuLog.isDebug()) {
                FuLog.debug("FTR:  read error " + e.getMessage());
                // FuLog.warning(e);
            }
        }
        return -1;
    }

    public final boolean isRubar() {
        return false;
    }

    @Override
    public void SetId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return this.id;
    }
}
