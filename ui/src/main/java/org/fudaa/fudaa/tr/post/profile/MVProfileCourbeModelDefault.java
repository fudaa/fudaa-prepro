/*
 * @creation 21 nov. 06
 * 
 * @modification $Date: 2007-06-13 12:58:09 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import org.locationtech.jts.geom.LineString;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.replay.TrReplayerProfilSpatial;

import java.awt.*;
import java.util.Map;

/**
 * A default courbe model that uses a MvProfileXYProvider.
 *
 * @author fred deniger
 * @version $Id: MVProfileCourbeModel.java,v 1.10 2007-06-13 12:58:09 deniger Exp $
 */
public abstract class MVProfileCourbeModelDefault implements MvProfileCourbeModelInterface {

  String title_;
  MvProfileXYProvider.Cached xyProvider;

  /**
   * ACHTUNG: Constructeur uniquement utilis� pour la persistance des donn�es.
   */
  public MVProfileCourbeModelDefault() {
    super();
  }

  @Override
  public boolean useSpecificIcon(int idx) {
    return false;
  }

  @Override
  public Color getSpecificColor(int idx) {
    return null;
  }

  @Override
  public boolean isGenerationSourceVisible() {
    return true;
  }

  @Override
  public boolean isReplayable() {
    return true;
  }

  @Override
  public final boolean addValue(final double _x, final double _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public final boolean addValue(final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public final boolean deplace(final int[] _selectIdx, final double _deltaX, final double _deltaY, final CtuluCommandContainer _cmd) {
    return false;
  }

  public final int getActiveTimeIdx() {
    return 0;
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {

    if (!(impl instanceof TrPostCommonImplementation)) {
      impl.error(TrResource.getS("Impossible de r�cup�rer la bonne interface fudaa"));
      return;
    }
    TrPostCommonImplementation implementation = (TrPostCommonImplementation) impl;

    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    new TrReplayerProfilSpatial((TrPostSource) getData(), implementation).displaySpatialProfilOrigin(this);

  }

  @Override
  public final LineString getLineString() {
    return xyProvider.getProfilelineString();
  }

  @Override
  public final int getNbValues() {
    return xyProvider.getNbData();
  }

  @Override
  public final String getTitle() {
    return title_;
  }

  @Override
  public final double getX(final int _idx) {
    return xyProvider.getX(_idx);
  }

  @Override
  public final double getXMax() {
    return xyProvider.getXMax();
  }

  @Override
  public final double getXMin() {
    return xyProvider.getXMin();
  }

  @Override
  public final double getY(final int _idx) {
    return xyProvider.getY(_idx);
  }

  @Override
  public final double getYMax() {
    return xyProvider.getYMax();
  }

  @Override
  public final double getYMin() {
    return xyProvider.getYMin();
  }

  public final boolean isActiveTimeEnable() {
    return false;
  }

  @Override
  public final boolean isPointDrawn(final int _i) {
    return xyProvider.isPointDrawn(_i);
  }

  @Override
  public final boolean isSegmentDrawn(final int _i) {
    return xyProvider.isSegmentDrawn(_i);
  }

  public final boolean isVisibleLong() {
    return false;
  }

  @Override
  public final boolean isXModifiable() {
    return false;
  }

  @Override
  public final boolean isModifiable() {
    return false;
  }

  @Override
  public final boolean removeValue(final int _i, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public final boolean removeValue(final int[] _i, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public final boolean setTitle(final String _newName) {
    title_ = _newName;
    return true;
  }

  @Override
  public final boolean setValue(final int _i, final double _x, final double _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public final boolean setValues(final int[] _idx, final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public final String getPointLabel(int i) {
    return null;
  }
}
