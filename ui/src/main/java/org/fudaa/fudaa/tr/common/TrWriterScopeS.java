package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuFileFilter;
import jxl.write.WriteException;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.table.CtuluTableModelInterface;
import org.fudaa.ctulu.table.CtuluWriter;
import org.fudaa.dodico.telemac.io.ScopeKeyWord;
import org.fudaa.dodico.telemac.io.ScopeSFileFormat;
import org.fudaa.dodico.telemac.io.ScopeStructure;
import org.fudaa.ebli.courbe.*;

import java.io.File;
import java.io.IOException;

/**
 * Writer, passe de la structure Eggraphe a la structure scopeS.
 * Writer goes from Eggraphe structure to scopeS structure
 *
 * @author Adrien Hadoux
 */
public class TrWriterScopeS implements CtuluWriter {
  private ScopeSFileFormat scopeSFileFormat;
  private File destination;
  private ScopeStructure.SorT structure;
  private String extension = ".scopS";
  BuFileFilter buFileFilter;

  public TrWriterScopeS() {
    scopeSFileFormat = ScopeSFileFormat.getInstance();
    buFileFilter = scopeSFileFormat.createFileFilter();
  }

  @Override
  public BuFileFilter getFilter() {
    return buFileFilter;
  }

  @Override
  public String getMostPopularExtension() {
    return extension;
  }

  @Override
  public void setFile(File _f) {
    destination = _f;
  }

  @Override
  public void setModel(CtuluTableModelInterface _model) {
    setModelVersionInterpoleY(_model);
  }

  /**
   * Cette version recupere l'union des X de toutes les courebs pour chaque
   * variable du graphe. Ecrit le y du x si x appartient a la courbe sinon ecrit
   * la valeur interpol�e de x
   * This version get back the union of X of the whole curves for each variable
   * of the graph. Written the y from the x if x belongs to the curve otherwise written
   * the value interpolled of x
   *
   * @param _model le modele
   */
  public void setModelVersionInterpoleY(CtuluTableModelInterface _model) {

    structure = new ScopeStructure.SorT();

    // -- data qui contient les courbes choisies --//
    //--data which have the selected curves
    EGExportData data = (EGExportData) _model;
    EGGraphe dataGraphe = data.getGraphe();

    // -- on prends UNIQUEMENT la premiere courbe choisie --//
    // --We only take the first curve selected
    EGCourbe[] listeChoixUser = new EGCourbe[1];
    listeChoixUser[0] = data.getCourbes()[0];

    // -- on ajoute la var des x, debut de chaque ligne de valeurs --//
    // -- we add the var of x, beginning of each line of value
    structure.addVariable("X");

    // --on ajoute tous les x donn�s de la courbe= UNION DE TOUTES LES
    // COURBES--//
    // -- We add all the x given from the curve = union from the whole curves
    addX(dataGraphe, listeChoixUser);

    // -- ON INSERE LES Y CORRESPONDANTS POUR CHAQUE VARIABLES--//
    // --WE INTEGRATE THE Y CORRESPONDING TO EACH VARIABLE--//
    for (int i = 0; i < dataGraphe.getModel().getNbEGObject(); i++) {
      final EGObject ci = dataGraphe.getModel().getEGObject(i);
      if (ci instanceof EGGroup) {
        EGGroup groupe = (EGGroup) ci;

        boolean hasAlreadyRegistered = false;

        for (int k = 0; k < groupe.getChildCount(); k++) {
          EGCourbeChild courbe = groupe.getCourbeAt(k);

          boolean selected = false;
          for (int j = 0; !selected && j < listeChoixUser.length; j++)
            if (listeChoixUser[j] == courbe) {
              selected = true;
            }

          if (selected) {

            // -- il y a au moins une courbe selectionnee pour la variable, on
            // va donc enregistrer la variable dans la structure
            //-- There is at least a curve selected for the variable
            //We are therefore going to record the variable in the structure
            if (!hasAlreadyRegistered) {
              hasAlreadyRegistered = true;
              structure.addVariable(groupe.getTitle());
            }

            // --ajout des valeurs pour chaque X--/
            for (int g = 0; g < structure.getAllX().size(); g++) {
              // -- on ajoute la valeur interpol�e pour les x pr�cemment saisis
              // --we add the interpolated value for the precedent x taken
              double x = structure.getX(g);


              double yInterpole = courbe.interpol(x);

              // -- si le x existe dans la courbe alors on inscrit son y
              // correspondant pour la bonne variable--//
              //--if the x exists in the curve then we write its y  corresponding for the good variable
              structure.addValueForVariable(yInterpole, structure
                  .getNbVariables() - 1);
            }
          }
        }
      }
    }
  }

  private void addX(EGGraphe dataGraphe, EGCourbe[] listeChoixUser) {
    final EGAxeHorizontal h = dataGraphe.getTransformer().getXAxe();
    for (int i = listeChoixUser.length - 1; i >= 0; i--) {
      final EGCourbe c = listeChoixUser[i];
      for (int j = c.getModel().getNbValues() - 1; j >= 0; j--) {
        final double x = c.getModel().getX(j);
        // -- on ajoute le x pour les variables x si il n existe pas deja --//
        // --we add the x for the variables x if there already exists
        structure.insertNewX(x);
        // }
      }
    }
  }

  /**
   * Cette version recupere l'union des X de toutes les courebs pour chaque
   * variable du graphe. Ecrit le y du x que si x de l'union appartient a la courbe
   * This version gets back the union of X of the whole curves for each variable
   * of the graph. Written the y from the x if x belongs to the curve otherwise written
   * the value interpolled of x
   * -- data qui contient les courbes choisies --//
   *
   * @param _model
   */
  public void setModelVersionUnionXcoubesY(CtuluTableModelInterface _model) {

    structure = new ScopeStructure.SorT();

    //-- data qui contient les courbes choisies --//
    //-- data  which  have the selected curves
    EGExportData data = (EGExportData) _model;
    EGGraphe graphe_ = data.getGraphe();
    EGCourbe[] listeChoixUser = data.getCourbes();

    //-- on ajoute la var des x, debut de chaque ligne de valeurs --//
    // --  We add the var of x , beginning from each line of values
    structure.addVariable("X");

    //--on ajoute tous les x donn�s de la courbe= UNION DE TOUTES LES COURBES--//
    //--We add all the x given from the curve = union from the whole curves
    addX(graphe_, listeChoixUser);

    //-- ON INSERE LES Y CORRESPONDANTS  POUR CHAQUE VARIABLES--//
    // -- We integrate the y corresponding for every varaible
    for (int i = 0; i < graphe_.getModel().getNbEGObject(); i++) {
      final EGObject ci = graphe_.getModel().getEGObject(i);
      if (ci instanceof EGGroup) {
        EGGroup groupe = (EGGroup) ci;

        boolean hasAlreadyRegistered = false;

        for (int k = 0; k < groupe.getChildCount(); k++) {
          EGCourbeChild courbe = groupe.getCourbeAt(k);

          boolean selected = false;
          for (int j = 0; !selected && j < listeChoixUser.length; j++)
            if (listeChoixUser[j] == courbe) {
              selected = true;
            }

          if (selected) {

            // -- il y a au moins une courbe selectionnee pour la variable, on
            // va donc enregistrer la variable dans la structure
            //--there is therefore at least a selected curve for the variable, we therefore record the variable in the structure
            if (!hasAlreadyRegistered) {
              hasAlreadyRegistered = true;
              structure.addVariable(groupe.getTitle());
            }

            // --ajout des valeurs pour chaque X--/
            //--add values for each x
            for (int g = 0; g < structure.getAllX().size(); g++) {
              //-- on ajoute la valeur interpol�e pour les x pr�cemment saisis --//
              //--we add the interpolated value for the precedent x selected
              double x = structure.getX(g);

              //-- on v�rifie que le x existe pour la courbe actuelle --//
              //--We check than te x exists for the present curve
              boolean xExistForCourbe = false;
              int emplacementYcorrespondant = -1;
              for (int l = 0; !xExistForCourbe && l < courbe.getModel().getNbValues(); l++) {
                if (courbe.getModel().getX(l) == x) {
                  xExistForCourbe = true;
                  emplacementYcorrespondant = l;
                }
              }

              // -- si le x existe dans la courbe alors on inscrit son y
              //-- if the x exists in the curve then we write its y
              // correspondant pour la bonne variable--//
              // corresponding for the good variable
              if (xExistForCourbe) {
                structure.addValueForVariable(courbe.getModel().getY(emplacementYcorrespondant), structure
                    .getNbVariables() - 1);
              } else
              // -- on enregistre une valeure foireuse.
              // -- We integrate a wrong value
              {
                structure.addValueForVariable(ScopeKeyWord.VALUE_UNDEFINED, structure.getNbVariables() - 1);
              }
            }
          }
        }
      }
    }
  }

  @Override
  public void write(ProgressionInterface _p) {
    scopeSFileFormat.getLastVersionInstance(null).write(destination, structure, null);
  }

  @Override
  public boolean allowFormatData() {
    return false;
  }
}
