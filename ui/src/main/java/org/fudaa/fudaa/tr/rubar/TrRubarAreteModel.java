/**
 *  @creation     10 juin 2004
 *  @modification $Date: 2006-11-24 09:32:45 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.rubar;

import java.util.List;
import org.fudaa.dodico.h2d.rubar.H2dRubarArete;
import org.fudaa.ebli.calque.ZModeleSegment;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarAreteModel.java,v 1.11 2006-11-24 09:32:45 deniger Exp $
 */
public interface TrRubarAreteModel extends ZModeleSegment {

  /**
   * @param _frIdx l'indice de l'ar�te sur la frontiere
   * @return l'indice global
   */
  int[] getGlobalIdx(int[] _selectedIdx);

  int getGlobalIdx(int _selectedIdx);

  /**
   * @param _idx l'indice de l'arete
   * @return l'arete d'indice _idx dans le meme ordre que les segments
   */
  H2dRubarArete getRubarArete(int _idx);

  double getXFromGlobalIdx(int _ptIdx);

  double getYFromGlobalIdx(int _ptIdx);
  

  /**
   * @return la liste des bords utilises
   */
  List getUsedBoundaryType();

}