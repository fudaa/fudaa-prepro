package org.fudaa.fudaa.tr.post;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinVolumeFileFormat;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.reflux.io.RefluxRefondeSolutionSequentielResult;
import org.fudaa.dodico.rubar.io.RubarFileFormatDefault;
import org.fudaa.fudaa.tr.common.TrFileFormatManager;

/**
 * TODO a revoir pour simplifier le tout.
 * 
 * @author deniger
 */
public class TrPostMultiSourceActivator2 {

  public TrPostProjet projetCourant_;

  public TrPostMultiSourceActivator2(final TrPostProjet _projet) {
    super();
    projetCourant_ = _projet;

  }

  /**
   * @param _s la source
   * @param _impl l'impl parente
   */
  protected static void afficheFrame(final TrPostProjet _p, final TrPostCommonImplementation _impl) {

    _impl.setProjet(_p);

  }

  /**
   * @param _f le fichier a ouvrir
   * @param _impl l'impl parent
   */
  public TrPostSource activeSerafin(final File _f, final TrPostCommonImplementation _impl, final String id,
      final boolean isVolume) {

    final TrPostSource source = activeSerafinAction(_f, _impl, _impl.getMainProgression(), isVolume);
    if (source == null) { return null; }
    source.setId(id);
    if (SerafinFileFormat.is3DGrid(source.getGrid())) {

      TrPostSourceBuilder.affiche3D(source, _impl);

    } else {
      afficheFrame(projetCourant_, _impl);
    }
    return source;

  }

  public TrPostSource activeGrid(final File _f, final FileFormatGridVersion _fmt, final TrPostCommonImplementation _impl,
      final String id) {

    TrPostSource activeGridAction = activeGridAction(_f, _fmt, _impl, _impl.getMainProgression(), id);
    if (projetCourant_ != null) {
      afficheFrame(projetCourant_, _impl);
    }
    return activeGridAction;

  }

  /**
   * @param _f liste de fichiers. Seulement dans le cas rubar, on essaie de charger les diff�rents fichiers si pr�sents.
   *          sinon, seul le premier est pris en compte.
   * @param _impl
   * @param iDSourceToActive
   */
  public void active(final List<File> _f, final TrPostCommonImplementation _impl, final String iDSourceToActive) {
    if (_f == null || _f.isEmpty()) { return; }

    final boolean isRubar = isRubar(_f);
    if (!isRubar) {
      final File first = _f.get(0);
      active(first, _impl, iDSourceToActive);
    } else {
      activeAndLoadFilesRubar(_f, _impl, iDSourceToActive);
    }

  }

  private boolean isRubar(final List<File> files) {
    if (files == null || files.isEmpty()) { return false; }
    final File first = files.get(0);
    final String name = first.getName();
    final TrFileFormatManager mng = TrFileFormatManager.INSTANCE;
    if (mng.isRubarTPSFile(name)) { return true; }
    final List<RubarFileFormatDefault> rubarFmt = new ArrayList<RubarFileFormatDefault>();
    rubarFmt.add(new RubarFileFormatDefault("TPC"));
    rubarFmt.add(new RubarFileFormatDefault("TPS"));
    rubarFmt.add(new RubarFileFormatDefault("ENV"));
    rubarFmt.add(new RubarFileFormatDefault("TRC"));
    rubarFmt.add(new RubarFileFormatDefault("HYC"));
    rubarFmt.add(new RubarFileFormatDefault("ZFN"));
    rubarFmt.add(new RubarFileFormatDefault("OUT"));
    rubarFmt.add(new RubarFileFormatDefault("MAS"));
    for (final File file : files) {
      final FileFormat ft = FileFormat.findFileFormat(rubarFmt, file);
      if (ft != null) { return true; }
    }

    return false;
  }

  /**
   * Ouvre le fichier en fonction de l'extension.
   * 
   * @param _f le fichier correct
   * @param _impl l'impl parent
   */
  public TrPostSource active(final File _f, final TrPostCommonImplementation _impl, final String IdSourceToActive) {
    if (_f == null) { return null; }
    final TrFileFormatManager mng = TrFileFormatManager.INSTANCE;
    final String name = _f.getName();
    if (TrPostSourceBuilder.isReflux(name)) { return activeINP(_f, _impl, IdSourceToActive); }
    if (mng.isRubarTPSFile(name)) { return activeRubar(_f, _impl, IdSourceToActive); }

    if (SerafinVolumeFileFormat.getInstance().createFileFilter().accept(_f)) { return activeSerafin(_f, _impl,
        IdSourceToActive, true); }
    // mettre serafin en dernier car le test est tres large ....
    if (SerafinFileFormat.getInstance().createFileFilter().accept(_f)) { return activeSerafin(_f, _impl,
        IdSourceToActive, false); }
    final FileFormat ft = FileFormat.findFileFormat(TrFileFormatManager.getAllGridFormat(), _f);
    if (ft == null) {
      final String id = _impl.getLauncher().getCurrentPrefHydId();
      if (FileFormatSoftware.REFLUX_IS.name.equals(id)) {
        return activeINP(_f, _impl, IdSourceToActive);
      } else if (FileFormatSoftware.TELEMAC_IS.name.equals(id)) {
        return activeSerafin(_f, _impl, IdSourceToActive, false);
      } else if (FileFormatSoftware.RUBAR_IS.name.equals(id)) { return activeRubar(_f, _impl, IdSourceToActive); }
    } else {
      return activeGrid(_f, (FileFormatGridVersion) ft, _impl, IdSourceToActive);
    }
    return null;
  }

  /**
   * Ouvir une boite de dialogue pour determiner le fichier inp et le fichier sov.
   * 
   * @param _f le fichier xinp,sov ou inp
   * @param _impl l'impl parent
   */
  public TrPostSource activeINP(final File _f, final TrPostCommonImplementation _impl, final String id) {
    if (RefluxRefondeSolutionSequentielResult.isRefonde(_f)) {
      final File inp = TrPostSourceBuilder.getInpFileForRefonde(_f, _impl);
      if (inp != null) { return activeINP(inp, _f, _impl, id); }
      return null;
    }
    final TrPostSourceBuilder.INPChooseFile pn = new TrPostSourceBuilder.INPChooseFile(_f);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_impl.getFrame()))) { return activeINP(pn.getInp(), pn.getsov(),
        _impl, id); }
    return null;
  }

  /**
   * @param _f le fichier tps
   * @param _impl l'impl parente
   */
  public TrPostSource activeRubar(final File _f, final TrPostCommonImplementation _impl, final String id) {

    final ProgressionInterface prog = _impl.getMainProgression();
    final TrPostSourceRubar src = activeRubarAction(_f, _impl, prog);
    if (src != null) {
      src.setId(id);
    }
    final TrPostRubarLoader loader = new TrPostRubarLoader(src, _f, true);
    loader.active(projetCourant_, _impl, prog);
    afficheFrame(projetCourant_, _impl);
    return src;

  }

  /**
   * @param _f le fichier tps
   * @param _impl l'impl parente
   */
  public void activeAndLoadFilesRubar(final List<File> _f, final TrPostCommonImplementation _impl, final String id) {

    final ProgressionInterface prog = _impl.getMainProgression();
    final File first = _f.get(0);
    final TrPostSourceRubar src = activeRubarAction(first, _impl, prog);
    if (src != null) {
      src.setId(id);
    }
    final TrPostRubarLoader loader = new TrPostRubarLoader(src, first, true);
    loader.setLoadNone();
    for (final File file : _f) {
      loader.setToLoad(file);
    }
    loader.loadFiles(prog, _impl);
    afficheFrame(projetCourant_, _impl);

  }

  /**
   * @param _f le fichier inp
   * @param _sov le fichier sov
   * @param _impl l'impl parente
   */
  public TrPostSource activeINP(final File _f, final File _sov, final TrPostCommonImplementation _impl, final String id) {

    TrPostSource activeINPAction = activeINPAction(_f, _sov, _impl, _impl.getMainProgression(), id);
    if (projetCourant_ != null) {
      afficheFrame(projetCourant_, _impl);
    }
    return activeINPAction;

  }

  /**
   * @param _f le fichier serafin
   * @param _impl l'impl parent
   * @param _inter la barre de progress
   * @return le source post
   */
  public TrPostSource activeSerafinAction(final File _f, final TrPostCommonImplementation _impl,
      final ProgressionInterface _inter, final boolean isVolume) {
    final TrPostSource r = TrPostSourceBuilder.activeSerafinSourceAction(_f, _impl, _inter, null, isVolume);
    if (r != null) {

      // ajout de la source au postProjet
      this.projetCourant_.addSource(r);
    }
    return r;
  }

  /**
   * @param _f le fichier serafin
   * @param _impl l'impl parent
   * @param _inter la barre de progress
   * @return le source post
   */
  public TrPostSourceRubar activeRubarAction(final File _f, final TrPostCommonImplementation _impl,
      final ProgressionInterface _inter) {
    final TrPostSourceRubar src = TrPostSourceBuilder.activeRubarSrcAction(_f, _impl, _inter);
    if (src == null) { return null; }

    this.projetCourant_.addSource(src);

    return src;
  }

  public static void loadData(final CtuluUI _impl, final ProgressionInterface _inter, final TrPostProjet _proj) {
    if (_proj != null && _proj.openSrcDataAndIsModified(_impl, _inter)) {
      _proj.setProjectModified();
    }
  }

  /**
   * @param _gridFile le fichier inp ou ximp
   * @param _sovFile le fichier contenant les solutions
   * @param _impl l'impl parent
   * @param _inter la barre de progression
   * @return le source
   */
  public TrPostSource activeINPAction(final File _gridFile, final File _sovFile, final CtuluUI _impl,
      final ProgressionInterface _inter, final String id) {

    final TrPostSourceReflux retour = (TrPostSourceReflux) TrPostSourceBuilder.activeINPSource(_gridFile, _sovFile,
        _impl, _inter);
    if (retour == null) { return null; }
    retour.setId(id);
    retour.buildDefaultVarUpdateLists();

    projetCourant_.addSource(retour);

    loadData(_impl, _inter, projetCourant_);
    return retour;
  }

  public TrPostSource activeGridAction(final File _f, final FileFormatGridVersion _ftGrid,
      final TrPostCommonImplementation _impl, final ProgressionInterface _inter, final String id) {

    final TrPostSource retour = TrPostSourceBuilder.activeGridSource(_f, _ftGrid, _impl, _inter);
    if (retour == null) { return retour; }
    retour.setId(id);
    this.projetCourant_.addSource(retour);

    loadData(_impl, _inter, projetCourant_);
    return retour ;
  }

}
