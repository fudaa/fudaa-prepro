package org.fudaa.fudaa.tr.post;

import org.locationtech.jts.geom.Coordinate;
import gnu.trove.TIntObjectHashMap;
import java.io.IOException;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * Cette table ne doit etre utilisee que pour un affiche modal.
 *
 * @author Fred Deniger
 * @version $Id: TrPostValueTableModel.java,v 1.9 2007-06-11 13:08:18 deniger Exp $
 */
public abstract class TrPostValueTableModel extends AbstractTableModel {

  private final EfGridInterface abstract_;
  private final TIntObjectHashMap cache_;
  final CtuluVariable[] var_;
  final boolean element_;
  int idx_;
  Coordinate pt_;

  TrPostValueTableModel(final EfGridInterface _abstract, final CtuluVariable[] _var, final boolean _element) {
    abstract_ = _abstract;
    var_ = _var;
    cache_ = var_ == null ? null : new TIntObjectHashMap(var_.length);
    element_ = _element;
  }

  public void setInCache(final Object _idx, final EfData _d) {
    final int idx = CtuluLibArray.findObject(var_, _idx);
    if (idx >= 0) {
      cache_.put(idx, _d);
    }
  }

  protected void clearCache() {
    cache_.clear();
  }

  @Override
  public Class getColumnClass(final int _columnIndex) {
    return _columnIndex == 0 ? Integer.class : Double.class;
  }

  public abstract EfData getData(int _idx);

  @Override
  public int getColumnCount() {
    return var_ == null ? 3 : 3 + var_.length;
  }

  @Override
  public String getColumnName(final int _column) {
    if (_column == 0) {
      return MvResource.getS("Indices");
    }
    if (_column == 1) {
      return element_ ? MvResource.getS("Centre de l'�lement") + ": X" : "X";
    } else if (_column == 2) {
      return element_ ? MvResource.getS("Centre de l'�lement") + ": Y" : "Y";
    } else if (var_ != null) {
      return var_[_column - 3].toString();
    }
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public int getRowCount() {
    return element_ ? abstract_.getEltNb() : abstract_.getPtsNb();
  }

  @Override
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {
    if (_columnIndex == 0) {
      return Integer.valueOf(_rowIndex + 1);
    }
    // X
    if (_columnIndex == 1) {
      if (element_) {
        if (pt_ == null) {
          pt_ = new Coordinate();
        }
        final EfElement e = abstract_.getElement(_rowIndex);
        e.getMilieuXY(abstract_, pt_);
        return CtuluLib.getDouble(pt_.x);
      }
      return CtuluLib.getDouble(abstract_.getPtX(_rowIndex));
    } // Y
    else if (_columnIndex == 2) {
      if (element_) {
        if (pt_ == null) {
          pt_ = new Coordinate();
        }
        final EfElement e = abstract_.getElement(_rowIndex);
        e.getMilieuXY(abstract_, pt_);
        return CtuluLib.getDouble(pt_.y);
      }
      return CtuluLib.getDouble(abstract_.getPtY(_rowIndex));
    }
    final int varIdx = _columnIndex - 3;
    EfData d = (EfData) cache_.get(varIdx);
    if (d == null) {
      d = getData(varIdx);
      if (!d.isElementData() && element_) {
        d = EfLib.getElementDataDanger(d, abstract_);
      } else if (!element_ && d.isElementData()) {
        abstract_.computeNeighbord(null, null);
        d = abstract_.getNeighbors().getDataNodeSimple(d);
      }
      cache_.put(varIdx, d);
    }
    if (d != null) {
      return CtuluLib.getDouble(d.getValue(_rowIndex));
    }
    return CtuluLibString.EMPTY_STRING;
  }

  /**
   * @author Fred Deniger
   * @version $Id: TrPostValueTableModel.java,v 1.9 2007-06-11 13:08:18 deniger Exp $
   */
  public static class DefaultTimeModel extends TrPostValueTableModel {

    int t_;
    EfGridData data_;

    public DefaultTimeModel(final EfGridData _abstract, final CtuluVariable[] _var, final boolean _element, final int _t) {
      super(_abstract.getGrid(), _var, _element);
      t_ = _t;
      data_ = _abstract;
    }

    public void setTime(final int _i) {
      if (t_ != _i) {
        t_ = _i;
        clearCache();
        fireTableDataChanged();
      }

    }

    @Override
    public EfData getData(final int _idx) {
      try {
        return data_.getData(var_[_idx], t_);
      } catch (final IOException e) {
        return null;
      }
    }
  }
}
