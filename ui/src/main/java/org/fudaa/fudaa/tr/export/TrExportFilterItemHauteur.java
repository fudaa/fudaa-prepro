/*
 * @creation 25 avr. 07
 * 
 * @modification $Date: 2007-04-30 14:22:44 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.export;

import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.operation.EfOperation;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.meshviewer.export.MvExportOperationBuilderSelection;
import org.fudaa.fudaa.meshviewer.export.MvExportSourceFilterActivity;
import org.fudaa.fudaa.tr.post.TrPostFilterHauteur;
import org.fudaa.fudaa.tr.post.TrPostFilterHauteurElement;

/**
 * @author fred deniger
 * @version $Id: TrExportFilterItemHauteur.java,v 1.2 2007-04-30 14:22:44 deniger Exp $
 */
public class TrExportFilterItemHauteur extends TrExportFilterItemHauteurAbstract {
  final EfGridData src_;

  public TrExportFilterItemHauteur(final EfGridData _src) {
    super();
    src_ = _src;
  }

  @Override
  public MvExportOperationBuilderSelection getBuilder(final EfGridData _src) {
    TrPostFilterHauteur res = null;
    if (_src.isElementVar(H2dVariableType.HAUTEUR_EAU)) {
      res = new TrPostFilterHauteurElement(getEps());
    } else {
      res = new TrPostFilterHauteur(getEps());
    }
    return new DefaultBuilder(res, _src);
  }

  private double getEps() {
    return ((Double) tfHauteur_.getValue()).doubleValue();
  }

  @Override
  public boolean isEnable() {
    return src_ != null && src_.isDefined(H2dVariableType.HAUTEUR_EAU);
  }

  private static class DefaultBuilder implements MvExportOperationBuilderSelection {

    final TrPostFilterHauteur filtre_;
    final EfGridData data_;

    public DefaultBuilder(final TrPostFilterHauteur _filtre, final EfGridData _data) {
      super();
      filtre_ = _filtre;
      data_ = _data;
    }

    @Override
    public CtuluListSelectionInterface getSelectedMeshes() {
      return EfLib.getSelectedElt(filtre_, data_.getGrid());
    }

    @Override
    public void stop() {}

    @Override
    public void buildOperation(final ProgressionInterface _progression) {}

    @Override
    public EfOperation getOperation(final int _tidx) {
      filtre_.updateTimeStep(_tidx, data_);
      return new MvExportSourceFilterActivity(data_, filtre_);
    }

  }

}
