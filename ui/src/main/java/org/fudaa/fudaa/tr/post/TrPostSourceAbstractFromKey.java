/*
 * @creation 24 mars 2004
 *
 * @modification $Date: 2007-06-28 09:28:18 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.helper.LRUCache;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.data.TrPostKey;
import org.fudaa.fudaa.tr.post.data.TrPostKeyTime;

import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: TrPostSourceAbstract.java,v 1.52 2007-06-28 09:28:18 deniger Exp $
 */
public abstract class TrPostSourceAbstractFromKey extends TrPostSourceAbstract {
  protected final Map<H2dVariableType, TrPostKey> keyByVariables = new HashMap<>();

  protected TrPostSourceAbstractFromKey(final String _titre, final EfGridInterface _g, final double[] _time,
                                        final CtuluUI _impl) {
    super(_titre, _g, _time, _impl);
  }

  @Override
  public void activate() {
  }

  public abstract EfData getInitData(TrPostKey trPostKey, int timeIdx);

  public abstract double getInitData(TrPostKey trPostKey, int timeIdx, int ptIdx) throws IOException;

  protected final TrPostKey getInitDataIndex(final H2dVariableType _t) {
    if (keyByVariables != null && _t != null && keyByVariables.containsKey(_t)) {
      return keyByVariables.get(_t);
    }
    return null;
  }

  protected void clearCaches() {

  }

  @Override
  protected final void setInitVar(final H2dVariableType[] _v) {
    throw new IllegalAccessError("not supported");
  }

  public final H2dVariableType getVariableWithInitIdx(final TrPostKey key) {
    for (Map.Entry<H2dVariableType, TrPostKey> h2dVariableTypeTrPostKeyEntry : keyByVariables.entrySet()) {
      if (h2dVariableTypeTrPostKeyEntry.getValue().equals(key)) {
        return h2dVariableTypeTrPostKeyEntry.getKey();
      }
    }
    return null;
  }

  @Override
  public double getInitData(H2dVariableType _varIdx, int _timeIdx, int _ptIdx) throws IOException {
    final TrPostKey init = getInitDataIndex(_varIdx);
    if (init != null) {
      return getInitData(init, _timeIdx, _ptIdx);
    }
    return 0;
  }

  private final Map<TrPostKeyTime, SoftReference> dataCacheMap = Collections.synchronizedMap(new LRUCache<>(8));

  @Override
  public EfData getInitData(H2dVariableType _varIdx, int _timeIdx) {
    final TrPostKey key = getInitDataIndex(_varIdx);
    if (key != null) {
      TrPostKeyTime keyTime = new TrPostKeyTime(key, _timeIdx);
      final SoftReference softReference = dataCacheMap.get(keyTime);
      if (softReference != null && softReference.get() != null) {
        return (EfData) softReference.get();
      }
      final EfData initData = getInitData(key, _timeIdx);
      dataCacheMap.put(keyTime, new SoftReference<>(initData));
      return initData;
    }
    return null;
  }
}
