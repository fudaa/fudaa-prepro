/**
 * @creation 9 juin 2004
 * @modification $Date: 2007-04-30 14:22:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.H2dParameters;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.rubar.io.RubarPARFileFormat;
import org.fudaa.fudaa.tr.common.TrCourbeTemporelleManager;
import org.fudaa.fudaa.tr.common.TrImplementationEditorAbstract;
import org.fudaa.fudaa.tr.common.TrParametres;

import java.awt.*;
import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarParameters.java,v 1.21 2007-04-30 14:22:39 deniger Exp $
 */
public class TrRubarParameters implements TrParametres {
  private TrRubarEvolutionManager courbeMng_;
  File dir_;
  TrImplementationEditorAbstract impl_;
  private final H2dRubarParameters params_;
  private TrRubarTarageCourbesManager tarageMng_;
  String title_;

  TrRubarParameters(final H2dRubarParameters _p, final String _title, final File _dir,
                    final TrImplementationEditorAbstract _impl) {
    super();
    params_ = _p;
    title_ = _title;
    dir_ = _dir;
    impl_ = _impl;
  }

  /**
   * @return la fenetre parente
   */
  public Frame getFrame() {
    return impl_.getFrame();
  }

  /**
   * @return le manager des evolutions
   */
  @Override
  public TrCourbeTemporelleManager getEvolMng() {
    if (courbeMng_ == null) {
      courbeMng_ = new TrRubarEvolutionManager(getH2dParametres());
    }
    return courbeMng_;
  }

  protected void reinitTarageMng() {
    if (tarageMng_ != null) {
      tarageMng_.removeUsedEvolution();
    }
  }

  protected void reinitEvolMng() {
    if (courbeMng_ != null) {
      courbeMng_.removeUsedCourbe();
    }
  }

  protected void clearAll() {
    reinitEvolMng();
    reinitTarageMng();
  }

  protected void initAll() {
    if (tarageMng_ != null) {
      tarageMng_.initWithCurves(TrRubarTarageCourbesManager.getAllTarage(getH2dRubarParametres()));
    }
    if (courbeMng_ != null) {
      courbeMng_.initWith(getH2dRubarParametres());
    }
  }

  /**
   * @return le manager des evolutions
   */
  public TrRubarTarageCourbesManager getTarageMng() {
    if (tarageMng_ == null) {
      tarageMng_ = new TrRubarTarageCourbesManager(getH2dRubarParametres());
    }
    return tarageMng_;
  }

  /**
   * @return l'impl parent
   */
  public TrImplementationEditorAbstract getImpl() {
    return impl_;
  }

  /**
   * @return titre du projet
   */
  public String getTitle() {
    return title_;
  }

  @Override
  public boolean loadData(final ProgressionInterface _interface) {
    return true;
  }

  @Override
  public boolean isGeometrieLoaded() {
    return true;
  }

  /**
   * @return le fichie dat
   */
  public File getDATFile() {
    return new File(dir_, CtuluLibFile.getFileName(title_, "DAT"));
  }

  /**
   * @return le path du projet (dir+title)
   */
  public String getProjectPath() {
    return new File(dir_, title_).getAbsolutePath();
  }

  /**
   * @return le fichier par
   */
  public File getPARFile() {
    return new File(dir_, CtuluLibFile.getFileName(title_, RubarPARFileFormat.getInstance().getExtensions()[0]));
  }

  /**
   * @return la dossier comportant tous les fichiers
   */
  public File getDirBase() {
    return dir_;
  }

  @Override
  public H2dParameters getH2dParametres() {
    return params_;
  }

  /**
   * @return les parametres (le modele)
   */
  public H2dRubarParameters getH2dRubarParametres() {
    return params_;
  }

  @Override
  public boolean canImportEvolution() {
    return true;
  }
}
