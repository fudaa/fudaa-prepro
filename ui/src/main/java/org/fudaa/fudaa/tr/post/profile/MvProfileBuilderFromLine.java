/*
 * @creation 13 juin 07
 * 
 * @modification $Date: 2007-06-13 12:58:11 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import org.locationtech.jts.geom.LineString;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsCorrectionTester;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.export.MvExportChooseVarAndTime;

public class MvProfileBuilderFromLine extends MvProfileBuilder {

  public MvProfileBuilderFromLine(final MvProfileTarget _data, final CtuluUI _ui, final LineString _line,
      final EfLineIntersectionsCorrectionTester _tester) {
    super(_data, _ui, _tester, _line);
    selectedLine_ = _line;

  }

  protected void actFirstTest(final ProgressionInterface _prog) {
    // premier résultats pour voir s'il y a des intersections
    final EfLineIntersectionsResultsI res = getDefaultRes((CtuluVariable) initVar_.getElementAt(0), _prog);
    if (res.isEmpty()) {

      showNoIntersectionFound();
    } else {
      final MvExportChooseVarAndTime export = createVarTimeChooser(getHelpForVarTime());
      if (export.afficheModaleOk(CtuluLibSwing.getActiveWindow(), MvResource.getS("Profils spatiaux"), export
          .getListRunnable())) {
        final CtuluTaskDelegate task = ui_.createTask(FudaaLib.getS("Construction des courbes"));
        task.start(new Runnable() {
          @Override
          public void run() {
            MvProfileBuilderFromLine.this.actBuildGroup(task.getStateReceiver(), export);
          }
        });

      }

    }
    close();
  }

  @Override
  protected void stepOne() {
    final CtuluTaskDelegate task = ui_.createTask(DodicoLib.getS("Recherche intersections"));
    task.start(new Runnable() {
      @Override
      public void run() {
        actFirstTest(task.getStateReceiver());
      }
    });

  }

}
