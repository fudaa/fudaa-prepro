/*
 * @creation 8 d�c. 2005
 *
 * @modification $Date: 2007-06-05 09:01:14 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuComparator;
import com.memoire.fu.FuSort;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.h2d.H2DLib;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreatedDefault;
import org.fudaa.fudaa.tr.post.persist.TrPostSourceComparaisonPersistReplay;
import org.fudaa.fudaa.tr.post.persist.TrPostSourceReplayPersist;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Une source permettant de comparer 2 resultats.
 *
 * @author Fred Deniger
 * @version $Id: TrPostSourceComparator.java,v 1.12 2007-06-05 09:01:14 deniger Exp $
 */
public final class TrPostSourceComparator extends TrPostSourceAbstractFromIdx implements TrPostSourceBuilt {
  final TrPostSource proj_;
  final TrPostSource ref_;
  Map compVar_;
  boolean inv_;
  TrPostSourceComparaisonPersistReplay replay;
  Collection<File> files_;
  private H2dVariableType[] srcVariable_;

  public TrPostSourceComparator(final TrPostSource _ref, final TrPostSource _proj,
                                final TrPostCommonImplementation _impl, TrPostSourceComparaisonPersistReplay replay) {
    super(TrResource.getS("Comparaison {0}", _impl.getProject().getSources().getShortNameOfSource(_ref) + " / " + _impl.getProject().getSources().getShortNameOfSource(_proj)),
        _ref.getGrid(), _ref.getTime().getInitTimeSteps(),
        new H2dVariableType[0], _impl);
    ref_ = _ref;
    proj_ = _proj;
    this.replay = replay;
  }

  @Override
  public Collection<File> getFiles() {
    Collection<File> res = new ArrayList<File>();
    Collection<File> file = ref_.getFiles();
    if (file != null) {
      res.addAll(file);
    }
    file = proj_.getFiles();
    if (file != null) {
      res.addAll(file);
    }
    return Collections.unmodifiableCollection(res);
  }

  @Override
  public String getFormatedTitle() {
    return TrResource.getS("Comparaison entre {0} et {1}", ref_.getTitle(), proj_.getTitle());
  }

  @Override
  protected void buildDefaultVectors() {
    varCreateData_ = new HashMap();
    final List refFlech = new ArrayList(ref_.getFlecheListModel().getSize() + proj_.getFlecheListModel().getSize());
    TrPostSourceComparatorBuilder.createFleches(proj_, refFlech, "proj");
    TrPostSourceComparatorBuilder.createFleches(ref_, refFlech, "ref");
    super.fleches_ = (TrPostFlecheContent[]) refFlech.toArray(new TrPostFlecheContent[refFlech.size()]);
    Arrays.sort(fleches_, TrPostDataCreatedDefault.NAME_COMPARATOR);
    for (int i = fleches_.length - 1; i >= 0; i--) {
      varCreateData_.put(fleches_[i].getVar(), fleches_[i]);
    }
  }

  protected boolean isInv() {
    return inv_;
  }

  public void setInv(final boolean _inv) {
    inv_ = _inv;
  }

  /**
   * On recup�re les variables communes.
   */
  @Override
  protected void updateVarList() {
    final Set commonVarList = new HashSet(ref_.getVariableNb());
    final H2dVariableType[] refVar = ref_.getAvailableVar();
    final Set projVar = new HashSet(Arrays.asList(proj_.getAvailableVar()));
    // on recup�re les variables communes et celle qui ont le meme nom
    compVar_ = null;
    for (int i = 0; i < refVar.length; i++) {
      // pour l'instant, pour �viter que l'utilisateur enleve une variable et que cette source
      // ne soit pas mise a jour
      if (!ref_.isUserCreatedVar(refVar[i])) {
        if (projVar.contains(refVar[i])) {
          commonVarList.add(refVar[i]);
        } else {
          final H2dVariableType commonInProj = H2DLib.getVarWithName(projVar, refVar[i].getName());
          if (commonInProj != null) {
            commonVarList.add(refVar[i]);
            if (compVar_ == null) {
              compVar_ = new HashMap();
            }
            compVar_.put(refVar[i], commonInProj);
          }
        }
      }
    }
    final H2dVariableType[] common = (H2dVariableType[]) commonVarList.toArray(new H2dVariableType[commonVarList.size()]);
    FuSort.sort(common, FuComparator.STRING_COMPARATOR);
    final List finalVar = new ArrayList(common.length * 2);
    srcVarByComparisonVar.clear();
    for (int i = 0; i < common.length; i++) {
      final H2dVariableType deltaVar = TrPostSourceComparatorBuilder.createDeltaVar(common[i], false);
      finalVar.add(deltaVar);
      srcVarByComparisonVar.put(deltaVar, common[i]);
    }
    setInitVar((H2dVariableType[]) finalVar.toArray(new H2dVariableType[finalVar.size()]));
    srcVariable_ = common;
    fireVariableAdd(false);
    fireVarListModelChanged();
  }

  private final Map<CtuluVariable, CtuluVariable> srcVarByComparisonVar = new HashMap<>();

  public TrPostInspectorReader createWatcher(final TrPostProjet _proj, final boolean _auto) {
    return null;
  }

  @Override
  public boolean isElementVar(CtuluVariable _idxVar) {
    CtuluVariable initVar = srcVarByComparisonVar.get(_idxVar);
    boolean refIsElement = ref_.isElementVar(initVar);
    if (refIsElement != proj_.isElementVar(initVar)) {
      return true;
    }
    return refIsElement;
  }

  @Override
  public EfData getInitData(final int _varIdx, final int _timeIdx) {
    H2dVariableType src = srcVariable_[_varIdx];
    EfData refData = ref_.getData(src, _timeIdx);
    if (compVar_ != null && compVar_.containsKey(src)) {
      src = (H2dVariableType) compVar_.get(src);
    }
    EfData projData = proj_.getData(src, _timeIdx);
    if (refData == null || projData == null) {
      return null;
    }
    if (refData.isElementData() != projData.isElementData()) {
      if (refData.isElementData()) {
        projData = EfLib.getElementDataDanger(projData, proj_.getGrid());
      } else {
        refData = EfLib.getElementDataDanger(refData, ref_.getGrid());
      }
    }

    final double[] res = new double[refData.getSize()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = projData.getValue(i) - refData.getValue(i);
      if (inv_) {
        res[i] = -res[i];
      }
    }
    return refData.isElementData() ? new EfDataElement(res) : new EfDataNode(res);
  }

  @Override
  public double getInitData(final int _varIdx,
                            final int _timeIdx,
                            final int _ptIdx) throws IOException {
    final H2dVariableType src = srcVariable_[_varIdx];
    H2dVariableType srcProj = src;
    if (compVar_ != null && compVar_.containsKey(src)) {
      srcProj = (H2dVariableType) compVar_.get(src);
    }
    if (proj_.isElementVar(src) != ref_.isElementVar(srcProj)) {
      final EfData initData = getInitData(_varIdx, _timeIdx);
      return initData == null ? 0d : initData.getValue(_ptIdx);
    }
    final double res = proj_.getData(src, _timeIdx, _ptIdx) - ref_.getData(srcProj, _timeIdx, _ptIdx);
    return inv_ ? -res : res;
  }

  public void fillInfosCreationWith(Map<String, String> _infosCreation) {
  }

  @Override
  public boolean containsElementVar() {
    return ref_.containsElementVar();
  }

  @Override
  public List<TrPostSource> getUsedSources() {
    return Arrays.asList(ref_, proj_);
  }

  @Override
  public TrPostSourceReplayPersist getReplay() {
    replay.setId(getId());
    return replay;
  }
}
