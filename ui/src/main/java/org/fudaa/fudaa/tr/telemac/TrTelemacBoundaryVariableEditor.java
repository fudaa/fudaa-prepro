/**
 *  @creation     17 mai 2004
 *  @modification $Date: 2006-09-19 15:07:29 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandCompositeInverse;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluUndoRedoInterface;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcManager;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcParameter;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * Un panneau permettant de d�finir les variables de bords.
 * A panel allowing to define boundary variables
 * @author Fred Deniger
 * @version $Id: TrTelemacBoundaryVariableEditor.java,v 1.17 2006-09-19 15:07:29 deniger Exp $
 */
public class TrTelemacBoundaryVariableEditor extends JPanel implements CtuluUndoRedoInterface {

  CtuluCommandManager cmd_;
  H2dTelemacBcManager mng_;

  /**
   * Constructeur par defaut.
   * Default constructor
   * 
   * @param _manager le gestionnaire des frontiere / the boundaries manager
   */
  public TrTelemacBoundaryVariableEditor(final H2dTelemacBcManager _manager) {
    final String variableInSpaceS = TrResource.getS("Variable en l'espace");
    mng_ = _manager;
    final boolean b = mng_.isTransientAvailable();
    String fixedOrTransientS = null;
    if (b) {
      fixedOrTransientS = TrResource.getS("Constant sur les fronti�res / variable dans le temps");
    } else {
      fixedOrTransientS = TrResource.getS("Constant sur les fronti�res");
    }
    final String[] ms = new String[] { fixedOrTransientS, variableInSpaceS };
    final H2dTelemacBcParameter[] vAll = mng_.getVariablesWithKeyword();
    if (vAll == null) {
      setLayout(new BuBorderLayout());
      add(new BuLabel(TrResource.getS("Aucune variable � d�finir")));
      return;
    }
    cmd_ = new CtuluCommandManager();
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    Arrays.sort(vAll, H2dTelemacBcParameter.createVariableComparator());
    setLayout(new BuGridLayout(2, 5, 5));
    for (int i = 0; i < vAll.length; i++) {
      if (vAll[i].isKeywordImposed()) {
        new Fixed(this, vAll[i], fixedOrTransientS);
      } else {
        new FixedOrNot(this, vAll[i], ms);
      }
    }
  }

  String getDesc(final H2dTelemacBcParameter _p) {
    return _p.getVariable().getName()/* + " ( " + _p.getEntite().getNom() + " )" */;
  }

  private static class Fixed {

    JLabel lb_;
    H2dTelemacBcParameter p_;

    public Fixed(final TrTelemacBoundaryVariableEditor _editor, final H2dTelemacBcParameter _p, final String _desc) {
      p_ = _p;
      lb_ = new JLabel(_editor.getDesc(p_));
      lb_.setToolTipText(p_.getEntite().getNom());
      _editor.add(lb_);
      _editor.add(new BuLabel(_desc));
    }
  }

  private static class FixedOrNot implements ItemListener {

    JLabel lb_;
    H2dTelemacBcParameter p_;
    JComboBox cb_;
    final TrTelemacBoundaryVariableEditor editor_;

    public FixedOrNot(final TrTelemacBoundaryVariableEditor _editor, final H2dTelemacBcParameter _p,
        final String[] _fixedOrNot) {
      p_ = _p;
      editor_ = _editor;
      lb_ = new JLabel(_editor.getDesc(p_));
      lb_.setToolTipText(p_.getEntite().getNom());
      _editor.add(lb_);
      cb_ = new BuComboBox(_fixedOrNot);
      _editor.add(cb_);
      if (p_.isFixedByUser()) {
        cb_.setSelectedIndex(0);
      } else {
        cb_.setSelectedIndex(1);
      }
      cb_.addItemListener(this);
    }

    @Override
    public void itemStateChanged(final ItemEvent _e) {
      if (_e.getStateChange() == ItemEvent.SELECTED) {

        final int i = cb_.getSelectedIndex();
        final boolean isFixe = cb_.getSelectedIndex() == 0;
        if (isFixe == p_.isFixedByUser()) { return; }
        final CtuluCommandCompositeInverse cr = new CtuluCommandCompositeInverse();
        p_.setFixedByUserAndCmd(isFixe, editor_.mng_, cr);
        cr.addCmd(new CtuluCommand() {

          @Override
          public void undo() {
            cb_.setSelectedIndex(i == 0 ? 1 : 0);
          }

          @Override
          public void redo() {
            cb_.setSelectedIndex(i);
          }
        });
        editor_.cmd_.addCmd(cr.getSimplify());
      }
    }
  }

  @Override
  public void clearCmd(final CtuluCommandManager _source) {
    if ((cmd_ != null) && (_source != cmd_)) {
      cmd_.clean();
    }
  }

  @Override
  public CtuluCommandManager getCmdMng() {
    return cmd_;
  }

  @Override
  public void setActive(final boolean _b) {}
}