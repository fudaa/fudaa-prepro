/**
 * @creation 3 nov. 2004
 * @modification $Date: 2006-09-19 15:07:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuFileRenderer;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuToolButton;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.vfs.VfsFile;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.Icon;
import javax.swing.JComponent;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFilter;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.fudaa.commun.FudaaUI;
import org.fudaa.fudaa.commun.exec.FudaaExec;
import org.fudaa.fudaa.tr.post.persist.TrPostFileFilter;
import org.fudaa.fudaa.tr.rubar.TrRubarImplHelper;

/**
 * @author Fred Deniger
 * @version $Id: TrFileRenderer.java,v 1.10 2006-09-19 15:07:30 deniger Exp $
 */
public class TrFileRenderer extends BuFileRenderer {

  TrApplicationManager mng_;

  private class SpecificButton extends BuToolButton {

    FudaaExec exe_;
    File f_;

    protected SpecificButton(final FudaaExec _exe, final File _f) {
      exe_ = _exe;
      f_ = _f;
      setActionCommand(_exe.getExecCommand());
      setIcon(_exe.getIcon());
      setToolTipText(_exe.getViewedName());
    }

    @Override
    protected void fireActionPerformed(final ActionEvent _event) {
      exe_.execOnFile(f_, ui_);
      super.fireActionPerformed(_event);
    }
  }

  FudaaUI ui_;

  /**
   * 
   */
  public TrFileRenderer(final TrApplicationManager _mng, final FudaaUI _ui) {
    mng_ = _mng;
    ui_ = _ui;
  }

  @Override
  public JComponent getActions(final VfsFile _file) {
    if (!_file.isDirectory()) {
      final BuToolButton btOuv = new BuToolButton();
      final String s = mng_.ouvrir(_file, btOuv);
      if (s != null) {
        btOuv.setToolTipText(s);
        btOuv.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(final ActionEvent _e) {
            mng_.ouvrir(_file);
          }
        }

        );
        final BuPanel pnV = new BuPanel();
        pnV.setLayout(new BuVerticalLayout(1, false, false));
        pnV.add(btOuv);
        final FudaaExec[] act = mng_.getActionForFile(_file);
        if (act != null) {
          final int nb = act.length;
          for (int i = 0; i < nb; i++) {
            final BuToolButton bt = new SpecificButton(act[i], _file);
            pnV.add(bt);
          }
        }
        return pnV;
      }
    }
    return super.getActions(_file);
  }

  @Override
  public Icon getIcon(final VfsFile _file) {
    if (!_file.isDirectory()) {
      if (mng_.isProjectFormat(_file)) { return TrResource.getEditorIcon(); }
      final FileFormat ft = mng_.findGridFileFormat(_file);
      if (ft != null) {
        if (mng_.getSoftId() != TrRubarImplHelper.getID() && ft == SerafinFileFormat.getInstance()
            && (((SerafinFileFilter) mng_.getFileFilterFor(ft)).isResFile(_file))) { return TrResource.getPostIcon(); }
        return TrResource.getMeshIcon();
      }
      if (mng_.isPostFormat(_file)) { return TrResource.getPostIcon(); }
      if (mng_.isPostLayoutFormat(_file)) { return TrResource.getPostLayoutIcon(); }
      if (mng_.getTxtFileFilter().accept(_file)) { return BuResource.BU.getToolIcon("texte"); }
      if(TrApplicationManager.isScopFormat(_file)){	return TrResource.getScopIcon();  	}
    }else
    	if(TrPostFileFilter.acceptDirectoryPOST(_file)){
    		return TrResource.getPostLayoutIcon(); 
    	}
    
    return super.getIcon(_file);
  }
}