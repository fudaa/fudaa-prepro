/*
 *  @creation     29 sept. 2003
 *  @modification $Date: 2007-06-28 09:28:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr.reflux;

import java.io.File;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.impl.EfGridDataDefault;
import org.fudaa.dodico.ef.impl.EfLibImpl;
import org.fudaa.dodico.fortran.FortranDoubleReaderResultInterface;
import org.fudaa.dodico.h2d.reflux.H2dRefluxDicoVersion;
import org.fudaa.dodico.h2d.reflux.H2dRefluxParameters;
import org.fudaa.dodico.h2d.reflux.H2dRefluxSolutionInitAdapterInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.olb.exec.OLBExec;
import org.fudaa.dodico.reflux.io.INPAdapter;
import org.fudaa.dodico.reflux.io.INPFileFormat;
import org.fudaa.dodico.reflux.io.RefluxSollicitationFileFormat;
import org.fudaa.dodico.reflux.io.RefluxSolutionInitReader;
import org.fudaa.fudaa.meshviewer.export.MvExportT6Activity;
import org.fudaa.fudaa.tr.common.TrImplementationEditorAbstract;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrRefluxProjectFactory.java,v 1.37 2007-06-28 09:28:18 deniger Exp $
 */
public final class TrRefluxProjectFactory {

  /**
   * Lit un fichier inp et initialise les parametres.
   * 
   * @param _f le fichier inp
   * @param _ft le format du fichier inp
   * @param _progress barre de progression
   * @param _ui l'interface utilisateur
   * @return les parametres
   */
  public static TrRefluxParametres createRefluxParametresFromINP(final File _f, final H2dRefluxDicoVersion _ft,
      final ProgressionInterface _progress, final TrImplementationEditorAbstract _ui,
      final TrRefluxProjectDispatcherListener _l) {
    CtuluIOOperationSynthese synt = _ft.read(_f, _progress);
    final INPAdapter inter = (INPAdapter) synt.getSource();
    if (_ui != null) {
      _ui.manageErrorOperationAndIsFatal(synt);
    }
    if (synt.containsSevereError()) { return null; }
    if (inter == null) { return null; }
    final TrRefluxParametres r = new TrRefluxParametres();
    r.ui_ = _ui;
    r.fINP_ = _f;
    String s = _f.getName();
    final int i = s.indexOf('.');
    if (i > 0) {
      s = s.substring(0, i);
    }
    r.title_ = s;
    final CtuluAnalyze analyze = new CtuluAnalyze();
    // analyze.addError(TrResource.getS("Le fichier inp est mal format�"), -1);
    final String sifile = _ft.getSolutionInitiales(inter.getFichiers());
    H2dRefluxSolutionInitAdapterInterface si2D = null;
    if (sifile != null) {
      final File siv = new File(_f.getParentFile(), sifile);
      // a modifier par la suite pour prendre en compte des si non 2D

      if (siv.exists()) {

        synt = RefluxSolutionInitReader.loadSI(inter.getGrid(), siv, _progress);
        if (_ui != null) {
          _ui.manageErrorOperationAndIsFatal(synt);
        }
        if (!synt.containsSevereError()) {
          si2D = (H2dRefluxSolutionInitAdapterInterface) synt.getSource();
        }
      }
    }
    // vent
    if (inter.contientVent()) {
      final File vent = new File(_f.getParentFile(), _ft.getVent(inter.getFichiers(), inter.getTypeProjet()));
      if (!vent.exists()) {
        analyze.addError(CtuluLib.getS("Le fichier {0} n'existe pas", vent.getName()) + CtuluLibString.ESPACE
            + TrResource.getS("les donn�es concernant le vent seront ignor�es."), -1);
      }
      synt = new RefluxSollicitationFileFormat(2, null).read(vent, _progress);
      if (_ui != null) {
        _ui.manageErrorOperationAndIsFatal(synt);
      }
      if (!synt.containsSevereError()) {
        inter.setVent((FortranDoubleReaderResultInterface) synt.getSource());
        // probleme avec le fichier, il faut forcer la modif
        if (synt.containsMessages() && synt.getAnalyze().containsErrors()) {
          _l.setParamsChanged();
        }
      }

    }
    // radiation
    if (inter.contientRadiations()) {
      final File radiation = new File(_f.getParentFile(), _ft.getRadiation(inter.getFichiers(), inter.getTypeProjet()));
      if (!radiation.exists()) {
        analyze.addError(CtuluLib.getS("Le fichier {0} n'existe pas", radiation.getName()) + CtuluLibString.ESPACE
            + TrResource.getS("les donn�es concernant les contraintes en radiations seront ignor�es."), -1);
      }
      synt = new RefluxSollicitationFileFormat(3, null).read(radiation, _progress);
      if (_ui != null) {
        _ui.manageErrorOperationAndIsFatal(synt);
      }
      if (!synt.containsSevereError()) {
        inter.setRadiations((FortranDoubleReaderResultInterface) synt.getSource());
        if (synt.containsMessages() && synt.getAnalyze().containsErrors()) {
          _l.setParamsChanged();
        }
      }

    }
    analyze.setDesc(TrResource.getS("Analyse du fichier") + CtuluLibString.ESPACE + _f.getAbsolutePath());
    r.params_ = H2dRefluxParameters.init(inter, si2D, _progress, _ft, analyze, _l);
    if (_ui != null) {
      _ui.manageAnalyzeAndIsFatal(analyze);
    }
    return r;
  }

  /**
   * Cree un projet reflux a partir d'un fichier inp.
   * 
   * @param _file le fichier inp
   * @param _fileFormat le format
   * @param _progress la barre de progression
   * @param _ui l'interface utilisateur
   * @return un nouveau projet
   */
  public static TrRefluxProjet createRefluxProjectFromINP(final File _file, final H2dRefluxDicoVersion _fileFormat,
      final ProgressionInterface _progress, final TrImplementationEditorAbstract _ui) {
    final TrRefluxProjectDispatcherListener l = new TrRefluxProjectDispatcherListener();
    final TrRefluxParametres params = createRefluxParametresFromINP(_file, _fileFormat, _progress, _ui, l);
    _progress.setProgression(100);
    if (params != null) {
      final TrRefluxProjet p = new TrRefluxProjet(params);

      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Verif des prop elementaires mal specifiees");
      }
      // on verifie les prop elementaires pour donner un message a l'utilisateur
      final H2dRefluxParameters p2d = params.getH2dRefluxParametres();
      final H2dVariableType[] r = p2d.getElementPropMng().getEltPropNotAllSet();
      if (r != null) {
        _ui.error(TrResource.getS("Variables �l�mentaires"), TrResource.getS(
            "Les variables {0} ne sont pas enti�rement sp�cifi�es.", CtuluLibString.arrayToString(r))
            + CtuluLibString.LINE_SEP + TrResource.getS("Les noeuds libres seront fix�s � 0."), false);
      }
      updateSIData(p);
      l.setProj(p);
      return p;
    }
    return null;
  }

  /**
   * Cree un projet reflux a partir d'un fichier de maillage.
   * 
   * @param _f le fichier de maillage
   * @param _ft le format du fichier de maillage
   * @param _t le type de projet voulu
   * @param _op la barre de progression
   * @param _ui l'interface utilisateur
   * @return un nouveau projet
   */
  public static TrRefluxProjet createRefluxProjectFromGrid(final File _f, final FileFormatGridVersion _ft,
      final H2dProjetType _t, final ProgressionInterface _op, final TrImplementationEditorAbstract _ui,
      final boolean _useOlb) {
    final EfGridInterface g = getValidGrid(_f, _ft, _ui, _op, _useOlb);
    if (g == null) { return null; }
    final TrRefluxParametres r = new TrRefluxParametres();
    r.ui_ = _ui;
    final CtuluAnalyze analyze = new CtuluAnalyze();
    final TrRefluxProjectDispatcherListener l = new TrRefluxProjectDispatcherListener();
    r.params_ = H2dRefluxParameters.init(g, _t, _op, INPFileFormat.getInstance().getLastINPVersionImpl(), analyze, l);
    final TrRefluxProjet rProjet = new TrRefluxProjet(r);
    r.title_ = TrResource.getS("Nouveau");
    if (r.params_ == null) { return null; }
    updateSIData(rProjet);
    l.setProj(rProjet);
    // l.setParamsChanged();
    return rProjet;
  }

  /**
   * @param _f le fichier contenant le maillage
   * @param _ft le format du maillage
   * @param _ui l'interface utilisateur
   * @param _op la barre de progression
   * @return maillage correctement initialise
   */
  public static EfGridInterface getValidGrid(final File _f, final FileFormatGridVersion _ft,
      final TrImplementationEditorAbstract _ui, final ProgressionInterface _op, final boolean _useOLB) {
    if ((_f == null) || (_ft == null)) { return null; }
    _ui.setMainMessage(TrResource.getS("Lecture fichier"));
    _ui.setMainProgression(5);
    CtuluIOOperationSynthese op = _ft.readGrid(_f, _op);
    if (_ui.manageErrorOperationAndIsFatal(op)) { return null; }
    EfGridInterface g = ((EfGridSource) op.getSource()).getGrid();
    op = null;
    if ((g.getPtsNb() == 0) || (g.getEltNb() == 0)) {
      _ui.error(TrResource.getS("Maillage vide"));
      _ui.unsetMainMessage();
      return null;
    }
    _ui.setMainMessage(TrResource.getS("optimisation maillage"));
    _ui.setMainProgression(50);
    if (g.getEltType() == EfElementType.T3) {
      _ui.setMainMessage(TrResource.getS("Transformation maillage en T6"));
      _ui.setMainProgression(30);
      g = EfLibImpl.maillageT3enT6(g, _op, null);
    } else if (g.getEltType() != EfElementType.T6) {
      final MvExportT6Activity act = new MvExportT6Activity(new EfGridDataDefault(g));
      final CtuluAnalyze ana = new CtuluAnalyze();
      final EfGridData res = act.process(_op, ana);
      _ui.manageAnalyzeAndIsFatal(ana);
      if (res == null) {
        g = null;
      } else {
        g = res.getGrid();
      }
    }
    if (g == null) {
      _ui.error(H2dResource.getS("Impossible de transformer le maillage en T6"));
      _ui.unsetMainMessage();
      return null;
    }
    if (_useOLB) {
      CtuluLibMessage.info("OLB optimization");
      g = new OLBExec().computeGrid(g, _op, _ui);
      if ((g == null) || (g.getPtsNb() == 0) || (g.getEltNb() == 0)) {
        _ui.error(H2dResource.getS("Maillage vide")
            + CtuluLibString.LINE_SEP
            + TrResource.getS("Reflux fait appel � l'optimiseur de bande OLB. "
                + "Vous pouvez pr�ciser le chemin de l'ex�cutable dans le panneau de pr�f�rences."));
        _ui.unsetMainMessage();
        return null;
      }
    }
    _ui.setMainMessage(TrResource.getS("Recherche des bords"));
    _ui.setMainProgression(70);
    final CtuluAnalyze ana = new CtuluAnalyze();
    g.computeBord(_op, ana);
    g.createIndexRegular(_op);
    _ui.manageAnalyzeAndIsFatal(ana);
    _ui.unsetMainMessage();
    return g;
  }

  /**
   * Constructeur interdit.
   */
  private TrRefluxProjectFactory() {
    super();
  }

  /**
   * @param _p le projet � verifier
   */
  private static void updateSIData(final TrRefluxProjet _p) {
    if (!_p.getRefluxParametres().getH2dRefluxParametres().isSolutionInitialSet()) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("create default reflux si");
      }
      _p.getRefluxParametres().getH2dRefluxParametres().initilialiseSolutionInitiale();
    }
  }
}