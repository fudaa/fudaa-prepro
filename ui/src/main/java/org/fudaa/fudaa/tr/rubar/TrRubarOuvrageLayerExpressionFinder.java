package org.fudaa.fudaa.tr.rubar;

import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrage;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.fudaa.tr.common.TrResource;
import org.nfunk.jep.Variable;

public class TrRubarOuvrageLayerExpressionFinder implements EbliFindExpressionContainerInterface {
  private Variable idxOuvrage;
  private Variable isElt2Set;
  private final TrRubarOuvrageLayerModel model;
  private Variable nbElementaryWork;
  private Variable nbInternMesh;
  private Variable rubarRef;

  public TrRubarOuvrageLayerExpressionFinder(TrRubarOuvrageLayerModel model) {
    this.model = model;
  }

  @Override
  public void initialiseExpr(CtuluExpr _expr) {
    idxOuvrage = _expr.addVar("workIdx", TrResource.getS("L'indice de l'ouvrage"));
    nbElementaryWork = _expr.addVar("elementaryWorksNb", TrResource.getS("Nombre d'ouvrages élémentaires"));
    nbInternMesh = _expr.addVar("internMeshNb", TrResource.getS("Nombre de mailles internes"));
    rubarRef = _expr.addVar("rubarRef", TrResource.getS("Référence Rubar ( -2 pour Pas de calcul avec les équations de Saint-Venant, -1 sinon)"));
    isElt2Set = _expr.addVar("hasDownstreamMesh", TrResource.getS("Contient maille aval"));
  }

  @Override
  public void majVariable(int _idx, Variable[] _varToUpdate) {
    if (model.isModeElt()) {
      return;
    }
    idxOuvrage.setValue(CtuluLib.getDouble(_idx + 1d));
    final H2dRubarOuvrage ouvrage = model.getOuvrage(_idx);
    nbElementaryWork.setValue(ouvrage == null ? Double.valueOf(0) : Double.valueOf(ouvrage.getNbOuvrageElementaires()));
    nbInternMesh.setValue(ouvrage == null ? Double.valueOf(0) : Double.valueOf(ouvrage.getNbMailleIntern()));
    rubarRef.setValue(ouvrage == null ? Double.valueOf(0) : Double.valueOf(ouvrage.getRubarRef()));
    isElt2Set.setValue(ouvrage != null && ouvrage.isElt2Set());
  }
}
