/*
 * @creation 19 avr. 2006
 * @modification $Date: 2007-06-20 12:23:40 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import gnu.trove.TDoubleArrayList;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.reflux.io.RefluxRefondeSolutionFileFormat;
import org.fudaa.dodico.reflux.io.RefluxRefondeSolutionNewReader;
import org.fudaa.dodico.reflux.io.RefluxRefondeSolutionSequentielResult;
import org.fudaa.dodico.reflux.io.RefluxSolutionSequentielReader;

import java.io.File;
import java.io.IOException;

/**
 * @author fred deniger
 * @version $Id: TrPostInspectorReaderReflux.java,v 1.6 2007-06-20 12:23:40 deniger Exp $
 */
public class TrPostInspectorReaderReflux implements TrPostInspectorReader {
  final File file_;
  File grid_;
  long lastModified_;
  TrPostProjet prj_;
  TrPostSource src_;
  TrPostCommonImplementation impl_;
  ProgressionInterface prog_;
  RefluxSolutionSequentielReader reader_;
  boolean isActivating_;

  @Override
  public boolean isPostActivating() {
    if (isActivating_ && TrPostInspectorReaderSerafin.isVisuDisplayed(prj_)) {
      isActivating_ = false;
    }
    return isActivating_;
  }

  public TrPostInspectorReaderReflux(final File _file, final File _grid) {
    super();
    file_ = _file;
    grid_ = _grid;
  }

  public TrPostInspectorReaderReflux(final TrPostProjet _prj, final TrPostSource _src) {
    super();
    file_ = _src.getMainFile();
    prj_ = _prj;
    src_ = _src;
  }

  protected ProgressionInterface getProg() {
    return prog_;
  }

  protected void setProg(final ProgressionInterface _prog) {
    prog_ = _prog;
  }

  @Override
  public void close() {
    try {
      reader_.close();
    } catch (final IOException _evt) {
      FuLog.error(_evt);
    }
  }

  @Override
  public TrPostProjet getPostActivatedProject() {
    return prj_;
  }

  @Override
  public boolean isPostActivated() {
    return prj_ != null;
  }

  @Override
  public int read() {
    int res = 0;
    if (!file_.exists() || file_.length() == 0) {
      return 0;
    }
    if (reader_ == null) {
      final RefluxRefondeSolutionNewReader reader = new RefluxRefondeSolutionNewReader(RefluxRefondeSolutionFileFormat
          .getInstance());
      reader.setSuivi(true);
      final CtuluIOOperationSynthese io = reader.read(file_, prog_);
      final Object o = io.getSource();
      if (o != null) {
        reader_ = new RefluxSolutionSequentielReader((RefluxRefondeSolutionSequentielResult) o, file_);
      }
    }
    if (reader_ == null) {
      return 0;
    }
    try {
      final int newTimeStep = reader_.getNbTimeStepGuessed();
      // on est en surveillance de fichier
      if (file_.lastModified() > lastModified_) {
        if (prj_ == null) {
          if (newTimeStep > 0) {
            res = newTimeStep;
            // dans le thread swing
            isActivating_ = true;
            // thread lecture
          }
        } else {

          final int nbTimeStep = src_.getNbTimeStep();
          if (newTimeStep > nbTimeStep) {
            final TDoubleArrayList ts = new TDoubleArrayList(newTimeStep);
            ts.add(src_.getTime().getInitTimeSteps());
            final double[] newTs = reader_.readTimeStep(nbTimeStep, newTimeStep - nbTimeStep);
            ts.add(newTs);
            res = newTs == null ? 0 : newTs.length;
            src_.getTime().setTimeSteps(ts.toNativeArray());
          }
        }

        lastModified_ = file_.lastModified();
      }
    } catch (final IOException _evt) {
      FuLog.error(_evt);
    }
    return res;
  }

  @Override
  public void setProgression(final ProgressionInterface _prog) {
    prog_ = _prog;
  }

  @Override
  public void setImpl(final TrPostCommonImplementation _impl) {
    impl_ = _impl;
  }
}
