/**
 *  @creation     1 oct. 2004
 *  @modification $Date: 2004-10-20 08:16:24 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.rubar;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFileStateListener.java,v 1.2 2004-10-20 08:16:24 deniger Exp $
 */
public interface TrRubarFileStateListener {

  /**
   * @param _f le fmt modifie
   */
  void fileStateChanged(TrRubarFileState _f);

}
