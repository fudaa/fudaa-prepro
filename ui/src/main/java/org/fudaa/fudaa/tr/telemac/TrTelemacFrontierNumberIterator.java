/**
 * @creation 24 f�vr. 2005 @modification $Date: 2007-03-30 15:40:30 $ @license GNU General Public License 2 @copyright
 * (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.telemac;

import javax.swing.table.TableCellEditor;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBordParFrontiere;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBoundary;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacFrontierNumberIterator.java,v 1.12 2007-03-30 15:40:30 deniger Exp $
 */
public class TrTelemacFrontierNumberIterator extends TrTelemacFrontierNumberIteratorAbstract {

  H2dTelemacBordParFrontiere fr_;
  EfGridInterface grid_;
  H2dTelemacBoundary idxDeb_;

  /**
   * @param _grid
   * @param _fr
   * @param _idxDeb
   */
  public TrTelemacFrontierNumberIterator(final EfGridInterface _grid, final H2dTelemacBordParFrontiere _fr,
                                         final H2dTelemacBoundary _idxDeb, final double[] _x) {
    super(_x);
    grid_ = _grid;
    fr_ = _fr;
    idxDeb_ = _idxDeb;
  }

  @Override
  public void setEditable(boolean editable) {
  }

  @Override
  public CtuluNumberFormatI getCopy() {
    return this;
  }

  @Override
  boolean isIndiceValid(final int _idx) {
    final int min = getMinDisplayValue();
    final int max = getMaxDisplayValue();
    if (max > min) {
      return _idx >= min && _idx <= max;
    }
    return (_idx >= 1 && _idx <= max) || (_idx >= min && _idx <= fr_.getNbPt());
  }

  @Override
  public String formatIntValue(final int _v) {
    return CtuluLibString.getString(grid_.getFrontiers().getFrontiereIndice(fr_.getFrIdx(),
                                                                            fr_.getReelIdxOnFr(_v + idxDeb_.getIdxDeb())) + 1);
  }

  public final H2dTelemacBordParFrontiere getFr() {
    return fr_;
  }

  public final EfGridInterface getGrid() {
    return grid_;
  }

  public final H2dTelemacBoundary getIdxDeb() {
    return idxDeb_;
  }

  public int getMaxDisplayValue() {
    return grid_.getFrontiers().getFrontiereIndice(fr_.getFrIdx(), idxDeb_.getIdxFin()) + 1;
  }

  public int getMinDisplayValue() {
    return grid_.getFrontiers().getFrontiereIndice(fr_.getFrIdx(), idxDeb_.getIdxDeb()) + 1;
  }

  @Override
  public int getNbPt() {
    return idxDeb_.getNPointInBord(fr_.getNbPt());
  }

  @Override
  public String getValidationMessage() {
    return null;
  }

  @Override
  public boolean isEditable() {
    return true;
  }

  public final void setFrAndBound(final H2dTelemacBordParFrontiere _fr, final H2dTelemacBoundary _idxDeb,
                                  final double[] _x) {
    fr_ = _fr;
    idxDeb_ = _idxDeb;
    setX(_x);
  }

  public final void setGrid(final EfGridInterface _grid) {
    grid_ = _grid;
  }

  @Override
  public TableCellEditor createCommonTableEditorComponent() {
    return createTableEditorComponent();
  }
}