/*
 * @creation 30 ao�t 2005
 * 
 * @modification $Date: 2007-04-02 08:56:22 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.db4o.ObjectContainer;
import org.fudaa.ctulu.CtuluDurationDateFormatter;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.fudaa.commun.save.FudaaSaveLib;

import java.io.Serializable;

/**
 * @author Fred Deniger
 * @version $Id: TrPostTimeModelSaver.java,v 1.10 2007-04-02 08:56:22 deniger Exp $
 */
public class TrPostTimeModelSaver implements Serializable {

  String expr_;
  String fmtPattern_;
  String fmtPattern__;
  double[] savedTimeSteps_;

  public TrPostTimeModelSaver() {

  }

  public void restoreFmtPattern() {
    if (fmtPattern_ == null && fmtPattern__ != null) {
      fmtPattern_ = fmtPattern__;
    }
  }

  public TrPostTimeModelSaver(final TrPostSource _src) {
    final TrPostTimeModel model = _src.getTime();
    expr_ = model.getUsedFormule();
    fmtPattern_ = model.timeFmt_ == null ? null : model.timeFmt_.toLocalizedPattern();
    savedTimeSteps_ = model.modifiedTimes_;
  }

  public static void restore(final TrPostSource _src, final ObjectContainer _db, final ProgressionInterface _prog, final boolean _isDbUptodate) {
    if (_db == null) {
      return;
    }
    final TrPostTimeModelSaver time = (TrPostTimeModelSaver) FudaaSaveLib.getUniqueData(TrPostTimeModelSaver.class, _db);
    if (time != null) {
      time.restore(_src, _prog, _isDbUptodate);
    }
  }

  public void restore(final TrPostSource _src, final ProgressionInterface _prog, final boolean _isDbUptodate) {
    if (fmtPattern_ != null) {
      CtuluNumberFormatI fromLocalizedPattern = CtuluDurationDateFormatter.buildFromPattern(fmtPattern_);
      _src.getTime().setTimeFormat(fromLocalizedPattern);
    }
    if (expr_ != null) {
      _src.getTime().initWith(expr_, _isDbUptodate ? savedTimeSteps_ : null, _prog);
    }
  }

  public static void save(final TrPostSource _src, final ObjectContainer _db) {
    if (_db == null) {
      return;
    }
    FudaaSaveLib.deleteAll(_db, TrPostTimeModelSaver.class);
    _db.set(new TrPostTimeModelSaver(_src));
    _db.commit();
  }

}
