/*
 *  @creation     2 mai 2005
 *  @modification $Date: 2007-06-05 09:01:14 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import org.fudaa.ctulu.collection.CtuluCollectionDoubleAbstract;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;
import org.fudaa.ebli.geometrie.GrSegment;

/**
 * @author Fred Deniger
 * @version $Id: TrPostDataVecteur.java,v 1.8 2007-06-05 09:01:14 deniger Exp $
 */
public class TrPostDataVecteur extends CtuluCollectionDoubleAbstract implements EfData {

  EfData x_;
  EfData y_;

  /**
   * @param _x les valeurs selon x
   * @param _y les valeurs selon y
   */
  public TrPostDataVecteur(final EfData _x, final EfData _y, EfGridInterface grid) {
    x_ = _x;
    y_ = _y;
    if (x_.isElementData() != y_.isElementData()) {
      if (!x_.isElementData()) {
        x_ = EfLib.getElementDataDanger(_x, grid);
      }
      if (!y_.isElementData()) {
        y_ = EfLib.getElementDataDanger(_y, grid);
      }
    }
    if (isElementData() != y_.isElementData()) {
      throw new IllegalArgumentException(
              "x and y data must be in the same type");
    }
  }

  @Override
  public final int getSize() {
    return x_.getSize();
  }

  public void interpolateValues(final GrSegment _target, final EfGridInterface _grid, final int _idxElt,
          final double _x, final double _y) {
    _target.o_.x_ = _x;
    _target.o_.y_ = _y;
    double vx = 0;
    double vy = 0;
    if (isElementData()) {
      vx = x_.getValue(_idxElt);
      vy = y_.getValue(_idxElt);
    } else {
      vx = EfGridDataInterpolator.interpolateDangerous(_idxElt, _x, _y, x_, _grid);
      vy = EfGridDataInterpolator.interpolateDangerous(_idxElt, _x, _y, y_, _grid);
    }
    _target.e_.x_ = _x + vx;
    _target.e_.y_ = _y + vy;

  }

  @Override
  public final boolean isElementData() {
    return x_.isElementData();
  }

  @Override
  public double getValue(final int _idxPt) {
    final double vx = getVx(_idxPt);
    final double vy = getVy(_idxPt);
    return InterpolationVectorContainer.getNorme(vx, vy);
  }

  public final double getVx(final int _ptIdx) {
    return x_.getValue(_ptIdx);
  }

  public final EfData getVxData() {
    return x_;
  }

  public final EfData getVyData() {
    return y_;
  }

  /**
   * @param _ptIdx l'indice du point demande
   * @return la composant en y
   */
  public final double getVy(final int _ptIdx) {
    return y_.getValue(_ptIdx);
  }
}
