/**
 *  @file         TrImplementationAbstract.java
 *  @creation     4 mai 2004
 *  @modification $Date: 2006-09-19 15:07:30 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuPreferences;
import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TrImplementationEditorAbstract.java,v 1.6 2006-09-19 15:07:30 deniger Exp $
 */
public abstract class TrImplementationEditorAbstract extends TrCommonImplementation {

  /**
   * Prefixe pour les commandes d'ouvertures.
   * Prefix for the open commands
   */
  public final static String PREF_OUVRIR = "OUVRIR_";

  @Override
  public final BuPreferences getApplicationPreferences() {
    return TrPreferences.TR;
  }

  /**
   * @return l'impl helper courant
   */
  public abstract TrImplHelperAbstract getCurrentImplHelper();

  protected abstract void ouvrirId(String _id, File _f);

  /**
   * Appelee si les preferences ont �t� modifiee.
   * Called if the prefeerences have been modified
   */
  // public abstract void applicationPreferencesChanged();
  protected abstract void creer(File _f);

  protected abstract void ouvrir(File _f);

  protected abstract void creer();

  protected abstract void activePostFrame(File _f);

}
