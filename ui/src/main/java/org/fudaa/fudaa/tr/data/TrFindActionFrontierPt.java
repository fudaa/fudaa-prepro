/**
 *  @creation     8 d�c. 2003
 *  @modification $Date: 2007-01-19 13:14:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.data;

import org.fudaa.ebli.find.EbliFindable;
import org.fudaa.fudaa.meshviewer.layer.MVFindActionFrontierPt;
import org.fudaa.fudaa.meshviewer.layer.MvFrontierPointLayer;

/**
 * @author deniger
 * @version $Id: TrFindActionFrontierPt.java,v 1.5 2007-01-19 13:14:09 deniger Exp $
 */
public class TrFindActionFrontierPt extends MVFindActionFrontierPt {

  /**
   *
   */
  public TrFindActionFrontierPt(final MvFrontierPointLayer _layer) {
    super(_layer);
  }

  @Override
  public boolean isEditableEnable(final String _searchId, final EbliFindable _parent) {
    return ((TrVisuPanel) _parent).isBcPointEditable();
  }

  @Override
  public String editSelected(final EbliFindable _parent) {
    return ((TrVisuPanel) _parent).editBcPoint();
  }
}