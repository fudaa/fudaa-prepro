/*
 *  @creation     14 nov. 2003
 *  @modification $Date: 2007-06-28 09:28:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBcManager;
import org.fudaa.dodico.h2d.reflux.H2dRefluxValue;
import org.fudaa.dodico.h2d.reflux.H2dRefluxValueCommonMutable;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dRefluxBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrCourbeTemporelleManager;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrRefluxBoundaryEditor.java,v 1.23 2007-06-28 09:28:19 deniger Exp $
 */
public class TrRefluxBoundaryEditor extends CtuluDialogPanel implements ItemListener {

  private final JComboBox cbBordType_;
  protected TrCourbeTemporelleManager evolMng_;
  private final TrRefluxValueEditorPanel[] datas_;
  private final Map bordTypeBordComportement_;
  final private JComponent txtNormal_;
  final private CtuluValueEditorDouble normalEditor_;
  private final Set varToEnableIfMixt_;
  private boolean isPointSelected_;

  public TrRefluxBoundaryEditor(final boolean _pointSelected, final Set _varsToEnableIfMixt,
      final Map _varCommonRefluxVal, final Double _commonNormale, final Map _bordTypeBordComportement,
      final H2dBoundaryType _t, final TrCourbeTemporelleManager _evolMng) {
    this(_pointSelected, _varsToEnableIfMixt, _varCommonRefluxVal, _commonNormale, _bordTypeBordComportement, _t,
        _evolMng, true);
  }

  public TrRefluxBoundaryEditor(final boolean _pointSelected, final Set _varsToEnableIfMixt,
      final Map _varCommonRefluxVal, final Double _commonNormale, final Map _bordTypeBordComportement,
      final H2dBoundaryType _bdType, final TrCourbeTemporelleManager _evolMng, final boolean _isBordEditable) {
    super();
    H2dBoundaryType bdType = _bdType;
    varToEnableIfMixt_ = _varsToEnableIfMixt;
    evolMng_ = _evolMng;
    addEmptyBorder(5);
    isPointSelected_ = _pointSelected;
    setPreferredSize(new Dimension(450, 400));
    setLayout(new BuBorderLayout(10, 10));
    cbBordType_ = new JComboBox();
    this.bordTypeBordComportement_ = _bordTypeBordComportement;
    if (_isBordEditable) {
      if (bdType == null) {
        bdType = H2dRefluxBoundaryType.MIXTE;
      }
      final DefaultComboBoxModel m = new DefaultComboBoxModel(this.bordTypeBordComportement_.keySet().toArray());
      if (bdType == H2dRefluxBoundaryType.MIXTE) {
        m.addElement(H2dRefluxBoundaryType.MIXTE);
      }
      cbBordType_.setModel(m);
      cbBordType_.setSelectedItem(bdType);
    } else {
      final DefaultComboBoxModel m = new DefaultComboBoxModel(
          new Object[] { bdType == null ? H2dRefluxBoundaryType.MIXTE : bdType });
      cbBordType_.setModel(m);
      cbBordType_.setSelectedIndex(0);
      cbBordType_.setEnabled(false);
    }
    final BuPanel pn = new BuPanel();
    pn.setLayout(new BuBorderLayout());
    pn.add(cbBordType_, BuBorderLayout.CENTER);
    add(pn, BuBorderLayout.NORTH);
    final BuPanel pnPropertiesNodale = new BuPanel();
    pnPropertiesNodale.setLayout(new BuVerticalLayout(10));
    // normale
    final BuPanel pnNormal = new BuPanel();
    pnNormal.setLayout(new BuGridLayout(2, 5, 5));
    final JLabel lbNormal = addLabel(pnNormal, TrResource.getS("Normale"));
    normalEditor_ = CtuluValueEditorDefaults.DOUBLE_EDITOR;
    txtNormal_ = normalEditor_.createEditorComponent();
    pnNormal.add(txtNormal_);
    // txtNormal= addDoubleText(pnNormal);
    txtNormal_.setToolTipText(TrResource.getS("Pour ne pas modifier les valeurs des normales, laisser ce champ vide"));
    if (_commonNormale != null) {
      normalEditor_.setValue(_commonNormale.doubleValue(), txtNormal_);
    }
    pnPropertiesNodale.add(pnNormal);
    H2dVariableType var;
    datas_ = new TrRefluxValueEditorPanel[_varCommonRefluxVal.size()];
    TrRefluxValueEditorPanel rug = null;
    H2dRefluxBcManager.BoundaryBehavior bcp = null;
    if ((bdType != null) && (bdType != H2dRefluxBoundaryType.MIXTE)) {
      bcp = (H2dRefluxBcManager.BoundaryBehavior) this.bordTypeBordComportement_.get(bdType);
    }
    int idx = 0;
    for (final Iterator it = _varCommonRefluxVal.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      var = (H2dVariableType) e.getKey();
      if (var == H2dVariableType.RUGOSITE) {
        datas_[idx] = new TrRefluxValueEditorPanel(evolMng_, null, var, (H2dRefluxValue) _varCommonRefluxVal.get(var),
            false);
        if (var == H2dVariableType.RUGOSITE) {
          rug = datas_[idx];
        }
      } else {
        datas_[idx] = new TrRefluxValueEditorPanel(evolMng_, pnPropertiesNodale, var,
            (H2dRefluxValue) _varCommonRefluxVal.get(var), var != H2dVariableType.DEBIT);
      }
      if (bcp == null) {
        datas_[idx].setEnable(_varsToEnableIfMixt.contains(datas_[idx].v_));
      } else {
        datas_[idx].setEnable(bcp.isVariableAvailable(var));
      }
      idx++;
    }
    if (rug != null) {
      rug.pn_.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray), TrResource
          .getS("Propri�t�s ar�tes")));
      add(rug.pn_, BuBorderLayout.SOUTH);
    }
    pnPropertiesNodale.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray),
        TrResource.getS("Propri�t�s nodales")));
    add(pnPropertiesNodale, BuBorderLayout.CENTER);
    // majDatasProperties();
    cbBordType_.addItemListener(this);
    doLayout();
    final Dimension commun = lbNormal.getPreferredSize();
    for (int i = datas_.length - 1; i >= 0; i--) {
      final Dimension d = datas_[i].lbTitle_.getPreferredSize();
      if (d.height > commun.height) {
        commun.height = d.height;
      }
      if (d.width > commun.width) {
        commun.width = d.width;
      }
    }
    lbNormal.setPreferredSize(commun);
    for (int i = datas_.length - 1; i >= 0; i--) {
      datas_[i].lbTitle_.setPreferredSize(commun);
    }
    doLayout();
  }

  /**
   * @return the value for the normal only if the text is not empty
   */
  public H2dRefluxValue getCommonNormalValue() {
    if (!normalEditor_.isEmpty(txtNormal_)) {
      final H2dRefluxValueCommonMutable res = new H2dRefluxValueCommonMutable(H2dBcType.PERMANENT, 0, null);
      final Object val = normalEditor_.getValue(txtNormal_);
      if (val instanceof Double) {
        res.setValuePublic(((Double) val).doubleValue());
      } else if (val instanceof CtuluExpr) {
        res.setOldExpr(((CtuluExpr) val).getParser());
      } else {
        FuLog.warning(new Throwable());
      }
      return res;
    }
    return null;
  }

  public Map getNewValues() {
    final Map r = new HashMap(datas_.length);
    for (int i = datas_.length - 1; i >= 0; i--) {
      final TrRefluxValueEditorPanel d = datas_[i];
      if (d.isEnable()) {
        r.put(d.v_, d.getValue());
      }
    }
    return r;
  }

  private void majDatasProperties() {
    final H2dBoundaryType bType = (H2dBoundaryType) cbBordType_.getSelectedItem();
    if ((bType == H2dRefluxBoundaryType.MIXTE) && (varToEnableIfMixt_ != null)) {
      for (int i = datas_.length - 1; i >= 0; i--) {
        datas_[i].setEnable(varToEnableIfMixt_.contains(datas_[i].v_));
      }
    } else {
      final H2dRefluxBcManager.BoundaryBehavior comport = (H2dRefluxBcManager.BoundaryBehavior) bordTypeBordComportement_
          .get(bType);
      if (comport == null) { return; }
      for (int i = datas_.length - 1; i >= 0; i--) {
        final H2dRefluxValue v = comport.getImposedComportement(datas_[i].v_);
        if (v != null) {
          datas_[i].forceInitWith(v);
        }
        datas_[i].setEnable(isPointSelected_ ? comport.isVariableAvailable(datas_[i].v_) : comport
            .isVariableAvailableForSegment(datas_[i].v_));
      }
    }
  }

  @Override
  public void itemStateChanged(final ItemEvent _evt) {
    if (_evt.getStateChange() != ItemEvent.SELECTED) { return; }
    if (_evt.getSource() == cbBordType_) {
      majDatasProperties();
    }
  }

  @Override
  public boolean isDataValid() {
    boolean r = true;
    for (int i = datas_.length - 1; i >= 0; i--) {
      r &= datas_[i].validate();
    }
    r &= normalEditor_.isEmpty(txtNormal_) || normalEditor_.isValueValidFromComponent(txtNormal_);
    txtNormal_.setForeground(r ? Color.BLACK : Color.RED);
    return r;
  }

  public H2dBoundaryType getSelectedBoundaryType() {
    return (H2dBoundaryType) this.cbBordType_.getSelectedItem();
  }

  public boolean isPointSelected() {
    return isPointSelected_;
  }

  public void setPointSelected(final boolean _b) {
    isPointSelected_ = _b;
  }

}
