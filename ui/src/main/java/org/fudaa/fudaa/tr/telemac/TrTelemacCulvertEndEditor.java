/**
 * @creation 2 d�c. 2004
 * @modification $Date: 2007-05-04 14:01:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import com.memoire.fu.FuLog;
import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSourceMng;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacCulvertEndEditor.java,v 1.10 2007-05-04 14:01:46 deniger Exp $
 */
public class TrTelemacCulvertEndEditor extends CtuluDialogPanel {
  CtuluCommandContainer cmd_;
  int[] endsSelected_;
  boolean[] isExtremiteDeux_;
  H2dTelemacSourceMng mng_;

  int[] selectedculverts_;
  BuTextField[] txt_;

  protected TrTelemacCulvertEndEditor(final TIntIntHashMap _selectedCulvertEndsSelected,
      final H2dTelemacSourceMng _mng, final CtuluCommandContainer _cmd) {
    if (_selectedCulvertEndsSelected == null || _selectedCulvertEndsSelected.size() == 0) { return; }
    cmd_ = _cmd;
    mng_ = _mng;
    selectedculverts_ = _selectedCulvertEndsSelected.keys();
    endsSelected_ = _selectedCulvertEndsSelected.getValues();
    final double[] initValues = mng_.getCommonSiphonEndsValues(selectedculverts_, endsSelected_);
    setLayout(new BuGridLayout(2, 5, 5));
    addLabel(TrResource.getS("Nombre de siphons concern�s"));
    addLabel(CtuluLibString.getString(_selectedCulvertEndsSelected.size()));
    final String[] txt = mng_.getExtremiteVarGeneralName();
    if (txt.length != initValues.length) {
      FuLog.warning(new Throwable());
    }
    txt_ = new BuTextField[txt.length];
    for (int i = 0; i < txt.length; i++) {
      addLabel(txt[i]);
      txt_[i] = addDoubleText(this, initValues[i] >= 0 ? Double.toString(initValues[i]) : CtuluLibString.EMPTY_STRING);
      // txt_[i].setValueValidator(BuValueValidator.MIN(0));
    }

  }

  @Override
  public boolean apply() {
    int[] initVar1 = mng_.getSiphonSource1IndexVar();
    int[] initVar2 = mng_.getSiphonSource2IndexVar();
    // les valeurs a prendre en compte
	// the values to take into account
    final Double[] values = TrTelemacSiphonEditor.getValues(txt_);
    final TIntArrayList l1 = new TIntArrayList();
    final TIntArrayList l2 = new TIntArrayList();
    final TDoubleArrayList valuesList = new TDoubleArrayList();
    // ne modifier que les valeurs voulues
    // Don't modify the wanted values
	for (int i = values.length - 1; i >= 0; i--) {
      if (values[i] != null) {
        valuesList.add(values[i].doubleValue());
        l1.add(initVar1[i]);
        l2.add(initVar2[i]);
      }
    }
    initVar1 = l1.toNativeArray();
    initVar2 = l2.toNativeArray();
    final double[] simpleValues = valuesList.toNativeArray();
    l1.add(initVar2);
    final int[] initVar3 = l1.toNativeArray();
    valuesList.add(simpleValues);
    final double[] values3 = valuesList.toNativeArray();
    final CtuluCommandComposite cmd = new CtuluCommandComposite();
    final int[] culvert = new int[1];
    for (int i = selectedculverts_.length - 1; i >= 0; i--) {
      culvert[0] = selectedculverts_[i];
      final int ends = endsSelected_[i];

      if (ends == 1) {

        mng_.modifySiphon(culvert, initVar1, simpleValues, cmd);
      } else if (ends == 2) {
        mng_.modifySiphon(culvert, initVar2, simpleValues, cmd);
      } else if (ends == 3) {
        mng_.modifySiphon(culvert, initVar3, values3, cmd);
      }
    }
    cmd_.addCmd(cmd.getSimplify());
    return true;
  }

  @Override
  public boolean isDataValid() {
    final boolean r = TrTelemacSiphonEditor.valideCulvertEndValues(txt_);
    if (!r) {
      setErrorText(TrResource.getS("Des valeurs sont erron�es"));
    }
    return r;
  }
}
