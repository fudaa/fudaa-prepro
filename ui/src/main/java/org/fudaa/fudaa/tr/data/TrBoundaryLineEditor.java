/**
 *  @creation     21 juin 2004
 *  @modification $Date: 2006-12-05 10:18:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuComboBox;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellButtonEditor;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.controle.BSelecteurAbstract;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.trace.TraceTraitRenderer;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrBoundaryLineEditor.java,v 1.22 2006-12-05 10:18:19 deniger Exp $
 */
public class TrBoundaryLineEditor implements BConfigurableInterface, BSelecteurInterface {
  private final static class ColorRenderer extends JComponent implements TableCellRenderer {

    public ColorRenderer() {
      setPreferredSize(new Dimension(6, 6));
      setSize(getPreferredSize());
    }

    @Override
    public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected,
        final boolean _hasFocus, final int _row, final int _column) {

      setForeground(((TraceLigneModel) _value).getCouleur());
      return this;
    }

    @Override
    public void paintComponent(final Graphics _g) {
      final Rectangle r = _g.getClipBounds();
      r.x++;
      r.y++;
      r.width -= 2;
      r.height -= 2;
      final Color old = _g.getColor();
      _g.setColor(getForeground());
      ((Graphics2D) _g).fill(r);
      _g.setColor(old);
    }

  }

  class DataTableModel extends AbstractTableModel {

    @Override
    public int getColumnCount() {
      return 3;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) { return TrResource.getS("bord"); }
      return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public int getRowCount() {
      return bordList_.size();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) { return bordList_.get(_rowIndex); }
      return values_.get(_rowIndex);
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return _columnIndex >= 1;
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      boolean changed = false;
      if (_value == null) { return; }
      final TraceLigneModel d = (TraceLigneModel) values_.get(_rowIndex);
      if (d == null) { return; }
      if (_columnIndex == 2) {
        final TraceLigneModel newD = (TraceLigneModel) _value;
        changed = d.setEpaisseur(newD.getEpaisseur());
      } else {
        changed = d.setColor((Color) _value);
      }
      if (changed) {
        target_.getBordTrace().savePreferencesCommon((H2dBoundaryType) bordList_.get(_rowIndex), d);
        target_.boundaryTypeRendererChanged();
      }
    }
  }

  protected class ColorEditor extends CtuluCellButtonEditor {

    int r_;

    public ColorEditor() {
      super(null);
      setPreferredSize(new Dimension(6, 6));
      setSize(getPreferredSize());
      setOpaque(true);
    }

    @Override
    protected void doAction() {
      final H2dBoundaryType t = (H2dBoundaryType) bordList_.get(r_);
      value_ = JColorChooser.showDialog(this, t.getName(), getBackground());
    }

    @Override
    public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _isSelected,
        final int _row, final int _column) {
      r_ = _row;
      return super.getTableCellEditorComponent(_table, _value, _isSelected, _row, _column);
    }

    @Override
    public void setValue(final Object _o) {
      setBackground(((TraceLigneModel) _o).getCouleur());
    }

  }

  protected static class MultiDataLigneModel extends AbstractListModel implements ComboBoxModel {

    transient TraceLigneModel selected_;

    transient TraceLigneModel[] size_;

    public MultiDataLigneModel() {
      size_ = new TraceLigneModel[9];
      float taille = 1f;
      for (int i = 0; i < size_.length; i++) {
        size_[i] = new TraceLigneModel();
        size_[i].setEpaisseur(taille);
        taille += 0.25f;
      }
    }

    @Override
    public Object getElementAt(final int _index) {
      return size_[_index];
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public int getSize() {
      return size_.length;
    }

    public void setColor(final Color _c) {
      for (int i = 0; i < size_.length; i++) {
        size_[i].setCouleur(_c);
      }
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != selected_) {
        selected_ = new TraceLigneModel((TraceLigneModel) _anItem);
      }
      fireContentsChanged(this, -1, -1);
    }
  }

  protected final static class TraitEditor extends DefaultCellEditor {

    MultiDataLigneModel model_;

    public TraitEditor() {
      super(new BuComboBox());
      final TraceTraitRenderer r = new TraceTraitRenderer();
      r.setOpaque(true);
      r.setBackground(Color.white);
      r.setBorder(BorderFactory.createEtchedBorder());
      r.setPreferredSize(new Dimension(15, 10));
      model_ = new MultiDataLigneModel();
      ((BuComboBox) editorComponent).setModel(model_);
      ((BuComboBox) editorComponent).setRenderer(r);
      editorComponent.setFocusable(false);
      editorComponent.setRequestFocusEnabled(false);
    }

    @Override
    public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _isSelected,
        final int _row, final int _column) {
      model_.setColor(((TraceLigneModel) _value).getCouleur());
      return super.getTableCellEditorComponent(_table, _value, _isSelected, _row, _column);
    }

  }

  public static String getBoundaryProperty() {
    return "BORD-TYPES";
  }

  List bordList_;

  JScrollPane sc_;

  TrBcBoundaryLayer target_;

  // List oldValues_;
  List values_;

  /**
   * @param _l le bord en question
   */
  public TrBoundaryLineEditor(final TrBcBoundaryLayer _l) {
    target_ = _l;
  }

  protected final JScrollPane createEditor() {
    if (sc_ != null) { return sc_; }
    bordList_ = target_.getAllBoundaryType();
    final int nb = bordList_.size();
    values_ = new ArrayList(nb);
    final JTable b = new JTable();
    b.setFocusable(false);
    b.setFocusCycleRoot(false);
    b.setColumnSelectionAllowed(false);
    b.setRowSelectionAllowed(true);
    final FontMetrics fm = b.getFontMetrics(b.getFont());
    int max = 10;

    final CtuluCellTextRenderer r = new CtuluCellTextRenderer();
    for (int i = 0; i < nb; i++) {
      values_.add(target_.getTlData((H2dBoundaryType) bordList_.get(i)));
      final int nm = fm.stringWidth(((H2dBoundaryType) bordList_.get(i)).getName()) + 2;
      if (nm > max) {
        max = nm;
      }
    }

    b.getTableHeader().setResizingAllowed(true);
    b.setModel(new DataTableModel());
    final TableColumnModel cm = b.getColumnModel();
    TableColumn c = cm.getColumn(0);
    c.setCellRenderer(r);
    c.setPreferredWidth(max + 10);
    c = cm.getColumn(1);
    c.setCellRenderer(new ColorRenderer());
    c.setCellEditor(new ColorEditor());
    c.setMaxWidth(25);
    c = cm.getColumn(2);
    c.setCellRenderer(new TraceTraitRenderer());
    c.setCellEditor(new TraitEditor());
    c.setMaxWidth(60);
    b.doLayout();
    sc_ = new JScrollPane(b);
    final Dimension d = sc_.getPreferredSize();
    final int w = (nb + 2) * fm.getHeight();
    d.width = b.getPreferredSize().width;
    if (d.height > w) {
      d.height = w;
    }
    sc_.setPreferredSize(d);
    return sc_;
  }

  @Override
  public boolean needAllWidth() {
    return true;
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    return new BSelecteurInterface[] { this };
  }

  @Override
  public JComponent[] getComponents() {
    return BSelecteurAbstract.createComponents(createEditor());
  }

  @Override
  public String getProperty() {
    return getBoundaryProperty();
  }

  @Override
  public BConfigurableInterface[] getSections() {
    return null;
  }

  @Override
  public BSelecteurTargetInterface getTarget() {
    return null;
  }

  @Override
  public String getTitle() {
    return TrResource.getS("Affichage des bords");
  }

  @Override
  public String getTooltip() {
    return null;
  }

  @Override
  public void reupdateFromTarget() {}

  @Override
  public void setEnabled(final boolean _b) {
    createEditor().setEnabled(_b);
  }

  @Override
  public void setSelecteurTarget(final BSelecteurTargetInterface _target) {}

  @Override
  public void stopConfiguration() {}

}