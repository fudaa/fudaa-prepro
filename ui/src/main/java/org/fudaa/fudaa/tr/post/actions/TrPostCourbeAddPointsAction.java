/*
 * @creation 1 sept. 06
 * @modification $Date: 2007-02-02 11:22:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.actions;

import com.memoire.bu.BuLib;
import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import org.fudaa.ctulu.ProgressionBuAdapter;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCourbeTreeModel;
import org.fudaa.fudaa.tr.post.TrPostCourbeTreeModelBuilder;
import org.fudaa.fudaa.tr.post.TrPostInterpolatePoint;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;

public class TrPostCourbeAddPointsAction extends EbliActionSimple {

  final FudaaCommonImplementation impl_;
  final EGGraphe model_;
  final TrPostVisuPanel panel_;

  public TrPostCourbeAddPointsAction(final FudaaCommonImplementation _impl, final EGGraphe _model,
          final TrPostVisuPanel _panel) {
    super(TrResource.getS("Ajouter les courbes pour les points sélectionnés"), BuResource.BU.getToolIcon("cible"),
            "ADD_SELECTED_NODES");
    impl_ = _impl;
    panel_ = _panel;
    model_ = _model;

  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    addNewPoints(impl_, panel_, model_);
  }

  public static void addNewPoints(final FudaaCommonImplementation _impl, final TrPostVisuPanel _panel,
          final EGGraphe _model) {
    if (_panel.isSelectionOkForEvolution()) {
      final int[] idx = _panel.getSelectionForEvolution();
      startImportIdx(_impl, _panel.getSource(), _model, idx);
    } else if (_panel.isSelectionOkForEvolutionSonde()) {
      final TrPostInterpolatePoint pt = _panel.getInterpolePointForEvol();
      startImportInterpolate(_impl, _panel.getSource(), _model, pt);
    }
  }

  public static void startImportInterpolate(final FudaaCommonImplementation _impl, final TrPostSource _src,
          final EGGraphe _model, final TrPostInterpolatePoint _pt) {
    new CtuluTaskOperationGUI(_impl, TrResource.getS("Courbes temporelles")) {
      @Override
      public void act() {
        final TrPostCourbeTreeModel model = (TrPostCourbeTreeModel) _model.getModel();
        model.addPoints(_src, _pt, _model.getCmd(), new ProgressionBuAdapter(this), null);
        BuLib.invokeLater(new Runnable() {
          @Override
          public void run() {
            _model.restore();
          }
        });
      }
    }.start();
  }

  public static void startImportIdx(final FudaaCommonImplementation _impl, final TrPostSource _src,
          final EGGraphe _model, final int[] _idx) {
    new CtuluTaskOperationGUI(_impl, TrResource.getS("Courbes temporelles")) {
      @Override
      public void act() {
        final TrPostCourbeTreeModel model = (TrPostCourbeTreeModel) _model.getModel();
        TrPostCourbeTreeModelBuilder builder = new TrPostCourbeTreeModelBuilder(model);
        builder.addPoints(_src, _idx, _model.getCmd(), new ProgressionBuAdapter(this), null);
        BuLib.invokeLater(new Runnable() {
          @Override
          public void run() {
            _model.restore();
          }
        });
      }
    }.start();
  }

  @Override
  public String getEnableCondition() {
    return TrResource.getS("Sélectionner des points dans la vue 2D<br>ou sonder un point");
  }

  @Override
  public void updateStateBeforeShow() {
    super.setEnabled(panel_ != null && panel_.isSelectionOkForEvolution());
  }
}