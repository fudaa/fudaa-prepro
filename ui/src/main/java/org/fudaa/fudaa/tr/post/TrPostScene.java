package org.fudaa.fudaa.tr.post;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.JComponent;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetEditModeListener;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetCreatorVueCalque;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetVueCalque;
import org.fudaa.ebli.visuallibrary.graphe.EbliWidgetCreatorGraphe;
import org.fudaa.ebli.visuallibrary.graphe.EbliWidgetGraphe;
import org.fudaa.fudaa.tr.post.actions.TrPostActionChangeSceneForWidget;
import org.fudaa.fudaa.tr.post.actions.TrPostActionController;
import org.fudaa.fudaa.tr.post.actions.TrPostActionFusionCalques;
import org.fudaa.fudaa.tr.post.actions.TrPostActionFusionGraphes;
import org.fudaa.fudaa.tr.post.profile.MvProfileTreeModel;
import org.netbeans.api.visual.widget.Widget;

public class TrPostScene extends EbliScene {

  TrPostProjet projet;

  public TrPostScene(final TrPostProjet _projet) {
    super();
    projet = _projet;
    setCtuluUi(_projet.getImpl());
  }

  @Override
  protected Widget attachNodeWidget(final EbliNode _node) {
    boolean isWidgetAlreadyCreated = _node.getCreator().isWidgetCreated();
    final Widget w = super.attachNodeWidget(_node);
    TrPostSource src = getSourceFromNode(_node);
    if (src != null) {
      getController().addEditListener(new EbliWidgetEditModeListener() {

        @Override
        public void editStop(Widget w, JComponent editor) {
          if (editor instanceof TrPostVisuPanel) {
            ((TrPostActionController) ((TrPostVisuPanel) editor).getController()).editStop();
          }

        }

        @Override
        public void editStart(Widget w, JComponent editor) {}

      });
    }
    if (!isWidgetAlreadyCreated) {
      // -- on remplit le node par les actions specifiques a la scene
      fillWithSpecificTrMenu(_node);
    }
    projet.getSources().nodeForSourceAdded(src);
    return w;
  }

  /**
   * l ajout d un node dans la scene trpost fait appel a cette methode qui complete els actions du widget par celles
   * specifiques du post.
   */
  public void fillWithSpecificTrMenu(final EbliNode node) {

    // -- ajout du menu sopecifique de fusion des graphes --//
    // -- cas bordures --//
    if(node==null||node.getWidget()==null) return;
    EbliWidget intern = node.getWidget().getIntern();
    // -- cas non bordures --//
    if (intern instanceof EbliWidgetGraphe) {
      new TrPostActionFusionGraphes((EbliWidgetGraphe) intern, node, projet);
    } else if (intern instanceof EbliWidgetVueCalque) {
      new TrPostActionFusionCalques((EbliWidgetVueCalque) intern, node, projet);
    }
    // -- PREPRO-11 on ajoute l'action de deplace ment dans un autre layout --//
    if (node.getWidget() instanceof EbliWidget) {
      EbliWidget widg = (EbliWidget) node.getWidget();
      widg.getController().getPopup().add(new TrPostActionChangeSceneForWidget(node, projet));
    }

  }

  /**
   * Methode qui retourne la collection plac�e en p�ram d entree de la scene.
   * 
   * @param collection
   */
  public final void removeAllNode(Collection<EbliNode> collection) {

    for (EbliNode node : collection) {
      this.removeNode(node);
      this.refresh();
    }
  }

  /**
   * Supprime toutes les widgets dont le csource figure dans leur donn�es.
   * 
   * @param src
   */
  public void removeAllWidgetLinkedToSrc(TrPostSource src) {
    List<EbliNode> listTodelete = new ArrayList<EbliNode>();
    for (Object objet : getObjects()) {
      if (objet instanceof EbliNode) {
        EbliNode node = (EbliNode) objet;
        // -- cas calque
        if (node.getCreator() instanceof EbliWidgetCreatorVueCalque) {
          TrPostVisuPanel panel = (TrPostVisuPanel) ((EbliWidgetCreatorVueCalque) node.getCreator()).getCalquesPanel();
          if (panel.getSource() == src) {
            // -- on degage cette widget --//
            listTodelete.add(node);
          }
        }
        if (node.getCreator() instanceof EbliWidgetCreatorGraphe) {
          EGGraphe graphe = ((EbliWidgetCreatorGraphe) node.getCreator()).getGraphe();
          if (graphe.getModel() instanceof MvProfileTreeModel) {
            MvProfileTreeModel model = (MvProfileTreeModel) graphe.getModel();
            if (model != null && model.target_ != null) if (model.target_.getData() != null
                && model.target_.getData() instanceof TrPostSource) {
              TrPostSource sourceGraphe = (TrPostSource) model.target_.getData();
              if (sourceGraphe == src)
              // -- on degage cette widget --//
              listTodelete.add(node);
            }
          }
        }
      }
    }
    // -- on supprime la liste des node --//
    removeAllNode(listTodelete);
  }

  TrPostSource getSourceFromNode(EbliNode node) {
    if (node.getCreator() instanceof EbliWidgetCreatorVueCalque) {
      EbliWidgetCreatorVueCalque vueCalque = (EbliWidgetCreatorVueCalque) node.getCreator();
      if (vueCalque.getCalquesPanel() instanceof TrPostVisuPanel) { return ((TrPostVisuPanel) vueCalque
          .getCalquesPanel()).getSource(); }
    }
    return null;
  }

  @Override
  protected void detachNodeWidget(EbliNode node, Widget widget) {
    super.detachNodeWidget(node, widget);
    TrPostSource src = getSourceFromNode(node);
    if (src != null) {
      projet.getSources().nodeForSourceRemoved(src);
    }
  }
}
