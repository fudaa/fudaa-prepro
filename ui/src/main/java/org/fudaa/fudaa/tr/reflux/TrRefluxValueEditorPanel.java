/**
 *  @creation     27 nov. 2003
 *  @modification $Date: 2007-05-04 14:01:52 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.dodico.h2d.reflux.H2dRefluxParameters;
import org.fudaa.dodico.h2d.reflux.H2dRefluxValue;
import org.fudaa.dodico.h2d.reflux.H2dRefluxValueCommonMutable;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.fudaa.tr.common.TrCourbeTemporelleManager;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * Classe gerant un label, un combobox pour la nature du bord et un textfield pour la valeur (si nature permanente) et
 * un combobox pour le cas transitoire.
 */
class TrRefluxValueEditorPanel implements ItemListener {

  private final TrCourbeTemporelleManager evolMng_;

  BuLabel lbTitle_;

  JComboBox cbType_;

  CtuluValueEditorDouble doubleValue_;

  JComponent txtValue_;

  JComboBox cbEvol_;

  JPanel pn_;

  H2dVariableType v_;

  boolean freeAllowed_;

  public TrRefluxValueEditorPanel(final TrCourbeTemporelleManager _evolMng, final JPanel _target,
      final H2dVariableType _v, final H2dRefluxValue _val, final boolean _libre) {
    this.pn_ = new BuPanel();
    this.evolMng_ = _evolMng;
    freeAllowed_ = _libre;
    pn_.setLayout(new BuGridLayout(3, 5, 5));
    if (_target != null) {
      _target.add(this.pn_);
    }
    v_ = _v;
    lbTitle_ = new BuLabel(v_.getName());
    lbTitle_.setVerticalAlignment(SwingConstants.CENTER);
    this.pn_.add(lbTitle_);
    cbType_ = new BuComboBox();
    final CtuluPermanentList l = _libre ? H2dRefluxParameters.CL_TYPE : H2dRefluxParameters.CL_SET_TYPE;
    final List list = new ArrayList(l);
    if ((_val != null) && (_val.getType() == H2dBcType.MIXTE)) {
      list.add(H2dBcType.MIXTE);
    }
    cbType_.setModel(new DefaultComboBoxModel(list.toArray()));
    this.pn_.add(cbType_);

    String tltip = "<html><body style=\"margin:2px\">"
        + TrResource.getS("Laisser le champ vide pour ne pas modifier les valeurs") + "<br>"
        + TrResource.getS("Etat initial:") + "<br>&nbsp;";
    if (_val == null) {
      majPermanentState(null);
      tltip += cbType_.getSelectedItem().toString();
    } else {
      forceInitWith(_val);
      tltip += cbType_.getSelectedItem().toString();
      if (_val.getType() == H2dBcType.TRANSITOIRE && cbEvol_.getSelectedItem() != null) {
        tltip += "<br>&nbsp;" + TrResource.getS("Evolution:") + cbEvol_.getSelectedItem().toString();
      } else if (_val.getType() == H2dBcType.PERMANENT && _val.isDoubleValueConstant()) {
        tltip += "<br>&nbsp;" + TrResource.getS("Valeur:") + _val.getValue();
      }
      tltip += "</body></html>";
    }
    lbTitle_.setToolTipText(tltip);
    cbType_.addItemListener(this);
  }

  public final void forceInitWith(final H2dRefluxValue _val) {
    final DefaultComboBoxModel m = (DefaultComboBoxModel) cbType_.getModel();
    final H2dBcType t = _val.getType();
    if (t == null) {
      cbType_.setSelectedIndex(-1);
    } else {
      if (m.getIndexOf(t) < 0) {
        m.addElement(t);
        if (cbType_.getSelectedItem() != t) {
          cbType_.setSelectedItem(t);
        }
      } else {
        cbType_.setSelectedItem(t);
      }
    }
    majPermanentState(_val);
  }

  CtuluExpr isPermanentAndFormule() {
    if (cbType_.getSelectedItem() == H2dBcType.PERMANENT) {
      final Object r = doubleValue_.getValue(txtValue_);
      if (r instanceof CtuluExpr) { return (CtuluExpr) r; }
    }
    return null;
  }

  H2dRefluxValue getValue() {
    final H2dBcType type = (H2dBcType) cbType_.getSelectedItem();
    final H2dRefluxValueCommonMutable r = new H2dRefluxValueCommonMutable(type, 0, null);
    r.setDoubleValueConstant(false);
    if (type == H2dBcType.PERMANENT && (txtValue_ != null) && doubleValue_ != null
        && (!doubleValue_.isEmpty(txtValue_))) {
      r.setDoubleValueConstant(true);
      final Object res = doubleValue_.getValue(txtValue_);
      if (res instanceof Double) {
        r.setValuePublic(((Double) res).doubleValue());
      } else {
        r.setOldExpr(((CtuluExpr) res).getParser());
      }
    } else if (type == H2dBcType.TRANSITOIRE && (cbEvol_ != null) && (this.evolMng_ != null)
        && (this.evolMng_.getNbEvolCommonAndFor(v_) > 0)) {
      final EvolutionReguliere e = (EvolutionReguliere) cbEvol_.getSelectedItem();
      r.setEvolution((e == EvolutionReguliere.MIXTE) ? null : e);
    }
    return r;
  }

  JComponent iniTxtValue() {
    if (doubleValue_ == null) {
      doubleValue_ = CtuluValueEditorDefaults.DOUBLE_EDITOR;
    }
    return doubleValue_.createCommonEditorComponent(true);
  }

  boolean validate() {
    boolean r = true;
    if (cbType_.getSelectedItem() == H2dBcType.PERMANENT) {
      r = doubleValue_.isEmpty(txtValue_) || doubleValue_.isValueValidFromComponent(txtValue_);
    } else {
      r = (cbType_.getSelectedItem() != H2dBcType.TRANSITOIRE)
          || (((evolMng_ != null) && (evolMng_.getNbEvolCommonAndFor(v_) > 0)));
    }
    lbTitle_.setForeground(r ? Color.BLACK : Color.RED);
    return r;
  }

  boolean isEnable() {
    return lbTitle_.isEnabled();
  }

  void setEnable(final boolean _b) {
    lbTitle_.setEnabled(_b);
    cbType_.setEnabled(_b);
    if (txtValue_ != null) {
      txtValue_.setEnabled(_b);
    }
    if (cbEvol_ != null) {
      cbEvol_.setEnabled(_b);
    }
    if (_b && !freeAllowed_) {
      final DefaultComboBoxModel m = (DefaultComboBoxModel) cbType_.getModel();
      final int i = m.getIndexOf(H2dBcType.LIBRE);
      if (i >= 0) {
        m.removeElementAt(i);
        cbType_.setSelectedItem(H2dBcType.PERMANENT);
      }
    }
  }

  JComboBox initCbEvol(final H2dRefluxValue _val) {
    final BuComboBox r = new BuComboBox();
    // lors de l'initialisation, si tout les points sont transitoires on
    // peut ajouter l'evolution mixte ( laisser les evolutions inchangees).
    if ((_val != null) && (_val.getType() == H2dBcType.TRANSITOIRE) && (!_val.isEvolutionFixed())) {
      evolMng_.createComboBoxModelWithEmpty(EvolutionReguliere.MIXTE, v_, r);
    } else {
      evolMng_.createComboBoxModel(v_, r);
    }
    return r;
  }

  private void majPermanentState(final H2dRefluxValue _v) {
    if (cbType_.getSelectedItem() == H2dBcType.TRANSITOIRE) {
      if (txtValue_ != null) {
        this.pn_.remove(txtValue_);
      }
      if (cbEvol_ == null) {
        cbEvol_ = initCbEvol(_v);
      }
      if (_v != null && _v.getEvolution() != null) {
        cbEvol_.setSelectedItem(_v.getEvolution());
        // else cbEvol.setSelectedItem(H2dEvolution.MIXTE);
      }
      this.pn_.add(cbEvol_);
    } else if (cbType_.getSelectedItem() == H2dBcType.PERMANENT) {
      if (cbEvol_ != null) {
        this.pn_.remove(cbEvol_);
      }
      if (txtValue_ == null) {
        txtValue_ = iniTxtValue();
      }
      this.pn_.add(txtValue_);
      if ((_v != null) && (_v.isDoubleValueConstant())) {
        doubleValue_.setValue(_v.getValue(), txtValue_);
        /*
         * else doubleValue.setValue(null, txtValue);
         */
      }
    } else {
      if (txtValue_ != null) {
        pn_.remove(txtValue_);
      }
      if (cbEvol_ != null) {
        pn_.remove(cbEvol_);
      }
    }
    this.pn_.revalidate();
    this.pn_.repaint();
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if ((_e.getStateChange() == ItemEvent.SELECTED) && (_e.getSource() == cbType_)) {
      majPermanentState(null);
    }
  }
}