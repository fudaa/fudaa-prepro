package org.fudaa.fudaa.tr.post;

import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTime;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModelDefault;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimePersistBuilder;

/**
 * @author deniger
 */
public class TrPostScopeTimeCourbePersistBuilder extends FudaaCourbeTimePersistBuilder {

  @Override
  public boolean saveXY() {
    return true;
  }

  @Override
  protected FudaaCourbeTime createEGObject(EGCourbePersist target, Map params, CtuluAnalyze log) {
    EGGroup parent = getGroup(params);
    EGModel createModel = createModel(target, params);
    if (createModel == null) {
      return null;
    }
    double[] times = new double[createModel.getNbValues()];
    for (int i = 0; i < times.length; i++) {
      times[i] = createModel.getX(i);
    }
    return new TrPostScopeTimeCourbe(parent, createModel, new FudaaCourbeTimeListModelDefault(times, null));
  }

}
