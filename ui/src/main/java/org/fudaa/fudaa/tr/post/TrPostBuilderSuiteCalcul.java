package org.fudaa.fudaa.tr.post;

import gnu.trove.TDoubleArrayList;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.*;
import javax.swing.Icon;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.SceneHelper;
import org.fudaa.ebli.visuallibrary.calque.CalqueLegendeWidgetAdapter;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostSourceReaderComposite.CoupleTimeStepData;

/**
 * Builder qui construit les suites de calcul.
 *
 * @author Adrien Hadoux
 */
public class TrPostBuilderSuiteCalcul {

  /**
   * Action qui r�alise la suite de calcul.
   *
   * @author Adrien Hadoux
   */
  public static class ActionBuildSuite extends EbliActionSimple {

    TrPostSource src_;
    TrPostProjet proj;

    public ActionBuildSuite(final TrPostSource src, final String _name, final Icon _ic, final String _ac, final TrPostProjet proj) {
      super(_name, _ic, _ac);
      src_ = src;
      src_ = src;
      this.proj = proj;
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {

      // -- test de comptabilit� de fichier source --//
      if (!isCompatibleSuiteCalcul(src_)) {
        proj.getImpl().error(TrResource.getS("Le fichier n'est pas valide."));
        return;
      }

      // -- on lance la dialog de choix des fichiers sources --//
      final TrPostSource srcChoisie = proj.getChooserMultiSources(-1);
      if (srcChoisie == null) {
        proj.getImpl().error(TrResource.getS(TrResource.getS("Aucune source choisie")));
        return;
      }

      if (!isCompatibleSuiteCalcul(srcChoisie)) {
        proj.getImpl().error(TrResource.getS("Le fichier n'est pas valide."));
        return;
      }

      final List<String> error = new ArrayList<String>();

      // -- creation de la suite de calcul --//
      final TrPostSource suiteCalcul = createSuiteCalcul((TrPostSourceFromReader) src_, proj, (TrPostSourceFromReader) srcChoisie, error);

      if (suiteCalcul == null) {
        // -- il se passe des choses �tranges.... --//
        String maxiStringLeo = "";
        for (final String err : error) {
          maxiStringLeo += err + "\n";
        }

        proj.getImpl().error(maxiStringLeo);
        return;
      }

      // -- on ajoute la nouvelle src r�sultat comme �tant une suite de calcul --//

      proj.getImpl().project.addSource(suiteCalcul, suiteCalcul.getTitle());

      // -- on cree la vue 2d correspondante --//

      final TrPostLayoutPanelController controller = proj.getImpl().getCurrentLayoutFille().controller_;
      final CalqueLegendeWidgetAdapter legendeCalque = new CalqueLegendeWidgetAdapter(controller.getSceneCourante());
      final TrPostVisuPanel pnVisu = new TrPostVisuPanel(proj.getImpl(), proj, legendeCalque, suiteCalcul);
      final EbliNode node = controller.addCalque(TrResource.getS("Calque") + (controller.getSceneCourante().getAllVue2d().size() + 1),
              SceneHelper.getOptimalePosition(proj.getImpl().getCurrentLayoutFille().getScene()), pnVisu.getPreferredSize(), pnVisu);
      // -- ajout de l INFO de la source utilis�e --//
      node.setDescription(suiteCalcul.getTitle());

      // -- ajout des infos de cr�ation --//

      ((TrPostSourceReaderComposite) ((TrPostSourceFromReader) suiteCalcul).getReader()).fillInfosWithComposite(pnVisu.getInfosCreation());

    }
  }

  /**
   * R�cup�rer l'interface � partir du fichier
   *
   * @param _f
   * @param _impl
   * @return
   */
  public static TrPostSourceFromReader createSourceFromeFile(final File _f, final TrPostCommonImplementation _impl) {
    final TrPostSource src = TrPostSourceBuilder.activeSourceAction(_f, _impl, _impl.getMainProgression());
    if (src != null && src instanceof TrPostSourceFromReader) {
      return ((TrPostSourceFromReader) src);
    }
    return null;

  }

  public static TrPostSource createSuiteCalcul(final File _f, final TrPostProjet proj, final TrPostSourceFromReader _reader, final List<String> error) {
    // -- lecture de l'interface � ajouter --//
    final TrPostSourceFromReader srcToConcat = createSourceFromeFile(_f, proj.getImpl());
    return createSuiteCalcul(_reader, proj, srcToConcat, error);
  }

  /**
   * Algorithme de construction d'une suite de calcul.
   *
   * @param srcToConcat
   * @param proj
   * @param _reader
   * @param error
   * @return
   */
  public static TrPostSource createSuiteCalcul(final TrPostSourceFromReader srcToConcat, final TrPostProjet proj,
          final TrPostSourceFromReader _reader, final List<String> error) {

    if (srcToConcat == null) {
      error.add(TrResource.getS("Le fichier n'est pas valide."));
      return null;
    }

    if (_reader.isVolumique() || srcToConcat.isVolumique()) {
      error.add(TrResource.getS("Format Rubar non g�r�"));
      return null;
    }

    // -- comparaison des grilles --//
    final EfGridInterface grille = srcToConcat.getGrid();
    if (!grille.isSameStrict(_reader.getGrid(), proj.getImpl().getMainProgression(), 0.01)) {
      error.add(TrResource.getS("Les grilles ne sont pas identiques"));
      return null;
    }

    // -- comparaison des pas de temps d�but et finaux --//
    final double[] time1 = TrPostSourceFromReader.getTimes(_reader.getReader());
    final double[] time2 = TrPostSourceFromReader.getTimes(srcToConcat.getReader());

    if (time1[time1.length - 1] != time2[0] && time2[time2.length - 1] != time1[0]) {
      error.add(TrResource.getS("Le dernier pas de temps de l'un doit �tre �quivalent au premier pas de temps de l'autre."));
      return null;
    }
    // attention, tu ajoutes des variables cr�es par l'interface !
    final HashSet<H2dVariableType> commonInitList = new HashSet<H2dVariableType>();
    // -- intersection des variables des 2 fichiers --//
    final H2dVariableType[] initVar = _reader.getAvailableVar();
    // final HashSet<H2dVariableType> destVar = new
    // HashSet<H2dVariableType>(Arrays.asList(srcToConcat.getAvailableVar()));
    for (int i = 0; i < initVar.length; i++) {
      final H2dVariableType variableType = initVar[i];
      // on ne prend pas en compte les variables cr��es par l'utilisateur
      if (_reader.isInitVar(variableType) && srcToConcat.isInitVar(variableType)) {
        commonInitList.add(variableType);
      }
    }
    final H2dVariableType[] var = commonInitList.toArray(new H2dVariableType[commonInitList.size()]);
    final TrPostUserVariableSaver userVar = TrPostUserVariableSaver.createSaver(_reader, null).getSaver();

    // var=srcToConcat.variable_;

    // -- initialisation de la liste de source --//
    final List<TrPostSourceReaderInterface> listeSourceInterface = new ArrayList<TrPostSourceReaderInterface>();

    // -- on ajoute � la liste des interfaces l'interface 1 et l'intrface 2 --//
    final TrPostSourceReaderInterface interface1 = _reader.reader_;
    final TrPostSourceReaderInterface interface2 = srcToConcat.reader_;

    // -- attetion au cas particulier: si une interface1 est deja une suite, il faut ajouter toutes ses interfaces --//
    if (interface1 instanceof TrPostSourceReaderComposite) {
      listeSourceInterface.addAll(((TrPostSourceReaderComposite) interface1).listeSourceInterface_);
    } else {
      listeSourceInterface.add(interface1);
    }

    if (interface2 instanceof TrPostSourceReaderComposite) {
      listeSourceInterface.addAll(((TrPostSourceReaderComposite) interface2).listeSourceInterface_);
    } else {
      listeSourceInterface.add(interface2);
    }

    // -- ajout de tous les fichiers dans la collection --//
    final Collection<File> file_ = new HashSet<File>(interface1.getFiles());;
    file_.addAll(interface2.getFiles());

    // -- on trie les listes dans l'ordre des timestep --//
    Collections.sort(listeSourceInterface, new ComparateurTrPostSourceReaderInterface());

    // - ajout des correspondances timestep/source avec nouveau timestep --//
    final Map<Integer, CoupleTimeStepData> mapTimeStep_ = new HashMap<Integer, CoupleTimeStepData>();
    // TODO utilise TDoubleArrayList
    final TDoubleArrayList times = new TDoubleArrayList();
    // int cpt = 0;
    for (final TrPostSourceReaderInterface data : listeSourceInterface) {
      for (int i = 0; i < data.getNbTimeStep(); i++) {
        // LOGIquement, il faudrait prendre en compte les erreurs d'arrondi
        // -- ICI, pour cet algo, on doit degager tout les doublons et ne conserver que des valeurs uniques --//
        if (!times.contains(data.getTimeStep(i))) {
          // le cpt vaut la taille des times
          mapTimeStep_.put(times.size(), new CoupleTimeStepData(data, i));
          times.add(data.getTimeStep(i));
          // cpt++;
        }
      }
    }

    final double[] timeTotal = times.toNativeArray();
    // -- creation de la suite de calcul
    final TrPostSourceReaderComposite suite = new TrPostSourceReaderComposite(timeTotal, var, mapTimeStep_, listeSourceInterface, file_);
    final TrPostSource resultat = new TrPostSourceFromReader(suite, TrResource.getS("Suite de calcul"), grille, proj.getImpl());
    resultat.buildDefaultVarUpdateLists();
    final CtuluAnalyze analyze = new CtuluAnalyze();
    userVar.restore(resultat, analyze, null, proj.getImpl(), proj.getSources(), null);
    proj.deliverSourceId(resultat);
    proj.getImpl().manageAnalyzeAndIsFatal(analyze);
    // -- creation du source Reader --//
    return resultat;
  }

  /**
   * Methode qui se charge de construire la suite de calcul a partir de la liste des id des fichiers.
   *
   * @param listeIdSrc
   * @param projet
   * @return
   */
  public static TrPostSource createSuiteCalculFromPersistance(final List<String> listeIdSrc, final TrPostProjet projet, final String idSrc) {
    final TrPostCommonImplementation impl = projet.getImpl();
    // - on met proprement en forme les donn�es --//
    final List<TrPostSourceFromReader> listeReader = new ArrayList<TrPostSourceFromReader>();
    for (final String id : listeIdSrc) {

      final TrPostSource srcChoisie = projet.getSources().findSourceById(id);
      if (srcChoisie == null) {
        impl.error(TrResource.getS(TrResource.getS("Le fichier r�sultat d'id " + id + " est introuvable.\n Suite de calcul interrompue")));
        return null;
      }
      if (!isCompatibleSuiteCalcul(srcChoisie)) {
        impl.error(TrResource.getS("Le fichier d'id " + id + " n'est pas valide pour la suite de calcul."));
        return null;
      }
      listeReader.add((TrPostSourceFromReader) srcChoisie);
    }

    // -- on demarre la suite de calcul --//
    final Iterator<TrPostSourceFromReader> it = listeReader.iterator();
    TrPostSourceFromReader suiteProgressive = it.next();

    final List<String> error = new ArrayList<String>();
    while (it.hasNext()) {
      // -- on calcule la suite progressive que l'on stocke dans la suite progressive --//
      final TrPostSourceFromReader suite = (TrPostSourceFromReader) createSuiteCalcul(suiteProgressive, projet, it.next(), error);
      if (suite == null) {
        String maxiStringLeo = "";
        for (final String err : error) {
          maxiStringLeo += err + "\n";
        }
        if (suiteProgressive != null) {
          maxiStringLeo += "La suite de calcul s'arrete donc avec les fichiers " + suiteProgressive.getTitle();
          suiteProgressive.setId(idSrc);
          projet.addSource(suiteProgressive, suiteProgressive.getTitle());
        }

        impl.error(maxiStringLeo);
        return suiteProgressive;
      } else {
        suiteProgressive = suite;
      }

    }

    // -- trigger signal modif --//
    suiteProgressive.setId(idSrc);
    projet.addSource(suiteProgressive, suiteProgressive.getTitle());
    return suiteProgressive;
  }

  /**
   * Methode appelee pour v�rifier que ceci est bien compatible avec la suite de calcul.
   *
   * @param src
   * @return
   */
  public static boolean isCompatibleSuiteCalcul(final TrPostSource src) {

    if (src != null && src instanceof TrPostSourceFromReader) {
      return true;
    }
    return false;
  }

  public static class ComparateurTrPostSourceReaderInterface implements Comparator<TrPostSourceReaderInterface> {

    @Override
    public int compare(final TrPostSourceReaderInterface o1, final TrPostSourceReaderInterface o2) {

      // -- on compare selon la valeur des timesteps --
      if (o1.getTimeStep(0) > o2.getTimeStep(0)) {
        return 1;
      }

      if (o1.getTimeStep(0) < o2.getTimeStep(0)) {
        return -1;
      }

      return 0;
    }
  }
}
