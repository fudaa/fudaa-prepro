/**
 * @creation 25 mars 2004
 * @modification $Date: 2007-06-14 12:01:40 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluExportDataInterface;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.animation.ActionAnimationTreeSelection;
import org.fudaa.ebli.animation.EbliAnimatedInterface;
import org.fudaa.ebli.animation.EbliAnimationSourceInterface;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.calque.action.CalqueActionSonde;
import org.fudaa.ebli.calque.action.EbliCalqueActionTimeChooser;
import org.fudaa.ebli.calque.action.EbliCalqueActionVariableChooser;
import org.fudaa.ebli.calque.edition.ZCalqueEditionGroup;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.calque.CalqueLegendeWidgetAdapter;
import org.fudaa.fudaa.meshviewer.MvSelectionNodeOrEltData;
import org.fudaa.fudaa.meshviewer.export.MvExportFactory;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.meshviewer.layer.MvFrontierLayerAbstract;
import org.fudaa.fudaa.meshviewer.layer.MvGridLayerGroup;
import org.fudaa.fudaa.meshviewer.layer.MvNodeNumberLayer;
import org.fudaa.fudaa.meshviewer.model.MvNodeModelDefault;
import org.fudaa.fudaa.tr.common.Tr3DFactory;
import org.fudaa.fudaa.tr.common.Tr3DInitialiser;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrBcBoundaryLayer;
import org.fudaa.fudaa.tr.data.TrVisuPanel;
import org.fudaa.fudaa.tr.export.TrExportFactory;
import org.fudaa.fudaa.tr.post.actions.*;
import org.fudaa.fudaa.tr.post.data.TrPostDataListener;
import org.fudaa.fudaa.tr.reflux.TrRefluxImplHelper;
import org.fudaa.fudaa.tr.rubar.TrRubarImplHelper;
import org.fudaa.fudaa.tr.telemac.TrTelemacImplHelper;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: TrPostVisuPanel.java,v 1.59 2007-06-14 12:01:40 deniger Exp $
 */
@SuppressWarnings("serial")
public class TrPostVisuPanel extends TrVisuPanel implements TrPostDataListener, EbliAnimatedInterface, CtuluExportDataInterface, TimeStepFormatAware,
    ListDataListener {
  private class ClearSonde extends EbliActionSimple {
    protected ClearSonde() {
      super(TrResource.getS("Effacer les sondes"), EbliResource.EBLI.getIcon("crystal_effacer"), "CLEAR_SOUNDS");
      setDefaultToolTip(TrResource.getS("Efface toutes les sondes couramment utilis�es"));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if ((TrPostVisuPanel.this.getCalqueActif() != null) && (TrPostVisuPanel.this.getCalqueActif() instanceof ZCalqueSondeInterface)) {
        ((ZCalqueSondeInterface) TrPostVisuPanel.this.getCalqueActif()).clearSonde();
      }
    }

    @Override
    public String getEnableCondition() {
      return TrResource.getS("S�lectionner un calque comportant des sondes");
    }

    @Override
    public void updateStateBeforeShow() {
      if ((TrPostVisuPanel.this.getCalqueActif() != null) && (TrPostVisuPanel.this.getCalqueActif() instanceof ZCalqueSondeInterface)) {
        setEnabled(true);
      }
    }
  }

  private class EvolutionAction extends EbliActionSimple {
    protected EvolutionAction() {
      super(TrResource.getS("Afficher les �volutions"), EbliResource.EBLI.getIcon("curves"), "DISPLAY_EVOLS");
      setDefaultToolTip(TrResource.getS("Affiche les �volutions temporelles sur la s�lection ou sur l'interpolation"));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      addEvolutionFor();
    }

    @Override
    public String getEnableCondition() {
      return TrResource.getS("S�lectionner au moins un objet ou activer la sonde");
    }

    @Override
    public void updateStateBeforeShow() {
      if ((TrPostVisuPanel.this.getCalqueActif() != null) && (!TrPostVisuPanel.this.getCalqueActif().isGroupeCalque())) {
        setEnabled(true);
      }
    }
  }

  public static class ExportAction extends EbliActionSimple {
    TrPostVisuPanel panel;
    final CtuluUI ui;

    ExportAction(TrPostVisuPanel pn, final CtuluUI impl) {
      super(TrResource.getS("Exporter"), TrResource.TR.getToolIcon("crystal_exporter"), "EXPORTDATA");
      panel = pn;
      ui = impl;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      panel.startExport(ui);
    }
  }

  public static TrPostVisuPanel buildVisuPanelForWidgets(final TrPostProjet projet, final TrPostCommonImplementation impl,
                                                         final TrPostSource source) {
    // -- creation de la legende --//
    final CalqueLegendeWidgetAdapter legendeCalque = new CalqueLegendeWidgetAdapter(impl.getCurrentLayoutFille().getScene());

    final TrPostVisuPanel pnVisu = new TrPostVisuPanel(impl, projet, source.getFlecheListModel().getSize() == 0, legendeCalque,
        source);
    return pnVisu;
  }

  /**
   * @param _impl l'implementation
   * @param _src la source
   * @param _selection la selection
   * @param pn peut etre null.//TODO WARN
   */
  public static void startExport(final CtuluUI _impl, final TrPostSource _src, final MvSelectionNodeOrEltData _selection,
                                 TrVisuPanel pn) {
    TrExportFactory fac = null;
    if ((_src instanceof TrPostSourceTelemac3D)
        && CtuluLibDialog.showConfirmation(_impl.getParentComponent(), "TELEMAC 3D",
        TrResource.getS("Voulez-vous exporter votre projet dans son format 3D initial ?"),
        TrLib.getString("Exporter en 3D"),
        TrLib.getString(" Rester en 2D"))) {
      fac = new TrExportFactory(_impl, TrTelemacImplHelper.getID(), ((TrPostSourceTelemac3D) _src).getInit3DSource(), pn);
      fac.setInitIpobo(((TrPostSourceTelemac3D) _src).getIpobo());
      fac.setIparams(((TrPostSourceTelemac3D) _src).getIparams());
      fac.setIdate(((TrPostSourceTelemac3D) _src).getIdate());
      if (_selection != null) {
        _selection.idxSelected_ = null;
      }
    }
    if (_src.containsElementVar()) {
      fac = new TrExportFactory(_impl, TrRubarImplHelper.getID(), _src.getNewVarListModel(), _src.getNewTimeListModel(),
          new TrPostSourceRubarNodeBathyAdapter((TrPostSourceRubar) _src), _src.getInterpolator().getVect(),
          pn, null, TrExportFactory.getNbConcentrationBlocks(_src));
    }
    if (fac == null) {
      fac = new TrExportFactory(_impl,
          (_src instanceof TrPostSourceReflux || _src instanceof TrPostSourceReaderReflux) ? TrRefluxImplHelper.getID()
              : null, _src, pn);
    }
    if (_src instanceof TrPostSourceSerafin) {
      fac.setIdate(((TrPostSourceSerafin) _src).getReaderSerafin().getReferenceDateInMillis());
    }
    MvExportFactory.startExport(fac, _impl, _selection);
  }

  CalqueActionSonde actionSonde = null;
  ActionAnimationTreeSelection anim_;
  TrPostInfoDelegate info_;
  protected TrIsoLayer isoLayer_;
  protected TrPostProjet projet_;
  public boolean simplifiedInit;
  /**
   * Tres important!!! c'est la source utilisee par le trpostvisupanel. Il est automatiquement plac� en param du constructeur. Si rien n est mis apr
   * defaut ce sera le source 0, donc le cas ou il y a une seule simul.
   */
  private final TrPostSource source_;

  public TrPostVisuPanel(final TrPostCommonImplementation _impl, final TrPostProjet _projet, final BCalqueLegende _leg,
                         final TrPostSource _src) {
    this(_impl, _projet, false, _leg, _src);
  }

  /**
   * @param _impl l'impl parent
   * @param _controller le controller contenant la source
   * @param _src : l indice de la source courante utilis�e
   */
  public TrPostVisuPanel(final TrPostCommonImplementation _impl, final TrPostProjet _controller, final boolean _simplified,
                         final BCalqueLegende _leg, final TrPostSource _src) {
    super(new TrPostActionController(_impl));

    // -- mise a jour de l indice de la source courante --//
    source_ = _src;
    projet_ = _controller;
    info_ = new TrPostInfoDelegate(source_, getEbliFormatter());
    addCqInfos(source_.getGrid());
    if (_leg != null) {
      addCqLegende(_leg);
    }
    addCqMaillage(source_.getGrid(), info_);
    final MvGridLayerGroup gr = getGridGroup();
    gr.getPointLayer().setVisible(false);
    MvElementLayer polygonLayer = gr.getPolygonLayer();
    BCalqueCacheManager.installDefaultCacheManager(polygonLayer);
    polygonLayer.setVisible(false);
    buildFrontiereLayer();
    isoLayer_ = buildIsoLayer();
    final EbliActionInterface[] action = new EbliActionInterface[]{addActionEvolutionFor(),
        new TrIsoLineAction(this), getClearSondeAction()};
    simplifiedInit = _simplified;
    if (!_simplified) {
      final TrPostFlecheLayer fleche = new TrPostFlecheLayer(source_);
      fleche.setVisible(false);
      fleche.setName("cqFleche");
      fleche.setDestructible(false);
      fleche.setLegende(getCqLegend());
      addCalque(fleche);
      addCalqueActions(fleche, action);
    }
    addCalque(isoLayer_);

    if (source_.containsElementVar()) {
      addCalqueActions(getGridGroup().getPolygonLayer(), action);
    } else {
      addCalqueActions(getGridGroup().getPointLayer(), action);
    }
    addCalqueActions(isoLayer_, action);
    final BGroupeCalque gc = buildGroupeFond();
    getDonneesCalque().enDernier(gc);
    gc.putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Affichage des images de fond"));
    getArbreCalqueModel().setSelectionCalque(isoLayer_);
    remove(getSuiviPanel());
    _src.fillWithSourceCreationInfo(CtuluLibString.EMPTY_STRING, getInfosCreation());
    _controller.registerListenerTo(_src, this);
    _controller.registerTimeListenerTo(_src, this);
  }

  protected EbliActionInterface addActionEvolutionFor() {
    return new EvolutionAction();
  }

  public void addEvolutionFor() {
    if (isSelectionOkForEvolution()) {
      final ZCalqueAffichageDonnees calque = (ZCalqueAffichageDonnees) getCalqueActif();

      final int[] idx = calque.getLayerSelection().getSelectedIndex();
      H2dVariableType selectedVar = null;
      if (calque instanceof TrIsoLayer) {
        selectedVar = ((TrIsoLayer) calque).getIsoModel().getVariableSelected();
      } else if (calque instanceof TrPostFlecheLayer) {
        selectedVar = ((TrPostFlecheLayer) calque).getSelectedVar();
      }
      TrPostCourbeBuilder.chooseAndBuild(this, (ZCalqueAffichageDonnees) getCalqueActif(), idx, selectedVar);
    } else if (isSelectionOkForEvolutionSonde()) {
      TrPostCourbeBuilder.chooseAndBuild(this, (ZCalqueAffichageDonnees) getCalqueActif(), getInterpolePointForEvol(),
          getSelectedVarInCalqueActif());
    } else {

      TrPostCourbeBuilder.chooseAndBuild(this, (ZCalqueAffichageDonnees) getCalqueActif(), null, null,
          getSelectedVarInCalqueActif());
    }
  }

  protected void buildFrontiereLayer() {
    final MvFrontierLayerAbstract l = new TrFrontierLayer(source_, info_);
    l.setName("cqGridFr");
    l.setTitle(TrResource.getS("fronti�res"));
    l.setActions(new EbliActionInterface[]{l.create3DAction(getImpl(), getCmdMng())});
    getDonneesCalque().enPremier(l);
  }

  protected TrIsoLayer buildIsoLayer() {
    final TrIsoLayer isoLayer = new TrIsoLayer(source_, info_);
    isoLayer.setLegende(getCqLegend());
    isoLayer.setDestructible(false);
    isoLayer.setName("cqIso");
    return isoLayer;
  }

  private void createAnimAction() {
    if (anim_ == null) {
      anim_ = new ActionAnimationTreeSelection(new AnimAdapter(), getFrame());
      getArbreCalqueModel().addTreeSelectionListener(anim_);
      anim_.updateFromModel(getArbreCalqueModel().getTreeSelectionModel());
    }
  }

  @Override
  public void dataAdded(final boolean _isFleche) {
    final BCalque[] cq = getDonneesCalque().getTousCalques();
    if (cq != null) {
      for (int i = cq.length - 1; i >= 0; i--) {
        if (cq[i] instanceof TrPostDataListener) {
          ((TrPostDataListener) cq[i]).dataAdded(_isFleche);
        }
      }
    }
  }

  @Override
  public void dataChanged(final H2dVariableType _old, final H2dVariableType _new, final boolean _contentChanged,
                          final boolean _isFleche,
                          final Set _varDependingOnOld) {
    final BCalque[] cq = getDonneesCalque().getTousCalques();
    if (cq != null) {
      for (int i = cq.length - 1; i >= 0; i--) {
        if (cq[i] instanceof TrPostDataListener) {
          ((TrPostDataListener) cq[i]).dataChanged(_old, _new, _contentChanged, _isFleche, _varDependingOnOld);
        }
      }
    }
  }

  @Override
  public void dataRemoved(final H2dVariableType[] _vars, final boolean _isFleche) {
    final BCalque[] cq = getDonneesCalque().getTousCalques();
    if (cq != null) {
      for (int i = cq.length - 1; i >= 0; i--) {
        if (cq[i] instanceof TrPostDataListener) {
          ((TrPostDataListener) cq[i]).dataRemoved(_vars, _isFleche);
        }
      }
    }
  }

  /**
   * duplication du trPostVisuPanel. surcharge de la duplication du zeblicalquePanel.
   */
  @Override
  public TrPostVisuPanel duplicate(final Map _options) {

    final CalqueLegendeWidgetAdapter legendeCalque = new CalqueLegendeWidgetAdapter((EbliScene) _options.get("scene"));
    final TrPostVisuPanel duplic = new TrPostVisuPanel(this.getPostImpl(), this.getProjet(), legendeCalque, getSource());

    // -- sauvegarde de l etat --//
    final BCalqueSaverInterface savedData = getDonneesCalque().getPersistenceMng().save(getDonneesCalque(), null);

    // -- chargement de l etat dans le calque duplique --//
    duplic.getDonneesCalque().getPersistenceMng().restore(savedData, duplic, duplic.getDonneesCalque(), null);

    // -- creation des actions specifiques au calque --//
    duplic.getController().buildActions();

    // -- duplic des infos de construction --//
    duplic.getInfosCreation().putAll(getInfosCreation());

    return duplic;
  }

  @Override
  public String editBcPoint() {
    return null;
  }

  @Override
  public String editGridPoint() {
    return null;
  }

  @Override
  public String editGridPoly() {
    return null;
  }

  @Override
  public EbliAnimationSourceInterface getAnimationSrc() {
    createAnimAction();
    return anim_.getSrc();
  }

  public TrBcBoundaryLayer getBcLayer() {
    return null;
  }

  ClearSonde actionClearSonde;

  public ClearSonde getClearSondeAction() {
    if (actionClearSonde == null) {
      actionClearSonde = new ClearSonde();
    }
    return actionClearSonde;
  }

  public TrPostFlecheLayer getFlecheLayer() {
    return (TrPostFlecheLayer) getDonneesCalqueByName("cqFleche");
  }

  public MvFrontierLayerAbstract getFrontierLayer3D() {
    return (MvFrontierLayerAbstract) getDonneesCalque().getCalqueParNom("cqGridFr");
  }

  private class BuildingData {
    ZCalqueLigneBrisee[] buildings;
    boolean useBoundary = false;
  }

  private BuildingData getBuildingLayers3D() {
    List<ZCalqueEditionGroup> groups = new ArrayList<ZCalqueEditionGroup>();
    List<ZCalqueLigneBrisee> buildingLayers = new ArrayList<ZCalqueLigneBrisee>();
    BCalque[] layers = getDonneesCalque().getTousCalques();

    for (BCalque layer : layers) {
      if (layer instanceof ZCalqueEditionGroup) {
        if (hasBathy((ZCalqueEditionGroup) layer)) {
          groups.add((ZCalqueEditionGroup) layer);
        }
      }
    }

    for (ZCalqueEditionGroup group : groups) {
      layers = group.getCalques();

      for (BCalque layer : layers) {
        if (layer instanceof ZCalqueLigneBrisee) {
          buildingLayers.add((ZCalqueLigneBrisee) layer);
        }
      }
    }
    MvFrontierLayerAbstract boundaryLayer = getFrontierLayer3D();
    boolean useFrontier = boundaryLayer != null && boundaryLayer.getModeleCl().getNbFrontier() > 0;
    if (!useFrontier && buildingLayers.isEmpty()) {
      getImpl().warn(TrLib.getString("Affichage des batiments 3D"), TrLib.getString(
          "Il n'y a pas de donn�es permettant l'affichage de batiments"),
          true);
      return new BuildingData();
    }
    BuildingLayerSelector selector = new BuildingLayerSelector(buildingLayers, useFrontier);

    if (selector.afficheModaleOk(this)) {
      BuildingData data = new BuildingData();
      data.useBoundary = selector.useBoundaryLayer();
      data.buildings = selector.getSelectedCalque();
      return data;
    }

    return null;
    //    return buildingLayers.toArray(new ZCalqueLigneBrisee[0]);
  }

  private boolean hasBathy(ZCalqueEditionGroup layer) {
    final GISAttributeInterface[] attributes = layer.getAttributes();

    if (attributes != null) {
      for (GISAttributeInterface attribute : attributes) {
        if (attribute == GISAttributeConstants.BATHY) {
          return true;
        }
      }
    }

    return false;
  }

  /*
   * protected void anim(){ if (anim_ == null) anim_ = new EbliAnimation(this); anim_.go(); }
   */
  public TrPostInfoDelegate getInfo() {
    return info_;
  }

  public TrPostInterpolatePoint getInterpolePointForEvol() {
    if (isSelectionOkForEvolutionSonde()) {
      final ZCalqueSondeInterface s = (ZCalqueSondeInterface) getCalqueActif();
      return new TrPostInterpolatePoint(s.getElementSonde(), s.getSondeX(), s.getSondeY(), source_.getPrecisionModel());
    }
    return null;
  }

  @Override
  public TrIsoLayer getIsoLayer() {
    return isoLayer_;
  }

  @Override
  public ZEbliCalquesPanelPersistManager getPersistenceManager() {
    return new TrPostVisuPanelPersistManager();
  }

  public TrPostCommonImplementation getPostImpl() {
    return (TrPostCommonImplementation) getImpl();
  }

  @Override
  public String getProjectName() {
    return source_.getTitle();
  }

  /**
   * @return le controller
   */
  public TrPostProjet getProjet() {
    return projet_;
  }

  /**
   * Retourne le pas de temps du calque actif. Retourne -1 sinon.
   */
  public int getSelectedTimeInCalqueActif() {
    int time = -1;
    final BCalque actif = getCalqueActif();
    if (actif instanceof TrIsoLayer) {
      time = ((TrIsoLayer) actif).getIsoModel().getTimeStep();
    } else if (actif instanceof TrPostFlecheLayer) {
      time = ((TrPostFlecheLayer) actif).getTimeIdx();
    }
    return time;
  }

  public H2dVariableType getSelectedVarInCalqueActif() {
    H2dVariableType selectedVar = null;
    final BCalque actif = getCalqueActif();
    if (actif instanceof TrIsoLayer) {
      selectedVar = ((TrIsoLayer) actif).getIsoModel().getVariableSelected();
    } else if (actif instanceof TrPostFlecheLayer) {
      selectedVar = ((TrPostFlecheLayer) actif).getSelectedVar();
    }
    return selectedVar;
  }

  public int[] getSelectionForEvolution() {
    if (getCalqueActif() instanceof ZCalqueAffichageDonnees) {
      return ((ZCalqueAffichageDonnees) getCalqueActif()).getSelectedIndex();
    }
    return null;
  }

  /**
   * Action qui recupere la sonde comme action.
   */
  public CalqueActionSonde getSondeAction() {
    if (actionSonde == null) {
      final ZCalqueSondeInteraction sonde = getController().addCalqueSondeInteraction();
      actionSonde = new CalqueActionSonde(sonde, getArbreCalqueModel());
    }
    return actionSonde;
  }

  public TrPostSource getSource() {
    return source_;
  }

  @Override
  public JMenu[] getSpecificMenus(String _title) {
    return super.getSpecificMenus(_title);
  }

  @Override
  public String getTitle() {
    return TrResource.getS("Vue 2D");
  }

  @Override
  protected void initButtonGroupSpecific(final List _l, final ZEbliCalquePanelController _res) {
    EbliActionPaletteAbstract plTime;
    KeyStroke ks;
    InputMap thisMap = null;
    ActionMap m = null;
    plTime = new EbliCalqueActionTimeChooser(getArbreCalqueModel());
    _l.add(plTime);
    ks = KeyStroke.getKeyStroke('t');
    plTime.putValue(Action.ACCELERATOR_KEY, ks);
    thisMap = getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    m = getActionMap();
    thisMap.put(ks, "TIME_CHOOSE");
    m.put("TIME_CHOOSE", plTime);
    plTime = new EbliCalqueActionVariableChooser(getArbreCalqueModel());
    _l.add(plTime);
    ks = KeyStroke.getKeyStroke('v');
    plTime.putValue(Action.ACCELERATOR_KEY, ks);
    thisMap.put(ks, "VARIABLE_CHOOSE");
    m.put("VARIABLE_CHOOSE", plTime);
    _l.add(getSondeAction());
    _l.add(new TrPostProfileAction(this));
    _l.add(new EvolutionAction());
    createAnimAction();

    _l.add(anim_);
    _l.add(Tr3DInitialiser.create3dAction(this, getProjectName()));
    _l.add(null);
    _l.add(new TrPostActionBilan(this, projet_.getImpl()));
    _l.add(new TrPostActionCubature(this, projet_.getImpl()));
    _l.add(new TrPostActionLigneCourants(this, projet_.getImpl()));
  }

  @Override
  public boolean isBcPointEditable() {
    return false;
  }

  public boolean isRubar() {
    return getSource().containsElementVar();
  }

  public boolean isSelectionOkForEvolution() {
    ZCalqueAffichageDonnees d = null;
    final BCalque actif = getCalqueActif();
    if (actif instanceof TrPostFlecheLayer || actif instanceof TrIsoLayer) {
      d = (ZCalqueAffichageDonnees) actif;
    }
    return d != null && !d.isSelectionEmpty();
  }

  @Override
  public void startExport(final CtuluUI _impl) {
    TrPostVisuPanel.startExport(_impl, source_, super.getCurrentSelection(), this);
  }

  @Override
  public void view3D(final JFrame _f) {
    BuildingData buildingLayers3D = getBuildingLayers3D();
    if (buildingLayers3D == null) {
      return;
    }
    new Tr3DFactory().afficheFrame(_f, source_, source_.getInterpolator().getVect(), getImpl(), getGroupFond(),
        buildingLayers3D.useBoundary ? getFrontierLayer3D() : null, buildingLayers3D.buildings);
  }

  @Override
  public void timeStepFormatChanged() {
    final BCalque[] cq = getDonneesCalque().getTousCalques();
    if (cq != null) {
      for (int i = cq.length - 1; i >= 0; i--) {
        if (cq[i] instanceof TimeStepFormatAware) {
          ((TimeStepFormatAware) cq[i]).timeStepFormatChanged();
        }
      }
    }
  }

  /**
   * @param _g le maillage associee
   * @param _c le groupe de calque de dest
   */
  @Override
  public void addCqNumber(final EfGridInterface _g, final BGroupeCalque _c) {
    final MvNodeNumberLayer pt = new MvNodeNumberLayer(new MvNodeModelDefault(_g)) {
      @Override
      public String getValueForIdx(final int idX) {
        int time = getSelectedTimeInCalqueActif();
        H2dVariableType selectedVar = getSelectedVarInCalqueActif();

        try {
          double value = source_.getData(selectedVar, time, idX);
          DecimalFormat decFormat = new DecimalFormat("##.####");
          return decFormat.format(value);
        } catch (IOException ex) {
          FuLog.error(ex);
        }
        return "";
      }
    };
    pt.setName("cqPtNumber");
    _c.add(pt);
  }

  @Override
  public void contentsChanged(ListDataEvent e) {
    timeStepFormatChanged();
  }

  @Override
  public void intervalAdded(ListDataEvent e) {
  }

  @Override
  public void intervalRemoved(ListDataEvent e) {
  }
}
