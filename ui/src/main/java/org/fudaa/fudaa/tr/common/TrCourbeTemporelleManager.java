/**
 * @creation 23 juin 2004 @modification $Date: 2007-05-22 14:20:38 $ @license GNU General Public License 2 @copyright (c)1998-2001
 *     CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuResource;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.H2dEvolutionFrontiereLiquide;
import org.fudaa.dodico.h2d.H2dParameters;
import org.fudaa.dodico.h2d.telemac.H2dTelemacEvolutionFrontiere;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionListener;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.courbe.*;
import org.fudaa.ebli.palette.BPalettePlageDefault;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeCreateGroupPanel;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeModel;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * Il est suppose qu'il n'y a qu'un seul graphe dessine. It is supposed that there is only a graph drawn
 *
 * @author Fred Deniger
 * @version $Id: TrCourbeTemporelleManager.java,v 1.29 2007-05-22 14:20:38 deniger Exp $
 */
public class TrCourbeTemporelleManager extends EGGrapheTreeModel implements EvolutionListener, TrCourbeImporter.Target {
  public static final H2dVariableType getRealVariable(final H2dVariableType _t) {
    if (_t == null) {
      return null;
    }
    return _t.getParentVariable() == null ? _t : _t.getParentVariable();
  }

  public static final boolean isCurveNotEnoughLarge(final double _min, final double _max, final H2dTelemacEvolutionFrontiere[] _l) {
    if (_l != null) {
      for (int i = _l.length - 1; i >= 0; i--) {
        final EvolutionReguliereInterface li = _l[i].getTelemacEvolution().getEvolution();
        if ((li.getMinX() > _min) || (li.getMaxX() < _max)) {
          return true;
        }
      }
      return false;
    }
    return true;
  }

  public static final boolean isCurveNotEnoughLarge(final double _min, final double _max, final EvolutionReguliereInterface[] _l) {
    if (_l != null) {
      for (int i = _l.length - 1; i >= 0; i--) {
        final EvolutionReguliereInterface li = _l[i];
        if ((li.getMinX() > _min) || (li.getMaxX() < _max)) {
          return true;
        }
      }
      return false;
    }
    return true;
  }

  public static final boolean isCurveNotEnoughLarge(final double _min, final double _max, final H2dEvolutionFrontiereLiquide[] _l) {
    if (_l != null) {
      for (int i = _l.length - 1; i >= 0; i--) {
        final EvolutionReguliereInterface li = _l[i].getEvolution();
        if ((li.getMinX() > _min) || (li.getMaxX() < _max)) {
          return true;
        }
      }
      return false;
    }
    return true;
  }

  // private EGGraphe graphe_;
  Map varGrapheGroup_;
  protected transient EGGroup groupNull_;
  protected final transient H2dParameters params_;

  public TrCourbeTemporelleManager(final H2dParameters _p, boolean createDefaultCourbe) {
    setCreateDefaultCourbe(createDefaultCourbe);
    params_ = _p;
    initWith(_p);
    _p.getProjectDispatcher().addEvolutionListener(this);
  }

  public TrCourbeTemporelleManager(final H2dParameters _p) {
    this(_p, false);
  }

  private TrCourbeComboboxModel initCombobox(final TrCourbeComboboxModel _m, final JComboBox _cb) {
    _cb.setModel(_m);
    _cb.setEditable(false);
    _cb.setRenderer(new TrCourbeComboBoxRenderer());
    if (_m.getSize() > 0) {
      _m.setSelectedItem(_m.getElementAt(0));
    }
    return _m;
  }

  @Override
  protected List copyCourbe(final List _l, final EGGroup _dest) {
    int n = getNbEvol();
    final List r = new ArrayList(_l.size());
    for (final Iterator it = _l.iterator(); it.hasNext(); ) {
      final EGCourbe c = (EGCourbe) it.next();
      final FudaaCourbeModel m = (FudaaCourbeModel) c.getModel();
      final EvolutionReguliere ne = m.getEvolution().duplicate();

      ne.setNom(majNom(ne.getNom(), ++n));
      r.add(createEGCourbe(_dest, ne));
    }
    return r;
  }

  protected EGCourbeChild createEGCourbe(final EGGroup _g, final EvolutionReguliere _e) {
    return createEGCourbe(_g, _e, -1);
  }

  protected EGCourbeChild createEGCourbe(final EGGroup _g, final EvolutionReguliere _e, final int _idx) {
    if (_e == null) {
      return null;
    }

    if (!varGrapheGroup_.values().contains(_g) && (_g != groupNull_)) {
      if (FuLog.isWarning()) {
        FuLog.warning(getClass().getName() + " groupe non defini");
      }
      return null;
    }
    if (params_ != null && _e.getListener() != params_.getProjectDispatcher()) {
      _e.setListener(params_.getProjectDispatcher());
    }
    final EGCourbeChild courbe = new EGCourbeChild(_g, new FudaaCourbeModel(_e));
    courbe.setAspectContour(BPalettePlageDefault.getColor(_idx));

    // on met a jour si la courbe est un nuage de points ou non --//
    //We update if the curve is a cloud of points or not

    courbe.setNuagePoints(_e.isNuagePoints());

    if (_g == null) {
      if (groupNull_ != null) {
        groupNull_.addEGComponent(courbe);
      }
    } else {
      _g.addEGComponent(courbe);
    }
    return courbe;
  }

  @Override
  protected EGCourbeChild duplicateCourbe(final EGCourbeChild _c) {
    if (_c == null) {
      return null;
    }
    final EvolutionReguliere newE = ((FudaaCourbeModel) _c.getModel()).getEvolution().duplicate();
    newE.setNom(_c.getTitle() + CtuluLibString.ESPACE + BuResource.BU.getString("Copie"));
    return createEGCourbe(_c.getParentGroup(), newE);
  }

  protected EGObject getObjectFor(final EvolutionReguliereInterface _e) {
    for (int i = getNbEGObject() - 1; i >= 0; i--) {
      final EGGroup g = getGroup(i);
      for (int j = g.getChildCount() - 1; j >= 0; j--) {
        final EGCourbe c = (EGCourbe) g.getChildAt(j);
        if (((FudaaCourbeModel) c.getModel()).getEvolution().equals(_e)) {
          return c;
        }
      }
    }
    return null;
  }

  protected String majNom(final String _nom, final int _idx) {
    final int idxUnder = _nom.lastIndexOf('_');
    if (idxUnder < 0) {
      return _nom + '_' + _idx;
    }
    try {
      Integer.parseInt(_nom.substring(idxUnder + 1));
      return _nom.substring(0, idxUnder) + "_" + _idx;
    } catch (final NumberFormatException e) {
      return _nom + '_' + _idx;
    }
  }

  /**
   *
   */
  @Override
  protected Map moveCourbe(final List _l, final EGGroup _p) {
    final Map r = new HashMap(_l.size());
    for (final Iterator it = _l.iterator(); it.hasNext(); ) {
      final EGCourbeChild c = (EGCourbeChild) it.next();
      final FudaaCourbeModel m = (FudaaCourbeModel) c.getModel();
      if (!m.getEvolution().isUsed()) {

        final EGGroup oldParent = c.getParentGroup();
        r.put(c, oldParent);
        _p.addEGComponent(c);
      }
    }
    return r;
  }

  protected void setValues(final Map _courbeVar) {
    int idx = 0;
    if (_courbeVar != null) {
      for (final Iterator it = _courbeVar.entrySet().iterator(); it.hasNext(); ) {
        final Map.Entry e = (Map.Entry) it.next();
        final H2dVariableType t = getRealVariable((H2dVariableType) e.getValue());
        EGGroup g = groupNull_;
        if (t != null) {
          g = (EGGroup) varGrapheGroup_.get(t);
        }
        if (g == null) {
          g = groupNull_;
        }
        createEGCourbe(g, ((EvolutionReguliere) e.getKey()), idx);
        idx++;
      }
    }
    EGGroup g;
    for (final Iterator it = varGrapheGroup_.values().iterator(); it.hasNext(); ) {
      g = (EGGroup) it.next();
      g.setVisible(g.getChildCount() < +5);
      if (!contains(g)) {
        add(g);
      }
      g.sortMembers();
    }
    groupNull_.sortMembers();
    fireGlobalChanged();
  }

  public void add(final EvolutionReguliereInterface[] _evol, final CtuluCommandManager _mng) {
    if (_evol != null) {
      final EGCourbeChild[] cs = new EGCourbeChild[_evol.length];
      for (int i = _evol.length - 1; i >= 0; i--) {
        cs[i] = createEGCourbe(groupNull_, EvolutionReguliere.createTimeEvolution(_evol[i], this));
      }
      curvesAdded(cs, groupNull_, _mng);
    }
  }

  @Override
  public void addNewCourbe(final CtuluCommandManager _cmd, final Component _parent, final EGGraphe _graphe) {
    final String title = TrResource.getS("courbe") + CtuluLibString.ESPACE + (getNbCourbes() + 1);
    final ComboBoxModel model = getGroupComboboxModel();
    if (getSelectedComponent() == null) {
      final TreePath p = getSelectionModel().getSelectionPath();
      if (p != null && (p.getLastPathComponent() instanceof EGGroup)) {
        model.setSelectedItem(p.getLastPathComponent());
      }
    } else {
      model.setSelectedItem(((EGCourbeChild) getSelectedComponent()).getParentGroup());
    }
    final EGGroup g = (EGGroup) model.getSelectedItem();
    double xminVal = 0;
    double xmaxVal = 1;
    if (g != null) {
      final CtuluRange r = g.getXRange();
      xmaxVal = r.max_;
      xminVal = r.min_;
      if (xmaxVal == xminVal) {
        xminVal = 0;
        xmaxVal = 1;
      }
    }
    final FudaaCourbeCreateGroupPanel pn = new FudaaCourbeCreateGroupPanel(title, xminVal, xmaxVal, model, _graphe, null);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_parent))) {
      final EGGroup gr = pn.getSelectedGroup();
      final EvolutionReguliere evol = pn.getEvol();
      final EGCourbeChild c = createEGCourbe(gr, evol);
      curvesAdded(c, _cmd);
    }
  }

  @Override
  public boolean canAddCourbe() {
    return true;
  }

  /**
   * @param _t la variable a considere / the variable to be considered
   * @return un model pour les courbes de la variable _t / a model for the curves of the variables_f
   */
  public ComboBoxModel createComboBoxModel(final H2dVariableType _t) {
    return new TrCourbeComboboxModel(getGroupeFor(_t));
  }

  public ComboBoxModel createComboBoxModel(final H2dVariableType _t, boolean includeCommon) {
    if (!includeCommon) {
      return createComboBoxModel(_t);
    }
    return new TrCourbeComboboxModel(getGroupeFor(_t), groupNull_);
  }

  /**
   * @param _t la variable a considerer / the variable to be considered
   * @return un model pour les courbes de la variable _t / a model for the curves of the variables_f
   */
  public ComboBoxModel createComboBoxModel(final H2dVariableType _t, final EvolutionReguliereInterface _evolSup) {
    return new TrCourbeComboboxModel(_evolSup, null, getGroupeFor(_t), null);
  }

  protected EGGroup getGroupeFor(final H2dVariableType _t) {
    return (EGGroup) varGrapheGroup_.get(getRealVariable(_t));
  }

  protected void removeGroup(final H2dVariableType _var, final EGGroup _g) {
    if (_g != null) {
      varGrapheGroup_.remove(getRealVariable(_var));
      _g.removeAll();
      super.remove(_g);
    }
  }

  /**
   * @param _t la variable a considere
   * @return un model pour les courbes de la variable _t
   */
  public ComboBoxModel createComboBoxModel(final H2dVariableType _t, final EvolutionReguliereInterface _evol1,
                                           final EvolutionReguliereAbstract _evol2) {
    return new TrCourbeComboboxModel(_evol1, _evol2, getGroupeFor(_t), null);
  }

  public ComboBoxModel createComboBoxModel(final H2dVariableType _t, final JComboBox _cb) {
    return initCombobox(createCombBoxModel(_t), _cb);
  }

  private TrCourbeComboboxModel createCombBoxModel(H2dVariableType _t) {
    return new TrCourbeComboboxModel(getGroupeFor(_t), groupNull_);
  }

  public ComboBoxModel createComboBoxModelWithEmpty(final EvolutionReguliereInterface _e0, final EvolutionReguliereInterface _e,
                                                    final H2dVariableType _t, final JComboBox _cb) {
    return initCombobox(new TrCourbeComboboxModel(_e0, _e, getGroupeFor(_t), groupNull_), _cb);
  }

  public ComboBoxModel createComboBoxModelWithEmpty(final EvolutionReguliereInterface _e, final H2dVariableType _t,
                                                    final JComboBox _cb) {
    return initCombobox(new TrCourbeComboboxModel(_e, null, getGroupeFor(_t), groupNull_), _cb);
  }

  /**
   * @return l'internal frame pour les courbes / the internal frame for the curves
   */
  public EGGraphe createGraphe() {
    final EGGraphe graphe = new EGGraphe(this);
    graphe.setXAxe(EGAxeHorizontal.buildDefautTimeAxe(null));
    return graphe;
  }

  @Override
  public void evolutionChanged(final EvolutionReguliereInterface _e) {
    final EGObject o = getObjectFor(_e);
    if (o != null) {
      fireCourbeContentChanged(o);
    }
  }

  @Override
  public void evolutionUsedChanged(final EvolutionReguliereInterface _e, final int _old, final int _new,
                                   final boolean _isAdjusting) {
  }

  public int getNbEvol() {
    int r = 0;
    for (final Iterator it = varGrapheGroup_.values().iterator(); it.hasNext(); ) {
      r += ((EGGroup) it.next()).getChildCount();
    }
    r += groupNull_.getChildCount();
    return r;
  }

  public int getNbEvolCommonAndFor(final H2dVariableType _t) {
    int r = getNbEvolFor(getRealVariable(_t));
    if (groupNull_ != null) {
      r += groupNull_.getChildCount();
    }
    return r;
  }

  public int getNbEvolFor(final H2dVariableType _t) {
    final EGGroup g = getGroupeFor(_t);
    return g == null ? 0 : g.getChildCount();
  }

  public H2dVariableType getVar(final EGGroup _g) {
    for (final Iterator it = varGrapheGroup_.entrySet().iterator(); it.hasNext(); ) {
      final Map.Entry e = (Map.Entry) it.next();
      if (e.getValue() == _g) {
        return (H2dVariableType) e.getKey();
      }
    }
    return null;
  }

  @Override
  public void importCourbes(final EvolutionReguliereInterface[] _crb, final CtuluCommandManager _mng,
                            final ProgressionInterface _prog) {
    add(_crb, _mng);
  }

  public final void initWith(final H2dParameters _p) {
    final Map m = _p.getUsedEvolutionVariables();
    final List r = _p.getTransientVar();
    if (varGrapheGroup_ == null) {
      varGrapheGroup_ = new TreeMap();
    }
    for (final Iterator it = r.iterator(); it.hasNext(); ) {
      getOrCreateGroupFor((H2dVariableType) it.next());
    }
    if (groupNull_ == null) {
      groupNull_ = new EGGroup();
      final EGAxeVertical v = new EGAxeVertical();
      v.setTitre("Import");
      groupNull_.setTitle(v.getTitre());
      groupNull_.setAxeY(v);
      if (createDefaultCourbe) {
        createDefaultCourbe(groupNull_);
      }
    }
    setValues(m);
    if (!contains(groupNull_)) {
      add(groupNull_);
    }
    fireGlobalChanged();
  }

  protected void fireGlobalChanged() {
    fireTreeStructureChanged(this, getPathToRoot(getGrapheTreeNode()), null, null);
  }

  private boolean createDefaultCourbe = false;

  private final void setCreateDefaultCourbe(boolean createDefaultCourbe) {
    this.createDefaultCourbe = createDefaultCourbe;
  }

  protected final EGGroup getOrCreateGroupFor(final H2dVariableType _tInit) {
    final H2dVariableType t = getRealVariable(_tInit);
    if (!varGrapheGroup_.containsKey(t) && t != null) {
      final EGGroup g = new EGGroup();
      final EGAxeVertical v = new EGAxeVertical();
      v.setTitre(t.getName());
      g.setTitle(t.getName());
      v.setUnite(_tInit.getCommonUnit() == null ? null : _tInit.getCommonUnit().toString());
      g.setAxeY(v);
      varGrapheGroup_.put(t, g);

      return g;
    }
    return (EGGroup) varGrapheGroup_.get(t);
  }

  EvolutionReguliere emptyCurve;

  public EvolutionReguliere getEmptyCurve() {
    return emptyCurve;
  }

  private void createDefaultCourbe(final EGGroup g) {
    EvolutionReguliere emptyCurve = new EvolutionReguliere(1);
    emptyCurve.add(0, 0);
    emptyCurve.setNom(TrLib.getString("vide"));
    EGCourbeChild createEGCourbe = createEGCourbe(g, emptyCurve);
    this.emptyCurve = ((FudaaCourbeModel) createEGCourbe.getModel()).getEvolution();
  }

  public boolean isOneCurveUsed() {
    FuLog.error("DO NOT USE");
    return false;
  }

  public void removeUsedCourbe() {
    for (final Iterator it = varGrapheGroup_.values().iterator(); it.hasNext(); ) {
      final EGGroup gr = (EGGroup) it.next();
      final List toRemove = new ArrayList();
      for (int i = gr.getChildCount() - 1; i >= 0; i--) {
        toRemove.clear();
        final EGCourbeChild c = (EGCourbeChild) gr.getChildAt(i);
        if (((FudaaCourbeModel) c.getModel()).getEvolution().isUsed()) {
          toRemove.add(c);
        }
      }
      gr.removeEGComponent(toRemove);
    }
    fireGlobalChanged();
  }

  @Override
  public boolean isSpatial() {
    return false;
  }
}
