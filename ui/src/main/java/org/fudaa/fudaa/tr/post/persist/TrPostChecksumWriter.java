package org.fudaa.fudaa.tr.post.persist;

import com.memoire.fu.FuLog;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourcesManager;

/**
 * Write several files in the checksum dir:
 * <ul>
 * <li>variables: contains the md5 checksum for each variables files</li>
 * <li>setup: contains the md5 checksum for the setup file</li>
 * <li>one file by sources: contains the md5 checksum for each variables files</li>
 * 
 * @author deniger
 */
public class TrPostChecksumWriter {

  private final TrPostPersistenceFileFinder fileFinder;

  private static void write(List<String> lines, File dest) {
    FortranWriter writer = null;

    try {
      writer = new FortranWriter(new FileWriter(dest));
      writer.writeln("#Ne pas �diter");
      for (String string : lines) {
        writer.writeln(string);
      }
    } catch (IOException e) {
      FuLog.error(e);
    } finally {
      FortranLib.close(writer);
    }

  }

  private final TrPostProjet projet;

  /**
   * @param fileFinder
   * @param projet
   */
  public TrPostChecksumWriter(TrPostPersistenceFileFinder fileFinder, TrPostProjet projet) {
    super();
    this.fileFinder = fileFinder;
    this.projet = projet;
  }

  public void write() {
    File dir = fileFinder.getMainChecksumDir();
    dir.mkdirs();
    writeSetupChecksum();
    writeVariableChecksums();
    writeSourcesChecksums();
  }

  private void writeVariableChecksums() {
    List<String> lines = getVariablesChecksums();
    write(lines, fileFinder.getChecksumFileForVariables());
  }

  protected List<String> getVariablesChecksums() {
    List<String> lines = new ArrayList<String>();
    File variablesDir = fileFinder.getVariablesDir();
    if (variablesDir.exists()) {
      File[] listFiles = variablesDir.listFiles();
      for (File file : listFiles) {
        lines.add(file.getName());
        lines.add(TrPostCheckSumGenerator.getMD5Checksum(file));

      }
    }
    return lines;
  }

  /**
   * Write 3 lines by files
   */
  private void writeSourcesChecksums() {

    TrPostSourcesManager sources = projet.getSources();
    List<TrPostSource> lstSources = sources.getLstSources();
    for (TrPostSource trPostSource : lstSources) {
      write(getChecksumForSources(trPostSource), fileFinder.getChecksumFileForSources(trPostSource));
    }
  }

  protected List<String> getChecksumForSources(TrPostSource trPostSource) {
    List<String> lines = new ArrayList<String>();
    Collection<File> files = trPostSource.getFiles();
    for (File file : files) {
      lines.add(file.getName());
      lines.add(TrPostCheckSumGenerator.getMD5Checksum(file));
    }
    return lines;
  }

  private void writeSetupChecksum() {
    write(Arrays.asList(TrPostCheckSumGenerator.getMD5Checksum(fileFinder.getMainSetupFile())), fileFinder.getChecksumFileForSetup());
  }

}
