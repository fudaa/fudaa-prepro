/*
 *  @creation     11 mai 2005
 *  @modification $Date: 2007-04-26 14:40:34 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: TrPostFilterHauteurEnvRubar.java,v 1.7 2007-04-26 14:40:34 deniger Exp $
 */
public class TrPostFilterHauteurEnvRubar extends TrPostFilterHauteurElement {
  /**
   *
   */
  public TrPostFilterHauteurEnvRubar() {
    super();
  }

  /**
   * @param _eps
   */
  public TrPostFilterHauteurEnvRubar(final double _eps) {
    super(_eps);
  }

  /**
   * @param _data
   */
  public TrPostFilterHauteurEnvRubar(final TrPostFilterHauteur _data) {
    super(_data);
  }

  @Override
  public void updateTimeStep(final int _t, final EfGridData _src) {
    grid_ = _src.getGrid();
    if (hauteur_ == null) {
      hauteur_ = ((TrPostSourceRubar) _src).getEnveloppData(H2dVariableType.HAUTEUR_EAU);
      if (hauteur_ == null) {
        hauteur_ = ((TrPostSourceRubar) _src).getEnveloppData(DELTA_H);
        if (hauteur_ != null) {
          isDelta = true;
        }
      }
    }
  }

  @Override
  public boolean valueChanged(final H2dVariableType _t, final TrPostSource _src) {
    if (_t != null && (_t == H2dVariableType.HAUTEUR_EAU || DELTA_H.getName().equals(_t.getName())) && hauteur_ != null) {
      hauteur_ = ((TrPostSourceRubar) _src).getEnveloppData(H2dVariableType.HAUTEUR_EAU);
      if (hauteur_ == null) {
        hauteur_ = ((TrPostSourceRubar) _src).getEnveloppData(DELTA_H);
        if (hauteur_ != null) {
          isDelta = true;
        }
      }
      return true;
    }
    return false;
  }
}
