/*
 * @creation 29 ao�t 2005
 *
 * @modification $Date: 2007-04-30 14:22:38 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.db4o.ObjectContainer;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.fudaa.commun.save.FudaaSaveLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.data.*;
import org.nfunk.jep.Variable;

import java.io.Serializable;
import java.util.*;
import java.util.Map.Entry;

/**
 * @author Fred Deniger
 * @version $Id: TrPostUserVariableSaver.java,v 1.12 2007-04-30 14:22:38 deniger Exp $
 */
public class TrPostUserVariableSaver implements Serializable {
  //variables should be serializable !
  private H2dVariableTypeCreated[] varCreated_;
  String[] expr_;
  private H2dVariableTypeCreated[] varCreatedSimple_;
  private TrPostDataCreatedSaver[] saver_;
  private Object g_;
  // pour compatibilit� ascendante: certaine version dispose
  Object g__;

  public TrPostUserVariableSaver() {

  }

  public void restoreG() {
    if (g_ == null && g__ != null) {
      g_ = g__;
    }
  }

  public boolean isExprEmpty() {
    return CtuluLibArray.isEmpty(varCreated_) || CtuluLibArray.isEmpty(expr_) || expr_.length != varCreated_.length;
  }

  public boolean isSimpleEmpty() {
    return CtuluLibArray.isEmpty(varCreatedSimple_) || CtuluLibArray.isEmpty(saver_) || saver_.length != varCreatedSimple_.length;
  }

  /**
   * Methode qui charge les donnes variables dans le source.
   *
   * @param savedValues savedValues in the file.
   */
  public CtuluAnalyze restore(final TrPostSource _src, final CtuluAnalyze _analyze, final ProgressionInterface _prog, final CtuluUI _ui,
                              TrPostSourcesManager ctx, Map<String, double[]> savedValues) {
    if (saver_ == null) {
      saver_ = new TrPostDataCreatedSaver[0];
    }
    Map<String, double[]> savedValuesInFile = savedValues;
    if (savedValuesInFile == null) {
      savedValuesInFile = Collections.emptyMap();
    }
    CtuluAnalyze log = new CtuluAnalyze();
    if (g_ != null) {
      _src.getGravity().setValue(g_);
    }
    Map<String, H2dVariableType> shortName = getShortNameVariable(_src);

    //we load all imported variable first:
    List<H2dVariableTypeCreated> alreadyLoaded = new ArrayList<>();
    if (!isSimpleEmpty()) {
      List<TrPostDataCreated> trPostDataCreated = new ArrayList<TrPostDataCreated>(saver_.length);
      for (int i = 0; i < saver_.length; i++) {
        if (saver_[i] instanceof TrPostDataCreatedImportSaver) {
          TrPostDataCreated dataCreated = saver_[i].restore(varCreatedSimple_[i], _src, _ui, shortName, ctx, log,
            savedValuesInFile.get(varCreatedSimple_[i].getShortName()));
          trPostDataCreated.add(dataCreated);
          alreadyLoaded.add(varCreatedSimple_[i]);
        }
      }
      if (!trPostDataCreated.isEmpty()) {
        _src.addUserVar(alreadyLoaded.toArray(new H2dVariableTypeCreated[0]),
          trPostDataCreated.toArray(new TrPostDataCreated[0]), null);
      }
    }

    //toutes les variables disponibles:
    shortName = getShortNameVariable(_src);

    Set<String> existantVar = new HashSet<>();
    Variable[] constantVar = _src.getConstantVar();
    for (Variable variable : constantVar) {
      existantVar.add(variable.getName());
    }
    existantVar.addAll(shortName.keySet());

    final Map<H2dVariableTypeCreated, String> validFormulaExpression = new HashMap<>();
    if (!isExprEmpty()) {
      final int nb = expr_.length;
      for (int i = nb - 1; i >= 0; i--) {
        H2dVariableTypeCreated created = varCreated_[i];
        if (created != null && expr_[i] != null && !existantVar.contains(created.getShortName())) {
          validFormulaExpression.put(created, expr_[i]);
          shortName.put(created.getShortName(), created);
        }
      }
    }
    //contient les variables utilis�es par variables
    Map<H2dVariableTypeCreated, Collection<String>> validSimpleDataVariables = new HashMap<>();
    if (!isSimpleEmpty()) {
      for (int i = 0; i < saver_.length; i++) {
        H2dVariableTypeCreated variableTypeCreated = varCreatedSimple_[i];
        if (!alreadyLoaded.contains(variableTypeCreated) && !existantVar.contains(variableTypeCreated.getShortName())) {
          validSimpleDataVariables.put(variableTypeCreated, saver_[i].getVarNameUsed());
          shortName.put(variableTypeCreated.getShortName(), variableTypeCreated);
        }
      }
    }

    Set<String> allValidVariables = new HashSet<>();
    allValidVariables.addAll(shortName.keySet());

    for (Variable variable : constantVar) {
      allValidVariables.add(variable.getName());
    }

    //this method will remove from varExpr, usedVarByVar the wrong data
    final List<String> notValidVariables = new ArrayList<>();
    validVariables(validFormulaExpression, validSimpleDataVariables, allValidVariables, notValidVariables);
    Collections.sort(notValidVariables);
    for (String invalidName : notValidVariables) {
      log.addWarn(TrResource.TR.getString("La variable {0} n'a pas �t� recharg�e car non valide", invalidName));
    }

    Map<String, H2dVariableType> validshortNames = new HashMap<String, H2dVariableType>();
    for (Entry<String, H2dVariableType> entry : shortName.entrySet()) {
      if (allValidVariables.contains(entry.getKey())) {
        validshortNames.put(entry.getKey(), entry.getValue());
      }
    }
    List<H2dVariableTypeCreated> vars = new ArrayList<H2dVariableTypeCreated>();
    List<TrPostDataCreated> createdPostDatas = new ArrayList<TrPostDataCreated>(saver_.length);
    if (!validFormulaExpression.isEmpty()) {
      for (Entry<H2dVariableTypeCreated, String> entry : validFormulaExpression.entrySet()) {
        TrPostDataCreatedExpr createData = TrPostDataHelper.createData(_src, entry.getValue(), constantVar, validshortNames);
        if (createData != null) {
          vars.add(entry.getKey());
          createdPostDatas.add(createData);
        }
      }
    }
    if (!validSimpleDataVariables.isEmpty()) {
      for (int i = 0; i < saver_.length; i++) {
        H2dVariableTypeCreated variable = varCreatedSimple_[i];
        if (!alreadyLoaded.contains(variable) && validSimpleDataVariables.containsKey(variable)) {
          TrPostDataCreated dataCreated = saver_[i].restore(variable, _src, _ui, validshortNames, ctx, log,
            savedValuesInFile.get(variable.getShortName()));
          createdPostDatas.add(dataCreated);
          vars.add(variable);
        }
      }
    }
    if (!createdPostDatas.isEmpty()) {
      _src.addUserVar((H2dVariableTypeCreated[]) vars.toArray(new H2dVariableTypeCreated[vars.size()]),
        (TrPostDataCreated[]) createdPostDatas.toArray(new TrPostDataCreated[createdPostDatas.size()]), null);
      for (TrPostDataCreated createdPostData : createdPostDatas) {
        createdPostData.restore();
      }
    }
    return log;
  }

  /**
   * @param varExpr will be modified is wrong formula found
   * @param usedVarByVar will be modified
   * @param allVariables will be modified
   * @return true is valid
   */
  private boolean validVariables(Map<H2dVariableTypeCreated, String> varExpr, Map<H2dVariableTypeCreated, Collection<String>> usedVarByVar,
                                 Set<String> allVariables, List<String> notValidVariables) {
    if (varExpr.isEmpty() && usedVarByVar.isEmpty()) {
      return true;
    }
    boolean res = true;
    //we valid expressions:
    ;
    for (Iterator<Entry<H2dVariableTypeCreated, String>> iterator = varExpr.entrySet().iterator(); iterator.hasNext(); ) {
      Entry<H2dVariableTypeCreated, String> entry = iterator.next();
      boolean valid = isFormulaValid(entry.getValue(), allVariables);
      if (!valid) {
        String shortName = entry.getKey().getShortName();
        notValidVariables.add(shortName);
        iterator.remove();
        allVariables.remove(shortName);
        res = false;
      }
    }
    for (Iterator<Entry<H2dVariableTypeCreated, Collection<String>>> iterator = usedVarByVar.entrySet().iterator(); iterator.hasNext(); ) {
      Entry<H2dVariableTypeCreated, Collection<String>> entry = iterator.next();
      boolean valid = isContentVariablesValid(entry.getValue(), allVariables);
      if (!valid) {
        iterator.remove();
        String shortName = entry.getKey().getShortName();
        notValidVariables.add(shortName);
        allVariables.remove(shortName);
        res = false;
      }
    }
    if (!res) {
      return validVariables(varExpr, usedVarByVar, allVariables, notValidVariables);
    }
    return true;
  }

  private static boolean isFormulaValid(String formula, Set<String> allVariables) {
    final CtuluExpr expr = new CtuluExpr();
    for (String variable : allVariables) {
      expr.addVar(variable, variable);
    }
    expr.getParser().parseExpression(formula);
    return !expr.getParser().hasError();
  }

  private static boolean isContentVariablesValid(Collection<String> usedVars, Set<String> allVariables) {
    for (String usedVar : usedVars) {
      if (!allVariables.contains(usedVar)) {
        return false;
      }
    }
    return true;
  }

  public Map<String, H2dVariableType> getShortNameVariable(final TrPostSource _src) {
    final H2dVariableType[] var = _src.getAllVariablesNonVec();
    if (var == null) {
      return Collections.emptyMap();
    }
    final Map<String, H2dVariableType> res = new HashMap<String, H2dVariableType>(var.length);
    for (int i = var.length - 1; i >= 0; i--) {
      res.put(var[i].getShortName(), var[i]);
    }
    return res;
  }

  public static void saveIn(final TrPostSourceAbstract _src, final ObjectContainer _db, final ProgressionInterface _prog) {
    if (_db == null || _src == null) {
      return;
    }
    FudaaSaveLib.deleteAll(_db, TrPostUserVariableSaver.class);
    final TrPostUserSaverContent save = createSaver(_src, _prog);
    _db.set(save.getSaver());
    _db.commit();
  }

  /**
   * Sauvegarde toutes les variabels du projet dans un objet TrPostUserVariableSaver
   *
   * @param _src
   * @param _prog
   */
  public static TrPostUserSaverContent createSaver(final TrPostSourceAbstract _src, final ProgressionInterface _prog) {
    if (_src == null) {
      return null;
    }

    final TrPostUserVariableSaver save = new TrPostUserVariableSaver();
    Map<String, double[]> valuesToPersist = new HashMap<String, double[]>();
    save.g_ = _src.getGravity().getValue();
    if (_src.varUserCreateData_ != null) {
      final Map map = new HashMap(_src.varUserCreateData_);
      // on enleve les variables import�es
      final Set varToRemove = new HashSet();
      for (Object o : map.entrySet()) {
        final Entry e = (Entry) o;
        final TrPostDataCreated created = (TrPostDataCreated) e.getValue();
        final H2dVariableType var = (H2dVariableTypeCreated) e.getKey();
        // pas de sauvegarde possible ou non basee sur une expression
        if (created != null && !created.isExpr() && created.createSaver() == null) {
          varToRemove.add(var);
          varToRemove.addAll(TrPostDataHelper.getAllVarDependingOn(var, _src));
        }
      }
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("vars enlevees\n" + CtuluLibString.arrayToString(varToRemove.toArray()));
      }
      map.keySet().removeAll(varToRemove);
      final Map exprVar = new HashMap(map.size());
      final Map otherVar = new HashMap(map.size());
      for (final Iterator it = map.entrySet().iterator(); it.hasNext(); ) {
        final Map.Entry e = (Map.Entry) it.next();
        final TrPostDataCreated data = ((TrPostDataCreated) e.getValue());
        if (data != null && data.isExpr()) {
          if (data != null) {
            exprVar.put(e.getKey(), data);
          }
        } else {
          if (data != null) {
            otherVar.put(e.getKey(), data);
          }
        }
      }
      save.varCreated_ = new H2dVariableTypeCreated[exprVar.size()];
      save.expr_ = new String[exprVar.size()];
      int idx = 0;
      for (final Iterator it = exprVar.entrySet().iterator(); it.hasNext(); ) {
        final Map.Entry e = (Map.Entry) it.next();
        save.varCreated_[idx] = (H2dVariableTypeCreated) e.getKey();
        save.expr_[idx++] = ((TrPostDataCreatedExpr) e.getValue()).getFormule();
      }
      save.varCreatedSimple_ = new H2dVariableTypeCreated[otherVar.size()];
      save.saver_ = new TrPostDataCreatedSaver[otherVar.size()];
      idx = 0;

      for (final Iterator it = otherVar.entrySet().iterator(); it.hasNext(); ) {
        final Map.Entry e = (Map.Entry) it.next();
        save.varCreatedSimple_[idx] = (H2dVariableTypeCreated) e.getKey();
        TrPostDataCreated dataCreated = (TrPostDataCreated) e.getValue();
        TrPostDataCreatedSaver saver = dataCreated.createSaver();
        save.saver_[idx++] = saver;
        double[] values = saver.getValuesToPersist(dataCreated);
        if (values != null) {
          valuesToPersist.put(((H2dVariableTypeCreated) e.getKey()).getShortName(), values);
        }
      }
    }
    return new TrPostUserSaverContent(valuesToPersist, save);
  }

  public static void fillWithForbidden(final Collection _var, final Set _forbiddenName, final Set _forbiddenShort) {
    for (final Iterator iter = _var.iterator(); iter.hasNext(); ) {
      final H2dVariableType element = (H2dVariableType) iter.next();
      _forbiddenName.add(element.getName());
      _forbiddenShort.add(element.getShortName());
    }
  }

  public static void addNormalUserVar(final Map _varTypeExpr, final TrPostSource _src, final CtuluAnalyze _analyze, final CtuluCommandContainer _cmd) {
    addNormalUserVar(_varTypeExpr, _src, _analyze, _cmd, null);
  }

  public static void addNormalUserVar(final Map _varTypeExpr, final TrPostSource _src, final CtuluAnalyze _analyze, final CtuluCommandContainer _cmd,
                                      Map shortName) {
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FTR: adduser var");
    }
    final Map toAdd = addUsersVar(_src, _varTypeExpr, _src.getAllVariablesNonVec(), _src.getConstantVar(), _analyze);
    if (toAdd.size() > 0) {
      final H2dVariableTypeCreated[] vars = new H2dVariableTypeCreated[toAdd.size()];
      final TrPostDataCreatedExpr[] expr = new TrPostDataCreatedExpr[vars.length];
      int idx = 0;
      for (final Iterator iter = toAdd.entrySet().iterator(); iter.hasNext(); ) {
        final Entry element = (Entry) iter.next();
        H2dVariableTypeCreated variableTypeCreated = (H2dVariableTypeCreated) element.getKey();
        vars[idx] = variableTypeCreated;
        TrPostDataCreatedExpr value = (TrPostDataCreatedExpr) element.getValue();
        expr[idx++] = value;
        if (shortName != null) {
          shortName.put(variableTypeCreated.getShortName(), variableTypeCreated);
        }
      }
      _src.addUserVar(vars, expr, _cmd);
    }
  }

  /**
   * @param _varTypeExpr la table H2DVariableTypeCreated -> String (expression). Cette table sera modifiee
   * @param _analyze le receveur d'infos
   */
  public static Map addUsersVar(final TrPostSource _src, final Map _varTypeExpr, final H2dVariableType[] _existantVars, final Variable[] _constant,
                                final CtuluAnalyze _analyze) {
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FTR: adduser var static access");
    }
    // 1er temps: on supprime de la table les variables dont le nom est deja utilisee
    final int nb = _varTypeExpr.size() + (_existantVars == null ? 0 : _existantVars.length);
    final HashSet forbiddenNames = new HashSet(nb);
    final HashSet forbiddenShorts = new HashSet(nb);
    if (_constant != null) {
      for (int i = _constant.length - 1; i >= 0; i--) {
        forbiddenShorts.add(_constant[i].getName());
      }
    }
    fillWithForbidden(Arrays.asList(_existantVars), forbiddenNames, forbiddenShorts);
    final List toRemove = getAlreadyUsedVar(_varTypeExpr.keySet(), forbiddenNames, forbiddenShorts);
    if (toRemove.size() > 0) {
      _analyze.addError(
        TrResource.getS("Des variables ne seront pas ajout�es car elles utilisent des noms d�j� affect�s:") + getListString(toRemove), -1);
      _varTypeExpr.keySet().removeAll(toRemove);
    }
    // 2eme temps: on teste la validit� des expressions
    final int nbVarTested = _varTypeExpr.size();
    // la tables des r�sultats qui aura au plus autant de valeurs que la table d'origine
    final Map varPostData = new HashMap(nbVarTested);
    // la liste des variables erronn�es : au pire autant de valeurs que la table d'origine
    final Map errorVar = new HashMap(nbVarTested);
    // la liste de toutes les variables: sera mise a jour en enlevant les variables erron�es.
    final List allGoodVar = new ArrayList(_varTypeExpr.size() + (_existantVars == null ? 0 : _existantVars.length));
    allGoodVar.addAll(_varTypeExpr.keySet());
    allGoodVar.addAll(Arrays.asList(_existantVars));
    for (final Iterator it = _varTypeExpr.entrySet().iterator(); it.hasNext(); ) {
      final Map.Entry entry = (Map.Entry) it.next();
      final H2dVariableTypeCreated vari = (H2dVariableTypeCreated) entry.getKey();
      final Map<String, H2dVariableType> varvar2d = new HashMap<String, H2dVariableType>();
      // on construit le parser avec toutes les variables valides
      final CtuluExpr expr = new CtuluExpr();
      expr.addVariable(_constant);
      for (final Iterator iter = allGoodVar.iterator(); iter.hasNext(); ) {
        final H2dVariableType v = (H2dVariableType) iter.next();
        varvar2d.put(expr.addVar(v.getShortName(), v.getName()).getName(), v);
      }
      expr.getParser().parseExpression((String) entry.getValue());
      if (expr.getParser().hasError()) {
        allGoodVar.remove(vari);
        errorVar.put(vari, expr.getLastError());
        // on doit enlever de la table des resultats, toutes les variables d�pendantes de cette derni�res
        for (final Iterator itUsed = varPostData.entrySet().iterator(); itUsed.hasNext(); ) {
          final Map.Entry e = (Map.Entry) itUsed.next();
          final TrPostDataCreatedExpr create = (TrPostDataCreatedExpr) e.getValue();
          final Set usedVar = fillWithUsedVar((H2dVariableType) e.getKey(), create, varPostData);
          if (usedVar.contains(vari)) {
            errorVar.put(vari, TrResource.getS("D�pend d'une variables erron�e") + ": " + vari.getName());
            itUsed.remove();
          }
        }
      } else {
        varPostData.put(vari, TrPostDataHelper.createData(_src, expr.getParser(), varvar2d));
      }
    }
    if (errorVar.size() > 0) {
      _analyze.addError(TrResource.getS("Des variables ne seront pas ajout�es car leur formule est fausse:") + getListString(errorVar), -1);
    }
    return varPostData;
  }

  public static String getListString(final Collection _var) {
    final StringBuffer buf = new StringBuffer(_var.size() * 20);
    for (final Iterator iter = _var.iterator(); iter.hasNext(); ) {
      buf.append(CtuluLibString.LINE_SEP).append(((H2dVariableType) iter.next()).getName());
    }
    return buf.toString();
  }

  public static String getListString(final Map _varError) {
    final StringBuffer buf = new StringBuffer(_varError.size() * 40);
    for (final Iterator iter = _varError.entrySet().iterator(); iter.hasNext(); ) {
      final Entry e = (Entry) iter.next();
      buf.append(CtuluLibString.LINE_SEP).append(((H2dVariableType) e.getKey()).getName()).append(CtuluLibString.ESPACE).append(':')
        .append(e.getValue());
    }
    return buf.toString();
  }

  /**
   * @param _var la variable a tester
   * @param _expr l'expression, utilisee pour connaitre les variables dependantes
   * @param _varCreated le tableau des variables deja cr�es pour recuperer toutes les variables dependantes
   * @return la liste des variables utilisees.
   */
  public static Set fillWithUsedVar(final H2dVariableType _var, final TrPostDataCreatedExpr _expr, final Map _varCreated) {
    final HashSet set = new HashSet();
    fillWithUsedVar(set, _var, _expr, _varCreated);
    return set;
  }

  /**
   * @param _var la variable a tester
   * @param _expr l'expression, utilisee pour connaitre les variables dependantes
   * @param _varCreated le tableau des variables deja cr�es pour recuperer toutes les variables dependantes
   * @param _setToFill la liste a remplir avec les variables utilisee: important de le passer en parametre pour eviter
   *   les boucles infinies dues aux cycles ferm�e (A depend de B qui depend de A)
   */
  public static void fillWithUsedVar(final Set _setToFill, final H2dVariableType _var, final TrPostDataCreatedExpr _expr, final Map _varCreated) {
    final HashSet set = new HashSet();
    _expr.getUsedVar(set);
    // pour eviter de retester la variable d'origine
    set.add(_var);
    // si une variable utilis�e est deja presente dans le tableau _varCreated, on ajoute
    // toutes les variables utilis�e par cette derniere.
    for (final Iterator iter = set.iterator(); iter.hasNext(); ) {
      final H2dVariableType element = (H2dVariableType) iter.next();
      if (!_setToFill.contains(element) && _varCreated.containsKey(element)) {
        fillWithUsedVar(set, element, (TrPostDataCreatedExpr) _varCreated.get(element), _varCreated);
      }
    }
    _setToFill.addAll(set);
  }

  /**
   * Detecte les variables qui ont un nom ou un nom court dej� utilise. Remplit les set avec les variables test�s: evite
   * les doublons dans la liste initiales
   *
   * @param _initList la liste des variables H2dVariableType a parcourir
   * @param _forbiddenName les noms interdits : remplit avec les noms des var acceptees
   * @param _forbiddenShort les noms courts interdits : remplit avec les noms courts des var acceptees
   * @return la liste des variables appartenant � _initList qui ne sont pas valides
   */
  public static List getAlreadyUsedVar(final Collection _initList, final Set _forbiddenName, final Set _forbiddenShort) {
    if (_initList == null) {
      return Collections.EMPTY_LIST;
    }
    final List res = new ArrayList(_initList.size());
    for (final Iterator iter = _initList.iterator(); iter.hasNext(); ) {
      final H2dVariableType varToTest = (H2dVariableType) iter.next();
      if (_forbiddenName.contains(varToTest.getName()) || _forbiddenShort.contains(varToTest.getShortName())) {
        res.add(varToTest);
      }
      _forbiddenName.add(varToTest.getName());
      _forbiddenName.add(varToTest.getShortName());
    }
    return res;
  }
}
