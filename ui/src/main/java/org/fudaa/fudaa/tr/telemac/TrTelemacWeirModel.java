/**
 * @creation 3 d�c. 2004
 * @modification $Date: 2007-04-16 16:35:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuTable;
import org.locationtech.jts.geom.Envelope;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuil;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuilMng;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.model.MvFrontierModelDefault;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacWeirModel.java,v 1.14 2007-04-16 16:35:30 deniger Exp $
 */
public class TrTelemacWeirModel extends MvFrontierModelDefault {

  H2dTelemacSeuilMng seuils_;
  private boolean workOnFrontierPt_;

  protected class WeirTableModel extends AbstractTableModel {

    @Override
    public int getColumnCount() {
      return 6;
    }

    @Override
    public Class getColumnClass(final int _columnIndex) {
      if (_columnIndex <= 1) { return Integer.class; }
      return Double.class;
    }

    @Override
    public String getColumnName(final int _column) {
      switch (_column) {
      case 0:
        return EbliLib.getS("Indices");
      case 1:
        return TrResource.getS("Nombre de noeuds");
      case 2:
        return H2dVariableType.COTE_EAU + CtuluLibString.ESPACE + EbliLib.getS("Min");
      case 3:
        return H2dVariableType.COTE_EAU + CtuluLibString.ESPACE + EbliLib.getS("Max");
      case 4:
        return H2dVariableType.DEBIT + CtuluLibString.ESPACE + EbliLib.getS("Min");
      case 5:
        return H2dVariableType.DEBIT + CtuluLibString.ESPACE + EbliLib.getS("Max");
      default:
        return CtuluLibString.EMPTY_STRING;
      }

    }

    @Override
    public int getRowCount() {
      return seuils_.getNbSeuil();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      switch (_columnIndex) {
      case 0:
        return new Integer(_rowIndex + 1);
      case 1:
        return new Integer(seuils_.getSeuil(_rowIndex).getNbPoint());
      case 2:
        return CtuluLib.getDouble(seuils_.getTelemacSeuil(_rowIndex).getCoteMin());
      case 3:
        return CtuluLib.getDouble(seuils_.getTelemacSeuil(_rowIndex).getCoteMax());
      case 4:
        return CtuluLib.getDouble(seuils_.getTelemacSeuil(_rowIndex).getDebitMin());
      case 5:
        return CtuluLib.getDouble(seuils_.getTelemacSeuil(_rowIndex).getDebitMax());
      default:
        return null;
      }

    }

  }

  protected final boolean isWorkOnFrontierPt() {
    return workOnFrontierPt_;
  }

  protected final void setWorkOnFrontierPt(final boolean _workOnFrontierPt) {
    workOnFrontierPt_ = _workOnFrontierPt;
  }

  protected final H2dTelemacSeuilMng getSeuils() {
    return seuils_;
  }

  @Override
  public void fillWithInfo(final InfoData _m, final ZCalqueAffichageDonneesInterface _s) {
    if (isWorkOnFrontierPt()) {
      super.fillWithInfo(_m, _s);
    } else {
      fillWithWeirInfo(_m, _s.getLayerSelectionMulti());
    }
  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    if (isWorkOnFrontierPt()) { return super.createValuesTable(_layer); }
    return new CtuluTable(new WeirTableModel());
  }

  public void fillWithWeirInfo(final InfoData _m, final EbliListeSelectionMultiInterface _s) {
    int idx = -1;
    if (_s != null && !_s.isEmpty()) {
      final CtuluListSelectionInterface selec = _s.getSelection(0);
      if (selec.isOnlyOnIndexSelected()) {
        idx = selec.getMaxIndex();
      }
    }
    if (idx < 0) {
      _m.setTitle(TrResource.getS("Seuils"));
      _m.put(TrResource.getS("Nombre de seuils"), CtuluLibString.getString(seuils_.getNbSeuil()));
      if (_s != null && !_s.isEmpty()) {
        _m.put(TrResource.getS("Nombre de seuils s�lectionn�s"), CtuluLibString.getString(_s.getNbSelectedItem()));
      }
    } else {
      _m.setTitle(TrResource.getS("Seuil {0}", CtuluLibString.getString(idx + 1)));
      final H2dTelemacSeuil s = seuils_.getTelemacSeuil(idx);
      _m.put(TrResource.getS("Nombre de noeuds"), CtuluLibString.getString(s.getNbPoint()));
      Double d = s.getCommonCoefficientCote();
      if (d != null) {
        _m.put(H2dResource.getS("Cote du seuil"), d.toString());
      }
      d = s.getCommonCoefficientDebit();
      if (d != null) {
        _m.put(H2dResource.getS("Coefficient de d�bit"), d.toString());
      }

    }
  }

  /**
   * @param _seuils le seuil
   */
  public TrTelemacWeirModel(final H2dTelemacSeuilMng _seuils, final TrTelemacInfoSenderDefault _sender) {
    super(_seuils.getGrid());
    seuils_ = _seuils;
    setDelegate(_sender);
  }

  public int getNbBox() {
    return seuils_.getNbSeuil();
  }

  public int getNbPointInBox(final int _idxBox) {
    return seuils_.getSeuil(_idxBox).getNbPoint();
  }

  public void getBoiteForBox(final int _idx, final GrBoite _b) {
    final Envelope e = seuils_.getMinMaxForSeuil(_idx);
    _b.o_.setCoordonnees(e.getMinX(), e.getMinY(), 0);
    _b.e_.setCoordonnees(e.getMaxX(), e.getMaxY(), 0);
  }

  public void getPoint1For(final int _idxBox, final int _idxInBox, final GrPoint _p) {
    final int glob = seuils_.getGrid().getFrontiers().getIdxGlobalFrom(seuils_.getSeuil(_idxBox).getPtIdx1(_idxInBox));
    final EfGridInterface g = seuils_.getGrid();
    _p.setCoordonnees(g.getPtX(glob), g.getPtY(glob), g.getPtZ(glob));
  }

  public void getPoint2For(final int _idxBox, final int _idxInBox, final GrPoint _p) {
    final int glob = seuils_.getGrid().getFrontiers().getIdxGlobalFrom(seuils_.getSeuil(_idxBox).getPtIdx2(_idxInBox));
    final EfGridInterface g = seuils_.getGrid();
    _p.setCoordonnees(g.getPtX(glob), g.getPtY(glob), g.getPtZ(glob));
  }

  public boolean isCycle(final int _idxBox) {
    final H2dTelemacSeuil s = seuils_.getTelemacSeuil(_idxBox);
    return s.getPtIdx1(0) == s.getPtIdx1(s.getNbPoint() - 1);
  }

  @Override
  public GrBoite getDomaine() {
    if (workOnFrontierPt_) { return super.getDomaine(); }
    if (seuils_.getNbSeuil() == 0) { return null; }
    final Envelope e = seuils_.getMinMax();
    return new GrBoite(new GrPoint(e.getMinX(), e.getMinY(), 0), new GrPoint(e.getMaxX(), e.getMaxY(), 0));
  }

  @Override
  public int getNombre() {
    if (workOnFrontierPt_) { return super.getNombre(); }
    return seuils_.getNbSeuil();
  }
}
