package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.CtuluDurationDateFormatter;
import org.fudaa.ctulu.CtuluNumberFormatI;

/**
 * Donn�es persistantes de l'objet TrPostCourbeTreeModel. Ces donn�es serviront a charger/sauvegarder les don�nes
 * sp�cifique au trpostcourbe. Cette classe doit etre s�rializable.
 * 
 * @author Adrien Hadoux
 */
public class TrPostScopeCourbeTreeModelPersist {

  String timeFormat;

  public TrPostScopeCourbeTreeModelPersist() {
  }

  /**
   * Methode qui remplit le model en param d'entr�es avec les datas de cette classe.
   * 
   * @param model
   */
  public void fillModelWith(TrPostCourbeTreeModel model) {

  }

  protected CtuluNumberFormatI getDurationFormatter() {
    if (timeFormat == null) {
      return null;
    }
    return CtuluDurationDateFormatter.buildFromPattern(timeFormat);
  };

  /**
   * Methode qui remplit cetet classe a partir du modele fourni en entree.
   */
  public void fillDataWithModel(TrPostCourbeTreeModel model) {
    timeFormat = model.getLastDurationFormatter();

  }

}
