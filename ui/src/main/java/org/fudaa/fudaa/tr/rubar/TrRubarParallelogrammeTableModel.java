/**
 * @creation 11 oct. 2004
 * @modification $Date: 2006-09-19 15:07:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuTableCellEditor;
import com.memoire.bu.BuTextField;
import gnu.trove.TIntDoubleHashMap;
import gnu.trove.TIntHashSet;
import java.awt.Component;
import java.awt.Toolkit;
import java.util.Arrays;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluNumberFormater;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.h2d.H2dParallelogrammeData;
import org.fudaa.dodico.h2d.H2dParallelogrammeManager;
import org.fudaa.dodico.h2d.rubar.H2dRubarDonneesBrutes;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarParallelogrammeTableModel.java,v 1.6 2005/12/14 16:38:09 deniger Exp $
 */
public class TrRubarParallelogrammeTableModel extends AbstractTableModel {

  transient CtuluNumberFormater[] fmt_;

  transient H2dRubarDonneesBrutes br_;

  transient H2dParallelogrammeData parall_;

  transient TIntDoubleHashMap[] intNewVal_;

  transient int[] varIdx_;

  /**
   * @param _br
   * @param _parall
   */
  public TrRubarParallelogrammeTableModel(final H2dRubarDonneesBrutes _br, final H2dParallelogrammeData _parall) {
    super();
    br_ = _br;
    parall_ = _parall;
    varIdx_ = br_.getAvailableVariableId();
    intNewVal_ = new TIntDoubleHashMap[2 + varIdx_.length];
    fmt_ = new CtuluNumberFormater[varIdx_.length];
    for (int i = varIdx_.length - 1; i >= 0; i--) {
      fmt_[i] = br_.getFmt().getFormatterFor((H2dVariableType) br_.getVariableId(getVarId(i)));
    }

  }

  private int getVarId(final int _idx) {
    return varIdx_[_idx];
  }

  @Override
  public int getColumnCount() {
    return 2 + varIdx_.length;
  }

  @Override
  public int getRowCount() {
    return 4;
  }

  @Override
  public String getColumnName(final int _column) {
    if (_column == 0) { return "X"; }
    if (_column == 1) { return "Y"; }
    return br_.getVariableId(getVarId(_column - 2)).toString();
  }

  protected CtuluNumberFormater getFormat(final int _col) {
    if (_col <= 1) { return br_.getFmt().getXY(); }
    return fmt_[_col - 2];
  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    // les variables sont editables (col sup a 1)
    // les coordonnées du point 2 (X3) ne le sont pas
    return (_columnIndex > 1) || _rowIndex != 2;
  }

  protected TableCellEditor getTableCellEditor() {
    return new CellEditor();
  }

  protected TableCellRenderer getTableCellRenderer() {
    return new CellRenderer();
  }

  class CellRenderer extends CtuluCellTextRenderer {

    @Override
    public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected,
        final boolean _hasFocus, final int _row, final int _column) {
      final CtuluNumberFormater fmt = getFormat(_column);
      if (fmt == null) {
        setToolTipText(null);
      } else {
        setToolTipText(fmt.getValueValidator().getDescription());
      }
      return super.getTableCellRendererComponent(_table, _value, _isSelected, _hasFocus, _row, _column);
    }
  }

  private class CellEditor extends BuTableCellEditor {

    protected CellEditor() {
      super((BuTextField) new CtuluValueEditorDouble(true).createEditorComponent());
    }

    @Override
    public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _isSelected,
        final int _row, final int _column) {
      final JTextField r = (JTextField) super.getTableCellEditorComponent(_table, _value, _isSelected, _row, _column);
      final CtuluNumberFormater fmt = getFormat(_column);
      if (fmt == null) {
        r.setToolTipText(null);
      } else {

        r.setToolTipText(fmt.getValueValidator().getDescription());
      }
      return r;
    }
  }

  @Override
  public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
    if (_value == null) { return; }
    final double v = ((Double) _value).doubleValue();
    if (v == getDoubleValueAt(_rowIndex, _columnIndex)) { return; }
    if (intNewVal_[_columnIndex] == null) {
      intNewVal_[_columnIndex] = new TIntDoubleHashMap(4);
    }
    final CtuluNumberFormater fmt = getFormat(_columnIndex);
    if (fmt != null && fmt.getValueValidator().isValueValid(v)) {
      intNewVal_[_columnIndex].put(_rowIndex, v);
    } else {
      Toolkit.getDefaultToolkit().beep();
    }
  }

  protected boolean isValid() {
    final double x1 = getDoubleValueAt(0, 0);
    final double y1 = getDoubleValueAt(0, 1);
    final double x2 = getDoubleValueAt(1, 0);
    final double y2 = getDoubleValueAt(1, 1);
    final double x4 = getDoubleValueAt(3, 0);
    final double y4 = getDoubleValueAt(3, 1);
    return ((x1 != x2) || (y1 != y2)) && ((x1 != x4) || (y1 != y4)) && ((x4 != x2) || (y4 != y2));
  }

  protected void updateParallel(final CtuluCommandContainer _cmd) {
    if (parall_ == null) { return; }
    // on va mettre des x,y en action .....
    final CtuluCommandComposite cmd = new CtuluCommandComposite();
    if ((intNewVal_[0] != null) || (intNewVal_[1] != null)) {
      final TIntHashSet idxModify = new TIntHashSet(4);
      if (intNewVal_[0] != null) {
        idxModify.addAll(intNewVal_[0].keys());
      }
      if (intNewVal_[1] != null) {
        idxModify.addAll(intNewVal_[1].keys());
      }
      final int[] idxTab = idxModify.toArray();
      Arrays.sort(idxTab);
      for (int i = idxTab.length - 1; i >= 0; i--) {
        final int idx = idxTab[i];
        parall_.set(idx, getDoubleValueAt(idx, 0), getDoubleValueAt(idx, 1), cmd);
      }
    }
    for (int i = 0; i < varIdx_.length; i++) {
      final int reeli = 2 + i;
      if (intNewVal_[reeli] != null) {
        parall_.getDoubleData(varIdx_[i]).set(intNewVal_[reeli].keys(), intNewVal_[reeli].getValues(), cmd);
      }
    }
    if (_cmd != null) {
      _cmd.addCmd(cmd.getSimplify());
    }
  }

  protected void createParallel(final H2dParallelogrammeManager _mng, final CtuluCommandContainer _cmd) {
    final H2dParallelogrammeData d = new H2dParallelogrammeData(br_.getNbVariable());
    d.set(0, getDoubleValueAt(0, 0), getDoubleValueAt(0, 1), null);
    d.set(1, getDoubleValueAt(1, 0), getDoubleValueAt(1, 1), null);
    d.set(3, getDoubleValueAt(3, 0), getDoubleValueAt(3, 1), null);
    for (int i = 0; i < varIdx_.length; i++) {
      final int reeli = 2 + i;
      if (intNewVal_[reeli] != null) {
        d.getDoubleData(varIdx_[i]).set(intNewVal_[reeli].keys(), intNewVal_[reeli].getValues(), null);
      }
    }
    _mng.addParallelogramme(d, _cmd);
  }

  protected double getDoubleValueAt(final int _rowIndex, final int _columnIndex) {
    if ((intNewVal_[_columnIndex] != null) && (intNewVal_[_columnIndex].contains(_rowIndex))) { return intNewVal_[_columnIndex]
        .get(_rowIndex); }
    if (parall_ == null) { return 0; }
    if (_columnIndex == 0) { return parall_.getX(_rowIndex); }
    // Y
    if (_columnIndex == 1) { return parall_.getY(_rowIndex); }
    // une valeur normale
    final int valueIdx = _columnIndex - 2;
    return parall_.getValue(varIdx_[valueIdx], _rowIndex);
  }

  @Override
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {
    final double d = getDoubleValueAt(_rowIndex, _columnIndex);
    final CtuluNumberFormater f = getFormat(_columnIndex);
    return f == null ? Double.toString(d) : f.format(d);
  }
}