/*
 * @creation 14 mars 07
 * @modification $Date: 2007-04-30 14:22:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.fu.FuComparator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.dodico.h2d.reflux.H2dRefluxParameters;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.commun.EbliSelectionState;
import org.fudaa.fudaa.meshviewer.layer.MvFrontierPointLayer;
import org.fudaa.fudaa.tr.common.TrCourbeUseCounter;
import org.fudaa.fudaa.tr.common.TrCourbeUseResultsAbstract;

/**
 * @author fred deniger
 * @version $Id: TrRefluxCourbeUseResult.java,v 1.2 2007-04-30 14:22:39 deniger Exp $
 */
public class TrRefluxCourbeUseResult extends TrCourbeUseResultsAbstract {

  boolean[] isPn_;
  final H2dRefluxParameters params_;

  /**
   * evolution -> List d'entier (index generaux des point frontiere).
   */
  final TrCourbeUseCounter usedInCl_ = new TrCourbeUseCounter(H2dResource.getS("Conditions limites"));

  /**
   * evolution -> List d'entier des indices des éléments.
   */
  public final TrCourbeUseCounter usedInPn_ = new TrCourbeUseCounter(H2dResource.getS("Propriétés élémentaires"));

  public TrRefluxCourbeUseResult(final H2dRefluxParameters _params, final TrRefluxVisuPanel _visu) {
    super(_visu);
    params_ = _params;
  }

  @Override
  public void validSearch() {
    final int nbEvolCl = usedInCl_.getNbEvol();
    final int nbEvolPn = usedInPn_.getNbEvol();
    final List allEvols = new ArrayList(nbEvolCl + nbEvolPn);
    EvolutionReguliereInterface[] evols = (EvolutionReguliereInterface[]) usedInCl_.getEvols().toArray(
        new EvolutionReguliereInterface[nbEvolCl]);
    Arrays.sort(evols, FuComparator.STRING_COMPARATOR);
    allEvols.addAll(Arrays.asList(evols));
    int size = allEvols.size();
    final int idx = size;
    evols = (EvolutionReguliereInterface[]) usedInPn_.getEvols().toArray(new EvolutionReguliereInterface[nbEvolPn]);
    allEvols.addAll(Arrays.asList(evols));
    size = allEvols.size();
    evols_ = (EvolutionReguliereInterface[]) allEvols.toArray(new EvolutionReguliereInterface[size]);
    isPn_ = new boolean[evols_.length];
    for (int i = idx; i < size; i++) {
      isPn_[i] = true;

    }

  }

  @Override
  public void setFilter(final EvolutionReguliereInterface _evol) {
    usedInPn_.setEvolToTest(_evol);
    usedInCl_.setEvolToTest(_evol);

  }

  @Override
  public TrCourbeUseCounter getCounter(final int _rowIndex) {
    return isPn_[_rowIndex] ? usedInPn_ : usedInCl_;
  }

  @Override
  public void selectInView(final EvolutionReguliereInterface _eve) {
    final int idx = CtuluLibArray.findObject(evols_, _eve);
    if (idx >= 0) {
      if (isPn_[idx]) {
        selectPnInView(_eve);
      } else {
        selectClInView(_eve);
      }
    }

  }

  protected void selectPnInView(final EvolutionReguliereInterface _eve) {
    selectedInMeshLayer(usedInPn_.getIdxSelected(_eve));

  }

  protected void selectClInView(final EvolutionReguliereInterface _eve) {
    final int[] select = usedInCl_.getIdxSelected(_eve);
    if (select != null) {
      final MvFrontierPointLayer layer = visu_.getGroupBoundary().getBcPointLayer();
      visu_.getArbreCalqueModel().setSelectionCalque(layer);
      layer.changeSelectionFromGlobalIdx(new CtuluListSelection(select), EbliSelectionState.ACTION_REPLACE);
      visu_.zoomOnSelected();
      activateVisuFrame();
    }

  }

}
