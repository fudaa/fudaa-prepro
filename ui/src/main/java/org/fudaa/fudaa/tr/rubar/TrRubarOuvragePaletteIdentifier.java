/**
 * @creation 4 janv. 2005
 * @modification $Date: 2006-09-08 16:53:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageI;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageRef;
import org.fudaa.ebli.palette.BPlageDiscret;
import org.fudaa.fudaa.tr.common.TrPreferences;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarOuvragePaletteIdentifier.java,v 1.10 2006-09-08 16:53:09 deniger Exp $
 */
public class TrRubarOuvragePaletteIdentifier {

  public static void clearPreferences() {
    final List r = new ArrayList();
    for (final Enumeration e = TrPreferences.TR.keys(); e.hasMoreElements();) {
      final String o = (String) e.nextElement();
      if (o.startsWith("rubarOuvragePalette.id.")) {
        r.add(o);
      }
    }
    for (int i = r.size() - 1; i >= 0; i--) {
      TrPreferences.TR.removeProperty((String) r.get(i));
    }
  }

  boolean containsOuvElt_;
  boolean elt2Set_;

  H2dRubarOuvrageRef ref_;

  @Override
  public boolean equals(final Object _obj) {
    if (_obj == this) { return true; }
    if (_obj == null) { return false; }
    if (_obj.getClass().equals(getClass())) {
      final TrRubarOuvragePaletteIdentifier id = (TrRubarOuvragePaletteIdentifier) _obj;
      return id.elt2Set_ == elt2Set_ && id.containsOuvElt_ == containsOuvElt_ && id.ref_.equals(ref_);
    }
    return false;
  }

  @Override
  public int hashCode() {
    int r = elt2Set_ ? -11 : 11;
    if (containsOuvElt_) {
      r *= 13;
    }
    r += ref_.hashCode();
    return r;
  }

  /**
   * @param _ref
   * @param _elt2Set
   * @param _containsOuvElt
   */
  public TrRubarOuvragePaletteIdentifier(final H2dRubarOuvrageRef _ref, final boolean _elt2Set,
      final boolean _containsOuvElt) {
    super();
    ref_ = _ref;
    if (ref_ == null) { throw new IllegalArgumentException("ref must not be null"); }
    elt2Set_ = _elt2Set;
    containsOuvElt_ = _containsOuvElt;
  }

  TrRubarOuvragePaletteIdentifier() {}

  protected BPlageDiscret getPlageDiscret() {
    final BPlageDiscret r = new BPlageDiscret(this);
    r.setCouleur(getColorInPref());
    return r;
  }

  /**
   * @return la couleur enregistree dans les pref. Renvoie null si pas de pref ou pref mal définie
   */
  public Color getColorInPref() {
    return TrPreferences.TR.getColorProperty("rubarOuvragePalette.id." + hashCode(), null);
  }

  /**
   * @param _o
   * @return true si cet identifieur correspond a l'ouvrage o.
   */
  public boolean isEquivalent(final H2dRubarOuvrageI _o) {
    return (_o.getOuvrageRef() == ref_) && (elt2Set_ == _o.isElt2Set())
        && containsOuvElt_ == _o.containsOuvrageElementaire();
  }

  /**
   * @param _c la couleur a enregistrer.
   */
  public void saveColorInPref(final Color _c) {
    TrPreferences.TR.putColorProperty("rubarOuvragePalette.id." + hashCode(), _c);
  }

  @Override
  public String toString() {
    final String esp = CtuluLibString.ESPACE;
    final StringBuffer r = new StringBuffer();
    r.append(TrResource.getS("Ref")).append(esp).append(ref_.getRubarCode());
    if (!elt2Set_) {
      r.append(", ").append(TrResource.getS("Maille 2 indéfinie"));
    }
    if (containsOuvElt_) {
      r.append(", ").append(TrResource.getS("Ouvrages élémentaires"));
    } else {
      r.append(", ").append(TrResource.getS("Sans ouvrages élémentaires"));
    }
    return r.toString();
  }
}