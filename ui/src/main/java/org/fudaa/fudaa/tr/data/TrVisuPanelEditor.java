/*
 * @creation 6 avr. 2005
 *
 * @modification $Date: 2007-06-14 12:01:41 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuWizardDialog;
import com.memoire.fu.FuLog;
import org.apache.commons.collections.Predicate;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluValuesEditorPanel;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;
import org.fudaa.dodico.ef.operation.clean.EfOperationCleanGrid;
import org.fudaa.dodico.ef.operation.translate.TranslatePointCleanValidator;
import org.fudaa.dodico.h2d.*;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.calque.find.CalqueFindFlecheExpression;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.sig.wizard.FSigWizardVariableModifier;
import org.fudaa.fudaa.tr.TrLauncherDefault;
import org.fudaa.fudaa.tr.common.Tr3DFactory;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrProjectDispatcherListener;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.export.TrExportFactory;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.profile.*;

import javax.swing.*;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * @author Fred Deniger
 * @version $Id: TrVisuPanelEditor.java,v 1.34 2007-06-14 12:01:41 deniger Exp $
 */
public abstract class TrVisuPanelEditor extends TrVisuPanel {
  private static class TrSiFlecheLayer extends ZCalqueFleche implements H2dSIListener, MvLayerGrid {
    protected TrSiFlecheLayer(final H2dSiSourceInterface _src) {
      super(new TrSiFlecheModel(_src));
      _src.addListener(this);
    }

    @Override
    public EbliFindExpressionContainerInterface getExpressionContainer() {
      return new CalqueFindFlecheExpression(modele_, false);
    }

    @Override
    public void siAdded(final H2dVariableType _var) {
      repaint();
    }

    @Override
    public int[] getSelectedElementIdx() {
      return null;
    }

    @Override
    public int[] getSelectedPtIdx() {
      return getSelectedIndex();
    }

    @Override
    public boolean isSelectionElementEmpty() {
      return true;
    }

    @Override
    protected String getFlecheUnit() {
      return H2dVariableType.VITESSE.getCommonUnitString();
    }

    @Override
    public int[] getSelectedEdgeIdx() {
      return null;
    }

    @Override
    public boolean isSelectionEdgeEmpty() {
      return true;
    }

    @Override
    public boolean isSelectionPointEmpty() {
      return isSelectionEmpty();
    }

    @Override
    public void siChanged(final H2dVariableType _var) {
      repaint();
    }

    @Override
    public void siRemoved(final H2dVariableType _var) {
      repaint();
    }
  }

  protected abstract class EditPropertiesAbstract extends EbliActionSimple {
    public EditPropertiesAbstract(final String _title, final Icon _i, final String _name) {
      super(_title, _i, _name);
    }

    public abstract ZCalqueAffichageDonneesInterface getLayer();

    public abstract H2dVariableProviderInterface getVariables();

    @Override
    public void updateStateBeforeShow() {
      final H2dVariableProviderInterface prov = getVariables();
      super.setEnabled(prov != null && prov.containsVarToModify());
      if (prov == null && FuLog.isDebug()) {
        FuLog.debug("TRV: no data for action " + super.getTitle());
      }
    }
  }

  protected abstract class EditProperties extends EditPropertiesAbstract {
    public EditProperties() {
      super(TrResource.getS("Editer"), null, "EDIT_LAYER_SELECTION");
    }

    public abstract Predicate getVariableFilter();

    @Override
    public void actionPerformed(final ActionEvent _e) {
      editProperties(this.getVariables(), this.getLayer(), getVariableFilter());
    }

    @Override
    public void updateStateBeforeShow() {
      super.updateStateBeforeShow();
      if (super.isEnabled() && getLayer().isSelectionEmpty()) {
        super.setEnabled(false);
      }
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: TrVisuPanelEditor.java,v 1.34 2007-06-14 12:01:41 deniger Exp $
   */
  public abstract class EditPropertiesFromGeom extends EditPropertiesAbstract {
    public EditPropertiesFromGeom() {
      super(FSigLib.getS("Initialiser depuis donn�es g�ographiques"), null, "INITIALISE_FROM_GEOM_DATA");
      setDefaultToolTip(TrResource.getS("Ouvre un assistant permettant de<br>modifier les propri�t�s � partir des donn�es g�om�triques"));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      initDataFromGeom(getVariables(), (BCalque) getLayer());
    }
  }

  ;

  /**
   * @author Fred Deniger
   * @version $Id: TrVisuPanelEditor.java,v 1.34 2007-06-14 12:01:41 deniger Exp $
   */
  public class SiEditGeomAction extends EditPropertiesFromGeom {
    @Override
    public ZCalqueAffichageDonneesInterface getLayer() {
      return getSiPointLayer();
    }

    @Override
    public H2dVariableProviderInterface getVariables() {
      return getParams().getSiData();
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: TrVisuPanelEditor.java,v 1.34 2007-06-14 12:01:41 deniger Exp $
   */
  public class NodalEditGeomAction extends EditPropertiesFromGeom {
    @Override
    public ZCalqueAffichageDonneesInterface getLayer() {
      return getGridGroup().getPointLayer();
    }

    @Override
    public H2dVariableProviderInterface getVariables() {
      return getParams().getNodalData();
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: TrVisuPanelEditor.java,v 1.34 2007-06-14 12:01:41 deniger Exp $
   */
  public class NodalEditAction extends EditProperties {
    public NodalEditAction() {
      super.putValue(EbliActionInterface.DEFAULT_TOOLTIP, TrResource.getS("Editer les noeuds s�lectionn�s"));
      super.putValue(EbliActionInterface.UNABLE_TOOLTIP, TrResource.getS("S�lectionner au moins un noeud"));
    }

    @Override
    public Predicate getVariableFilter() {
      return TrVisuPanelEditor.this.getNodalVariableFilter();
    }

    @Override
    public ZCalqueAffichageDonneesInterface getLayer() {
      return getGridGroup().getPointLayer();
    }

    @Override
    public H2dVariableProviderInterface getVariables() {
      return getParams().getNodalData();
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: TrVisuPanelEditor.java,v 1.34 2007-06-14 12:01:41 deniger Exp $
   */
  public class SiEditAction extends EditProperties {
    public SiEditAction() {
      super.putValue(EbliActionInterface.DEFAULT_TOOLTIP, TrResource.getS("Editer les conditions initiales aux noeuds s�lectionn�s"));
      super.putValue(EbliActionInterface.UNABLE_TOOLTIP, TrResource.getS("S�lectionner au moins un noeud"));
    }

    @Override
    public Predicate getVariableFilter() {
      return null;
    }

    @Override
    public ZCalqueAffichageDonneesInterface getLayer() {
      return getSiPointLayer();
    }

    @Override
    public H2dVariableProviderInterface getVariables() {
      return getParams().getSiData();
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: TrVisuPanelEditor.java,v 1.34 2007-06-14 12:01:41 deniger Exp $
   */
  public class ElementEditAction extends EditProperties {
    public ElementEditAction() {
      super.putValue(EbliActionInterface.DEFAULT_TOOLTIP, TrResource.getS("Editer les �l�ments s�lectionn�s"));
      super.putValue(EbliActionInterface.UNABLE_TOOLTIP, TrResource.getS("S�lectionner au moins un �l�ment"));
    }

    @Override
    public Predicate getVariableFilter() {
      return null;
    }

    @Override
    public ZCalqueAffichageDonneesInterface getLayer() {
      return getGridGroup().getPolygonLayer();
    }

    @Override
    public H2dVariableProviderInterface getVariables() {
      return getParams().getElementData();
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: TrVisuPanelEditor.java,v 1.34 2007-06-14 12:01:41 deniger Exp $
   */
  public class ElementEditGeomAction extends EditPropertiesFromGeom {
    @Override
    public ZCalqueAffichageDonneesInterface getLayer() {
      return getGridGroup().getPolygonLayer();
    }

    @Override
    public H2dVariableProviderInterface getVariables() {
      return getParams().getElementData();
    }
  }

  protected class SiCheckHauteurEau extends EbliActionSimple {
    public SiCheckHauteurEau() {
      super(TrResource.getS("Supprimer hauteur d'eau n�gative"), null, "SI_EDIT_VALUES");
      setDefaultToolTip(TrResource.getS("Supprimer les hauteurs d'eau n�gatives"));
    }

    @Override
    public void updateStateBeforeShow() {
      super.setEnabled(getSiPointLayer() != null);
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final TrSiLayer si = TrVisuPanelEditor.this.getSiPointLayer();
      final String message = si.isSelectionEmpty() ? TrResource.getS("Voulez-vous tester tous les noeuds du projet ?") : TrResource
          .getS("Voulez-vous tester les noeuds s�lectionn�s?");
      if (!getImpl().question(
          super.getTitle(),
          TrResource.getS("Cette action r�initialise la hauteur d'eau initiale � z�ro \nsi elle est actuellement n�gative") + CtuluLibString.LINE_SEP
              + message)) {
        return;
      }

      final int i = si.getSource().testCoteEau(si.isSelectionEmpty() ? null : si.getLayerSelection(), getCmdMng());
      getImpl().setMainMessageAndClear(TrResource.getS("Nombre de noeuds corrig�s:") + CtuluLibString.EMPTY_STRING + CtuluLibString.getString(i));
    }
  }

  protected class SiClearHAction extends EbliActionSimple {
    public SiClearHAction() {
      super(TrResource.getS("Supprimer zones inond�es (h=0)"), null, "SI_REMOVE_H");
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final TrSiLayer lay = getSiPointLayer();
      if (lay != null && !lay.isSelectionEmpty()) {
        final H2dSiSourceInterface src = ((TrSiNodeLayer) lay).getSource();
        src.setHToZero(lay.getLayerSelection().getSelectedIndex(), getCmdMng());
      }
    }

    @Override
    public void updateStateBeforeShow() {
      final TrSiLayer lay = getSiPointLayer();
      super.setEnabled(lay != null && !lay.isSelectionEmpty());
    }
  }

  protected abstract void fireGridPointModified();

  /**
   * @return for some modelling system it's possible to use an undefined value for the bathy
   */
  public Double getUndefinedValueForZ() {
    return null;
  }

  /**
   * @param _impl
   */
  public TrVisuPanelEditor(final FudaaCommonImplementation _impl) {
    super(_impl);
  }

  public Predicate getNodalVariableFilter() {
    return null;
  }

  protected BGroupeCalque buildGroupSI() {
    BGroupeCalque gr = getGroupSI();
    if (gr != null) {
      return gr;
    }
    gr = new BGroupeCalque();
    gr.setTitle(TrResource.getS("Conditions initiales"));
    gr.setName(getSiGroupName());
    gr.putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Permet de g�rer les conditions initiales"));
    addCalque(gr);
    return gr;
  }

  protected void buildGroupSiLayers(final H2dSiSourceInterface _si) {
    final BGroupeCalque grSi = buildGroupSI();
    final TrSiProfilModel poly = new TrSiProfilModel(getGrid(), getCmdMng());
    final ZCalqueLigneBriseeEditable si = new TrSiProfilLayer(poly, getCqLegend(), getEditor());
    grSi.add(si);
    final TrSiFlecheLayer fl = new TrSiFlecheLayer(_si);
    fl.setVisible(false);
    fl.setName("cqSiFleche");
    fl.setTitle(H2dResource.getS("Vitesses initiales"));
    fl.setModele(new TrSiFlecheModel(_si));
    grSi.enPremier(fl);
  }

  @Override
  protected final void initButtonGroupSpecific(final List _l, final ZEbliCalquePanelController _res) {
    super.initButtonGroupSpecific(_l, _res);
    _l.add(new MvProfileAction(new ProfileAdapter(), getImpl(), this));
  }

  public class ProfileAdapter implements MvProfileTarget {
    final EfGridData data_ = getParams().createGridDataAdapter();
    final EfGridDataInterpolator interpolator_ = new EfGridDataInterpolator(data_, getParams().getVectorContainer());

    @Override
    public EfGridData getData() {
      return data_;
    }

    @Override
    public FudaaCourbeTimeListModel getTimeModel() {
      return null;
    }

    @Override
    public EfGridDataInterpolator getInterpolator() {
      return interpolator_;
    }

    @Override
    public CtuluVariable[] getVars() {
      return getParams().createGridDataAdapter().getVar();
    }

    @Override
    public void profilPanelCreated(final MvProfileFillePanel _panel, final ProgressionInterface _prog, final String _title) {
      final MvProfileTreeFille fille = new MvProfileTreeFille(_panel, MvProfileBuilder.getProfileName(_title), getImpl(), null);

      MvProfileFillePanel.finishProfilCreation(getImpl(), _panel, fille);
      final Observer obs = new Observer() {
        @Override
        public void update(final Observable _o, final Object _arg) {
          fille.varUpdated((CtuluVariable) _arg);
        }
      };
      ((TrProjectDispatcherListener) getParams().getMainListener()).addGridDataObserver(obs);
      fille.addInternalFrameListener(new InternalFrameAdapter() {
        @Override
        public void internalFrameClosed(final InternalFrameEvent _e) {
          ((TrProjectDispatcherListener) getParams().getMainListener()).removeGridDataObserver(obs);
        }
      });
    }

    public TrPostSource getDataSource() {
      return null;
    }
  }

  @Override
  public final FSigEditor createGisEditor() {
    TrGisProjectEditor trGisProjectEditor = new TrGisProjectEditor(this);
    mng_ = trGisProjectEditor.getMng();
    return trGisProjectEditor;
  }

  /**
   * @return le maillage du projet
   */
  public abstract EfGridInterface getGrid();

  public abstract H2dParameters getParams();

  @Override
  public void view3D(final JFrame _f) {
    new Tr3DFactory().afficheFrame(_f, getParams().createGridDataAdapter(), getParams().getVectorContainer(), getImpl(), getGroupFond(), getLayerBcPoint());
  }

  @Override
  public void startExport(final CtuluUI _impl) {
    final H2dGridDataAdapter data = getParams().createGridDataAdapter();
    final H2dVariableType[] vars = data.getVar();
    if (getParams().isGridInvalid()) {
      new CtuluRunnable(TrLib.getString("Construction du nouveau maillage"), _impl) {
        @Override
        public boolean run(ProgressionInterface _proj) {
          final EfOperationCleanGrid efOperationCleanGrid = new EfOperationCleanGrid();
          efOperationCleanGrid.setInitGridData(data);
          efOperationCleanGrid.setMinDistance(TranslatePointCleanValidator.DEFAULT_MIN_DISTANCE);
          final EfGridData newData = efOperationCleanGrid.process(_proj).getGridData();
          EfLib.orienteGrid(newData.getGrid(), _proj, true, null);
          EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
              TrExportFactory
                  .startExport(TrLauncherDefault.getHydIdFromPrefFile(), getImpl(), newData, vars, null, null, TrVisuPanelEditor.this,
                      TrVisuPanelEditor.getTextForExportSpecificGrid(), TrExportFactory.getNbConcentrationBlocks(vars));
            }
          });

          return false;
        }
      }.run();
    } else {
      TrExportFactory.startExport(TrLauncherDefault.getHydIdFromPrefFile(), getImpl(), data, vars, getCurrentSelection(), getParams()
          .getVectorContainer(), this, null, TrExportFactory.getNbConcentrationBlocks(vars));
    }
  }

  private static String getTextForExportSpecificGrid() {
    return
        "<html><body>" +
            TrLib.getString("Le projet courant courant contient un maillage avec une structure modifi�e et ne peut pas �tre sauvegard�.")
            + "<br>"
            + TrLib.getString("Le maillage export� aura une structure valid�e.")
            + "</body><html>";
  }

  /**
   * @return le calque utilis� pour definir les plans des si.
   */
  public BCalque getLayerSiProfiles() {
    final BCalque parent = getGroupSI();
    if (parent == null) {
      return null;
    }
    return parent.getCalqueParNom(TrSiProfilLayer.getDefaultName());
  }

  public String getSiGroupName() {
    return "grSI";
  }

  @Override
  public String editGridPoint() {
    return editProperties(getParams().getNodalData(), getGridGroup().getPointLayer(), getNodalVariableFilter());
  }

  public String editSiNodeOrElement() {
    return editProperties(getParams().getSiData(), getSiPointLayer());
  }

  protected void afficheValuesPanel(final CtuluValuesEditorPanel _panel, final String _title) {
    final CtuluDialog d = _panel.createDialog(getFrame());
    d.setTitle(_title);
    d.setOption(CtuluDialog.OK_CANCEL_OPTION);
    d.afficheDialogModal();
  }

  public String editProperties(final H2dVariableProviderInterface _provider, final ZCalqueAffichageDonneesInterface _cq) {
    return editProperties(_provider, _cq, null);
  }

  public String editProperties(final H2dVariableProviderInterface _provider, final ZCalqueAffichageDonneesInterface _cq, Predicate variablePredicate) {
    String r = null;
    final H2dVariableType[] var = _provider == null ? null : _provider.getVarToModify();
    if (_provider != null && var != null && var.length > 0) {
      if (_cq.isSelectionEmpty()) {
        r = MvResource.getS("S�lection vide");
      } else {
        final CtuluValuesEditorPanel pn = buildValuesPanel(_cq, _provider, variablePredicate);
        String title = null;

        if (_cq.isOnlyOneObjectSelected()) {
          if (_provider.isElementVar()) {
            title = MvResource.getS("El�ment n� {0}",
                CtuluLibString.getString(_cq.getLayerSelection().getMaxIndex() + 1));
          } else {
            title = MvResource.getS("Noeud n� {0}",
                CtuluLibString.getString(getGridGroup().getPointLayer().getLayerSelection().getMaxIndex() + 1));
          }
        } else {
          title = _cq.getTitle();
        }
        afficheValuesPanel(pn, title);
      }
    } else {
      TrResource.getS("Les propri�t�s ne sont pas modifiables");
    }
    return r;
  }

  public TrSiLayer getSiPointLayer() {
    return (TrSiLayer) getGroupSI().getCalqueParNom(TrSiNodeLayer.getDefaultName());
  }

  public final CtuluValuesEditorPanel buildValuesPanel(final ZCalqueAffichageDonneesInterface _cq, final H2dVariableProviderInterface _src,
                                                       Predicate variablePredicate) {
    return TrValuesEditorBuilder.buildPanelFor(_src, _cq.getLayerSelection().getSelectedIndex(), variablePredicate, getCmdMng());
  }

  public void initNodeDataFromGeom() {
    initDataFromGeom(getParams().getNodalData(), getGridGroup().getPointLayer());
  }

  public void initElementDataFromGeom() {
    initDataFromGeom(getParams().getElementData(), getGridGroup().getPolygonLayer());
  }

  public void initSiDataFromGeom() {
    initDataFromGeom(getParams().getSiData(), (BCalque) getSiPointLayer());
  }

  /**
   * A redefinir si l'on veut ajouter des variables discretes.
   *
   * @param _setToFill la liste a completer
   */
  public void getDiscreteIsoPainters(final Collection _setToFill) {
  }

  /**
   * Permet de remplir les varialbes iso automatiquement.
   */
  @Override
  public final void getIsoPainters(final Collection _setToFill) {
    TrIsoPainter.fillWithIsoPainter(getParams().getNodalData(), _setToFill, false);
    TrIsoPainter.fillWithIsoPainter(getParams().getSiData(), _setToFill, true);
    TrIsoPainter.fillWithIsoPainter(getParams().getElementData(), _setToFill, false);
    getDiscreteIsoPainters(_setToFill);
  }

  protected void initDataFromGeom(final H2dVariableProviderInterface _provider, final BCalque _cq) {
    final FSigWizardVariableModifier modifier = new FSigWizardVariableModifier(getGroupGIS(), _provider, getImpl(), getCmdMng(), _cq);
    final H2dVariableType[] var = _provider == null ? null : _provider.getVarToModify();
    if (var != null && var.length > 0) {
      final BuWizardDialog dialog = new BuWizardDialog(getFrame(), modifier);
      dialog.setModal(true);
      dialog.setLocationRelativeTo(TrVisuPanelEditor.this);
      dialog.pack();
      dialog.show();
      dialog.dispose();
    } else {
      getImpl().error(_cq.getTitle() + ": " + TrResource.getS("Les propri�t�s ne sont pas modifiables"));
    }
  }
}
