/*
 *  @creation     11 mars 2005
 *  @modification $Date: 2006-09-19 15:07:27 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.ebli.calque.BCalqueSaverSingle;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.fudaa.tr.persistence.TrPostFlecheLayerPersistence;
import org.fudaa.fudaa.tr.persistence.TrPostIsoLayerPersistence;

/**
 * @deprecated garder pour ancienne sauvegarde
 * @author Fred Deniger
 * @version $Id: TrPostFlecheLayerSaver.java,v 1.4 2006-09-19 15:07:27 deniger Exp $
 */
@Deprecated
public final class TrPostFlecheLayerSaver extends BCalqueSaverSingle {

  boolean isBase_;

  /**
   * @deprecated garder pour ancienne sauvegarde
   * @param _cqToSave
   * @param _prog
   */
  @Deprecated
  public TrPostFlecheLayerSaver() {
    setPersistenceClass(TrPostFlecheLayerPersistence.class.getName());

  }

  @Override
  public EbliUIProperties getUI() {
    final EbliUIProperties ebliUIProperties = super.getUI();
    ebliUIProperties.put(TrPostIsoLayerPersistence.getIsBaseId(), isBase_);
    return ebliUIProperties;
  }

}