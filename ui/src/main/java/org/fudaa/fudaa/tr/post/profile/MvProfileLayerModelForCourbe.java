/*
 * @creation 13 juin 07
 * @modification $Date: 2007-06-13 14:46:13 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import com.memoire.bu.BuTable;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModelGeometryListener;
import org.fudaa.ebli.calque.ZModeleLigneBrisee;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * @author fred deniger
 * @version $Id: MvProfileLayerModelForCourbe.java,v 1.1 2007-06-13 14:46:13 deniger Exp $
 */
public class MvProfileLayerModelForCourbe implements ZModeleLigneBrisee {

  EfLineIntersectionsResultsI res_;
  final GrBoite domaine_;

  public MvProfileLayerModelForCourbe(final EfLineIntersectionsResultsI _res, final GrBoite _domaine) {
    super();
    res_ = _res;
    domaine_ = _domaine;
  }

  @Override
  public boolean containsPolygone() {
    return false;
  }

  @Override
  public void getDomaineForGeometry(final int _idxLigne, final GrBoite _target) {
    _target.ajuste(domaine_);
  }

  public int getNbLigneBrisee() {
    return res_ == null ? 0 : 1;
  }
  
  
  
  @Override
  public int getNbPolyligne() {
    return getNbLigneBrisee();
  }

  @Override
  public int getNbPointForGeometry(final int _idxLigne) {
    return res_ == null ? 0 : res_.getNbIntersect();
  }

  @Override
  public int getNbPolygone() {
    return 0;
  }

  @Override
  public boolean isGeometryFermee(final int _idxLigne) {
    return false;
  }
  
  @Override
  public boolean isGeometryReliee(int _idxGeom) {
    return true;
  }

  @Override
  public boolean point(final GrPoint _pt, final int _ligneIdx, final int _pointIdx) {
    if (res_ == null) return false;
    _pt.x_ = res_.getIntersect(_pointIdx).getX();
    _pt.y_ = res_.getIntersect(_pointIdx).getY();
    return true;
  }

  @Override
  public GISZoneCollectionLigneBrisee getGeomData() {
    return null;
  }

  @Override
  public void prepareExport() {}

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    return null;
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {}

  @Override
  public GrBoite getDomaine() {
    return new GrBoite(domaine_);
  }

  @Override
  public int getNombre() {
    return res_ == null ? 0 : 1;
  }

  @Override
  public Object getObject(final int _ind) {
    return null;
  }

  @Override
  public boolean isValuesTableAvailable() {
    return false;
  }

  public EfLineIntersectionsResultsI getRes() {
    return res_;
  }

  public void setRes(final EfLineIntersectionsResultsI _res) {
    res_ = _res;
  }

  public void setDomaine(final GrBoite _b) {
    domaine_.setToNill();
    domaine_.ajuste(_b);
  }

  @Override
  public void addModelListener(ZModelGeometryListener listener) {}

  @Override
  public boolean isGeometryVisible(int idxGeom) {
    return false;
  }

  @Override
  public void removeModelListener(ZModelGeometryListener listener) {}

  @Override
  public void attributeAction(Object source, int indexAtt, GISAttributeInterface att, int action) {}

  @Override
  public void attributeValueChangeAction(Object source, int indexAtt, GISAttributeInterface att, int indexObj,
      Object newValue) {}

  @Override
  public void objectAction(Object source, int indexObj, Object obj, int action) {}

  @Override
  public GrPoint getVertexForObject(int ind, int idVertex) {
    return null;
  }

}
