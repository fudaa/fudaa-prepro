package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuLib;
import java.util.List;
import java.util.Set;
import org.fudaa.ctulu.ProgressionBuAdapter;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.courbe.FudaaGrapheTimeAnimatedFille;
import org.fudaa.fudaa.commun.courbe.FudaaGrapheTimeAnimatedVisuPanel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.data.TrPostDataListener;

/**
 * Cette classe delegue au model les appels de mise a jour pour les variables cr�ees.
 * 
 * @author Fred Deniger
 * @version $Id: TrPostCourbeFille.java,v 1.7 2007-03-09 08:39:04 deniger Exp $
 */
public class TrPostCourbeFille extends FudaaGrapheTimeAnimatedFille implements TrPostDataListener,
    TrPostTimeContentListener {

  public static List getCourbeFilleFrames(final BuCommonImplementation _impl) {
    return FudaaLib.getFrameWithClientProperty(_impl, "fudaa.post.courbe.temp");
  }

  public TrPostCourbeFille(final FudaaGrapheTimeAnimatedVisuPanel _g, final String _titre,
      final FudaaCommonImplementation _appli, final BuInformationsDocument _id) {
    super(_g, _titre, _appli, _id);
    init();
  }

  public TrPostCourbeFille(final EGGraphe _g, final String _titre, final FudaaCommonImplementation _appli,
      final BuInformationsDocument _id) {
    this(new FudaaGrapheTimeAnimatedVisuPanel(_g), _titre, _appli, _id);
  }

  private void init() {
    setName("ifPostCourbe");
    putClientProperty("fudaa.post.courbe.temp", Boolean.TRUE);
  }

  @Override
  public void dataAdded(final boolean _isFleche) {
    ((TrPostCourbeTreeModel) getGrapheTree()).dataAdded(_isFleche);

  }

  @Override
  public void dataChanged(final H2dVariableType _old, final H2dVariableType _new, final boolean _contentChanged,
      final boolean _isFleche, final Set _varsUsing) {
    new CtuluTaskOperationGUI(impl_, TrResource.getS("Mise � jour")) {

      @Override
      public void act() {
        ((TrPostCourbeTreeModel) getGrapheTree()).dataChanged(_old, _new, _contentChanged, _isFleche, _varsUsing,
            new ProgressionBuAdapter(this));
        BuLib.invokeLater(new Runnable() {

          @Override
          public void run() {
            getGraphe().restore();
          }
        });
      }
    }.start();

  }

  @Override
  public void dataRemoved(final H2dVariableType[] _vars, final boolean _isFleche) {
    ((TrPostCourbeTreeModel) getGrapheTree()).dataRemoved(_vars, _isFleche);
  }

  @Override
  public void updateTimeStep(final int[] _idx, final ProgressionInterface _prog) {

    final TrPostCourbeTreeModel trPostCourbeTreeModel = ((TrPostCourbeTreeModel) getGrapheTree());
    trPostCourbeTreeModel.updateTimeStep(_idx, _prog);
    getGraphe().restore();

  }

}