/**
 *  @creation     28 mai 2004
 *  @modification $Date: 2005-09-22 08:55:01 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.reflux;

import org.fudaa.dodico.calcul.CalculExecBatch;
import org.fudaa.dodico.olb.exec.OLBExec;
import org.fudaa.dodico.reflux.exec.RefluxExec;
import org.fudaa.fudaa.tr.TrChainePreferencePanel;
import org.fudaa.fudaa.tr.common.TrExecBatchPreferencesPanel;

/**
 * @author Fred Deniger
 * @version $Id: TrRefluxExecPreferencesPanel.java,v 1.8 2005-09-22 08:55:01 deniger Exp $
 */
public class TrRefluxExecPreferencesPanel extends TrExecBatchPreferencesPanel {

  /**
   * Le panneau des exe pour reflux
   */
  public TrRefluxExecPreferencesPanel() {
    super(new CalculExecBatch[] { new RefluxExec(), new OLBExec() });
  }

  @Override
  public String getCategory() {
    return TrChainePreferencePanel.getModelisationCategory();
  }

  @Override
  public String getTitle() {
    return "Reflux";
  }
}