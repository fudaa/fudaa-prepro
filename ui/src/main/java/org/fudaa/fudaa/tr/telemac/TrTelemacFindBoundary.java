/*
 *  @creation     8 d�c. 2003
 *  @modification $Date: 2007-06-28 09:28:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr.telemac;

import org.fudaa.ebli.find.EbliFindActionAbstract;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindComponent;
import org.fudaa.ebli.find.EbliFindComponentDefault;
import org.fudaa.ebli.find.EbliFindable;
import org.fudaa.ebli.find.EbliFindableItem;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrTelemacFindBoundary.java,v 1.16 2007-06-28 09:28:19 deniger Exp $
 */
public class TrTelemacFindBoundary implements EbliFindActionInterface {

  TrTelemacBcBoundaryBlockLayer layer_;

  /**
   * @param _layer le calque
   */
  public TrTelemacFindBoundary(final TrTelemacBcBoundaryBlockLayer _layer) {
    layer_ = _layer;
  }

  @Override
  public String erreur(final Object _s, final String _findId, final EbliFindable _parent) {
    final String exp = (String) _s;
    final int[] idx = EbliFindActionAbstract.getIndex(exp);
    String r = EbliFindActionAbstract.testVide(idx);
    if (r == null) {
      boolean erreur = false;
      final int nbMax = layer_.getNbFrontiereLiquide();
      for (int i = 0; i < idx.length; i++) {
        final int idxToTest = idx[i];
        if (idxToTest < 0 || idxToTest >= nbMax) {
          erreur = true;
          break;
        }
      }
      if (erreur) {
        r = TrResource.getS("Des indices sont en dehors des limites");
      }
    }
    return r;
  }

  @Override
  public boolean find(final Object _s, final String _arction, final int _selOption, final EbliFindable _parent) {
    final int[] idx = EbliFindActionAbstract.getIndex((String) _s);
    if (idx == null || idx.length == 0) { return false; }
    return layer_.setLiquidSelection(idx);
  }

  @Override
  public EbliFindableItem getCalque() {
    return layer_;
  }

  @Override
  public EbliFindComponent createFindComponent(final EbliFindable _parent) {
    return new EbliFindComponentDefault(TrResource.getS("Index(s)"));
  }

  @Override
  public boolean isEditableEnable(final String _searchId, final EbliFindable _parent) {
    return true;
  }

  @Override
  public GrBoite getZoomOnSelected() {
    return layer_.getZoomOnSelected();
  }

  @Override
  public String editSelected(final EbliFindable _parent) {
    return ((TrTelemacVisuPanel) _parent).editBoundary();
  }

  @Override
  public String toString() {
    return TrResource.getS("Fronti�res liquides");
  }
}