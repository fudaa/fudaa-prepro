package org.fudaa.fudaa.tr.post.actions;

import com.memoire.bu.BuWizardDialog;
import org.locationtech.jts.geom.LineString;
import java.awt.event.ActionEvent;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostWizardProfilSpatial;
import org.fudaa.fudaa.tr.post.profile.MvProfileBuilderFromTree;
import org.fudaa.fudaa.tr.post.profile.MvProfileCoteTester;
import org.fudaa.fudaa.tr.post.profile.MvProfileFillePanel;
import org.fudaa.fudaa.tr.post.profile.MvProfileTarget;

/**
 * Adaptation de l action mvProfileAction avec gestion des widgets
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public class TrPostProfileAction extends EbliActionSimple {
  /**
   * @author deniger
   */
  public static class ProfileAdapter implements MvProfileTarget {

    final TrPostProjet proj_;

    final TrPostSource src_;

    public ProfileAdapter(final TrPostSource _src, final TrPostProjet _proj) {
      super();
      src_ = _src;
      proj_ = _proj;
    }

    @Override
    public EfGridData getData() {
      return src_;
    }

    public TrPostSource getDataSource() {
      return src_;
    }

    @Override
    public EfGridDataInterpolator getInterpolator() {
      return src_.getInterpolator();
    }

    @Override
    public FudaaCourbeTimeListModel getTimeModel() {
      return src_.getNewTimeListModel();
    }

    @Override
    public CtuluVariable[] getVars() {
      return src_.getAvailableVar();
    }

    @Override
    public void profilPanelCreated(final MvProfileFillePanel _panel, final ProgressionInterface _prog,
        final String _title) {
      proj_.profilPanelCreated(_panel, _prog, _title);

    }

    public TrPostSource getSrc_() {
      return src_;
    }

  }

  // final MvProfileTarget src_;
  final TrPostVisuPanel panel_;

  public TrPostProfileAction(final TrPostVisuPanel _visu) {
    super(MvResource.getS("Profils spatiaux"), MvResource.MV.getIcon("profile"), "SPATIAL_PROFILE");
    panel_ = _visu;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final TrPostCommonImplementation impl = panel_.getPostImpl();
    
    
    
    final MvProfileBuilderFromTree builder = new MvProfileBuilderFromTree(createProfileAdapter(panel_), impl,
        getSelectedLine(), new MvProfileCoteTester());

    // -- construction des params --//
    builder.stepOne();

    // -- creeation du wizard --//
    final TrPostWizardProfilSpatial wizard = new TrPostWizardProfilSpatial(panel_, builder,
        new TrPostActionAddPointFromWidgetCalque.Spatial(impl.getCurrentLayoutFille().getScene(), impl));

    final BuWizardDialog DialogWizard = new BuWizardDialog(impl.getFrame(), wizard);

    // --affichage du wizard --//
    DialogWizard.setSize(600, 500);
    DialogWizard.setLocationRelativeTo(impl.getCurrentLayoutFille());
    DialogWizard.setVisible(true);

  }

  /**
   * @param _visu le panel de visu
   * @return un adapter pour la construction des profils. Le TrPostProjet construit le widget dans le layout actif.
   */
  public static final ProfileAdapter createProfileAdapter(final TrPostVisuPanel _visu) {
    return new ProfileAdapter(_visu.getSource(), _visu.getProjet());
  }

  public static final ProfileAdapter createProfileAdapter(TrPostSource src, TrPostProjet projet) {
    return new ProfileAdapter(src, projet);
  }

  protected LineString getSelectedLine() {
    return panel_.getSelectedLine();
  }
}
