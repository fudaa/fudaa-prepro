/*
 * @creation 1 f�vr. 07
 * @modification $Date: 2007-03-09 08:39:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import java.awt.event.ActionEvent;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.layer.MvVisuPanel;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * @author fred deniger
 * @version $Id: MvVolumeAction.java,v 1.3 2007-03-09 08:39:05 deniger Exp $
 */
public class MvVolumeAction extends EbliActionSimple {

  protected final EfGridData data_;
  final MvVisuPanel pn_;

  public MvVolumeAction(final EfGridData _data, final MvVisuPanel _pn) {
    super(DodicoLib.getS("Calcul de volume"), FudaaResource.FUDAA.getIcon("volume"), "VOLUME");
    data_ = _data;
    pn_ = _pn;
    setDefaultToolTip(MvResource.getS("Permet de calculer le volume sur le maillage ou sur la s�lection courante"));
    setUnableToolTip(DodicoLib.getS("La hauteur d'eau n'est pas d�finie"));
  }

  protected double[] getTimeSteps() {
    return new double[] { 0 };
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    MvProfileVolumeBuilder.computeVolume(data_, pn_, pn_.getImpl(), getTimeSteps(), getTimeModel());
  }

  protected FudaaCourbeTimeListModel getTimeModel() {
    return null;
  }

  @Override
  public void updateStateBeforeShow() {
    setEnabled(data_.isDefined(H2dVariableType.HAUTEUR_EAU));
  }

}
