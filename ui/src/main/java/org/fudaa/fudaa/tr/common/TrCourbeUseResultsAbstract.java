/*
 * @creation 15 mars 07
 * @modification $Date: 2007-05-04 14:01:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuInternalFrame;
import gnu.trove.TIntHashSet;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.tr.data.TrVisuPanel;

/**
 * @author fred deniger
 * @version $Id: TrCourbeUseResultsAbstract.java,v 1.4 2007-05-04 14:01:46 deniger Exp $
 */
public abstract class TrCourbeUseResultsAbstract extends AbstractTableModel implements TrCourbeUseResultsI {

  protected final TrVisuPanel visu_;
  protected EvolutionReguliereInterface[] evols_;


  public TrCourbeUseResultsAbstract(final TrVisuPanel _visu) {
    super();
    visu_ = _visu;
  }

  @Override
  public final JTable getTableOfUse(final EvolutionReguliereInterface[] _selected) {
    final TIntHashSet idxInTable = new TIntHashSet(getRowCount());
    // on recherche les courbes sélectionnées
	// we look for the selected plots
    if (_selected != null) {
      for (int i = 0; i < _selected.length; i++) {
        final int idx = CtuluLibArray.findObjectEgalEgal(evols_, _selected[i]);
        if (idx >= 0) {
          idxInTable.add(i);
        }
      }
    }
    final CtuluTable table = new CtuluTable(this);
    // on met a jour les lignes sélectionnées.
	// We update the selected lines
    final int[] idx = idxInTable.toArray();
    for (int i = 0; i < idx.length; i++) {
      table.getSelectionModel().addSelectionInterval(idx[i], idx[i]);
    }

    return table;
  }

  @Override
  public String isUsedBy(final EvolutionReguliereInterface _eve) {
    return null;
  }

  @Override
  public Class getColumnClass(final int _columnIndex) {
    return String.class;
  }

  @Override
  public int getRowCount() {
    return evols_ == null ? 0 : evols_.length;
  }

  @Override
  public int getColumnCount() {
    return 4;
  }

  protected EvolutionReguliereInterface getEvol(final int _idx) {
    return evols_[_idx];
  }

  public final String getUsedCount(final int _rowIndex) {
    return CtuluLibString.getString(getCounter(_rowIndex).getUsedCount(getEvol(_rowIndex)));
  }

  public String getDomain(final int _rowIndex) {
    return getCounter(_rowIndex).getDomain();
  }

  public final String getVariables(final int _rowIndex) {
    final List var = new ArrayList(getCounter(_rowIndex).getVariablesUsing(getEvol(_rowIndex)));
    if (var.size() == 0) { return CtuluLibString.EMPTY_STRING; }
    if (var.size() == 1) { return var.get(0).toString(); }
    return CtuluLibString.arrayToHtmlString(var.toArray());

  }

  @Override
  public final Object getValueAt(final int _rowIndex, final int _columnIndex) {
    if (_columnIndex == 0) { return getEvol(_rowIndex); }
    if (_columnIndex == 1) { return getUsedCount(_rowIndex); }
    if (_columnIndex == 2) { return getDomain(_rowIndex); }
    if (_columnIndex == 3) { return getVariables(_rowIndex); }
    return CtuluLibString.EMPTY_STRING;
  }

  public abstract TrCourbeUseCounter getCounter(int _rowIndex);

  @Override
  public String getColumnName(final int _column) {
    if (_column == 0) { return TrLib.getString("Courbe"); }
    if (_column == 1) { return TrLib.getString("Fréquence d'utilisation"); }
    if (_column == 2) { return TrLib.getString("Domaine d'utilisation"); }
    if (_column == 3) { return TrLib.getString("Variable(s) affectée(s)"); }
    return CtuluLibString.ESPACE;
  }

  protected void activateVisuFrame() {
    visu_.getImpl().activateInternalFrame(
        (BuInternalFrame) SwingUtilities.getAncestorOfClass(BuInternalFrame.class, visu_));
  }

  protected void selectedInMeshLayer(final int[] _idx) {
    if (_idx != null) {
      final MvElementLayer layer = visu_.getGridGroup().getPolygonLayer();
      visu_.getArbreCalqueModel().setSelectionCalque(layer);
      layer.setSelection(_idx);
      visu_.zoomOnSelected();
      activateVisuFrame();
    }
  }

}
