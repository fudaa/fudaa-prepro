/*
 *  @creation     3 mai 2005
 *  @modification $Date: 2007-03-02 13:01:33 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.*;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;

/**
 * @author Fred Deniger
 * @version $Id: TrPostFilterDefaultValuePanel.java,v 1.7 2007-03-02 13:01:33 deniger Exp $
 */
public class TrPostFilterDefaultValuePanel {
  BuCheckBox cbAlwaysStrict_;
  BuCheckBox cbAlwaysActivated_;
  BuCheckBox cbAlwaysActivatedOnDeltaH_;
  TrPostFilterHauteurPanel hauteur_;
  TrPostFilterVarPanel var_;

  /**
   * @param _src la source pour les variables
   */
  public TrPostFilterDefaultValuePanel(final TrPostSource _src) {
    cbAlwaysStrict_ = new BuCheckBox(DodicoLib.getS("Filtre strict"));
    cbAlwaysStrict_.setToolTipText(TrResource
        .getS("Si activ�, un �l�ment n'est dessin� que si tous ses sommets sont accept�s par les filtres"));
    cbAlwaysStrict_.setSelected(TrPostFilterLayer.getAlwaysStrictFromPref());
    hauteur_ = new TrPostFilterHauteurPanel(_src);
    hauteur_.updateFor(new TrPostFilterHauteur());
    var_ = new TrPostFilterVarPanel(_src);
    var_.updateFor(TrPostFilterLayer.getH2dVarFromPref(_src.getShortNameCreateVar()));
    cbAlwaysActivated_ = new BuCheckBox();
    cbAlwaysActivated_.setToolTipText(FudaaLib
        .getS("Si coch�, ce filtre sera toujours activ� � l'ouverture d'un projet"));
    cbAlwaysActivated_.setSelected(TrPostFilterHauteur.isActivatedFromPref());
  }

  public JPanel buildPanel() {
    final JPanel r = new JPanel();
    r.setLayout(new BuVerticalLayout());
    JPanel r1;
    if (cbAlwaysStrict_ != null) {
      r1 = new BuPanel();
      r1.setLayout(new BuBorderLayout());
      r1.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Filtre strict")));
      r1.add(cbAlwaysStrict_, BuBorderLayout.CENTER);
      r.add(r1);
    }
    r1 = hauteur_.buildPanel();
    r1.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Hauteur d'eau minimale")));
    r1.add(new BuLabel(FudaaLib.getS("Filtre activ� par defaut")));
    r1.add(cbAlwaysActivated_);
    r1.add(new BuLabel(FudaaLib.getS("Filtre activ� sur delta Hauteur d'eau par defaut")));
    r1.add(cbAlwaysActivatedOnDeltaH_);
    r.add(r1);
    r1 = var_.buildPanel();
    r1.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Variables non affect�es")));
    r.add(r1);

    return r;
  }

  /**
   * Applique les valeurs par defaut.
   */
  public void apply() {
    if (isDataValid()) {
      TrPostFilterHauteur.setEpsInPref(hauteur_.getCondition().getEps());
      TrPostFilterHauteur.setActivatedInPref(cbAlwaysActivated_.isSelected());
      if (cbAlwaysStrict_ != null) {
        TrPostFilterLayer.setAlwaysStrictInPref(cbAlwaysStrict_.isSelected());
      }
      TrPostFilterLayer.setVarInPref(var_.getAvoidVarForPref());
    }
  }

  /**
   * @param _b le nouvel etat d'activation
   */
  public void setEnable(final boolean _b) {
  }

  /**
   * @return true si valid
   */
  public boolean isDataValid() {
    return hauteur_.isDataValid();
  }
}
