/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.dialogSpec;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluComboBoxModelAdapter;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.graphe.EbliWidgetCreatorGraphe;
import org.fudaa.ebli.visuallibrary.graphe.GrapheCellRenderer;
import org.fudaa.fudaa.tr.common.TrLib;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Frederic Deniger
 */
public abstract class AbstractTrPostDialogAddToScene<T extends AbstractTrPostDialogAddToSceneDialogContent> {
  protected final T from;
  protected JCheckBox cbResult;
  protected JTextField tfResult;
  protected JTextField tfPositive;
  protected JTextField tfNegative;
  protected JCheckBox cbPositive;
  protected JCheckBox cbNegative;
  protected JComboBox targetGraph;
  protected JComboBox startTime;
  protected JComboBox endTime;
  protected  JButton buttonAddCurves;

  public AbstractTrPostDialogAddToScene(T from) {
    this.from = from;
  }

  public EbliScene getScene() {
    return from.getScene();
  }

  JPanel panel;

  private void updateStates() {
    tfResult.setEnabled(cbResult.isSelected());
    tfPositive.setEnabled(cbPositive.isSelected());
    tfNegative.setEnabled(cbNegative.isSelected());
  }

  protected abstract void updateNames();

  public JPanel createPanel() {
    if (panel != null) {
      return panel;
    }
    from.getTxtSeuil().getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void insertUpdate(DocumentEvent e) {
        updateNames();
      }

      @Override
      public void removeUpdate(DocumentEvent e) {
        updateNames();
      }

      @Override
      public void changedUpdate(DocumentEvent e) {
        updateNames();
      }
    });
    from.getModelVariables().addListDataListener(new ListDataListener() {
      @Override
      public void intervalAdded(ListDataEvent e) {
      }

      @Override
      public void intervalRemoved(ListDataEvent e) {
      }

      @Override
      public void contentsChanged(ListDataEvent e) {
        if (e.getIndex0() == e.getIndex1() && e.getIndex0() == -1) {
          updateNames();
        }
      }
    });
    panel = new JPanel(new BuGridLayout(2));
    cbResult = new JCheckBox(TrLib.getString("Cr�er courbe r�sultat nomm�e") + ": ");
    cbPositive = new JCheckBox(TrLib.getString("Cr�er courbe partie positive nomm�e") + ": ");
    cbNegative = new JCheckBox(TrLib.getString("Cr�er courbe partie n�gative nomm�e") + ": ");
    tfResult = new BuTextField(15);
    tfPositive = new BuTextField(15);
    tfNegative = new BuTextField(15);
    panel.add(cbResult);
    panel.add(tfResult);
    panel.add(cbNegative);
    panel.add(tfNegative);
    panel.add(cbPositive);
    panel.add(tfPositive);

    startTime = new JComboBox();
    startTime.setModel(new CtuluComboBoxModelAdapter(from.getSource().getNewTimeListModel()));
    startTime.setSelectedIndex(0);
    endTime = new JComboBox();
    endTime.setModel(new CtuluComboBoxModelAdapter(from.getSource().getNewTimeListModel()));
    endTime.setSelectedIndex(Math.max(0, endTime.getModel().getSize() - 1));
    panel.add(new JLabel(TrLib.getString("Pas de temps d�part de la courbe")));
    panel.add(startTime);
    panel.add(new JLabel(TrLib.getString("Pas de temps fin")));
    panel.add(endTime);

    targetGraph = new JComboBox();
    targetGraph.setRenderer(new GrapheCellRenderer());
    panel.add(new JLabel(TrLib.getString("Graphe cible")));
    panel.add(targetGraph);
    panel.add(new JLabel());
    buttonAddCurves = new JButton(TrLib.getString("Ajouter les courbes"));
    panel.add(buttonAddCurves);
    buttonAddCurves.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        addCurves();
      }
    });
    cbResult.setSelected(true);
    updateStates();
    ActionListener tfUpdater = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateStates();
      }
    };
    cbNegative.addActionListener(tfUpdater);
    cbPositive.addActionListener(tfUpdater);
    cbResult.addActionListener(tfUpdater);
    updateNames();
    updateStates();
    updateTarget();
    return panel;
  }

  public void updateTarget() {
    final Map params = new HashMap();
    CtuluLibImage.setCompatibleImageAsked(params);
    TrPostTimeWidgetFinder finder = new TrPostTimeWidgetFinder(getScene());
    List<EbliNode> findGraphTimeNodes = finder.findGraphTimeNodes();
    JLabel labelNew = new JLabel(TrLib.getString("Nouveau graphe"));
    List<JLabel> modelItems = new ArrayList<JLabel>();
    modelItems.add(labelNew);
    for (EbliNode node : findGraphTimeNodes) {
      final JLabel label = new JLabel();
      EbliWidgetCreatorGraphe graph = (EbliWidgetCreatorGraphe) node.getCreator();
      final BufferedImage image = graph.getGraphe().produceImage(80, 50, params);
      final Icon icone = new ImageIcon(image);
      label.setIcon(icone);
      label.putClientProperty("GRAPH", graph.getGraphe());
      label.setText(node.getTitle());
      modelItems.add(label);
    }
    targetGraph.setModel(new DefaultComboBoxModel((JLabel[]) modelItems.toArray(new JLabel[modelItems.size()])));
  }

  protected abstract void addCurves(ProgressionInterface prog);

  protected void addCurves() {
    buttonAddCurves.setEnabled(false);
    buttonAddCurves.setText(TrLib.getString("Cr�ation du graphe"));
    final CtuluTaskDelegate task = from.getVisuPanel().getCtuluUI().createTask(TrLib.getString("Graphe"));

    task.start(new Runnable() {
      @Override
      public void run() {
        final ProgressionInterface prog = task.getStateReceiver();
        addCurves(prog);
      }
    });
  }

  public JComboBox getTargetGraph() {
    return targetGraph;
  }

  public void setTargetGraph(JComboBox targetGraph) {
    this.targetGraph = targetGraph;
  }

  public JComboBox getStartTime() {
    return startTime;
  }

  public void setStartTime(JComboBox startTime) {
    this.startTime = startTime;
  }

  public JComboBox getEndTime() {
    return endTime;
  }

  public void setEndTime(JComboBox endTime) {
    this.endTime = endTime;
  }

  public T getFrom() {
    return from;
  }

  public JCheckBox getCbResult() {
    return cbResult;
  }

  public void setCbResult(JCheckBox cbResult) {
    this.cbResult = cbResult;
  }

  public JCheckBox getCbPositive() {
    return cbPositive;
  }

  public void setCbPositive(JCheckBox cbPositive) {
    this.cbPositive = cbPositive;
  }

  public JCheckBox getCbNegative() {
    return cbNegative;
  }

  public void setCbNegative(JCheckBox cbNegative) {
    this.cbNegative = cbNegative;
  }
}
