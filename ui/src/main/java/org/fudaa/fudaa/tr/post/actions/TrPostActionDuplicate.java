package org.fudaa.fudaa.tr.post.actions;

import com.memoire.bu.BuDialogChoice;
import java.util.Iterator;
import java.util.List;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionDuplicate;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostLayoutFille;
import org.fudaa.fudaa.tr.post.TrPostProjet;

/**
 * Refonte de l action duplicate qui g�re le multi-layout. Chooser qui demande la fille layout puis autre chooser qui
 * demande l'onglet. Si la fille est unique pas de chooser idem pour l onglet de la fille selectionnee.
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public class TrPostActionDuplicate extends EbliWidgetActionDuplicate {

  TrPostProjet projet_;

  public TrPostActionDuplicate(final EbliScene _scene, final TrPostProjet _projet) {
    super(_scene);

    projet_ = _projet;
  }

  /**
   * surcharge de la methode. Envoie de la liste des frame layout + interne layout si il y e na plusieurs
   */
  @Override
  protected EbliScene selectionneSceneNode() {

    TrPostLayoutFille fenetre = null;
    final TrPostCommonImplementation impl = projet_.getImpl();
    final List<TrPostLayoutFille> ls = impl.getAllLayoutFille();

    if (ls.size() == 1) {
      fenetre = ls.get(0);
    } else {

      // recuperation de toutes les noms des frames post du projet --//
      final String[] listeFillesLayout = formatteFilles(ls);

      final BuDialogChoice chooser = new BuDialogChoice(impl.getApp(), impl.getInformationsSoftware(), TrResource
          .getS("S�lectionnez"), TrResource.getS("S�lectionnez la fen�tre dans laquelle dupliquer l'objet "),
          listeFillesLayout);

      final int response = chooser.activate();

      if (response == 0) {
        final int indiceFilleelect = chooser.getSelectedIndex();
        fenetre = ls.get(indiceFilleelect);
      }

    }
    // -- on recherche si dans la fille selectionne il existe plusieurs sous
    // layout --//
    if(fenetre==null){
      return null;
    }
    return fenetre.controller_.getSceneCourante();
  }

  public static String[] formatteFilles(final List<TrPostLayoutFille> ls) {
    final String[] noms = new String[ls.size()];
    int cpt = 0;
    for (final Iterator<TrPostLayoutFille> it = ls.iterator(); it.hasNext();) {
      noms[cpt++] = it.next().getTitle();
    }
    return noms;
  }

}
