/*
 * @creation 15 mars 07
 * @modification $Date: 2007-05-04 14:01:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.fu.FuComparator;
import java.util.Arrays;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.H2dParameters;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.fudaa.tr.data.TrVisuPanel;
import org.fudaa.fudaa.tr.rubar.TrRubarBcAreteLayer;
import org.fudaa.fudaa.tr.rubar.TrRubarVisuPanel;

/**
 * @author fred deniger
 * @version $Id: TrRubarCourbeTarageUseFinder.java,v 1.4 2007-05-04 14:01:46 deniger Exp $
 */
public class TrRubarCourbeTarageUseFinder extends TrCourbeUseFinderAbstract {

  public TrRubarCourbeTarageUseFinder(final H2dParameters _params, final TrVisuPanel _panel, final EGGrapheModel _graphe) {
    super(_params, _panel, _graphe);
  }

  @Override
  protected TrCourbeUseResultsI createResult() {
    return new RubarTarageResults(getPanel());
  }

  @Override
  protected void go(final TrCourbeUseResultsI _res, final ProgressionInterface _prog) {
    ((H2dRubarParameters) getParams()).getBcMng().iterateOnTarageCurves(((RubarTarageResults) _res).count_);
  }

  private static class RubarTarageResults extends TrCourbeUseResultsAbstract {
    TrCourbeUseCounter count_ = new TrCourbeUseCounter(H2dResource.getS("Loi de tarage"));

    public RubarTarageResults(final TrVisuPanel _visu) {
      super(_visu);
    }

    @Override
    public TrCourbeUseCounter getCounter(final int _rowIndex) {
      return count_;
    }

    @Override
    public void selectInView(final EvolutionReguliereInterface _eve) {
      final int[] select = count_.getIdxSelected(_eve);
      if (select != null) {
        final TrRubarBcAreteLayer layer = ((TrRubarVisuPanel) visu_).getBcAreteLayer();
        visu_.getArbreCalqueModel().setSelectionCalque(layer);
        layer.setSelection(select);
        visu_.zoomOnSelected();
        activateVisuFrame();
      }
    }

    @Override
    public void setFilter(final EvolutionReguliereInterface _evol) {
      count_.setEvolToTest(_evol);
    }

    @Override
    public void validSearch() {
      evols_ = (EvolutionReguliereInterface[]) count_.getEvols().toArray(
          new EvolutionReguliereInterface[count_.getNbEvol()]);
      Arrays.sort(evols_, FuComparator.STRING_COMPARATOR);

    }

  }

}
