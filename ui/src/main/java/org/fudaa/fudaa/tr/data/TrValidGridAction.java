package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuActionModifier;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluAnalyzeGUI;
import org.fudaa.dodico.ef.operation.translate.EfStructureAllGridValidator;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

import java.awt.event.ActionEvent;

public class TrValidGridAction extends EbliActionSimple {
  private TrVisuPanelEditor visuPanelEditor;

  public TrValidGridAction(TrVisuPanelEditor visuPanelEditor) {
    super(TrResource.getS("Validation du maillage"), null, "VALID_GRID");
    setDefaultToolTip(TrLib.getString("Bilan des modifications impactant la structure du maillage"));
    this.visuPanelEditor = visuPanelEditor;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final CtuluTaskDelegate task = visuPanelEditor.getImpl().createTask(getTitle());
    final ProgressionInterface prog = task.getStateReceiver();
    task.start(new Runnable() {
      @Override
      public void run() {
        EfStructureAllGridValidator validator = new EfStructureAllGridValidator(visuPanelEditor.getGrid());
        final CtuluLogGroup ctuluLogGroup = validator.isValid(prog);
        if (ctuluLogGroup.containsSomething()) {
          CtuluAnalyzeGUI.showDialogErrorFiltre(ctuluLogGroup, visuPanelEditor.getCtuluUI(), getTitle(), false);
          setGridInvalid(visuPanelEditor, true);
        } else {
          setGridInvalid(visuPanelEditor, false);
          visuPanelEditor.getCtuluUI().message(getTitle(), TrLib.getString("La structure du maillage est valide. Le projet peut �tre sauvegard�."), false);
        }
      }
    });
  }

  public static void setGridInvalid(TrVisuPanelEditor visuPanelEditor, boolean invalid) {
    visuPanelEditor.getParams().setGridInvalid(invalid);
    visuPanelEditor.getImpl().setEnabledForAction(FudaaGuiLib.getSaveCopy(), !invalid);
    visuPanelEditor.getImpl().setEnabledForAction(FudaaGuiLib.getArchiver(), !invalid);
    visuPanelEditor.getImpl().setEnabledForAction("ENREGISTRERSOUS", !invalid);
    final String saveCommand = "ENREGISTRER";
    String text;
    if (invalid) {
      text = "<html><body>"
          + TrLib.getString("Le projet courant courant contient un maillage avec une structure modifi�e et ne peut pas �tre sauvegard�.")
          + "<br>" + TrLib.getString("Les donn�es seront uniquement export�es.")
          + "</body></html>";
      visuPanelEditor.getImpl().setDynamicTextForAction(saveCommand, TrLib.getString("Exporter"));
    } else {
      text = TrLib.getString("Enregistrer");
      visuPanelEditor.getImpl().setDynamicTextForAction(saveCommand, text);
    }
    BuActionModifier.setTooltipForAction(visuPanelEditor.getImpl().getMainMenuBar(), saveCommand, text);
    BuActionModifier.setTooltipForAction(visuPanelEditor.getImpl().getMainMenuBar(), saveCommand, text);
  }

  @Override
  public void updateStateBeforeShow() {
    this.setEnabled(true);
  }
}
