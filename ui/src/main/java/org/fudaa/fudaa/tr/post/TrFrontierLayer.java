/*
 *  @creation     3 oct. 2005
 *  @modification $Date: 2007-04-16 16:35:32 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;
import java.io.IOException;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JComponent;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.controle.BSelecteurListTarget;
import org.fudaa.ebli.controle.BSelecteurListTimeTarget;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.layer.MvFrontierPolygonLayer;
import org.fudaa.fudaa.meshviewer.model.MvFrontierModelDefault;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrFrontierLayer.java,v 1.11 2007-04-16 16:35:32 deniger Exp $
 */
public class TrFrontierLayer extends MvFrontierPolygonLayer implements BSelecteurListTarget, BSelecteurListTimeTarget {

  public TrFrontierLayer(final TrPostSource _src, final TrPostInfoDelegate _infos) {
    super(new TrFrontiereModel(_src, _infos));
    setName("cqFrontiers");
    setDestructible(false);
  }

  private TrFrontiereModel getTrModel() {
    return (TrFrontiereModel) m_;
  }

  @Override
  public ListModel getListModel() {
    return getTrModel().getListModel();
  }

  @Override
  public ListSelectionModel getListSelectionModel() {
    return getTrModel().getListSelectionModel();
  }

  public JComponent getTargetComponent() {
    return this;
  }

  @Override
  public ListModel getTimeListModel() {
    return getTrModel().getTimeListModel();
  }

  @Override
  public ListSelectionModel getTimeListSelectionModel() {
    return getTrModel().getTimeListSelectionModel();
  }

  public JComponent getTimeTargetComponent() {
    return this;
  }

  private static class TrFrontiereModel extends MvFrontierModelDefault {

    final TrPostSource src_;
    final ListModel timeModel_;
    final ListModel varModel_;
    final ListSelectionModel timeSelectionModel_;
    final ListSelectionModel varSelectionModel_;

    private class PostBcTable extends AbstractTableModel {

      final int[] tmp_ = new int[2];
      final EfData data_;
      final String var_;

      /**
       * @param _data
       */
      public PostBcTable(final EfData _data, final String _varName) {
        super();
        data_ = _data;
        var_ = _varName;
      }

      @Override
      public int getColumnCount() {
        return 6;
      }

      @Override
      public String getColumnName(final int _column) {
        if (_column == 0) { return MvResource.getS("Fronti�re"); }
        if (_column == 1) { return MvResource.getS("Indice sur la fronti�re"); }
        if (_column == 2) { return MvResource.getS("Indice global"); }
        if (_column == 3) { return "X"; }
        if (_column == 4) { return "Y"; }
        return var_;
      }

      @Override
      public int getRowCount() {
        return TrFrontiereModel.this.getNbTotalPoint();
      }

      @Override
      public Object getValueAt(final int _rowIndex, final int _columnIndex) {
        if (_columnIndex == 5) { return CtuluLib.getDouble(data_.getValue(TrFrontiereModel.this
            .getGlobalIdxFromFr(_rowIndex))); }
        TrFrontiereModel.this.getIdxFrIdxOnFrFromFrontier(_rowIndex, tmp_);
        if (_columnIndex == 0) { return new Integer(tmp_[0] + 1); }
        if (_columnIndex == 1) { return new Integer(tmp_[1] + 1); }
        if (_columnIndex == 2) { return new Integer(TrFrontiereModel.this.getGlobalIdx(tmp_[0], tmp_[1]) + 1); }
        if (_columnIndex == 3) { return CtuluLib.getDouble(TrFrontiereModel.this.getX(tmp_[0], tmp_[1])); }
        return CtuluLib.getDouble(TrFrontiereModel.this.getY(tmp_[0], tmp_[1]));
      }
    }

    /**
     * @param _src la source
     */
    public TrFrontiereModel(final TrPostSource _src, final TrPostInfoDelegate _delegate) {
      super(_src.getGrid());
      src_ = _src;
      setDelegate(_delegate);
      timeModel_ = _src.getTimeListModel();
      timeSelectionModel_ = new DefaultListSelectionModel();
      varSelectionModel_ = new DefaultListSelectionModel();
      timeSelectionModel_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      varSelectionModel_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      varModel_ = _src.getVarListModel();
    }

    @Override
    public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
      if (timeSelectionModel_.isSelectionEmpty() || varSelectionModel_.isSelectionEmpty()) { return super
          .createValuesTable(_layer); }
      final H2dVariableType var = (H2dVariableType) varModel_.getElementAt(varSelectionModel_.getMaxSelectionIndex());
      final String s = TrResource.getS("<u>Variable</u>: {0}<br><u>Pas de temps</u>: {1}", var.getName(),
          (String) timeModel_.getElementAt(timeSelectionModel_.getMaxSelectionIndex()));
      final BuTable table = new CtuluTable(new PostBcTable(src_
          .getData(var, timeSelectionModel_.getMaxSelectionIndex()), var.getName()));
      EbliTableInfoPanel.setTitle(table, "<html>" + s + "</html>");
      return table;
    }

    @Override
    public void fillWithInfo(final InfoData _m, final ZCalqueAffichageDonneesInterface _s) {
      super.fillWithInfo(_m, _s);
      if (!_s.isSelectionEmpty() && !timeSelectionModel_.isSelectionEmpty() && !varSelectionModel_.isSelectionEmpty()) {
        final EbliListeSelectionMultiInterface selection = _s.getLayerSelectionMulti();
        final int frSelected = selection.isSelectionInOneBloc();
        if (frSelected >= 0) {
          final CtuluListSelectionInterface frSelection = selection.getSelection(frSelected);
          if (frSelection.isOnlyOnIndexSelected()) {
            final int idxPtOnFr = frSelection.getMaxIndex();
            final int idxPt = super.getGlobalIdx(frSelected, idxPtOnFr);
            final H2dVariableType var = (H2dVariableType) varModel_.getElementAt(varSelectionModel_
                .getMaxSelectionIndex());

            try {
              ((TrPostInfoDelegate) delegate_).fillInfoWithVar(idxPt, var, timeSelectionModel_.getMaxSelectionIndex(),
                  _m, src_.getData(var, timeSelectionModel_.getMaxSelectionIndex(), idxPt));
            } catch (final IOException _e) {
              FuLog.warning(_e);
            }

          }
        }

      }
    }

    public ListModel getListModel() {
      return varModel_;
    }

    public ListSelectionModel getListSelectionModel() {
      return varSelectionModel_;
    }

    public ListModel getTimeListModel() {
      return timeModel_;
    }

    public ListSelectionModel getTimeListSelectionModel() {
      return timeSelectionModel_;
    }

  }

}
