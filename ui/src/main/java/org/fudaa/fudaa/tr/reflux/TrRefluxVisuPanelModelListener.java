/*
 * @creation 30 avr. 07
 * @modification $Date: 2007-06-14 12:01:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.reflux;

import org.fudaa.dodico.h2d.H2dBcFrontierInterface;
import org.fudaa.dodico.h2d.H2dBoundary;
import org.fudaa.dodico.h2d.H2dSIListener;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBcListener;
import org.fudaa.dodico.h2d.reflux.H2dRefluxElementPropertyMngAbstract;
import org.fudaa.dodico.h2d.reflux.H2dRefluxElementPropertyMngListener;
import org.fudaa.dodico.h2d.reflux.H2dRefluxNodalPropertiesListener;
import org.fudaa.dodico.h2d.reflux.H2dRefluxParameters;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author fred deniger
 * @version $Id: TrRefluxVisuPanelModelListener.java,v 1.2 2007-06-14 12:01:39 deniger Exp $
 */
public class TrRefluxVisuPanelModelListener implements H2dRefluxNodalPropertiesListener, H2dSIListener,
    H2dRefluxBcListener, H2dRefluxElementPropertyMngListener {

  @Override
  public void elementPropertyChanged(final H2dRefluxElementPropertyMngAbstract _source, final H2dVariableType _v) {
    pn_.updateInfoAndIso();

  }

  final TrRefluxVisuPanel pn_;

  public TrRefluxVisuPanelModelListener(final TrRefluxVisuPanel _pn) {
    super();
    pn_ = _pn;
    final H2dRefluxParameters params = _pn.proj_.getH2dRefluxParametres();
    params.getPropNodal().addListener(this);
    params.getSolutionInitiales().addListener(this);
    params.getRefluxClManager().addClListener(this);
    params.getElementPropMng().addListener(this);

  }

  @Override
  public void bcBoundaryTypeChanged(final H2dBoundary _b, final H2dBoundaryType _old) {
    pn_.updateInfoAndIso();
  }

  @Override
  public void bcFrontierStructureChanged(final H2dBcFrontierInterface _b) {
    pn_.updateInfoAndIso();
  }

  @Override
  public void bcParametersChanged(final H2dBoundary _b, final H2dVariableType _t) {}

  @Override
  public void bcPointsNormalChanged() {
    pn_.updateInfoAndIso();
  }

  @Override
  public void bcPointsParametersChanged(final H2dVariableType _t) {}

  @Override
  public void nodalPropertyChanged(final H2dVariableType _t, final boolean _addedOrRemove) {
    pn_.updateInfoAndIso();
  }

  @Override
  public void siAdded(final H2dVariableType _var) {}

  @Override
  public void siChanged(final H2dVariableType _var) {
    pn_.updateInfoAndIso();
  }

  @Override
  public void siRemoved(final H2dVariableType _var) {}

}
