/**
 *  @creation     24 mai 2004
 *  @modification $Date: 2006-12-05 10:18:20 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr;

/**
 * @author Fred Deniger
 * @version $Id: TrSupervisor.java,v 1.12 2006-12-05 10:18:20 deniger Exp $
 */
public final class TrSupervisor {

  private TrSupervisor() {}

  /**
   * @param args les arguments de l'appli
   */
  public static void main(final String[] _args) {
    TrInvocationHandler.initThreaExceptionHandler();
    TrLauncherDefault.init();
    TrLauncherDefault.launch(_args, null, new TrSupervisorImplementation());

  }
}