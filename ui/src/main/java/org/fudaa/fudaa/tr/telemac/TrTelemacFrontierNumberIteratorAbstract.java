/**
 * @creation 24 f�vr. 2005
 * @modification $Date: 2007-05-22 14:20:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuTableCellRenderer;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.iterator.NumberIntegerIterator;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import java.awt.*;
import java.text.Format;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacFrontierNumberIteratorAbstract.java,v 1.11 2007-05-22 14:20:38 deniger Exp $
 */
public abstract class TrTelemacFrontierNumberIteratorAbstract extends NumberIntegerIterator implements
    CtuluNumberFormatI, CtuluValueEditorI {
  private String separator;
  private final TableCellRenderer renderer_ = new BuTableCellRenderer();

  @Override
  public TableCellRenderer createTableRenderer() {
    return renderer_;
  }

  @Override
  public String getSeparator() {
    return separator;
  }

  /**
   * @author Fred Deniger
   * @version $Id: TrTelemacFrontierNumberIteratorAbstract.java,v 1.11 2007-05-22 14:20:38 deniger Exp $
   */
  final class ComboEditor extends BasicComboBoxEditor {
    ComboEditor() {
      super();
      editor = BuTextField.createIntegerField();
      ((BuTextField) editor).setValueValidator(new BuValueValidator() {
        @Override
        public boolean isValueValid(final Object _value) {
          if (_value == null) {
            return false;
          }
          return isIndiceValid(((Integer) _value).intValue());
        }
      });
    }
  }

  class ComboModel extends AbstractListModel implements ComboBoxModel {
    Object selected_;

    @Override
    public Object getElementAt(final int _index) {
      return formatIntValue(_index);
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public int getSize() {
      return getNbPt();
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != selected_) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }
  }

  private double[] x_;

  protected final void setX(final double[] _x) {
    x_ = _x;
  }

  public TrTelemacFrontierNumberIteratorAbstract(final double[] _x) {
    super();
    x_ = _x;
  }

  public abstract int getNbPt();

  @Override
  public final double currentValue() {
    return x_[(int) super.currentValue()];
  }

  @Override
  public final JComponent createEditorComponent() {
    final BuComboBox cb = new BuComboBox();
    cb.setModel(new ComboModel());
    cb.setEditable(true);
    cb.setEditor(new ComboEditor());
    return cb;
  }

  @Override
  public final TableCellEditor createTableEditorComponent() {
    return null;
  }

  @Override
  public final String currentLabel() {
    super.currentLabel();
    return formatValue(currentValue());
  }

  @Override
  public final String format(final double _d) {
    return formatValue(_d);
  }

  @Override
  public final String formatSubValue(final double _v) {
    return formatValue(_v);
  }

  final int getInt(final double _value) {
    int res = Arrays.binarySearch(x_, _value);
    if (res < 0) {
      res = -res - 2;
    }
    return res;
  }

  @Override
  public final JComponent createCommonEditorComponent() {
    return createEditorComponent();
  }

  public abstract String formatIntValue(int _v);

  @Override
  public final String formatValue(final double _v) {
    return formatIntValue(getInt(_v));
  }

  abstract boolean isIndiceValid(int _idx);

  @Override
  public final String getStringValue(final Component _comp) {
    final int item = ((BuComboBox) _comp).getSelectedIndex();
    if (item >= 0) {
      try {
        return Double.toString(x_[item]);
      } catch (final NumberFormatException e) {
      }
    }
    return CtuluLibString.EMPTY_STRING;
  }

  public String getUnit() {
    return null;
  }

  @Override
  public final void init(final double _minimum, final double _maximum, final int _nbTick) {
    super.init(getInt(_minimum), getInt(_maximum), _nbTick);
  }

  @Override
  public final Object getValue(final Component _comp) {
    final int item = ((BuComboBox) _comp).getSelectedIndex();
    if (item >= 0) {
      try {
        return CtuluLib.getDouble(x_[item]);
      } catch (final NumberFormatException e) {
      }
    }

    return CtuluLib.ZERO;
  }

  @Override
  public final boolean isEmpty(final Component _c) {
    return ((BuComboBox) _c).getSelectedItem() == null;
  }

  @Override
  public final boolean isValid(final Object _o) {
    if (_o == null) {
      return false;
    }
    try {
      return isIndiceValid(Integer.parseInt(_o.toString()));
    } catch (final NumberFormatException e) {
    }
    return false;
  }

  @Override
  public Object parseString(String in) {
    try {
      return Integer.parseInt(in);
    } catch (final NumberFormatException e) {
    }
    return null;
  }

  @Override
  public final boolean isValueValidFromComponent(final Component _comp) {
    return isValid(((BuComboBox) _comp).getSelectedItem());
  }

  @Override
  public final void next() {
    super.nextMajor();
  }

  @Override
  public Class getDataClass() {
    return Double.class;
  }

  public final void setValue(final double _s, final Component _comp) {
    ((BuComboBox) _comp).setSelectedItem(format(_s));
  }

  @Override
  public final void setValue(final Object _s, final Component _comp) {
    if (_s instanceof String) {
      ((BuComboBox) _comp).setSelectedItem(_s);
    }
  }

  @Override
  public CtuluNumberFormatI getCopy() {
    return this;
  }

  public Format getFormat() {
    return null;
  }

  @Override
  public boolean isDecimal() {
    return false;
  }

  @Override
  public String toLocalizedPattern() {
    return null;
  }

  @Override
  public final String toString(final Object _o) {
    return _o == null ? CtuluLibString.EMPTY_STRING : _o.toString();
  }
}
