/*
 * @creation 24 avr. 07
 * @modification $Date: 2007-05-04 14:01:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ebli.calque.BCalqueConfigureSectionAbstract;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.controle.BSelecteurColorChooserBt;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurLineModel;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.tr.common.TrLib;

/**
 * @author fred deniger
 * @version $Id: TrIsoLayerConfigure.java,v 1.3 2007-05-04 14:01:51 deniger Exp $
 */
public class TrIsoLayerConfigure extends BCalqueConfigureSectionAbstract {

  public static final String ISO_LINE_COLOR_PROP = "iso.draw.color";

  public static final String ISO_LINE_PROP = BSelecteurLineModel.getProperty(0);
  public static final String ISO_PROP = "iso.draw.line";

  public TrIsoLayerConfigure(final TrIsoLayerDefault _target) {
    super(_target, EbliLib.getS("Isolignes"));
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    final List dest = new ArrayList();
    /*
     * if (((ZCalqueAffichageDonneesAbstract) target_).isAntialiasSupported()) { BSelecteurCheckBox cbAntialias = new
     * BSelecteurCheckBox(ANTIALIAS); cbAntialias.setTitle(EbliLib.getS("Lissage"));
     * cbAntialias.setTooltip(EbliLib.getS("Permet de lisser les trac�s")); dest.add(cbAntialias); } dest.add(new
     * BSelecteurAlpha());
     */
    final BSelecteurCheckBox cb = new BSelecteurCheckBox(ISO_PROP);
    cb.setTitle(TrLib.getString("Tracer des isolignes"));
    final BSelecteurLineModel line = new BSelecteurLineModel(ISO_LINE_PROP);
    line.setAddColor(false);

    dest.add(cb);
    dest.add(line);
    line.setEnabled(((TrIsoLayerDefault) target_).isTraceIsoLine());
    final BSelecteurColorChooserBt selecteurColorChooserBt = new BSelecteurColorChooserBt(ISO_LINE_COLOR_PROP);
    selecteurColorChooserBt.setTitle(TrLib.getString("Utiliser une seule couleur"));
    selecteurColorChooserBt.setTooltip(TrLib
        .getString("Permet de modifier la palette de couleur et de n'utiliser qu'une seule couleur"));
    selecteurColorChooserBt.setEnabled(cb.getCb().isSelected());
    dest.add(selecteurColorChooserBt);

    cb.getCb().addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(final ItemEvent _e) {
        line.setEnabled(cb.getCb().isSelected());
        selecteurColorChooserBt.setEnabled(cb.getCb().isSelected());
      }

    });

    return (BSelecteurInterface[]) dest.toArray(new BSelecteurInterface[dest.size()]);
  }

  @Override
  public Object getProperty(final String _key) {
    if (_key == ISO_PROP) { return Boolean.valueOf(((TrIsoLayerDefault) target_).isTraceIsoLine()); }
    if (_key == ISO_LINE_PROP) { return ((TrIsoLayerDefault) target_).getLineModel(0); }
    if (_key == ISO_LINE_COLOR_PROP) {
      final BPalettePlageInterface plages = ((TrIsoLayerDefault) target_).getPaletteCouleur();
      Color c = null;
      if (plages.getNbPlages() > 0) {
        c = plages.getPlageInterface(0).getCouleur();
      }
      if (c == null) { return null; }
      for (int i = plages.getNbPlages() - 1; i > 0; i--) {
        if (!c.equals(plages.getPlageInterface(i).getCouleur())) { return null; }

      }
      return c;
    }
    return null;
  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    if (_key == ISO_PROP) {
      ((TrIsoLayerDefault) target_).setTraceIsoLine(((Boolean) _newProp).booleanValue());
      return true;
    }
    if (_key == ISO_LINE_PROP) {
      ((TrIsoLayerDefault) target_).setLineModel(0, ((TraceLigneModel) _newProp));
      return true;
    }
    if (_key == ISO_LINE_COLOR_PROP) {
      final Color c = (Color) _newProp;
      if (c == null) { return false; }
      final BPalettePlageInterface plages = ((TrIsoLayerDefault) target_).getPaletteCouleur();
      if (plages != null) {
        final BPalettePlage plage = new BPalettePlage(plages.getPlages());
        for (int i = plage.getNbPlages() - 1; i >= 0; i--) {
          plage.getPlage(i).setCouleur(c);
        }
        ((TrIsoLayerDefault) target_).setPaletteCouleurPlages(plage);

      }
    }
    return false;
  }
}
