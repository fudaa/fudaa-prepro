/*
 * @creation 17 juil. 06
 * @modification $Date: 2006-09-08 16:51:33 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.fu.FuLog;
import javax.swing.JLabel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import org.fudaa.dodico.h2d.type.H2dVariableType;

public class TrSuffixeDocListener implements DocumentListener {

  final JLabel lb_;

  /**
   * @param _lb
   */
  public TrSuffixeDocListener(final JLabel _lb) {
    super();
    lb_ = _lb;
  }

  private void update(final DocumentEvent _e) {
    try {
      lb_.setText("<html><body>" + TrResource.getS("Exemple:") + "<br>'" + H2dVariableType.HAUTEUR_EAU + "' -> '"
          + H2dVariableType.HAUTEUR_EAU + _e.getDocument().getText(0, _e.getLength()) + "'</body></html>");
    } catch (final BadLocationException _evt) {
      FuLog.error(_evt);

    }
  }

  @Override
  public void changedUpdate(final DocumentEvent _e) {
    update(_e);
  }

  @Override
  public void insertUpdate(final DocumentEvent _e) {
    update(_e);
  }

  @Override
  public void removeUpdate(final DocumentEvent _e) {
    update(_e);
  }

}