/*
 * @creation 30 nov. 06
 * @modification $Date: 2007-06-13 12:58:10 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import org.locationtech.jts.geom.LineString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsBuilder;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.fudaa.tr.common.TrDataSourceNomme;

/**
 * @author fred deniger
 * @version $Id: MvProfileCourbeModelInterface.java,v 1.3 2007-06-13 12:58:10 deniger Exp $
 */
public interface MvProfileCourbeModelInterface extends EGModel, TrDataSourceNomme {

  /**
   *
   * @return the LineString used to defined the profile
   */
  LineString getLineString();

  EfGridData getData();

  /**
   *
   * @return the line used to compute the profile
   */
  LineString getInitLine();

  /**
   *
   * @return true if the result is defined on meshes.
   */
  boolean isDefinedOnMeshes();

  void setRes(EfLineIntersectionsResultsBuilder _res, ProgressionInterface _prog);
}
