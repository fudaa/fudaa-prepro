/*
 *  @creation     3 mai 2005
 *  @modification $Date: 2006-09-19 15:07:27 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.io.IOException;
import java.util.Set;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.TrPostSource;

/**
 * Calcul de froud.
 * 
 * @author Fred Deniger
 * @version $Id: TrPostDataCreatedFroud.java,v 1.9 2006-09-19 15:07:27 deniger Exp $
 */
public final class TrPostDataCreatedFroud extends TrPostDataCreatedDefault {

  public TrPostDataCreatedFroud(final TrPostSource _src) {
    super(_src);
  }

  @Override
  public String getDescription() {
    return "froud";
  }

  @Override
  public double buildDataFor(final int _idxTime, final int _idxObject) {
    try {
      final double h = src_.getData(H2dVariableType.HAUTEUR_EAU, _idxTime, _idxObject);
      if (h <= 0) { return 0; }
      final double vx = src_.getData(H2dVariableType.VITESSE_U, _idxTime, _idxObject);
      final double vy = src_.getData(H2dVariableType.VITESSE_V, _idxTime, _idxObject);
      final double gravit = ((Double) src_.getGravity().getValue()).doubleValue();
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("TRP: froud use gravity " + gravit);
      }
      return Math.sqrt((vx * vx + vy * vy) / (gravit * h));
    } catch (final IOException _e) {
      FuLog.warning(_e);
    }
    return 0;
  }

  @Override
  public void fillWhithAllUsedVar(final Set _res) {
    if (_res != null) {
      _res.add(H2dVariableType.VITESSE_U);
      _res.add(H2dVariableType.VITESSE_V);
      _res.add(H2dVariableType.HAUTEUR_EAU);
    }

  }

  @Override
  public EfData buildDataFor(final int _idxTime) {
    final EfData vx = src_.getData(H2dVariableType.VITESSE_U, _idxTime);
    final EfData vy = src_.getData(H2dVariableType.VITESSE_V, _idxTime);
    final EfData h = src_.getData(H2dVariableType.HAUTEUR_EAU, _idxTime);
    final double[] froud = new double[vx.getSize()];
    final double gravit = ((Double) src_.getGravity().getValue()).doubleValue();
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("TRP: froud use gravity " + gravit);
    }
    for (int i = froud.length - 1; i >= 0; i--) {
      final double hi = h.getValue(i);

      if (hi <= 0) {
        froud[i] = 0;
      } else {
        final double vxi = vx.getValue(i);
        final double vyi = vy.getValue(i);
        froud[i] = Math.sqrt((vxi * vxi + vyi * vyi) / (gravit * hi));
      }
    }
    return vx.isElementData() ? ((EfData) new EfDataElement(froud)) : ((EfData) new EfDataNode(froud));
  }
}