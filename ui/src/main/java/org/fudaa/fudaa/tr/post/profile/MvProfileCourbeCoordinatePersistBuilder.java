package org.fudaa.fudaa.tr.post.profile;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.LineString;
import java.io.File;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.FastBitSet;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.courbe.EGCourbePersistBuilder;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.actions.TrPostProfileAction;
import org.fudaa.fudaa.tr.post.persist.TrPostReloadParameter;
import org.fudaa.fudaa.tr.post.profile.MvProfileXYProvider.Cached;

public class MvProfileCourbeCoordinatePersistBuilder extends EGCourbePersistBuilder<MVProfileCourbeCoordinate> {

  public static final String HIDDEN_POINTS = "hiddenPoints";
  public static final String HIDDEN_SEGMENTS = "hiddenSegments";
  public static final String PROFILE_LINE_X = "PROFILE_LINE_X";
  public static final String PROFILE_LINE_Y = "PROFILE_LINE_Y";

  protected static Cached createPersistProfileData(Map _infos, final int[] hiddenPointsIdx, final int[] hiddenSegmentsIdx) {
    double[] courbeX = (double[]) _infos.get(X_KEY);
    double[] courbeY = (double[]) _infos.get(Y_KEY);
    double[] lineX = (double[]) _infos.get(PROFILE_LINE_X);
    double[] lineY = (double[]) _infos.get(PROFILE_LINE_Y);
    if (courbeX != null && courbeY != null && lineX != null && lineY != null) {
      if (courbeX.length != courbeY.length) {
        return null;
      }
      if (lineX.length != lineY.length) {
        return null;
      }
      Coordinate[] cs = new Coordinate[lineX.length];
      for (int i = 0; i < cs.length; i++) {
        cs[i] = new Coordinate(lineX[i], lineY[i]);
      }

      FastBitSet  hiddenPoints = CtuluListSelection.createForSelectedIdx(hiddenPointsIdx);
      FastBitSet hiddenSegment = CtuluListSelection.createForSelectedIdx(hiddenSegmentsIdx);
      LineString init = GISGeometryFactory.INSTANCE.createLineString(cs);
      final Cached createCachedData = MvProfileXYProvider.createCachedData(courbeX, courbeY, hiddenSegment, hiddenPoints, init);
      return createCachedData;
    }
    return null;
  }

  @Override
  protected MVProfileCourbeCoordinate createEGObject(EGCourbePersist target, Map params, CtuluAnalyze log) {
    return new MVProfileCourbeCoordinate(getGroup(params), createModel(target, params));
  }

  @Override
  protected Map<String, double[]> generateMapToPersistInBinFile(EGModel initialModel) {
    final Map<String, double[]> map = super.generateMapToPersistInBinFile(initialModel);
    LineString lineString = ((MvProfileCoordinatesModel) initialModel).getLineString();
    putInMap(lineString, map);
    return map;
  }

  protected static void putInMap(LineString data, Map<String, double[]> target) {
    final CoordinateSequence coordinateSequence = data.getCoordinateSequence();
    double[] x = new double[coordinateSequence.size()];
    double[] y = new double[x.length];
    for (int i = 0; i < y.length; i++) {
      x[i] = coordinateSequence.getX(i);
      y[i] = coordinateSequence.getY(i);
    }
    target.put(PROFILE_LINE_X, x);
    target.put(PROFILE_LINE_Y, y);
  }

  @Override
  protected MvProfileCoordinatesModel createModel(EGCourbePersist persist, Map _infos) {
    boolean isX = persist.getSpecificBooleanValue("isX");
    LineString line = (LineString) persist.getSpecificValue("initLine");
    TrPostProjet projet = (TrPostProjet) _infos.get(TrPostReloadParameter.POST_PROJET);
    String idSource = persist.getSpecificStringValue("dataId");
    TrPostSource src = projet.getSources().findSourceById(idSource);
    if (src == null) {
      ((List<String>) _infos.get("errorMsg")).add("Erreur, la frame graphe ne trouve pas le fichier r�sultat qui correspond � l'ID " + idSource);
      return null;
    }
    MvProfileTarget target = new TrPostProfileAction.ProfileAdapter(src, projet);
    MvProfileBuilder builder = new MvProfileBuilderFromLine(target, projet.impl_, line, new MvProfileCoteTester());
    boolean isNode = persist.getSpecificBooleanValue("isNode");

    boolean forceRecompute = TrPostReloadParameter.isForceRecompute(_infos);
    if (!forceRecompute) {
      Cached persistProfileData = createPersistProfileData(persist, _infos);
      if (persistProfileData != null) {
        return new MvProfileCoordinatesModel(src, line, persistProfileData, isX, !isNode);
      }
    }
    EfLineIntersectionsResultsI res;
    if (isNode) {
      res = builder.getNodeResultsMng(null).getDefaultRes();
    } else {
      res = builder.getMeshResultsMng(null).getDefaultRes();
    }
    // -- intersectionResultI --//

    return new MvProfileCoordinatesModel(src, line, res, isX);
  }

  protected static Cached createPersistProfileData(EGCourbePersist persist, Map _infos) {
    final int[] hiddenPointsIdx = (int[]) persist.getSpecificValue(HIDDEN_POINTS);
    final int[] hiddenSegmentsIdx = (int[]) persist.getSpecificValue(HIDDEN_SEGMENTS);
    return createPersistProfileData(_infos, hiddenPointsIdx, hiddenSegmentsIdx);
  }

  @Override
  protected void postCreatePersist(EGCourbePersist res, MVProfileCourbeCoordinate courbe, EGGraphe graphe, File binFile) {
    super.postCreatePersist(res, courbe, graphe, binFile);
    MvProfileCoordinatesModel model = (MvProfileCoordinatesModel) courbe.getModel();
    res.saveSpecificData("isX", model.isX_);
    res.saveSpecificData("isNode", !model.isDefinedOnMeshes);
    res.saveSpecificData("initLine", model.initLine);
    res.saveSpecificData(HIDDEN_POINTS, model.xyProvider.getHiddenPoints());
    res.saveSpecificData(HIDDEN_SEGMENTS, model.xyProvider.getHiddenSegments());
    // -- ajout des datas --//
    if (model.data_ instanceof TrPostSource) {
      res.saveSpecificData("dataId", ((TrPostSource) model.data_).getId());
    }
  }
}
