/*
 *  @creation     6 avr. 2005
 *  @modification $Date: 2008-01-17 16:20:15 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuLog;
import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.gis.*;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2DSiProfilListener;
import org.fudaa.dodico.h2d.H2dSiSourceInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliListeSelectionMulti;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.tr.common.TrResource;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.*;

import java.util.*;

/**
 * Le mod�le pour le calque des profils de solutions initiales.
 *
 * @author Fred Deniger
 * @version $Id: TrSiProfilModel.java,v 1.19.4.1 2008-01-17 16:20:15 bmarchan Exp $
 */
public class TrSiProfilModel extends ZModeleLigneBriseeEditable implements GISZoneListener {
  /**
   * @author Fred Deniger
   * @version $Id: TrSiProfilModel.java,v 1.19.4.1 2008-01-17 16:20:15 bmarchan Exp $
   */
  public static class Zone extends GISZoneCollectionPolygone {
    int nbZone_;
    List name_ = new ArrayList();
    GISAttributeModelObjectArray array_;

    public Zone() {
      super(null);
      setAttributeIsZ(null);
      setAttributes(null, null);
    }

    public int getAttributeNb() {
      return 1;
    }

    @Override
    public GISAttributeModel getModel(final GISAttributeInterface _att) {
      return array_;
    }

    @Override
    public GISAttributeModel getModel(final int _i) {
      return array_;
    }

    @Override
    public GISAttributeInterface getAttribute(final int _i) {
      return GISAttributeConstants.TITRE;
    }

    protected void add(final TrSIZone _r) {
      super.geometry_.addObject(_r, null);
      name_.add(TrResource.getS("Surface") + CtuluLibString.ESPACE + super.geometry_.getSize());
    }

    protected void addLitMineure(final int _idxZone, final GISPolygone _poly) {
      super.geometry_.addObject(_poly, null);
      name_.add(TrResource.getS("Lit mineur") + CtuluLibString.ESPACE + (_idxZone + 1));
    }

    protected void clear() {
      nbZone_ = 0;
      super.geometry_.removeAll();
      array_ = null;
    }

    void setEditable(final boolean _b) {
      super.isGeomModifiable_ = _b;
    }

    void zoneExternBuild() {
      nbZone_ = super.getNumGeometries();
    }

    void zoneBuildFinal() {
      array_ = new GISAttributeModelObjectArray(name_.toArray(), GISAttributeConstants.TITRE);
      name_.clear();
    }

    public int getNbZone() {
      return nbZone_;
    }
  }

  LinearRing[] frRing_;
  EfGridInterface grid_;
  Envelope[] gridEnv_;
  PointOnGeometryLocator[] testers_;
  final GISAttributeDouble hauteur_ = new GISAttributeDouble(H2dVariableType.COTE_EAU.getName(), true);
  Set listener_;
  CtuluCommandContainer cmd_;

  /**
   * Met les valeurs de la variable hauteur dans le z des lignes brisees.
   */
  @Override
  public void prepareExport() {
    final int[] str = getProfilLinesIdx();
    if (str != null) {
      for (int i = str.length - 1; i >= 0; i--) {
        final GISAttributeModelDoubleArray arr = (GISAttributeModelDoubleArray) geometries_.getModel(hauteur_)
            .getObjectValueAt(str[i]);
        final CoordinateSequence seq = geometries_.getCoordinateSequence(str[i]);
        for (int k = seq.size() - 1; k >= 0; k--) {
          seq.setOrdinate(k, 2, arr.getValue(k));
        }
      }
    }
  }

  @Override
  protected void fillWithAtomicInfo(final int _idxLigne, final int _idxPt, final InfoData _d) {
    final int nbAtt = getAttributeNb();
    for (int i = 0; i < nbAtt; i++) {
      if (getAttribute(i).isAtomicValue() && (i == 0 || (isSpecLigne(_idxLigne)))) {
        _d.put(getAttribute(i).getName(), ((GISAttributeModel) getDataModel(i).getObjectValueAt(_idxLigne))
            .getObjectValueAt(_idxPt).toString());
      }
    }
  }

  ;

  @Override
  protected void fillWithInfo(final int _idxLigne, final InfoData _d) {
    super.fillWithInfo(_idxLigne, _d);
    _d.put(CtuluLib.getS("Profil"), isSpecLigne(_idxLigne) ? CtuluLib.getS("vrai") : CtuluLib.getS("faux"));
  }

  /**
   * @param _ligneIdx l'indice de la ligne
   * @return la tableau des valeurs des cotes
   */
  protected GISAttributeModelDoubleArray getHauteur(final int _ligneIdx) {
    return (GISAttributeModelDoubleArray) geometries_.getModel(hauteur_).getObjectValueAt(_ligneIdx);
  }

  /**
   * @param _domaine le domaine du maillage utilise pour calcul les zones.
   */
  public TrSiProfilModel(final EfGridInterface _domaine, final CtuluCommandContainer _cmd) {
    grid_ = _domaine;
    cmd_ = _cmd;
    geometries_ = createGeometry();
  }

  /**
   * @param _zone la zone a parcourir
   * @param _si les solutions initiales
   * @param _prog la barre de progression
   * @return true si le calcul a pu etre fait.
   */
  public boolean computeZeUV(final TrSiProfilModel.Zone _zone, final H2dSiSourceInterface _si,
                             final ProgressionInterface _prog) {
    if (!isProfilLineDefined()) {
      return false;
    }

    int nb = _zone.nbZone_;
    TrSiZoneOperation[] op = new TrSiZoneOperation[nb];

    if (nb == 0 && isProfilTwoPointOut()) {
      final int idx = getProfilLinesIdx()[0];
      nb = 1;
      op = new TrSiZoneOperation[1];
      final TrSIZone zone = new TrSIZone(GISGeometryFactory.INSTANCE.createLinearRing(grid_.getMinX(), grid_.getMaxX(),
          grid_.getMinY(), grid_.getMaxY()).getCoordinateSequence(), 0);
      zone.setPtAmont(1);
      zone.setPtAval(0);
      op[0] = new TrSiZoneOperation(zone, getLine(idx), getCoteModel(idx));
    } else {
      for (int i = 0; i < nb; i++) {
        final TrSIZone zone = (TrSIZone) _zone.getGeometry(i);
        final int idx = zone.getProfileIdx();
        op[i] = new TrSiZoneOperation(zone, getLine(idx), getCoteModel(idx));
      }
    }
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(5, grid_.getPtsNb(), 0, 100);
    final double[] cote = _si.getZe();
    final double[] u = _si.getU();
    final double[] v = _si.getV();
    final CtuluCollectionDouble bathy = _si.getBathyModel();
    for (int idxPt = grid_.getPtsNb() - 1; idxPt >= 0; idxPt--) {
      for (int idxZ = nb - 1; idxZ >= 0; idxZ--) {
        if (op[idxZ].process(grid_.getPtX(idxPt), grid_.getPtY(idxPt), idxPt, cote, u, v, bathy)) {
          break;
        }
      }
    }
    _si.set(cote, u, v, cmd_);

    return true;
  }

  void createTesterZone() {
    if (frRing_ == null) {
      frRing_ = grid_.getExtRings();
      gridEnv_ = GISLib.getInternalEnvelop(frRing_);
      testers_ = GISLib.getTester(frRing_);
    }
  }

  protected void computeZones(final Zone _zones, final CtuluAnalyze _analyze) {
    final TrSiProfileSplitter splitter = new TrSiProfileSplitter(this);
    splitter.computeZones(_zones, _analyze);
  }

  public static final String BERGE_ID = "B";
  public static final String PROFIL_ID = "P";
  private final GISAttributeString type_ = new GISAttributeString(null, "TYPE");

  @Override
  protected final GISZoneCollectionLigneBrisee createGeometry() {
    final GISZoneCollectionLigneBrisee r = GISZoneAttributeFactory.createPolyligne(this, new GISAttributeInterface[]{
        GISAttributeConstants.TITRE, hauteur_, type_});
    r.setAttributeIsZ(hauteur_);
    return r;
  }

  protected void fireSiDataChanged() {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2DSiProfilListener) it.next()).siProfilDataChanged();
      }
    }
  }

  /**
   * Comporte l'indice du domaine contenant la lignes brisee.
   */
  int[] domaineIdx_;

  protected void fireSiGeometryChanged() {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2DSiProfilListener) it.next()).siProfilGeometryChanged();
      }
    }
    clearSavedData();
  }

  private void clearSavedData() {
    domaineIdx_ = null;
  }

  public int getDomaineIdx(final int _geom) {
    return domaineIdx_ == null ? -1 : domaineIdx_[_geom];
  }

  void computeIdx() {
    if (domaineIdx_ != null) {
      return;
    }
    if (geometries_ == null) {
      domaineIdx_ = FuEmptyArrays.INT0;
    } else {
      createTesterZone();
      final int nb = geometries_.getNumGeometries();
      domaineIdx_ = new int[nb];
      final Coordinate tmp = new Coordinate();
      for (int i = nb - 1; i >= 0; i--) {
        domaineIdx_[i] = GISLib.isInIdx(geometries_.getCoordinateSequence(i), gridEnv_, testers_, tmp);
      }
    }
  }

  public final LineString getLine(final int _idx) {
    return (LineString) geometries_.getGeometry(_idx);
  }

  protected int[] getBergesIntersectProfil() {
    final LineString[] l = getProfilLines();
    if (l == null) {
      return null;
    }
    final TIntArrayList list = new TIntArrayList();
    final GISAttributeModel model = geometries_.getModel(type_);
    for (int i = getNombre() - 1; i >= 0; i--) {
      if (BERGE_ID.equals(model.getObjectValueAt(i))
          && GISLib.intesects(((LineString) super.geometries_.getGeometry(i)), l)) {
        list.add(i);
      }
    }
    return list.isEmpty() ? null : list.toNativeArray();
  }

  // protected Coordinate getCoordinateOnEnvelop(int _i) {
  // final int reelIdx = _i % 4;
  // switch (reelIdx) {
  // case 0:
  // return new Coordinate(gridEnv_.getMinX(), gridEnv_.getMinY());
  // case 1:
  // return new Coordinate(gridEnv_.getMaxX(), gridEnv_.getMinY());
  // case 2:
  // return new Coordinate(gridEnv_.getMaxX(), gridEnv_.getMaxY());
  // case 3:
  // return new Coordinate(gridEnv_.getMinX(), gridEnv_.getMaxY());
  // default:
  // return null;
  // }
  // }

  public int[] getProfilLinesIdx() {
    if (geometries_ == null) {
      return null;
    }
    final int nb = geometries_.getNumGeometries();
    final TIntArrayList list = new TIntArrayList(nb);
    final GISAttributeModel types = geometries_.getModel(type_);
    for (int i = 0; i < nb; i++) {
      if (PROFIL_ID.equals(types.getObjectValueAt(i))) {
        list.add(i);
      }
    }
    return list.isEmpty() ? null : list.toNativeArray();
  }

  protected LineString[] getProfilLines() {
    final int[] idx = getProfilLinesIdx();
    if (idx == null) {
      return null;
    }
    final LineString[] ls = new LineString[idx.length];
    for (int i = 0; i < ls.length; i++) {
      ls[i] = (LineString) geometries_.getGeometry(idx[i]);
    }
    return ls;
  }

  public boolean isProfilLineDefined() {
    if (geometries_ == null) {
      return false;
    }
    final int nb = geometries_.getNumGeometries();
    final GISAttributeModel types = geometries_.getModel(type_);
    for (int i = 0; i < nb; i++) {
      if (PROFIL_ID.equals(types.getObjectValueAt(i))) {
        return true;
      }
    }
    return false;
  }

  int[] getExtFrContainingProfil() {
    if (!isProfilLineDefined()) {
      return null;
    }
    final LineString[] profiles = getProfilLines();
    final int[] res = new int[profiles.length];
    createTesterZone();
    return res;
  }

  protected boolean isProfilTwoPointOut() {
    final int[] profiles = getProfilLinesIdx();
    if (profiles == null || profiles.length != 1) {
      return false;
    }
    final CoordinateSequence l = ((LineString) geometries_.getGeometry(profiles[0])).getCoordinateSequence();
    if (l == null || l.size() != 2) {
      return false;
    }
    createTesterZone();
    final Coordinate xi = new Coordinate();
    l.getCoordinate(0, xi);
    if (GISLib.isIn(xi, gridEnv_, testers_)) {
      return false;
    }
    l.getCoordinate(1, xi);
    if (GISLib.isIn(xi, gridEnv_, testers_)) {
      return false;
    }
    return true;
  }

  public boolean isProfile(final int _idx) {
    return PROFIL_ID.equals(geometries_.getModel(type_).getObjectValueAt(_idx));
  }

  protected boolean isProfilPointsOut() {
    final int nb = geometries_.getNumGeometries();
    for (int i = 0; i < nb; i++) {
      if (isProfile(i) && !CtuluLibArray.isEmpty(getProfilPointsOut(i))) {
        return false;
      }
    }
    return false;
  }

  protected EbliListeSelectionMulti getProfilPointsOut() {
    final int nb = geometries_.getNumGeometries();
    final EbliListeSelectionMulti res = new EbliListeSelectionMulti(nb);
    for (int i = 0; i < nb; i++) {
      if (isProfile(i)) {
        final int[] idxOut = getProfilPointsOut(i);
        if (idxOut != null) {
          res.set(i, new CtuluListSelection(idxOut));
        }
      }
    }
    return res.isEmpty() ? null : res;
  }

  protected int[] getProfilPointsOut(final int _idxProfil) {
    final CoordinateSequence l = geometries_.getCoordinateSequence(_idxProfil);
    computeIdx();
    if (domaineIdx_[_idxProfil] < 0) {
      final int[] res = new int[l.size()];
      for (int i = res.length - 1; i >= 0; i--) {
        res[i] = i;
      }
      return res;
    }
    createTesterZone();
    final int domaine = domaineIdx_[_idxProfil];
    // LinearRing ring = frRing_[domaine];
    final PointOnGeometryLocator included = testers_[domaine];
    final Coordinate xi = new Coordinate();
    // le premier point a l'interieur
    int firstPointInside = 0;
    final int nbProfilPt = l.size();
    l.getCoordinate(firstPointInside, xi);
    if (!GISLib.isInside(included, xi)) {
      firstPointInside++;
    }
    if (firstPointInside == nbProfilPt) {
      if (FuLog.isTrace()) {
        FuLog.trace("TRS: tous les points sont externes");
      }
      final int[] res = new int[nbProfilPt];
      for (int i = res.length - 1; i >= 0; i--) {
        res[i] = i;
      }
      return res;
    }
    // le dernier point a l'interieur
    int lastPointInside = nbProfilPt - 1;
    l.getCoordinate(lastPointInside, xi);
    if (!GISLib.isInside(included, xi)) {
      lastPointInside--;
    }
    final TIntArrayList list = new TIntArrayList();
    for (int i = firstPointInside; i <= lastPointInside; i++) {
      l.getCoordinate(i, xi);
      if (!GISLib.isInside(included, xi)) {
        list.add(i);
      }
    }
    return list.size() == 0 ? null : list.toNativeArray();
  }

  protected boolean isBergesIntersectProfil() {
    final LineString[] l = getProfilLines();
    if (l == null) {
      return false;
    }
    final GISAttributeModel model = geometries_.getModel(type_);
    for (int i = getNombre() - 1; i >= 0; i--) {
      if (BERGE_ID.equals(model.getObjectValueAt(i))
          && GISLib.intesects(((LineString) super.geometries_.getGeometry(i)), l)) {
        return true;
      }
    }
    return false;
  }

  @Override
  protected boolean isIntersectedLigneBriseeAccepted() {
    return false;
  }

  public void addListener(final H2DSiProfilListener _si) {
    if (listener_ == null) {
      listener_ = new HashSet();
    }
    listener_.add(_si);
  }

  @Override
  public void attributeAction(Object source, int indexAtt, GISAttributeInterface att, int action) {
    fireSiDataChanged();
  }

  @Override
  public void objectAction(Object source, int idx, Object obj, int action) {
    super.objectAction(source, idx, obj, action);
    fireSiGeometryChanged();
  }

  public final GISAttributeInterface[] getCoteAttributes() {
    return new GISAttributeInterface[]{hauteur_};
  }

  public final GISAttributeModelDoubleInterface getCoteModel(final int _idxPoly) {
    final GISAttributeModel m = super.geometries_.getDataModel(1);
    return (GISAttributeModelDoubleInterface) m.getObjectValueAt(_idxPoly);
  }

  public boolean isSpecLigne(final int _i) {
    return geometries_ != null && PROFIL_ID.equals(geometries_.getModel(type_).getObjectValueAt(_i));
  }

  @Override
  public boolean removeLigneBrisee(final int[] _idx, final CtuluCommandContainer _cmd) {
    final boolean r = super.removeLigneBrisee(_idx, _cmd);
    if (r) {
      fireSiGeometryChanged();
    }
    return r;
  }

  public void removeListener(final H2DSiProfilListener _si) {
    if (listener_ != null) {
      listener_.remove(_si);
    }
  }

  /**
   * @param _idx l'indice de la ligne servant de profil
   * @param _cmd le receveur de commande
   * @param _fireEvent true si un evt doit etre envoy�.
   */
  public void setProfil(final int _idx, final CtuluCommandContainer _cmd, final boolean _fireEvent) {

    if (!isSpecLigne(_idx)) {
      computeIdx();
      final int domaine = domaineIdx_[_idx];
      // pour l'instant, il ne doit y avoir qu'un profil par berge
      // le profil a annul� au cas ou le nouveau profil appartient au meme domaine
      int profilToUnset = -1;
      // on parcourt toutes les lignes tant que pas de profil trouv�
      for (int i = domaineIdx_.length - 1; i >= 0 && profilToUnset < 0; i--) {
        // on ne teste pas le meme profile evidemment
        // si c'est le meme domaine et si la ligne est un profile on l'enleve
        if (i != _idx && domaineIdx_[i] == domaine && isProfile(i)) {
          profilToUnset = i;
        }
      }
      CtuluCommandContainer cmd = _cmd;
      if (cmd != null && profilToUnset >= 0 && !(cmd instanceof CtuluCommandComposite)) {
        cmd = new CtuluCommandComposite();
        _cmd.addCmd((CtuluCommandComposite) cmd);
      }
      if (profilToUnset >= 0) {
        geometries_.getModel(type_).setObject(profilToUnset, BERGE_ID, cmd);
      }

      geometries_.getModel(type_).setObject(_idx, PROFIL_ID, cmd);
      if (_fireEvent) {
        fireSiGeometryChanged();
      }
    }
  }
}
