package org.fudaa.fudaa.tr.post;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.telemac.io.ScopeStructure;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeModel;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.replay.TrReplayScope;

/**
 * Modele des courbes scop. Model of the scop curves
 *
 * @author Adrien Hadoux
 */
public class TrPostScopeCourbeModel extends FudaaCourbeModel {

  /**
   * Map d'informations specifiques scop qui a permi de generer ce model de courbe. Specific informations map which has allowed to generate this model
   * of curve
   */
  Map infos_;

  public TrPostScopeCourbeModel(final EvolutionReguliere _e) {
    super(_e);
    infos_ = new HashMap(_e.infos_);

  }

  /**
   * Achtung: utiliser uniqument pour la persistance des donnees. Use only for the persistance of datas
   */
  public TrPostScopeCourbeModel() {
    super();
    this.e_ = new EvolutionReguliere();

  }

  @Override
  public Object savePersistSpecificDatas() {
    return infos_;
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
    super.restoreFromSpecificDatas(data, infos);
    if (data != null && (data instanceof Map)) {
      infos_ = (Map) data;
    }

  }

  /**
   * Retourne le fichier qui a permi de g�n�rer la courbe. Retourne null si le fichier n'a pa s �t� enregistr�. Return the file which has allowed to
   * generate the curve. Return null if the file has not been saved.
   *
   * @return
   */
  public File getFichierGenerateur() {
    if (infos_ == null) {
      return null;
    }
    if (infos_.get(ScopeStructure.NOM_FICHIER) != null) {

      String path = infos_.get(ScopeStructure.NOM_FICHIER).toString();

      File res = new File(path);

      return res;

    } else {
      return null;
    }

  }

  @Override
  public void fillWithInfo(InfoData _table, CtuluListSelectionInterface _selectedPt) {
    if (infos_ != null) {
      for (Object key : infos_.keySet()) {
        if (infos_.get(key) != null) {
          _table.put(key.toString(), infos_.get(key).toString());
        }
      }
    }

  }

  @Override
  public EGModel duplicate() {
    if (this.e_ instanceof EvolutionReguliere) {
      ((EvolutionReguliere) this.e_).infos_ = infos_;
    }

    return new TrPostScopeCourbeModel((EvolutionReguliere) this.e_.getCopy(this.e_.getListener()));
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
    if (infos_.get(ScopeStructure.NOM_FICHIER) != null) {
      String msg = "Cette courbe est issue du fichier " + infos_.get(ScopeStructure.NOM_FICHIER); //This curv is issued from the file	  
      impl.message("Origine de la courbe scop", msg, false); //Origin of the curve scop
    } else {
      impl.message("Origine de la courbe scop", "Impossible de trouver", false);
      //Origin of the scop curve, impossible to find 
    }
  }

  public void replayData(org.fudaa.ebli.courbe.EGGrapheTreeModel model, Map infos, CtuluUI impl) {
    if (!(impl instanceof TrPostCommonImplementation)) {
      impl.error(TrResource.getS("Impossible de r�cup�rer la bonne interface fudaa")); //Impossible to recuparate the good fudaa interface
      return;
    }
    TrPostCommonImplementation implementation = (TrPostCommonImplementation) impl;

    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    // we create a 2d sight from the source list given and we center it on the interpolated point or not  
    new TrReplayScope().getScopGeneReplayData(model, this, implementation);

  }
}
