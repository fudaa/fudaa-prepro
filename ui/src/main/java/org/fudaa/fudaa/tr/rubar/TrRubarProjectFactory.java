/**
 * @creation 9 juin 2004
 * @modification $Date: 2007-06-29 15:09:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuResource;
import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.gui.CtuluAnalyzeGUI;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.dodico.fortran.FortranDoubleReaderResultInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dRubarFileType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.rubar.io.*;
import org.fudaa.fudaa.tr.common.TrImplementationEditorAbstract;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarProjectFactory.java,v 1.42 2007-06-29 15:09:41 deniger Exp $
 */
public final class TrRubarProjectFactory {
  private TrRubarProjectFactory() {
    super();
  }

  /**
   * @param _file le fichier MAI
   * @param _int la progression
   * @param _ui l'interface utilisateur
   * @return le nouveau projet
   */
  public static TrRubarProject createNewProject(final File _file, final ProgressionInterface _int, final TrImplementationEditorAbstract _ui) {
    final File f = findFile(_ui, _file);
    if (f == null) {
      return null;
    }
    final TrRubarFileStateMng mng = new TrRubarFileStateMng();
    // Le titre du projet

    final String title = CtuluLibFile.getSansExtension(f.getName());
    final String mes = BuResource.BU.getString("Ouverture") + CtuluLibString.ESPACE + title;
    _ui.setMainMessage(mes);
    if (_int != null) {
      _int.setDesc(mes);
    }
    // le rep du projet
    final File dir = f.getParentFile();
    final String ext = RubarMAIFileFormat.getInstance().getExtensions()[0];
    // lecture du fichier de maillage
    final File maiFile = new File(dir, CtuluLibFile.getFileName(title, ext));
    H2dRubarGridAreteSource datSrc = null;
    final File datFile = RubarDATFileFormat.getInstance().getFileFor(dir, title);
    // tout existe ... on ne cree pas
    if (datFile.exists() && RubarPARFileFormat.getInstance().getFileFor(dir, title).exists()) {
      return loadProject(RubarPARFileFormat.getInstance().getFileFor(dir, title), _int, _ui);
    }

    CtuluIOOperationSynthese s;

    boolean datCreated = false;
    boolean newFormat = false;
    // Le fichier n'existe pas (message d'erreur a ajouter)
    if (maiFile.exists() && (!datFile.exists())) {
      final Pair<H2dRubarGridAreteSource, Boolean> gridFromMAIFile = createDATGridFromMAIFile(_int, _ui, mng, maiFile);
      if (gridFromMAIFile == null) {
        return null;
      }
      datSrc = gridFromMAIFile.firstValue;
      newFormat = gridFromMAIFile.secondValue;
      datCreated = true;
    } else if (datFile.exists()) {
      RubarDATReader reader = (RubarDATReader) RubarDATFileFormat.getInstance().createReader();
      s = reader.read(datFile, _int);
      newFormat = reader.isNewFormat();
      if (_ui.manageErrorOperationAndIsFatal(s)) {
        _ui.unsetMainMessage();
        return null;
      }
      datSrc = (H2dRubarGridAreteSource) s.getSource();
    }
    if (datSrc == null) {
      _ui.error(TrResource.getS("Maillage non trouv�"));
      return null;
    }
    mng.setProjectName(title);
    final CtuluAnalyze an = new CtuluAnalyze();
    // on va essayer de deviner le type du projet: si le fichier inx contient 4 donnees -> transport
    // sinon couranto
    final File inx = RubarINXFileFormat.getInstance().getFileFor(dir, title);
    H2dRubarProjetType t = null;
    int nbTransportBlock = H2dRubarBcMng.NEW_TRANSPORT_PROJECT_DEFAULT_BLOCK;
    if (inx.exists()) {
      s = RubarINXFileFormat.getInstance().read(inx, _int);
      if (!s.containsSevereError()) {
        final FortranDoubleReaderResultInterface res = (FortranDoubleReaderResultInterface) s.getSource();
        final int nbVal = res.createXYValuesInterface().getNbValues();
        if (nbVal >= 4) {
          t = H2dRubarProjetType.TRANSPORT;
          nbTransportBlock = (nbVal - 4) / 3;
        } else {
          t = H2dRubarProjetType.COURANTOLOGIE_2D;
        }
      }
    } else {
      final File ini = RubarINIFileFormat.getInstance().getFileFor(dir, title);
      if (ini.exists()) {
        s = RubarINIFileFormat.getInstance().read(ini, _int);
        if (!s.containsSevereError()) {
          final RubarVF2MResult res = (RubarVF2MResult) s.getSource();
          int nbVal = 3;
          if (res.getGridData() != null) {
            nbVal = res.getGridData().getNbValues();
          } else if (res.getNbParall() > 0) {
            nbVal = res.getParall(0).getNbValues();
          }
          if (nbVal >= 4) {
            t = H2dRubarProjetType.TRANSPORT;
            nbTransportBlock = (nbVal - 4) / 3;
          } else {
            t = H2dRubarProjetType.COURANTOLOGIE_2D;
          }
        }
      }
    }
    nbTransportBlock = Math.max(1, nbTransportBlock);
    if (t == null) {
      t = H2dRubarProjetType.COURANTOLOGIE_2D;
    }
    // g ne peut �tre que null;
    final TrRubarNewProjectPanel pn = new TrRubarNewProjectPanel(t, datSrc.getGrid().getEltNb(), newFormat, nbTransportBlock);
    if (!pn.afficheModaleOk(_ui.getImplementation().getFrame(), TrResource.getS("Nouveau projet"), CtuluDialog.OK_OPTION)) {
      return null;
    }
    t = pn.getSelectedProjetType();
    final H2DRubarSolutionsInitialesInterface si = pn.createSolutionInitales();
    final CtuluCollectionDoubleEdit frt = pn.getFrt();
    H2DRubarDicoCasFileFormatVersion model = null;
    if (t == H2dRubarProjetType.TRANSPORT) {
      model = RubarPARFileFormat.getInstance().getLastTransportVersionImpl(Math.max(1, nbTransportBlock));
    } else {
      model = (H2DRubarDicoCasFileFormatVersion) RubarPARFileFormat.getInstance().getLastVersionImpl();
    }
    final H2DRubarDicoParams param = new H2DRubarDicoParams(null, model);
    final H2dRubarParameters p = H2dRubarParameters
        .createParameters(t, datSrc, param, null, null, _int, an, new boolean[1], newFormat ? H2dRubarFileType.NEW_FORMAT_9_DIGITS : H2dRubarFileType.OLD_FORMAT_6_DIGITS,
            nbTransportBlock);
    if (newFormat) {
      p.setNbConcentrationBlocks(nbTransportBlock, null);
    }
    final TrRubarFileStateDAT trRubarFileStateDAT = new TrRubarFileStateDAT(mng, p);
    mng.add(trRubarFileStateDAT);
    if (datCreated) {
      trRubarFileStateDAT.setModified(true);
    }
    if (p == null) {
      _ui.unsetMainMessage();
      return null;
    }
    // frottement
    readDonneesBrutes(_int, _ui, mng, title, dir, p);
    final TrRubarProject proj = new TrRubarProject(new TrRubarParameters(p, title, dir, _ui), mng);
    proj.setNewFormatForNumberOfDigits(newFormat, false, null);
    p.setSI(si);
    p.setFriction(frt);
    // les fichiers qui seront cr��s d'office
    final TrRubarFileStateCIN cin = new TrRubarFileStateCIN(mng, p);

    cin.setModified(true);
    mng.add(cin);
    final TrRubarFileStateFRT frtState = new TrRubarFileStateFRT(mng, p);
    frtState.setModified(true);
    mng.add(frtState);
    final TrRubarFileStatePAR parState = new TrRubarFileStatePAR(mng, p);
    parState.setModified(true);
    mng.add(parState);
    new TrRubarFileStateAPP(mng, p);
    new TrRubarFileStateVEN(mng, p);
    new TrRubarFileStateSED(mng, p);
    new TrRubarFileStateOUV(mng, p.getOuvrageMng(), p.getBcMng());
    final TrRubarFileStateCLI cli = new TrRubarFileStateCLI(mng, p);
    mng.add(cli);
    cli.setModified(p.getBcMng().isTimeConditionInit());
    new TrRubarFileStateDTR(mng, p);

    // diffusion
    final File dif = RubarDIFFileFormat.getInstance().getFileExistFor(dir, title);
    if (dif != null) {
      s = RubarDIFFileFormat.getInstance().read(dif, _int, datSrc.getGrid().getEltNb());
      if (!_ui.manageErrorOperationAndIsFatal(s)) {
        p.createDiffusion(((CtuluCollectionDoubleEdit) s.getSource()).getValues(), null);
      }
    }
    new TrRubarFileStateDIF(mng, p);
    final TrRubarProjectDispatcherListener lis = new TrRubarProjectDispatcherListener();
    p.setListener(lis);
    lis.setProj(proj);
    _ui.unsetMainMessage();
    mng.setState(proj.getState());
    return proj;
  }

  private static Pair<H2dRubarGridAreteSource, Boolean> createDATGridFromMAIFile(final ProgressionInterface _int, final TrImplementationEditorAbstract _ui,
                                                                                 final TrRubarFileStateMng _mng, final File _maiFile) {
    H2dRubarGridAreteSource datSrc;
    CtuluIOOperationSynthese s;
    final TrRubarFileStateMAI file = new TrRubarFileStateMAI(_mng);
    file.setInitFile(_maiFile);
    _mng.add(file);
    final RubarMAIReader reader = new RubarMAIReader();
    s = reader.read(_maiFile, _int);
    boolean newFormat = reader.isNewFormat();
    // on recupere le nombre de d�cimal pour la nouvelle source
    final int nbDec = reader.getNbDecimal();
    // erreur de lecture on retourne null.
    if (_ui.manageErrorOperationAndIsFatal(s)) {
      _ui.unsetMainMessage();
      return null;
    }
    final H2dRubarGrid g = (H2dRubarGrid) s.getSource();
    // on va creer le fichier dat
    if (s.containsMessages()) {
      s.getAnalyze().clear();
    }
    if (_int != null) {
      _int.setDesc(TrResource.getS("Cr�ation du fichier DAT"));
    }
    datSrc = H2dRubarGrid.createCompleteGridFrom(g, _int, s.getAnalyze(), nbDec);
    if (_ui.manageAnalyzeAndIsFatal(s.getAnalyze())) {
      _ui.unsetMainMessage();
      return null;
    }
    return new Pair<>(datSrc, newFormat);
  }

  private static File findFile(final TrImplementationEditorAbstract _ui, final File _f) {
    if ((_f == null) || (!_f.exists())) {

      final String s = TrResource.getS("Choisir un fichier contenant un maillage");
      final CtuluFileChooserPanel pnFile = new CtuluFileChooserPanel(s);
      pnFile.setInitDir(_f);
      pnFile.setWriteMode(false);
      pnFile.setFileSelectMode(JFileChooser.FILES_ONLY);
      pnFile.setFilter(new FileFilter[]{RubarMAIFileFormat.getInstance().createFileFilter(), RubarDATFileFormat.getInstance().createFileFilter()});
      final CtuluDialogPanel pn = new CtuluDialogPanel() {
        @Override
        public boolean isDataValid() {
          final File f = pnFile.getFile();
          if (f != null && f.exists()) {
            return true;
          }
          setErrorText(TrResource.getS("S�lectionner un fichier maillage au format DAT ou MAI"));
          return false;
        }
      };
      pn.setLayout(new BuBorderLayout());
      pn.add(new BuLabel(s), BuBorderLayout.NORTH);
      pn.add(pnFile, BuBorderLayout.CENTER);
      if (pn.afficheModaleOk(_ui.getParentComponent(), TrResource.getS("Fichier de maillage"))) {
        return pnFile.getFile();
      }
    }
    return _f;
  }

  private static void readDonneesBrutes(final ProgressionInterface _int, final TrImplementationEditorAbstract _ui, final TrRubarFileStateMng _mng,
                                        final String _title, final File _dir, final H2dRubarParameters _p) {
    // FRT
    TrRubarFileBrutesState.readFile(_dir, _title, _p, RubarFROFileFormat.getInstance(), H2dRubarDonneesBrutesMng.FRT_ID, _mng, _ui, _int);
    TrRubarFileBrutesStateNuage.readFile(_dir, _title, _p, RubarFRXFileFormat.getInstance(), H2dRubarDonneesBrutesMng.FRT_ID, _mng, _ui, _int);
    // cot
    TrRubarFileBrutesState.readFile(_dir, _title, _p, RubarCOTFileFormat.getInstance(), H2dRubarDonneesBrutesMng.BATY_ID, _mng, _ui, _int);
    TrRubarFileBrutesStateNuage.readFile(_dir, _title, _p, RubarCOXFileFormat.getInstance(), H2dRubarDonneesBrutesMng.BATY_ID, _mng, _ui, _int);
    // ini
    TrRubarFileBrutesState.readFile(_dir, _title, _p, RubarINIFileFormat.getInstance(), H2dRubarDonneesBrutesMng.INI_ID, _mng, _ui, _int);
    TrRubarFileBrutesStateNuage.readFile(_dir, _title, _p, RubarINXFileFormat.getInstance(), H2dRubarDonneesBrutesMng.INI_ID, _mng, _ui, _int);
    // DIF
    TrRubarFileBrutesState.readFile(_dir, _title, _p, RubarDIOFileFormat.getInstance(), H2dRubarDonneesBrutesMng.DIF_ID, _mng, _ui, _int);
    TrRubarFileBrutesStateNuage.readFile(_dir, _title, _p, RubarDIXFileFormat.getInstance(), H2dRubarDonneesBrutesMng.DIF_ID, _mng, _ui, _int);
  }

  /**
   * @param _f un fichier du projet
   * @param _int la barre de progress
   * @param _ui l'impl parente
   * @return le projet correctement intialise ou nul si erreur fatale.
   */
  public static TrRubarProject loadProject(final File _f, final ProgressionInterface _int, final TrImplementationEditorAbstract _ui) {
    if ((_f == null) || (!_f.exists())) {
      return null;
    }
    if (_ui.getLauncher().findImplWithOpenedFile(_f) != null) {
      _ui.error(TrLib.getMessageAlreadyOpen(_f));
      BuLib.invokeLater(new Runnable() {
        @Override
        public void run() {
          _ui.getLauncher().findImplWithOpenedFile(_f).getFrame().toFront();
        }
      });
    }
    _ui.setMainProgression(0);
    // titre et rep
    final String title = CtuluLibFile.getSansExtension(_f.getName());
    _ui.setMainMessage(BuResource.BU.getString("Ouverture") + CtuluLibString.ESPACE + title);
    final File dir = _f.getParentFile();
    // //DAT FILE// indispensable
    final File datFile = RubarDATFileFormat.getInstance().getFileFor(dir, title);
    if (!datFile.exists()) {
      _ui.error(datFile.getName(), TrResource.getS("Le fichier n'existe pas"), false);
      return null;
    }

    // fichier dat
    final FileFormatVersionInterface dateFileFormatVersion = RubarDATFileFormat.getInstance().getLastVersionInstance(datFile);
    final RubarDATReader reader = (RubarDATReader) dateFileFormatVersion.createReader();
    CtuluIOOperationSynthese s = reader.read(datFile, _int);
    reader.setProgressReceiver(null);

    boolean newFormat = reader.isNewFormat();
    int nbBlockTransport = H2dRubarBcMng.NEW_TRANSPORT_PROJECT_DEFAULT_BLOCK;
    _ui.manageAnalyzeAndIsFatal(s.getAnalyze());
    if (s.containsSevereError()) {
      return null;
    }
    final H2dRubarGridAreteSource arete = (H2dRubarGridAreteSource) s.getSource();
    // //DAT FILE FIN//

    // //MAI FILE// juste pour copier ce fichier s'il existe
    final File maiFile = RubarMAIFileFormat.getInstance().getFileFor(dir, title);
    _ui.setMainProgression(30);
    // //FICHIER PAR// on determine le type de projet ici
    final File parFile = RubarPARFileFormat.getInstance().getFileFor(dir, title);
    s = RubarPARFileFormat.getInstance().getLastVersionInstance(parFile).read(parFile, _int);
    _ui.manageAnalyzeAndIsFatal(s.getAnalyze());
    if (s.containsSevereError()) {
      return null;
    }
    final TIntObjectHashMap m = (TIntObjectHashMap) s.getSource();
    final boolean isTrans = H2DRubarDicoParams.isTransport(m);
    if (isTrans) {
      nbBlockTransport = H2DRubarDicoParams.getNbTransportBlock(m);
    }
    final H2DRubarDicoParams dicoP = RubarPARFileFormat.createParams(m);
    // //FICHIER PAR FIN//

    // // FICHIER CLI
    final File cliFile = RubarCLIFileFormat.getInstance().getFileFor(dir, title);
    H2dRubarTimeConditionInterface[] idxTimeCL = null;
    if (cliFile.exists()) {
      s = RubarCLIFileFormat.getInstance().getLastVersionInstance(cliFile).read(cliFile, _int);
      _ui.manageAnalyzeAndIsFatal(s.getAnalyze());
      idxTimeCL = (H2dRubarTimeConditionInterface[]) s.getSource();
    }

    // // FICHIER CLI FIN
    _ui.setMainProgression(50);
    // // FICHIER TAR
    TIntObjectHashMap tarData = null;
    final File tarFile = RubarTARFileFormat.getInstance().getFileFor(dir, title);
    if (tarFile.exists()) {
      s = RubarTARFileFormat.getInstance().read(tarFile, _int);
      _ui.manageAnalyzeAndIsFatal(s.getAnalyze());
      if (!s.containsSevereError()) {
        tarData = (TIntObjectHashMap) s.getSource();
      }
    }
    final int nbElt = arete.getGrid().getEltNb();
    // //FIN FICHIER TAR
    // les solutions initiales : on s'en sert pour savoir si le projet est du type transport
    // // FICHIER CIN

    // //on construit les parametres
    final H2dRubarProjetType type = isTrans ? H2dRubarProjetType.TRANSPORT : H2dRubarProjetType.COURANTOLOGIE_2D;
    CtuluAnalyze an = new CtuluAnalyze();

    // le fichier cli
    final boolean[] cliChanged = new boolean[1];

    final H2dRubarParameters parameter = H2dRubarParameters
        .createParameters(type, arete, dicoP, idxTimeCL, tarData, _int, an, cliChanged, newFormat ? H2dRubarFileType.NEW_FORMAT_9_DIGITS : H2dRubarFileType.OLD_FORMAT_6_DIGITS,
            nbBlockTransport);
    if (_ui.manageAnalyzeAndIsFatal(an)) {
      return null;
    }
    an.clear();
    // Mise a jour fichiers ....
    final TrRubarFileStateMng mng = new TrRubarFileStateMng();

    // CLI
    final TrRubarFileStateCLI stateCli = new TrRubarFileStateCLI(mng, parameter);
    if (cliFile.exists()) {
      stateCli.setInitFile(cliFile);
    }
    mng.add(stateCli);
    stateCli.setModified(cliChanged[0]);

    // dat filestate
    final TrRubarFileStateDAT datState = new TrRubarFileStateDAT(mng, parameter);
    datState.setInitFile(datFile);
    mng.add(datState);

    // mai filestate
    if (maiFile.exists()) {
      final TrRubarFileStateMAI file = new TrRubarFileStateMAI(mng);
      file.setInitFile(maiFile);
      mng.add(file);
    }

    // tar filestate
    final TrRubarFileStateTAR stateTar = new TrRubarFileStateTAR(mng, parameter);
    if (tarFile.exists()) {
      stateTar.setInitFile(tarFile);
    } else if (parameter.getBcMng().getTarageMng().getNbEvolution() > 0) {
      //some discharge evolutions have been created by default
      stateTar.setModified(true);
    }

    _ui.setMainProgression(70);
    // par filestate

    final TrRubarFileStatePAR statePar = new TrRubarFileStatePAR(mng, parameter);
    if (parFile.exists()) {
      statePar.setInitFile(parFile);
    }
    mng.add(statePar);

    // apport fluvial
    final RubarAPPFileFormat fmt = new RubarAPPFileFormat();
    final File appFile = fmt.getFileFor(dir, title);
    if (appFile.exists()) {
      s = fmt.read(appFile, _int, nbElt);
      if (!_ui.manageErrorOperationAndIsFatal(s)) {
        // creation apport
        parameter.createAppMng((H2dRubarAppInterface) s.getSource());
      }
    }
    final TrRubarFileStateAPP app = new TrRubarFileStateAPP(mng, parameter);
    if (appFile.exists()) {
      app.setInitFile(appFile);
    }
    // vent
    final RubarVENFileFormat ventFmt = new RubarVENFileFormat();
    final File ventFile = ventFmt.getFileFor(dir, title);
    if (ventFile.exists()) {
      s = ventFmt.read(ventFile, _int, nbElt);
      if (!_ui.manageErrorOperationAndIsFatal(s)) {
        parameter.createVentMng((H2dRubarVentInterface) s.getSource());
      }
    }
    TrRubarFileStateVEN vent = new TrRubarFileStateVEN(mng, parameter);
    if (ventFile.exists()) {
      vent.setInitFile(ventFile);
    }

    // fichier dtr
    final RubarDTRFileFormat dtrFmt = new RubarDTRFileFormat();
    File dtrFile = dtrFmt.getFileFor(dir, title);
    boolean modified = false;
    if (!dtrFile.exists()) {
      dtrFile = dtrFmt.getFileExistFor(dir, title);
    }
    if (dtrFile != null) {
      s = dtrFmt.read(dtrFile, _int);
      if (!_ui.manageErrorOperationAndIsFatal(s)) {
        if (CtuluLibMessage.DEBUG) {
          CtuluLibMessage.debug("limni read from file");
        }
        an.clear();
        parameter.createLimniMng((H2dRubarDTRResult) s.getSource(), an);
        if (an.containsWarnings()) {
          modified = true;
          CtuluAnalyzeGUI.showDialogErrorFiltre(an, _ui, H2dResource.getS("Limnigramme"), false);
        }
      }
    }
    final TrRubarFileStateDTR dtr = new TrRubarFileStateDTR(mng, parameter);
    if (modified) {
      dtr.setModified(true);
    }
    if (dtrFile != null) {
      dtr.setInitFile(dtrFile);
    }

    // Ouvrages
    final RubarOUVFileFormat ouvFmt = new RubarOUVFileFormat();
    File ouvFile = ouvFmt.getFileFor(dir, title);
    if (!ouvFile.exists()) {
      ouvFile = ouvFmt.getFileExistFor(dir, title);
    }
    boolean ouvrageModifie = false;
    if (ouvFile != null) {
      s = ouvFmt.read(ouvFile, _int);
      if (!_ui.manageErrorOperationAndIsFatal(s)) {
        if (CtuluLibMessage.DEBUG) {
          CtuluLibMessage.debug("ouvrage read from file");
        }
        an = new CtuluAnalyze();
        an.setDesc(H2dResource.getS("Ouvrages"));
        parameter.initOuvrageMng((H2dRubarOuvrageContainer) s.getSource(), an);
        if (!an.isEmpty() && (an.containsWarnings() || an.containsErrorOrFatalError())) {
          CtuluAnalyzeGUI.showDialogErrorFiltre(an, _ui, TrLib.getString("La liste des ouvrages a �t� modifi�e"), false);
          ouvrageModifie = true;
        }
      }
    }
    final TrRubarFileStateOUV ouv = new TrRubarFileStateOUV(mng, parameter.getOuvrageMng(), parameter.getBcMng());
    if (ouvFile != null) {
      ouv.setInitFile(ouvFile);
    }
    ouv.setModified(ouvrageModifie);
    // frottement et cin : dialogue pour valeurs par defaut
    final File cinFile = RubarCINFileFormat.getInstance().getFileFor(dir, title);
    H2DRubarSolutionsInitialesInterface initialSolutions = null;

    if (cinFile.exists()) {
      s = RubarCINFileFormat.getInstance().read(cinFile, _int, nbElt, isTrans ? nbBlockTransport : -1);
      if (!_ui.manageAnalyzeAndIsFatal(s.getAnalyze())) {
        initialSolutions = (H2DRubarSolutionsInitialesInterface) s.getSource();
      }
    }

    final File frtFile = RubarFRTFileFormat.getInstance().getFileFor(dir, title);
    boolean frModified = false;
    boolean siModified = false;
    CtuluCollectionDoubleEdit val = null;
    if (frtFile.exists()) {
      s = RubarFRTFileFormat.getInstance().read(frtFile, _int, arete.getGrid().getEltNb());
      if (!_ui.manageAnalyzeAndIsFatal(s.getAnalyze())) {
        val = (CtuluArrayDouble) s.getSource();
      }
    }
    if (val == null || initialSolutions == null) {
      final TrRubarNewProjectPanel pn = new TrRubarNewProjectPanel(type, nbElt, val == null, initialSolutions == null, true, newFormat, nbBlockTransport);
      final int res = pn.afficheModale(_ui.getImplementation().getFrame(), CtuluDialog.OK_OPTION);
      if (!CtuluDialogPanel.isOkResponse(res)) {
        return null;
      }
      if (val == null) {
        val = pn.getFrt();
        frModified = true;
      }
      if (initialSolutions == null) {
        initialSolutions = pn.createSolutionInitales();
        siModified = true;
      }
    }
    // /FROTTEMENT

    if (val == null) {
      // ne doit jamais arriver
      parameter.setDefaultFriction();
      frModified = true;
      CtuluLibMessage.info(TrResource.getS("Des coefficients de frottement par d�faut seront utilis�s (=0)"));
    } else {
      parameter.setFriction(val);
      if (parameter.checkFriction()) {
        frModified = true;
      }
    }
    final TrRubarFileStateFRT frt = new TrRubarFileStateFRT(mng, parameter);
    if (frtFile.exists()) {
      frt.setInitFile(frtFile);
    }
    frt.setModified(frModified);
    mng.add(frt);

    // diffusion
    final File dif = RubarDIFFileFormat.getInstance().getFileExistFor(dir, title);
    if (dif != null) {
      s = RubarDIFFileFormat.getInstance().read(dif, _int, nbElt);
      if (!_ui.manageErrorOperationAndIsFatal(s)) {
        parameter.createDiffusion(((CtuluCollectionDoubleEdit) s.getSource()).getValues(), null);
      }
    }
    new TrRubarFileStateDIF(mng, parameter).setInitFile(dif);

    // cin filestate
    final TrRubarFileStateCIN cinState = new TrRubarFileStateCIN(mng, parameter);
    mng.add(cinState);
    cinState.setInitFile(cinFile);
    if (initialSolutions == null) {
      // ne doit jamais arriver
      parameter.setDefaultSI();
      cinState.setModified(true);
    } else {
      parameter.setSI(initialSolutions);
      if (parameter.checkSI()) {
        CtuluLibMessage.info(TrResource.getS("Des conditions initiales par d�faut seront utilis�s (=0)"));
        cinState.setModified(true);
      } else {
        cinState.setModified(siModified);
      }
    }
    File dur = null;
    File sed = null;
    if (parameter.getProjetType().equals(H2dRubarProjetType.TRANSPORT)) {
      dur = RubarDURFileFormat.getInstance().getFileExistFor(dir, title);
      if (dur != null) {
        s = RubarDURFileFormat.getInstance().read(dur, _int, parameter.getGridVolume().getPtsNb());
        if (!_ui.manageErrorOperationAndIsFatal(s)) {
          parameter.setFondDur(((RubarDURResult) s.getSource()).getzValues());
        }
      }

      RubarSEDFileFormat sedFileFormat = new RubarSEDFileFormat();
      sed = sedFileFormat.getFileExistFor(dir, title);
      if (sed != null) {
        s = sedFileFormat.read(sed, _int, parameter.getGridVolume().getPtsNb());
        if (!_ui.manageErrorOperationAndIsFatal(s)) {
          parameter.initSedimentMng(((H2dRubarSedimentInterface) s.getSource()));
        }
      }
    }
    TrRubarFileStateDUR durState = new TrRubarFileStateDUR(mng, parameter);
    durState.setInitFile(dur);

    TrRubarFileStateSED sedState = new TrRubarFileStateSED(mng, parameter);
    sedState.setInitFile(sed);

    mng.setProjectName(title);
    readDonneesBrutes(_int, _ui, mng, title, dir, parameter);
    _ui.setMainProgression(90);
    arete.getGrid().computeBord(_int, an);
    final TrRubarProject proj = new TrRubarProject(new TrRubarParameters(parameter, title, dir, _ui), mng);
    proj.setNewFormatForNumberOfDigits(newFormat, false, null);
    final TrRubarProjectDispatcherListener lis = new TrRubarProjectDispatcherListener();
    parameter.setListener(lis);
    lis.setProj(proj);
    _int.setDesc(BuResource.BU.getString("Ouverture..."));

    //to be sure that the blocks number is the same in all data ( donnees brute, boundary conditions and initial solution) :
    parameter.updateConcentrationBlocks(nbBlockTransport, null);

    _ui.unsetMainMessage();
    _ui.unsetMainProgression();
    mng.setState(proj.getState());
    return proj;
  }
}
