/**
 * @file TrBordLegendePanel.java
 * @creation 8 oct. 2003
 * @modification $Date: 2006-11-14 09:08:07 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import java.util.Iterator;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.ebli.calque.BCalqueLegendePanel;
import org.fudaa.ebli.trace.TraceLigneVisuComponent;
import org.fudaa.fudaa.tr.rubar.TrRubarBcAreteLayer;

/**
 * @author deniger
 * @version $Id: TrBcBoundaryLegendPanel.java,v 1.12 2006-11-14 09:08:07 deniger Exp $
 */
public class TrBcBoundaryLegendPanel extends BCalqueLegendePanel {

  /**
   * @param _cqBord le bord en question
   */
  public TrBcBoundaryLegendPanel(final TrBcBoundaryLayerAbstract _cqBord) {
    super(_cqBord, _cqBord.getTitle());
    init(_cqBord);
  }

  /**
   * @param _cqBord le bord en question
   */
  public TrBcBoundaryLegendPanel(final TrRubarBcAreteLayer _cqBord) {
    super(_cqBord, _cqBord.getTitle());
    init(_cqBord);
  }

  /**
   * @param _cqBord
   */
  public final void init(final TrBcBoundaryLayer _cqBord) {
    JPanel c = (JPanel) getMainComponent();
    if (c == null) {
      c = new JPanel();
      c.setOpaque(false);
      c.setBorder(BorderFactory.createEmptyBorder(3, 3, 1, 3));
      c.setLayout(new BuGridLayout(2, 5, 2));
      super.add(c, BuBorderLayout.CENTER);
    }
    c.removeAll();

    for (final Iterator it = _cqBord.getUsedBoundaryType().iterator(); it.hasNext();) {
      final H2dBoundaryType b = (H2dBoundaryType) it.next();
      final BuLabel lb = new BuLabel(b.getName());
      lb.setOpaque(false);
      lb.setFont(getFont());
      c.add(lb);
      c.add(new TraceLigneVisuComponent(_cqBord.getTlData(b)));
    }
  }
}