/*
 * @creation 16 d�c. 2004
 * @modification $Date: 2007-06-20 12:23:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * Un lecteur pour reflux et refonde.
 * 
 * @author Fred Deniger
 * @version $Id: TrPostSourceReflux.java,v 1.17 2007-06-20 12:23:39 deniger Exp $
 */
public class TrPostSourceReflux extends TrPostSourceFromReader {


  // public TrPostInspectorReader createWatcher(final TrPostProjet _proj, final boolean _auto) {
  // return new TrPostInspectorReaderReflux(_proj, this);
  // }

  public TrPostSourceReflux(final TrPostSourceReaderInterface _reader, final String _titre, final EfGridInterface _g,
      final CtuluUI _impl) {
    super(_reader, _titre, _g, _impl);
  }

}