/*
 * @creation 1 f�vr. 07
 * @modification $Date: 2007-05-04 13:59:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuVerticalLayout;
import javax.swing.ButtonGroup;
import javax.swing.JInternalFrame;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfFilter;
import org.fudaa.fudaa.commun.FudaaInternalFrameSelectorPanel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.meshviewer.MvResource;

class MvVolumePanel extends CtuluDialogPanel {
  private final FudaaInternalFrameSelectorPanel pn_;
  final EfFilter proposed_;
  final BuRadioButton rdAll_;

  public MvVolumePanel(final EfFilter _proposed, final FudaaCommonImplementation _impl, final int _nbTime) {
    super(false);
    pn_ = _nbTime > 1 ? new FudaaInternalFrameSelectorPanel(MvProfileVolumeBuilder.getInternalFrames(_impl),
        MvResource.getS("Afficher les courbes de volume dans une nouvelle fen�tre"), MvProfileFlowrateFille
            .getCommonTitle(_impl)) : null;

    rdAll_ = new BuRadioButton(MvResource.getS("Calculer le volume sur tout le domaine"));
    final BuRadioButton rdSelection = new BuRadioButton(MvResource
        .getS("Calculer le volume sur la s�lection uniquement"));
    final ButtonGroup bg = new ButtonGroup();
    bg.add(rdAll_);
    bg.add(rdSelection);
    proposed_ = _proposed;

    if (proposed_ == null) {
      // pas de filtre par d�faut
      rdSelection.setEnabled(false);
      rdAll_.setSelected(true);
    } else {
      rdSelection.setSelected(true);
    }
    final BuPanel pnSelection = new BuPanel();
    pnSelection.setLayout(new BuVerticalLayout(3));
    pnSelection.add(rdSelection);
    pnSelection.add(rdAll_);
    pnSelection.setBorder(CtuluLibSwing.createTitleBorder(DodicoLib.getS("Calcul du volume")));
    if (pn_ != null) {
      pn_.setBorder(CtuluLibSwing.createTitleBorder(CtuluLib.getS("Fen�tre cible")));
    }
    setLayout(new BuVerticalLayout(7));
    setBorder(BuBorders.EMPTY3333);
    add(pnSelection);
    if (_nbTime > 1) {
      add(pn_);
    }
  }

  EfFilter getSelectedFilter() {
    return rdAll_.isSelected() ? null : proposed_;
  }

  public JInternalFrame getSelectedFrame() {
    return pn_ == null ? null : pn_.getSelectedFrame();
  }

  public String getNewFrameTitle() {
    return pn_ == null ? null : pn_.getNewFrameTitle();
  }
}