/**
 * @creation 8 d�c. 2004
 * @modification $Date: 2007-06-05 09:01:14 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacWeirLayerDisplay.java,v 1.12 2007-06-05 09:01:14 deniger Exp $
 */
public final class TrTelemacWeirLayerDisplay extends CtuluDialogPanel implements ItemListener {

  BuCheckBox[] cb_;
  BuTextField ft_;
  TrTelemacWeirLayer layer_;

  public TrTelemacWeirLayerDisplay(final TrTelemacWeirLayer _l) {
    super(true);
    layer_ = _l;
    setLayout(new BuVerticalLayout());
    cb_ = new BuCheckBox[3];
    cb_[0] = new BuCheckBox(TrResource.getS("Afficher les limites des seuils"));
    cb_[0].setToolTipText("<html>" + TrResource.getS("Affiche \"1\" au d�but du seuil.<br>Affiche \"2\" � la fin.")
        + "</html>");
    cb_[0].setSelected(layer_.isAfficheDebFin());
    add(cb_[0]);
    cb_[1] = new BuCheckBox(TrResource
        .getS("Dessiner en rouge les seuils ne respectant pas l'ordre des noeuds fronti�res"));
    cb_[1]
        .setToolTipText("<html>"
            + TrResource
                .getS("Si un seuil contient un cot� dont les noeuds ne sont pas contigus sur la fronti�re, <br>il sera dessin� en rouge")
            + "</html>");
    add(cb_[1]);
    cb_[1].setSelected(layer_.isTraceSeuilWithBadIdx());
    cb_[2] = new BuCheckBox(TrResource
        .getS("Marquer par une croix rouge les segments correspondants d'un seuil ayant une longueur diff�rente"));
    final BuPanel pn = new BuPanel();
    pn.setLayout(new BuHorizontalLayout(2, false, false));
    pn.add(cb_[2]);
    final int pourc = layer_.getPourcentageErreur();
    cb_[2].setSelected(pourc >= 0);
    ft_ = BuTextField.createIntegerField();
    ft_.setColumns(3);
    ft_.setValueValidator(BuValueValidator.MIN(0));
    ft_.setEditable(cb_[2].isSelected());
    ft_.setEnabled(ft_.isEditable());
    pn.add(ft_);
    pn.add(new BuLabel("% (" + TrResource.getS("Erreur autoris�e") + ')'));
    ft_.setText(CtuluLibString.getString(ft_.isEnabled() ? pourc : 5));
    add(pn);
    cb_[2].setToolTipText(getHelp());
    cb_[2].addItemListener(this);
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    ft_.setEditable(cb_[2].isSelected());
    ft_.setEnabled(ft_.isEditable());
  }

  protected String getHelp() {
    if (CtuluLib.isFrenchLanguageSelected()) { return "<html>Les noeuds des seuils doivent <br>"
        + "se correspondre 2 � 2 et les distances entre les noeuds<br> doivent �tre les m�mes "
        + "des deux c�t�s. <br>Les segments de longueur diff�rentes seront marqu�s.</html>"; }
    return "<html>The distance between two nodes <br>should be the same on both sides. "
        + "<br>The segments with different length will be marked.</html>";
  }

  @Override
  public boolean apply() {
    layer_.setAfficheDebFin(cb_[0].isSelected());
    layer_.setTraceSeuilWithBadIdx(cb_[1].isSelected());
    if (cb_[2].isSelected()) {
      layer_.setPourcentageErreur(Integer.parseInt(ft_.getText()));
    } else {
      layer_.setPourcentageErreur(-1);
    }
    layer_.repaint();
    return true;
  }

  @Override
  public boolean isDataValid() {
    final String txt = ft_.getText().trim();
    if (txt.length() > 0 && Integer.parseInt(txt) > 0) { return true; }
    setErrorText(TrResource.getS("Pr�ciser un pourcentage positif"));
    return false;
  }
}
