/**
 * @creation 13 oct. 2004
 * @modification $Date: 2007-04-30 14:22:40 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import java.io.File;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.dico.DicoParamsListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.rubar.io.RubarPARFileFormat;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFileStatePAR.java,v 1.10 2007-04-30 14:22:40 deniger Exp $
 */
public class TrRubarFileStatePAR extends TrRubarFileState implements DicoParamsListener {

  /**
   * @param _l le listener
   */
  public TrRubarFileStatePAR(final TrRubarFileStateListener _l, final H2dRubarParameters _p) {
    super(RubarPARFileFormat.getInstance(), _l);
    _p.getDicoParams().addModelListener(this);
  }

  @Override
  public void dicoParamsEntiteAdded(final DicoParams _cas, final DicoEntite _ent) {
    setModified(true);
  }

  @Override
  public void dicoParamsEntiteCommentUpdated(final DicoParams _cas, final DicoEntite _ent) {}

  @Override
  public void dicoParamsEntiteRemoved(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    setModified(true);
  }

  @Override
  public void dicoParamsEntiteUpdated(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    setModified(true);
  }

  public void dicoParamsProjectModifyStateChanged(final DicoParams _cas) {}

  public void dicoParamsStateLoadedEntiteChanged(final DicoParams _cas, final DicoEntite _ent) {}

  @Override
  public void dicoParamsValidStateEntiteUpdated(final DicoParams _cas, final DicoEntite _ent) {}

  @Override
  public void dicoParamsVersionChanged(final DicoParams _cas) {
    setModified(true);
  }

  @Override
  public CtuluIOOperationSynthese save(final File _dir, final String _projectName,
      final ProgressionInterface _progression, final TrRubarProject _projet) {
    return RubarPARFileFormat.getInstance().getLastVersionImpl().write(
        RubarPARFileFormat.getInstance().getFileFor(_dir, _projectName),
        RubarPARFileFormat.transform(_projet.getDicoParams()), _progression);
  }

}