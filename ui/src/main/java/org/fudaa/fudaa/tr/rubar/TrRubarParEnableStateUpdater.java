package org.fudaa.fudaa.tr.rubar;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.dodico.dico.DicoCasFileFormatVersionAbstract;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.dico.DicoParamsListener;
import org.fudaa.dodico.h2d.rubar.H2DRubarDicoModelTransport;
import org.fudaa.fudaa.fdico.FDicoEntiteTableModel;

/**
 * Used to update to table model of the entities
 * 
 * @author genesis
 */
public class TrRubarParEnableStateUpdater implements DicoParamsListener {

  private final FDicoEntiteTableModel dicoModel;

  public TrRubarParEnableStateUpdater(FDicoEntiteTableModel dicoModel) {
    this.dicoModel = dicoModel;
  }

  @Override
  public void dicoParamsEntiteAdded(DicoParams _cas, DicoEntite _ent) {
    updateStates(_cas);
  }

  @Override
  public void dicoParamsEntiteRemoved(DicoParams _cas, DicoEntite _ent, String _oldValue) {
    updateStates(_cas);
  }

  private void updateStates(DicoParams params) {
    boolean changed = false;
    Map entiteValues = params.getEntiteValues();
    Set<DicoEntite> keySet = entiteValues.keySet();
    DicoCasFileFormatVersionAbstract version = params.getDicoFileFormatVersion();
    List<DicoEntite> entites = params.getDico().getEntites();
    for (DicoEntite entity : entites) {
      boolean modifiable = H2DRubarDicoModelTransport.isModifiable(entity, entiteValues, version);
      if (modifiable != entity.isModifiable()) {
        changed = true;
        entity.setModifiable(modifiable);
      }
    }
    if (changed) {
      dicoModel.fireTableDataChanged();
    }

  }

  @Override
  public void dicoParamsEntiteUpdated(DicoParams _cas, DicoEntite _ent, String _oldValue) {
    updateStates(_cas);

  }

  @Override
  public void dicoParamsEntiteCommentUpdated(DicoParams _cas, DicoEntite _ent) {

  }

  @Override
  public void dicoParamsValidStateEntiteUpdated(DicoParams _cas, DicoEntite _ent) {

  }

  @Override
  public void dicoParamsVersionChanged(DicoParams _cas) {

  }

}
