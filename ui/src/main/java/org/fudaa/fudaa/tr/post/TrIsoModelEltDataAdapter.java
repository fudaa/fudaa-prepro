/*
 * @creation 25 avr. 07
 * 
 * @modification $Date: 2007-06-11 13:08:18 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuTable;
import org.locationtech.jts.geom.Envelope;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.operation.EfIsoRestructuredGridResult;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.layer.MvIsoModelInterface;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;

/**
 * @author fred deniger
 * @version $Id: TrIsoModelEltDataAdapter.java,v 1.4 2007-06-11 13:08:18 deniger Exp $
 */
public class TrIsoModelEltDataAdapter implements MvIsoModelInterface {

  final EfIsoRestructuredGridResult res_;
  EfData currentNodeData_;
  final TrIsoModelAbstract support_;

  public TrIsoModelEltDataAdapter(final EfIsoRestructuredGridResult _res, final TrIsoModelAbstract _support) {
    super();
    res_ = _res;
    support_ = _support;
  }

  public void setCurrentEltData(final CtuluVariable _var, final int _tidx, final EfData _currentEltData) {
    currentNodeData_ = res_
        .getNewValues(_var, _tidx, _currentEltData, support_.s_, support_.s_.getInterpolator().getVect(), null, new CtuluAnalyze());
  }

  @Override
  public EfGridInterface getGrid() {
    return support_.getGrid();
  }

  @Override
  public int getNbElt() {
    return res_.getNewGrid().getEltNb();
  }

  @Override
  public double getDatatFor(int idxElt, int idxPtOnElt) {
    return currentNodeData_.getValue(res_.getNewGrid().getElement(idxElt).getPtIndex(idxPtOnElt));
  }

  @Override
  public double[] fillWithData(final int _idxElement, final double[] _l) {
    return TrIsoModelAbstract.fillWithData(res_.getNewGrid().getElement(_idxElement), currentNodeData_, _l);
  }

  public boolean isAccepted(final int _i) {
    final EfElement elt = res_.getNewGrid().getElement(_i);
    for (int i = elt.getPtNb() - 1; i >= 0; i--) {
      final int idx = elt.getPtIndex(i);
      if (res_.getNewGrid().isPtAddedForEltCenter(idx) && !support_.isAccepted(res_.getNewGrid().getOltEltIdxForAddedPt(idx), false)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public void polygone(final GrPolygone _p, final int _i, final boolean _force) {
    if (_force || isAccepted(_i)) {
      MvElementModelDefault.polygone(res_.getNewGrid(), _p, _i);
    }
  }

  @Override
  public boolean isPainted(int idxElt) {
    return isAccepted(idxElt);
  }

  @Override
  public GrPoint getVertexForObject(int ind, int idVertex) {
    return new GrPoint(getX(ind, idVertex), getY(ind, idVertex), 0);
  }

  @Override
  public double getX(int idxElt, int idxPtInElt) {
    int idxPt = res_.getNewGrid().getElement(idxElt).getPtIndex(idxPtInElt);
    return res_.getNewGrid().getGrid().getPtX(idxPt);
  }

  @Override
  public double getY(int idxElt, int idxPtInElt) {
    int idxPt = res_.getNewGrid().getGrid().getElement(idxElt).getPtIndex(idxPtInElt);
    return res_.getNewGrid().getGrid().getPtY(idxPt);
  }

  @Override
  public Envelope getEnvelopeForElement(int idxElt) {
    return res_.getNewGrid().getEnvelopeForElement(idxElt);
  }

  @Override
  public int getNbPt(int idxElt) {
    return res_.getNewGrid().getElement(idxElt).getPtNb();
  }

  @Override
  public boolean getCentre(GrPoint pt, int idx, boolean force) {
    if (force || isAccepted(idx)) {
      return MvElementModelDefault.cente(res_.getNewGrid(), pt, idx);
    }
    return false;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    return null;
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
  }

  @Override
  public GrBoite getDomaine() {
    return support_.getDomaine();
  }

  @Override
  public int getNombre() {
    return res_.getNewGrid().getPtsNb();
  }

  @Override
  public Object getObject(final int _ind) {
    return null;
  }

  @Override
  public boolean isValuesTableAvailable() {
    return false;
  }
}
