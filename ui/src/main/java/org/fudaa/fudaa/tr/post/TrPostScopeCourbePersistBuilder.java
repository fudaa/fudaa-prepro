package org.fudaa.fudaa.tr.post;

import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.courbe.EGCourbePersistBuilder;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGModel;

/**
 * @author deniger
 */
public class TrPostScopeCourbePersistBuilder extends EGCourbePersistBuilder<TrPostScopeCourbe> {

  @Override
  protected TrPostScopeCourbe createEGObject(EGCourbePersist target, Map params, CtuluAnalyze log) {
    EGGroup parent = getGroup(params);
    EGModel createModel = createModel(target, params);
    if (createModel == null) {
      return null;
    }
    double[] times = new double[createModel.getNbValues()];
    for (int i = 0; i < times.length; i++) {
      times[i] = createModel.getX(i);
    }
    return new TrPostScopeCourbe(parent, createModel);
  }

}
