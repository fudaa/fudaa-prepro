/*
 * @creation 14 mars 07
 * @modification $Date: 2007-04-30 14:22:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.dodico.h2d.H2dEvolutionUseCounter;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author fred deniger
 * @version $Id: TrCourbeUseCounter.java,v 1.2 2007-04-30 14:22:42 deniger Exp $
 */
public class TrCourbeUseCounter implements H2dEvolutionUseCounter {

  final String domain_;

  public static class Detail {

    Map varInt_ = new HashMap();

    protected void add(final H2dVariableType _t, final int _idx) {
      TIntArrayList list = (TIntArrayList) varInt_.get(_t);
      if (list == null) {
        list = new TIntArrayList();
        varInt_.put(_t, list);
      }
      list.add(_idx);

    }

    public int[] getSelectedIdx(final CtuluListSelection _res) {
      final TIntHashSet set = new TIntHashSet(getCount());
      for (final Iterator it = varInt_.values().iterator(); it.hasNext();) {
        final TIntArrayList integers = (TIntArrayList) it.next();
        for (int i = integers.size() - 1; i >= 0; i--) {
          set.add(integers.getQuick(i));
        }
      }
      final int[] res = set.toArray();
      Arrays.sort(res);
      return res;
    }

    public Collection getVars() {
      return varInt_.keySet();
    }

    public int getCount() {
      int res = 0;
      for (final Iterator it = varInt_.values().iterator(); it.hasNext();) {
        res += ((TIntArrayList) it.next()).size();
      }
      return res;
    }

  }

  Map evolDetail_ = new HashMap(20);

  EvolutionReguliereInterface evolToTest_;

  /**
   * Permet de filtrer les courbes.
   * Allow to filter the curves
   * @param _evol
   * @return true si on peut traiter cette courbe / true if we can deal this curve
   */
  public boolean isAccepted(final EvolutionReguliereInterface _evol) {
    if (evolToTest_ != null) { return evolToTest_ == _evol; }
    return true;
  }

  public boolean isUsed(final EvolutionReguliereInterface _evol) {
    return evolDetail_.containsKey(_evol);
  }

  public int getNbEvol() {
    return evolDetail_.size();
  }

  public Collection getVariablesUsing(final EvolutionReguliereInterface _evol) {
    final Detail res = getDetail(_evol);
    if (res == null) { return Collections.EMPTY_LIST; }
    return res.getVars();
  }

  public int[] getIdxSelected(final EvolutionReguliereInterface _evol) {
    final Detail res = getDetail(_evol);
    if (res == null) { return null; }
    return res.getSelectedIdx(null);
  }

  public int getUsedCount(final EvolutionReguliereInterface _evol) {
    final Detail res = getDetail(_evol);
    if (res == null) { return 0; }
    return res.getCount();
  }

  public Set getEvols() {
    return evolDetail_.keySet();
  }

  @Override
  public void add(final EvolutionReguliereInterface _evol, final H2dVariableType _t, final int _idxGlobal) {
    if (!isAccepted(_evol)) { return; }
    Detail res = getDetail(_evol);
    if (res == null) {
      res = new Detail();
      evolDetail_.put(_evol, res);
    }
    res.add(_t, _idxGlobal);

  }

  private Detail getDetail(final EvolutionReguliereInterface _evol) {
    return (Detail) evolDetail_.get(_evol);
  }

  public EvolutionReguliereInterface getEvolToTest() {
    return evolToTest_;
  }

  public void setEvolToTest(final EvolutionReguliereInterface _evolToTest) {
    evolToTest_ = _evolToTest;
  }

  public TrCourbeUseCounter(final String _domain) {
    super();
    domain_ = _domain;
  }

  public String getDomain() {
    return domain_;
  }

}
