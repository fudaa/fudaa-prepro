/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuEmptyArrays;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author deniger
 */
public class TrPostSourceFromReader extends TrPostSourceListVar {

  TrPostSourceReaderInterface reader_;

  /**
   * @param _file
   * @param _titre
   * @param _g
   * @param _time
   * @param _v
   * @param _impl
   */
  public TrPostSourceFromReader(TrPostSourceReaderInterface _reader, String _titre, EfGridInterface _g, CtuluUI _impl) {
    super(_titre, _g, getTimes(_reader), (H2dVariableType[]) _reader.getInitVar().toArray(
        new H2dVariableType[_reader.getInitVar().size()]), _impl);
    reader_ = _reader;
  }

  @Override
  public String getFormatedTitle() {
    if (reader_ instanceof TrPostSourceReaderComposite) { return getTitle(); }
    return super.getFormatedTitle();
  }

  @Override
  public final boolean isVolumique() {
    return reader_.isVolumique();
  }

  public static double[] getTimes(TrPostSourceReaderInterface _init) {
    if (_init == null || _init.getNbTimeStep() == 0) return FuEmptyArrays.DOUBLE0;
    double[] res = new double[_init.getNbTimeStep()];
    for (int i = 0; i < res.length; i++) {
      res[i] = _init.getTimeStep(i);
    }
    return res;

  }

  @Override
  public final void close() {
    super.close();
    reader_.close();
  }

  @Override
  public final String getTitle() {
    if (reader_.getTitle() != null) return reader_.getTitle();
    return title_;
  }

  @Override
  public Collection<File> getFiles() {
    return reader_.getFiles();
  }

  /**
   * @return the reader
   */
  public TrPostSourceReaderInterface getReader() {
    return reader_;
  }

  /**
   * @param _reader the reader to set
   */
  public void setReader(TrPostSourceReaderInterface _reader) {
    reader_ = _reader;
  }

  @Override
  public EfData getInitData(H2dVariableType _varIdx, int _timeIdx) {
    return reader_.getInitData(_varIdx, _timeIdx);
  }

  @Override
  public double getInitData(H2dVariableType _varIdx, int _timeIdx, int _ptIdx) throws IOException {
    return reader_.getInitData(_varIdx, _timeIdx, _ptIdx);
  }

  @Override
  public boolean containsElementVar() {
    return false;
  }

  @Override
  public void fillWithSourceCreationInfo(String _pref, Map _table) {

    if (this.getReader() instanceof TrPostSourceReaderComposite) {
      ((TrPostSourceReaderComposite) getReader()).fillInfosWithComposite(_table);
    } else {
      super.fillWithSourceCreationInfo(_pref, _table);
    }
  }

  @Override
  public String getId() {
    return reader_.getId();
  }

  @Override
  public void setId(String id) {
    reader_.SetId(id);
  }

}
