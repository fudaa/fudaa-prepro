/*
 *  @creation     14 mars 2005
 *  @modification $Date: 2007-04-30 14:22:38 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.ebli.calque.BCalqueSaverSingle;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.fudaa.tr.persistence.TrPostIsoLayerPersistence;

/**
 * @deprecated garde pour ancienne sauvegarde
 * @author Fred Deniger
 * @version $Id: TrIsoLayerSaver.java,v 1.6 2007-04-30 14:22:38 deniger Exp $
 */
@Deprecated
public final class TrIsoLayerSaver extends BCalqueSaverSingle {

  private boolean isBase_;

  /**
   * @deprecated garde pour ancienne sauvegarde
   */
  @Deprecated
  public TrIsoLayerSaver() {
    setPersistenceClass(TrPostIsoLayerPersistence.class.getName());
  }

  @Override
  public EbliUIProperties getUI() {
    final EbliUIProperties ui = super.getUI();
    ui.put(TrPostIsoLayerPersistence.getIsBaseId(), isBase_);
    return ui;
  }

}