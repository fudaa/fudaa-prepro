/*
 * @creation 15 mars 07
 * 
 * @modification $Date: 2007-04-30 14:22:42 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.fudaa.tr.rubar.TrRubarVisuPanel;

/**
 * @author fred deniger
 * @version $Id: TrRubarCourbeUseFinder.java,v 1.3 2007-04-30 14:22:42 deniger Exp $
 */
public class TrRubarCourbeUseFinder extends TrCourbeUseFinderAbstract {

  public TrRubarCourbeUseFinder(final H2dRubarParameters _params, final TrRubarVisuPanel _panel, final EGGrapheModel _graphe) {
    super(_params, _panel, _graphe);
  }

  @Override
  protected TrCourbeUseResultsI createResult() {
    return new TrRubarCourbeUseResults(getPanel());
  }

  @Override
  protected void go(final TrCourbeUseResultsI _res, final ProgressionInterface _prog) {
    final TrRubarCourbeUseResults res = (TrRubarCourbeUseResults) _res;
    final H2dRubarParameters params = (H2dRubarParameters) getParams();
    if (isStop()) {
      return;
    }
    params.getBcMng().iterateOnBcCurves(res.usedInCl_);
    if (isStop()) {
      return;
    }
    params.getAppMng().fillWithTransientCurves(res.usedInApp_);
    if (isStop()) {
      return;
    }
    params.getVentMng().fillWithTransientCurves(res.usedInVent_);

  }

}
