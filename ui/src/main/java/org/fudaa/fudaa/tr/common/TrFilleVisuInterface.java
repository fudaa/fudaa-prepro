/**
 *  @file         TrFilleVisuAbstract.java
 *  @creation     5 mai 2004
 *  @modification $Date: 2006-09-19 15:07:30 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.common;

import org.fudaa.ctulu.CtuluUndoRedoInterface;
import org.fudaa.ctulu.gui.CtuluFilleWithComponent;

/**
 * @author Fred Deniger
 * @version $Id: TrFilleVisuInterface.java,v 1.10 2006-09-19 15:07:30 deniger Exp $
 */
public interface TrFilleVisuInterface extends CtuluUndoRedoInterface, CtuluFilleWithComponent {

  /**
   * Met a jour le composant d'info et les isosurfaces.
   * update the component of news and the isosurfaces 
   */
  void updateInfoComponentAndIso();

  /**
   * Met a jour les variables.
   * update the variables
   */
  void updateIsoVariables();

}
