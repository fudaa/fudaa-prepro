/**
 * @creation 20 janv. 2005
 * @modification $Date: 2007-05-22 14:20:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.*;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.rubar.H2DRubarDicoParams;
import org.fudaa.dodico.h2d.rubar.H2dRubarBcMng;
import org.fudaa.dodico.h2d.rubar.H2dRubarDTRResult;
import org.fudaa.dodico.h2d.rubar.H2dRubarLimniMng;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereFixe;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.rubar.io.RubarDTRReader;
import org.fudaa.dodico.rubar.io.RubarPARFileFormat;
import org.fudaa.dodico.rubar.io.RubarTRCReader;
import org.fudaa.dodico.rubar.io.RubarTRCResult;
import org.fudaa.ebli.courbe.*;
import org.fudaa.ebli.palette.BPalettePlageDefault;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeDefaultModelName;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FilenameFilter;
import java.util.*;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: TrPostRubarLimniMng.java,v 1.26 2007-05-22 14:20:38 deniger Exp $
 */
public class TrPostRubarLimniMng {
  class VarModel extends AbstractListModel {
    void fireDonneesChanged() {
      super.fireIntervalAdded(this, 0, getSize());
    }

    @Override
    public Object getElementAt(final int _index) {
      return availVar_[_index].getName();
    }

    @Override
    public int getSize() {
      return availVar_ == null ? 0 : availVar_.length;
    }
  }

  private H2dVariableType[] availVar_;
  private final Map<H2dVariableType, EvolutionReguliereInterface[]> evols_ = new HashMap<>();
  private H2dRubarLimniMng limnis_;
  private TrPostSource trPostSource;
  private TrPostProjet trPostProjet;
  private VarModel varModel_;

  private boolean buildExtraElevation() {
    if (getEvolution(H2dVariableType.COTE_EAU, 0) != null || getEvolution(H2dVariableType.HAUTEUR_EAU, 0) == null) {
      return false;
    }
    final EvolutionReguliereInterface[] hEvolutions = evols_.get(H2dVariableType.HAUTEUR_EAU);
    EvolutionReguliereInterface[] coteEvolutions = evols_.get(H2dVariableType.COTE_EAU);
    if (hEvolutions != null) {
      for (int idxPt = limnis_.getNbPoint() - 1; idxPt >= 0; idxPt--) {
        final EvolutionReguliereInterface eh = hEvolutions[idxPt];
        if (eh == null) {
          return false;
        }
        final int elt = limnis_.getIdxElt(idxPt);

        final EvolutionReguliere bathy = TrPostCourbeBuilder
            .getEvolOnMesh(trPostSource, elt, H2dVariableType.BATHYMETRIE, eh.getMinX(), eh.getMaxX());
        if (bathy == null) {
          return false;
        }
        final double[] values = new double[eh.getNbValues()];
        for (int itime = values.length - 1; itime >= 0; itime--) {
          // ce test me semble dangereux: mais je ne comprends pas certaines exceptions dues
          // au TRC ...
          if (itime < eh.getNbValues()) {
            values[itime] = bathy.getInterpolateYValueFor(eh.getX(itime)) + eh.getY(itime);
          }
        }
        if (coteEvolutions == null) {
          coteEvolutions = new EvolutionReguliereInterface[hEvolutions.length];
          evols_.put(H2dVariableType.COTE_EAU, coteEvolutions);
        }
        coteEvolutions[idxPt] = new EvolutionReguliereFixe(eh.getArrayX(), values);
        coteEvolutions[idxPt].setNom(H2dVariableType.COTE_EAU.getName());
        coteEvolutions[idxPt].setUnite(H2dVariableType.COTE_EAU.getCommonUnit());
      }
    }
    return true;
  }

  private void buildExtraLimni() {
    buildExtraElevation();
    buildExtraVitesse();
    updateAvailVar();
  }

  private boolean buildExtraVitesse() {
    if (getEvolution(H2dVariableType.VITESSE, 0) != null || getEvolution(H2dVariableType.VITESSE_U, 0) == null
        || getEvolution(H2dVariableType.VITESSE_V, 0) == null) {
      return false;
    }
    // pour chaque point limni, on va calculer la vitesse scalaire
    EvolutionReguliereInterface[] vitesseEvolutions = evols_.get(H2dVariableType.VITESSE);
    final EvolutionReguliereInterface[] vxEvolution = evols_.get(H2dVariableType.VITESSE_U);
    final EvolutionReguliereInterface[] vyEvolution = evols_.get(H2dVariableType.VITESSE_V);
    for (int idxPt = limnis_.getNbPoint() - 1; idxPt >= 0; idxPt--) {
      final EvolutionReguliereInterface evx = vxEvolution[idxPt];
      final EvolutionReguliereInterface evy = vyEvolution[idxPt];
      final double[] vScalaire = new double[evx.getNbValues()];
      for (int itime = vScalaire.length - 1; itime >= 0; itime--) {
        // ce test me semble dangereux: mais je ne comprends pas certaines exceptions dues
        // au TRC ...
        if (itime < evx.getNbValues() && itime < evy.getNbValues()) {
          vScalaire[itime] = Math.hypot(evx.getY(itime), evy.getY(itime));
        } else {
          vScalaire[itime] = -1;
        }
      }
      if (vitesseEvolutions == null) {
        vitesseEvolutions = new EvolutionReguliereInterface[vxEvolution.length];
        evols_.put(H2dVariableType.VITESSE, vitesseEvolutions);
      }
      vitesseEvolutions[idxPt] = new EvolutionReguliereFixe(evx.getArrayX(), vScalaire);
      vitesseEvolutions[idxPt].setNom(H2dVariableType.VITESSE.getName());
      vitesseEvolutions[idxPt].setUnite(H2dVariableType.VITESSE.getCommonUnit());
    }
    return true;
  }

  private CtuluIOOperationSynthese loadCommon(final H2dVariableType[] variableTypes, final File trcFile, final ProgressionInterface progressionInterface,
                                              final TrPostRubarLoader trPostRubarLoader) {
    if (limnis_ == null) {
      return null;
    }
    trPostSource = trPostRubarLoader.src_;
    trPostProjet = trPostRubarLoader.proj_;
    final RubarTRCReader reader = new RubarTRCReader(limnis_.getNbPoint(), variableTypes.length);
    final CtuluIOOperationSynthese s = reader.read(trcFile, progressionInterface);
    if (reader.isResultsTruncated()) {
      final String titre = TrPostRubarLoader.getDesc(trcFile);
      if (reader.isErrorOnFirstLine()) {
        s.getAnalyze()
            .addFatalError(
                TrResource
                    .getS(
                        "Il se peut que le nombre de points d�finis dans le fichier {0}\n ne soit pas le m�me que celui du fichier des limnigrammes (fichier DTR)",
                        trcFile.getName())
                    + CtuluLibString.LINE_SEP + TrResource.getS("Le fichier {0} n'est pas charg�", trcFile.getName()));
      } else {
        trPostRubarLoader.warnResTrunc(trPostProjet.getImpl(), titre, reader.getTimeError());
      }
    }
    s.getAnalyze().setDesc(TrPostRubarLoader.getDesc(trcFile));
    if (s.containsSevereError()) {
      return s;
    }
    final RubarTRCResult result = (RubarTRCResult) s.getSource();

    if (result.getNbPoint() == limnis_.getNbPoint() && result.getNbTimeStep() > 0) {
      // on initialise les tableaux des evolutions
      final EvolutionReguliereInterface e = result.getEvolFor(0, 0);
      double[] t = e.getArrayX();
      for (int varsIdx = variableTypes.length - 1; varsIdx >= 0; varsIdx--) {

        final H2dVariableType variableType = variableTypes[varsIdx];
        EvolutionReguliereInterface[] evolutionReguliereInterfaces = evols_.get(variableType);
        if (evolutionReguliereInterfaces == null) {
          evolutionReguliereInterfaces = new EvolutionReguliereInterface[limnis_.getNbPoint()];
          evols_.put(variableType, evolutionReguliereInterfaces);
        }
        for (int idxPt = limnis_.getNbPoint() - 1; idxPt >= 0; idxPt--) {
          final EvolutionReguliereInterface ev = result.getEvolFor(varsIdx, idxPt);
          if (ev != null) {
            evolutionReguliereInterfaces[idxPt] = new EvolutionReguliereFixe(t, ev.getArrayY());
            evolutionReguliereInterfaces[idxPt].setNom(variableType.getName());
            evolutionReguliereInterfaces[idxPt].setUnite(variableType.getCommonUnit());
          }
        }
      }
      // si vitesse contenu on construit le scalaire

    }

    updateAvailVar();
    return s;
  }

  private void updateAvailVar() {
    if (evols_.isEmpty()) {
      return;
    }
    final List<H2dVariableType> availableVariables = new ArrayList(10);
    for (Map.Entry<H2dVariableType, EvolutionReguliereInterface[]> entry : evols_.entrySet()) {
      if (entry.getValue() != null && entry.getValue().length > 0 && entry.getValue()[0] != null) {
        availableVariables.add(entry.getKey());
      }
    }
    availVar_ = availableVariables.toArray(new H2dVariableType[0]);
    Arrays.sort(availVar_);
    if (varModel_ != null) {
      varModel_.fireDonneesChanged();
    }
  }

  private void addGraphe(final int _ptIdx, final ProgressionInterface _prog, final FudaaCommonImplementation _impl, final TrPostSource _projet) {
    buildExtraLimni();
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    int nbVar = getNbVar();
    up.setValue(4, nbVar);
    up.majProgessionStateOnly();
    EGGrapheTreeModel treeModel = new EGGrapheTreeModel();
    final Map varAxe = new HashMap();
    for (int i = 0; i < nbVar; i++) {
      H2dVariableType var = getVar(i);
      if (var == null) {
        continue;
      }
      H2dVariableType parent = var.getParentVariable();
      if (parent == null) {
        parent = var;
      }
      EGGroup egGroup = (EGGroup) varAxe.get(parent);
      if (egGroup == null) {
        egGroup = createGroup(treeModel, varAxe, parent);
        egGroup.setVisible(i == 0);
      }
      final EvolutionReguliereInterface e = getEvolution(var, _ptIdx);
      e.setUnite(var.getCommonUnit());
      final EGModel model = new FudaaCourbeDefaultModelName(e);
      model.setTitle(var.getName());
      EGCourbeChild cs = new EGCourbeChild(egGroup, model);
      egGroup.addEGComponent(cs);
      cs.setAspectContour(BPalettePlageDefault.getColor(i));
      up.majAvancement();
    }
    final EGGraphe g = new EGGraphe(treeModel);
    g.setXAxe(EGAxeHorizontal.buildDefautTimeAxe(trPostSource.getTimeFormatter()));
    trPostProjet.addGrapheNodeInCurrentScene(new EGFillePanel(g), TrResource.getS("Limnigramme {0}", CtuluLibString.getString(_ptIdx + 1)), null);
  }

  void addGraphe(final int[] _ptIdx, final ProgressionInterface _prog, final TrPostCommonImplementation _impl, final TrPostSource _projet) {
    if (trPostProjet == null) {
      trPostProjet = _impl.getCurrentProject();
    }
    final int nbVar = getNbVar();
    if (nbVar == 0 || trPostProjet == null) {
      _impl.warn(TrResource.getS("Pas de variables"),
          TrResource.getS("Pas de variables � afficher. Avez-vous bien charg� un des fichiers TRC ou HYC ?"));
      return;
    }

    buildExtraLimni();
    if (_ptIdx == null) {
      return;
    }
    if (_ptIdx.length == 1) {
      addGraphe(_ptIdx[0], _prog, _impl, _projet);
      return;
    }
    final EGGrapheTreeModel treeModel = new EGGrapheTreeModelStatic();

    final Map varAxe = new HashMap();
    final ProgressionUpdater up = new ProgressionUpdater(_prog, false);
    up.setValue(10, _ptIdx.length * nbVar, 10, 90);
    up.majProgessionStateOnly();
    for (int i = 0; i < nbVar; i++) {
      final H2dVariableType v = availVar_[i];
      H2dVariableType parent = v.getParentVariable();
      if (parent == null) {
        parent = v;
      }
      EGGroup g = (EGGroup) varAxe.get(parent);
      if (g == null) {
        g = createGroup(treeModel, varAxe, parent);
        g.setVisible(i == 0);
      }
      final Color ci = BPalettePlageDefault.getColor(i);
      for (int idxPt = 0; idxPt < _ptIdx.length; idxPt++) {
        final EvolutionReguliereInterface e = getEvolution(getVar(i), _ptIdx[idxPt]);
        final EGModel model = new FudaaCourbeDefaultModelName(e);
        final String nom = getVar(i).getName() + CtuluLibString.ESPACE + (_ptIdx[idxPt] + 1);
        model.setTitle(nom);
        final EGCourbeChild c = new EGCourbeChild(g, model);
        c.setAspectContour(ci);
        g.addEGComponent(c);
        up.majAvancement();
      }
    }
    final EGGraphe g = new EGGraphe(treeModel);
    g.setXAxe(EGAxeHorizontal.buildDefautTimeAxe(trPostSource.getTimeFormatter()));
    trPostProjet.addGrapheNodeInCurrentScene(new EGFillePanel(g), TrResource.getS("Limnigrammes en {0} points", CtuluLibString.getString(_ptIdx.length)),
        null);
  }

  private EGGroup createGroup(EGGrapheTreeModel treeModel, Map varAxe, H2dVariableType parent) {
    EGGroup g;
    final EGAxeVertical axeV = new EGAxeVertical();
    axeV.setTitre(parent.getName());
    axeV.setUnite(parent.getCommonUnit());
    axeV.setGraduations(true);
    g = new EGGroup();
    g.setAxeY(axeV);
    g.setTitle(parent.getName());
    varAxe.put(parent, g);
    treeModel.add(g);
    return g;
  }

  final H2dRubarLimniMng getLimniPoint() {
    return limnis_;
  }

  /**
   * @param _t la variable demandee
   * @param _idxPt l'indice du point
   * @return l'evolution correspondante ou null si pas de donnees
   */
  public EvolutionReguliereInterface getEvolution(final H2dVariableType _t, final int _idxPt) {
    if (evols_.isEmpty()) {
      return null;
    }
    final EvolutionReguliereInterface[] evolutionReguliereInterfaces = evols_.get(_t);
    if (evolutionReguliereInterfaces == null || _idxPt >= evolutionReguliereInterfaces.length) {
      return null;
    }
    return evolutionReguliereInterfaces[_idxPt];
  }

  /**
   * @return le nombre de point limni
   */
  public int getNbPoint() {
    return limnis_ == null ? 0 : limnis_.getNbPoint();
  }

  /**
   * @return le nombre de variable
   */
  public int getNbVar() {
    return availVar_ == null ? 0 : availVar_.length;
  }

  /**
   * @param _idx l'indice de la variable
   * @return la variable
   */
  public H2dVariableType getVar(final int _idx) {
    return availVar_[_idx];
  }

  /**
   * @return le modele des variables supportees
   */
  public ListModel getVarModel() {
    if (varModel_ == null) {
      varModel_ = new VarModel();
    }
    return varModel_;
  }

  /**
   * @param _i l'indice du point
   * @return le x
   */
  public double getX(final int _i) {
    return limnis_.getX(_i);
  }

  /**
   * @param _i l'indice du point
   * @return le y
   */
  public double getY(final int _i) {
    return limnis_.getY(_i);
  }

  /**
   * @return true si les points limni sont deja charges
   */
  public boolean isLimniPointsLoaded() {
    return limnis_ != null;
  }

  /**
   * @param _trcFile le fichier hyc
   * @param _prog la barre de progression
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese loadHYC(final File _trcFile, final ProgressionInterface _prog, final TrPostRubarLoader _l) {
    if (_trcFile == null) {
      return null;
    }
    final H2dVariableType[] t = H2dRubarBcMng.getTransportVariables(nbConcentrationsBlocks).toArray(new H2dVariableType[0]);
    return loadCommon(t, _trcFile, _prog, _l);
  }

  private int nbConcentrationsBlocks = 1;

  /**
   * @param dtrFile le fichier DTR
   * @param _prog la progression
   * @return le resultat de la lecture
   */
  public CtuluIOOperationSynthese loadLimni(final File dtrFile, final EfGridInterface _grid, final ProgressionInterface _prog) {
    if (limnis_ != null) {
      FuLog.warning("limni nodes are already loaded");
      return null;
    }
    final RubarDTRReader r = new RubarDTRReader();
    final CtuluIOOperationSynthese s = r.read(dtrFile, _prog);
    if (!s.containsSevereError()) {
      final H2dRubarDTRResult result = (H2dRubarDTRResult) s.getSource();
      limnis_ = new H2dRubarLimniMng(_grid);
      limnis_.initLimni(result, s.getAnalyze(), true);
    }
    File parFile = CtuluLibFile.changeExtension(dtrFile, "par");
    final CtuluIOOperationSynthese read = RubarPARFileFormat.getInstance().getLastVersionInstance(parFile).read(parFile, null);
    final TIntObjectHashMap readValues = (TIntObjectHashMap) read.getSource();
    if (readValues != null && H2DRubarDicoParams.isTransport(readValues)) {
      nbConcentrationsBlocks = H2DRubarDicoParams.getNbTransportBlock(readValues);
      if (nbConcentrationsBlocks <= 1) {
        //on va v�rifier le nombre de fichiers tpc
        File dir = dtrFile.getParentFile();
        final File[] files = dir.listFiles(new FilenameFilter() {
          @Override
          public boolean accept(File dir, String name) {
            return TrPostRubarLoader.getTPCIndex(name) > 0;
          }
        });
        if (files != null) {
          for (File file : files) {
            nbConcentrationsBlocks = Math.max(nbConcentrationsBlocks, TrPostRubarLoader.getTPCIndex(file.getName()));
          }
        }
      }
    }
    return s;
  }

  /**
   * @param _trcFile le fichier trc
   * @param _prog la barre de progression
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese loadTRC(final File _trcFile, final ProgressionInterface _prog, final TrPostRubarLoader _l) {
    if (_trcFile == null) {
      return null;
    }
    final H2dVariableType[] t = new H2dVariableType[]{H2dVariableType.VITESSE_V, H2dVariableType.VITESSE_U, H2dVariableType.HAUTEUR_EAU};
    return loadCommon(t, _trcFile, _prog, _l);
  }
}
