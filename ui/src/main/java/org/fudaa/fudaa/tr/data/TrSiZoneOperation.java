/*
 *  @creation     19 avr. 2005
 *  @modification $Date: 2007-01-10 09:03:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPoint;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;

/**
 * Cette classe permet de calculer les pentes, cotes d'eau et les vitesses sur les zones d�finies par les plans d'eau.
 *
 * @author Fred Deniger
 * @version $Id: TrSiZoneOperation.java,v 1.9 2007-01-10 09:03:18 deniger Exp $
 */
public class TrSiZoneOperation {
  /**
   * Le x du point amont.
   */
  double xAmont_;
  /**
   * Le y du point amont.
   */
  double yAmont_;
  /**
   * la cote du point amont.
   */
  double zAmont_;
  /**
   * La pente selon x.
   */
  double px_;
  /**
   * La pente selon y.
   */
  double py_;
  /**
   * Le coefficient de Ch�zy utilis� pour calculer les vitesses � partir de la hauteur d'eau.
   */
  double coefChezy_ = 50d;
  /**
   * Le tester pour savoir si un point est dans la zone.
   */
  final IndexedPointInAreaLocator zoneTester_;
  final LineString zoneLine_;
  // final LineString litLine_;
  /**
   * le testeur pour savoir si un point est dans le lit. Si null toute la zone est consideree comme le lit.
   */
  final IndexedPointInAreaLocator litTester_;
  /**
   * true si le plan est horizontal.
   */
  final boolean isHor_;
  final Coordinate tmpCoord_ = new Coordinate();

  /**
   * @param _zone la zone concernee par cette operation
   * @param _profile la ligne de fond
   * @param _cotes les valeurs des cotes d'eau
   */
  public TrSiZoneOperation(final TrSIZone _zone, final LineString _profile, final CtuluCollectionDouble _cotes) {
    super();
    boolean isHor = false;
    zoneLine_ = _zone;
    // litLine_ = _zone.lit_;
    if (_zone.getPtAmont() >= 0) {
      xAmont_ = _profile.getCoordinateSequence().getX(_zone.getPtAmont());
      yAmont_ = _profile.getCoordinateSequence().getY(_zone.getPtAmont());
      zAmont_ = _cotes.getValue(_zone.getPtAmont());
      // horizontal si le point aval n'est pas fourni
      isHor = _zone.getPtAval() < 0;
    } else {
      // il n'y a pas de point amont donc le plan est horizontal.
      isHor = true;
      zAmont_ = _cotes.getValue(_zone.getPtAval());
    }
    // on cree les testeurs.
    zoneTester_ = new IndexedPointInAreaLocator(_zone);
    // le testeur pour le lit que si non null
    litTester_ = _zone.lit_ == null ? null : new IndexedPointInAreaLocator(_zone.lit_);
    // si le plan n'est pas horizontal, on calcule les pentes selon x et selon y
    if (!isHor) {
      final double zAval = _cotes.getValue(_zone.getPtAval());
      // moins d'un 1/2 mm d'ecart: on suppose que c'est horizontal.
      if (Math.abs(zAmont_ - zAval) < 0.5E-3) {
        isHor = true;
      } else {
        isHor = false;
        final double xAval = _profile.getCoordinateSequence().getX(_zone.getPtAval());
        final double yAval = _profile.getCoordinateSequence().getY(_zone.getPtAval());
        // la norme au carre
        final double norm = CtuluLibGeometrie.getD2(xAmont_, yAmont_, xAval, yAval);
        px_ = (xAval - xAmont_) * (zAval - zAmont_) / norm;
        py_ = (yAval - yAmont_) * (zAval - zAmont_) / norm;
      }
    }
    isHor_ = isHor;
  }

  /**
   * Permet de calculer les valeurs du point (x,_y) sur cette zone et renvoie true si le point est inclus dans cette
   * zone.
   *
   * @param _x le x du point a traiter
   * @param _y le y du point a traiter
   * @param _tabIdx l'indice des valeurs a modifier dans le tableau
   * @param _coteEau le tableau des cotes d'eau
   * @param _vx le tableau des vitesses selon x: peut etre null
   * @param _vy le tableau des vitesses selon y: peut etre null
   * @param _fond les cotes de fond
   * @return true si le point (_x,_y) est trait� par cette zone.
   */
  public boolean process(final double _x, final double _y, final int _tabIdx, final double[] _coteEau,
                         final double[] _vx, final double[] _vy, final CtuluCollectionDouble _fond) {
    tmpCoord_.x = _x;
    tmpCoord_.y = _y;
    final GISPoint pt = new GISPoint(tmpCoord_);
    if (GISLib.isInside(zoneTester_, tmpCoord_) || zoneLine_.distance(pt) < 1E-2) {
      // si un lit est defini est le point n'est pas dans le lit: on met la hauteur et les vitesses
      // a zero
      if (litTester_ != null && (!GISLib.isInside(litTester_, tmpCoord_))) {
        _coteEau[_tabIdx] = _fond.getValue(_tabIdx);
        if (_vx != null) {
          _vx[_tabIdx] = 0;
        }
        if (_vy != null) {
          _vy[_tabIdx] = 0;
        }
      }
      // si horizontal: on met a jour la hauteur et on met les vitesses a 0
      else if (isHor_) {
        _coteEau[_tabIdx] = zAmont_;
        if (_vx != null) {
          _vx[_tabIdx] = 0;
        }
        if (_vy != null) {
          _vy[_tabIdx] = 0;
        }
      }
      // finalement, on peut tout calculer
      else {
        // le z au point demande
        _coteEau[_tabIdx] = px_ * (_x - xAmont_) + py_ * (_y - yAmont_) + zAmont_;
        if (_vx != null || _vy != null) {
          final double h = _coteEau[_tabIdx] - _fond.getValue(_tabIdx);
          final double sss = Math.hypot(px_, py_);
          if (_vx != null) {
            if (h <= 0) {
              _vx[_tabIdx] = 0;
            } else {
              _vx[_tabIdx] = -px_ * Math.sqrt(h / sss) * coefChezy_;
            }
          }
          if (_vy != null) {
            if (h <= 0) {
              _vy[_tabIdx] = 0;
            } else {
              _vy[_tabIdx] = -py_ * Math.sqrt(h / sss) * coefChezy_;
            }
          }
        }
      }
      return true;
    }
    return false;
  }
}
