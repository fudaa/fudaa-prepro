package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuGridLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.dico.DicoEntite.Vecteur;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacTracerMng;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.fdico.FDicoEditorInterface;
import org.fudaa.fudaa.fdico.FDicoTableEditorChooser;
import org.fudaa.fudaa.tr.common.TrLib;

/**
 * @author genesis
 */
public class TrTelemacSourceModifyTracerAction extends EbliActionSimple {
  private final H2dTelemacTracerMng source;
  private final CtuluCommandManager cmdManager;

  public TrTelemacSourceModifyTracerAction(H2dTelemacTracerMng source, CtuluCommandManager cmdManager) {
    super(TrLib.getString("Modifier les traceurs"), null, "MODIFY_TRACER_NAME");
    this.source = source;
    this.cmdManager = cmdManager;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {

    final FDicoTableEditorChooser.EditorCreator creat = new FDicoTableEditorChooser.EditorCreator();
    Vecteur tracerNameDicoEntite = source.getTracerNameDicoEntite();
    Vecteur tracerInitValuesDicoEntite = source.getTracerInitialesValuesDicoEntite();
    final String[] tracerNames = source.getTracerNames();
    final String[] tracerInitValues = tracerInitValuesDicoEntite.getValues(source.getDico().getValue(tracerInitValuesDicoEntite));
    final FDicoEditorInterface[] namesEditor = new FDicoEditorInterface[tracerNames.length];
    final FDicoEditorInterface[] initValuesEditor = new FDicoEditorInterface[tracerNames.length];
    final JLabel[] labels = new JLabel[tracerNames.length];
    for (int tracerIdx = 0; tracerIdx < tracerNames.length; tracerIdx++) {
      namesEditor[tracerIdx] = creat.createEditor(tracerNameDicoEntite.getType());
      namesEditor[tracerIdx].setValue(tracerNames[tracerIdx]);
      initValuesEditor[tracerIdx] = creat.createEditor(tracerInitValuesDicoEntite.getType());
      initValuesEditor[tracerIdx].setValue(tracerInitValues[tracerIdx]);
      labels[tracerIdx] = new JLabel(H2dResource.getS("Traceur {0}", CtuluLibString.getString(tracerIdx + 1)));
    }
    CtuluDialogPanel pn = new CtuluDialogPanel() {
      @Override
      public boolean isDataValid() {
        boolean ok = true;
        for (int tracerIdx = 0; tracerIdx < tracerNames.length; tracerIdx++) {
          String value = namesEditor[tracerIdx].getValue();
          if (CtuluLibString.isEmpty(value)) {
            ok = false;
            labels[tracerIdx].setForeground(Color.RED);
          } else {
            labels[tracerIdx].setForeground(Color.BLACK);
          }
        }
        if (!ok) {
          setErrorText(TrLib.getString("Choisir un nom de traceur non vide"));
        }
        return ok;

      }
    };
    pn.setLayout(new BuGridLayout(3, 10, 10, true, false));
    pn.add(new JLabel());
    pn.add(new JLabel(TrLib.getString("Nom  du traceur")));
    pn.add(new JLabel(TrLib.getString("Valeur initiale")));
    for (int tracerIdx = 0; tracerIdx < tracerNames.length; tracerIdx++) {
      pn.add(labels[tracerIdx]);
      pn.add(namesEditor[tracerIdx].getComponent());
      pn.add(initValuesEditor[tracerIdx].getComponent());
    }
    if (pn.afficheModaleOk(null, getTitle())) {
      String[] newNames = new String[tracerNames.length];
      String[] newInitialValues = CtuluLibArray.copy(tracerInitValues);
      for (int tracerIdx = 0; tracerIdx < tracerNames.length; tracerIdx++) {
        newNames[tracerIdx] = namesEditor[tracerIdx].getValue();
        String initVal = initValuesEditor[tracerIdx].getValue();
        if (!CtuluLibString.isEmpty(initVal)) {
          newInitialValues[tracerIdx] = initVal;
        }
      }
      CtuluCommandComposite cmp = new CtuluCommandComposite();
      source.getDico().setValue(tracerNameDicoEntite, tracerNameDicoEntite.getStringValuesArray(newNames), cmp);
      source.getDico().setValue(tracerInitValuesDicoEntite, tracerInitValuesDicoEntite.getStringValuesArray(newInitialValues), cmp);
      cmdManager.addCmd(cmp);
    }

  }
}
