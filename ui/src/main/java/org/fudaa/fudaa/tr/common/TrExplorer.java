/*
 * @file TrExplorer.java
 *
 * @creation 8 mars 2004
 *
 * @modification $Date: 2007-04-30 14:22:42 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.*;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import com.memoire.fu.FuSort;
import com.memoire.vfs.VfsFile;
import com.memoire.vfs.VfsFileFile;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaUI;
import org.fudaa.fudaa.commun.exec.FudaaEditor;
import org.fudaa.fudaa.commun.exec.FudaaExec;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.impl.FudaaExplorer;
import org.fudaa.fudaa.tr.post.persist.TrPostFileFilter;
import org.fudaa.fudaa.tr.post.persist.TrPostFileFormat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;

/**
 * @author Fred Deniger
 * @version $id$
 */
public class TrExplorer extends FudaaExplorer {
  /**
   * Classe permettant d'executer une commande dans un repertoire. Class allowing to execute a command in a repetory
   *
   * @author Fred Deniger
   * @version $Id: TrExplorer.java,v 1.23 2007-04-30 14:22:42 deniger Exp $
   */
  public class ExplorerMenuItem extends BuMenuItem {
    boolean dir_;
    transient FudaaExec fudaaExec;

    ExplorerMenuItem(final FudaaExec _exe, final boolean _dir) {
      fudaaExec = _exe;
      dir_ = _dir;
      setAction(fudaaExec.getAction());
    }

    @Override
    protected void fireActionPerformed(final ActionEvent _event) {
      if (dir_) {
        File dir = TrExplorer.this.getSelectedDirectory();
        if (dir == null) {
          dir = getCurrentDirectory();
        }
        if (dir == null) {
          ui_.error(CtuluLib.getS("S�lectionner au moins un dossier"));
          return;
        }
        fudaaExec.execInDir(dir, ui_, getRefreshRunnable());
      } else {
        File f = getSelectedFile();
        // on prend le repertoire parent
        //We take the relative reperory
        if (f == null) {
          f = getCurrentDirectory();
        }
        if (f == null) {
          ui_.error(CtuluLib.getS("S�lectionner au moins un fichier"));
          return;
        }
        fudaaExec.execOnFile(f, ui_, getRefreshRunnable());
      }
    }

    public final FudaaExec getExec() {
      return fudaaExec;
    }
  }

  /**
   * @return le dernier rep selon la pref "trexplorer.last.dir" / the last directory according the pref
   */
  private static String getLastDir() {
    return TrPreferences.TR.getStringProperty("superviseur.last.dir", FuLib.getUserHome());
  }

  transient TrApplicationManager appli_;
  private transient FudaaUI ui_;

  /**
   * @param _impl l'impl parente
   * @param _appli le manager d'appli
   */
  public TrExplorer(final FudaaCommonImplementation _impl, final TrApplicationManager _appli) {
    this(_impl, _appli, VfsFile.createFile(getLastDir()));
  }

  /**
   * @param _impl l'impl parente
   * @param _appli le manager d'appli
   * @param _path le chemin initial
   */
  public TrExplorer(final FudaaCommonImplementation _impl, final TrApplicationManager _appli, final VfsFile _path) {
    super(_impl, true, _path);

    ui_ = _impl;
    setAppliManager(_appli);
    createFilters();
    viewer_.setFileRenderer(new TrFileRenderer(_appli, ui_));
  }

  private void buildMenuForDir() {
    pmDirs_.removeAll();
    appli_.buildCmdForMenuDir(pmDirs_, this);
    pmDirs_.addSeparator();
    pmDirs_.addMenuItem(FudaaLib.getS("Ajouter aux favoris"), "ADD_FAVORITES", getNoneIcon(), true);
    pmDirs_.addSeparator();
    pmDirs_.addMenuItem(BuResource.BU.getString("Cr�er un r�pertoire..."), "CREER_REPERTOIRE", BuResource.BU.getIcon("creer"), true);
    pmDirs_.addMenuItem(BuResource.BU.getString("Renommer..."), "RENOMMER_REPERTOIRE", BuResource.BU.getIcon("renommer"), true);
    pmDirs_.addMenuItem(BuResource.BU.getString("D�truire..."), "DETRUIRE_REPERTOIRE", BuResource.BU.getIcon("detruire"), true);
  }

  /**
   * @return icone aucun
   */
  private BuIcon getNoneIcon() {
    return BuResource.BU.getIcon("aucun");
  }

  private void buildMenuForFiles() {
    pmFiles_.removeAll();
    miOuvrirFichier_ = pmFiles_.addMenuItem(BuResource.BU.getString("Ouvrir"), "OUVRIR_FICHIER", true);
    appli_.buildCmdForMenuFile(pmFiles_, this);
    pmFiles_.addSeparator();
    pmFiles_.addMenuItem(BuResource.BU.getString("Tout s�lectionner"), "TOUTSELECTIONNER", true);
    pmFiles_.addSeparator();
    pmFiles_.addMenuItem(BuResource.BU.getString("Renommer..."), "RENOMMER_FICHIER", true);
    pmFiles_.addMenuItem(BuResource.BU.getString("D�truire..."), "DETRUIRE_FICHIER", true);
  }

  private void createFilters() {
    final Object o = filters_.getSelectedItem();
    final FileFormat[] fts = TrFileFormatManager.getFormatToFilter(appli_.getSoftId());
    final int nb = fts.length;
    for (int i = 0; i < nb; i++) {
      addFilter(appli_.getFileFilterFor(fts[i]));
    }
    final BuFileFilter txt = appli_.getTxtFileFilter();
    addFilter(txt);
    filters_.setSelectedItem(o);
  }

  /**
   * @param _mng le nouveau gestion des appli.
   */
  private void setAppliManager(final TrApplicationManager _mng) {
    appli_ = _mng;
    buildMenuForDir();
    buildMenuForFiles();
  }

  @Override
  protected void fireFileAction(final String _action) {
    if ("OUVRIR".equals(_action)) {
      appli_.ouvrir(getSelectedFile());
    }
  }

  @Override
  protected void updateMenuCurrent() {
    //nothing to do
  }

  /**
   * Pour etre accessible depuis inner classes. In order to be accessible from inner classes
   */
  @Override
  protected void updateMenuDirs() {
    super.updateMenuDirs();
  }

  private BuMenuItem itemEditSetupLayout = null;
  private BuMenuItem itemPostReloadAll = null;

  private void removeItems(BuMenuItem... items) {
    for (BuMenuItem buMenuItem : items) {
      if (buMenuItem != null && pmFiles_.getComponentIndex(buMenuItem) != -1) {
        pmFiles_.remove(buMenuItem);
      }
    }
  }

  @Override
  protected void updateMenuFiles() {
    final int l = files_.getSelectedValuesList().size();
    BuActionEnabler.setEnabledForAction(pmFiles_, "OUVRIR_FICHIER", l == 1);
    BuActionEnabler.setEnabledForAction(pmFiles_, "DETRUIRE_FICHIER", l >= 1);
    BuActionEnabler.setEnabledForAction(pmFiles_, "RENOMMER_FICHIER", l == 1);
    pmFiles_.computeMnemonics();
    appli_.updateOuvrirMenu(miOuvrirFichier_, l, getSelectedFile());

    //-- on degage l'editeur de texte specifique setup post si existe --//
    //We close the text editor if exists
    removeItems(itemEditSetupLayout, itemPostReloadAll);
    //-- on v�rifie qu'il s'agit d'un fichier de type POST, si oui on ajoute --// 
    //We check that it is a list of POST kind if we add
    final VfsFile selection = getSelectedFile();
    if (TrPostFileFilter.acceptDirectoryPOST(selection)) {
      EbliActionSimple action = new EbliActionSimple(TrResource.getS("Editer le fichier Setup"), EbliResource.EBLI.getIcon("text"), "EDITSETUP") {
        @Override
        public void actionPerformed(ActionEvent event) {
          //-- recuperation du fichier setup --//
          File setup = TrPostFileFormat.getInstance().getSetupFromMainDirectory(selection);
          if (setup != null) {
            FudaaEditor.getInstance().edit(setup);
          }
        }
      };
      //-- on ajoute l'editeur du fichier setup en deuxieme position --//
      itemEditSetupLayout = new BuMenuItem(action);
      pmFiles_.add(itemEditSetupLayout, 1);
      EbliActionSimple actionPost = new EbliActionSimple(TrResource.getS("Ouvrir et recalculer (Post)"), TrResource.getPostLayoutIcon(), "OPEN_POST_RELOAD") {
        @Override
        public void actionPerformed(ActionEvent event) {
          getAppliManager().doOpenPostLayout(selection, true);
        }
      };
      //-- on ajoute l'editeur du fichier setup en deuxieme position --//
      itemPostReloadAll = new BuMenuItem(actionPost);
      itemPostReloadAll.setToolTipText(TrResource.getS("Toutes les variables, trajectoires,... vont �tre recalcul�es au lancement"));
      pmFiles_.add(itemPostReloadAll, 1);
    }

    appli_.updateMenuFiles(l, getSelectedFile());
  }

  /**
   * Methode appelee lors d'un changement des applications utilisee. Method called in the case of a modifying used
   * applications
   */
  public void applicationPreferencesChanged() {
    buildMenuForDir();
    buildMenuForFiles();
  }

  public BuMenuItem createDirAction(final FudaaExec _ex) {
    return new ExplorerMenuItem(_ex, true);
  }

  public ExplorerMenuItem createFileAction(final FudaaExec _ex) {
    return new ExplorerMenuItem(_ex, false);
  }

  /**
   * @return le gestion de l'application
   */
  public final TrApplicationManager getAppliManager() {
    return appli_;
  }

  private transient Runnable refresh_;

  public Runnable getRefreshRunnable() {
    if (refresh_ == null) {
      refresh_ = new Runnable() {
        @Override
        public void run() {
          refresh();
        }
      };
    }
    return refresh_;
  }

  /**
   * Sauvegarde le dernier rep. Saves the last directory
   */
  public void saveLastDir() {
    if (getCurrentDirectory() != null && getCurrentDirectory().getAbsolutePath() != null) {
      TrPreferences.TR.putStringProperty("superviseur.last.dir", getCurrentDirectory().getAbsolutePath());
    }
  }

  /**
   * Surcharge du filtrage des fichiers et repertoires de l'explorer. Overload the filtering lists and notebooks from
   * the explorer Dans le cas du tr, cela affiche els repertoire .POST propre au post traitement dasn la vue des
   * fichiers.
   *
   * @author Adrien Hadoux
   */
  @Override
  protected void updateLists0(final VfsFile _dir, BuFileFilter _filter, final Cursor _cursor, final VfsFile _sel, final boolean _showing) {
    if (EventQueue.isDispatchThread()) {
      throw new RuntimeException("In swing thread. " + "Use updateLists() instead");
    }

    if (!updating_) {
      throw new RuntimeException("updating_ should be true");
    }

    last_ = _dir;
    setProgression(10);

    Runnable runnable;

    runnable = new CursorRunnable(_cursor, _dir);

    if (_showing) {
      SwingUtilities.invokeLater(runnable);
    } else {
      runnable.run();
    }

    timestamp_ = _dir.lastModified();

    String[] els = _dir.list();
    liststamp_ = liststamp(els);
    if (els == null) {
      els = STRING0;
    }

    setProgression(20);

    int l = els.length;

    final VfsFile[] dirs = new VfsFile[l + 1];
    final VfsFile[] files = new VfsFile[l];
    int nb_dirs = 0;
    int nb_files = 0;

    VfsFile pf = _dir.getParentVfsFile();
    if (pf instanceof VfsFileFile) {
      String p = pf.getAbsolutePath();
      pf = new Root(p, p, "..");
    }

    if (pf != null) {
      pf.setViewName("..");
      dirs[0] = pf;
      nb_dirs++;
    }

    setProgression(30);

    for (int i = 0; i < l; i++) {
      if (els[i].startsWith(".")) {
        continue;
      }
      if (els[i].endsWith("~")) {
        continue;
      }
      if ("CVS".equals(els[i])) {
        continue;
      }

      VfsFile f = _dir.createChild(els[i]);

      if (f == null) {
        FuLog.error("can create file " + els[i]);
      }

      //--cas particulier: si le fichier est un repertoire .POST
      //particular case if the list is a POST directory
      //-- on place les repertoire .POST avec les fichiers --// 
      // We take the repertories with the files  
      else if (TrPostFileFilter.acceptDirectoryPOST(f)) {

        files[nb_files] = f;
        nb_files++;
      } else if (f.isDirectory()) {
        dirs[nb_dirs] = f;
        nb_dirs++;
      } else if (f.isFile()) {
        if ((_filter == null) || _filter.accept(f)) {
          files[nb_files] = f;
          nb_files++;
        }
      }
    }

    els = null;
    setProgression(40);
    FuSort.sort(dirs, 0, nb_dirs);
    setProgression(50);
    FuSort.sort(files, 0, nb_files, getSorter());

    final int fnb_dirs = nb_dirs;
    final int fnb_files = nb_files;

    if (_sel != null) {
      _sel.build();
    }

    runnable = new Runnable() {
      @Override
      public void run() {
        dirs_.setEmptyText("No directory");
        dirs_.setModel(new Model(dirs, fnb_dirs));
        dirs_.revalidate();
        setProgression(60);

        files_.setEmptyText("No file");
        files_.setModel(new Model(files, fnb_files));
        files_.revalidate();
        setProgression(70);

        setProgression(80);
        repaint();

        if ((_sel != null) && !_sel.isDirectory()) {
          files_.setSelectedValue(_sel, true);
        }
        if (!WAIT_CURSOR.equals(_cursor)) {
          setCursor(_cursor);
        }

        setProgression(100);
        updating_ = false;
      }
    };

    if (_showing) {
      SwingUtilities.invokeLater(runnable);
    } else {
      runnable.run();
    }
  }

  private class CursorRunnable implements Runnable {
    private final Cursor _cursor;
    private final VfsFile _dir;

    public CursorRunnable(Cursor _cursor, VfsFile _dir) {
      this._cursor = _cursor;
      this._dir = _dir;
    }

    @Override
    public void run() {
      if (!WAIT_CURSOR.equals(_cursor)) {
        setCursor(WAIT_CURSOR);
      }

      current_.setText(FuLib.reducedPath(_dir.getAbsolutePath()));
      current_.setToolTipText(_dir.getAbsolutePath());
      current_.revalidate();
      updateMenuCurrent();
    }
  }
}
