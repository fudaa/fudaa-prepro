/**
 * @creation 1 d�c. 2004 @modification $Date: 2006-12-05 10:18:17 $ @license GNU General Public License 2 @copyright (c)1998-2001
 * CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuTable;
import org.locationtech.jts.geom.Coordinate;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.gis.GISCoordinateSequence;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcManager;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSourceMng;
import org.fudaa.dodico.h2d.telemac.H2dtelemacSiphon;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModeleSegment;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacSiphonModel.java,v 1.15 2006-12-05 10:18:17 deniger Exp $
 */
public class TrTelemacSiphonModel extends TrTelemacSourceModel implements ZModeleSegment {

  class SiphonModel extends AbstractTableModel {

    final int[] var_ = getMng().getSiphonCommonIndexVar();

    @Override
    public int getColumnCount() {
      return 3 + var_.length;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return TrResource.getS("Index");
      }
      if (_column == 1) {
        return TrResource.getS("Indice source {0}", CtuluLibString.UN);
      }
      if (_column == 2) {
        return TrResource.getS("Indice source {0}", CtuluLibString.DEUX);
      }
      final int varIdx = _column - 3;
      if (varIdx >= 0 && varIdx < var_.length) {
        return getMng().getSiphonVariableName(var_[varIdx]);
      }
      return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public int getRowCount() {
      return getMng().getNbSiphon();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _column) {
      if (_column == 0) {
        return CtuluLibString.getString(_rowIndex + 1);
      }
      final H2dtelemacSiphon s = getMng().getSiphon(_rowIndex);
      if (_column == 1) {
        return CtuluLibString.getString(s.getI1() + 1);
      }
      if (_column == 2) {
        return CtuluLibString.getString(s.getI2() + 1);
      }
      final int varIdx = _column - 3;
      if (varIdx >= 0 && varIdx < var_.length) {
        return Double.toString(s.getValue(var_[varIdx]));
      }
      return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return false;
    }
  }

  class SourceModel extends AbstractTableModel {

    private int[] v1_;
    private int[] v2_;
    final String[] commonVar_ = getMng().getExtremiteVarGeneralName();

    @Override
    public int getColumnCount() {
      return 3 + commonVar_.length;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return TrResource.getS("Index");
      }
      if (_column == 1) {
        return "X";
      }
      if (_column == 2) {
        return "Y";
      }
      if (_column == 3) {
        return TrResource.getS("Utilis�e par le siphon");
      }
      final int varIdx = _column - 4;
      if (varIdx >= 0 && varIdx < commonVar_.length) {
        return commonVar_[varIdx];
      }
      return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public int getRowCount() {
      return getMng().getNbSource();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _column) {
      if (_column == 0) {
        return CtuluLibString.getString(_rowIndex + 1);
      }
      final H2dTelemacSourceMng mng = getMng();
      final H2dTelemacSource src = mng.getSource(_rowIndex);
      if (_column == 1) {
        return Double.toString(src.getX());
      }
      if (_column == 2) {
        return Double.toString(src.getY());
      }

      final int usedBy = mng.getSiphonUsingSource(_rowIndex);
      if (usedBy >= 0) {
        if (_column == 3) {
          return CtuluLibString.getString(usedBy + 1);
        }
        final H2dtelemacSiphon sip = mng.getSiphon(usedBy);
        final int varIdx = _column - 4;
        if (v1_ == null) {
          v1_ = mng.getSiphonSource1IndexVar();
        }
        if (v2_ == null) {
          v2_ = mng.getSiphonSource2IndexVar();
        }
        int finalValue = -1;
        if (sip.getI1() == _rowIndex) {
          finalValue = v1_[varIdx];
        } else {
          finalValue = v2_[varIdx];
        }
        return Double.toString(sip.getValue(finalValue));

      }
      return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return false;
    }
  }

  protected final boolean isWorkOnSourcePoint() {
    return workOnSourcePoint_;
  }

  /**
   * @param _mng
   */
  public TrTelemacSiphonModel(final H2dTelemacSourceMng _mng, final H2dTelemacBcManager bcManager, final EfGridInterface _grid) {
    super(_mng, bcManager, _grid);
  }

  /**
   * @return le domaine des sources
   */
  public GrBoite getSourceDomaine() {
    return super.getDomaine();
  }
  boolean workOnSourcePoint_;

  /**
   * @param _workOnSourcePoint true si on travaille sur les sources
   * @return true si changement
   */
  public final boolean setWorkOnSourcePoint(final boolean _workOnSourcePoint) {
    // si changement on fait les modifs qui vont bien
    if (workOnSourcePoint_ != _workOnSourcePoint) {
      workOnSourcePoint_ = _workOnSourcePoint;
      return true;
    }
    return false;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    final BuTable b = new CtuluTable();
    if (isWorkOnSourcePoint()) {
      b.setModel(new SourceModel());
      EbliTableInfoPanel.setTitle(b, TrResource.getS("Sources"));
    } else {
      b.setModel(new SiphonModel());
      EbliTableInfoPanel.setTitle(b, TrResource.getS("Siphons"));
    }
    return b;
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
    if (isWorkOnSourcePoint()) {
      fillWithSourceInfo(_d, _layer.getLayerSelection());
    } else {
      fillWithInfo(_d, _layer.getLayerSelection());
    }
  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  @Override
  public double getNorme(final int _i) {
    return 0;
  }

  @Override
  public double getVx(final int _i) {
    return 0;
  }

  @Override
  public double getVy(final int _i) {
    return 0;
  }

  protected void fillWithSourceInfo(final InfoData _m, final CtuluListSelectionInterface _l) {
    super.fillWithInfo(_m, _l);
    if (_l != null && _l.isOnlyOnIndexSelected()) {
      final int i = _l.getMaxIndex();
      final H2dTelemacSourceMng mng = getMng();
      final int sip = mng.getSiphonUsingSource(i);
      if (sip >= 0) {
        _m.put(TrResource.getS("Source utilis�e par le siphon"), CtuluLibString.getString(sip + 1));
      }
    }
  }

  @Override
  public void prepare() {
  }

  @Override
  public double getZ1(final int _i) {
    return 0;
  }

  @Override
  public double getZ2(final int _i) {
    return 0;
  }

  @Override
  public void fillWithInfo(final InfoData _m, final CtuluListSelectionInterface _l) {
    final H2dTelemacSourceMng mng = getMng();
    if ((_l != null) && (_l.isOnlyOnIndexSelected())) {
      final int i = _l.getMaxIndex();
      _m.setTitle(TrResource.getS("Siphon") + CtuluLibString.ESPACE + CtuluLibString.getString(i + 1));
      final H2dtelemacSiphon sip = mng.getSiphon(i);
      _m.put(TrResource.getS("Extr�mit�") + CtuluLibString.ESPACE + CtuluLibString.UN, CtuluLibString.getString(sip.getI1() + 1));
      _m.put(TrResource.getS("Extr�mit�") + CtuluLibString.ESPACE + CtuluLibString.DEUX, CtuluLibString.getString(sip.getI2() + 1));

      final int[] glVar = mng.getSiphonCommonIndexVar();
      for (int var = 0; var < glVar.length; var++) {
        _m.put(mng.getSiphonVariableName(glVar[var]), Double.toString(sip.getValue(glVar[var])));
      }
    } else {
      _m.setTitle(TrResource.getS("Siphons"));
      _m.put(TrResource.getS("Nombre de siphons d�finis"), CtuluLibString.getString(mng.getNbSiphon()));
      if ((_l != null) && (!_l.isEmpty())) {
        _m.put(TrResource.getS("Nombre de siphons s�lectionn�s"), CtuluLibString.getString(_l.getNbSelectedIndex()));
      }
    }
  }

  @Override
  public GrBoite getDomaine() {
    if (workOnSourcePoint_) {
      return getSourceDomaine();
    }
    if (mng_.getNbSiphon() == 0) {
      return null;
    }

    H2dtelemacSiphon s = mng_.getSiphon(0);
    double x = s.getB1().getX();
    double y = s.getB1().getY();
    final GrBoite b = new GrBoite(new GrPoint(x, y, 0), new GrPoint(x, y, 0));
    x = s.getB2().getX();
    y = s.getB2().getY();
    b.ajuste(x, y, 0);
    for (int i = mng_.getNbSiphon() - 1; i >= 0; i--) {
      s = mng_.getSiphon(i);
      b.ajuste(s.getB1().getX(), s.getB1().getY(), 0);
      b.ajuste(s.getB2().getX(), s.getB2().getY(), 0);
    }
    return b;
  }

  @Override
  public int getNombre() {
    if (isWorkOnSourcePoint()) {
      return mng_.getNbSource();
    }
    return mng_.getNbSiphon();
  }

  @Override
  public boolean isGeometryReliee(int _idxGeom) {
    return true;
  }

  @Override
  public GISZoneCollection getGeomData() {
    GISZoneCollectionLigneBrisee res = new GISZoneCollectionLigneBrisee();
    int nb = mng_.getNbSiphon();
    for (int i = 0; i < nb; i++) {
      final H2dtelemacSiphon s = mng_.getSiphon(i);
      H2dTelemacSource b1 = s.getB1();
      Coordinate o1 = new Coordinate(b1.getX(), b1.getY());
      H2dTelemacSource b2 = s.getB2();
      Coordinate o2 = new Coordinate(b2.getX(), b2.getY());
      GISCoordinateSequence seq = new GISCoordinateSequence(new Coordinate[]{o1, o2});
      res.addPolyligne(seq, null);
    }
    return res;
  }

  @Override
  public Object getObject(final int _ind) {
    return null;
  }

  @Override
  public boolean segment(final GrSegment _s, final int _i, final boolean _force) {
    final H2dtelemacSiphon s = mng_.getSiphon(_i);
    H2dTelemacSource b = s.getB1();
    _s.o_.setCoordonnees(b.getX(), b.getY(), 0);
    b = s.getB2();
    _s.e_.setCoordonnees(b.getX(), b.getY(), 0);
    return true;
  }
}
