package org.fudaa.fudaa.tr.export;

import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.operation.EfIndexVisitorEltInRing;
import org.fudaa.dodico.ef.operation.EfOperation;
import org.fudaa.fudaa.meshviewer.export.MvExportOperationBuilderInterface;
import org.fudaa.fudaa.meshviewer.export.MvExportSourceFilterActivity;
import org.fudaa.fudaa.meshviewer.filter.MvFilterSelectedElement;
import org.fudaa.fudaa.tr.data.TrVisuPanel;

/**
 * Permet d'exporter uniquement les elements appartenant a un polygone
 *
 * @author deniger
 */
public class TrExportOperationMeshesInClosedLine extends TrExportOperationOnClosedLineAbstract {

  protected class Builder implements MvExportOperationBuilderInterface {

    LinearRing line;
    final EfGridData grid;
    CtuluListSelectionInterface meshSelected;

    private Builder(final LinearRing line, final EfGridData grid) {
      super();
      this.line = line;
      this.grid = grid;
    }

    @Override
    public void buildOperation(final ProgressionInterface progression) {
      meshSelected = EfIndexVisitorEltInRing.findContainedElt(grid.getGrid(), line, true);

    }

    @Override
    public EfOperation getOperation(final int tidx) {
      return new MvExportSourceFilterActivity(grid, new MvFilterSelectedElement(meshSelected, grid.getGrid()));
    }

    @Override
    public void stop() {
    }
  }

  public static TrExportOperationMeshesInClosedLine build(String title, CtuluUI ui, EfGridInterface grid, TrVisuPanel visu) {
    if (visu == null) {
      return new TrExportOperationMeshesInClosedLine(title, ui, grid);
    }
    return new TrExportOperationMeshesInClosedLine(title, visu);
  }

  /**
   * @param title
   * @param ui
   * @param grid
   */
  private TrExportOperationMeshesInClosedLine(String title, CtuluUI ui, EfGridInterface grid) {
    super(ui, grid, title);
  }

  /**
   * @param title
   * @param panel
   */
  private TrExportOperationMeshesInClosedLine(String title, TrVisuPanel panel) {
    super(title, panel);
  }

  @Override
  public MvExportOperationBuilderInterface getBuilder(final EfGridData src) {
    final LineString selectedLine = mvLineChooser.getSelectedLine();
    return new Builder(GISGeometryFactory.INSTANCE.transformToLinearRing(selectedLine), src);
  }
}
