/**
 * @creation 29 nov. 2004
 * @modification $Date: 2007-06-05 09:01:14 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import org.locationtech.jts.geom.LinearRing;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSourceMng;
import org.fudaa.dodico.h2d.telemac.H2dtelemacSiponListener;
import org.fudaa.ebli.calque.ZModeleDonnees;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.meshviewer.layer.MvNodeLayer;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrSaisiePointAction;
import org.fudaa.fudaa.tr.data.TrVisuPanel;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacSourceLayer.java,v 1.17 2007-06-05 09:01:14 deniger Exp $
 */
public class TrTelemacSourceLayer extends MvNodeLayer implements H2dtelemacSiponListener {

  private class AddSourceAction extends TrSaisiePointAction {

    /**
     * Initialisation par defaut.
     */
    public AddSourceAction() {
      super(TrResource.getS("Ajouter une source"), null, "ADD_DEFINED_CULVERT");
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if (isEnCours()) {
        super.selectPalette();
        return;
      }
      new TrTelemacSourceSaisie(parent_, TrTelemacSourceLayer.this).activeSaisiePoint(this);
    }
  }

  private class EditDefaultSourceAction extends EbliActionSimple {

    protected EditDefaultSourceAction() {
      super(TrResource.getS("Editer les paramètres communs"), null, "EDIT_GENERAL_CULVERT");
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      new TrTelemacSourceDefaultValues(modeleDonnees().getMng(), parent_.getCmdMng()).afficheModale(parent_.getFrame());
    }
  }

  private class EditSourceAction extends EbliActionSimple {

    /**
     * Initialisation par defaut.
     */
    public EditSourceAction() {
      super(TrResource.getS("Editer source(s)"), null, "EDIT_DEFINED_CULVERT");
      setDefaultToolTip(TrResource.getS("Editer les sources sélectionnées"));
    }

    @Override
    public String getEnableCondition() {
      return TrResource.getS("Sélectionner au moins une source");
    }

    @Override
    public void updateStateBeforeShow() {
      super.setEnabled(!isSelectionEmpty());
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if (!isSelectionEmpty()) {
        final TrTelemacSourceEditor edit = new TrTelemacSourceEditor(getSelectedIndex(), modeleDonnees().getMng(), parent_);
        final CtuluDialog d = edit.createDialog(parent_.getFrame());
        d.setTitle(TrResource.getS("Edition source(s)"));
        edit.setDial(d);
        d.afficheDialogModal();
      }
    }
  }

  private class RemoveSourceAction extends EbliActionSimple {

    public RemoveSourceAction() {
      super(TrResource.getS("Enlever les sources"), null, "REMOVE_DEFINED_CULVERT");
      setDefaultToolTip(TrResource.getS("Enlever les sources sélecionnées"));
    }

    @Override
    public String getEnableCondition() {
      return TrResource.getS("Sélectionner au moins une source");
    }

    @Override
    public void updateStateBeforeShow() {
      super.setEnabled(!isSelectionEmpty());
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if (!isSelectionEmpty()) {
        final int[] srcToRemove = getSelectedIndex();
        final int[] usedSiphon = modeleDonnees().getMng().getSiphonUsingSource(srcToRemove);
        if (usedSiphon != null
            && usedSiphon.length > 0
            && !parent_.getImpl().question(
                TrResource.getS("Siphons"),
                TrResource.getS("Des siphons utilisent les sources à supprimer.") + CtuluLibString.LINE_SEP
                    + TrResource.getS("Voulez les supprimer également ?"))) {
          return;
        }
        getSelection().clear();
        modeleDonnees().getMng().removeSource(srcToRemove, parent_.getCmdMng());

      }

    }
  }

  private class RemoveUsedSourceFromSelectionAction extends EbliActionSimple {

    public RemoveUsedSourceFromSelectionAction() {
      super(TrResource.getS("Déselectionner les sources utilisées"), null, "REMOVE_USED_SRC_FROM_SELECTION");
      setDefaultToolTip(TrResource.getS("Toutes les sources qui sont utilisées par un siphon seront enlevées de la sélection courante"));
    }

    @Override
    public String getEnableCondition() {
      return TrResource.getS("Sélectionner au moins une source");
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if (!isSelectionEmpty()) {
        final CtuluListSelection sele = getSelection();
        final int minIndex = sele.getMinIndex();
        final CtuluListSelection s = new CtuluListSelection();
        final H2dTelemacSourceMng src = modeleDonnees().getMng();
        for (int i = sele.getMaxIndex(); i >= minIndex; i--) {
          if (sele.isSelected(i) && !src.isSiphonUsingSource(i)) {
            s.add(i);
          }
        }
        sele.setSelection(s);
        TrTelemacSourceLayer.this.repaint();
      }
    }

    @Override
    public void updateStateBeforeShow() {
      super.setEnabled(!isSelectionEmpty());
    }
  }

  TrVisuPanel parent_;

  public TrTelemacSourceLayer(final TrTelemacSourceModel _modele) {
    super(_modele);
    modeleDonnees().getMng().addListener(this);
    iconModel_ = new TraceIconModel(TraceIcon.DISQUE, 5, Color.YELLOW);
    setForeground(Color.YELLOW.darker());
  }

  @Override
  public TrTelemacSourceModel modeleDonnees() {
    return  (TrTelemacSourceModel) super.modeleDonnees();
  }

  protected CtuluListSelection getSelection() {
    return selection_;
  }

  GrPoint realPt_;

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    return super.selectionBasic(_pt, _tolerance);
  }

  @Override
  public CtuluListSelection selection(final LinearRing _poly, final int _mode) {
    return super.selectionBasic(_poly, _mode);
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final int _nbPt, final GrMorphisme _versEcran, final GrBoite _clipReel) {
    super.paintDonnees(_g, _nbPt, _versEcran, _clipReel);
    if (realPt_ != null && _clipReel.contient(realPt_)) {
      final GrPoint p = realPt_.applique(_versEcran);
      final TraceIconModel m2 = new TraceIconModel(iconModel_);
      m2.setCouleur(Color.RED);
      final TraceIcon ic = new TraceIcon(m2);
      ic.paintIconCentre(this, _g, p.x_, p.y_);

    }
  }

  @Override
  public void culvertAdded() {
  }

  @Override
  public void culvertRemoved() {
  }

  @Override
  public void culvertValueChanged(final boolean _connexionChanged) {
  }

  public EbliActionInterface[] getActions(final TrVisuPanel _f) {
    parent_ = _f;
    return new EbliActionInterface[] { new EditSourceAction(), new AddSourceAction(), new RemoveSourceAction(), null,
        new RemoveUsedSourceFromSelectionAction(), new EditDefaultSourceAction(), null,
        new TrTelemacSourceAddTracerAction(modeleDonnees().getTracerManager(), _f.getCmdMng()),
        new TrTelemacSourceModifyTracerAction(modeleDonnees().getTracerManager(), _f.getCmdMng()),
        new TrTelemacSourceRemoveTracerAction(modeleDonnees().getTracerManager(), _f.getCmdMng()),
        
        null, parent_.getEditor().getExportAction() };
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    final int w = getIconWidth();
    final int h = getIconHeight();
    final Graphics2D g2d = (Graphics2D) _g;
    final Color old = _g.getColor();
    _g.setColor(Color.white);
    _g.fillRect(_x + 1, _y + 1, w - 1, h - 1);
    _g.setColor(getForeground());
    _g.drawRect(_x, _y, w, h);
    int ray = w / 5;
    if ((h / 5) < ray) {
      ray = h / 5;
    }
    final boolean fill = (modele_ != null) && (modele_.getNombre() > 0);
    // point 1
    if (fill) {
      g2d.fillOval(_x + 1, _y + 2, ray, ray);
    } else {
      g2d.drawOval(_x + 1, _y + 2, ray, ray);
    }

    // point 2
    if (fill) {
      g2d.fillOval(_x + w - 1 - ray, _y + h / 5, ray, ray);
    } else {
      g2d.drawOval(_x + w - 1 - ray, _y + h / 5, ray, ray);
    }

    // point 3
    if (fill) {
      g2d.fillOval(_x + w / 3 - ray, _y + 3 * h / 5, ray, ray);
    } else {
      g2d.drawOval(_x + w / 3 - ray, _y + 3 * h / 5, ray, ray);
    }
    _g.setColor(old);
  }

  @Override
  public void sourcesChanged(final DicoEntite _v) {
    final H2dTelemacSourceMng mng = modeleDonnees().getMng();
    if (mng.isCoordonnesEnt(_v)) {
      repaint();
      // on est dans le cas ou il faut changer l'icone
      if ((modeleDonnees().getNombre() == 0) || (modeleDonnees().getNombre() == 1)) {
        firePropertyChange("iconeChanged", true, false);
      }
    }
  }

  public void setTempoNode(final GrPoint _realPt) {
    realPt_ = _realPt;
  }

}
