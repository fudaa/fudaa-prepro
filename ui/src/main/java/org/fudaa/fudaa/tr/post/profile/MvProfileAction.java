/*
 * @creation 22 nov. 06
 * @modification $Date: 2007-06-13 12:58:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import org.locationtech.jts.geom.LineString;
import java.awt.event.ActionEvent;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.layer.MvVisuPanel;

/**
 * @author fred deniger
 * @version $Id: MvProfileAction.java,v 1.5 2007-06-13 12:58:09 deniger Exp $
 */
@SuppressWarnings("serial")
public class MvProfileAction extends EbliActionSimple {
  final MvProfileTarget src_;
  final MvVisuPanel panel_;
  final FudaaCommonImplementation ui_;
 
  public MvProfileAction(final MvProfileTarget _src, final FudaaCommonImplementation _ui, final MvVisuPanel _visu) {
    super(MvResource.getS("Profils spatiaux"), MvResource.MV.getIcon("profile"), "SPATIAL_PROFILE");
    src_ = _src;
    ui_ = _ui;
    panel_ = _visu;
    
  }

  protected LineString getSelectedLine() {
	//-- on tente de recuperer la ligne issue d'une creation geometrique --//
	  LineString ligneSelectionnee=panel_.getSelectedLine();
	//-- on essaie avec une ligne brisee formee par les sondes --//
	  if(ligneSelectionnee==null)
		  ligneSelectionnee=panel_.getSelectedLineFromSonde();
	return ligneSelectionnee;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
	  
	  
    new MvProfileBuilderFromTree(src_, ui_, getSelectedLine(), new MvProfileCoteTester()).start();
  }

}
