/*
 *  @creation     20 avr. 2005
 *  @modification $Date: 2007-06-28 09:28:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import java.util.Collection;
import java.util.Iterator;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.meshviewer.model.MvIsoPainterDefault;
import org.fudaa.fudaa.tr.common.TrLib;

/**
 * Un painter pour des variables d�finies sur les noeuds.
 * 
 * @author Fred Deniger
 * @version $Id: TrIsoPainter.java,v 1.2 2007-06-28 09:28:18 deniger Exp $
 */
public class TrIsoPainter extends MvIsoPainterDefault {

  public static void fillWithIsoPainter(final H2dVariableProviderInterface _prov, final Collection _setToFill,
      final boolean _si) {
    if (_prov == null) return;
    final EfGridInterface grid = _prov.getGrid();
    final Collection siVars = _prov.getUsableVariables();
    final boolean elementVar = _prov.isElementVar();
    for (final Iterator it = siVars.iterator(); it.hasNext();) {
      final H2dVariableType var = (H2dVariableType) it.next();
      if (elementVar) _setToFill.add(new TrIsoPainterElement(grid, _prov, var, _si ? getSiName(var) : getName(var)));
      else _setToFill.add(new TrIsoPainter(grid, _prov, var, _si ? getSiName(var) : getName(var)));
    }

  }

  public static String getName(final H2dVariableType _t) {
    return _t.getName();
  }

  public static String getSiName(final H2dVariableType _t) {
    return TrLib.getSIDisplayName() + ": " + _t.getName();
  }

  CtuluCollectionDouble model_;

  final String name_;

  final H2dVariableProviderInterface provider_;

  final H2dVariableType t_;

  /**
   * @param _g le maillage
   * @param _si les solutions initiales
   * @param _t la variable en question
   */
  public TrIsoPainter(final EfGridInterface _g, final H2dVariableProviderInterface _si, final H2dVariableType _t,
      final String _name) {
    super(_g);
    t_ = _t;
    provider_ = _si;
    name_ = _name;
  }

  private void buildModel() {
    model_ = provider_.getViewedModel(t_);
  }

  @Override
  public double getMax() {
    if (model_ == null) {
      buildModel();
    }
    return model_ == null ? 0 : model_.getMax();
  }

  @Override
  public double getMin() {
    if (model_ == null) {
      buildModel();
    }
    return model_ == null ? 0 : model_.getMin();
  }

  @Override
  public String getNom() {
    return name_;
  }

  @Override
  public double getValue(final int _idxPoint) {
    if (model_ == null) {
      buildModel();
    }
    return model_ == null ? 0 : model_.getValue(_idxPoint);
  }

}
