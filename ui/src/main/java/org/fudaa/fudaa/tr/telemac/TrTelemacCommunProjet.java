/*
 * @creation 16 juin 2003
 * @modification $Date: 2007-06-11 13:08:22 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluHelpComponent;
import org.fudaa.dodico.calcul.CalculLauncher;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoVersionManager;
import org.fudaa.dodico.dico.TelemacExec;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacDicoFileFormatVersion;
import org.fudaa.dodico.h2d.telemac.H2dTelemacParameters;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.telemac.TelemacVersionManager;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGFilleTree;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaProjetStateInterface;
import org.fudaa.fudaa.commun.exec.FudaaExec;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.fdico.*;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.tr.common.*;
import org.fudaa.fudaa.tr.data.TrFilleVisu;
import org.fudaa.fudaa.tr.data.TrVisuPanelEditor;
import org.fudaa.fudaa.tr.post.TrPostInspector;
import org.fudaa.fudaa.tr.post.TrPostInspectorReaderSerafin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * @author deniger
 * @version $Id: TrTelemacCommunProjet.java,v 1.63 2007-06-11 13:08:22 deniger Exp $
 */
public class TrTelemacCommunProjet extends FDicoProjet implements TrProjet, ActionListener {
  private class DicoProjectMenu extends BuMenu {
    /**
     * Initialise avec Projet.
     * Initialize with the project
     */
    public DicoProjectMenu() {
      super(TrResource.getS("Projet"), "PROJET");
      setName("mnProject");
      setIcon(null);
      build();
    }

    private void build() {
      if (getMenuComponentCount() != 0) {
        return;
      }
      TrTelemacCommunProjet.this.addDocMenuItem(this);
      buildFilleActions();
      add(getCalculActions().getCalcul().buildMenuItem(EbliComponentFactory.INSTANCE));
      add(getDiffAction().buildMenuItem(EbliComponentFactory.INSTANCE));
      add(loadAction_.buildMenuItem(EbliComponentFactory.INSTANCE));
      addSeparator();
      add(projectAction_.buildMenuItem(EbliComponentFactory.INSTANCE));
      add(visuAction_.buildMenuItem(EbliComponentFactory.INSTANCE));
      if (getTrParams().canImportEvolution()) {
        add(courbeAction_.buildMenuItem(EbliComponentFactory.INSTANCE));
      }
    }
  }

  private class TelemacEntiteFille extends FDicoFilleProjet implements CtuluHelpComponent {
    /**
     * @param _proj
     * @param _impl
     * @param _leftUp
     */
    public TelemacEntiteFille(final FDicoProjet _proj, final FudaaCommonImplementation _impl, final Component _leftUp) {
      super(_proj, _impl, _leftUp);
    }

    @Override
    public String getShortHtmlHelp() {
      return TrResource.getS("Permet d'�diter les mot-cl�s du projet et de configurer le comportement g�n�ral des "
          + "conditions limites: onglet \"conditions limites\"")
          + "<ul><li>"
          + getImpl().buildLink(TrResource.getS("Edition des mot-cl�s"), "telemac-editor-params-gen")
          + "</li><li>"
          + getImpl().buildLink(TrResource.getS("Edition des conditions limites"), "telemac-editor-bc")
          + "</li></ul>";
    }
  }

  EbliActionSimple courbeAction_;
  EGFilleTree courbeFille_;
  EbliActionSimple loadAction_;
  EbliActionSimple projectAction_;
  DicoProjectMenu projectMenu_;
  TrFilleVisu visu_;
  EbliActionSimple visuAction_;

  /**
   * @param _params les parametres / the parameters
   */
  public TrTelemacCommunProjet(final TrTelemacCommunParametres _params) {
    super(_params);
    if (isDataValide()) {
      actions_.setEnableCalcul(true);
    }
  }

  /**
   * Composant qui sera ajoute au panel entite
   * Component which will be added to the entity panel
   */
  private Component buildLeftUpComponent() {
    final JPanel r = new JPanel();
    r.setBorder(BorderFactory.createEtchedBorder());
    r.setLayout(new BuGridLayout(3, 2, 2));
    BuToolButton b;
    if (getTelemacFileFormatVersion().getTitreEntite() != null) {
      b = new BuToolButton(TrResource.TR.getToolIcon("modify-title"));
      b.setToolTipText(TrResource.getS("Modifier le titre du projet"));
      b.setActionCommand("MODIFY_TITLE");
      b.addActionListener(this);
      r.add(b);
    }
    if ((getTelemacFileFormatVersion().getPasTemps() != null)
        && (getTelemacFileFormatVersion().getNbPasTemps() != null)) {
      b = new BuToolButton(MvResource.MV.getToolIcon("time"));
      b.setToolTipText(TrResource.getS("D�finir la dur�e de la simultation"));
      b.setActionCommand("SIMULATION_DURATION");
      b.addActionListener(this);
      r.add(b);
    }
    return r;
  }

  /**
   * Charge tous les parametres possibles.
   * Load all the possible parameters
   */
  private boolean loadAll(final boolean _isCreation, final boolean _olb, final ProgressionInterface _inter) {
    if (!getTrTelemacParams().loadAllAction(_isCreation, _olb, _inter)) {
      return false;
    }
    if (getTrTelemacParams().isBoundaryConditionLoaded()) {
      final TrTelemacBoundaryVariableEditor v = new TrTelemacBoundaryVariableEditor(getTrTelemacParams().getClManager());
      v.setName("pnBoundary");
      TrTelemacCommunProjet.this.getProjetFille().addTab(TrResource.getS("Conditions limites"), null, v,
          TrResource.getS("Conditions limites"));
    }
    /** else if (getTelemacFileFormatVersion().getCLEntiteFile().isRequired()) FuLog.warning(new Throwable());* */
    if ((visu_ != null) && (visu_.getTrVisuPanel().getGroupBoundary() == null)
        && (getTrTelemacParams().isBoundaryConditionLoaded())) {
      ((TrTelemacVisuPanel) visu_.getVisuPanel()).addBcLayerIfNot();
      visu_.getArbreCalqueModel().refresh();
      visu_.repaint();
    }
    majAction();
    getTrTelemacParams().loadSavedData(getParamsFile(), _inter);

    return true;
  }

  void buildFilleActions() {
    if (projectAction_ == null) {
      projectAction_ = new EbliActionSimple(FudaaLib.getS("Param�tres g�n�raux"), BuResource.BU.getToolIcon("maison"),
          "MAIN_PROJECT_VIEW") {
        @Override
        public void actionPerformed(final ActionEvent _ae) {
          showGeneralFille();
        }
      };
      projectAction_.putValue(Action.SHORT_DESCRIPTION, FudaaLib.getS("Param�tres g�n�raux"));
      projectAction_.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.ALT_MASK
          + InputEvent.CTRL_MASK));
    }
    if (visuAction_ == null) {
      visuAction_ = new EbliActionSimple(TrResource.getS("Editeur 2D"), MvResource.MV.getToolIcon("maillage"),
          "GRID_PROJECT_VIEW") {
        @Override
        public void actionPerformed(final ActionEvent _ae) {
          showVisuFille();
        }
      };
      visuAction_.putValue(Action.SHORT_DESCRIPTION, TrResource.getS("Vue du maillage"));
      visuAction_.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.ALT_MASK
          + InputEvent.CTRL_MASK));
    }
    if (loadAction_ == null) {
      loadAction_ = new EbliActionSimple(TrResource.getS("Charger les fichiers"), FudaaResource.FUDAA
          .getToolIcon("charger"), "LOAD_DATA") {
        @Override
        public void actionPerformed(final ActionEvent _ae) {
          loadAll();
        }
      };
      loadAction_.putValue(Action.SHORT_DESCRIPTION, TrResource.getS("Charger tous les fichiers de param�tres"));
    }
    if ((courbeAction_ == null) && (getTrParams().canImportEvolution())) {
      courbeAction_ = new EbliActionSimple(H2dResource.getS("Courbes temporelles"), EbliResource.EBLI
          .getToolIcon("curves"), "TIME_CURVES") {
        @Override
        public void actionPerformed(final ActionEvent _ae) {
          showGrapheFille();
        }
      };
      courbeAction_.putValue(Action.SHORT_DESCRIPTION, TrResource.getS("Afficher la fen�tre des courbes temporelles"));
      courbeAction_.setEnabled(false);
      courbeAction_.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.ALT_MASK
          + InputEvent.CTRL_MASK));
    }
  }

  /**
   * Met a jour l'action de chargement.
   * Update the action of loading.
   */
  void majAction() {
    if (loadAction_ != null) {
      loadAction_.setEnabled(false);
    }
    majTransientCurveAction();
  }

  /**
   * Met a jour l'action concernant les courbes transitoires.
   * Update the action concerning the transitory curves
   */
  void majTransientCurveAction() {
    if (courbeAction_ == null) {
      return;
    }
    courbeAction_.setEnabled(getTelemacFileFormatVersion().isTransientAvailable());
  }

  @Override
  protected void buildEntiteFille() {
    if (entiteFille_ != null) {
      return;
    }
    entiteFille_ = new TelemacEntiteFille(this, getImpl(), buildLeftUpComponent());
    entiteFille_.setFrameIcon(BuResource.BU.getIcon("maison"));
    entiteFille_.setTitle(FudaaLib.getS("Param�tres g�n�raux"));
  }

  protected TrTelemacAppliManager getTelemacAppliMng() {
    return (TrTelemacAppliManager) getTrTelemacParams().getEditorImpl().getLauncher().getImplHelper(
        TrTelemacImplHelper.getID()).getAppliMng();
  }

  protected TrProjectDispatcherListener getUIObserver() {
    return (TrProjectDispatcherListener) getTrParams().getH2dParametres().getMainListener();
  }

  @Override
  public CalculLauncher actionCalcul() {
    final FDicoCalculLocalBuilder r = new FDicoCalculLocalBuilder(getCodeName(), getVersionManager(), getParamsFile(),
        true);
    final int resp = r.afficheModale(getImpl().getFrame(), FudaaLib.getS("Lancer le calcul"));
    if (CtuluDialogPanel.isOkResponse(resp)) {
      final FDicoCalculLocalOp op = new FDicoCalculLocalOp(this, r.getTelemacExec(r.isParallel()));
      op.setUi(getUI());
      if (r.isFollow()) {
        new TrPostInspector(new TrPostInspectorReaderSerafin(getParamsFile(), TrTelemacAppliManager
            .getTmpDirForSteeringFile(getParamsFile())), getCodeName() + CtuluLibString.ESPACE
            + getParamsFile().getName(), ((TrCommonImplementation) TrTelemacCommunProjet.this.getImpl()).getLauncher())
            .start();
      }
      return op;
    }
    return null;
  }

  /**
   * Gere les commandes MODIFY_TITLE et SIMULATION_DURATION.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    final String c = _e.getActionCommand();
    if ("MODIFY_TITLE".equals(c)) {
      if (entiteFille_.getSelectedComponent() == entiteFille_.getEntitePanel()) {
        super.changeTitleProjet(entiteFille_.getActiveCmdMng());
      }
    } else if ("SIMULATION_DURATION".equals(c)) {
      if (entiteFille_.getSelectedComponent() != entiteFille_.getEntitePanel()) {
        return;
      }
      final TrTelemacDurationEditor editor = new TrTelemacDurationEditor(getTrTelemacParams().getTelemacParametres());
      if (CtuluDialogPanel.isOkResponse(editor.afficheModale(getImpl().getFrame()))) {
        final CtuluCommandCompositeInverse cmd = new CtuluCommandCompositeInverse();
        if (editor.isDuration()) {
          getDicoParams().setValue(getTelemacFileFormatVersion().getDureeDuCalcul(), editor.getDurationString(), cmd);
          getDicoParams().setValue(getTelemacFileFormatVersion().getPasTemps(), editor.getTimeStep(), cmd);
          getDicoParams().removeValue(getTelemacFileFormatVersion().getNbPasTemps(), cmd);
        } else {
          getDicoParams().setValue(getTelemacFileFormatVersion().getNbPasTemps(), editor.getNbTimeStep(), cmd);
          getDicoParams().setValue(getTelemacFileFormatVersion().getPasTemps(), editor.getTimeStep(), cmd);
          getDicoParams().removeValue(getTelemacFileFormatVersion().getDureeDuCalcul(), cmd);
        }
        if (!cmd.isEmpty()) {
          entiteFille_.getActiveCmdMng().addCmd(cmd);
        }
      }
    }
  }

  /**
   * Affiche le panneau des mots-cl�s.
   * Display the panel of key words
   */
  @Override
  public void active() {
    boolean change = false;
    final FudaaCommonImplementation impl = getTrTelemacParams().getImpl();
    if (entiteFille_ == null) {
      change = true;
    }
    super.active();

    impl.addMenu(getProjectMenu(), impl.getNbMenuInMenuBar() - 2);
    if (change) {
      entiteFille_.setFrameIcon(BuResource.BU.getFrameIcon("maison"));
      entiteFille_.repaint();
      // TrLib.initFrameDimensionWithPref(entiteFille_, "general", impl.getMainPanel().getDesktop().getSize());
    }
  }

  /**
   * MEt a jour le fenetre interne des mot-cl�s.
   * Updates the internal window of key words
   */
  public void applicationPreferencesChanged() {
    if (super.entiteFille_ != null) {
      super.entiteFille_.applicationPreferencesChanged();
    }
  }

  @Override
  public void close() {
    final BuCommonImplementation impl = getImpl();
    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {
        impl.getMainMenuBar().removeMenu(getProjectMenu());
        final FDicoFilleProjet f = getProjetFille();
        if (f != null) {
          TrLib.closeInternalFrame(impl, f);
        }
        /*
         * if (visu_ != null) impl.getMainMenuBar().remove(visu_.getLayerMenu());
         */
        TrLib.closeInternalFrame(impl, visu_);
        visu_ = null;
        TrLib.closeInternalFrame(impl, courbeFille_);
        courbeFille_ = null;
      }
    });
  }

  /**
   * @return true
   */
  public boolean containsVisuFille() {
    return true;
  }

  @Override
  public final void exportMaillage() {
    if (visu_ == null) {
      final Runnable r = new Runnable() {
        @Override
        public void run() {
          TrProjetCommon.startExport(TrTelemacCommunProjet.this.getTrParams().getH2dParametres(), getImpl(), null, visu_.getTrVisuPanel());
        }
      };
      if (getTrParams().getH2dParametres().getMaillage() == null) {
        final CtuluTaskDelegate task = getImpl().createTask(TrResource.getS("Export"));
        final ProgressionInterface prog = task.getStateReceiver();
        task.start(new Runnable() {
          @Override
          public void run() {
            loadAll(prog);
            r.run();
          }
        });
      } else {
        r.run();
      }
    } else {
      ((TrVisuPanelEditor) visu_.getVisuPanel()).startExport(getImpl());
    }
  }

  @Override
  public void finishCreation(final ProgressionInterface _prog, final Properties _options) {
    final boolean useOlb = Boolean.toString(true).equals(_options.getProperty("use.olb", "false"));
    if (loadAll(true, useOlb, _prog)) {
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          showVisuFille();
        }
      });
    }
  }

  /**
   * @return l'entite designant les conditions limites liquides / the entity designying the boundary liquid conditions
   */
  public DicoEntite getCLLiquideEntiteFile() {
    return getTrTelemacParams().getDicoFileFormatVersion().getCLLiquideEntiteFile();
  }

  @Override
  public String getCodeExecDir() {
    final TelemacVersionManager mng = TelemacVersionManager.INSTANCE;
    String execDir = null;
    if (mng.getNbVersion() > 0) {
      execDir = mng.getVersionPath(mng.getNbVersion() - 1);
    }
    return execDir;
  }

  /**
   * Renvoie le manager des courbes des parametres.
   * Send the manager of the curves of parameters
   *
   * @return le gestionnaire de courbe / the plot manager
   */
  public TrCourbeTemporelleManager getEvolMng() {
    return getTrTelemacParams().getEvolMng();
  }

  public String getID() {
    return FileFormatSoftware.TELEMAC_IS.name;
  }

  /**
   * Construit si n�cessaire le menu "projet".
   * Build if necessary the "project" menu
   */
  @Override
  public BuMenu getProjectMenu() {
    if (projectMenu_ == null) {
      projectMenu_ = new DicoProjectMenu();
    }
    return projectMenu_;
  }

  @Override
  public CtuluSavable[] getSavableComponent() {
    return new CtuluSavable[]{visu_, getTrTelemacParams()};
  }

  /**
   * @see FileFormatSoftware#TELEMAC_IS
   */
  @Override
  public String getSoftwareID() {
    return FileFormatSoftware.TELEMAC_IS.name;
  }

  /**
   * Construit les actions si n�cessaires.
   * Build the necesary actions
   */
  @Override
  public EbliActionInterface[] getSpecificActions() {
    final List r = new ArrayList(10);
    r.add(getCalculActions().getCalcul());
    r.add(getDiffAction());
    buildFilleActions();
    r.add(loadAction_);
    r.add(visuAction_);
    if (getTrParams().canImportEvolution()) {
      r.add(courbeAction_);
    }
    final TrTelemacAppliManager app = getTelemacAppliMng();
    final EbliActionInterface[] rf = new EbliActionInterface[r.size()];
    r.toArray(rf);
    return rf;
  }

  @Override
  public FDicoProjectState getState() {
    return getFDicoParams().getState();
  }

  @Override
  public FudaaProjetStateInterface getProjectState() {
    return getState();
  }

  /**
   * @return la version utilisee / the used version
   */
  public H2dTelemacDicoFileFormatVersion getTelemacFileFormatVersion() {
    return getTrTelemacParams().getTelemacParametres().getTelemacVersion();
  }

  public H2dTelemacParameters getTelemacParametres() {
    return getTrTelemacParams().getTelemacParametres();
  }

  @Override
  public TrParametres getTrParams() {
    return (TrParametres) super.getFDicoParams();
  }

  /**
   * @return les parametres visu de telemac
   */
  public TrTelemacCommunParametres getTrTelemacParams() {
    return (TrTelemacCommunParametres) super.getFDicoParams();
  }

  @Override
  public TrCourbeUseFinder getUsedCourbeFinder(final EGGrapheModel _model) {
    return new TrTelemacUseCourbeFinder(getTelemacParametres(), visu_.getTrVisuPanel(), _model);
  }

  @Override
  public DicoVersionManager getVersionManager() {
    return TelemacVersionManager.INSTANCE;
  }

  /**
   * Ne construit pas la fenetre de visu. renvoie uniquement l'instance
   * Doesn't build the window. Only sends the instance
   */
  @Override
  public TrFilleVisuInterface getVisuFille() {
    return visu_;
  }

  @Override
  public void importCourbesTemporelles(final EvolutionReguliereInterface[] _r) {
    if ((getImpl() == null) || (getEvolMng() == null)) {
      return;
    }
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        showGrapheFille();
        if (courbeFille_ != null) {
          getEvolMng().add(_r, courbeFille_.getCmdMng());
        }
      }
    });
  }

  @Override
  public boolean isAlreadyUsed(final File _f) {
    return getFDicoParams().isUsed(_f);
  }

  /**
   * @return true si le projet accepte des courbes transitoires / true if the project accepts transitory curves
   */
  public boolean isTransientAvailable() {
    return getTrTelemacParams().canImportEvolution();
  }

  /**
   * Charge tous les parametres.
   * Loads all the parameters
   */
  public void loadAll() {
    if (getTrTelemacParams().isAllLoaded()) {
      getImpl().message(TrResource.getS("Tous les fichiers sont d�j� charg�s"));
      majAction();
    }
    getImpl().setGlassPaneStop();

    final CtuluTaskDelegate task = getImpl().createTask(TrResource.getS("Chargement fichiers"));
    task.start(new Runnable() {
      @Override
      public void run() {
        try {
          loadAll(task.getStateReceiver());
        } finally {
          BuLib.invokeLater(getImpl().getUnsetGlassPaneRunnable());
        }
      }
    });
  }

  @Override
  public boolean loadAll(final ProgressionInterface _inter) {
    return loadAll(false, false, _inter);
  }

  @Override
  public boolean save(final ProgressionInterface _prog) {
    final boolean res = super.save(_prog);
    if (TrProjectPersistence.saveProject(getImpl(), this, getParamsFile(), _prog)) {
      getState().setUIModified(false);
    }
    return res;
  }

  @Override
  public boolean saveAs(final ProgressionInterface _prog) {
    final boolean res = super.saveAs(_prog);
    if (TrProjectPersistence.saveProject(getImpl(), this, getParamsFile(), _prog)) {
      getState().setUIModified(false);
    }
    return res;
  }

  @Override
  public File saveCopy(final ProgressionInterface _prog, final File _f) {
    final File f = super.saveCopy(_prog, _f);
    if (f != null) {
      TrProjectPersistence.saveProject(getImpl(), this, f, _prog);
    }
    return f;
  }

  @Override
  public void showGeneralFille() {
    activeEntiteFille();
  }

  @Override
  public final void showGrapheFille() {
    if ((getImpl() == null) || (getEvolMng() == null)) {
      return;
    }
    getImpl().setGlassPaneStop();
    try {
      if (this.courbeFille_ == null) {
        courbeAction_.setEnabled(true);
        final EGGraphe graphe = getEvolMng().createGraphe();
        final EGFillePanel panel = new EGFillePanel(graphe);
        final List acts = new ArrayList();
        acts.add(new TrCourbeUseAction(getUsedCourbeFinder(graphe.getModel())));
        acts.add(null);
        acts.addAll(Arrays.asList(TrCourbeImporter.getImportExportAction(getEvolMng(), graphe, getImpl())));
        panel.setPersonnalAction((EbliActionInterface[]) acts.toArray(new EbliActionInterface[acts.size()]));
        courbeFille_ = new TrGrapheTreeTimeFille(panel, H2dResource.getS("Courbes temporelles"), getImpl(),
            getInformationsDocument()) {
          @Override
          public String getShortHtmlHelp() {
            return TrProjetCommon.getCurvesHelp(getImpl(), new String[]{getImpl().buildLink(
                TrResource.getS("Les conditions limites"), "telemac-editor-bc")});
          }
        };
        courbeFille_.setVisible(true);
        courbeFille_.pack();
        getImpl().addInternalFrame(courbeFille_);
        // TrLib.initFrameDimensionWithPref(courbeFille_, "curve", getImpl().getMainPanel().getDesktop().getSize());
      } else if (courbeFille_.isClosed()) {
        getImpl().addInternalFrame(courbeFille_);
      } else {
        getImpl().activateInternalFrame(courbeFille_);
      }
    } finally {
      getImpl().unsetGlassPaneStop();
    }
  }

  /**
   * Appelle activeVisu().
   * Calls activVisu(()
   */
  @Override
  public void showVisuFille() {
    if (getImpl() == null) {
      return;
    }
    if (visu_ == null) {

      BuLib.invokeNow(new Runnable() {
        @Override
        public void run() {
          getImpl().setGlassPaneStop();
        }
      });
      final String msg = TrResource.getS("Chargement fichiers");
      final CtuluTaskDelegate task = getImpl().createTask(msg);
      task.start(new Runnable() {
        @Override
        public void run() {
          try {
            task.getMainStateReceiver().setDesc(msg);
            final ProgressionInterface prog = task.getStateReceiver();
            if (!getTrTelemacParams().isBoundaryConditionLoaded() && !loadAll(prog)) {
              return;
            }
            majAction();
            if (getTrTelemacParams().isGeometrieLoaded()) {
              task.getMainStateReceiver().setProgression(60);
              task.getMainStateReceiver().setDesc(FSigLib.getS("Construction vue 2D"));

              visu_ = new TrFilleVisu(new TrTelemacVisuPanel(getImpl(), TrTelemacCommunProjet.this));
              visu_.setFrameIcon(MvResource.MV.getIcon("maillage"));
              if (getImpl().getMainPanel().getDesktop() != null) {
                TrLib.initFrameDimensionWithPref(visu_, getImpl().getMainPanel().getDesktop().getSize());
              }
              final Runnable finish = TrLib.restoreEditorMainFille(getImpl(), getParamsFile(), visu_, prog);

              SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                  getImpl().addInternalFrame(visu_);
                  visu_.restaurer();
                  if (finish != null) {
                    finish.run();
                  }
                  // le listener qui ecoute les evt de l'arbre des calques
                  // the listener which listens the evt from the tree of layers
                }
              });
            }
          } catch (final Throwable _e) {
            FuLog.error(msg, _e);
          } finally {
            BuLib.invokeLater(getImpl().getUnsetGlassPaneRunnable());
            if (visu_ != null) {
              visu_.getArbreCalqueModel().getObservable().addObserver(TrTelemacCommunProjet.this.getUIObserver());
            }
          }
        }
      });
      return;
    }
    getImpl().addInternalFrame(visu_);
  }
}
