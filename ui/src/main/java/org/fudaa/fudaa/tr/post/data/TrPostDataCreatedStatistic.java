/*
 * @creation 2 f�vr. 07
 *
 * @modification $Date: 2007-05-04 14:01:51 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleAbstract;
import org.fudaa.ctulu.collection.CtuluDoubleVisitor;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.impl.EfDataStatisticEvaluator;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.fudaa.tr.post.TrPostFlecheContent;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.nfunk.jep.Variable;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * @author fred deniger
 * @version $Id: TrPostDataCreatedStatistic.java,v 1.5 2007-05-04 14:01:51 deniger Exp $
 */
public class TrPostDataCreatedStatistic implements TrPostDataCreated {
  @Override
  public boolean updateConstantVar(final Variable _var) {
    return false;
  }

  final static CtuluPermanentList STAT_OPERATOR = new CtuluPermanentList(new EfDataStatisticEvaluator[]{new EfDataStatisticEvaluator.Max(),
      new EfDataStatisticEvaluator.Min(), new EfDataStatisticEvaluator.Moyenne(), new EfDataStatisticEvaluator.Sum()});
  private EfData efData = new EfData() {
    @Override
    public void expandTo(final CtuluRange _rangeToExpand) {
      if (_rangeToExpand != null && r_ != null) {
        _rangeToExpand.expandTo(r_);
      }
    }

    @Override
    public void getRange(final CtuluRange _r) {
      if (_r != null && r_ != null) {
        _r.initWith(r_);
      }
    }

    @Override
    public Object getObjectValueAt(final int _i) {
      return CtuluLib.getDouble(getValue(_i));
    }

    @Override
    public Double getCommonValue(final int[] _i) {
      return CtuluCollectionDoubleAbstract.getCommonValue(efData, _i);
    }

    @Override
    public double getMax() {
      return mustCompute_ || r_ == null ? 0 : r_.getMax();
    }

    @Override
    public double getMin() {
      return mustCompute_ || r_ == null ? 0 : r_.getMin();
    }

    @Override
    public int getSize() {
      return value_ == null ? 0 : value_.length;
    }

    @Override
    public double getValue(final int _i) {
      return value_ == null ? 0 : value_[_i];
    }

    @Override
    public double[] getValues() {
      return value_ == null ? null : CtuluLibArray.copy(value_);
    }

    @Override
    public boolean isElementData() {
      return src_.isElementVar(varToStat_);
    }

    @Override
    public void iterate(final CtuluDoubleVisitor _visitor) {
      final int n = getSize();
      for (int i = 0; i < n; i++) {
        if (!_visitor.accept(i, getValue(i))) {
          return;
        }
      }
    }
  };
  EfDataStatisticEvaluator evaluator_;
  private volatile boolean mustCompute_ = true;
  CtuluRange r_;
  final TrPostSource src_;
  int tsBegin_;
  int tsEnd_;
  final CtuluUI ui_;
  double[] value_;
  H2dVariableTypeCreated varCreated_;
  H2dVariableType varToStat_;
  private String varToStatId;


  @Override
  public void restore() {
    updateValues();
  }

  public static TrPostDataCreatedStatistic createFrom(final TrPostDataCreatedStatisticSaver _saver, final TrPostSource _src, final Map _shortname,
                                                      final CtuluUI _ui, double[] savedValues) {
    if (_saver == null || _shortname == null || _ui == null || _src == null) {
      return null;
    }
    final H2dVariableType var = (H2dVariableType) _shortname.get(_saver.shortName_);
    if (var == null) {
      return null;
    }
    final EfDataStatisticEvaluator eval = EfDataStatisticEvaluator.findById(STAT_OPERATOR, _saver.statId_);
    if (eval == null) {
      return null;
    }
    final TrPostDataCreatedStatistic res = new TrPostDataCreatedStatistic(_src, _ui, var, eval);
    res.tsBegin_ = _saver.tBegin_;
    res.tsEnd_ = _saver.tEnd_;
    res.mustCompute_ = false;
    res.r_ = _saver.r_;
    res.value_ = savedValues;
    if (res.value_ == null || res.r_ == null) {
      res.mustCompute_ = true;
    }
    return res;
  }

  public TrPostDataCreatedStatistic(final TrPostSource _src, final CtuluUI _ui, final H2dVariableType _var, final EfDataStatisticEvaluator _eval) {
    evaluator_ = _eval;
    varToStat_ = _var;
    src_ = _src;
    ui_ = _ui;
    unsetTimeStep();
  }

  private String getShortName() {
    return evaluator_.getStatTitle() + CtuluLibString.ESPACE + varToStat_.getName();
  }

  boolean isForAll() {
    return tsBegin_ < 0 || (tsBegin_ == 0 && tsEnd_ == src_.getNbTimeStep() - 1);
  }

  @Override
  public void clearCache() {
    mustCompute_ = true;
    updateValues();
  }

  @Override
  public TrPostDataCreatedSaver createSaver() {
    return new TrPostDataCreatedStatisticSaver(this);
  }

  @Override
  public void fillWhithAllUsedVar(final Set _res) {
    if (_res != null && !_res.contains(varToStat_)) {
      _res.add(varToStat_);
      TrPostDataHelper.addDependingVarOf(varToStat_, src_, _res);
    }
  }

  @Override
  public EfData getDataFor(final int _idxTime) {
    return efData;
  }

  @Override
  public String getDescription() {
    String desc = getShortName();
    // on ajoute des infos sur les pas de temps.
    if (!isForAll()) {
      desc += " [" + src_.getTimeListModel().getElementAt(tsBegin_) + ", " + src_.getTimeListModel().getElementAt(tsEnd_) + ']';
    }
    return desc;
  }

  public int getTsBegin() {
    return tsBegin_;
  }

  public int getTsEnd() {
    return tsEnd_;
  }

  @Override
  public double getValue(final int _idxTime, final int _idxObject) {
    updateValues();
    if (value_ == null) {
      return 0;
    }
    return value_[_idxObject];
  }

  public H2dVariableTypeCreated getVarCreated() {
    return varCreated_;
  }

  @Override
  public boolean isEditable() {
    return true;
  }

  @Override
  public TrPostFlecheContent isFleche() {
    return null;
  }

  public boolean isUptodate() {
    return !mustCompute_;
  }

  public void setTsBegin(final int _tsBegin) {
    final int newTs = evaluator_.getCorrectTsBegin(_tsBegin, src_.getNbTimeStep());
    if (newTs != tsBegin_) {
      tsBegin_ = newTs;
      mustCompute_ = true;
    }
  }

  public void setTsEnd(final int _tsEnd) {
    final int newTs = evaluator_.getCorrectTsEnd(_tsEnd, src_.getNbTimeStep());
    if (newTs != tsEnd_) {
      tsEnd_ = newTs;
      mustCompute_ = true;
    }
  }

  public void setVarCreated(final H2dVariableTypeCreated _varCreated) {
    varCreated_ = _varCreated;
  }

  public final void unsetTimeStep() {
    tsBegin_ = -1;
    tsEnd_ = -1;
  }

  public void updateValues() {
    updateValues(null);
  }

  public void updateValues(ProgressionInterface prog) {
    if (!mustCompute_) {
      return;
    }
    mustCompute_ = false;
    try {
      value_ = evaluator_.compute(src_, varToStat_, tsBegin_, tsEnd_, src_.getNbTimeStep(), value_, prog);
      if (r_ == null) {
        r_ = new CtuluRange();
      }
      CtuluRange.getRangeFor(value_, r_);
    } catch (final IOException _evt) {
      ui_.error(_evt.getMessage());
      FuLog.error(_evt);
    } finally {
      src_.updateUserValue(varCreated_, varCreated_, TrPostDataCreatedStatistic.this, null);
    }
  }

  @Override
  public boolean isExpr() {
    return false;
  }
}
