/**
 * @creation 6 f�vr. 2004
 * @modification $Date: 2008-01-17 11:39:01 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.fu.FuLog;
import java.util.List;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JMenu;
import org.fudaa.ctulu.CtuluExportDataInterface;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.image.CtuluImageImporter;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZEbliCalquePanelController;
import org.fudaa.ebli.calque.action.EbliCalqueActionVariableChooser;
import org.fudaa.ebli.calque.edition.ZCalqueEditionGroup;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.meshviewer.layer.MvFrontierPointLayer;
import org.fudaa.fudaa.meshviewer.layer.MvIsoPaintersLayer;
import org.fudaa.fudaa.meshviewer.layer.MvVisuPanel;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.sig.layer.FSigImageImportAction;
import org.fudaa.fudaa.sig.layer.FSigVisuPanelController;
import org.fudaa.fudaa.tr.common.Tr3DInitialiser;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrVisuPanel.java,v 1.36.4.1 2008-01-17 11:39:01 bmarchan Exp $
 */
public abstract class TrVisuPanel extends MvVisuPanel implements CtuluExportDataInterface, CtuluImageImporter {

    
    FudaaCommonImplementation impl;
	
  /**
   */
  public TrVisuPanel(final FSigVisuPanelController _controller) {
    super(_controller);
  }

 
  
  public TrVisuPanel(final FudaaCommonImplementation _impl) {
    super(_impl);
    this.impl = _impl;
  }

  public abstract void view3D(JFrame _f);

  public abstract String getProjectName();

  public void updateInfoAndIso() {
    updateInfoComponent();
    final MvIsoPaintersLayer p = getLayerIsoSurface();
    if ((p != null) && (p.isVisible())) {
      if (FuLog.isTrace()) {
        FuLog.trace("repaint isos");
      }
      p.repaint();
    }
  }

  @Override
  public void importImage() {
    final BGroupeCalque cq = getGroupFond();
    if (cq != null) {
      new FSigImageImportAction(this, cq).actionPerformed(null);

    }
  }

  public H2dVariableType[] getProposedVariables() {
    return null;
  }

  @Override
  protected void initButtonGroupSpecific(final List _l, final ZEbliCalquePanelController _res) {
    super.initButtonGroupSpecific(_l, _res);
    _l.add(new EbliCalqueActionVariableChooser(getArbreCalqueModel()));
    _l.add(Tr3DInitialiser.create3dAction(this, getProjectName()));

  }

  /**
   * Action appelee pour editer un point du maillage.
   */
  @Override
  public abstract String editGridPoint();

  /**
   * Action appelee pour editer un polygone.
   */
  @Override
  public abstract String editGridPoly();

  /**
   * Action appelee pour editer un point de bord.
   */
  public abstract String editBcPoint();

  /**
   * @return true si les points sont editables
   */
  public abstract boolean isBcPointEditable();

  /**
   * Doit renvoyer true si les points du maillage sont editables.
   * 
   * @return false par defaut
   */
  @Override
  public boolean isGridPointEditable() {
    return false;
  }

  /**
   * Doit renvoyer true si les elements sont editables.
   * 
   * @return false par defaut
   */
  @Override
  public boolean isGridElementEditable() {
    return false;
  }

  /**
   * La construction du groupe de calque conditions limites. 
   * @return Le calque cr�e, sans ajout des sous calques.
   */
  protected final TrBcLayerGroup buildGroupConlim() {
    TrBcLayerGroup r = getGroupBoundary();
    if (r == null) {
      r = new TrBcLayerGroup();
      r.setName("gpCl");
      addCalque(r);
    }
    return r;
  }

  @Override
  protected void configureGISGroup(final ZCalqueEditionGroup _layer) {
    _layer.putClientProperty(CtuluLib.getHelpProperty(), getImpl().buildHelpPathFor("common-sig"));
    _layer.putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Gestion des informations g�ographiques"));
  }

  protected void addCalqueFleche(final BGroupeCalque _grIsoLayer) {

  }

  public void updateIsoVariables() {
    final MvIsoPaintersLayer l = getLayerIsoSurface();
    l.updateList();
  }

  public final void addIsoLayer() {
    if (getGroupFond() != null) { return; }
    final BGroupeCalque gr = buildGroupeFond();
    addCalqueFleche(gr);
    gr.putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Affichage des images de fond et des isosurfaces"));
    final MvIsoPaintersLayer l = MvIsoPaintersLayer.buildIsoLayer(this, getGridGroup().getDomaine());
    l.setName("cqIsoSurface");
    l.setTitle(EbliLib.getS("Isosurfaces"));
    l.setLegende(getCqLegend());
    l.setVisible(false);
    l.putClientProperty(Action.SHORT_DESCRIPTION, TrResource
        .getS("Affiche les isosurfaces des variables �dit�es par le projet:<ul><li>s�lectionner ce calque,"
            + "</li><li>utiliser l'action <code>variables</code> pour s�lectionner une variable</li></ul>"));
    gr.add(l);
    addCalque(gr);
  }

  /**
   * La construction du groupe de calques pour les fonds de plan.
   * @return Le calque cr�e, sans ajout des sous calques.
   */
  protected BGroupeCalque buildGroupeFond() {
    final BGroupeCalque gr = new BGroupeCalque();
    gr.setActions(new EbliActionInterface[] { new FSigImageImportAction(this, gr) });
    gr.setTitle(FSigLib.getS("Fond de plan"));
    gr.setName("gpIsos");
    gr.setDestructible(false);
    return gr;
  }

  public BGroupeCalque getGroupFond() {
    return (BGroupeCalque) getDonneesCalqueByName("gpIsos");
  }

  /**
   * Retourne le calque d'isosurface contenu dans le groupe de calque fond de plan.
   * @return Le calque. <code>null</code> si ce calque n'a pas �t� cr�e.
   */
  public MvIsoPaintersLayer getLayerIsoSurface() {
    final BCalque cq = getGroupFond();
    if (cq != null) { return (MvIsoPaintersLayer) getDonneesCalqueByName("cqIsoSurface"); }
    return null;
  }

  /**
   * Retourne le groupe de calques pour les conditions limites.
   * @return Le groupe. <code>null</code> si ce groupe n'a pas �t� cr�e.
   */
  public TrBcLayerGroup getGroupBoundary() {
    return (TrBcLayerGroup) getDonneesCalqueByName("gpCl");
  }

  /**
   * @return la calque points limites
   */
  public MvFrontierPointLayer getLayerBcPoint() {
    if (getGroupBoundary() == null) { return null; }
    return getGroupBoundary().getBcPointLayer();
  }

  /**
   * @return le groupe bords limites
   */
  public TrBcBoundaryLayer getLayerBcBoundary() {
    return getGroupBoundary().getBcBoundaryLayer();
  }

  protected BGroupeCalque getGroupSI() {
    return (BGroupeCalque) getDonneesCalqueByName("grSI");
  }
  
  @Override
  public void fillMenuWithToolsActions(final JMenu _m) {
      super.fillMenuWithToolsActions(_m);
       _m.addSeparator();
    _m.add(getPointValueDisplayMenu());
  }

  
  
        

}