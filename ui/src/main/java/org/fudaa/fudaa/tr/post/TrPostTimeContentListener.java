/*
 * @creation 4 d�c. 06
 * @modification $Date: 2006-12-05 10:18:16 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.ProgressionInterface;

/**
 * @author fred deniger
 * @version $Id: TrPostTimeContentListener.java,v 1.1 2006-12-05 10:18:16 deniger Exp $
 */
public interface TrPostTimeContentListener {

  void updateTimeStep(final int[] _idx, final ProgressionInterface _prog);

}
