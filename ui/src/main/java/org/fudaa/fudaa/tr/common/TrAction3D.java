/*
 GPL 2
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuIcon;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.volume.ZVue3DPanel;
import org.fudaa.fudaa.tr.data.TrVisuPanel;

/**
 *
 * @author Frederic Deniger
 */
public class TrAction3D extends EbliActionSimple {
  final TrVisuPanel visu_;
  JFrame f_;
  final String name_;

  TrAction3D(final TrVisuPanel _visu, final String _name) {
    super(Tr3DInitialiser.get3dName(), EbliResource.EBLI.getToolIcon("3d"), Tr3DInitialiser.get3dName());
    visu_ = _visu;
    name_ = _name;
    setEnabled(true);
    super.setDefaultToolTip(EbliLib.getS("Afficher la vue 3D"));
  }

  public ZVue3DPanel getVue3D() {
    return (ZVue3DPanel) f_.getContentPane();
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (f_ == null) {
      f_ = new JFrame(super.getTitle() + " : " + name_);
      f_.setIconImage(((BuIcon) getIcon()).getImage());
      f_.addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosed(final WindowEvent _evt) {
          f_ = null;
        }
      });
      visu_.view3D(f_);
    } else {
      if (f_.getExtendedState() == Frame.ICONIFIED) {
        f_.setExtendedState(Frame.NORMAL);
      }
      f_.toFront();
    }
  }
  
}
