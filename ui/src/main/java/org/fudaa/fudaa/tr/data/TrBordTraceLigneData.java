/**
 * @creation 21 juin 2004
 * @modification $Date: 2007-05-04 14:01:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.ebli.palette.BPalettePlageDefault;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.tr.common.TrPreferences;

public class TrBordTraceLigneData {

  int idx_;
  // int maxBord_;
  Map bdTypeTlDataMap_;

  public TrBordTraceLigneData() {
  // maxBord_ = _maxBord;

  }

  /**
   * @param _t le type de bord.
   * @return l'identifiant pour la largeur.
   */
  public static String getWidthPrefString(final H2dBoundaryType _t) {
    return "boundary.type." + FuLib.clean(_t.getName()).toLowerCase() + ".w";
  }

  /**
   * @param _t le type de bord.
   * @return l'identifiant pour la couleur.
   */
  public static String getColorPrefString(final H2dBoundaryType _t) {
    return "boundary.type." + FuLib.clean(_t.getName()).toLowerCase() + ".rgb";
  }

  /**
   * @param _t le type de bord pour lequel on veut recuperer la ligne.
   * @return null si non def dans les preferences.
   */
  public static TraceLigneModel getPreferenceDataCommon(final H2dBoundaryType _t) {
    final float w = (float) TrPreferences.TR.getDoubleProperty(getWidthPrefString(_t), getMinLineThickness());
    final String rgb = TrPreferences.TR.getStringProperty(getColorPrefString(_t), null);
    if (rgb == null) {
      if (w != 2) {
        FuLog.warning(new Throwable());
      }
      return null;
    }
    int r, g, b;
    final StringTokenizer tk = new StringTokenizer(rgb, CtuluLibString.VIR);
    if (tk.countTokens() != 3) { return null; }
    r = Integer.parseInt(tk.nextToken());
    g = Integer.parseInt(tk.nextToken());
    b = Integer.parseInt(tk.nextToken());
    final Color c = new Color(r, g, b);
    return new TraceLigneModel(TraceLigne.LISSE, Math.max(w, getMinLineThickness()), c);
  }

  public void savePreferencesCommon(final H2dBoundaryType _t, final TraceLigneModel _d) {
    TrPreferences.TR.putDoubleProperty(getWidthPrefString(_t), _d.getEpaisseur());
    final Color c = _d.getCouleur();
    final String col = c.getRed() + CtuluLibString.VIR + c.getGreen() + CtuluLibString.VIR + c.getBlue();
    TrPreferences.TR.putStringProperty(getColorPrefString(_t), col);
    if (bdTypeTlDataMap_ == null) {
      bdTypeTlDataMap_ = new HashMap(6);
    }
    bdTypeTlDataMap_.put(_t, _d);
  }

  public TraceLigneModel createTlData(final H2dBoundaryType _t) {
    if (_t.isSolide()) { return new TraceLigneModel(TraceLigne.LISSE, getMinLineThickness(), new Color(164, 95, 30)); }
    final Color c = BPalettePlageDefault.getColor(idx_++);
    return new TraceLigneModel(TraceLigne.LISSE, getMinLineThickness(), c);
  }

  /**
   * @return le minimum autorise pour l'�paisseur d'un trait fronti�re.
   */
  public static float getMinLineThickness() {
    return 2F;
  }

  public TraceLigneModel getTlData(final H2dBoundaryType _bd) {
    if (bdTypeTlDataMap_ == null) {
      bdTypeTlDataMap_ = new HashMap(6);
    }
    TraceLigneModel r = (TraceLigneModel) bdTypeTlDataMap_.get(_bd);
    if (r == null) {
      r = getPreferenceDataCommon(_bd);
      if (r == null) {
        r = createTlData(_bd);
      }
      bdTypeTlDataMap_.put(_bd, r);
    }
    return r;
  }

}