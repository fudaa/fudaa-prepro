/*
 GPL 2
 */
package org.fudaa.fudaa.tr.common;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JComponent;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.animation.EbliAnimationAdapterInterface;
import org.fudaa.ebli.controle.BSelecteurListTarget;
import org.fudaa.fudaa.meshviewer.threedim.Mv3DCalque;
import org.fudaa.fudaa.tr.post.TrPostSource;

/**
 *
 * @author Frederic Deniger
 */
class Tr3DCalqueTime extends Mv3DCalque implements EbliAnimationAdapterInterface, BSelecteurListTarget, ListSelectionListener {
  boolean changedFromList_;
  ListSelectionModel select_;
  TrPostSource srcInit_;

  public Tr3DCalqueTime(final H2dVariableType _var, final TrPostSource _srcInit, final EfGridData _data, final int _initIdx) {
    super(_var, _data);
    srcInit_ = _srcInit;
    idx_ = _initIdx;
  }

  private TrPostSource getSrc() {
    return srcInit_;
  }

  @Override
  public ListModel getListModel() {
    return srcInit_.getTimeListModel();
  }

  @Override
  public ListSelectionModel getListSelectionModel() {
    if (select_ == null) {
      select_ = new DefaultListSelectionModel();
      select_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      select_.setSelectionInterval(idx_, idx_);
      select_.addListSelectionListener(this);
    }
    return select_;
  }

  @Override
  public int getNbTimeStep() {
    return srcInit_.getNbTimeStep();
  }

  @Override
  public double getTimeStepValueSec(final int _idx) {
    return srcInit_.getTimeStep(_idx);
  }

  @Override
  public boolean getRange(final CtuluRange _r) {
    getSrc().getExtrema(_r, getVarDisplayed(), null, null);
    return true;
  }

  public JComponent getTargetComponent() {
    return null;
  }

  @Override
  public boolean getTimeRange(final CtuluRange _r) {
    getSrc().getExtremaForTimeStep(_r, getVarDisplayed(), idx_, null);
    return true;
  }

  @Override
  public String getTimeStep(final int _idx) {
    return getSrc().getTimeListModel().getElementAt(_idx).toString();
  }

  @Override
  public boolean isDonneesBoiteTimeAvailable() {
    return true;
  }

  @Override
  public void setTimeStep(final int _idx) {
    idx_ = _idx;
    if (!changedFromList_ && select_ != null) {
      select_.setSelectionInterval(idx_, idx_);
    }
    affiche(false);
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    if (_e.getValueIsAdjusting()) {
      return;
    }
    changedFromList_ = true;
    if (select_.isSelectionEmpty()) {
      select_.setSelectionInterval(0, 0);
      return;
    }
    final int idx = select_.getMaxSelectionIndex();
    if (idx != idx_) {
      setTimeStep(idx);
    }
    changedFromList_ = false;
  }
  
}
