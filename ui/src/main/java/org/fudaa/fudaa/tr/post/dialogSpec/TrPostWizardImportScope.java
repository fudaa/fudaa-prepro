package org.fudaa.fudaa.tr.post.dialogSpec;

import com.memoire.bu.BuResource;
import com.memoire.bu.BuWizardDialog;
import java.awt.event.ActionEvent;
import java.io.File;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrCourbeImporter;
import org.fudaa.fudaa.tr.common.TrCourbeWizardImportScope;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostProjet;

/**
 * Wizard qui surcharge le wizard de parametrage d'import des scope dans un graphe existant.
 * 
 * @author Adrien Hadoux
 */
public class TrPostWizardImportScope extends TrCourbeWizardImportScope {

  TrPostProjet projet_;
  TrCourbeImporter.Target target_;

  public static class ImportAction extends EbliActionSimple {
    final FudaaCommonImplementation ui_;
    TrPostProjet projet_;
    TrCourbeImporter.Target target_;

    public ImportAction(final TrPostProjet projet, final TrCourbeImporter.Target target) {
      super(TrResource.getS("Importer dans le graphe"), BuResource.BU.getIcon("IMPORTER"), "IMPORTER");
      ui_ = projet.impl_;
      projet_ = projet;
      target_ = target;
    }

    public ImportAction(final TrPostProjet projet, final TrCourbeImporter.Target target,boolean replayData) {
    	 super(TrResource.getS("Importer dans le graphe"), BuResource.BU.getIcon("IMPORTER"), "IMPORTER");
         ui_ = projet.impl_;
         projet_ = projet;
         target_ = target;
    }
    
    @Override
    public void actionPerformed(final ActionEvent _e) {
      final TrPostWizardImportScope wizard = new TrPostWizardImportScope(projet_, target_);
      final BuWizardDialog DialogWizard = new BuWizardDialog(ui_.getFrame(), wizard);
      // --affichage du wizard --//
      DialogWizard.setSize(600, 600);
      DialogWizard.setLocationRelativeTo(ui_.getFrame());
      DialogWizard.setVisible(true);
    }
  }

  public TrPostWizardImportScope(final TrPostProjet projet_, final TrCourbeImporter.Target target,boolean replayData,File fileToReload) {
	    super(projet_.impl_, target,replayData,fileToReload);
	    this.projet_ = projet_;
	  }
  
  
  public TrPostWizardImportScope(final TrPostProjet projet_, final TrCourbeImporter.Target target) {
    super(projet_.impl_, target);
    this.projet_ = projet_;
  }

  @Override
  public void createGraphImported() {
  // projet_.addGrapheNodeInCurrentScene(new EGFillePanel((EGGraphe) target_), "Graphe import� (format scope)");
  }

}
