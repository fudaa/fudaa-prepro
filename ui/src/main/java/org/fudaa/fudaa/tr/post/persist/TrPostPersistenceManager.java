package org.fudaa.fudaa.tr.post.persist;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.persistence.XmlArrayList;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.fortran.DodicoDoubleArrayBinaryFileSaver;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.XstreamCustomizer;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.PaletteCourbes;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidgetGroup;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionGroup;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetCreatorVueCalque;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetFusionCalques;
import org.fudaa.ebli.visuallibrary.persist.EbliSceneSerializeXml;
import org.fudaa.ebli.visuallibrary.persist.EbliWidgetGroupSerializeXml;
import org.fudaa.ebli.visuallibrary.persist.EbliWidgetSerializeXml;
import org.fudaa.ebli.visuallibrary.persist.EbliWidgetSerializeXml.CoupleNomId;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrPaletteCourbe;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.*;
import org.fudaa.fudaa.tr.post.actions.TrPostActionFusionCalques;
import org.netbeans.api.visual.widget.Widget;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.io.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Manager qui se charge de donner les noms standart des balises, le chooser de fichiers et appel aux bonnes m�thodes pour la persistence des donn�es.
 *
 * @author Adrien Hadoux
 */
public class TrPostPersistenceManager {
  public static final String CURRENT_VERSION = "1.0";
  /**
   * Le projet a persiter
   */
  TrPostProjet mainProject;
  /**
   * Nom du projet. Creation du projet sous un repertoire nomProjet.EXTENSION
   */
  ProgressionInterface prog_;
  TrPostPersistenceFileFinder projectFileFinder;

  public void setProjet(final File projetBaseDir) {
    this.projectFileFinder = new TrPostPersistenceFileFinder(projetBaseDir);
  }

  TrPostCommonImplementation ui_;
  TrPostPersistanceErrorManager managerError = new TrPostPersistanceErrorManager();

  public TrPostPersistenceManager(final TrPostProjet projet) {
    super();
    mainProject = projet;
    ui_ = projet.impl_;
    prog_ = ui_.getMainProgression();
  }

  public void progression(final String title, final int progression) {
    if (prog_ == null || title == null || progression > 100) {
      return;
    }
    if (progression == 100) {
      prog_.setProgression(0);
      prog_.setDesc("");
      return;
    }
    prog_.setDesc(TrLib.getString(title));
    prog_.setProgression(progression);
  }

  /**
   * Cr�� le repertoire contenant tous les fichiers de donn�es. Base pour la cr�ation des donn�es.
   */
  public boolean savePersistDirectory(final boolean saveAs) {
    // -- creation d un chooser --//

    // -- init avec les param noms projet et autre --//
    if (projectFileFinder == null || saveAs) {
      final CtuluFileChooser fileChooser = new CtuluFileChooser(true);
      fileChooser.setDialogTitle(EbliResource.EBLI.getString("S�lectionnez l'emplacement de votre projet"));
      fileChooser.setAcceptAllFileFilterUsed(false);

      // -- par defaut on choisis le nom du fichier resultat + .POST --//

      if (mainProject.getSources().isNotEmpty()) {
        String indice = "";
        final File mainFile = mainProject.getSource(0).getMainFile();
        final File directory = mainFile.getParentFile();

        File fileDefaut = new File(directory, mainFile.getName()
            + TrPostPersistenceFileFinder.EXTENSION);
        if (fileDefaut.exists()) {
          int i = 1;
          do {
            fileDefaut = new File(directory, mainFile.getName() + indice
                + TrPostPersistenceFileFinder.EXTENSION);
            indice = "_" + (i++);
          } while (fileDefaut.exists() && i < 20);// Garde fou
        }

        if (directory != null && directory.exists()) {
          try {
            fileChooser.setCurrentDirectory(directory);
          } catch (Exception e) {
          }
        }
        if (fileDefaut.exists()) {
          fileChooser.setSelectedFile(fileDefaut);
        }
      }
      File conteneurProjet = null;

      fileChooser.setFileFilter(new FileFilter() {
        @Override
        public String getDescription() {
          return TrLib.getString("*.POST (Fichier POST)");
        }

        @Override
        public boolean accept(File f) {
          if (f.isDirectory()) {
            return true;
          }

          return false;
        }
      });

      final int reponse = fileChooser.showSaveDialog(CtuluLibSwing.getFrameAncestor(ui_.getParentComponent()));
      if (reponse == JFileChooser.APPROVE_OPTION) {
        conteneurProjet = new File(GetExtensionName(fileChooser.getSelectedFile().getAbsolutePath()));
      } else {
        return false;
      }

      if (conteneurProjet.exists() && !conteneurProjet.isDirectory()) {

        mainProject.impl_.error(TrResource.getS("Erreur, le fichier choisit doit �tre un r�pertoire"));
        return false;
      }

      // -- test si le fichier choisit par l'utilisateur existe djea, si oui. demander confirmation d'ecrasement --//
      if (conteneurProjet.exists()) {

        final boolean resp = mainProject.impl_.question("Fichier " + conteneurProjet.getName() + " existant",
            "Le fichier " + conteneurProjet.getName() + " existe d�j�.\n Voulez-vous l'�craser ?");
        if (!resp) {
          return false;
        }
        CtuluLibFile.deleteDir(conteneurProjet);
      }
      conteneurProjet.mkdirs();
      if (conteneurProjet.isDirectory()) {
        projectFileFinder = new TrPostPersistenceFileFinder(conteneurProjet);
        return true;
      } else {
        ui_.error(EbliResource.EBLI.getString("Impossible de cr�er le r�pertoire"));
        return false;
      }
    } else if (projectFileFinder != null) {
      // -- on sauvegarde au meme emplacement --//
      return true;
    } else {
      return false;
    }
  }

  /**
   * Retourne un nom avec extension
   *
   * @param name
   */
  public String GetExtensionName(final String name) {

    if (name.endsWith(TrPostPersistenceFileFinder.EXTENSION)) {
      return name;
    } else {
      return name + TrPostPersistenceFileFinder.EXTENSION;
    }
  }

  /**
   * Charge le repertoire contenant tous les fichiers
   */
  public boolean loadPersistDirectory(final boolean executeByExplorer, final File explorerFile) {
    File fichierSetup = null;
    // -- verification que ca ne provients pas directement de l explorer --//
    if (!executeByExplorer) {
      // -- creation d un chooser --//
      final CtuluFileChooser fileChooser = new CtuluFileChooser(true);
      fileChooser.setDialogTitle(EbliResource.EBLI.getString("S�lectionnez l'emplacement de votre r�pertoire POST"));
      fileChooser.setAcceptAllFileFilterUsed(false);
      fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      fileChooser.addChoosableFileFilter(new TrPostFileFilter.DirectoryPOSTFilter());
      // -- init avec les param noms projet et autre --//
      if (projectFileFinder != null) {
        fileChooser.setSelectedFile(projectFileFinder.getBaseDir());
      }
      final int reponse = fileChooser.showOpenDialog(CtuluLibSwing.getFrameAncestor(ui_.getParentComponent()));
      if (reponse == JFileChooser.APPROVE_OPTION) {

        // -- Recuperation du fichier du chooser --//
        fichierSetup = fileChooser.getSelectedFile();
        if (TrPostFileFilter.acceptDirectoryPOST(fichierSetup)) {
          fichierSetup = TrPostFileFilter.getSetupFormDirectoryPOST(fichierSetup);
        } else {
          ui_.error(EbliResource.EBLI.getString(
              "Impossible de charger ce r�pertoire, Soit il n'est pas de type ."
                  + TrPostFileFilter.DEFAULT_EXTENSION + "\n soit il ne contient pas de fichier setup."));
          return false;
        }
      } else {
        return false;
      }
    } else {
      fichierSetup = explorerFile;
    }

    // creation du repertoire global contenant toutes les donn�es
    if (fichierSetup == null || fichierSetup.isDirectory()) {
      ui_.error(EbliResource.EBLI.getString("Impossible de charger le r�pertoire"));
      return false;
    } else {

      // -- tentative d'ouverture du projet --//

      // -- on a le chemin vers le fichier setup.POST, il suffit d4enlever le
      // no; du fichier et on obtient le che;in du rep total --//
      String path = fichierSetup.getAbsolutePath();
      path = path.substring(0, path.lastIndexOf(TrPostPersistenceFileFinder.MAIN_SETUP_FILENAME));

      // -- verification que le fichier est bien un repertoire --//
      final File candidat = new File(path);

      if (candidat.isDirectory()) {
        projectFileFinder = new TrPostPersistenceFileFinder(candidat);
        return true;
      } else {
        return false;
      }
    }
  }

  /**
   * Fichier qui cree le fichier qui indique les diff�rents fichiers layout a lire. Cela permettra pour la lecture de proposer de charger ou non
   * certains layout
   */
  public void saveDescriptorScenes(final File file) {
    // -- outputstream du xstream --//

    // -- ecriture de la classe xstream
    try (ObjectOutputStream out = EbliLib.createObjectOutpuStream(file, getParser(true))) {

      // -- readme --//
      out.writeObject(XmlCommentaire.getCommentaireLayout());

      // -- g�n�ration du xml pour la scene --//
      final List<TrPostLayoutFille> listeFilles = mainProject.impl_.getAllLayoutFille();
      final int nbFichiersToGenrate = listeFilles.size();

      out.writeInt(nbFichiersToGenrate);
      for (int i = nbFichiersToGenrate - 1; i >= 0; i--) {
        // creation du fichier de desciption des scenes dans le repertoire de meme nom.
        final String nomLayout = FuLib.clean(listeFilles.get(i).previousTitleFrame);
        final String nomFichierLayout = nomLayout + TrPostPersistenceFileFinder.EXTENSION_REPLAYOUT + File.separator
            + TrPostPersistenceFileFinder.LAYOUT_CONFIG_FILENAME;

        out.writeObject(new LayoutDescriptor(nomFichierLayout));
      }
    } catch (final IOException e) {
    }
  }

  /**
   * Lis les fichiers des layouts a utiliser.
   *
   * @param file
   */
  public List<File> loadDescriptorScenes(final File file) {

    if (!file.canRead()) {
      managerError.addMessageError("FilePath " + file.getPath() + " du descriptor introuvable");
      return new ArrayList<>();
    }
    // -- outputstream du xstream --//
    final List<File> listeFiles = new ArrayList<File>();

    try (ObjectInputStream in = EbliLib.createObjectInpuStream(file, getParser(isNewVersion))) {

      int nbFichiersToGenrate;
      // -- lecture readme --//
      final Object debutFichier = in.readObject();

      if (debutFichier instanceof XmlCommentaire) {
        // -- g�n�ration du xml pour la scene --//
        nbFichiersToGenrate = in.readInt();
      } else {
        nbFichiersToGenrate = ((Integer) debutFichier).intValue();
      }
      for (int i = 0; i < nbFichiersToGenrate; i++) {

        // //-- recuperation du path relatif vers le fichier de la scene --//
        final String pathRelatif = ((LayoutDescriptor) in.readObject()).pathRelatifLayout;
        // // creation du fichier de desciption des scenes
        listeFiles.add(projectFileFinder.getPath(pathRelatif));
      }
    } catch (final FileNotFoundException e) {
      managerError.addMessageError("infolayout: path " + file.getPath() + " invalide. Impossible de charger des layouts");
    } catch (final IOException e) {
      messageError(false);
    } catch (final ClassNotFoundException e) {
      messageError(false);
    }

    return listeFiles;
  }

  public static class LayoutSaveCallable implements Callable<List<CoupleNomId>> {
    private final TrPostLayoutFille fille;
    private final TrPostPersistenceFileFinder fileFinder;
    final HashMap<String, Object> parametres;

    public LayoutSaveCallable(TrPostLayoutFille fille, TrPostPersistenceFileFinder fileFinder,
                              HashMap<String, Object> initParameters) {
      this.fille = fille;
      this.fileFinder = fileFinder;
      this.parametres = new HashMap<String, Object>(initParameters);
      parametres.put("dimensions", fille.getSize());
      parametres.put("location", fille.getLocation());
      parametres.put("scene", fille.getScene());
    }

    @Override
    public List<CoupleNomId> call() throws Exception {
      List<CoupleNomId> res = new ArrayList<CoupleNomId>();
      try {
        final String title = fille.previousTitleFrame;
        final EbliScene scene = fille.getScene();
        final String nomLayout = FuLib.clean(fille.previousTitleFrame) + TrPostPersistenceFileFinder.EXTENSION_REPLAYOUT;
        File layoutDir = fileFinder.getPath(nomLayout);
        savePersitSceneXml(fileFinder, scene, title, layoutDir, parametres, res, nomLayout);
      } catch (Exception e) {
        FuLog.error(e);
      }
      return res;
    }
  }

  /**
   * Recuperation de la liste des variables.
   *
   * @param init: si on veut ecraser (sauvegarder) mettre le booleen a true.
   * @param path
   */
  List<TrPostUserVariableSaver> getPersitantVariablesList(final String path, final boolean init) {

    final List<TrPostUserVariableSaver> liste = new XmlArrayList(
        new VariablesStreamStrategy(new File(path), mainProject.getSources().getLstSources()));

    return liste;
  }

  private void clearDatas(final List<TrPostUserVariableSaver> list) {
    // CtuluRemoveContentDirectory.contentDirectoryRemover(getDirectoryCurves());
    if (list == null) {
      return;
    }
    for (final Iterator<TrPostUserVariableSaver> it = list.iterator(); it.hasNext(); ) {
      it.next();
      it.remove();
    }
  }

  /**
   * Methode qui g�n�re le fichier contenant toutes les sources ouverts.
   */
  public void saveSourceAndVariables(final File file, final File pathVariables) {

    // -- outputstream du xstream --//
    // -- g�n�ration du xml pour la scene --//
    final List<TrPostSourcePersist> listeCouplesIdSources = new ArrayList<TrPostSourcePersist>();
    final List<TrPostUserSaverContent> listeVar = new ArrayList<TrPostUserSaverContent>();
    final List<TrPostTimeModelSaver> listePdt = new ArrayList<TrPostTimeModelSaver>();
    // -- sauvegarde des source composite (SUITE CALCUL....) --//
    final List<TrPostUserVariableSaver> listeVarSuites = new ArrayList<TrPostUserVariableSaver>();
    final List<TrPostTimeModelSaver> listePdtSuites = new ArrayList<TrPostTimeModelSaver>();

    final List<TrPostSourcePersistComposite> listeSuiteCalcul = new ArrayList<TrPostSourcePersistComposite>();

    try (ObjectOutputStream out = EbliLib.createObjectOutpuStream(file, getParser(true))) {
      if (!file.exists()) {
        file.getParentFile().mkdirs();
      }

      // -- redame --//
      out.writeObject(XmlCommentaire.getCommentaireSources());

      // -- enregistrement des fichiers sources --//
      final List<TrPostSource> listeSrcClassic = mainProject.getAllClassicalSource();
      int nbElements = listeSrcClassic.size();
      out.writeInt(nbElements);

      // on commence par d�livrer des id aux sources:
      for (final Iterator<TrPostSource> it = listeSrcClassic.iterator(); it.hasNext(); ) {
        final TrPostSource src = it.next();
        mainProject.deliverSourceId(src);
        listeCouplesIdSources.add(new TrPostSourcePersist(src, projectFileFinder.getBaseDir()));
      }
      for (final Iterator<TrPostSource> it = listeSrcClassic.iterator(); it.hasNext(); ) {
        final TrPostSource src = it.next();
        // -- sauveagrde de la variable correspondante --//
        final TrPostUserSaverContent var = TrPostUserVariableSaver.createSaver((TrPostSourceAbstract) src, null);
        listeVar.add(var);
        listePdt.add(new TrPostTimeModelSaver(src));
      }

      // -- sauvegarde du fichier contenant les liens path/Sources --//
      out.writeObject(listeCouplesIdSources);
      final List<TrPostSourceReplayPersist> buildSources = new ArrayList<TrPostSourceReplayPersist>();
      final List<TrPostSourceBuilt> lstSrcBuilt = mainProject.getSources().getLstSrcBuilt();
      for (TrPostSourceBuilt trPostSourceBuilt : lstSrcBuilt) {
        buildSources.add(trPostSourceBuilt.getReplay());
      }
      out.writeObject(buildSources);

      final List<TrPostSourceFromReader> listeSrcComposite = mainProject.getAllCCompositeSource();
      nbElements = listeSrcComposite.size();
      out.writeInt(nbElements);

      final Iterator<TrPostSourceFromReader> it2 = listeSrcComposite.iterator();

      while (it2.hasNext()) {
        final TrPostSourceFromReader src = it2.next();
        listeSuiteCalcul.add(new TrPostSourcePersistComposite(src.getReader(), src.getId()));

        // -- on enregistre les var --//
        final TrPostUserVariableSaver var = TrPostUserVariableSaver.createSaver((TrPostSourceAbstract) src, null).getSaver();
        listeVarSuites.add(var);
        listePdtSuites.add(new TrPostTimeModelSaver(src));
      }

      // -- sauvegarde du fichier contenant les liens path/Sources --//
      out.writeObject(listeSuiteCalcul);
    } catch (final IOException e) {
      FuLog.error(e);
      messageError(true);
    }
    // -- sauvegarde dans le repertoire variable c un fichier par donnees
    // persistantes --//
    pathVariables.mkdirs();
    for (int i = 0; i < listeVar.size(); i++) {
      final TrPostUserSaverContent var = listeVar.get(i);
      final TrPostTimeModelSaver ptd = listePdt.get(i);
      XStream parser = getParser(true);
      parser.omitField(TrPostUserVariableSaver.class, "g__");
      String idSource = listeCouplesIdSources.get(i).IdSource;
      File xmlFile = getVariableFile(pathVariables, idSource);
      try (ObjectOutputStream out = parser.createObjectOutputStream(new FileWriter(xmlFile))) {
        out.writeObject(var.getSaver());
        out.writeObject(ptd);
      } catch (final IOException e) {
        FuLog.error(e);
        messageError(true);
      }
      Map<String, double[]> persistData = var.getPersistData();
      if (!persistData.isEmpty()) {
        File binFile = getVariableBinFile(pathVariables, idSource);
        DodicoDoubleArrayBinaryFileSaver recordSaver = new DodicoDoubleArrayBinaryFileSaver(binFile);
        recordSaver.save(persistData);
      }
    }

    // -- enregistrement des var et pdt pour les suites --//
    for (int i = 0; i < listeVarSuites.size(); i++) {
      final TrPostUserVariableSaver var = listeVarSuites.get(i);
      final TrPostTimeModelSaver ptd = listePdtSuites.get(i);
      XStream parser = getParser(true);
      parser.omitField(TrPostTimeModelSaver.class, "fmtPattern__");
      try (ObjectOutputStream out = parser.createObjectOutputStream(new FileWriter(getVariableFile(pathVariables, listeSuiteCalcul.get(i).Id)))) {
        out.writeObject(var);
        out.writeObject(ptd);
      } catch (final IOException e) {
        FuLog.error(e);
        messageError(true);
      }
    }
  }

  protected static File getVariableBinFile(final File pathVariables, String idSource) {
    return new File(pathVariables, "variables_" + idSource + ".bin");
  }

  /**
   * Methode qui ouvre tous les sources et ouvre le trpostProjets correspondants
   *
   * @param file
   */
  public boolean loadSource(final File file, final HashMap<String, Object> parametres) {

    boolean reussite = true;
    // -- outputstream du xstream --//
    ObjectInputStream in = null;
    try {
      in = EbliLib.createObjectInpuStream(file, getParser(isNewVersion));

      // -- recuperation du multiSourceActivator--//
      final TrPostMultiSourceActivator2 multiProjectActivator = new TrPostMultiSourceActivator2(mainProject);

      // -- lecture des commentaires --//
      final Object debutFichier = in.readObject();
      int nbSources;
      if (!(debutFichier instanceof XmlCommentaire)) {
        nbSources = ((Integer) debutFichier).intValue();
      } else {
        // -- lecture nb sources pour les sources --//
        nbSources = in.readInt();
      }

      // -- lecture de la liste des objets source perssitant --//
      final List<TrPostSourcePersist> listeCouplesIdSources = (List<TrPostSourcePersist>) in.readObject();

      // -- liste qui contient l'ensemble des fichiers � recharger pour les vues 2d et vues 1d
      // final List<String> listeReloadSource = new ArrayList<String>();
      nbSources = Math.min(nbSources, listeCouplesIdSources.size());
      for (int i = 0; i < nbSources; i++) {

        TrPostSourcePersist sourcePersist = listeCouplesIdSources.get(i);
        // -- tentative de lecture du path relatif --//
        List<File> notFoundFiles = new ArrayList<File>();
        List<File> sourceFiles = getFilesSources(sourcePersist, notFoundFiles);
        // pas de fichier trouv�
        if (!notFoundFiles.isEmpty()) {
          messageError(true);
          reussite = false;
          for (File notFound : notFoundFiles) {
            managerError.addMessageError("setup: path " + notFound.getAbsolutePath() + " invalide. Fichier r�sultat non charg�");
          }
        }
        // pas de fichier a charger on retourne
        if (sourceFiles.isEmpty()) {
          return false;
        }

        TrPostSource findSource = mainProject.getSources().findSource(sourceFiles);

        if (findSource != null) {
          mainProject.getImpl().warn(TrLib.getString("Solution d�j� charg�e"),
              TrLib.getString("Le fichier source existe d�j� et ne peut �tre ajout� au projet."));
          // --ACHTUNG !!! --//
          // -- mise a jour de l'ID necessaire afin de charger correctement la suite --//
          findSource.setId(sourcePersist.IdSource);
        } else {
          // -- tentative de chargement du source dans le multi-projet --//
          multiProjectActivator.active(sourceFiles, mainProject.impl_, sourcePersist.IdSource);
        }
      }

      Object readObject = in.readObject();
      int nbSuitesCalcul = 0;
      if (readObject instanceof List) {
        final List<TrPostSourceReplayPersist> lstReplay = (List<TrPostSourceReplayPersist>) readObject;
        CtuluAnalyze log = new CtuluAnalyze();
        if (lstReplay != null) {
          for (TrPostSourceReplayPersist srcReplay : lstReplay) {
            TrPostSource replay = srcReplay.replay(mainProject, log);
            if (replay != null) {
              mainProject.getSources().addSourceBuilt((TrPostSourceBuilt) replay);
            }
          }
        }
        if (!log.isEmpty()) {
          mainProject.getImpl().manageAnalyzeAndIsFatal(log);
        }
        nbSuitesCalcul = in.readInt();
      } else {
        // -- lecture nb suites de calcul pour les sources --//
        nbSuitesCalcul = ((Integer) readObject).intValue();
      }

      // -- lecture de la liste des objets source perssitant --//
      if (nbSuitesCalcul > 0) {
        final List<TrPostSourcePersistComposite> listeSuitesCalculs = (List<TrPostSourcePersistComposite>) in.readObject();

        for (final TrPostSourcePersistComposite composite : listeSuitesCalculs) {
          composite.generateComposite(mainProject);
        }
      }
    } catch (final Exception e) {
      messageError(false);
    } finally {
      try {
        if (in != null) {
          in.close();
        }
      } catch (final IOException e) {
        messageError(false);
      }
    }
    return reussite;
  }

  /**
   * @param sourcePersist
   * @param searchFile liste a remplir avec les fichiers attendus.
   */
  private List<File> getFilesSources(TrPostSourcePersist sourcePersist, List<File> searchFile) {
    int length = sourcePersist.pathRelatifSource.length;
    List<File> res = new ArrayList<File>(length);
    List<File> searchedFile = new ArrayList<File>(length);
    for (int i = 0; i < length; i++) {
      File f = CtuluLibFile.getAbsolutePathnameTo(projectFileFinder.getBaseDir(), new File(sourcePersist.pathRelatifSource[i]));
      if (!f.exists()) {
        f = new File(sourcePersist.pathSource[i]);
      }
      if (f.exists()) {
        res.add(f);
      } else {
        searchedFile.add(f);
      }
    }
    return res;
  }

  public void loadVariables(final File pathVariables, final ProgressionInterface prog, boolean forceRestore) {
    // -- recuperation de la listed es variables persistantes --//
    for (final TrPostSource src : mainProject.getSources().getLstSources()) {

      final File fileVariables = getVariableFile(pathVariables, src.getId());

      // -- tous les fichiers variables ne sont surement pas cr�es, donc on teste --//
      if (fileVariables.canRead()) {
        ObjectInputStream in = null;
        try {
          in = EbliLib.createObjectInpuStream(fileVariables, getParser(isNewVersion));
          Object var = in.readObject();
          if (var instanceof TrPostUserVariableSaver) {
            Map<String, double[]> savedValues = new HashMap<>();
            if (!forceRestore) {
              final File fileVariablesBin = getVariableBinFile(pathVariables, src.getId());
              if (fileVariablesBin.exists()) {
                savedValues = new DodicoDoubleArrayBinaryFileSaver(fileVariablesBin).load();
              }
            }
            final TrPostUserVariableSaver varUser = (TrPostUserVariableSaver) var;
            varUser.restoreG();
            final CtuluAnalyze restore = varUser.restore(src, new CtuluAnalyze(), prog, ui_, mainProject.getSources(), savedValues);
            if (restore != null && restore.isNotEmpty()) {
              ui_.manageAnalyzeAndIsFatal(restore);
            }
          }

          var = in.readObject();
          if (var instanceof TrPostTimeModelSaver) {
            final TrPostTimeModelSaver varUser = (TrPostTimeModelSaver) var;
            varUser.restoreFmtPattern();
            varUser.restore(src, ui_.createProgressionForMainPanel(), true);
          }
        } catch (final Exception e) {
          FuLog.error(e);
        } finally {
          try {
            if (in != null) {
              in.close();
            }
          } catch (final IOException e) {
            FuLog.error(e);
          }
        }
      }
    }
  }

  private File getVariableFile(final File pathVariables, String id) {
    return new File(pathVariables, "variables_" + id);
  }

  /**
   * Methode qui peut etre appel�e pour nettoyer le projet
   */
  public void clearProject() {
    mainProject.removeAllSources();
    for (final JInternalFrame frame : mainProject.getImpl().getAllInternalFrames()) {
      frame.dispose();
    }

    // -- retire le nom de fichier du precedent projet POST --//
    projectFileFinder = null;
  }

  /**
   * Methode qui tente de sauvegarder tous les fichiers: Soit tout les fichiers li�s entre eux sont sauvegard�s, soit les fichier li�s ne le sont
   * pas(destruction si n�cessaire)
   */
  public boolean saveProject(final boolean saveAs) {
    this.ui_.setGlassPaneStop();
    boolean res;
    try {
      final HashMap<String, Object> parametres = new HashMap<String, Object>();
      parametres.put(XstreamCustomizer.PARAMETER_ID, new TrPostPersistenceXstreamConfiguration.CustomXstreamCustomizer(true));

      progression("Cr�ation du r�pertoire", 10);
      res = false;
      // -- etape init creation du repertoire global --//
      if (savePersistDirectory(saveAs)) {
        CtuluLibFile.deleteDir(projectFileFinder.getBaseDir());

        // -- etape 1: creation du repertoire des graphe et calques --//
        final File fileVar = projectFileFinder.getVariablesDir();
        fileVar.mkdirs();
        projectFileFinder.saveVersion(CURRENT_VERSION);
        // -- etape 2 sauvegarde du fichier contenant tous les sources ouverts
        progression("Cr�ation du fichier descripteur de sources", 15);
        File mainSetupFile = projectFileFinder.getMainSetupFile();
        saveSourceAndVariables(mainSetupFile, fileVar);

        // -- etape 3 sauvegarde des ebliscene unitairement --//
        progression("Cr�ation des fichiers layout", 20);

        // -- creation de la list des noms des widgets
        final ArrayList<EbliWidgetSerializeXml.CoupleNomId> listeNomsComposants = new ArrayList<>();

        final List<TrPostLayoutFille> listFilles = mainProject.impl_.getAllLayoutFille();
        TrPostPersistenceFileFinder fileFinder = projectFileFinder;
        List<LayoutSaveCallable> save = new ArrayList<LayoutSaveCallable>();
        for (final TrPostLayoutFille fille : listFilles) {
          save.add(new LayoutSaveCallable(fille, fileFinder, parametres));
        }
        if (save.size() == 1) {
          try {
            listeNomsComposants.addAll(save.get(0).call());
          } catch (Exception exception) {
            FuLog.error(exception);
          }
        } else if (save.size() > 1) {
          ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(Math.min(Runtime.getRuntime().availableProcessors(),
              save.size()));
          try {
            List<Future<List<CoupleNomId>>> invokeAll = newFixedThreadPool.invokeAll(save);
            for (Future<List<CoupleNomId>> future : invokeAll) {
              List<CoupleNomId> list = future.get();
              if (list != null) {
                listeNomsComposants.addAll(list);
              }
            }
          } catch (Exception ex) {
            FuLog.error(ex);
          } finally {
            newFixedThreadPool.shutdown();
          }
        }
        // -- etap 4 ecriture du fichier des noms --//
        mainSetupFile = projectFileFinder.getMainWidgetDir();
        sauverNomsFrames(mainSetupFile, listeNomsComposants);

        //-- etape 5 - ecriture des commons properties si il y a --//
        if (mainProject.getCommonProperties() != null) {
          File commonFile = projectFileFinder.getCommonDir();
          sauverCommon(commonFile, mainProject.getCommonProperties());
        }
        // -- etape finale: sauvegarde du fichier de descriptions des layout
        // --//
        progression("Cr�ation du descripteur de layout", 90);
        mainSetupFile = projectFileFinder.getMainLayoutDir();
        saveDescriptorScenes(mainSetupFile);
        TrPostChecksumWriter checksum = new TrPostChecksumWriter(projectFileFinder, mainProject);
        checksum.write();
        progression("", 100);

        ui_.message(TrResource.getS("Sauvegarde termin�e"),
            TrResource.getS("Sauvegarde termin�e dans le dossier {0}", projectFileFinder.getBaseDir().getName()), true);

        ui_.setTitle("POST: " + projectFileFinder.getBaseDir().getAbsolutePath());
        res = true;
        mainProject.setProjectNotModified();
      }
    } finally {
      this.ui_.unsetGlassPaneStop();
    }
    return res;
  }

  public void messageError(final boolean save) {
    if (save) {
      ui_.error(TrResource.getS("Erreur dans la sauvegarde du projet"));
    } else {
      ui_.error(TrResource.getS("Erreur dans le chargement du projet"));
    }
  }

  /**
   * methode qui sauvegarde tous les noms des widgets dans un meme fichier facile d acces.
   *
   * @param file
   */
  public void sauverCommon(final File file, final CommonProperties properties) {
    // -- outputstream du xstream --//
    try (ObjectOutputStream out = EbliLib.createObjectOutpuStream(file, getParser(true))) {
      mkdirParent(file);
      out.writeObject(properties);
    } catch (final IOException e) {
      FuLog.error(e);
      messageError(true);
    }
  }

  /**
   * methode qui sauvegarde tous les noms des widgets dans un meme fichier facile d acces.
   *
   * @param file
   * @param liste use ArrayList as it's serializable
   */
  public void sauverNomsFrames(final File file, final ArrayList<EbliWidgetSerializeXml.CoupleNomId> liste) {
    // -- outputstream du xstream --//
    try (ObjectOutputStream out = EbliLib.createObjectOutpuStream(file, getParser(true))) {
      mkdirParent(file);

      // --readme --//
      out.writeObject(XmlCommentaire.getCommentaireName());

      out.writeObject(liste);
    } catch (final IOException e) {
      FuLog.error(e);
      messageError(true);
    }
  }

  private void mkdirParent(final File file) {
    if (file.getParentFile() != null) {
      file.getParentFile().mkdirs();
    }
  }

  /**
   * Read common properties
   *
   * @param file
   * @author Adrien Hadoux
   */
  public CommonProperties loadCommonProperties(final File file) {
    if (!file.exists()) {
      FuLog.error("no properties found");
      return null;
    }
    try (ObjectInputStream in = EbliLib.createObjectInpuStream(file, getParser(isNewVersion))) {
      // --readme --//
      final Object data = in.readObject();
      if (data instanceof CommonProperties) {
        return (CommonProperties) data;
      } else {
        return null;
      }
    } catch (final Exception e) {
      FuLog.error(e);
      messageError(false);
    }
    return null;
  }

  /**
   * methode qui sauvegarde tous les noms des widgets dans un meme fichier facile d acces.
   */
  public List<EbliWidgetSerializeXml.CoupleNomId> loadNomsFrames(final File file) {
    List<EbliWidgetSerializeXml.CoupleNomId> liste = null;
    // -- outputstream du xstream --//
    if (!file.exists()) {
      FuLog.error("no frame found");
      return Collections.emptyList();
    }
    try (ObjectInputStream in = EbliLib.createObjectInpuStream(file, getParser(isNewVersion))) {
      // --readme --//
      final Object debuTfichier = in.readObject();
      if (debuTfichier instanceof XmlCommentaire) {
        liste = (List<CoupleNomId>) in.readObject();
      } else {
        liste = (List<CoupleNomId>) debuTfichier;
      }
    } catch (final Exception e) {
      FuLog.error(e);
      messageError(false);
    }
    return liste;
  }

  private boolean isNewVersion;

  public void beforeLoading() {
    //-- desactivate automatic curve rendering --//
    EGGroup.paletteTraceAuto = null;
    PaletteCourbes.nbDifferentGroups = 0;
  }

  public void afterLoading() {
    EGGroup.paletteTraceAuto = TrPaletteCourbe.getInstance();
  }

  /**
   * Methode qui charge le projet,l'ensemble des sources et toutes les widgets dans les layouts.
   */
  public void loadProject(final boolean executeByExplorer, final File explorerFile, final boolean forceRecompute) {
    managerError.clear();
    beforeLoading();

    progression("Chargement du projet", 0);
    // -- on elimine les doublons potentiels des frames layouts --//
    ui_.elimineDoublonNomsLayout();
    ui_.setGlassPaneStop();
    progression("Lecture du r�pertoire", 10);
    // -- etape 1 lecture du repertoire global ou du fichier param --//

    if (!loadPersistDirectory(executeByExplorer, explorerFile)) {
      return;
    }

    Runnable r = new Runnable() {
      @Override
      public void run() {

        try {
          // -- etape 2 sauvegarde du fichier contenant tous les sources ouverts
          progression("Chargement du fichier descripteur de sources", 15);
          File mainSetupFile = projectFileFinder.getMainSetupFile();
          isNewVersion = projectFileFinder.getVersion() != null;//pour l'instant qu'une version...

          final HashMap<String, Object> parametres = new HashMap<String, Object>();
          parametres.put(XstreamCustomizer.PARAMETER_ID, new TrPostPersistenceXstreamConfiguration.CustomXstreamCustomizer(isNewVersion));
          if (!loadSource(mainSetupFile, parametres)) {
            managerError.addMessageError(
                "Veuillez corriger les chemins des fichiers r�sultats dans le fichier setup et r�it�rez l'op�ration.");
            managerError.showDialog(ui_);
            return;
          }
          boolean recomputeAllData = forceRecompute;
          if (!forceRecompute) {
            TrPostChecksumTester tester = new TrPostChecksumTester(projectFileFinder, mainProject);
            tester.testFiles();
            if (tester.isModified()) {
              recomputeAllData = ui_.question(
                  TrLib.getString("Recalculer les donn�es"),
                  TrLib.getString(
                      "Des modifications ont �t� d�tect�es dans les fichiers de configuration. Voulez-vous que les variables, trajectoires,.. soient recalcul�es ?"));
            }
          }
          // -- etape 6; lecture des variables --//
          progression("Lecture variables", 20);

          final File pathVariables = projectFileFinder.getVariablesDir();
          loadVariables(pathVariables, prog_, recomputeAllData);
          // -- etape 3: sauvegarde du fichier de descriptions des layout
          progression("Chargement layout", 30);
          final List<File> listeFichiers = loadDescriptorScenes(projectFileFinder.getMainLayoutDir());
          // -- etape 4 chargement des ebliscene unitairement --//
          progression("Affichage des vues", 40);
          parametres.put("path", projectFileFinder.getBaseDir().getAbsolutePath());
          parametres.put("ui", ui_);
          parametres.put(TrPostReloadParameter.POST_PROJET, mainProject);
          parametres.put("errorMsg", managerError.listeMessageError);
          parametres.put(TrPostReloadParameter.RECOMPUTE, Boolean.valueOf(recomputeAllData));
          // -- etape 5 lecture de la liste des couples nomsfRAME /ID --//
          final List<EbliWidgetSerializeXml.CoupleNomId> listeCouplesNoms = loadNomsFrames(projectFileFinder.getMainWidgetDir());
          SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
              displayInternalFrames(parametres, listeFichiers, listeCouplesNoms);
              mainProject.setProjectNotModified();
              ui_.unsetGlassPaneStop();
              afterLoading();
            }
          });

          //-- etape 6: lecture des donn�es common si elles existent --//
          File fileCommon = projectFileFinder.getCommonDir();
          if (fileCommon.exists()) {
            CommonProperties properties = loadCommonProperties(fileCommon);
            if (properties != null) {
              mainProject.setCommonProperties(properties);
            }
          }
        } catch (Exception e) {
          FuLog.error(e);
        }
      }
    };
    new Thread(r).start();
  }

  private void displayInternalFrames(final HashMap<String, Object> parametres, final List<File> listeFichiers,
                                     final List<EbliWidgetSerializeXml.CoupleNomId> listeCouplesNoms) {
    for (final File file : listeFichiers) {
      if (listeFichiers.size() != 0) {
        progression("chargement du fichier  " + file.getName(), 40 + 60 / listeFichiers.size());
      }

      // -- creation de la trpostlayoutFille --//
      final TrPostLayoutFille postFrame = new TrPostLayoutFille(mainProject);
      // -- sauvegarde du persitant --//

      mainProject.getImpl().addInternalFrame(postFrame);

      loadPersitSceneXml(file, postFrame, parametres, listeCouplesNoms);
    }

    progression("", 100);
    managerError.showDialog(ui_);
    // ui_.message(TrResource.getS("Chargement termin�"), TrResource.getS("Chargement termin� avec succ�s"), false);

    // -- mettre a jour l'interface de egstion des fichiers--//
    mainProject.filleProjetctManager_.update(null, null);

    mainProject.impl_.setTitle("POST: " + projectFileFinder.getBaseDir().getAbsolutePath());
  }

  private XStream parser_;

  private XStream getParser(boolean newVersion) {
    if (parser_ == null) {
      parser_ = TrPostPersistenceXstreamConfiguration.createXmlParser(newVersion);
    }
    return parser_;
  }

  private static void buildMapWithDataPath(TrPostPersistenceFileFinder projectFileFinder, final Map parametres,
                                           final String repertoireLayout) {
    final File repertoireLayoutFile = projectFileFinder.getPath(repertoireLayout);
    repertoireLayoutFile.mkdirs();
    final File dirGraphes = new File(repertoireLayoutFile, TrPostPersistenceFileFinder.REP_GRAPHES);
    dirGraphes.mkdirs();
    final File dirCalques = new File(repertoireLayoutFile, TrPostPersistenceFileFinder.REP_CALQUES);
    dirCalques.mkdirs();
    final File dirText = new File(repertoireLayoutFile, TrPostPersistenceFileFinder.REP_TEXTES);
    dirText.mkdirs();
    // -- creation des params pour les datas --//
    parametres.put("path", projectFileFinder.getBaseDir().getAbsolutePath());
    parametres.put("pathLayout", repertoireLayoutFile.getAbsolutePath());
    parametres.put("pathGraphes", dirGraphes.getAbsolutePath());
    parametres.put("pathCalques", dirCalques.getAbsolutePath());
    parametres.put("pathTexte", dirText.getAbsolutePath());
  }

  /**
   * Utilise xstream pour serializer les parametres scene, widget
   */
  protected static void savePersitSceneXml(TrPostPersistenceFileFinder projectFileFinder, final EbliScene scene,
                                           final String title, File file, final Map parametres,
                                           final List<EbliWidgetSerializeXml.CoupleNomId> listeNomsComposants,
                                           final String titreRepertoireLayout) {

    // -- creation du repertoire du layout layoutN.POST--//
    file.mkdirs();

    // -- remplissage de la map avec les r�pertoires calques/graphes/textes --//
    buildMapWithDataPath(projectFileFinder, parametres, titreRepertoireLayout);

    // -- mise en place du fichier decrivant le contenu du layout --//

    file = new File(file, TrPostPersistenceFileFinder.LAYOUT_CONFIG_FILENAME);// DESCRIPTORSCENE + cpt + EXTENSION);

    // -- outputstream du xstream --//
    ObjectOutputStream out = null;
    try {
      out = EbliLib.createObjectOutpuStream(file, TrPostPersistenceXstreamConfiguration.createXmlParser(true));

      // -- g�n�ration du xml pour la scene --//
      out.writeObject(new EbliSceneSerializeXml(scene, title, (Dimension) parametres.get("dimensions"), (Point) parametres.get(
          "location")));

      // -- g�n�ration du xml pour les eblinode/widgets --//
      // -- ACHTUNG!!! IL FAUT RECUPERER LES OBJETS VIA CHILDREN POUR PRESERVER L
      // ORDRE DE SUPERPOSITION!!! --//
      final Iterator<Widget> it = scene.getLayerVisu().getChildren().iterator();

      // -- creation d une liste particuliere pour les groupes--//
      final List<EbliWidgetGroupSerializeXml> listeGroupes = new ArrayList<EbliWidgetGroupSerializeXml>();

      while (it.hasNext()) {
        final Widget widget = it.next();

        if (widget instanceof EbliWidgetGroup /*
         * || widget instanceof EbliWidgetFusionCalques
         */) {
          // -- cas widget group, il faut recuper ses child --//
          final EbliNode nodeGroupe = (EbliNode) scene.findObject(widget);
          listeNomsComposants.add(new EbliWidgetSerializeXml.CoupleNomId(nodeGroupe.getTitle(), nodeGroupe.getWidget().getId()));
          // ecriture de l objet
          if (nodeGroupe != null) {
            listeGroupes.add(new EbliWidgetGroupSerializeXml(nodeGroupe, parametres, (widget instanceof EbliWidgetFusionCalques)));
          }

          for (final Widget child : widget.getChildren()) {
            final EbliNode node = (EbliNode) scene.findObject(child);
            // ecriture de l objet
            if (node != null) {

              out.writeObject(new EbliWidgetSerializeXml(node, parametres));
              listeNomsComposants.add(new EbliWidgetSerializeXml.CoupleNomId(node.getTitle(), node.getWidget().getId()));
            }
          }
        } else {
          final EbliNode node = (EbliNode) scene.findObject(widget);
          // ecriture de l objet
          if (node != null) {

            out.writeObject(new EbliWidgetSerializeXml(node, parametres));
            listeNomsComposants.add(new EbliWidgetSerializeXml.CoupleNomId(node.getTitle(), node.getWidget().getId()));
          }
        }
      }

      // -- ecriture des groupes --//
      out.writeInt(listeGroupes.size());
      for (final EbliWidgetGroupSerializeXml serializeGroupe : listeGroupes) {
        out.writeObject(serializeGroupe);
      }
    } catch (final IOException e) {
      FuLog.error(e);
    } finally {
      CtuluLibFile.close(out);
    }
  }

  /**
   * Methode qui retourne le nom correspondant de la widget
   *
   * @param listeCouplesNoms
   * @param idWidget
   */
  public String getNodeName(final List<EbliWidgetSerializeXml.CoupleNomId> listeCouplesNoms, final String idWidget) {

    for (final CoupleNomId couple : listeCouplesNoms) {
      if (couple.id.equals(idWidget)) {
        return couple.nom;
      }
    }
    this.managerError.addMessageError(TrPostPersistenceFileFinder.MAIN_WIDGET_NAMES_FILENAME
        + ": Il n'existe pas de nom correspondant � la frame d'ID " + idWidget);
    return "";
  }

  /**
   * Methode qui recupere les infos de la scene et la reconstruit a partir de celle vierge fourni en param d'entr�e.
   *
   * @param file le fichier qui contient les infos xml
   */
  private void loadPersitSceneXml(final File file, final TrPostLayoutFille fille, final Map parametres,
                                  final List<EbliWidgetSerializeXml.CoupleNomId> listeCouplesNoms) {

    if (!file.canRead()) {
      managerError.addMessageError("Repertoire layout introuvable, " + file.getPath() + " incorrect");
      return;
    }

    final String nomRepertoireLayout = file.getPath().substring(0, file.getPath().lastIndexOf(
        TrPostPersistenceFileFinder.LAYOUT_CONFIG_FILENAME));
    final EbliScene scenToUpdate = fille.getScene();
    parametres.put("scene", scenToUpdate);
    parametres.put("pathLayout", nomRepertoireLayout);

    // -- inputstream du xstream --//
    try (ObjectInputStream in = EbliLib.createObjectInpuStream(file, getParser(isNewVersion))) {

      // -- r�cup�ration de la scene xml --//
      final EbliSceneSerializeXml sceneXml = (EbliSceneSerializeXml) in.readObject();

      // mise a jour des infos
      sceneXml.configureSceneWith(scenToUpdate);
      if (sceneXml.getWidth() > 0 && sceneXml.getHeight() > 0) {
        fille.setSize(sceneXml.getWidth(), sceneXml.getHeight());
      }
      fille.setTitle(sceneXml.getTitle());
      fille.setLocation(sceneXml.getX(), sceneXml.getY());

      // -- preparation de la liste de selection des frames dans le layout --//
      final Set<Object> listeNodeSelect = new HashSet<Object>();

      // -- preparation d'une structure interm�diaire pour optimiser --//
      // -- la complexit� de calcul des groupes (passer en complexit� lin�aire)
      // --//
      final HashMap<String, HashSet<EbliNode>> listeGroupToPerform = new HashMap<>();

      final List<EbliWidgetSerializeXml> listeWidgetLinked = new ArrayList<EbliWidgetSerializeXml>();
      final Map<EbliWidgetSerializeXml, Integer> listePositionWidgetLinked = new HashMap<EbliWidgetSerializeXml, Integer>();

      // -- r�cup�ration via xml des eblinode/widgets --//
      boolean endReached = false;
      try {
        for (int i = 0; i < sceneXml.getNbFrames(); i++) {

          // lecture xml de l objet
          final Object objetXml = in.readObject();
          if (objetXml == null || !(objetXml instanceof EbliWidgetSerializeXml)) {
            managerError.addMessageError(
                nomRepertoireLayout + ": l'objet xml N�" + i + " lu n'est pas un descripteur de frame valide");
          } else {
            final EbliWidgetSerializeXml widgetXml = (EbliWidgetSerializeXml) objetXml;

            if (!widgetXml.isLinked()) {
              // generation de la widget dans la scene
              parametres.put("nodeName", getNodeName(listeCouplesNoms, widgetXml.getId()));
              final EbliNode node = widgetXml.generateWidgetInScene(parametres, scenToUpdate);
              if (node != null) {
                // ajout des noeuds a ajouter dans la selection
                if (widgetXml.isSelected()) {
                  listeNodeSelect.add(node);
                }

                // ajout de la creation d'un groupe
                if (!widgetXml.getIdGroup().equals(EbliWidgetGroup.NOGROUP)) {
                  // il y a un group a prendre en compte d'id
                  final String idGroup = widgetXml.getIdGroup();
                  if (listeGroupToPerform.get(idGroup) == null) {
                    // on init
                    listeGroupToPerform.put(idGroup, new HashSet<>());
                  }
                  // on ajoute l'objet node pour la widget
                  listeGroupToPerform.get(idGroup).add(node);
                }
              }
            } else {
              // -- on ajoute la widget liee a la liste des widget a cree au
              // final--//
              listeWidgetLinked.add(widgetXml);
              listePositionWidgetLinked.put(widgetXml, new Integer(i));
            }
          }
        }
      } catch (EOFException e) {
        endReached = true;
      }

      // --On executre les widgets liees --//
      for (final EbliWidgetSerializeXml widgetXml : listeWidgetLinked) {

        // generation de la widget dans la scene
        parametres.put("nodeName", getNodeName(listeCouplesNoms, widgetXml.getId()));
        final EbliNode node = widgetXml.generateWidgetInScene(parametres, scenToUpdate);
        if (node == null || node.getWidget() == null) {
          continue;
        }
        // ajout des noeuds a ajouter dans la selection
        if (widgetXml.isSelected() && node.getWidget() != null) {
          listeNodeSelect.add(node);
        }

        final Integer position = listePositionWidgetLinked.get(widgetXml);
        if (position != null) {
          scenToUpdate.moveWidgetTo(node.getWidget(), position.intValue());
        }

        // ajout de la creation d'un groupe
        if (!widgetXml.getIdGroup().equals(EbliWidgetGroup.NOGROUP)) {
          // il y a un group a prendre en compte d'id
          final String idGroup = widgetXml.getIdGroup();
          if (listeGroupToPerform.get(idGroup) == null) {
            // on init
            listeGroupToPerform.put(idGroup, new HashSet<>());
          }
          // on ajoute l'objet node pour la widget
          listeGroupToPerform.get(idGroup).add(node);
        }
      }

      // -- lecture des groupes --//
      // -- ecriture des groupes --//
      Map<String, EbliWidgetGroupSerializeXml> listeGroupes = Collections.emptyMap();
      if (!endReached) {
        final int nbGroups = in.readInt();
        listeGroupes = new HashMap<>();

        for (int i = 0; i < nbGroups; i++) {
          final EbliWidgetGroupSerializeXml group = (EbliWidgetGroupSerializeXml) in.readObject();
          listeGroupes.put(group.getId(), group);
        }
      }

      scenToUpdate.refresh();
      // -- on s'occupe de grouper l'ensemble des widgets --//
      for (final String idGroup : listeGroupToPerform.keySet()) {
        // --l'idGroup est jetable, on ne le sauvegarde pas --//
        final HashSet<EbliNode> listeToGroup = listeGroupToPerform.get(idGroup);
        EbliNode nodeGroup = null;

        // -- on met a jour le ebliNode avec les infos du groupe sauv� --//
        final EbliWidgetGroupSerializeXml infoGroup = listeGroupes.get(idGroup);
        if (!infoGroup.isFusion()) {
          nodeGroup = new EbliWidgetActionGroup(scenToUpdate).groupWidgets(listeToGroup);
        } else {
          GrBoite initZoom = ((EbliWidgetCreatorVueCalque) listeToGroup.iterator().next().getCreator()).getInitZoom();
          nodeGroup = new TrPostActionFusionCalques(scenToUpdate, mainProject).performGroupFusion(listeToGroup);
          EbliWidgetFusionCalques widget = (EbliWidgetFusionCalques) nodeGroup.getCreator().getWidget();
          widget.initZoom(initZoom);
        }

        if (infoGroup != null) {
          parametres.put("nodeName", getNodeName(listeCouplesNoms, infoGroup.getId()));
          infoGroup.updateGroup(nodeGroup, parametres);
        }
      }

      // -- on ajoute les noeuds selectionnes a ebliScene --//
      if (listeNodeSelect != null) {
        if (listeNodeSelect != null) {
          scenToUpdate.setSelectedObjects(listeNodeSelect);
        }
      }
      // mise a jour de la scene
      scenToUpdate.refresh();
    } catch (final Exception e) {
      FuLog.error(e);
      messageError(false);
    }
  }

  /**
   * Methode qui se charge de modifier les paths relatifs dans les cas ou l'on sauvegarde un projet POST sous linux et que l'on charge ce projet sous
   * windows. Il faut changer dynamiquement les / par des \ au chargement du projet
   *
   * @param path
   */
  public static String updatePathForOs(String path) {
    final String os = System.getProperty("os.name");
    if (os.startsWith("Windows")) {
      path = path.replace('/', File.separatorChar);
    } else {
      path = path.replace('\\', File.separatorChar);
    }
    return path;
  }

  public void writeCommentaire(final OutputStream out) {
  }
}
