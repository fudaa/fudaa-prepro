/*
 * @creation 19 d�c. 2005
 * 
 * @modification $Date: 2007-04-26 14:40:16 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.export;

import com.memoire.fu.FuLog;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.meshviewer.export.MvExportFilterBuilderTime;
import org.fudaa.fudaa.meshviewer.export.MvExportOperationBuilderSelection;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostFilterHauteur;
import org.fudaa.fudaa.tr.post.TrPostFilterHauteurElement;

/**
 * @author Fred Deniger
 * @version $Id: TrExportFilterItemHauteurTime.java,v 1.6 2007-04-26 14:40:16 deniger Exp $
 */
public class TrExportFilterItemHauteurTime extends TrExportFilterItemHauteurAbstract implements ListSelectionListener {

  ListSelectionModel time_;

  /**
   * @param _time
   */
  public TrExportFilterItemHauteurTime(final ListSelectionModel _time) {
    super();
    time_ = _time;
    time_.addListSelectionListener(this);
  }

  @Override
  public MvExportOperationBuilderSelection getBuilder(final EfGridData _src) {
    if (save_ != null && ((MvExportFilterBuilderTime) save_).isStrict() != isStrict()) {
      save_ = null;
    }
    if (save_ == null) {
      final int ts = CtuluLibArray.getSelectedIdxNb(time_);
      TrPostFilterHauteur filter = new TrPostFilterHauteur(((Double) tfHauteur_.getValue()).doubleValue());
      if (_src.isElementVar(H2dVariableType.HAUTEUR_EAU)) {
        if (FuLog.isTrace()) {
          FuLog.trace("FTR: build filter for elements");
        }
        filter = new TrPostFilterHauteurElement(((Double) tfHauteur_.getValue()).doubleValue());
      }
      save_ = new MvExportFilterBuilderTime(_src, filter, ts, false, isStrict());
    }
    return save_;
  }

  @Override
  public String getTitle() {
    return TrResource.getS("Exporter uniquement les �l�ments mouill�s");
  }

  @Override
  public boolean isEnable() {
    return !time_.isSelectionEmpty();
  }

  @Override
  public boolean isStateModifiable() {
    return true;
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    save_ = null;
    if (tfHauteur_ != null) tfHauteur_.setEnabled(isEnable());
    setChanged();
    notifyObservers(this);
  }

}