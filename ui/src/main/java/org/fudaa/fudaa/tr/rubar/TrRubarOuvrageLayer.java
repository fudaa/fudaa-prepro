/**
 * @creation 3 janv. 2005
 * @modification $Date: 2007-06-05 09:01:15 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.fu.FuLog;
import gnu.trove.TIntHashSet;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.dodico.ef.EfGridVolumeInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageRef;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.calque.find.CalqueFindActionDefault;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.geometrie.*;
import org.fudaa.ebli.palette.BPalettePlageAbstract;
import org.fudaa.ebli.palette.BPalettePlageDiscret;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.ebli.palette.BPlageDiscret;
import org.fudaa.ebli.trace.*;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.data.TrIconeInput;
import org.fudaa.fudaa.tr.data.TrIconeOutput;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LinearRing;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarOuvrageLayer.java,v 1.31 2007-06-05 09:01:15 deniger Exp $
 */
public class TrRubarOuvrageLayer extends MvElementLayer implements H2dRubarOuvrageListener {
  private final GrPolygone p_ = new GrPolygone();
  private boolean showMeshes_ = true;
  TraceBox boxNb_;
  GrBoite domaine_;
  TraceIcon inputIc_;
  TraceIcon outputIc_;
  Set ouvToDisplay_;
  int[] paintTempo_;
  List paletteId_;
  boolean showEdgeDesc_;
  boolean showEdges_ = true;
  boolean showElementDesc_;
  boolean showMeshesCenter_ = true;
  boolean showWorkNumber_;
  TraceLigne tlPoly_;
  TraceLigne tlPolyInterne_;

  public TrRubarOuvrageLayer(final TrRubarOuvrageLayerModel _m) {
    super(_m);
    _m.initListener(this);
    isPaletteCouleurUsed_ = !_m.isEmpty();
    paletteCouleur_ = null;
    initPal();
    construitLegende();
  }

  public void selectWorksWithDownstreamMesh(boolean hasDownstreamMesh) {
    if (!getM().isModeElt()) {
      clearSelection();
      final int[] idx = getM().getWorksIdxWithDownstreamMesh(hasDownstreamMesh);
      if (idx != null) {
        setSelection(idx);
      }
    }
  }

  public void selectWorksWithRubarRef(int ref) {
    if (!getM().isModeElt()) {
      clearSelection();
      final int[] idx = getM().getWorksIdxWithRubarRef(ref);
      if (idx != null) {
        setSelection(idx);
      }
    }
  }

  @Override
  public TraceLigneModel getLineModel(int _idx) {
    buildTraceLigne();
    if (_idx == 0) {
      return this.tlPoly_.getModel();
    }
    return this.tlPolyInterne_.getModel();
  }

  @Override
  public TraceIconModel getIconModel(int _idx) {
    builInOut();
    if (_idx == 0) {
      return this.inputIc_.getModel();
    }
    return this.outputIc_.getModel();
  }

  @Override
  public boolean setIconModel(int _idx, TraceIconModel _model) {
    builInOut();
    if (_idx == 0) {
      boolean updateData = this.inputIc_.getModel().updateData(_model);
      if (updateData) {
        repaint();
      }
      return updateData;
    }
    boolean updateData = this.outputIc_.getModel().updateData(_model);
    if (updateData) {
      repaint();
    }
    return updateData;
  }

  @Override
  public boolean setLineModel(int _idx, TraceLigneModel _model) {
    buildTraceLigne();
    if (_idx == 0) {
      boolean updateData = this.tlPoly_.getModel().updateData(_model);
      if (updateData) {
        repaint();
      }
      return updateData;
    }
    boolean updateData = this.tlPolyInterne_.getModel().updateData(_model);
    if (updateData) {
      repaint();
    }
    return updateData;
  }

  @Override
  public int getNbSet() {
    return 2;
  }

  private GrBoite buildDomaine() {
    final GrBoite res = new GrBoite();
    final GrBoite tmp = new GrBoite(new GrPoint(), new GrPoint());
    for (int i = getM().getNbOuvrage() - 1; i >= 0; i--) {
      updateBoite(res, tmp, p_, i);
    }
    return res;
  }

  private void drawBox(final Graphics2D _g, final Color _c, final GrPoint _pt, final String _s) {
    buildBox();
    boxNb_.setColorBoite(_c);
    boxNb_.paintBox(_g, (int) _pt.x_, (int) _pt.y_, _s);
  }

  private void drawBoxAval(final Graphics2D _g, final Color _c, final GrPoint _pt) {
    drawBox(_g, _c, _pt, H2dResource.getS("Aval"));
  }

  private void drawInput(final Graphics2D _g, final Color _c, final GrPoint _pt) {
    builInOut();
    inputIc_.setCouleur(_c);
    inputIc_.paintIconCentre(this, _g, (int) _pt.x_, (int) _pt.y_);
  }

  private void drawOutput(final Graphics2D _g, final Color _c, final GrPoint _pt) {
    builInOut();
    outputIc_.setCouleur(_c);
    outputIc_.paintIconCentre(this, _g, (int) _pt.x_, (int) _pt.y_);
  }

  private void drawPt(final Graphics2D _g2d, final boolean _point, final Polygon _p, final double _x1, final double _y1) {
    if (_point) {

      _g2d.drawLine((int) _x1 - 1, (int) _y1, (int) _x1 + 1, (int) _y1);
      _g2d.drawLine((int) _x1, (int) _y1 - 1, (int) _x1, (int) _y1 + 1);
    } else {
      _p.addPoint((int) _x1, (int) _y1);
    }
  }

  private Color getAlphaColor(final H2dRubarOuvrage _o) {
    Color c = getCouleurFor(_o);
    if (isAttenue()) {
      c = EbliLib.getAlphaColor(attenueCouleur(c), alpha_);
    } else if (EbliLib.isAlphaChanged(alpha_)) {
      c = EbliLib.getAlphaColor(c, alpha_);
    }
    return c;
  }

  private void initPaletteList() {
    if (paletteId_ != null) {
      return;
    }
    paletteId_ = new ArrayList(8);
    TrRubarOuvragePaletteIdentifier id = new TrRubarOuvragePaletteIdentifier(H2dRubarOuvrageRef.COMPUTE_WITH_SAINT_VENANT, true, true);
    paletteId_.add(id.getPlageDiscret());
    id = new TrRubarOuvragePaletteIdentifier(H2dRubarOuvrageRef.COMPUTE_WITH_SAINT_VENANT, false, true);
    paletteId_.add(id.getPlageDiscret());
    id = new TrRubarOuvragePaletteIdentifier(H2dRubarOuvrageRef.COMPUTE_WITH_SAINT_VENANT, true, false);
    paletteId_.add(id.getPlageDiscret());
    id = new TrRubarOuvragePaletteIdentifier(H2dRubarOuvrageRef.COMPUTE_WITH_SAINT_VENANT, false, false);
    paletteId_.add(id.getPlageDiscret());
    id = new TrRubarOuvragePaletteIdentifier(H2dRubarOuvrageRef.NO_COMPUTE_WITH_SAINT_VENANT, true, true);
    paletteId_.add(id.getPlageDiscret());
    id = new TrRubarOuvragePaletteIdentifier(H2dRubarOuvrageRef.NO_COMPUTE_WITH_SAINT_VENANT, true, false);
    paletteId_.add(id.getPlageDiscret());
    id = new TrRubarOuvragePaletteIdentifier(H2dRubarOuvrageRef.NO_COMPUTE_WITH_SAINT_VENANT, false, false);
    paletteId_.add(id.getPlageDiscret());
    id = new TrRubarOuvragePaletteIdentifier(H2dRubarOuvrageRef.NO_COMPUTE_WITH_SAINT_VENANT, false, true);
    paletteId_.add(id.getPlageDiscret());
    TrRubarOuvragePaletteIdentifier.clearPreferences();
    // on sauvegarde uniquement les couleurs r�ellement utilis�es
    for (int i = 0; i < paletteId_.size(); i++) {
      final BPlageDiscret bpd = (BPlageDiscret) paletteId_.get(i);
      if (bpd.getCouleur() == null) {
        bpd.setCouleur(BPalettePlageAbstract.getCouleur(Color.BLUE, Color.RED, ((double) i) / ((double) paletteId_.size())));
      } else {
        ((TrRubarOuvragePaletteIdentifier) bpd.getIdentifiant()).saveColorInPref(bpd.getCouleur());
      }
    }
  }

  private void paintDonneesDesc(final Graphics2D _g, final GrMorphisme _versEcran, final BitSet _drawElt1, final BitSet _drawElt2) {
    final GrMorphisme versEcran = _versEcran;
    _g.setFont(getFont());
    buildTraceLigne();
    final GrPoint pt = new GrPoint();
    final GrSegment s = new GrSegment(new GrPoint(), new GrPoint());
    for (int iOuv = getM().getNbOuvrage() - 1; iOuv >= 0; iOuv--) {
      final H2dRubarOuvrage o = getM().getOuvrage(iOuv);
      if (!_drawElt1.get(iOuv) && !_drawElt2.get(iOuv)) {
        continue;
      }
      final Color c = getAlphaColor(o);
      if (c == null) {
        continue;
      }

      if (_drawElt2.get(iOuv)) {
        getM().getElt(p_, o.getElt2());
        p_.autoApplique(versEcran);
        p_.centre(pt);
        if (showElementDesc_) {
          drawBoxAval(_g, c, pt);
        } else if (showMeshesCenter_) {
          drawOutput(_g, c, pt);
        }
        getM().getArete(o.getArete2(), s);
        s.autoApplique(versEcran);
        s.milieu(pt);
        if (showEdgeDesc_) {
          drawBoxAval(_g, c, pt);
        } else if (showEdges_) {
          drawOutput(_g, c, pt);
        }
      }
      // on dessine l'element 1 en dernier pour que le num�ro soit bien visible.
      getM().getElt(p_, o.getElt1());
      if (_drawElt1.get(iOuv)) {
        getM().getArete(o.getArete1(), s);
        s.autoApplique(versEcran);
        s.milieu(pt);
        if (showEdgeDesc_) {
          drawBox(_g, c, pt, H2dResource.getS("Amont"));
        } else if (showEdges_) {
          drawInput(_g, c, pt);
        }
        final double xEdge = pt.x_;
        final double yEdge = pt.y_;
        p_.autoApplique(versEcran);
        p_.centre(pt);
        if (showMeshesCenter_) {
          drawInput(_g, c, pt);
        }
        if (showWorkNumber_ || showElementDesc_) {
          // demande A.Paquier juin 2007: dessiner le num�ro de l'elt entre le centre de l'�l�ment et lerer
          pt.x_ = (pt.x_ + xEdge) / 2;
          pt.y_ = (pt.y_ + yEdge) / 2;
          paintWorkNumber(_g, c, pt, iOuv);
        }
      }
    }
  }

  private void paintError(final Graphics2D _g, final GrPoint _pt, final TraceIcon _err) {
    _g.setColor(Color.RED);
    _g.fillPolygon(p_.polygon());
    if (_err != null) {
      _err.paintIconCentre(this, _g, _pt.x_, _pt.y_);
    }
  }

  private void paintWorkNumber(final Graphics2D _g, final Color _c, final GrPoint _pt, final int _iOuv) {
    buildBox();
    boxNb_.setColorBoite(_c);
    String str = null;
    if (showWorkNumber_) {
      str = CtuluLibString.getString(_iOuv + 1);
      if (showElementDesc_) {
        str += ": " + H2dResource.getS("Amont");
      }
    } else {
      str = H2dResource.getS("Amont");
    }
    if (str != null) {
      boxNb_.paintBox(_g, (int) _pt.x_, (int) _pt.y_, str);
    }
  }

  private void updateBoite(final GrBoite _res, final GrBoite _tmp, final GrPolygone _p, final int _i) {
    final H2dRubarOuvrage ouv = getM().getOuvrage(_i);
    getM().getElt(_p, ouv.getElt1());
    _p.boite(_tmp);
    _res.ajuste(_tmp);
    if (ouv.isElt2Set()) {
      getM().getElt(_p, ouv.getElt2());
      _p.boite(_tmp);
      _res.ajuste(_tmp);
    }
    for (int j = 0; j < ouv.getNbMailleIntern(); j++) {
      getM().getElt(_p, ouv.getMailleIntern(j));
      _p.boite(_tmp);
      _res.ajuste(_tmp);
    }
  }

  Color getCouleurFor(final H2dRubarOuvrageI _o) {
    for (int i = paletteCouleur_.getNbPlages() - 1; i >= 0; i--) {
      final BPlageDiscret plage = (BPlageDiscret) paletteCouleur_.getPlageInterface(i);
      if (((TrRubarOuvragePaletteIdentifier) plage.getIdentifiant()).isEquivalent(_o)) {
        return plage.getCouleur();
      }
    }
    return null;
  }

  TrRubarOuvrageLayerModel getM() {
    return (TrRubarOuvrageLayerModel) super.modele_;
  }

  final boolean isShowEdgeDesc() {
    return showEdgeDesc_;
  }

  final boolean isShowElementDesc() {
    return showElementDesc_;
  }

  final boolean isShowWorkNumber() {
    return showWorkNumber_;
  }

  final boolean isUsed(final BPlageDiscret _o) {
    // H2dRubarOuvrageMng mng = getM().getOuvrageMng();
    for (int i = getM().getNbOuvrage() - 1; i >= 0; i--) {
      if (((TrRubarOuvragePaletteIdentifier) _o.getIdentifiant()).isEquivalent(getM().getOuvrage(i))) {
        return true;
      }
    }
    return false;
  }

  final void setShowEdgeDesc(final boolean _showNumber) {
    if (_showNumber != showEdgeDesc_) {
      showEdgeDesc_ = _showNumber;
      repaint();
    }
  }

  final void setShowElementDesc(final boolean _showNumber) {
    if (_showNumber != showElementDesc_) {
      showElementDesc_ = _showNumber;
      repaint();
    }
  }

  final void setShowWorkNumber(final boolean _showNumber) {
    if (_showNumber != showWorkNumber_) {
      showWorkNumber_ = _showNumber;
      repaint();
    }
  }

  void setTempo(final int[] _idx) {
    paintTempo_ = _idx;
  }

  void unsetTempo() {
    paintTempo_ = null;
  }

  protected void buildBox() {
    if (boxNb_ == null) {
      boxNb_ = new TraceBox();
      boxNb_.setColorFond(Color.WHITE);
      boxNb_.setColorText(Color.BLACK);
      boxNb_.setHMargin(1);
      boxNb_.setVMargin(1);
      boxNb_.setHPosition(SwingConstants.CENTER);
      boxNb_.setVPosition(SwingConstants.CENTER);
    }
  }

  protected void buildTraceLigne() {
    if (tlPoly_ == null) {
      tlPoly_ = new TraceLigne();
      tlPoly_.setEpaisseur(2f);
      tlPolyInterne_ = new TraceLigne();
    }
  }

  protected void builInOut() {
    if (inputIc_ == null) {
      inputIc_ = new TrIconeInput();
      inputIc_.setTaille(6);
      outputIc_ = new TrIconeOutput();
      outputIc_.setTaille(6);
    }
  }

  protected TraceIcon getErreurIcone() {
    return new TraceIcon(TraceIcon.CROIX, 10, Color.RED);
  }

  protected final void initPal() {
    if (paletteCouleur_ == null) {
      paletteCouleur_ = new BPalettePlageDiscret(getM().getOuvrages());
      paletteCouleur_.setTitre(getTitle());
      paletteCouleur_.setSousTitre(CtuluLibString.EMPTY_STRING);
      initPaletteList();
    }
    final List displayList = new ArrayList(paletteId_.size());
    for (int i = paletteId_.size() - 1; i >= 0; i--) {
      if (isUsed((BPlageDiscret) paletteId_.get(i))) {
        displayList.add(paletteId_.get(i));
      }
    }
    final BPlageDiscret[] pbs = new BPlageDiscret[displayList.size()];
    displayList.toArray(pbs);
    Arrays.sort(pbs);
    paletteCouleur_.setPlages(pbs);
  }

  protected final boolean isPaintElt() {
    return showMeshes_;
  }

  protected void paintTempo(final Graphics2D _g, final GrMorphisme _versEcran) {
    if (paintTempo_ != null) {
      if (boxNb_ == null) {
        buildBox();
      }
      boxNb_.setColorBoite(Color.RED);
      // outputIc_.couleur(inputIc_.couleur());
      final GrPoint pt = new GrPoint();
      if (paintTempo_[0] >= 0) {
        getM().polygone(p_, paintTempo_[0], true);
        p_.centre(pt);
        pt.autoApplique(_versEcran);
        boxNb_.paintBox(_g, (int) pt.x_, (int) pt.y_, CtuluLibString.UN);
      }
      if (paintTempo_[2] >= 0) {
        getM().polygone(p_, paintTempo_[2], true);
        p_.centre(pt);
        pt.autoApplique(_versEcran);
        boxNb_.paintBox(_g, (int) pt.x_, (int) pt.y_, CtuluLibString.DEUX);
      }
      final GrSegment s = new GrSegment(new GrPoint(), new GrPoint());
      if (paintTempo_[1] >= 0) {
        getM().getArete(paintTempo_[1], s);
        s.milieu(pt);
        pt.autoApplique(_versEcran);
        boxNb_.paintBox(_g, (int) pt.x_, (int) pt.y_, CtuluLibString.UN);
      }
      if (paintTempo_[3] >= 0) {
        getM().getArete(paintTempo_[3], s);
        s.milieu(pt);
        pt.autoApplique(_versEcran);
        boxNb_.paintBox(_g, (int) pt.x_, (int) pt.y_, CtuluLibString.DEUX);
      }
    }
  }

  protected final void setPaintElt(final boolean _paintElt) {
    if (_paintElt != showMeshes_) {
      showMeshes_ = _paintElt;
      repaint();
    }
  }

  @Override
  protected void setPaletteCouleur(final BPalettePlageAbstract _paletteCouleur) {
    FuLog.warning(new Throwable());
  }

  /**
   * Inverse le mode de travail.
   */
  public void changeModeElt() {
    setModeElt(!getM().isModeElt());
    fireSelectionEvent();
  }

  @Override
  protected BConfigurableInterface getAffichageConf() {
    final BConfigurableInterface[] sect = new BConfigurableInterface[getNbSet() + 2];

    for (int i = 1; i < sect.length; i++) {
      ZCalqueAffichageDonneesTraceConfigure traceConfigure = new ZCalqueAffichageDonneesTraceConfigure(this, i - 1);
      traceConfigure.setShowColor(false);
      traceConfigure.setShowIconType(false);
      sect[i] = traceConfigure;
    }
    sect[0] = new ZCalqueAffichageDonneesConfigure(this);
    sect[sect.length - 1] = new TrRubarOuvrageConfiguration(this);
    return new BConfigurableComposite(sect, EbliLib.getS("Affichage"));
  }

  @Override
  public String getSetTitle(int _idx) {
    if (_idx == 0) {
      return TrLib.getString("El�ments ext�rieurs");
    }
    return TrLib.getString("El�ments int�rieurs");
  }

  @Override
  public GrBoite getDomaine() {
    if (getM().isModeElt()) {
      return getM().getDomaine();
    }
    if (domaine_ == null) {
      domaine_ = buildDomaine();
    }
    return new GrBoite(domaine_);
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return new CalqueFindActionDefault(this);
  }

  @Override
  public BPalettePlageInterface getPaletteCouleur() {
    initPal();
    return paletteCouleur_;
  }

  @Override
  public int[] getSelectedEdgeIdx() {
    return getSelectedEdgeIdx((EfGridVolumeInterface) getM().getGrid(), getSelectedElementIdx());
  }

  @Override
  public int[] getSelectedElementIdx() {
    if (isSelectionEmpty()) {
      return null;
    }
    if (getM().isModeElt()) {
      return super.getSelectedElementIdx();
    }
    final TIntHashSet set = new TIntHashSet();
    final int max = selection_.getMaxIndex();
    for (int i = selection_.getMinIndex(); i <= max; i++) {
      if (selection_.isSelected(i)) {
        final H2dRubarOuvrage ouv = getM().getOuvrage(i);
        set.add(ouv.getElt1());
        if (ouv.isElt2Set()) {
          set.add(ouv.getElt2());
        }
        for (int j = 0; j < ouv.getNbMailleIntern(); j++) {
          set.add(ouv.getMailleIntern(j));
        }
      }
    }
    final int[] res = set.toArray();
    Arrays.sort(res);
    return res;
  }

  @Override
  public int[] getSelectedObjectInTable() {
    return getSelectedIndex();
  }

  @Override
  public int[] getSelectedPtIdx() {
    if (isSelectionEmpty()) {
      return null;
    }
    if (getM().isModeElt()) {
      return super.getSelectedPtIdx();
    }
    final TIntHashSet set = new TIntHashSet();
    final int max = selection_.getMaxIndex();
    for (int i = selection_.getMinIndex(); i <= max; i++) {
      if (selection_.isSelected(i)) {
        final H2dRubarOuvrage ouv = getM().getOuvrage(i);
        getM().getGrid().getElement(ouv.getElt1()).fillList(set);
        if (ouv.isElt2Set()) {
          getM().getGrid().getElement(ouv.getElt2()).fillList(set);
        }
        for (int j = 0; j < ouv.getNbMailleIntern(); j++) {
          getM().getGrid().getElement(ouv.getMailleIntern(j)).fillList(set);
        }
      }
    }
    final int[] res = set.toArray();
    Arrays.sort(res);
    return res;
  }

  @Override
  /**
   * Devrait �tre remplac� par getDomaineOnSelected(). Je ne touche pas a cet ajustement, le calcul de la marge autour du zoom me semble douteux. B.M.
   */
  public GrBoite getDomaineOnSelected() {
    if (isSelectionEmpty()) {
      return null;
    }
    if (getM().isModeElt()) {
      return super.getZoomOnSelected();
    }
    final GrBoite res = new GrBoite();
    final GrBoite tmp = new GrBoite(new GrPoint(), new GrPoint());
    final int max = selection_.getMaxIndex();
    for (int i = selection_.getMinIndex(); i <= max; i++) {
      if (selection_.isSelected(i)) {
        updateBoite(res, tmp, p_, i);
      }
    }
    return res;
  }

  @Override
  public void initFrom(final EbliUIProperties _p) {
    _p.remove("calque.paletteCouleur");
    super.initFrom(_p);
  }

  @Override
  public boolean isConfigurable() {
    return true;
  }

  @Override
  public boolean isDiscrete() {
    return true;
  }

  /**
   * Pour les ouvrages, seule la palette est utilisable.
   */
  public boolean isForegroundColorModifiable() {
    return false;
  }

  public boolean isOnlySelectedShown() {
    return ouvToDisplay_ != null;
  }

  public boolean isOnlySelectedShownEnable() {
    return isOnlySelectedShown() || (!getM().isModeElt() && !isSelectionEmpty());
  }

  @Override
  public boolean isPaletteModifiable() {
    return true;
  }

  @Override
  public boolean isSelectionElementEmpty() {
    return isSelectionEmpty();
  }

  @Override
  public boolean isSelectionPointEmpty() {
    return isSelectionEmpty();
  }

  @Override
  public ZModeleDonnees modeleDonnees() {
    return getM();
  }

  public void ouvrageAddByUser(final H2dRubarOuvrage _o) {
    if (ouvToDisplay_ != null && _o != null) {
      ouvToDisplay_.add(_o);
    }
  }

  @Override
  public void ouvrageAdded(final H2dRubarOuvrageMng _mng) {
    isPaletteCouleurUsed_ = true;
    initPal();
    construitLegende();
    repaint();
    domaine_ = null;
  }

  @Override
  public void ouvrageChanged(final H2dRubarOuvrageMng _mng, final H2dRubarOuvrage _ouv, final boolean _ouvrageElementaireAddedOrRemoved) {
    initPal();
    construitLegende();
    repaint();
  }

  @Override
  public void ouvrageElementaireChanged(final H2dRubarOuvrageMng _mng, final H2dRubarOuvrage _ouv, final H2dRubarOuvrageElementaireInterface _i) {
    if (FuLog.isTrace()) {
      FuLog.trace("ouvrage elementaire change " + _ouv);
    }
  }

  @Override
  public void ouvrageRemoved(final H2dRubarOuvrageMng _mng) {
    isPaletteCouleurUsed_ = !getM().isEmpty();
    initPal();
    construitLegende();
    repaint();
    domaine_ = null;
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel, final GrBoite _clipReel) {
    final int nbOuvrage = getM().getNbOuvrage();
    final Color old = _g.getColor();
    final Font oldFont = _g.getFont();
    if (nbOuvrage >= 0) {
      final GrBoite b = new GrBoite();

      _g.setFont(getFont());
      buildTraceLigne();
      final GrPoint pt = new GrPoint();
      final GrSegment s = new GrSegment(new GrPoint(), new GrPoint());
      // pour dessiner les descriptions: permet de garder les ouvrages dessin�s.
      final BitSet drawElt1 = new BitSet(nbOuvrage);
      final BitSet drawElt2 = new BitSet(nbOuvrage);
      final TraceIcon err = getErreurIcone();
      for (int iOuv = nbOuvrage - 1; iOuv >= 0; iOuv--) {
        final H2dRubarOuvrage o = getM().getOuvrage(iOuv);
        if (ouvToDisplay_ != null && !ouvToDisplay_.contains(o)) {
          continue;
        }
        final Color c = getAlphaColor(o);
        tlPoly_.setCouleur(c);
        tlPolyInterne_.setCouleur(tlPoly_.getCouleur());
        if (showMeshes_) {
          for (int i = o.getNbMailleIntern() - 1; i >= 0; i--) {
            getM().getElt(p_, o.getMailleIntern(i));
            p_.boite(b);
            if (b.intersectXY(_clipReel)) {
              p_.autoApplique(_versEcran);
              if (c == null) {
                _g.setColor(Color.RED);
                _g.fillPolygon(p_.polygon());
                err.paintIconCentre(this, _g, pt.x_, pt.y_);
              } else {
                ZCalquePolygone.paintPoly(p_, 1, tlPolyInterne_, null, _g, this);
              }
            }
          }
        }
        getM().getElt(p_, o.getElt1());
        p_.boite(b);
        if (b.intersectXY(_clipReel)) {
          drawElt1.set(iOuv);
          p_.autoApplique(_versEcran);
          if (c == null) {
            paintError(_g, pt, err);
          } else {
            if (showMeshes_) {
              ZCalquePolygone.paintPoly(p_, 1, tlPoly_, null, _g, this);
            } else if (showEdges_) {
              getM().getArete(o.getArete1(), s);
              s.autoApplique(_versEcran);
              tlPoly_.dessineTrait(_g, s.e_.x_, s.e_.y_, s.o_.x_, s.o_.y_);
            }
          }
        }
        if (o.isElt2Set()) {
          getM().getElt(p_, o.getElt2());
          p_.boite(b);
          if (b.intersectXY(_clipReel)) {
            drawElt2.set(iOuv);
            p_.autoApplique(_versEcran);
            if (c == null) {
              paintError(_g, pt, err);
            } else {
              if (showMeshes_) {
                ZCalquePolygone.paintPoly(p_, 1, tlPoly_, null, _g, this);
              } else if (showEdges_) {
                getM().getArete(o.getArete2(), s);
                s.autoApplique(_versEcran);
                tlPoly_.dessineTrait(_g, s.e_.x_, s.e_.y_, s.o_.x_, s.o_.y_);
              }
            }
          }
        }
      }
      paintDonneesDesc(_g, _versEcran, drawElt1, drawElt2);
    }
    paintTempo(_g, _versEcran);
    _g.setColor(old);
    _g.setFont(oldFont);
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    final double w = getIconWidth();
    final double h = getIconHeight();
    final Graphics2D g2d = (Graphics2D) _g;
    final Color old = _g.getColor();
    _g.setColor(Color.white);
    _g.fillRect(_x + 1, _y + 1, (int) w - 1, (int) h - 1);
    _g.setColor(getForeground());
    _g.drawRect(_x, _y, (int) w, (int) h);
    // Stroke oldStroke = g2d.getStroke();
    final boolean point = getM().isModeElt();
    if (isAttenue()) {
      _g.setColor(attenueCouleur(getForeground()));
    } else {
      _g.setColor(getForeground());
    }
    final boolean empty = (!point) && (modeleDonnees() == null || (modeleDonnees().getNombre() == 0));
    final Polygon poly = new Polygon();
    double x1 = _x + w / 5;
    double y1 = _y + 4 * h / 5;
    drawPt(g2d, point, poly, x1, y1);
    final double largeurBase = 3 * w / 5;
    x1 = x1 + largeurBase / 4;
    drawPt(g2d, point, poly, x1, y1);

    x1 = x1 + largeurBase / 8;
    y1 = _y + h / 2;
    drawPt(g2d, point, poly, x1, y1);
    x1 = x1 + largeurBase / 4;
    drawPt(g2d, point, poly, x1, y1);
    y1 = _y + 4 * h / 5;
    x1 = x1 + largeurBase / 8;
    drawPt(g2d, point, poly, x1, y1);
    x1 = x1 + largeurBase / 4;
    drawPt(g2d, point, poly, x1, y1);
    x1 = x1 - largeurBase / 8;
    y1 = _y + h / 5;
    drawPt(g2d, point, poly, x1, y1);
    x1 = _x + w / 5 + largeurBase / 8;
    drawPt(g2d, point, poly, x1, y1);
    if (!point) {
      if (empty) {
        ((Graphics2D) _g).draw(poly);
      } else {
        ((Graphics2D) _g).fill(poly);
      }
    }

    _g.setColor(old);
  }

  @Override
  public void doPaintSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran, final GrBoite _clipReel) {
    if (getM().isModeElt()) {
      super.doPaintSelection(_g, _trace, _versEcran, _clipReel);
      return;
    }
    if (isSelectionEmpty() || getM().isEmpty()) {
      return;
    }
    final int max = selection_.getMaxIndex();
    Color cs = _trace.getColor();
    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    final TraceLigne tlSelection = _trace.getLigne();
    final TraceIcon ic = _trace.getIcone();
    final Color old = _g.getColor();
    _g.setColor(cs);

    final GrBoite clipReel = _clipReel;
    final GrBoite tmp = new GrBoite();
    final GrMorphisme versEcran = _versEcran;
    for (int i = selection_.getMinIndex(); i <= max; i++) {
      if (selection_.isSelected(i)) {
        final H2dRubarOuvrage o = getM().getOuvrage(i);
        getM().getElt(p_, o.getElt1());
        p_.boite(tmp);
        if (clipReel.intersectXY(tmp)) {
          p_.autoApplique(versEcran);
          ZCalquePolygone.paintPoly(p_, 1, tlSelection, ic, _g, this);
        }
        if (o.isElt2Set()) {
          getM().getElt(p_, o.getElt2());
          p_.boite(tmp);
          if (clipReel.intersectXY(tmp)) {
            p_.autoApplique(versEcran);

            ZCalquePolygone.paintPoly(p_, 1, tlSelection, ic, _g, this);
          }
        }
        for (int iInt = o.getNbMailleIntern() - 1; iInt >= 0; iInt--) {
          getM().getElt(p_, o.getMailleIntern(iInt));
          p_.boite(tmp);
          if (clipReel.intersectXY(tmp)) {
            p_.autoApplique(versEcran);
            ZCalquePolygone.paintPoly(p_, 1, tlSelection, ic, _g, this);
          }
        }
      }
    }
    _g.setColor(old);
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    final EbliUIProperties res = super.saveUIProperties();
    res.remove("calque.paletteCouleur");
    return res;
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    if (getM().isModeElt()) {
      return super.selection(_pt, _tolerance);
    }
    if (getM().isEmpty()) {
      return null;
    }
    final double distanceReel = GrMorphisme.convertDistanceXY(getVersReel(), _tolerance);
    final GrBoite domaineReel = getDomaine();
    // le point n'appartient au domaine et est distant > tolerance
    if (!domaineReel.contientXY(_pt) && domaineReel.distanceXY(_pt) > distanceReel) {
      return null;
    }
    final GrBoite clipReel = getClipReel(getGraphics());
    for (int i = getM().getNbOuvrage() - 1; i >= 0; i--) {
      final H2dRubarOuvrage o = getM().getOuvrage(i);
      // on teste l'element 1
      getM().getElt(p_, o.getElt1());
      p_.boite(domaineReel);
      if (domaineReel.intersectXY(clipReel) && GrPolygone.estSelectionne(p_, distanceReel, _pt)) {
        final CtuluListSelection r = new CtuluListSelection();
        r.setSelectionInterval(i, i);
        return r;
      }
      // on teste l'element 2
      if (o.isElt2Set()) {
        getM().getElt(p_, o.getElt2());
        p_.boite(domaineReel);
        if (domaineReel.intersectXY(clipReel) && GrPolygone.estSelectionne(p_, distanceReel, _pt)) {
          final CtuluListSelection r = new CtuluListSelection();
          r.setSelectionInterval(i, i);
          return r;
        }
      }
      // on teste les elements internes
      for (int iInt = o.getNbMailleIntern() - 1; iInt >= 0; iInt--) {
        getM().getElt(p_, o.getMailleIntern(iInt));
        p_.boite(domaineReel);
        if (domaineReel.intersectXY(clipReel) && GrPolygone.estSelectionne(p_, distanceReel, _pt)) {
          final CtuluListSelection r = new CtuluListSelection();
          r.setSelectionInterval(i, i);
          return r;
        }
      }
    }
    return null;
  }

  @Override
  public CtuluListSelection selection(final LinearRing _poly, final int _mode) {
    if (getM().isModeElt()) {
      return super.selection(_poly, _mode);
    }
    if (getM().isEmpty()) {
      return null;
    }
    final GrBoite tmp = new GrBoite();
    final Envelope polyEnv = _poly.getEnvelopeInternal();
    final GrBoite domaineBoite = getDomaine();
    final Envelope domaine = new Envelope(domaineBoite.e_.x_, domaineBoite.o_.x_, domaineBoite.e_.y_, domaineBoite.o_.y_);
    // si l'envelop du polygone n'intersect pas le domaine, il n'y a pas de selection
    if (!polyEnv.intersects(domaine)) {
      return null;
    }
    final Coordinate c = new Coordinate();
    CtuluListSelection r = null;
    final IndexedPointInAreaLocator tester = new IndexedPointInAreaLocator(_poly);
    for (int i = getM().getNbOuvrage() - 1; i >= 0; i--) {
      final H2dRubarOuvrage o = getM().getOuvrage(i);
      // on teste l'element 1
      getM().getElt(p_, o.getElt1());
      p_.boite(tmp);
      if ((polyEnv.contains(tmp.e_.x_, tmp.e_.y_)) && (polyEnv.contains(tmp.o_.x_, tmp.o_.y_))
          && VecteurGrPoint.estSelectionneEnv(c, polyEnv, _poly, tester, p_.sommets_, _mode)) {
        // on teste l'element 2
        boolean goOne = true;
        if (o.isElt2Set()) {
          getM().getElt(p_, o.getElt2());
          p_.boite(tmp);
          if ((polyEnv.contains(tmp.e_.x_, tmp.e_.y_)) && (polyEnv.contains(tmp.o_.x_, tmp.o_.y_))) {
            goOne = VecteurGrPoint.estSelectionneEnv(c, polyEnv, _poly, tester, p_.sommets_, _mode);
          }
        }
        if (goOne) {
          // on teste les elements internes
          boolean all = true;
          for (int iInt = o.getNbMailleIntern() - 1; iInt >= 0 && all; iInt--) {
            getM().getElt(p_, o.getMailleIntern(iInt));
            p_.boite(tmp);
            if ((polyEnv.contains(tmp.e_.x_, tmp.e_.y_)) && (polyEnv.contains(tmp.o_.x_, tmp.o_.y_))
                && !VecteurGrPoint.estSelectionneEnv(c, polyEnv, _poly, tester, p_.sommets_, _mode)) {
              all = false;
            }
          }
          if (all) {
            if (r == null) {
              r = new CtuluListSelection();
            }
            r.add(i);
          }
        }
      }
    }
    return r;
  }

  public void setDisplayOnlySelected(final boolean _b) {
    if (_b) {
      if (!isSelectionEmpty() && !getM().isModeElt()) {
        ouvToDisplay_ = new HashSet();
        final int max = selection_.getMaxIndex();
        for (int i = selection_.getMinIndex(); i <= max; i++) {
          if (selection_.isSelected(i)) {
            ouvToDisplay_.add(getM().getOuvrage(i));
          }
        }
      }
    } else {
      ouvToDisplay_ = null;
    }
    repaint();
  }

  /**
   * Pour les ouvrages, seule la palette est utilisable.
   */
  @Override
  public void setForeground(final Color _v) {
  }

  /**
   * @param _b le nouveau mode d'affichage
   */
  public void setModeElt(final boolean _b) {
    if (_b != getM().isModeElt()) {
      clearSelection();
      getM().setModeElt(_b);
      // pour le panel d'info
      fireSelectionEvent();
    }
  }

  @Override
  public void setPaletteCouleurPlages(final BPalettePlageInterface _newPlage) {
    paletteCouleur_.initFrom(_newPlage);
    if (_newPlage != null) {
      for (int i = paletteCouleur_.getNbPlages() - 1; i >= 0; i--) {
        final BPlageDiscret newPlage = (BPlageDiscret) paletteCouleur_.getPlageInterface(i);
        final TrRubarOuvragePaletteIdentifier identifiant = (TrRubarOuvragePaletteIdentifier) newPlage.getIdentifiant();
        identifiant.saveColorInPref(newPlage.getCouleur());
        upatePaletteIdColor(identifiant, newPlage);
      }
    }
    repaint();
  }

  private void upatePaletteIdColor(TrRubarOuvragePaletteIdentifier identifiant, BPlageDiscret newPlage) {
    for (int i = paletteId_.size() - 1; i >= 0; i--) {
      final BPlageDiscret bPlageDiscret = (BPlageDiscret) paletteId_.get(i);
      if (identifiant.equals(bPlageDiscret.getIdentifiant())) {
        bPlageDiscret.initFrom(newPlage);
        break;
      }
    }
  }
}
