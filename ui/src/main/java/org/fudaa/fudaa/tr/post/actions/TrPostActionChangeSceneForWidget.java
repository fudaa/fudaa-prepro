package org.fudaa.fudaa.tr.post.actions;

import java.util.List;
import java.util.Set;
import org.apache.commons.collections.Predicate;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.PredicateFactory;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionDelete;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionDuplicate;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionFilteredAbstract;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostLayoutFille;
import org.fudaa.fudaa.tr.post.TrPostProjet;

/**
 * Creation de l action cahngement de sscene pruo le widget qui g�re le multi-layout. Chooser qui demande la fille
 * layout puis autre chooser qui demande l'onglet. Si la fille est unique pas de chooser idem pour l onglet de la fille
 * selectionnee.
 * 
 * @author Adrien Hadoux
 */
public class TrPostActionChangeSceneForWidget extends EbliWidgetActionFilteredAbstract {

  TrPostProjet projet_;

  public TrPostActionChangeSceneForWidget(final EbliScene _scene, final TrPostProjet _projet) {
    super(_scene, EbliResource.EBLI.getString("Changer de layout les objets s�lectionn�s"), CtuluResource.CTULU
        .getIcon("crystal_couper"), "DEPLACER");
    projet_ = _projet;
  }

  /**
   * construc teur reserv� au mode u_nitaire activ� depuis le popumenu
   * 
   * @param target
   * @param _projet
   */
  public TrPostActionChangeSceneForWidget(EbliNode target, final TrPostProjet _projet) {
    super(target, EbliResource.EBLI.getString("Changer de layout les objets s�lectionn�s"), CtuluResource.CTULU
        .getIcon("crystal_couper"), "DEPLACER");
    projet_ = _projet;
  }

  @Override
  protected Predicate getAcceptPredicate() {
    return PredicateFactory.geDuplicatablePredicate();
  }

  @Override
  protected CtuluCommand act(Set<EbliNode> filteredNode) {

    // -- selection de la nouvelle scene ou positionner les widget --//
    final EbliScene sceneDestination = selectionneSceneNode();
    if (sceneDestination == null) return null;
    CtuluCommandComposite cmp = new CtuluCommandComposite();
    cmp.addCmd(EbliWidgetActionDuplicate.duplicate(filteredNode, sceneDestination));
    cmp.addCmd(EbliWidgetActionDelete.deleteNodes(filteredNode));
    return cmp;
  }

  private EbliScene selectionneSceneNode() {

    TrPostLayoutFille fenetre = null;
    final TrPostCommonImplementation impl = projet_.getImpl();
    final List<TrPostLayoutFille> ls = impl.getAllLayoutFille();
    if (ls.size() == 1) {
      fenetre = ls.get(0);
    } else {

      // recuperation de toutes les noms des frames post du projet --//
      final String[] listeFillesLayout = TrPostActionDuplicate.formatteFilles(ls);
      final int indiceFilleelect = CtuluLibSwing.chooseValue(listeFillesLayout, projet_.getImpl().getFrame(), TrLib
          .getString("Dupliquer l'objet"), TrLib.getString("S�lectionnez la fen�tre dans laquelle dupliquer l'objet"),
          true);
      if (indiceFilleelect < 0) return null;
      fenetre = ls.get(indiceFilleelect);
    }
    return fenetre.controller_.getSceneCourante();
  }

  // public String[] formatteLayout(TrPostLayoutPanelController _controller) {
  // String[] noms = new String[_controller.listeScenes_.size()];
  //
  // for (int cpt = 0; cpt < _controller.listeScenes_.size(); cpt++) {
  // noms[cpt] = "Layout " + (cpt + 1);
  // }
  // return noms;
  // }

}
