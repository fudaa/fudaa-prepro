/**
 *
 */
package org.fudaa.fudaa.tr.post;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TDoubleHashSet;
import gnu.trove.TDoubleIntHashMap;
import gnu.trove.TIntIntHashMap;
import org.apache.commons.lang.StringUtils;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.fudaa.tr.common.TrResource;

import java.util.Arrays;
import java.util.List;

/**
 * @author CANEL Christophe (Genesis)
 */
public class RubarTimeIndexResolver {
  private double[] timeStep;
  private double[] tpcTimeStep;
  private double[] tpsTimeStep;
  private double[] zfnTimeStep;
  private double[] sedTimeStep;
  private double[] dzfTimeStep;
  private TIntIntHashMap tpcIndexes = new TIntIntHashMap();
  private TIntIntHashMap tpsIndexes = new TIntIntHashMap();
  private TIntIntHashMap zfnIndexes = new TIntIntHashMap();
  private TIntIntHashMap sedIndexes = new TIntIntHashMap();
  private TIntIntHashMap dzfIndexes = new TIntIntHashMap();

  double[] getFirstNonNull(List<double[]> timeSteps) {
    for (double[] ds : timeSteps) {
      if (ds != null) {
        return ds;
      }
    }
    return null;
  }

  public boolean computeTimeSteps() {
    List<double[]> timeSteps = Arrays.asList(tpcTimeStep, tpsTimeStep,
        zfnTimeStep,
        sedTimeStep,
        dzfTimeStep);
    double[] firstNonNull = getFirstNonNull(timeSteps);
    if (firstNonNull == null) {
      return false;
    }

    if (this.timeStep == null) {
      this.timeStep = firstNonNull;
    }

    this.tpcIndexes.clear();
    this.tpsIndexes.clear();
    this.zfnIndexes.clear();
    this.sedIndexes.clear();
    this.dzfIndexes.clear();

    TDoubleHashSet timeStepSet = new TDoubleHashSet(this.timeStep);
    TDoubleIntHashMap tpcTimeStepMap = new TDoubleIntHashMap(this.tpcTimeStep == null ? 0 : this.tpcTimeStep.length);
    TDoubleIntHashMap tpsTimeStepMap = new TDoubleIntHashMap(this.tpsTimeStep == null ? 0 : this.tpsTimeStep.length);
    TDoubleIntHashMap zfnTimeStepMap = new TDoubleIntHashMap(this.zfnTimeStep == null ? 0 : this.zfnTimeStep.length);
    TDoubleIntHashMap sedTimeStepMap = new TDoubleIntHashMap(this.sedTimeStep == null ? 0 : this.sedTimeStep.length);
    TDoubleIntHashMap dzfIndexesMap = new TDoubleIntHashMap(this.dzfTimeStep == null ? 0 : this.dzfTimeStep.length);

    this.loadTimeStepMap(tpcTimeStepMap, this.tpcTimeStep);
    this.loadTimeStepMap(tpsTimeStepMap, this.tpsTimeStep);
    this.loadTimeStepMap(zfnTimeStepMap, this.zfnTimeStep);
    this.loadTimeStepMap(sedTimeStepMap, this.sedTimeStep);
    this.loadTimeStepMap(dzfIndexesMap, this.dzfTimeStep);

    for (double d : this.timeStep) {
      if (((this.tpcTimeStep != null) && (!tpcTimeStepMap.contains(d)))
          || ((this.tpsTimeStep != null) && (!tpsTimeStepMap.contains(d)))
          || ((this.zfnTimeStep != null) && (!zfnTimeStepMap.contains(d)))
          || ((this.sedTimeStep != null) && (!sedTimeStepMap.contains(d)))
          || ((this.dzfTimeStep != null) && (!dzfIndexesMap.contains(d)))) {
        timeStepSet.remove(d);
      }
    }

    this.timeStep = new double[timeStepSet.size()];
    final TDoubleArrayList timeStepList = new TDoubleArrayList(timeStepSet.toArray());

    timeStepList.sort();

    for (int i = 0; i < timeStepList.size(); i++) {
      final double d = timeStepList.get(i);

      this.timeStep[i] = d;

      if (this.tpcTimeStep != null) {
        this.tpcIndexes.put(i, tpcTimeStepMap.get(d));
      }

      if (this.tpsTimeStep != null) {
        this.tpsIndexes.put(i, tpsTimeStepMap.get(d));
      }

      if (this.zfnTimeStep != null) {
        this.zfnIndexes.put(i, zfnTimeStepMap.get(d));
      }
      if (this.sedTimeStep != null) {
        this.sedIndexes.put(i, sedTimeStepMap.get(d));
      }
      if (this.dzfTimeStep != null) {
        this.dzfIndexes.put(i, sedTimeStepMap.get(d));
      }
    }

    return true;
  }

  /**
   * @return the timeStep
   */
  public double[] getTimeStep() {
    return timeStep;
  }

  public int getTPCTimeStep(int timeStep) {
    if (!this.tpcIndexes.contains(timeStep)) {
      return timeStep;
    }

    return this.tpcIndexes.get(timeStep);
  }

  public int getTPSTimeStep(int timeStep) {
    if (!this.tpsIndexes.contains(timeStep)) {
      return timeStep;
    }

    return this.tpsIndexes.get(timeStep);
  }

  public int getZFNTimeStep(int timeStep) {
    if (!this.zfnIndexes.contains(timeStep)) {
      return timeStep;
    }

    return this.zfnIndexes.get(timeStep);
  }

  public int getSEDTimeStep(int timeStep) {
    if (!this.sedIndexes.contains(timeStep)) {
      return timeStep;
    }
    return this.sedIndexes.get(timeStep);
  }

  public int getDZFTimeStep(int timeStep) {
    if (!this.dzfIndexes.contains(timeStep)) {
      return timeStep;
    }
    return this.dzfIndexes.get(timeStep);
  }

  public String getTimeStepModificationExplanation() {
    if (isOriginalTimeStep()) {
      return StringUtils.EMPTY;
    }
    StringBuilder res = new StringBuilder();
    res.append("<html><body>");
    res.append("<p>");
    res.append(TrResource.getS("Les pas de temps ont �t� modifi�s pour �tre communs � tous les fichiers r�sulats. Le plus petit d�nominateur commun est conserv�."));
    res.append("</p><ul>");
    appendSizeInfo(res, "TPC", this.tpcTimeStep);
    appendSizeInfo(res, "TPS", this.tpsTimeStep);
    appendSizeInfo(res, "ZFN", this.zfnTimeStep);
    appendSizeInfo(res, "DZF", this.dzfTimeStep);
    appendSizeInfo(res, "SED", this.sedTimeStep);
    res.append("</ul></body></html>");
    return res.toString();
  }

  private void appendSizeInfo(StringBuilder res, String fileType, double[] array) {
    if (array != null) {
      res.append("<li>").append(TrResource.getS("Nombre de pas de temps pour le fichier {0}:", fileType)).append(" ").append(array.length).append("</li>");
    }
  }

  public boolean isOriginalTimeStep() {
    if ((this.timeStep == null)
        || ((this.tpcTimeStep == null) && (this.tpsTimeStep == null) && (this.sedTimeStep == null) && (this.zfnTimeStep == null))) {
      return false;
    }

    final int nbTimeStep = this.timeStep.length;

    return ((this.tpcTimeStep == null) || (this.tpcTimeStep.length == nbTimeStep))
        && ((this.tpsTimeStep == null) || (this.tpsTimeStep.length == nbTimeStep))
        && ((this.zfnTimeStep == null) || (this.zfnTimeStep.length == nbTimeStep))
        && ((this.dzfTimeStep == null) || (this.dzfTimeStep.length == nbTimeStep))
        && ((this.sedTimeStep == null) || (this.sedTimeStep.length == nbTimeStep));
  }

  private void loadTimeStepMap(TDoubleIntHashMap map, double[] timeStep) {
    if (timeStep == null) {
      return;
    }

    for (int i = 0; i < timeStep.length; i++) {
      map.put(timeStep[i], i);
    }
  }

  public void setSedTimeStep(double[] sedTimeStep) {
    if (!CtuluLibArray.isEmpty(sedTimeStep)) {
      this.sedTimeStep = sedTimeStep;
    }
  }

  /**
   * @param timeStep the timeStep to set
   */
  public void setTimeStep(double[] timeStep) {
    this.timeStep = timeStep;
  }

  /**
   * @param tpcTimeStep the tpcTimeStep to set
   */
  public void setTpcTimeStep(double[] tpcTimeStep) {
    if (!CtuluLibArray.isEmpty(tpcTimeStep)) {
      this.tpcTimeStep = tpcTimeStep;
    }
  }

  /**
   * @param tpsTimeStep the tpsTimeStep to set
   */
  public void setTpsTimeStep(double[] tpsTimeStep) {
    if (!CtuluLibArray.isEmpty(tpsTimeStep)) {
      this.tpsTimeStep = tpsTimeStep;
    }
  }

  /**
   * @param zfnTimeStep the zfnTimeStep to set
   */
  public void setZfnTimeStep(double[] zfnTimeStep) {
    if (!CtuluLibArray.isEmpty(zfnTimeStep)) {
      this.zfnTimeStep = zfnTimeStep;
    }
  }

  public void setDzfTimeStep(double[] dzfTimeStep) {
    if (!CtuluLibArray.isEmpty(dzfTimeStep)) {
      this.dzfTimeStep = dzfTimeStep;
    }
  }
}
