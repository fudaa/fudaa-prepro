/**
 * @creation 28 mai 2004
 * @modification $Date: 2006-09-19 15:07:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.dodico.calcul.CalculExecBatch;
import org.fudaa.dodico.commun.DodicoPreferences;

/**
 * @author Fred Deniger
 * @version $Id: TrExecBatchPreferencesPanel.java,v 1.10 2006-09-19 15:07:30 deniger Exp $
 */
public class TrExecBatchPreferencesPanel extends BuAbstractPreferencesPanel implements DocumentListener, ActionListener {

  CalculExecBatch[] batch_;

  BuLabelMultiLine errorText_;
  JTextField[] execPath_;

  public TrExecBatchPreferencesPanel(final CalculExecBatch[] _batch) {
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    setLayout(new BuBorderLayout());
    BuPanel n = new BuPanel();
    errorText_ = new BuLabelMultiLine();
    errorText_.setForeground(Color.red);
    errorText_.setVisible(false);
    n.add(errorText_);
    add(n, BuBorderLayout.NORTH);
    batch_ = _batch;
    final int nb = batch_.length;
    for (int i = 0; i < nb; i++) {
      if (!batch_[i].isOSSupported()) {
        errorText_.setText(new StringBuffer(40).append(batch_[i]).append(CtuluLibString.ESPACE).append(
            TrResource.getS("non support�")).toString());
        errorText_.setVisible(true);
        return;
      }
    }
    n = new BuPanel();
    n.setLayout(new BuGridLayout(3, 5, 5, true, false, false, false));
    execPath_ = new JTextField[batch_.length];
    final int nb2 = execPath_.length;
    for (int i = 0; i < nb2; i++) {
      n.add(new BuLabel(batch_[i].getExecName()));
      execPath_[i] = new BuTextField();
      execPath_[i].setColumns(30);
      execPath_[i].setText(batch_[i].getExecFile());
      execPath_[i].getDocument().addDocumentListener(this);
      n.add(execPath_[i]);
      final BuButton bt = new BuButton();
      bt.setText("...");
      bt.setActionCommand(CtuluLibString.getString(i));
      bt.addActionListener(this);
      n.add(bt);
    }
    add(n, BuBorderLayout.CENTER);
    controleData();
  }

  private void setUnvalide(final String _txt) {
    final boolean change = !errorText_.isVisible();
    errorText_.setText(_txt);
    errorText_.setVisible(true);
    if (change) {
      revalidate();
    }
  }

  private void setValide() {
    if (errorText_.isVisible()) {
      errorText_.setVisible(false);
      revalidate();
    }
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final String com = _e.getActionCommand();
    if (com == null) { return; }
    try {
      final int i = Integer.parseInt(com);
      final File f = new File(execPath_[i].getText()).getParentFile();
      final CtuluFileChooser fileChooser = new CtuluFileChooser(f == null);
      if (f != null) {
        fileChooser.setCurrentDirectory(f);
      }
      fileChooser.setFileHidingEnabled(true);
      fileChooser.setMultiSelectionEnabled(false);
      fileChooser.setDialogTitle(batch_[i].getExecName());
      final int returnVal = fileChooser.showOpenDialog(this);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        final String text = fileChooser.getSelectedFile().getAbsolutePath();
        execPath_[i].setText(text);
        execPath_[i].setToolTipText(text);
        controleData();
      }
    } catch (final NumberFormatException e) {

    }
  }

  protected final void controleData() {
    boolean invalide = false;
    for (int i = batch_.length - 1; i >= 0; i--) {
      final File f = new File(execPath_[i].getText());
      if (f.exists()) {
        execPath_[i].setForeground(Color.BLACK);
        // batch_[i].setExecFile(execPath_[i].getText());
      } else {
        invalide = true;
        setUnvalide(TrResource.getS("L'ex�cutable {0} est introuvable", batch_[i].getExecName()));
        execPath_[i].setForeground(Color.RED);
      }
    }
    if (!invalide) {
      setValide();
    }
  }

  public boolean isDataValide() {
    controleData();
    return !errorText_.isVisible();
  }

  @Override
  public void applyPreferences() {
    controleData();
    if (errorText_.isVisible()) {
      final BuDialogError e = new BuDialogError(null, null, TrResource.getS("Les donn�es ne sont pas valides")
          + CtuluLibString.LINE_SEP + errorText_.getText());
      e.setModal(true);
      e.activate();
      e.dispose();
      setDirty(true);
    }
    for (int i = batch_.length - 1; i >= 0; i--) {
      batch_[i].setExecFile(execPath_[i].getText());
    }
    DodicoPreferences.DODICO.writeIniFile();
    if (!errorText_.isVisible()) {
      setDirty(false);
    }

  }

  @Override
  public void cancelPreferences() {
    for (int i = batch_.length - 1; i >= 0; i--) {
      execPath_[i].setText(batch_[i].getExecFile());
    }
  }

  @Override
  public void changedUpdate(final DocumentEvent _e) {
    setDirty(true);
  }

  @Override
  public String getTitle() {
    return TrResource.getS("Exec");
  }

  @Override
  public void insertUpdate(final DocumentEvent _e) {
    setDirty(true);
  }

  @Override
  public boolean isPreferencesApplyable() {
    return true;
  }

  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }

  @Override
  public boolean isPreferencesValidable() {
    return true;
  }

  @Override
  public void removeUpdate(final DocumentEvent _e) {
    setDirty(true);
  }

  @Override
  public void validatePreferences() {
    applyPreferences();
  }

}