/**
 * @creation 1999-08-10
 * @modification $Date: 2007-06-05 09:01:14 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JComponent;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.animation.EbliAnimationAdapterInterface;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.controle.BSelecteurListTimeTarget;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.fudaa.meshviewer.model.MvInfoDelegate;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * Un calque de trace de cartes avec un nouvel algorithme de trace.
 * 
 * @version $Id: TrIsoLayer.java,v 1.32 2007-06-05 09:01:14 deniger Exp $
 * @author Bertrand Marchand
 */
public class TrIsoLayer extends TrIsoLayerDefault implements BSelecteurListTimeTarget, EbliAnimationAdapterInterface {

  ListSelectionModel mainTimeSelectionModel_;

  /**
   * Contructeur du calque.
   */
  public TrIsoLayer(final TrPostSource _m, final MvInfoDelegate _d) {
    super(new TrIsoModel(_m, _d));
    setTitle(TrResource.getS("Résultats"));
    timeUpdated();
  }

  public TrIsoModel getIsoModel() {
    return (TrIsoModel) modele_;
  }
  
  
  

  @Override
  protected void updateEltAdapter() {
    eltAdapter_.setCurrentEltData(getIsoModelAbstract().var_, getIsoModel().tIdx_, getIsoModelAbstract().oldData_);
  }

  @Override
  public boolean getTimeRange(final CtuluRange _b) {
    return getIsoModel().getTimeDataRange(_b);
  }

  public static EbliUIProperties saveTime(final EbliUIProperties _prop, final BSelecteurListTimeTarget _timeTarget,
      TrPostSource src) {
    if (_prop == null) { return null; }
    if (_timeTarget == null || _timeTarget.getTimeListSelectionModel().isSelectionEmpty()) { return _prop; }
    if (!_timeTarget.getTimeListSelectionModel().isSelectionEmpty()) {
      final int i = _timeTarget.getTimeListSelectionModel().getMaxSelectionIndex();
      double time = src.getTimeStep(i);
      _prop.put("post.timeSelected", i);
      _prop.put("post.timeValueSelected", time);
    }
    return _prop;
  }

  public static void restoreTime(final EbliUIProperties _prop, final BSelecteurListTimeTarget _timeTarget,
      TrPostSource src) {
    double eps = 1E-1;
    if (_prop == null || _timeTarget == null) { return; }
    final int size = _timeTarget.getTimeListModel().getSize();
    if (size == 0) { return; }
    int idx = _prop.getInteger("post.timeSelected", size - 1);
    Double timeStep = _prop.getDoubleObject("post.timeValueSelected");
    if (idx < 0 || idx >= size) {
      idx = size - 1;
    }
    if (size > 0 && timeStep != null && !CtuluLib.isEquals(src.getTimeStep(idx), timeStep.doubleValue(), eps)) {
      idx = src.getTime().findTimeStep(timeStep.doubleValue(), eps);
      if (idx < 0 || idx >= size) {
        idx = size - 1;
      }
    }
    if (idx >= 0) {
      _timeTarget.getTimeListSelectionModel().setSelectionInterval(idx, idx);
    }
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    return saveTime(super.saveUIProperties(), this, getIsoModel().s_);
  }

  @Override
  public void initFrom(final EbliUIProperties _p) {
    super.initFrom(_p);
    restoreTime(_p, this, getIsoModel().s_);
  }

  @Override
  protected void updateSavedPalBeforeSet(final BPalettePlage _p) {
    super.updateSavedPalBeforeSet(_p);
    _p.setSousTitre(getIsoModel().getTimeSelected());
  }

  @Override
  protected CtuluRange getRangeForPalette() {
    return getIsoModel().getPaletteExtremaForSelectedValueAndTimeStep();
  }

  @Override
  protected TrIsoLayerDefault buildLayer() {
    return new TrIsoLayer(getIsoModel().s_, getIsoModel().getDelegate());
  }

  @Override
  public void timeStepFormatChanged() {
    super.timeStepFormatChanged();
    timeUpdated();
  }

  public ListSelectionModel getMainTimeModel() {
    if (mainTimeSelectionModel_ == null) {
      mainTimeSelectionModel_ = createTimeSelectionModel(this);
      mainTimeSelectionModel_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      final int t = getIsoModel().getTimeStep();
      mainTimeSelectionModel_.setSelectionInterval(t, t);
      mainTimeSelectionModel_.addListSelectionListener(this);

    }
    return mainTimeSelectionModel_;
  }

  public static DefaultListSelectionModel createTimeSelectionModel(final EbliAnimationAdapterInterface _anim) {
    return new DefaultListSelectionModel() {
      @Override
      public void insertIndexInterval(final int _index, final int _length, final boolean _before) {
        super.insertIndexInterval(_index, _length, _before);
        if (_length > 0) {
          final int idx = _anim.getNbTimeStep() - 1;
          if (idx > 0) {
            this.setSelectionInterval(idx, idx);
          }
        }

      }
    };
  }

  @Override
  public int getNbTimeStep() {
    return getIsoModel().getTimeStepNb();
  }

  @Override
  public ListModel getTimeListModel() {
    return getIsoModel().getTimeList();
  }

  @Override
  public ListSelectionModel getTimeListSelectionModel() {
    return getMainTimeModel();
  }

  @Override
  public String getTimeStep(final int _idx) {
    return getIsoModel().getT(_idx);
  }

  @Override
  public double getTimeStepValueSec(final int _idx) {
    return getIsoModel().getTInSec(_idx);
  }

  public JComponent getTimeTargetComponent() {
    return this;
  }

  @Override
  public final boolean isDonneesBoiteTimeAvailable() {
    return true;
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    // super.paintIcon(_c,_g,_x,_y);
    _g.translate(_x, _y);
    final boolean attenue = isAttenue();
    final int w = getIconWidth();
    final int h = getIconHeight();
    Color c;
    c = Color.red;
    if (attenue) {
      c = attenueCouleur(c);
    }
    _g.setColor(c);
    _g.fillRect(1, 1, w - 1, h - 1);
    c = Color.yellow;
    if (attenue) {
      c = attenueCouleur(c);
    }
    _g.setColor(c);
    _g.fillOval(3, 3, w - 5, h - 5);
    c = Color.blue;
    if (attenue) {
      c = attenueCouleur(c);
    }
    _g.setColor(c);
    _g.fillOval(7, 7, w - 14, h - 14);
    _g.translate(-_x, -_y);
  }

  /**
   * @param _i le nouvel indice du temps
   */
  public void setT(final int _i) {
    getMainTimeModel().setSelectionInterval(_i, _i);
  }

  @Override
  public void setTimeStep(final int _idx) {
    setT(_idx);
  }

  @Override
  protected void updateCalqueInfo() {
    final String var = getIsoModel().getVariableNameSelected();
    if (var == null) {
      putCalqueInfo(null);
    } else {
      final String time = getIsoModel().getTimeSelected();
      putCalqueInfo(time == null ? var : var + CtuluLibString.ESPACE + time);
    }
  }

  public final void timeUpdated() {
    if (paletteCouleur_ == null) {
      updateLegende();
      return;
    }
    updateCalqueInfo();
    paletteCouleur_.setSousTitre(getIsoModel().getTimeSelected());
    updateLegendeTitre();
    fireSelectionEvent();
    firePropertyChange("t", true, false);
    repaint();
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    if (_e.getValueIsAdjusting()) { return; }
    if (_e.getSource() == mainTimeSelectionModel_) {
      int old = getIsoModel().getTimeStep();
      getIsoModel().setT(this.mainTimeSelectionModel_.getMinSelectionIndex());
      firePropertyChange("time", old, getIsoModel().getTimeStep());
      timeUpdated();
    } else {
      super.valueChanged(_e);
    }
  }

}