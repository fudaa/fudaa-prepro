/*
 *  @creation     25 mars 2005
 *  @modification $Date: 2007-01-19 13:14:24 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuInformationsDocument;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.gui.CtuluHelpComponent;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.fudaa.commun.courbe.FudaaGrapheTimeTreeFille;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * @author Fred Deniger
 * @version $Id: TrGrapheTreeTimeFille.java,v 1.9 2007-01-19 13:14:24 deniger Exp $
 */
public class TrGrapheTreeTimeFille extends FudaaGrapheTimeTreeFille implements CtuluHelpComponent {

  /**
   * @param _g
   * @param _titre
   * @param _appli
   * @param _id
   */
  public TrGrapheTreeTimeFille(final EGGraphe _g, final String _titre, final FudaaCommonImplementation _appli,
      final BuInformationsDocument _id) {
    this(new EGFillePanel(_g), _titre, _appli, _id);
  }

  /**
   * @param _g
   * @param _titre
   * @param _appli
   * @param _id
   */
  public TrGrapheTreeTimeFille(final EGFillePanel _g, final String _titre, final FudaaCommonImplementation _appli,
      final BuInformationsDocument _id) {
    super(_g, _titre, _appli, _id);
  }

  @Override
  public String getShortHtmlHelp() {
    final CtuluHtmlWriter buf = new CtuluHtmlWriter();
    buf.h2Center(getTitle());
    buf.close(buf.para(), TrResource.getS("Affiche des �volutions temporelles"));
    buf.h3(TrResource.getS("Documents associ�s"));
    buf.addTag("lu");
    buf.close(buf.addTag("li"), impl_.buildLink(TrResource.getS("Description du composant d'affichage des courbes"),
        "common-curves"));
    return buf.toString();
  }

}
