/*
 *  @creation     11 mars 2005
 *  @modification $Date: 2007-05-04 14:01:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.collection.CtuluArrayDoubleUnique;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2DRubarSolutionsInitialesInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarBcMng;
import org.fudaa.dodico.h2d.rubar.H2dRubarSI;
import org.fudaa.dodico.h2d.type.H2dRubarFileType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarNewProjectPanel.java,v 1.14 2007-05-04 14:01:53 deniger Exp $
 */
public class TrRubarNewProjectPanel extends CtuluDialogPanel implements BuBorders, ItemListener {
  BuComboBox cbHauteurCote;
  private BuComboBox cbType;
  BuComboBox cbVitesseDebit;
  private BuLabel lbHauteurCote;
  private BuLabel lbVitesseDebitU;
  private BuLabel lbVitesseDebitV;
  private BuTextField tfFrot;
  private BuTextField textFieldNbBlockTransport;
  BuTextField tfHauteurCote;
  BuTextField tfU;
  BuTextField tfV;
  List<BuTextField> textFieldsBlockConcentrations = new ArrayList<>();
  int nbElt_;
  private final String toolTipForPosValue_ = TrResource.getS("La valeur doit �tre positive");
  private final BuValueValidator validator_ = BuValueValidator.MIN(0);
  BuLabel lbType_;
  BuPanel panelTansport;

  /**
   * @param _default le type de projet a selectionner par defaut
   * @param _nbElt le nombre d'element du maillage: utilise pour construire les interfaces CIN et FRT
   */
  public TrRubarNewProjectPanel(final H2dRubarProjetType _default, final int _nbElt, boolean newFormat, int nbTransportBlock) {
    this(_default, _nbElt, true, true, false, newFormat, nbTransportBlock);
  }

  /**
   * @param _default le type de projet a selectionner par defaut
   * @param _nbElt le nombre d'element du maillage: utilise pour construire les interfaces CIN et FRT
   */
  public TrRubarNewProjectPanel(final H2dRubarProjetType _default, final int _nbElt, final boolean _frt,
                                final boolean _sin, final boolean doNotChangeTypeAndNbBlock, boolean newFormat, int nbTransportBlock) {
    // layout + border
    nbElt_ = _nbElt;
    setLayout(new BuVerticalLayout(2, true, true));
    final BuGridLayout lay = new BuGridLayout(2, 5, 5);
    setBorder(EMPTY3333);
    // choix du type de projet
    if (doNotChangeTypeAndNbBlock) {
      cbType = new BuComboBox(new Object[]{_default});
      cbType.setEnabled(false);
    } else {
      cbType = new BuComboBox(new Object[]{H2dRubarProjetType.COURANTOLOGIE_2D, H2dRubarProjetType.TRANSPORT});
      if (_default != null && _default != H2dRubarProjetType.COURANTOLOGIE_2D) {
        cbType.setSelectedItem(_default);
      }
      cbType.addItemListener(this);
    }
    BuPanel panelData = new BuPanel();
    panelData.setLayout(lay);
    lbType_ = addLabel(panelData, TrResource.getS("Choix du type de projet"));
    panelData.add(cbType);
    lbType_.setEnabled(cbType.isEnabled());
    panelData.add(new JLabel(BuResource.BU.getString("Format")));
    panelData.add(new JLabel(newFormat ? H2dRubarFileType.NEW_FORMAT_9_DIGITS.getName() : H2dRubarFileType.OLD_FORMAT_6_DIGITS.getName()));

    add(panelData);
    BuLabel lb = new BuLabel(TrResource.getS("Les valeurs saisies ci-dessous seront appliqu�es sur tous les noeuds"));
    add(lb);
    if (_frt) {
      panelData = new BuPanel();
      panelData.setLayout(lay);
      panelData.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), H2dResource
          .getS("Frottement")));
      addLabel(panelData, TrResource.getS("Frottement par d�faut"));
      tfFrot = addDoubleText(panelData, CtuluLibString.EMPTY_STRING);
      tfFrot.setValueValidator(validator_);
      tfFrot.setToolTipText(toolTipForPosValue_);
      add(panelData);
    }
    if (_sin) {
      panelData = new BuPanel();
      panelData.setLayout(lay);
      panelData.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), H2dResource
          .getS("Conditions initiales")));
      cbHauteurCote = new BuComboBox(new H2dVariableType[]{H2dVariableType.HAUTEUR_EAU, H2dVariableType.COTE_EAU});
      lb = new BuLabel(H2dVariableType.HAUTEUR_EAU + "/" + H2dVariableType.COTE_EAU);
      lb.setToolTipText(TrResource.getS("Pr�ciser la variable � utiliser"));
      panelData.add(lb);
      panelData.add(cbHauteurCote);
      cbHauteurCote.setSelectedIndex(0);
      lbHauteurCote = new BuLabel();
      lbHauteurCote.setText(cbHauteurCote.getSelectedItem().toString());
      panelData.add(lbHauteurCote);
      tfHauteurCote = addDoubleText(panelData, CtuluLibString.EMPTY_STRING);
      if (cbHauteurCote.getSelectedItem() == H2dVariableType.HAUTEUR_EAU) {
        tfHauteurCote.setValueValidator(validator_);
        tfHauteurCote.setToolTipText(toolTipForPosValue_);
      } else {
        tfHauteurCote.setValueValidator(null);
        tfHauteurCote.setToolTipText(null);
      }
      cbVitesseDebit = new BuComboBox(new H2dVariableType[]{H2dVariableType.DEBIT, H2dVariableType.VITESSE});
      lb = new BuLabel(H2dVariableType.DEBIT.getName() + '/' + H2dVariableType.VITESSE.getName());
      lb.setToolTipText(TrResource.getS("Pr�ciser la variable � utiliser"));
      panelData.add(lb);
      panelData.add(cbVitesseDebit);
      cbVitesseDebit.setSelectedIndex(0);
      lbVitesseDebitU = new BuLabel(H2dVariableType.DEBIT_X.getName());
      lbVitesseDebitV = new BuLabel(H2dVariableType.DEBIT_Y.getName());
      panelData.add(lbVitesseDebitU);
      tfU = addDoubleText(panelData, CtuluLibString.EMPTY_STRING);
      panelData.add(lbVitesseDebitV);
      tfV = addDoubleText(panelData, CtuluLibString.EMPTY_STRING);

      panelData.add(new BuLabel(H2dResource.getS("Nombre de blocs transports")));
      textFieldNbBlockTransport = addIntegerText(panelData, nbTransportBlock);
      textFieldNbBlockTransport.setValueValidator(BuValueValidator.MIN(1));
      add(panelData);

      panelTansport = new BuPanel();
      panelTansport.setLayout(lay);
      add(panelTansport);

      cbVitesseDebit.addItemListener(this);
      cbHauteurCote.addItemListener(this);
      updateNbBlockTransportState();
      updateConcentrationBlocks();
      textFieldNbBlockTransport.getDocument().addDocumentListener(new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
          updateConcentrationBlocks();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
          updateConcentrationBlocks();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
          updateConcentrationBlocks();
        }
      });
    }
  }

  List<H2dVariableTransType> h2dVariableTransTypes;

  private void updateConcentrationBlocks() {
    setErrorText(null);
    if (textFieldNbBlockTransport.getValue() == null) {
      setErrorText(TrLib.getString("Le nombre de block de transport n'est pas correct"));
      return;
    }
    if (panelTansport == null) {
      return;
    }
    for (BuTextField textFieldsBlockConcentration : textFieldsBlockConcentrations) {
      textFieldsBlockConcentration.setEnabled(getSelectedProjetType().isTransportType());
    }
    if (!getSelectedProjetType().isTransportType()) {
      return;
    }
    final List<H2dVariableTransType> newH2dVariableTransTypes = H2dRubarBcMng.getTransportVariables((Integer) textFieldNbBlockTransport.getValue());

    //no modification
    if (h2dVariableTransTypes != null && h2dVariableTransTypes.size() == newH2dVariableTransTypes.size()) {
      return;
    }
    h2dVariableTransTypes = newH2dVariableTransTypes;
    panelTansport.removeAll();
    final int nbTextField = textFieldsBlockConcentrations.size();
    if (h2dVariableTransTypes.size() > nbTextField) {
      for (int i = nbTextField; i < h2dVariableTransTypes.size(); i++) {
        final BuTextField doubleTextField = createDoubleTextField();
        doubleTextField.setToolTipText(toolTipForPosValue_);
        doubleTextField.setValueValidator(validator_);
        textFieldsBlockConcentrations.add(doubleTextField);
      }
    }
    int idx = 0;
    for (H2dVariableTransType h2dVariableTransType : h2dVariableTransTypes) {
      panelTansport.add(new BuLabel(h2dVariableTransType.getName()));
      panelTansport.add(textFieldsBlockConcentrations.get(idx++));
    }
    revalidate();
    JDialog dialog = (JDialog) SwingUtilities.getAncestorOfClass(JDialog.class, this);
    if (dialog != null) {
      dialog.pack();
      dialog.repaint();
      repaint(0);
    }
  }

  private void updateNbBlockTransportState() {
    //if the type of project is block we block the number of block too
    textFieldNbBlockTransport.setEnabled(cbType.isEnabled() && getSelectedProjetType().isTransportType());
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if (_e.getStateChange() != ItemEvent.SELECTED) {
      return;
    }
    if (_e.getSource() == cbHauteurCote) {
      final Object o = cbHauteurCote.getSelectedItem();
      lbHauteurCote.setText(o.toString());
      tfHauteurCote.setText(CtuluLibString.EMPTY_STRING);
      String tooltip = null;
      BuValueValidator val = null;
      if (o == H2dVariableType.HAUTEUR_EAU) {
        tooltip = toolTipForPosValue_;
        val = validator_;
      } else {
        val = BuValueValidator.DOUBLE;
      }
      tfHauteurCote.setToolTipText(tooltip);
      tfHauteurCote.setValueValidator(val);
    } else if (_e.getSource() == cbVitesseDebit) {
      final Object o = cbVitesseDebit.getSelectedItem();
      tfU.setText(CtuluLibString.EMPTY_STRING);
      tfV.setText(CtuluLibString.EMPTY_STRING);
      if (o == H2dVariableType.VITESSE) {
        lbVitesseDebitU.setText(H2dVariableType.VITESSE_U.getName());
        lbVitesseDebitV.setText(H2dVariableType.VITESSE_V.getName());
      } else {
        lbVitesseDebitU.setText(H2dVariableType.DEBIT_X.getName());
        lbVitesseDebitV.setText(H2dVariableType.DEBIT_Y.getName());
      }
    } else if (_e.getSource() == cbType) {
      updateNbBlockTransportState();
      updateConcentrationBlocks();
    }
  }

  public H2dRubarProjetType getSelectedProjetType() {
    return (H2dRubarProjetType) cbType.getSelectedItem();
  }

  private class BuildSI implements H2DRubarSolutionsInitialesInterface {
    CtuluArrayDoubleUnique h_;

    @Override
    public boolean isConcentrationParHauteur() {
      return false;
    }

    CtuluArrayDoubleUnique velocityU;
    CtuluArrayDoubleUnique velocityV;
    CtuluArrayDoubleUnique cote_;
    CtuluArrayDoubleUnique[] transportValues;

    /**
     * Construit les modeles � partir des valeurs entr�es.
     */
    public BuildSI() {
      if (cbHauteurCote.getSelectedItem() == H2dVariableType.HAUTEUR_EAU) {
        h_ = new CtuluArrayDoubleUnique(((Double) tfHauteurCote.getValue()).doubleValue(), nbElt_);
        cote_ = new CtuluArrayDoubleUnique(H2dRubarSI.INDETERMINED_VALUE, nbElt_);
      } else {
        h_ = new CtuluArrayDoubleUnique(H2dRubarSI.H_INDETERMINED_VALUE, nbElt_);
        cote_ = new CtuluArrayDoubleUnique(((Double) tfHauteurCote.getValue()).doubleValue(), nbElt_);
      }
      velocityU = new CtuluArrayDoubleUnique(((Double) tfU.getValue()).doubleValue(), nbElt_);
      velocityV = new CtuluArrayDoubleUnique(((Double) tfV.getValue()).doubleValue(), nbElt_);
      if (getSelectedProjetType() == H2dRubarProjetType.TRANSPORT) {
        final int nbTransportVariable = h2dVariableTransTypes.size();
        transportValues = new CtuluArrayDoubleUnique[nbTransportVariable];
        for (int i = 0; i < transportValues.length; i++) {
          transportValues[i] = new CtuluArrayDoubleUnique(((Double) textFieldsBlockConcentrations.get(i).getValue()).doubleValue(), nbElt_);
        }
      }
    }

    @Override
    public int getCommonValueSize() {
      return nbElt_;
    }

    @Override
    public int getNbValues() {
      return getSelectedProjetType() == H2dRubarProjetType.TRANSPORT ? 5 : 4;
    }

    @Override
    public double getT0() {
      return 0;
    }

    @Override
    public CtuluCollectionDoubleEdit getValues(final int index) {
      if (index == 0) {
        return h_;
      }
      if (index == 1) {
        return velocityU;
      }
      if (index == 2) {
        return velocityV;
      }
      if (index == 3) {
        return cote_;
      }
      if (index >= 4 && (index - 4) < transportValues.length) {
        return transportValues[index - 4];
      }
      return null;
    }

    @Override
    public boolean isVitesse() {
      return cbVitesseDebit.getSelectedItem() == H2dVariableType.VITESSE;
    }
  }

  public H2DRubarSolutionsInitialesInterface createSolutionInitales() {
    return new BuildSI();
  }

  public CtuluCollectionDoubleEdit getFrt() {
    return new CtuluArrayDoubleUnique(((Double) tfFrot.getValue()).doubleValue(), nbElt_);
  }

  @Override
  public boolean isDataValid() {
    final StringBuffer buf = new StringBuffer();

    if (tfFrot != null && !tfFrot.getValueValidator().isValueValid(tfFrot.getValue())) {
      buf.append(H2dVariableType.COEF_FROTTEMENT).append(':').append(CtuluLibString.ESPACE).append(
          tfFrot.getText().length() == 0 ? getStrVide() : tfFrot.getToolTipText());
    }
    if (tfU != null) {
      if (!tfU.getValueValidator().isValueValid(tfU.getValue())) {
        if (buf.length() > 0) {
          buf.append(CtuluLibString.LINE_SEP);
        }
        buf.append(lbVitesseDebitU.getText()).append(':').append(CtuluLibString.ESPACE).append(getStrVide());
      }
      if (tfV != null && !tfV.getValueValidator().isValueValid(tfV.getValue())) {
        if (buf.length() > 0) {
          buf.append(CtuluLibString.LINE_SEP);
        }
        buf.append(lbVitesseDebitV.getText()).append(':').append(CtuluLibString.ESPACE).append(getStrVide());
      }
    }
    if (tfHauteurCote != null && !tfHauteurCote.getValueValidator().isValueValid(tfHauteurCote.getValue())) {
      if (buf.length() > 0) {
        buf.append(CtuluLibString.LINE_SEP);
      }
      buf.append(lbHauteurCote.getText()).append(':').append(CtuluLibString.ESPACE).append(
          tfHauteurCote.getText().length() == 0 ? getStrVide() : tfHauteurCote.getToolTipText());
    }
    if (getSelectedProjetType().isTransportType() && h2dVariableTransTypes!=null) {
      for (int i = 0; i < h2dVariableTransTypes.size(); i++) {
        final BuTextField textField = textFieldsBlockConcentrations.get(i);
        if (!textField.getValueValidator().isValueValid(textField.getValue())) {
          if (buf.length() > 0) {
            buf.append(CtuluLibString.LINE_SEP);
          }
          buf.append(h2dVariableTransTypes.get(i).getName()).append(':').append(CtuluLibString.ESPACE).append(
              textField.getText().length() == 0 ? getStrVide() : textField.getToolTipText());
        }
      }
    }

    if (buf.length() > 0) {
      setErrorText(buf.toString());
      return false;
    }
    return true;
  }

  private String getStrVide() {
    return CtuluLib.getS("vide");
  }
}
