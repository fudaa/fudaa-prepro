/*
 * @creation 11 juin 2004
 * @modification $Date: 2007-05-04 14:01:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuBorderLayout;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryFlowrateGroupType;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryTarageGroupType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.fudaa.fdico.FDicoEditorInterface;
import org.fudaa.fudaa.fdico.FDicoTableEditorChooser;
import org.fudaa.fudaa.tr.common.TrProjectDispatcherListener;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import java.awt.*;

/**
 * @author Fred Deniger
 */
public class TrRubarProjectDispatcherListener extends TrProjectDispatcherListener implements H2dRubarProjectDispatcherListener {
  /**
   * ne fait rien.
   */
  public TrRubarProjectDispatcherListener() {
  }

  @Override
  public void apportChanged(final H2dRubarApportSpatialMng h2dRubarApportSpatialMng) {
    super.setParamsChanged();
  }

  @Override
  public void ouvrageAdded(final H2dRubarOuvrageMng h2dRubarOuvrageMng) {
    super.setParamsChanged();
    displayDialogToChangeWork(h2dRubarOuvrageMng);
  }

  /**
   * Used to show user a dialog if some structure option should be turned off/on
   *
   * @param h2dRubarOuvrageMng the manager
   */
  private void displayDialogToChangeWork(H2dRubarOuvrageMng h2dRubarOuvrageMng) {
    final DicoParams dicoParams = super.proj_.getDicoParams();
    final DicoEntite dicoEntite = H2dRubarDicoModel.getOuvrage(dicoParams.getDicoFileFormatVersion());
    boolean workActivated = H2dRubarDicoModel.workActivatedByValue(dicoParams.getValue(dicoEntite));

    boolean hasWork = !h2dRubarOuvrageMng.isEmpty();

    String title = TrResource.getS("Option ouvrages hydrauliques");
    if (workActivated && !hasWork) {
      String message = TrResource.getS("Le projet ne contient plus d'ouvrages.<br> L'option '{0}' devrait �tre modifi�e en cons�quence", dicoEntite.getNom());
      editValue(title, message, dicoParams, dicoEntite);
    }
    if (!workActivated && hasWork) {
      String message = TrResource.getS("Le projet contient d�sormais au moins un ouvrage.<br> L'option '{0}' devrait �tre modifi�e en cons�quence", dicoEntite.getNom());
      editValue(title, message, dicoParams, dicoEntite);
    }
  }

  /**
   * Used to show user a dialog if some storm water option should be turned off/on
   *
   * @param h2dRubarApportSpatialMng the manager
   */
  private void displayDialogToChangeRain(H2dRubarApportSpatialMng h2dRubarApportSpatialMng) {
    final DicoParams dicoParams = super.proj_.getDicoParams();
    final DicoEntite dicoEntite = H2dRubarDicoModel.getOuvrage(dicoParams.getDicoFileFormatVersion());
    boolean rainActivated = H2dRubarDicoModel.rainActivatedByValue(dicoParams.getValue(dicoEntite));

    boolean isRainUsed = h2dRubarApportSpatialMng.containUsedCurveNotEmpty();

    String title = TrResource.getS("Option pluie");
    if (rainActivated && !isRainUsed) {
      String message = TrResource.getS("Le projet ne contient plus de chronique d'apport.<br> L'option '{0}' devrait �tre modifi�e en cons�quence", dicoEntite.getNom());
      editValue(title, message, dicoParams, dicoEntite);
    }
    if (!rainActivated && isRainUsed) {
      String message = TrResource
          .getS("Le projet contient d�sormais au moins une chronique d'apport.<br> L'option '{0}' devrait �tre modifi�e en cons�quence", dicoEntite.getNom());
      editValue(title, message, dicoParams, dicoEntite);
    }
  }

  /**
   * @param title the title of the dialog
   * @param message the message
   * @param dicoParams the dico params containing values
   * @param dicoEntite the entity to change
   */
  private void editValue(String title, String message, DicoParams dicoParams, DicoEntite dicoEntite) {
    FDicoTableEditorChooser uiBuilder = new FDicoTableEditorChooser();
    final FDicoEditorInterface editor = uiBuilder.getEditor(dicoEntite);
    editor.setValue(dicoParams.getValue(dicoEntite));
    CtuluDialogPanel pn = new CtuluDialogPanel(false);
    pn.setLayout(new BuBorderLayout(0, 15));
    pn.add(new JLabel("<html><body>" + message), BorderLayout.NORTH);
    pn.add(editor.getComponent(), BuBorderLayout.CENTER);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale((Component) proj_.getVisuFille(), title))) {
      dicoParams.setValue(dicoEntite, editor.getValue());
    }
  }

  @Override
  public void ouvrageChanged(final H2dRubarOuvrageMng h2dRubarOuvrageMng, final H2dRubarOuvrage rubarOuvrage, final boolean ouvrageElementaireAddedOrRemoved) {
    super.setParamsChanged();
  }

  @Override
  public void sedimentChanged(H2dRubarSedimentMng h2dRubarSedimentMng) {
    super.setParamsChanged();
  }

  @Override
  public void ventChanged(H2dRubarVentMng h2dRubarVentMng) {
    super.setParamsChanged();
  }

  @Override
  public void ventEvolutionContentChanged(H2dRubarVentMng h2dRubarVentMng, EvolutionReguliereInterface dest) {
    super.setParamsChanged();
  }

  @Override
  public void ventEvolutionUsedChanged(H2dRubarVentMng h2dRubarVentMng, EvolutionReguliereInterface dest) {
    super.setParamsChanged();
  }

  @Override
  public void ouvrageElementaireChanged(final H2dRubarOuvrageMng h2dRubarOuvrageMng, final H2dRubarOuvrage h2dRubarOuvrage,
                                        final H2dRubarOuvrageElementaireInterface elementaireInterface) {
    super.setParamsChanged();
  }

  @Override
  public void bathyChanged() {
    super.setParamsChanged();
    fireGridDataChanged(H2dVariableType.BATHYMETRIE);
  }

  @Override
  public void nodeInGridChanged() {
    super.setParamsChanged();
  }

  @Override
  public void fondDurChanged() {
    super.setParamsChanged();
    fireGridDataChanged(H2dVariableTransType.FOND_INERODABLE);
  }

  @Override
  public void ouvrageRemoved(final H2dRubarOuvrageMng h2dRubarOuvrageMng) {
    super.setParamsChanged();
    displayDialogToChangeWork(h2dRubarOuvrageMng);
  }

  @Override
  public void limniPointChanged(final H2dRubarLimniMng h2dRubarLimniMng) {
    super.setParamsChanged();
  }

  @Override
  public void limniTimeStepChanged(final H2dRubarLimniMng h2dRubarLimniMng) {
    super.setParamsChanged();
  }

  @Override
  public void apportEvolutionContentChanged(final H2dRubarApportSpatialMng h2dRubarApportSpatialMng, final EvolutionReguliereInterface evolution) {
    if (proj_ == null) {
      return;
    }
    if (evolution.isUsed()) {
      super.setParamsChanged();
    }
  }

  @Override
  public void apportEvolutionUsedChanged(final H2dRubarApportSpatialMng h2dRubarApportSpatialMng, final EvolutionReguliereInterface evolution) {
    displayDialogToChangeRain(h2dRubarApportSpatialMng);
  }

  @Override
  public void areteTypeChanged() {
    super.setParamsChanged();
  }

  @Override
  public void dicoParamsStateLoadedEntiteChanged(final DicoParams dicoParams, final DicoEntite dicoEntite) {
    if (proj_ == null) {
      return;
    }
    FuLog.warning(new Throwable());
  }

  @Override
  public void dicoParamsValidStateEntiteUpdated(final DicoParams dicoParams, final DicoEntite dicoEntite) {
    if (proj_ == null) {
      return;
    }
    FuLog.warning(new Throwable());
  }

  public String getPrefix(final H2dRubarDonneesBrutes h2dRubarDonneesBrutes) {
    return "unprocessed " + h2dRubarDonneesBrutes.getTitle();
  }

  @Override
  public void donneesBrutesDataChanged(final H2dRubarDonneesBrutes h2dRubarDonneesBrutes) {
    CtuluLibMessage.info(getPrefix(h2dRubarDonneesBrutes) + " data changed");
    super.setParamsChanged();
  }

  @Override
  public void donneesBrutesDataNombreChanged(final H2dRubarDonneesBrutes h2dRubarDonneesBrutes) {
    CtuluLibMessage.info(getPrefix(h2dRubarDonneesBrutes) + " data nombre changed");
    super.setParamsChanged();
  }

  private String getPrefXY(final H2dRubarDonneesBrutes _source) {
    return "unprocessed xy " + _source.getTitle();
  }

  @Override
  public void donneesBrutesNuageDataChanged(final H2dRubarDonneesBrutes donneesBrutes) {
    CtuluLibMessage.info(getPrefXY(donneesBrutes) + " data changed");
    super.setParamsChanged();
  }

  @Override
  public void donneesBrutesNuageDataNombreChanged(final H2dRubarDonneesBrutes donneesBrutes) {
    CtuluLibMessage.info(getPrefXY(donneesBrutes) + " data nombre changed");
    super.setParamsChanged();
  }

  @Override
  public void donneesBrutesNuageSupportAddedOrRemoved(final H2dRubarDonneesBrutes donneesBrutes) {
    CtuluLibMessage.info(getPrefXY(donneesBrutes) + " support added/removed");
    super.setParamsChanged();
  }

  @Override
  public void donneesBrutesNuageSupportChanged(final H2dRubarDonneesBrutes donneesBrutes) {
    CtuluLibMessage.info(getPrefXY(donneesBrutes) + " support changed");
    super.setParamsChanged();
  }

  @Override
  public void donneesBrutesSupportAddedOrRemoved(final H2dRubarDonneesBrutes donneesBrutes) {
    CtuluLibMessage.info(getPrefXY(donneesBrutes) + " support added/removed");
    super.setParamsChanged();
  }

  @Override
  public void donneesBrutesSupportChanged(final H2dRubarDonneesBrutes donneesBrutes) {
    CtuluLibMessage.info(getPrefix(donneesBrutes) + " support changed");
    super.setParamsChanged();
  }

  @Override
  public void donneesBrutesTypeChanged(final H2dRubarDonneesBrutes donneesBrutes) {
    CtuluLibMessage.info(getPrefix(donneesBrutes) + " type changed");
    super.setParamsChanged();
  }

  @Override
  public void flowrateGroupChanged(final H2dRubarBoundaryFlowrateGroupType flowrateGroupType) {
    if (proj_ == null) {
      return;
    }
    CtuluLibMessage.info("group " + flowrateGroupType.getName() + " changed");
    super.setParamsChanged();
  }

  @Override
  public void frictionChanged() {
    fireGridDataChanged(H2dVariableType.COEF_FROTTEMENT_FOND);
    super.setParamsChanged();
  }

  @Override
  public void diffusionChanged(final boolean addOrRemove) {
    fireGridDataChanged(H2dVariableType.COEF_DIFFUSION);
    CtuluLibMessage.info("diffusion changed");
    super.setParamsChanged();
  }

  @Override
  public void siAdded(final H2dVariableType variableType) {
    super.setParamsChanged();
    fireGridDataChanged(variableType);
    if (variableType == null) {
      CtuluLibMessage.info("SI: values changed");
    } else {
      CtuluLibMessage.info("SI: " + variableType.getName() + " added");
    }
  }

  @Override
  public void siChanged(final H2dVariableType variableType) {
    if (proj_ == null) {
      return;
    }
    super.setParamsChanged();
    fireGridDataChanged(variableType);
    if (variableType == null) {
      CtuluLibMessage.info("SI: values changed");
    } else {
      CtuluLibMessage.info("SI: " + variableType.getName() + " changed");
    }
  }

  @Override
  public void siRemoved(final H2dVariableType variableType) {
    if (proj_ == null) {
      return;
    }
    fireGridDataChanged(variableType);
    super.setParamsChanged();
    if (variableType == null) {
      CtuluLibMessage.info("SI: values changed");
    } else {
      CtuluLibMessage.info("SI: " + variableType.getName() + " removed");
    }
  }

  /**
   * le manager de courbe de tarage utilise aussi ce listener meme pour des courbes non utilisees.
   */
  @Override
  public void tarageCourbeChanged(final H2dRubarTarageMng h2dRubarTarageMng, final EvolutionReguliereInterface evolution) {
    if (proj_ == null) {
      return;
    }
    if (evolution.isUsed()) {
      super.setParamsChanged();
    }
  }

  @Override
  public void TarageGroupeUsedChanged(H2dRubarTarageMng h2dRubarTarageMng, H2dRubarBoundaryTarageGroupType tarageGroupeType) {
  }

  @Override
  public void tarageUsedChanged(final H2dRubarTarageMng h2dRubarTarageMng) {
    super.setParamsChanged();
  }

  @Override
  public void timeClChanged() {
    if (proj_ == null) {
      return;
    }
    super.setParamsChanged();
  }

  @Override
  public void projectTypeChanged() {
    if (proj_ == null) {
      return;
    }
    super.setParamsChanged();
  }

  @Override
  public void numberOfConcentrationChanged() {

  }

  @Override
  public void dicoParamsVersionChanged(final DicoParams dicoParams) {
  }
}
