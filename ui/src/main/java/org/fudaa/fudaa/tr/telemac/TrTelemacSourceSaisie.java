/**
 * @creation 30 nov. 2004
 * @modification $Date: 2007-06-11 14:34:31 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import org.fudaa.dodico.h2d.telemac.H2dTelemacSourceMng;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.meshviewer.layer.MvNodeLayer;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrSaisiePoint;
import org.fudaa.fudaa.tr.data.TrVisuPanel;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacSourceSaisie.java,v 1.14 2007-06-11 14:34:31 clavreul Exp $
 */
public class TrTelemacSourceSaisie extends TrSaisiePoint {
  private TrTelemacSourceEditor editor_;
  private MvNodeLayer nodes_;

  /**
   * @param trVisuPanel
   * @param trTelemacSourceLayer
   */
  public TrTelemacSourceSaisie(final TrVisuPanel trVisuPanel, final TrTelemacSourceLayer trTelemacSourceLayer) {
    super(trVisuPanel, trTelemacSourceLayer);
    nodes_ = trVisuPanel.getGridGroup().getPointLayer();
  }

  @Override
  protected void buildPanel() {
    add(infoLabel_);
    editor_ = new TrTelemacSourceEditor(null, ((TrTelemacSourceLayer) layer_).modeleDonnees().getMng(), super.pn_);
    add(editor_);
    add(bt_);
  }

  @Override
  public void pointClicked(GrPoint ptReel) {
    final int i = ZCalquePoint.getSelectedPoint(nodes_.modele(), ptReel, 5, nodes_.getVersReel(), pn_.getVueCalque()
        .getViewBoite());
    if (i < 0) {
      infoLabel_.setText(TrResource.getS("Aucun noeud trouv�"));
      return;
    }
    nodes_.modele().point(ptReel, i, true);
    final H2dTelemacSourceMng mng = ((TrTelemacSourceLayer) layer_).modeleDonnees().getMng();
    final String[] val = editor_.getValues();
    val[0] = Double.toString(ptReel.x_);
    val[1] = Double.toString(ptReel.y_);
    mng.addSource(val, pn_.getCmdMng());
    pointAjouteOk(val[0], val[1]);
  }
}
