package org.fudaa.fudaa.tr.rubar;

import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.dodico.h2d.H2dVariableProviderContainerInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarEvolution;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrExpressionSupplierForData;
import org.nfunk.jep.Variable;

/**
 * Search feature for Rubar meshes.
 */
public class TrRubarElementModelExpression extends TrExpressionSupplierForData.Element {
  private Variable hasStormWater;
  private Variable hasWind;
  private Variable hasWindX;
  private Variable hasWindY;
  private final H2dRubarParameters rubarParameters;
  private Variable stormWaterName;
  private Variable windX;
  private Variable windY;

  public TrRubarElementModelExpression(H2dVariableProviderContainerInterface _container, TrRubarElementModel _nodes, H2dRubarParameters rubarParameters) {
    super(_container, _nodes);
    this.rubarParameters = rubarParameters;
  }

  @Override
  public void initialiseExpr(CtuluExpr _expr) {
    super.initialiseExpr(_expr);
    if (rubarParameters != null) {
      hasStormWater = _expr.addVar("hasStormWater", TrResource.getS("Contient Chronique apport"));
      hasWind = _expr.addVar("hasWind", TrResource.getS("Contient vent"));
      hasWindX = _expr.addVar("hasWindX", TrResource.getS("Contient vent selon X"));
      hasWindY = _expr.addVar("hasWindY", TrResource.getS("Contient vent selon Y"));
      stormWaterName = _expr.addVar("stormWaterName", TrResource.getS("Nom Chronique apport"));
      windX = _expr.addVar("windXName", TrResource.getS("Nom Courbe Vent selon X"));
      windY = _expr.addVar("windYName", TrResource.getS("Nom Courbe Vent selon Y"));
    }
  }

  @Override
  public void majVariable(int _idx, Variable[] _varToUpdate) {
    super.majVariable(_idx, _varToUpdate);
    TrRubarElementModel elementModel = (TrRubarElementModel) super.data_;
    if (rubarParameters != null) {
      updateStormWater(_idx);

      final H2dRubarEvolution alongX = rubarParameters.getVentMng().getEvolutionAlongX(_idx);
      final H2dRubarEvolution alongY = rubarParameters.getVentMng().getEvolutionAlongY(_idx);
      hasWindX.setValue(alongX != null);
      hasWindY.setValue(alongY != null);
      hasWind.setValue(alongX != null || alongY != null);
      if (alongX != null) {
        windX.setValue(alongX.getNom());
      } else {
        windX.setValue(null);
      }
      if (alongY != null) {
        windY.setValue(alongY.getNom());
      } else {
        windY.setValue(null);
      }
    }
  }

  private void updateStormWater(int _idx) {
    final H2dRubarEvolution evol = rubarParameters.getAppMng().getEvol(_idx);
    hasStormWater.setValue(evol != null);
    if (evol != null) {
      stormWaterName.setValue(evol.getNom());
    } else {
      stormWaterName.setValue(null);
    }
  }
}
