/*
 * @creation 20 d�c. 2005
 * 
 * @modification $Date: 2007-06-20 12:23:42 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.export;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuPanel;
import com.memoire.fu.FuLog;
import java.io.File;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluAnalyzeGroup;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.io.corelebth.CorEleBthFileFormat;
import org.fudaa.dodico.reflux.io.RefluxRefondeSolutionFileFormat;
import org.fudaa.dodico.reflux.io.RefluxSolutionWriter;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.export.MvExportActAbstract;
import org.fudaa.fudaa.meshviewer.export.MvExportActInterface;
import org.fudaa.fudaa.meshviewer.export.MvExportTaskSkeleton;
import org.fudaa.fudaa.meshviewer.export.MvExportToT3Activity;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrExportRefluxSovAct.java,v 1.15 2007-06-20 12:23:42 deniger Exp $
 */
public class TrExportRefluxSovAct extends MvExportActAbstract {

  /**
   * @author Fred Deniger
   * @version $Id: TrExportRefluxSovAct.java,v 1.15 2007-06-20 12:23:42 deniger Exp $
   */
  public static class CustomPanel implements MvExportTaskSkeleton.ExportPanelCustom {

    BuCheckBox cbCor_ = new BuCheckBox(TrResource.getS("G�n�rer le fichier de maillage cor,ele,bth"));
    JPanel pn_;

    @Override
    public JComponent getComponent() {
      if (pn_ == null) {
        pn_ = new BuPanel(new BuBorderLayout());
        pn_.setBorder(CtuluLibSwing.createTitleBorder(CtuluLib.getS("Options")));
        pn_.add(cbCor_);
      }
      return pn_;

    }

    @Override
    public void close() {}

    @Override
    public String getErrorMessage() {
      return null;
    }

    @Override
    public void update(final MvExportActInterface _target) {
      if (_target instanceof TrExportRefluxSovAct) {
        ((TrExportRefluxSovAct) _target).setExportCorEleBth(cbCor_.isSelected());
      }
    }
  }

  private boolean exportCorEleBth_;

  public TrExportRefluxSovAct() {}

  @Override
  protected void doExport(final ProgressionInterface _prog, final File[] _dest, final CtuluAnalyzeGroup _analyze,
      final String[] _messages) {
    initActivity();
    EfGridData last = super.processOperations(_prog, _analyze);
    if (!isInternOk(_analyze, last)) {
      if (FuLog.isTrace()) {
        FuLog.trace("TRE: stop after filter");
      }
      return;
    }
    CtuluAnalyze log=new CtuluAnalyze();
    log.setDesc(MvResource.getS("Adaptation des donn�es"));
    _analyze.addAnalyzer(log);
    // s'il y a des variables definis sur les elements on les transforme en vari sur les noeuds
    last = filterToNodes(_prog, last, log);
    if (!isInternOk(_analyze, last)) {
      if (FuLog.isTrace()) {
        FuLog.trace("TRE: top after transform to node data");
      }
      return;
    }
    final EfElementType eltType = last.getGrid().getEltType();
    if (exportCorEleBth_ && eltType != EfElementType.T6 && eltType != EfElementType.T3) {
      final MvExportToT3Activity toT3 = new MvExportToT3Activity(last);
      setCurrentActivity(toT3);
      last = toT3.process(_prog, log);
      if (!isInternOk(_analyze, last)) {
        if (FuLog.isTrace()) {
          FuLog.trace("TRE: top after transform to T3");
        }
        return;
      }
    }
    if (exportCorEleBth_) {
      CorEleBthFileFormat.getInstance().writeGrid(_dest[0], last.getGrid(), _prog);
      if (!isInternOk(_analyze, last)) {
        if (FuLog.isTrace()) {
          FuLog.trace("TRE: top after write grid");
        }
        return;
      }
    }

    final TrExportRefluxSovAdapter sov = new TrExportRefluxSovAdapter(super.selectedTimeStep_, last);
    final RefluxSolutionWriter writer = (RefluxSolutionWriter) RefluxRefondeSolutionFileFormat.getInstance()
        .createWriter();
    setCurrentActivity(writer);
    File dest = _dest[0];

    final String ext = RefluxRefondeSolutionFileFormat.isOk2D(last) ? ".sov" : ".sot";
    if (!dest.getName().toLowerCase().endsWith(ext)) {
      dest = new File(dest.getAbsolutePath() + ext);
    }
    writer.write(sov, dest, _prog);
    addInfo(_messages, last.getGrid().getPtsNb(), last.getGrid().getEltNb());
    initActivity();
  }

  public TrExportRefluxSovAct.CustomPanel createCustom() {
    return new CustomPanel();
  }

  @Override
  public int getNbLabelInfoNeeded() {
    return 2;
  }

  @Override
  public String getTitre() {
    return RefluxRefondeSolutionFileFormat.getInstance().getName();
  }

  public final boolean isExportCorEleBth() {
    return exportCorEleBth_;
  }

  public final void setExportCorEleBth(final boolean _exportCorEleBth) {
    exportCorEleBth_ = _exportCorEleBth;
  }

}
