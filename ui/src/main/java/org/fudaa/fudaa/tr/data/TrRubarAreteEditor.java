/*
  @creation 11 juin 2004
 * @modification $Date: 2007-06-28 09:28:18 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuResource;
import org.apache.commons.collections.CollectionUtils;
import org.fudaa.ctulu.CtuluCommandCompositeInverse;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.*;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.fudaa.tr.common.*;
import org.jdesktop.swingx.JXTitledSeparator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarAreteEditor.java,v 1.27 2007-06-28 09:28:18 deniger Exp $
 */
public final class TrRubarAreteEditor extends CtuluDialogPanel {
  private class TarageEvolution implements ItemListener {
    JComboBox evols_;
    JLabel l_;
    BuLabel lbError_;

    public TarageEvolution(final H2dRubarBoundaryType _t) {
      buildPanel(_t);
    }

    private void buildPanel(final H2dRubarBoundaryType _t) {
      l_ = addLabel(H2dResource.getS("Courbe de tarage"));
      if (tarageEvolutions_.getNbEvol() == 0) {
        lbError_ = addLabel(TrResource.getS("Pas de courbes disponibles"));
        lbError_.setVisible(false);
        lbError_.setForeground(Color.red);
      } else {
        if (_t.isTypeTarageGlobal()) {
          if (!bcMng_.getTarageMng().isTarageGroupDefined((H2dRubarBoundaryTarageGroupType) _t)) {
            lbError_ = addLabel(TrResource.getS("Le groupe n'est pas d�fini"));
            lbError_.setVisible(false);
            lbError_.setForeground(Color.red);
          } else {
            EvolutionReguliereAbstract condition = bcMng_.getTarageMng().getGroupTimeCondition((H2dRubarBoundaryTarageGroupType) _t);
            evols_ = new BuComboBox(new Object[]{condition});
            evols_.setSelectedIndex(0);
            evols_.setEnabled(false);
          }
        } else if (_t == H2dRubarBcTypeList.TARAGE) {
          final EvolutionReguliereInterface first = bcMng_.getTarageMng().getCommonEvolution(ar_);
          if (first == null) {
            if (bcMng_.getTarageMng().isAllCurveSet(ar_)) {
              evols_ = tarageEvolutions_.buildCombobox(EvolutionReguliere.MIXTE);
            } else {
              evols_ = tarageEvolutions_.buildCombobox(null);
            }
          } else {
            evols_ = tarageEvolutions_.buildCombobox(null);
          }
          evols_.setSelectedItem(first);
        } else {
          evols_ = tarageEvolutions_.buildCombobox(null);
          evols_.setSelectedIndex(0);
        }
        evols_.setVisible(false);
        add(evols_);
      }

      updateState();
    }

    public EvolutionReguliereAbstract getSelectedEvolution() {
      return evols_ == null ? null : (EvolutionReguliere) evols_.getSelectedItem();
    }

    public boolean isError() {
      if (lbError_ != null && lbError_.isVisible()) {
        return true;
      }
      if (evols_.getSelectedItem() == null && !bcMng_.getTarageMng().isAllCurveSet(ar_)) {
        return true;
      }
      return false;
    }

    protected void updateState() {
      H2dRubarBoundaryType selectedBoundary = getSelectedBoundary();
      final boolean v = selectedBoundary == H2dRubarBcTypeList.TARAGE || (selectedBoundary != null && selectedBoundary.isTypeTarageGlobal());
      l_.setEnabled(v);
      if (lbError_ == null) {
        evols_.setVisible(v);
      } else {
        lbError_.setVisible(v);
      }
    }

    @Override
    public void itemStateChanged(final ItemEvent _e) {
      if (_e.getStateChange() == ItemEvent.SELECTED) {

        remove(l_);
        if (lbError_ != null) {
          remove(lbError_);
          lbError_ = null;
        }
        if (evols_ != null) {
          remove(evols_);
        }

        buildPanel((H2dRubarBoundaryType) comboBoxChooseType.getSelectedItem());
        revalidateDialog();
      }
    }

    public String getErrorMessage() {
      if (lbError_ != null && lbError_.isVisible()) {
        return TrResource.getS("Ajouter des courbes de tarages et d�finir des groupes (menu 'Projet')");
      }
      return TrResource.getS("S�lectionner une courbe de tarage");
    }
  }

  private class EvolutionEditor implements ItemListener {
    boolean error_;
    int idxBlock;
    BuComboBox evols_;
    JLabel l_;
    BuLabel lbError_;
    // Object selectedItem_;
    TrCourbeComboboxModel model_;
    H2dVariableType t_;

    /**
     * @param _t le type de variable
     * @param _selected les conditions communes ou nulles si aucunes
     * @param _group true si un groupe debit est selectionne
     * @param _enable true si la variable est activee
     */
    public EvolutionEditor(final H2dVariableType _t, final H2dRubarTimeConditionCommon _selected, final boolean _group, final int idxBlock, final boolean _enable,
                           final boolean prefixWithIndex) {
      if (!prefixWithIndex || !H2dRubarTimeConditionConcentrationBlock.isTransientVariable(_t)) {
        l_ = addLabel(_t.getName());
      } else {
        l_ = addLabel(_t.getName() + CtuluLibString.getEspaceString(idxBlock + 1));
      }
      this.idxBlock = idxBlock;
      t_ = _t;
      if ((evolMng_ == null) || evolMng_.getNbEvol() == 0) {
        lbError_ = addLabel(TrResource.getS("Pas de courbes disponibles"));
        lbError_.setForeground(Color.red);
        error_ = true;
      } else {
        // dans ce cas on est sur que toutes les courbes sont definies
        if (_group) {
          EvolutionReguliereInterface evol = _selected.getEvol(_t, idxBlock);
          if (_t == H2dVariableType.COTE_EAU && evol == null) {
            evol = H2dRubarParameters.NON_TORENTIEL;
          }
          evols_ = new BuComboBox(new Object[]{evol});
          evols_.setEnabled(false);
        } else {
          evols_ = new BuComboBox();
          final boolean mixteNeeded = _enable && !_selected.getVarState(t_, idxBlock);
          final boolean nonTorrentielNeeded = getSelectedBoundary().isTorrentielVar(t_);
          EvolutionReguliereInterface o = _selected.getEvol(t_, idxBlock);
          if (o == null && nonTorrentielNeeded && _selected.getVarState(t_, idxBlock)) {
            o = H2dRubarParameters.NON_TORENTIEL;
          } else if (mixteNeeded) {
            o = EvolutionReguliere.MIXTE;
          }
          H2dVariableType t = _t;
          if (mixteNeeded) {
            if (nonTorrentielNeeded) {
              evolMng_.createComboBoxModelWithEmpty(EvolutionReguliere.MIXTE, H2dRubarParameters.NON_TORENTIEL, t, evols_);
            } else {
              evolMng_.createComboBoxModelWithEmpty(EvolutionReguliere.MIXTE, t, evols_);
            }
          } else if (nonTorrentielNeeded) {
            evolMng_.createComboBoxModelWithEmpty(H2dRubarParameters.NON_TORENTIEL, t, evols_);
          } else {
            evolMng_.createComboBoxModel(t, evols_);
          }
          if (o != null) {
            evols_.setSelectedItem(o);
          }
          model_ = (TrCourbeComboboxModel) evols_.getModel();
        }
        add(evols_);
      }
      setEnable(_enable);
    }

    /**
     * @return l'evolution selectionnee ou nulle si pas adapt�
     */
    public EvolutionReguliere getEvolution() {
      if ((evols_ == null) || (!l_.isEnabled())) {
        return null;
      }
      if (!getSelectedBoundary().isTypeDebitGlobal()) {
        final EvolutionReguliere r = (EvolutionReguliere) evols_.getSelectedItem();
        if (r == EvolutionReguliere.MIXTE) {
          return null;
        }
        return r;
      }
      return null;
    }

    /**
     * @return true si erreur
     */
    public boolean isError() {
      if (!l_.isEnabled()) {
        return false;
      }
      return error_;
    }

    @Override
    public void itemStateChanged(final ItemEvent _e) {
      if (_e.getStateChange() == ItemEvent.SELECTED) {
        final H2dRubarBoundaryType bc = getSelectedBoundary();
        final List list = bcMng_.getVariableAllowed(bc);
        if ((list != null) && (list.contains(t_))) {
          setEnable(true);
          if (bc.isTypeDebitGlobal()) {
            final H2dRubarTimeCondition c = bcMng_.getGroupTimeCondition((H2dRubarBoundaryFlowrateGroupType) bc);
            if (c == null) {
              updatedForGroup(TrResource.getS("Le groupe n'est pas d�fini"), true);
            } else {
              EvolutionReguliere e = c.getEvol(t_, idxBlock);
              if ((e == null) && bc.isTorrentielVar(t_)) {
                e = H2dRubarParameters.NON_TORENTIEL;
              }
              if (e != null) {
                updatedForGroup(e.getNom(), false);
              }
            }
          } else {
            updateNoGroup();
          }
        } else {
          setEnable(false);
        }
      }
      revalidateDialog();
    }

    /**
     * @param _b le nouveau etat des composants
     */
    public void setEnable(final boolean _b) {
      if (evols_ != null) {
        evols_.setEnabled(_b);
        evols_.setVisible(_b);
      }
      if (lbError_ != null) {
        lbError_.setEnabled(_b);
      }
      l_.setEnabled(_b);
    }

    /**
     * A appeler si un bord de type groupe est selectionne.
     *
     * @param _message le message a ajouter
     * @param _b true si erreur
     */
    public void updatedForGroup(final String _message, final boolean _b) {
      if (lbError_ == null) {
        error_ = _b;
        // if (model_ != null) selectedItem_ = model_.getSelectedItem();
        evols_.setForeground(_b ? Color.RED : Color.BLACK);
        evols_.setModel(new DefaultComboBoxModel(new String[]{_message}));
      }
    }

    /**
     * A appeler si un type de bord normal est selectionne.
     */
    public void updateNoGroup() {
      if (lbError_ == null) {
        error_ = false;
        evols_.setForeground(Color.black);
        if (model_ == null) {
          final boolean nonTorrentielNeeded = getSelectedBoundary().isTorrentielVar(t_);
          if (nonTorrentielNeeded) {
            evolMng_.createComboBoxModelWithEmpty(H2dRubarParameters.NON_TORENTIEL, t_, evols_);
          } else {
            evolMng_.createComboBoxModel(t_, evols_);
          }
        } else {
          final boolean nonTorrentielNeeded = getSelectedBoundary().isTorrentielVar(t_);
          if (nonTorrentielNeeded && !model_.containsRubarNonTorrentiel()) {
            evolMng_.createComboBoxModelWithEmpty(H2dRubarParameters.NON_TORENTIEL, t_, evols_);
          } else if (!nonTorrentielNeeded && model_.containsRubarNonTorrentiel()) {
            evolMng_.createComboBoxModel(t_, evols_);
          } else {
            evols_.setModel(model_);
          }
        }
        if (evols_.getModel() instanceof TrCourbeComboboxModel) {
          model_ = (TrCourbeComboboxModel) evols_.getModel();
          // evols_.setSelectedIndex(0);
        }
      }
    }
  }

  private final CtuluCommandManager cmdMng_;
  private final List<EvolutionEditor> varEvolution_;
  protected H2dRubarBcMng bcMng_;
  protected TrCourbeTemporelleManager evolMng_;
  int[] ar_;
  TrEvolutionManager tarageEvolutions_;
  BuComboBox comboBoxChooseType;
  TarageEvolution tarageEditor_;

  /**
   * @param _ar les aretes a editer
   * @param _mng les parametres
   * @param _cmdMng le receveur des commandes
   */
  public TrRubarAreteEditor(final int[] _ar, final H2dRubarParameters _mng, final CtuluCommandManager _cmdMng, final TrCourbeTemporelleManager _cMng,
                            final TrEvolutionManager _tarageEvolutions) {
    super(true);
    ar_ = _ar;
    bcMng_ = _mng.getBcMng();
    cmdMng_ = _cmdMng;
    evolMng_ = _cMng;
    tarageEvolutions_ = _tarageEvolutions;
    setLayout(new BuGridLayout(2, 5, 5));
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    final H2dRubarBoundaryType common = H2dRubarArete.getCommonType(_ar, bcMng_.getGridVolume());
    Object[] bord;
    if (common == H2dRubarBcTypeList.MIXTE) {
      final List bordL = new ArrayList(bcMng_.getBordList());
      bordL.add(H2dRubarBcTypeList.MIXTE);
      bord = bordL.toArray();
      Arrays.sort(bord);
    } else {
      bord = bcMng_.getBordList().toArray();
    }
    if (common == null) {
      varEvolution_ = Collections.emptyList();
    } else {
      addLabel(TrResource.getS("Type du bord"));
      comboBoxChooseType = new BuComboBox(bord);
      add(comboBoxChooseType);
      comboBoxChooseType.setSelectedItem(common);
      final List var = bcMng_.getAllTimeVar();
      final boolean isFlowrateGroup = common.isTypeDebitGlobal();

      H2dRubarTimeConditionCommon timeCond = null;
      final List availableVar = bcMng_.getVariableAllowed(common);
      timeCond = bcMng_.getCommonTimeCondition(_ar);

      varEvolution_ = new ArrayList<>();
      boolean prefixWithNumber = bcMng_.getNbConcentrationBlocks() > 1;
      for (final Iterator it = var.iterator(); it.hasNext(); ) {
        final H2dVariableType t = (H2dVariableType) it.next();
        if (!H2dRubarTimeConditionConcentrationBlock.isTransientVariable(t)) {
          final EvolutionEditor variableEvolution = new EvolutionEditor(t, timeCond, isFlowrateGroup, 0, availableVar == null ? false : availableVar.contains(t), prefixWithNumber);
          comboBoxChooseType.addItemListener(variableEvolution);
          varEvolution_.add(variableEvolution);
        }
      }

      for (int idxBlock = 0; idxBlock < bcMng_.getNbConcentrationBlocks(); idxBlock++) {
        if (bcMng_.getNbConcentrationBlocks() > 1) {
          final JXTitledSeparator comp = new JXTitledSeparator("Block " + (idxBlock + 1), SwingConstants.LEFT);
          comp.setFont(CtuluLibSwing.getMiniFont());
          add(comp);
          add(new JLabel());
        } else {
          add(new JLabel());
          add(new JLabel());
        }
        for (final Iterator it = var.iterator(); it.hasNext(); ) {
          final H2dVariableType t = (H2dVariableType) it.next();
          if (H2dRubarTimeConditionConcentrationBlock.isTransientVariable(t)) {
            final EvolutionEditor variableEvolution = new EvolutionEditor(t, timeCond, isFlowrateGroup, idxBlock, availableVar == null ? false : availableVar.contains(t),
                prefixWithNumber);
            comboBoxChooseType.addItemListener(variableEvolution);
            varEvolution_.add(variableEvolution);
          }
        }
      }
    }
    tarageEditor_ = new TarageEvolution(common);
    comboBoxChooseType.addItemListener(tarageEditor_);
  }

  protected H2dRubarBoundaryType getSelectedBoundary() {
    return (H2dRubarBoundaryType) comboBoxChooseType.getSelectedItem();
  }

  @Override
  public boolean apply() {
    if (comboBoxChooseType != null) {
      final H2dRubarBoundaryType t = getSelectedBoundary();
      if (!t.equals(H2dRubarBcTypeList.MIXTE)) {
        final H2dRubarTimeCondition tc = new H2dRubarTimeCondition(this.bcMng_.getNbConcentrationBlocks());

        for (EvolutionEditor variableEvolution : this.varEvolution_) {
          final Object selectedItem = variableEvolution.evols_.getSelectedItem();
          H2dRubarEvolution evolution = null;
          if (selectedItem instanceof H2dRubarEvolution) {
            evolution = (H2dRubarEvolution) selectedItem;
          }
          tc.setEvolution(variableEvolution.t_, evolution, variableEvolution.idxBlock, null, false);
        }
        final List ls = tc.getInvalidEvol();
        if (CollectionUtils.isNotEmpty(ls)) {
          String msg = null;
          if (ls.size() == 1) {
            msg = TrResource.getS("La courbe {0} contient des valeurs erron�es. Ces valeurs seront supprim�es.\nVoulez-vous continuer ?", ls.get(0)
                .toString());
          } else {

            final StringBuilder b = TrLib.join(ls);
            msg = TrResource.getS("Les courbes {0} contiennent des valeurs erron�es. Ces valeurs seront supprim�es.\nVoulez-vous continuer ?",
                b.toString());
          }
          final String yesText = BuResource.BU.getString("Continuer");
          final String noText = BuResource.BU.getString("Non");
          if (!CtuluLibDialog.showConfirmation(TrRubarAreteEditor.this, TrResource.getS("Attention"), msg, yesText, noText)) {
            return true;
          }
        }
        final CtuluCommandCompositeInverse cmd = new CtuluCommandCompositeInverse();
        cmd.addCmd(bcMng_.modifyArete(ar_, t, tc, tarageEditor_.getSelectedEvolution()));
        cmdMng_.addCmd(cmd.getSimplify());
      }
    }
    return true;
  }

  @Override
  public boolean isDataValid() {
    boolean r = true;
    StringBuilder err = null;
    if (tarageEditor_ != null && tarageEditor_.isError()) {
      err = new StringBuilder(tarageEditor_.getErrorMessage());
    }
    for (EvolutionEditor variableEvolution : varEvolution_) {
      if (variableEvolution.isError()) {
        r = false;
        if (err == null) {
          err = new StringBuilder();
        } else {
          err.append(CtuluLibString.LINE_SEP);
        }
        err.append(TrResource.getS("Ajouter des courbes l'�diteur de courbes (menu 'Projet')"));
        break;
      }
    }
    if (err != null) {
      setErrorText(err.toString());
    }
    return r;
  }

  private void revalidateDialog() {
    invalidate();
    doLayout();
    repaint();
    JDialog fr = (JDialog) SwingUtilities.getAncestorOfClass(JDialog.class, this);
    if (fr != null) {
      fr.pack();
      fr.repaint();
    }
  }
}
