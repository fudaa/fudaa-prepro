/**
 *  @creation     11 juin 2004
 *  @modification $Date: 2007-01-19 13:14:12 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.rubar;

import java.awt.Graphics;
import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.fudaa.meshviewer.layer.MvNodeLayer;
import org.fudaa.fudaa.meshviewer.model.MvNodeModel;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarCINLayer.java,v 1.8 2007-01-19 13:14:12 deniger Exp $
 */
public class TrRubarCINLayer extends MvNodeLayer {

  /**
   * @param _modele
   */
  public TrRubarCINLayer(final MvNodeModel _modele) {
    super(_modele);
  }

  @Override
  public boolean isPaletteModifiable() {
    return false;
  }

  @Override
  public boolean isFontModifiable() {
    return false;
  }

  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrBoite _clipReel) {}

  public void paintDonnees(final Graphics _g, final int _nbPt) {}

}
