/*
 * @creation 9 ao�t 2005
 *
 * @modification $Date: 2007-05-04 14:01:52 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuTextField;
import com.memoire.fu.FuComparator;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.editor.CtuluExprTextField;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.fudaa.tr.post.TrPostSource;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * Permet l'edition des variables creees par l'utilisateur.
 *
 * @author Fred Deniger
 * @version $Id: TrPostDataEditExprPanel.java,v 1.5 2007-05-04 14:01:52 deniger Exp $
 */
public class TrPostDataEditExprPanel extends TrPostDataEditCommonPanel {
    CtuluValueEditorDouble editor_;
    CtuluExpr expr_;
    final String initExpr_;
    BuTextField tfExpr_;
    Map<String, H2dVariableType> varVar2d_;

    /**
     * @param _src la source
     * @param _var la variable a modifier: si null, phase de creation.
     */
    public TrPostDataEditExprPanel(final TrPostSource _src, final H2dVariableTypeCreated _var) {
        super(_src, _var);
        expr_ = new CtuluExpr();
        final TrPostDataCreatedExpr userExpr = (TrPostDataCreatedExpr) _src.getUserCreatedVar(_var);
        initExpr_ = userExpr == null ? null : userExpr.getFormule();

        src_.fillWithConstantVar(expr_);
        varVar2d_ = new TreeMap<>(FuComparator.STRING_IGNORE_CASE_COMPARATOR);
        for (final Iterator iter = varToAdd_.iterator(); iter.hasNext(); ) {
            final H2dVariableType v = (H2dVariableType) iter.next();
            varVar2d_.put(v.getShortName(), v);
        }
        List<H2dVariableType> varsInOrder = new ArrayList<>(varVar2d_.values());
        Collections.sort(varsInOrder, FuComparator.STRING_IGNORE_CASE_COMPARATOR);
        for (H2dVariableType var : varsInOrder) {
            expr_.addVar(var.getShortName(), var.getName());
        }
        // si la variable est deja existante
        if (initExpr_ != null) {
            expr_.getParser().parseExpression(initExpr_);
        }
        editor_ = new CtuluValueEditorDouble(expr_);
        tfExpr_ = (BuTextField) editor_.createEditorComponent();
        tfExpr_.setText(initExpr_);
        ((CtuluExprTextField) tfExpr_).setFormulaMenuAsButton(true);
        ((CtuluExprTextField) tfExpr_).setButtonLaunchEditDialog(true);
        final BuLabel lb = addLabel(CtuluLib.getS("Expression:"));
        lb.setLabelFor(tfExpr_);
        add(tfExpr_);
        updateExprState();
    }

    protected final void updateExprState() {
        final boolean valide = isExprOk();
        tfExpr_.setForeground(valide ? CtuluLibSwing.getDefaultTextFieldForegroundColor() : Color.RED);
        final JComponent c = (JComponent) tfExpr_.getClientProperty("labeledBy");
        if (c != null) {
            c.setForeground(valide ? CtuluLibSwing.getDefaultLabelForegroundColor() : Color.RED);
        }
        if (valide) {
            tfExpr_.setToolTipText(c == null ? null : c.getToolTipText());
        } else {
            tfExpr_.setToolTipText(expr_.getParser().getHtmlError());
        }
    }

    @Override
    public boolean apply() {
        return true;
    }

    public final boolean isExprOk() {
        expr_.getParser().parseExpression(tfExpr_.getText());
        return !editor_.isEmpty(tfExpr_) && editor_.isValueValidFromComponent(tfExpr_);
    }

    /**
     * @return true si des modifications ont ete faites.
     */
    @Override
    public boolean isModified() {
        if (var_ == null || initExpr_ == null) {
            return true;
        }
        return isVarModified() || (!initExpr_.equals(tfExpr_.getText()));
    }

    @Override
    public boolean addError(final StringBuffer _b) {
        if (!isExprOk()) {
            if (_b.length() > 0) {
                _b.append(CtuluLibString.LINE_SEP);
            }
            _b.append(CtuluLib.getS("L'expression est invalide"));
            return true;
        }
        return false;
    }

    @Override
    public TrPostDataCreated createData() {
        return TrPostDataHelper.createData(src_, expr_.getParser(), varVar2d_);
    }
}
