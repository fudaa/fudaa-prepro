/*
 * @creation 2 f�vr. 07
 * @modification $Date: 2007-02-07 09:56:16 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.data;

import java.util.Collection;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourcesManager;

/**
 * @author fred deniger
 * @version $Id: TrPostDataCreatedSaver.java,v 1.1 2007-02-07 09:56:16 deniger Exp $
 */
public interface TrPostDataCreatedSaver {

  /**
   * @param _newVar
   * @param _src
   * @param _ui
   * @param _shortName
   * @param srcMng
   * @param log
   * @param savedValues savedValues in the persist file
   * @return
   */
  TrPostDataCreated restore(H2dVariableType _newVar, TrPostSource _src, CtuluUI _ui, Map _shortName, TrPostSourcesManager srcMng, CtuluAnalyze log, double[] savedValues);
  
  
  /**
   * @return the collection a variables used by this data.
   */
  Collection<String> getVarNameUsed();
  
  
  double[] getValuesToPersist(TrPostDataCreated dataCreated);
}
