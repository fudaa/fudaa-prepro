/**
 * @creation 13 oct. 2004
 * @modification $Date: 2007-05-04 14:01:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.rubar.H2DRubarDiffusionListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.rubar.io.RubarDIFFileFormat;

import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFileStateDIF.java,v 1.4 2007-05-04 14:01:53 deniger Exp $
 */
public class TrRubarFileStateDIF extends TrRubarFileState implements H2DRubarDiffusionListener {
  final H2dRubarParameters params_;

  /**
   * @param _l le listener
   */
  public TrRubarFileStateDIF(final TrRubarFileStateMng _l, final H2dRubarParameters _p) {
    super(RubarDIFFileFormat.getInstance(), _l);
    params_ = _p;
    params_.addDiffusionListener(this);
    if (params_.isDiffusionCreated()) {
      _l.add(this);
    }
  }

  @Override
  protected CtuluIOOperationSynthese loadIfNeeded(final File _initFile, final ProgressionInterface _prog) {
    FuLog.warning(new Throwable());
    return null;
  }

  protected CtuluIOOperationSynthese loadIfNeeded(final File _initFile, final ProgressionInterface _prog,
                                                  final int _nbElt) {
    // le fichier est a jour : inutile
    if ((initFile_ != null) && (!initFile_.equals(_initFile))) {
      FuLog.error(
          new Throwable(initFile_.getName() + " loaded " + _initFile.getName()));
      return null;
    }
    if (initFile_ == null) {
      initFile_ = _initFile;
    }
    if (isLoadedFileUpToDate()) {
      CtuluLibMessage.info("RELOAD VF2M: " + _initFile.getName() + " uptodate");

      return null;
    }
    CtuluLibMessage.info("RELOAD VF2M: " + _initFile.getName() + " will be reloaded");
    lastModifiedTime_ = _initFile.lastModified();
    return ((RubarDIFFileFormat) fmt_).read(_initFile, _prog, _nbElt);
  }

  @Override
  public void diffusionChanged(final boolean _add) {
    final TrRubarFileStateMng l = (TrRubarFileStateMng) l_;
    final boolean isPresent = l.contains(this);
    if (_add) {
      if (params_.isDiffusionCreated() && !isPresent) {
        l.add(this);
        setMarkRemoved(false);
        setModified(true);
      }
      if (!params_.isDiffusionCreated() && isPresent) {
        setMarkRemoved(true);
      }
    } else {
      setModified(true);
    }
  }

  @Override
  public CtuluIOOperationSynthese save(final File _dir, final String _projectName,
                                       final ProgressionInterface _progression, final TrRubarProject _projet) {
    return RubarDIFFileFormat.getInstance().write(RubarDIFFileFormat.getInstance().getFileFor(_dir, _projectName),
        _projet.getH2dRubarParameters().getDiffusion(), _progression);
  }
}
