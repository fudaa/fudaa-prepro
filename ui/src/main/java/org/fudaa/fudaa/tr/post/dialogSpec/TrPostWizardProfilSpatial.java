package org.fudaa.fudaa.tr.post.dialogSpec;

import com.memoire.bu.BuButtonPanel;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuWizardTask;
import org.locationtech.jts.geom.LineString;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeDefault;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.sig.FSigAttibuteTypeManager;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.sig.layer.FSigLayerLine;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostScene;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.post.actions.TrPostActionAddPointFromWidgetCalque;
import org.fudaa.fudaa.tr.post.profile.MvProfileBuilderFromTree;
import org.fudaa.fudaa.tr.post.profile.MvProfileFillePanel;
import org.fudaa.fudaa.tr.post.profile.MvProfileTreeModel;

/**
 * classe reservee a la creation de profils spatiaux via un wizard.
 *
 * @author Adrien Hadoux
 */
public class TrPostWizardProfilSpatial extends BuWizardTask {

  TrPostActionAddPointFromWidgetCalque.Spatial actionChoixGraphes_;
  MvProfileBuilderFromTree builderParams_;
  BuCheckBox checkboxNewGraphe_ = new BuCheckBox(TrResource.TR.getString("Cr�er un nouveau graphe"));
  BuCheckBox traceOrigineDansVue2d_ = new BuCheckBox(TrResource.TR
          .getString("Cr�er calque avec points d'origine de la courbe"));
  private final TrPostVisuPanel calque_;
  TrPostSource src_;

  public TrPostWizardProfilSpatial(final TrPostVisuPanel _calque, final MvProfileBuilderFromTree _chooserParam,
          final TrPostActionAddPointFromWidgetCalque.Spatial _actionChoixGraphes) {
    actionChoixGraphes_ = _actionChoixGraphes;
    builderParams_ = _chooserParam;
    // calque_ = _calque;
    src_ = _calque.getSource();
    calque_ = _calque;
  }

  @Override
  public void cancelTask() {
    super.cancelTask();
    builderParams_.close();
  }

  /**
   * Action de creation d un nouveau widget graphe en live
   */
  protected void createNewGraphe() {

    builderParams_.selectedLine_ = builderParams_.getSelectedLine();
    builderParams_.name_ = builderParams_.getSelectedLineName();

    final CtuluTaskDelegate task = builderParams_.ui_.createTask(FudaaLib.getS("Construction des courbes"));

    task.start(new Runnable() {
      @Override
      public void run() {
        final EfLineIntersectionsResultsI res = builderParams_.getDefaultRes((CtuluVariable) builderParams_.initVar_
                .getElementAt(0), task.getStateReceiver());
        if (res == null || res.isEmpty()) {
          builderParams_.showNoIntersectionFound();
        } else {

          builderParams_.actBuildGroup(task.getStateReceiver(), builderParams_.getVarTime());
        }
      }
    });
  }

  /**
   * Retourne le groupe unique associ� a la vue 2d pour y ins�rer les calques des traces
   *
   * @return
   */
  public static FSigLayerGroup getGroup(final String ID, final TrPostVisuPanel pnVisu) {
    String TYPE_ID = "data.type";
    final FSigLayerGroup gr = pnVisu.getGroupGIS();

    // -- recherche si existe deja --//
    final BCalque[] cq = gr.getCalques();
    if (cq != null) {
      for (int i = 0; i < cq.length; i++) {
        if (ID.equals(cq[i].getClientProperty(TYPE_ID))) {
          return (FSigLayerGroup) cq[i];
        }
      }
    }

    // -- creation sinon --//
    final FSigLayerGroup res = gr.addGroupAct(ID, gr, true, null);
    res.setActions(null);
    res.setAtt(new FSigAttibuteTypeManager());
    res.putClientProperty(TYPE_ID, ID);
    res.setDestructible(true);
    return res;

  }

  /**
   * Creer une trace de l'origine du graphe sous forme dd calque
   */
  private void creerTraceCalque() {
    CtuluDialogPanel pn = new CtuluDialogPanel(false);
    pn.setLayout(new BuGridLayout(2));
    pn.addLabel(TrLib.getString("Indiquer le nom du calque � cr�er"));
    JTextField tf = nomCalque_;
    if (tf.getText() == null || tf.getText().length() == 0) {
      tf.setText("Origine profil spatial");
    }
    String titreCalque = tf.getText();

    // -- cas reels --//
    LineString listePoints = builderParams_.selectedLine_;

    GISZoneCollectionPoint collectionPoint = new GISZoneCollectionPoint();

    for (int i = 0; i < listePoints.getNumPoints(); i++) {
      collectionPoint.add(listePoints.getCoordinateN(i).x, listePoints.getCoordinateN(i).y, 0);
    }
    GISZoneCollectionLigneBrisee ligneBrisee = new GISZoneCollectionLigneBrisee();
    ligneBrisee.addCoordinateSequence(collectionPoint.getAttachedSequence(), null, null);
    ZModeleLigneBriseeDefault modeleDefault = new ZModeleLigneBriseeDefault(ligneBrisee);
    FSigLayerLine calqueZ = new FSigLayerLine(modeleDefault);
    calqueZ.setIconModel(0, new TraceIconModel(TraceIcon.CARRE_SELECTION, 3, Color.blue));
    calqueZ.setTitle(titreCalque);
    calqueZ.setDestructible(true);

    FSigLayerGroup group = getGroup(getGroupTraceName(), calque_);
    group.add(calqueZ);

  }

  public static String getGroupTraceName() {
    return TrResource.getS("Trace profils spatiaux");
  }

  /**
   * appel� a la fin du wizard
   */
  @Override
  public void doTask() {
    builderParams_.close();
    done_ = true;

    // -- creation d un nouveau graphe si coch� --//
    if (checkboxNewGraphe_.isSelected()) {
      createNewGraphe();
    }

    // -- on enregistre les calques initiaux dans le graphe
    if (traceOrigineDansVue2d_.isSelected()) {
      creerTraceCalque();
    }

    final TrPostCommonImplementation impl = getPostImpl();
    // -- ajout des courbes dans les graphes --//
    for (int i = 0; i < this.actionChoixGraphes_.listeGraphesChoisis.size(); i++) {
      final EGGraphe graphe = this.actionChoixGraphes_.listeGraphesChoisis.get(i);

      // -- preliminaire --//
      // bizarre ?
      builderParams_.selectedLine_ = builderParams_.getSelectedLine();
      builderParams_.name_ = builderParams_.getSelectedLineName();

      // -- Creation d un nouveau graphe panel sans widget--//
      final CtuluTaskDelegate task = impl.createTask(FudaaLib.getS("Construction des courbes"));
      final MvProfileFillePanel newGrapheAFusionner = builderParams_.actAddGroupForExistingModel(task
              .getStateReceiver(), builderParams_.getVarTime());
      // -- recuperation du graphe panel precedemment cree et fusion avec le
      // graphe widget selectionne
      ((MvProfileTreeModel) graphe.getModel()).mergeWithAnotherTreeModel(newGrapheAFusionner.getProfileTreeModel());

    }
    TrPostScene scene = impl.getCurrentLayoutFille().getScene();
    scene.stopEditing();
    scene.refresh();


  }

  protected TrPostCommonImplementation getPostImpl() {
    return actionChoixGraphes_.impl_;
  }

  /**
   * Recupere les variables selectionnees du panel correspondant.
   *
   * @return
   */
  protected H2dVariableType[] getSelectedVar() {
    final Object[] obj = builderParams_.getVarTime().getSelectedVar();
    final H2dVariableType[] var = new H2dVariableType[obj.length];
    System.arraycopy(obj, 0, var, 0, var.length);
    return var;
  }

  @Override
  public JComponent getStepComponent() {
    switch (current_) {
      case 0: {

        return builderParams_.getPanelTreeIsoLignes();
      }
      case 1: {
        return builderParams_.getVarTime();

      }
      case 2: {

        // -- le panel avec les jlists et combobox --//
        final JPanel content = new JPanel(new BorderLayout());
        content.add(actionChoixGraphes_.getInterfacePartielle(false, false, false), BorderLayout.CENTER);
        final JPanel bouttonCenter = new JPanel(new FlowLayout(FlowLayout.CENTER));
        bouttonCenter.add(checkboxNewGraphe_);
        content.add(bouttonCenter, BorderLayout.NORTH);

        content.setBorder(CtuluLibSwing.createTitleBorder(TrLib.getString("Etape 3: S�lection et cr�ation des graphes")));

        if (actionChoixGraphes_.listeGraphesPossibles.size() == 0 && actionChoixGraphes_.listeGraphesChoisis.size() == 0) {
          checkboxNewGraphe_.setSelected(true);
          checkboxNewGraphe_.setEnabled(false);
          actionChoixGraphes_.setEnabled(false);
        }

        traceOrigineDansVue2d_.setSelected(false);
        nomCalque_.setText("Origine profil spatial");
        final JPanel panelControle = new JPanel(new FlowLayout(FlowLayout.CENTER));
        panelControle.add(traceOrigineDansVue2d_);
        panelControle.add(nomCalque_);
        content.add(panelControle, BorderLayout.SOUTH);
        return content;
      }
    }

    return null;
  }
  BuTextField nomCalque_ = new BuTextField(10);

  @Override
  public int getStepCount() {
    return 3;
  }

  @Override
  public void nextStep() {
    if (current_ == 0 && builderParams_.getSelectedLine() == null) {
      CtuluLibDialog.showError(builderParams_.getPanelTreeIsoLignes(), TrLib.getString("Pas de ligne s�lectionn�e"),
              TrLib.getString("Une ligne doit �tre cr��e ou s�lectionn�e afin de construire le profil"));
      return;
    }
    super.nextStep();
  }

  /**
   * MEthode utilisee pour disabler ou non les boutons suivants
   */
  @Override
  public int getStepDisabledButtons() {
    int r = super.getStepDisabledButtons();
//    if (current_ == 0) {
//      if (builderParams_.getSelectedLine() == null) r |= BuButtonPanel.SUIVANT;
//    } else
    if (current_ == 2) {
      if ((actionChoixGraphes_.listeGraphesChoisis == null || actionChoixGraphes_.listeGraphesChoisis.size() == 0)
              && !checkboxNewGraphe_.isSelected()) {
        r |= BuButtonPanel.TERMINER;
      }

    }
    return r;
  }

  // private static int cptGraphe = 1;
  @Override
  public String getStepText() {
    String r = null;

    switch (current_) {
      case 0:
        r = TrResource.getS("Choisir les points");
        break;
      case 1:
        r = TrResource.getS("Veuillez s�lectionner le(s) variable(s)");
        break;
      case 2:
        r = TrResource.getS("Veuillez s�lectionner les graphes pour la cr�ation des courbes ou cr�er un nouveau graphe");
        break;
    }

    return r;
  }

  @Override
  public String getStepTitle() {
    String r = null;

    switch (current_) {
      case 0:
        r = TrResource.getS("Choisir les isolignes");
        break;
      case 1:
        r = TrResource.getS("S�lectionner les variables");
        break;
      case 2:
        r = TrResource.getS("S�lectionner les graphes");
        break;
    }
    return r;
  }

  @Override
  public String getTaskTitle() {
    return MvResource.getS("Profils spatiaux");
  }
}
