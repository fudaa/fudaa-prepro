/**
 * @creation 30 nov. 2004
 * @modification $Date: 2006-07-27 13:35:36 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import javax.swing.JTextField;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSourceMng;
import org.fudaa.fudaa.fdico.FDicoEditorInterface;
import org.fudaa.fudaa.fdico.FDicoTableEditorChooser;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacSourceDefaultValues.java,v 1.10 2006-07-27 13:35:36 deniger Exp $
 */
public class TrTelemacSourceDefaultValues extends CtuluDialogPanel implements ItemListener {

  BuCheckBox cbActiveVx_;
  FDicoEditorInterface tfVx_;
  BuCheckBox cbActiveVy_;
  FDicoEditorInterface tfVy_;
  FDicoEditorInterface tfDebit_;
  FDicoEditorInterface[] edits_;
  H2dTelemacSourceMng mng_;
  CtuluCommandContainer cmd_;
  List vectEnt_;

  /**
   * @param _mng le manager des siphons
   */
  public TrTelemacSourceDefaultValues(final H2dTelemacSourceMng _mng, final CtuluCommandContainer _cmd) {
    mng_ = _mng;
    cmd_ = _cmd;
    setLayout(new BuGridLayout(3, 5, 5));
    add(new BuLabel(TrResource.getS("Variable")));
    add(new BuLabel(TrResource.getS("Activ�")));
    add(new BuLabel(TrResource.getS("Valeur par d�faut")));
    vectEnt_ = mng_.getKeywordForSource();
    final int nb = vectEnt_.size();
    edits_ = new FDicoEditorInterface[nb - 1];//to avoid tracer
    // active_ = new BuCheckBox[nb];
    final int nbDefinit = mng_.getNbSource();
    final FDicoTableEditorChooser.EditorCreator creat = new FDicoTableEditorChooser.EditorCreator();
    for (int i = 0; i < nb; i++) {
      final DicoEntite.Vecteur v = (DicoEntite.Vecteur) vectEnt_.get(i);
      if (mng_.isTraceur(v)) {
        continue;
      }
      if (!mng_.isCoordonnesEnt(v)) {
        BuCheckBox cb = new BuCheckBox();
        if (nbDefinit > 0) {
          cb.setSelected(mng_.isValueSet(v));
        } else {
          cb.setSelected(mng_.isSetByDefault(i) || mng_.isValueRequired(v));
        }
        if (mng_.isValueRequired(v)) {
          cb.setEnabled(false);
        }
        if (mng_.isTraceur(v)) {

        } else {
          add(new BuLabel(v.getNom()));
          edits_[i] = creat.createEditor(v.getType());
          ((JTextField) edits_[i].getComponent()).setEditable(cb.isSelected());
          edits_[i].setValue(mng_.getDefaultValue(v));
        }
        if (mng_.isVx(v)) {
          cbActiveVx_ = cb;
          cb.addItemListener(this);
          tfVx_ = edits_[i];
        } else if (mng_.isVy(v)) {
          cbActiveVy_ = cb;
          cb.addItemListener(this);
          tfVy_ = edits_[i];
        } else if (mng_.isDebit(v)) {
          tfDebit_ = edits_[i];
        }
        add(cb);
        add(edits_[i].getComponent());
      }
    }
    int nbTracers = mng_.getNbTraceurs();
    for (int tracerIdx = 0; tracerIdx < nbTracers; tracerIdx++) {

    }
  }

  @Override
  public boolean isDataValid() {
    boolean r = true;
    for (int i = edits_.length - 1; i >= 0; i--) {
      if (edits_[i] != null) {
        final String val = edits_[i].getValue().trim();
        if ((((JTextField) edits_[i].getComponent()).isEditable()) && !((DicoEntite) vectEnt_.get(i)).getType().isValide(val)) {
          r = false;
          edits_[i].getComponent().setForeground(Color.RED);
        } else {
          edits_[i].getComponent().setForeground(Color.BLACK);
        }
      }
    }
    if (!r) {
      Toolkit.getDefaultToolkit().beep();
    }
    return r;
  }

  @Override
  public boolean apply() {
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    mng_.setDebitDefaultValue(tfDebit_.getValue(), cmp);
    mng_.setSourceDefined(cbActiveVx_.isSelected(), tfVx_.getValue(), tfVy_.getValue(), cmp);
    cmd_.addCmd(cmp.getSimplify());
    return true;
  }

  /**
   * Permet de mettre a jour les textfield en fonction de l'etat du combobox.
   */
  @Override
  public void itemStateChanged(final ItemEvent _e) {
    final Object source = _e.getSource();
    if (source == cbActiveVx_) {
      ((JTextField) tfVx_.getComponent()).setEditable(cbActiveVx_.isSelected());
      cbActiveVy_.setSelected(cbActiveVx_.isSelected());
    } else if (source == cbActiveVy_) {
      ((JTextField) tfVy_.getComponent()).setEditable(cbActiveVy_.isSelected());
      cbActiveVx_.setSelected(cbActiveVy_.isSelected());
    }
  }

}
