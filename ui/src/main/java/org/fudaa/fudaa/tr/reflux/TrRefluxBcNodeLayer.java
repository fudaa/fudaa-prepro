/*
 *  @creation     20 janv. 2006
 *  @modification $Date: 2007-04-16 16:35:34 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.bu.BuTable;
import java.util.Set;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBcManager;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBoundaryCondition;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.fudaa.meshviewer.model.MvExpressionSupplierFrNode;
import org.fudaa.fudaa.meshviewer.model.MvFrontierModel;
import org.fudaa.fudaa.meshviewer.model.MvFrontierModelDefault;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id: TrRefluxBcNodeLayer.java,v 1.8 2007-04-16 16:35:34 deniger Exp $
 */
public class TrRefluxBcNodeLayer extends TrBcNodeLayer {
  H2dRefluxBcManager mng_;

  protected class RefluxBcExpression extends MvExpressionSupplierFrNode {

    Variable normale_;

    public RefluxBcExpression() {
      super((MvFrontierModel) modeleDonnees());
    }

    @Override
    public void initialiseExpr(final CtuluExpr _expr) {
      super.initialiseExpr(_expr);
      normale_ = _expr.addVar("normale", H2dResource.getS("normale"));
    }

    @Override
    protected void majVariable(final int _idx, final Variable[] _varToUpdate, final int[] _frIdx) {
      super.majVariable(_idx, _varToUpdate, _frIdx);
      final H2dRefluxBcManager.RefluxMiddleFrontier fr = mng_.getRefluxMiddleFrontier(_frIdx[0]);
      normale_.setValue(CtuluLib.getDouble(fr.getRefluxBc(_frIdx[1]).getNormale()));
    }
  }

  protected class RefluxBcTableModel extends MvFrontierModelDefault.BcTableModel {

    final int parentColCount_;

    final H2dVariableType[] allVar_;

    RefluxBcTableModel() {
      super((MvFrontierModel) modeleDonnees());
      parentColCount_ = super.getColumnCount();
      allVar_ = H2dRefluxBcManager.getClNodaleVariables();
    }

    @Override
    public int getColumnCount() {
      return parentColCount_ + 2 + allVar_.length;
    }

    public int getVarIdx(final int _col) {
      return _col - parentColCount_ - 2;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == parentColCount_) {
        return H2dResource.getS("Normale");
      } else if (_column == parentColCount_ + 1) {
        return H2dResource.getS("Type");
      } else if (_column >= parentColCount_ + 2) { return allVar_[getVarIdx(_column)].getName(); }
      return super.getColumnName(_column);
    }

    @Override
    public Class getColumnClass(final int _column) {
      if (_column == parentColCount_) {
        return Double.class;
      } else if (_column >= parentColCount_ + 1) { return String.class; }
      return super.getColumnClass(_column);
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      Object r = super.getValueAt(_rowIndex, _columnIndex);
      final H2dRefluxBcManager.RefluxMiddleFrontier fr = mng_.getRefluxMiddleFrontier(tmp_[0]);
      if (_columnIndex == parentColCount_) {
        r = CtuluLib.getDouble(((H2dRefluxBoundaryCondition) fr.getCl(tmp_[1])).getNormale());
      } else if (_columnIndex == parentColCount_ + 1) {
        r = fr.getSurroundingBoundaryType(tmp_[1]);
        if (r != null) {
          r = r.toString();
        }
      } else if (_columnIndex >= parentColCount_ + 2) {
        final int varIdx = getVarIdx(_columnIndex);
        final int idxOnFr = tmp_[1];
        final H2dRefluxBoundaryCondition bc = fr.getRefluxBc(idxOnFr);
        final Set s = fr.getAvailablesVariables(idxOnFr);
        if (s.contains(allVar_[varIdx])) {
          final H2dVariableType v = allVar_[varIdx];
          final H2dBcType bcType = bc.getType(v);
          if (bcType == H2dBcType.LIBRE) {
            r = H2dBcType.LIBRE.getName();
          } else if (bcType == H2dBcType.PERMANENT) {
            r = H2dBcType.PERMANENT.getName() + ": " + bc.getValue(v);
          } else {
            r = bcType.getName() + ": " + bc.getEvolution(v);
          }
        } else {
          r = CtuluLibString.EMPTY_STRING;
        }
      }
      return r;
    }

  }

  /**
   * @param _cl
   */
  public TrRefluxBcNodeLayer(final MvFrontierModel _cl, final H2dRefluxBcManager _mng) {
    super(_cl, new TrRefluxNormaleModel(_mng));
    mng_ = _mng;
  }

  @Override
  public BuTable createValuesTable() {
    return new CtuluTable(new RefluxBcTableModel());
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return new RefluxBcExpression();
  }

}
