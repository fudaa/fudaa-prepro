package org.fudaa.fudaa.tr.post.data;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostSourcesManager;

public class TrPostDataCreatedImportSaver implements TrPostDataCreatedSaver {

  String sourceId;

  /**
   * @param sourceId
   * @param shortName
   */
  public TrPostDataCreatedImportSaver(String sourceId, String shortName) {
    super();
    this.sourceId = sourceId;
    shortName_ = shortName;
  }

  String shortName_;
  
  @Override
  public Collection<String> getVarNameUsed() {
    return shortName_ == null ? Collections.<String>emptyList() : Arrays.asList(shortName_);
  }
  
  @Override
  public double[] getValuesToPersist(TrPostDataCreated dataCreated) {
    return null;
  }

  @Override
  public TrPostDataCreated restore(H2dVariableType newVar, TrPostSource src, CtuluUI ui, Map shortName,
      TrPostSourcesManager srcMng, CtuluAnalyze log, double[] savedValues) {
    TrPostSource source = srcMng.getSource(sourceId);
    if (source == null) {
      log.addError(TrResource.getS("La source {0} n'a pas �t� charg�e", sourceId));
      return null;
    }
    H2dVariableType var = getVariableFor(shortName_, source);
    return var == null ? null : new TrPostDataCreatedImport(source, var);
  }

  private H2dVariableType getVariableFor(String shortName, TrPostSource source) {
    H2dVariableType[] availableVar = source.getAvailableVar();
    for (int i = 0; i < availableVar.length; i++) {
      if (shortName.equals(availableVar[i].getShortName())) return availableVar[i];
    }
    return null;
  }
}
