/*
 * @creation 28 mai 2004
 * @modification $Date: 2007-06-05 09:01:14 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.*;
import org.fudaa.ctulu.gui.*;
import org.fudaa.dodico.telemac.TelemacVersionManager;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.tr.TrChainePreferencePanel;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacExecPreferencesPanel.java,v 1.27 2007-06-05 09:01:14 deniger Exp $
 */
public class TrTelemacExecPreferencesPanel extends BuAbstractPreferencesPanel implements ItemListener {
  static class CbCellRenderer extends CtuluCellTextRenderer {
    @Override
    protected void setValue(final Object _value) {
      if (_value instanceof VersionItem) {
        super.setValue(((VersionItem) _value).getVersion());
      } else {
        super.setValue(_value);
      }
    }
  }

  private class ItemVersionEditor extends CtuluDialogPanel {
    JTextField ftBin_;
    JTextField ftVersionName_;
    String initVersion_;

    ItemVersionEditor() {
      this(null);
    }

    ItemVersionEditor(final VersionItem _it) {

      setLayout(new BuGridLayout(2, 5, 5));
      ftVersionName_ = addLabelStringText(TrResource.getS("Version:"));
      final String lb = TrResource.getS("R�pertoire des ex�cutables de T�l�mac:");
      addLabel(lb);
      final CtuluFileChooserPanel pn = new CtuluFileChooserPanel(_it == null ? null : new File(_it.getPath()),
          TrResource.getS("R�pertoire des ex�cutables de Telemac"));
      pn.setFileSelectMode(JFileChooser.FILES_AND_DIRECTORIES);
      ftBin_ = pn.getTf();
      add(pn);
      addLabel(FudaaLib.getS("Aide:"));
      addLabel("<html><p><b>"
          + TrResource.getS("Le r�pertoire s�lectionn� doit contenir les ex�cutables telemac.<br>Utiliser le bouton '...' pour choisir le dossier") + "</b></p></html>");
      if (_it != null) {
        initVersion_ = _it.getVersion();
        ftVersionName_.setText(_it.getVersion());
        ftBin_.setText(_it.getPath());
      }
    }

    public VersionItem getVersionItem() {
      File f = new File(ftBin_.getText().trim());
      if (!f.isDirectory()) {
        f = f.getParentFile();
      }
      return new VersionItem(ftVersionName_.getText().trim(), f.getAbsolutePath());
    }

    @Override
    public boolean isDataValid() {

      String v = ftVersionName_.getText().trim();
      if (v.length() == 0) {
        setErrorText(FudaaLib.getS("Remplir tous les champs"));
        return false;
      }
      v = TelemacVersionManager.cleanVersion(v);
      if ((initVersion_ == null || !initVersion_.equals(v)) && (model_.versions_ != null)
          && (model_.versions_.contains(new VersionItem(v, null)))) {
        setErrorText(TrResource.getS("Version d�j� d�finie"));
        return false;
      }
      boolean r = true;
      final String s = ftBin_.getText().trim();
      if (s.length() == 0) {
        r = false;
        setErrorText(TrResource.getS("Remplir tous les champs"));
      } else {
        File f = new File(s);
        if (!f.isDirectory()) {
          f = f.getParentFile();
        }
        if (!f.exists()) {
          setErrorText(TrResource.getS("Le dossier n'existe pas"));
          r = false;
        } else if ((!new File(f, "scripts" + File.separator + "python27" + File.separator + "telemac2d.py").exists())) {
          setErrorText(TrResource.getS("Ce n'est pas le dossier des ex�cutable telemac"));
          r = false;
        }
      }
      return r;
    }
  }

  private static class VersionItem implements Comparable {
    private final String path_;
    private final String version_;

    /**
     * @param _version
     * @param _path
     */
    public VersionItem(final String _version, final String _path) {
      super();
      version_ = _version;
      path_ = _path;
    }

    @Override
    public int compareTo(final Object _o) {
      return version_.compareTo(((VersionItem) _o).version_);
    }

    @Override
    public boolean equals(final Object _obj) {
      if (_obj == this) {
        return true;
      }
      if (_obj == null) {
        return false;
      }
      if (_obj.getClass().equals(getClass())) {
        return version_.equals(((VersionItem) _obj).version_);
      }
      return false;
    }

    public final String getPath() {
      return path_;
    }

    public final String getVersion() {
      return version_;
    }

    @Override
    public int hashCode() {
      return version_.hashCode();
    }

    @Override
    public String toString() {
      return version_ + ": " + path_;
    }
  }

  private class VersionModel extends CtuluListEditorModel {
    Set versions_;

    /**
     * @param _o
     */
    public VersionModel(final VersionItem[] _o) {
      super(_o, true);
      if (_o != null && _o.length > 0) {
        versions_ = new HashSet();
        versions_.addAll(v_);
      }
    }

    @Override
    public void addElement(final Object _o) {
      v_.add(_o);
      Collections.sort(v_);
      super.fireTableDataChanged();
      if (versions_ == null) {
        versions_ = new HashSet();
        versions_.add(_o);
      }
      setDirty(true);
      updateCb();
    }

    @Override
    public boolean canAdd() {
      return true;
    }

    @Override
    public Object createNewObject() {
      final ItemVersionEditor d = new ItemVersionEditor();
      if (CtuluDialogPanel.isOkResponse(d.afficheModale(TrTelemacExecPreferencesPanel.this, TrResource
          .getS("D�finir une nouvelle version de T�l�mac")))) {

        return d.getVersionItem();
      }
      return null;
    }

    @Override
    public void remove(final ListSelectionModel _m) {
      super.remove(_m);
      setDirty(true);
      updateCbAfterRemove();
    }

    @Override
    public void setData(final Object[] _o) {
      super.setData(_o);
      Collections.sort(v_);
      if (_o != null && _o.length > 0) {
        versions_ = new HashSet(v_);
      } else {
        versions_ = null;
      }
      updateCb();
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      versions_.remove(v_.set(_rowIndex, _value));
      versions_.add(_value);
      Collections.sort(v_);
      updateCb();
      setDirty(true);
      super.fireTableDataChanged();
    }
  }

  String default_ = TrResource.getS("Demander");
  VersionModel model_;

  /**
   *
   */
  public TrTelemacExecPreferencesPanel() {
    // appli_ = appli;
    setLayout(new BuVerticalLayout(10, true, true));
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    initAll();
    final CtuluListEditorPanel editor = new CtuluListEditorPanel(model_, true, true, false, true, false);
    BuLabel lb = new BuLabel();
    lb
        .setText("<html><p><b><u>"
            + TrResource
            .getS("Pour utilisateur avanc� uniquement. Par d�faut, vous n'avez pas � modifier les propri�t�s de ce panneau.")
            + "</u></b></p></html>");
    add(lb);
    lb = new BuLabel();
    lb.setText("<html><p><b>"
        + TrResource.getS("Si le r�pertoire des binaires de T�l�mac n'est pas dans votre PATH "
        + "(propri�t� syst�me),vous devez le pr�ciser dans le tableau ci-dessous."
        + "<br>Vous pouvez d�finir plusieurs versions.") + "</b></p></html>");
    add(lb);
    final BuScrollPane sp = new BuScrollPane(editor);
    editor.getTable().setPreferredSize(new Dimension(90, 100));
    editor.setPreferredSize(new Dimension(90, 100));
    if (model_.getRowCount() == 0) {
      sp.setPreferredHeight(100);
    }
    sp.setPreferredWidth(70);
    add(sp);
    final BuPanel pn = new BuPanel();
    pn.setLayout(new BuGridLayout(2, 5, 5));
    add(pn);
    editor.setValueListCellEditor(new CtuluCellButtonEditor(null) {
      @Override
      protected void doAction() {
        if (value_ != null) {
          final VersionItem it = (VersionItem) value_;
          final ItemVersionEditor ed = new ItemVersionEditor(it);
          if (CtuluDialogPanel.isOkResponse(ed.afficheModale(TrTelemacExecPreferencesPanel.this))) {
            value_ = ed.getVersionItem();
          }
        }
      }
    });
    setDirty(false);
    pn.doLayout();
  }

  private void initAll() {
    final TelemacVersionManager mng = TelemacVersionManager.INSTANCE;
    final VersionItem[] it = new VersionItem[mng.getNbVersion()];
    for (int i = it.length - 1; i >= 0; i--) {
      it[i] = new VersionItem(mng.getVersion(i), mng.getVersionPath(i));
    }
    if (model_ == null) {
      model_ = new VersionModel(it);
    } else {
      model_.setData(it);
    }
    CbCellRenderer cellRender = null;
  }

  protected void updateCb() {

  }

  protected void updateCbAfterRemove() {
    updateCb();
  }

  @Override
  public void applyPreferences() {
    final TelemacVersionManager mng = TelemacVersionManager.INSTANCE;
    final Map r = new HashMap();
    final Object[] o = model_.getValues();
    if (o != null) {
      for (int i = o.length - 1; i >= 0; i--) {
        final VersionItem it = (VersionItem) o[i];
        r.put(it.getVersion(), it.getPath());
      }
    }
    String matisse = null;
    mng.setVersion(r);
    mng.saveVersions(true);
    setDirty(false);
  }

  @Override
  public void cancelPreferences() {
    TelemacVersionManager.INSTANCE.loadProperties();
    initAll();
    setDirty(false);
  }

  @Override
  public String getCategory() {
    return TrChainePreferencePanel.getModelisationCategory();
  }

  @Override
  public String getTitle() {
    return TrResource.getS("T�l�mac");
  }

  @Override
  public boolean isPreferencesApplyable() {
    return false;
  }

  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }

  @Override
  public boolean isPreferencesValidable() {
    return true;
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    setDirty(true);
  }

  @Override
  public void validatePreferences() {
    applyPreferences();
  }
}
