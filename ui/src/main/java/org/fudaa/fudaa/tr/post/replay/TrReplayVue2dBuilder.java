/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.replay;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;

/**
 *
 * @author Frederic Deniger
 */
public class TrReplayVue2dBuilder {

  public static TraceIconModel createTraceIconModel() {
    return new TraceIconModel(TraceIcon.CARRE_SELECTION, 4, Color.RED);
  }

  public static TrPostVisuPanel constructVue2d(final TrPostCommonImplementation implementation, final TrPostSource src, final TrReplayVisuCustomizer customizer) {
    return constructVue2d(implementation, src, customizer, null);
  }

  public static TrPostVisuPanel constructVue2d(final TrPostCommonImplementation implementation, final TrPostSource src, final TrReplayVisuCustomizer customizer, final BCalqueLegende legende) {
    final TrPostVisuPanel vue2d = new TrPostVisuPanel(implementation, implementation.getCurrentProject(), legende, src);
    final Dimension rec = new Dimension(300, 300);
    vue2d.setPreferredSize(rec);
    customizer.customizeVisu(vue2d);
    final BCalque[] tousCalques = vue2d.getVueCalque().getCalque().getTousCalques();
    vue2d.getVueCalque().setSize(rec.width, rec.height);
    vue2d.getVueCalque().getCalque().setSize(rec.width, rec.height);
    for (int i = 0; i < tousCalques.length; i++) {
      tousCalques[i].setSize(rec.width, rec.height);
    }
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        vue2d.restaurer();
      }
    });
    return vue2d;
  }
}
