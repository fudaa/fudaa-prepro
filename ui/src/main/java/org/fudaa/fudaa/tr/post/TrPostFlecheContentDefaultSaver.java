/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.fudaa.tr.post;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreated;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreatedSaver;

/**
 * Classe de sauvegarde pour les fleches
 * 
 * @author deniger
 */
public class TrPostFlecheContentDefaultSaver implements TrPostDataCreatedSaver {

  String vx_;
  String vy_;
  String name_;

  public TrPostFlecheContentDefaultSaver(TrPostFlecheContentDefaut _defaut) {
    name_ = _defaut.getVar().getShortName();
    vx_ = _defaut.getVx().getShortName();
    vy_ = _defaut.getVy().getShortName();
  }
  
  
  @Override
  public Collection<String> getVarNameUsed() {
    return Arrays.asList(vx_,vy_);
  }
  
  @Override
  public double[] getValuesToPersist(TrPostDataCreated dataCreated) {
    return null;
  }

  @Override
  public TrPostDataCreated restore(H2dVariableType _newVar, TrPostSource _src, CtuluUI _ui, Map _shortName, TrPostSourcesManager srcMng, CtuluAnalyze log, double[] savedValues) {
    H2dVariableType var = _newVar;
    H2dVariableType varX = (H2dVariableType) _shortName.get(vx_);
    H2dVariableType varY = (H2dVariableType) _shortName.get(vy_);
    if (var != null && varX != null && varY != null) { return new TrPostFlecheContentDefaut(_src, var, varX, varY,true); }
    return null;
  }
}
