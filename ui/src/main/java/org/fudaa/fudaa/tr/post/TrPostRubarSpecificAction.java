/**
 *  @creation     19 janv. 2005
 *  @modification $Date: 2007-02-07 09:56:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuCheckBoxMenuItem;
import com.memoire.bu.BuMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrPostProjetRubar.java,v 1.14 2007-02-07 09:56:17 deniger Exp $
 */
public class TrPostRubarSpecificAction implements ActionListener {

  final TrPostSourceRubar s_;
  final TrPostCommonImplementation impl_;

  public TrPostRubarSpecificAction(final TrPostSourceRubar _s, final TrPostCommonImplementation _impl) {
    super();
    s_ = _s;
    impl_ = _impl;
  }

  protected void addLimniFrame(final TrPostSource _src, final int[] _ptIdx) {
    addLimniFrame(impl_, s_, _ptIdx);
  }

  public static void addLimniFrame(final TrPostCommonImplementation _impl, final TrPostSource _src, final int[] _ptIdx) {
    final TrPostRubarLimniMng limni = ((TrPostSourceRubar) _src).getLimni();
    if (limni != null) {
      new CtuluTaskOperationGUI(_impl, H2dResource.getS("Limnigrammes")) {

        @Override
        public void act() {
          limni.addGraphe(_ptIdx, _impl.createProgressionInterface(this), _impl, _src);
        }
      }.start();
    }

  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if ("LOAD_FILE".equals(_e.getActionCommand())) {
      new TrPostRubarLoader(s_, null, false).active(impl_.getCurrentProject(),impl_, null);
    } else if (mnEnv_ == _e.getSource()) {
      updateMenuEnvTime();
    }

  }

  protected void updateMenuEnvTime() {
    if (mnEnv_ == null) { return; }
    final TrPostSourceRubar srcRubar = s_;
    mnEnv_.setEnabled(srcRubar.isEnvTimeStepAvailable());
    mnEnv_.setSelected(srcRubar.isEnvTimeStepActivated());
  }

  BuCheckBoxMenuItem mnEnv_;

  // File getPostDbFile() {
  // return new File(CtuluLibFile.getSansExtension(s_.getFile().getAbsolutePath()) + ".post.fdb");
  // }

  protected void addSpecificItemInMainMenu(final BuMenu _m) {
    _m.addSeparator();
    _m.addMenuItem(TrResource.getS("Charger des fichiers de résultats"), "LOAD_FILE", this);
    mnEnv_ = _m.addCheckBox(TrResource.getS("Utiliser les pas de temps du fichier env"), "ENV_TIMESTEPS", false, false);
    mnEnv_.addActionListener(this);
    updateMenuEnvTime();
  }

}