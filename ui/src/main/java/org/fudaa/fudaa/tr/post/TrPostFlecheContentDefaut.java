/*
 *  @creation     3 mai 2005
 *  @modification $Date: 2007-06-05 09:01:13 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreatedSaver;
import org.fudaa.fudaa.tr.post.data.TrPostDataVecteur;
import org.nfunk.jep.Variable;

import java.io.IOException;
import java.util.Set;

public class TrPostFlecheContentDefaut implements TrPostFlecheContent {
    final H2dVariableType name_;
    final TrPostSource src_;
    final H2dVariableType varVx_;
    final H2dVariableType varVy_;
    final boolean useSaver_;

    /**
     * @param _name le nom de la variable
     * @param _idxX la valeur pour x
     * @param _idxY la valeur pour y
     */
    public TrPostFlecheContentDefaut(final TrPostSource _src, final H2dVariableType _name, final H2dVariableType _idxX,
                                     final H2dVariableType _idxY) {
        this(_src, _name, _idxX, _idxY, false);
    }

    /**
     * @param _name     le nom de la variable
     * @param _idxX     la valeur pour x
     * @param _idxY     la valeur pour y
     * @param _useSaver true si ce vecteur doit fournir une classe de sauvegarde cf {@link #createSaver()}
     */
    public TrPostFlecheContentDefaut(final TrPostSource _src, final H2dVariableType _name, final H2dVariableType _idxX,
                                     final H2dVariableType _idxY, boolean _useSaver) {
        src_ = _src;
        name_ = _name;
        varVx_ = _idxX;
        varVy_ = _idxY;
        useSaver_ = _useSaver;
    }

    @Override
    public void restore() {
    }

    @Override
    public TrPostFlecheContent changeVar(final H2dVariableType _newName) {
        return new TrPostFlecheContentDefaut(src_, _newName, varVx_, varVy_, useSaver_);
    }

    @Override
    public void clearCache() {
    }

    @Override
    public TrPostDataCreatedSaver createSaver() {

        return useSaver_ ? new TrPostFlecheContentDefaultSaver(this) : null;
    }

    @Override
    public TrPostFlecheContent duplicate(final TrPostSource _src) {
        return new TrPostFlecheContentDefaut(_src, name_, varVx_, varVy_, useSaver_);
    }

    @Override
    public TrPostFlecheContent duplicate(final TrPostSource _src, final H2dVariableType _newName) {
        return new TrPostFlecheContentDefaut(_src, _newName, varVx_, varVy_, useSaver_);
    }

    @Override
    public void fillWhithAllUsedVar(final Set _res) {
        _res.add(varVx_);
        _res.add(varVy_);
    }

    @Override
    public EfData getDataFor(final int _idxTime) {
        return getValues(_idxTime);
    }

    @Override
    public String getDescription() {
        return toString();
    }

    @Override
    public double getValue(final int _idxTime, final int _idxObject) {
        try {
            final double vx = src_.getData(varVx_, _idxTime, _idxObject);
            final double vy = src_.getData(varVy_, _idxTime, _idxObject);
            return Math.hypot(vx, vy);
        } catch (final IOException _e) {
            FuLog.warning(_e);
        }
        return 0;
    }

    @Override
    public TrPostDataVecteur getValues(final int _tIdx) {
        return new TrPostDataVecteur(src_.getData(varVx_, _tIdx), src_.getData(varVy_, _tIdx), src_.getGrid());
    }

    @Override
    public H2dVariableType getVar() {
        return name_;
    }

    @Override
    public String getVxDesc() {
        return varVx_.getName();
    }

    public H2dVariableType getVxVar() {
        return varVx_;
    }

    @Override
    public String getVyDesc() {
        return varVy_.getName();
    }

    public H2dVariableType getVyVar() {
        return varVy_;
    }

    @Override
    public boolean isEditable() {
        return useSaver_;
    }

    @Override
    public TrPostFlecheContent isFleche() {
        return this;
    }

    @Override
    public String toString() {
        return name_.getName();
    }

    @Override
    public boolean isExpr() {
        return false;
    }

    @Override
    public boolean updateConstantVar(final Variable _var) {
        return false;
    }

    @Override
    public H2dVariableType getVx() {
        return varVx_;
    }

    @Override
    public H2dVariableType getVy() {
        return varVy_;
    }
}
