/**
 *  @creation     10 juin 2004
 *  @modification $Date: 2007-01-19 13:14:08 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import java.util.List;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;

/**
 * @author Fred Deniger
 * @version $Id: TrBcBoundaryLayer.java,v 1.10 2007-01-19 13:14:08 deniger Exp $
 */
public interface TrBcBoundaryLayer extends MvLayerGrid {

  TrBordTraceLigneData getBordTrace();

  /**
   * @param _t le type de bord a considerer
   * @return le type de ligne a utiliser pour le type de bord
   */
  TraceLigneModel getTlData(H2dBoundaryType _t);

  /**
   * @return la liste des bords utilises par le modele
   */
  List getUsedBoundaryType();

  List getAllBoundaryType();

  /**
   * @return le titre
   */
  String getTitle();

  void clearSelection();

  // void setContextuelDelegator(BCalqueContextuelListener _d);

  void boundaryTypeRendererChanged();

}
