/**
 * @creation 10 nov. 2004 @modification $Date: 2007-04-30 14:22:39 $ @license GNU General Public License 2 @copyright (c)1998-2001
 *     CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuLib;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluNumberFormater;
import org.fudaa.ctulu.CtuluUIAbstract;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.dodico.h2d.H2dParameters;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryFlowrateGroupType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeModel;
import org.fudaa.fudaa.tr.common.TrCourbeTemporelleManager;
import org.fudaa.fudaa.tr.common.TrResource;

import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarEvolutionManager.java,v 1.17 2007-04-30 14:22:39 deniger Exp $
 */
public class TrRubarEvolutionManager extends TrCourbeTemporelleManager implements H2dRubarApportListener, H2dRubarVentListener, H2dRubarBcListener {
  transient H2dRubarParameters rubarParam_;

  /**
   * @param _p
   */
  public TrRubarEvolutionManager(final H2dParameters _p) {
    super(_p, true);
    rubarParam_ = (H2dRubarParameters) _p;
    rubarParam_.getBcMng().addListener(this);
    rubarParam_.getAppMng().addAppListener(this);
    rubarParam_.getVentMng().addVentListener(this);
  }

  public H2dRubarProjetType getProjectType() {
    return rubarParam_.getProjetType();
  }

  public H2dRubarParameters getRubarParam() {
    return rubarParam_;
  }

  @Override
  public void areteTypeChanged() {
  }

  @Override
  public void nodeInGridChanged() {
  }

  @Override
  public void bathyChanged() {
  }

  @Override
  public void fondDurChanged() {
  }

  @Override
  public void flowrateGroupChanged(final H2dRubarBoundaryFlowrateGroupType _t) {
  }

  @Override
  public void numberOfConcentrationChanged() {

  }

  @Override
  public void projectTypeChanged() {
    final Set<H2dVariableType> transVariables = new HashSet<H2dVariableType>();
    transVariables.add(H2dVariableTransType.CONCENTRATION);
    transVariables.add(H2dVariableTransType.DIAMETRE);
    transVariables.add(H2dVariableTransType.ETENDUE);
    if (rubarParam_.getProjetType() == H2dRubarProjetType.TRANSPORT) {
      for (H2dVariableType h2dVariableType : transVariables) {
        final EGGroup g = super.getOrCreateGroupFor(h2dVariableType);
        add(g);
      }

      final Map cs = rubarParam_.getUsedEvolutionVariables();
      for (final Iterator it = cs.entrySet().iterator(); it.hasNext(); ) {
        if (!transVariables.contains(((Map.Entry) it.next()).getValue())) {
          it.remove();
        }
      }
      setValues(cs);
      sortGroups();
    } else {
      for (H2dVariableType t : transVariables) {
        final EGGroup g = super.getGroupeFor(t);
        if (g != null) {
          super.removeGroup(t, g);
        }
      }
    }
    fireGlobalChanged();
  }

  @Override
  public void timeClChanged() {
  }

  /**
   * Ne fait rien.
   */
  @Override
  public void apportEvolutionUsedChanged(final H2dRubarApportSpatialMng _mng, final EvolutionReguliereInterface _dest) {
  }

  @Override
  public void ventEvolutionUsedChanged(H2dRubarVentMng _mng, EvolutionReguliereInterface _dest) {
  }

  @Override
  public void apportEvolutionContentChanged(final H2dRubarApportSpatialMng _mng, final EvolutionReguliereInterface _dest) {
    super.evolutionChanged(_dest);
  }

  @Override
  public void ventEvolutionContentChanged(H2dRubarVentMng _mng, EvolutionReguliereInterface _dest) {
    super.evolutionChanged(_dest);
  }

  @Override
  public void apportChanged(final H2dRubarApportSpatialMng _mng) {
  }

  @Override
  public void ventChanged(H2dRubarVentMng _mng) {
  }

  @Override
  protected EGCourbeChild createEGCourbe(final EGGroup _g, final EvolutionReguliere _e, final int _i) {
    final H2dRubarEvolution ne = (_e instanceof H2dRubarEvolution) ? (H2dRubarEvolution) _e : new H2dRubarEvolution(_e);

    final EGCourbeChild r = super.createEGCourbe(_g, ne, _i);
    final H2dVariableType destVar = getVar(_g);
    ne.setFmt(H2dRubarTimeCondition.getTimeFmt(destVar), H2dRubarTimeCondition.getFmt(destVar), null);
    return r;
  }

  @Override
  public void add(final EvolutionReguliereInterface[] _evol, final CtuluCommandManager _mng) {
    Map<EGGroup, List<EGCourbe>> courbesByGroups = new HashMap<>();
    if (_evol != null) {
      for (int i = _evol.length - 1; i >= 0; i--) {
        EGGroup groupToUse = groupNull_;
        if (_evol[i] instanceof EvolutionReguliere) {
          EvolutionReguliere evolutionReguliere = (EvolutionReguliere) _evol[i];
          if (evolutionReguliere.getInfos() != null && evolutionReguliere.getInfos().get(EvolutionReguliere.INFO_TYPE) instanceof H2dVariableType) {
            H2dVariableType variableType = (H2dVariableType) evolutionReguliere.getInfos().get(EvolutionReguliere.INFO_TYPE);
            if (variableType.getParentVariable() != null) {
              variableType = variableType.getParentVariable();
            }
            groupToUse = getGroupeFor(variableType);
            if (groupToUse == null) {
              groupToUse = groupNull_;
            }
          }
        }
        List<EGCourbe> egCourbes = courbesByGroups.get(groupToUse);
        if (egCourbes == null) {
          egCourbes = new ArrayList<>();
          courbesByGroups.put(groupToUse, egCourbes);
        }

        EGCourbeChild cs = createEGCourbe(groupToUse, H2dRubarEvolution.createH2dRubarEvolution(null, _evol[i], this));
        egCourbes.add(cs);
      }
      CtuluCommandComposite cmp = new CtuluCommandComposite();
      for (Map.Entry<EGGroup, List<EGCourbe>> egGroupListEntry : courbesByGroups.entrySet()) {
        final List<EGCourbe> courbesAdded = egGroupListEntry.getValue();
        curvesAdded(courbesAdded.toArray(new EGCourbeChild[courbesAdded.size()]), egGroupListEntry.getKey(), cmp);
      }
      if (_mng != null) {
        _mng.addCmd(cmp.getSimplify());
      }
    }
  }

  @Override
  protected List copyCourbe(final List _l, final EGGroup _dest) {
    int n = getNbEvol();
    final List r = new ArrayList(_l.size());
    final H2dVariableType destVar = getVar(_dest);
    final CtuluNumberFormater fmt = H2dRubarTimeCondition.getFmt(destVar);
    final CtuluNumberFormater fmtTime = H2dRubarTimeCondition.getTimeFmt(destVar);
    for (final Iterator it = _l.iterator(); it.hasNext(); ) {
      final EGCourbe c = (EGCourbe) it.next();
      final FudaaCourbeModel m = (FudaaCourbeModel) c.getModel();
      final H2dRubarEvolution ne = (H2dRubarEvolution) m.getEvolution().duplicate();
      if ((ne.isIdxErrorForFmt(fmtTime, fmt))) {
        final String msg = TrResource.getS(
            "La courbe {0} contient des valeurs erron�es. Ces valeurs seront supprim�es.\nVoulez-vous continuer ?",
            ne.getNom());
        if (!CtuluLibDialog.showConfirmation(BuLib.HELPER, CtuluUIAbstract.getDefaultWarnTitle(), msg)) {
          continue;
        }
      }
      ne.setFmt(fmtTime, fmt, null);
      if (ne.getListener() == null) {
        ne.setListener(this);
      }
      ne.setNom(majNom(ne.getNom(), ++n));
      r.add(createEGCourbe(_dest, ne));
    }
    return r;
  }

  @Override
  protected Map moveCourbe(final List _l, final EGGroup _p) {
    final Map r = new HashMap(_l.size());
    final H2dVariableType destVar = getVar(_p);
    final CtuluNumberFormater fmt = H2dRubarTimeCondition.getFmt(destVar);
    final CtuluNumberFormater fmtTime = H2dRubarTimeCondition.getTimeFmt(destVar);
    for (final Iterator it = _l.iterator(); it.hasNext(); ) {
      final EGCourbeChild c = (EGCourbeChild) it.next();
      final FudaaCourbeModel m = (FudaaCourbeModel) c.getModel();
      if (!m.getEvolution().isUsed()) {
        final H2dRubarEvolution ne = (H2dRubarEvolution) m.getEvolution();
        if (ne.isIdxErrorForFmt(fmtTime, fmt)) {
          final String msg = TrResource.getS(
              "La courbe {0} contient des valeurs erron�es. Ces valeurs seront supprim�es.\nVoulez-vous continuer ?",
              ne.getNom());
          if (!CtuluLibDialog.showConfirmation(BuLib.HELPER, TrResource.getS("Attention"), msg)) {
            continue;
          }
        }
        ne.setFmt(fmtTime, fmt, null);
        final EGGroup oldParent = c.getParentGroup();
        r.put(c, oldParent);
        _p.addEGComponent(c);
      }
    }
    return r;
  }
}
