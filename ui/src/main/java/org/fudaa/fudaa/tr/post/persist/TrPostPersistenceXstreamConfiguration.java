/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.persist;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.XmlFriendlyReplacer;
import com.thoughtworks.xstream.io.xml.XppDriver;
import org.fudaa.ctulu.xml.BooleanArrayConverter;
import org.fudaa.ctulu.xml.DoubleArrayConverter;
import org.fudaa.ctulu.xml.FloatArrayConverter;
import org.fudaa.ctulu.xml.IntegerArrayConverter;
import org.fudaa.ebli.commun.XstreamCustomizer;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.ebli.courbe.EGCourbeMarqueur;
import org.fudaa.ebli.courbe.convert.EGAxePersistConverter;
import org.fudaa.ebli.courbe.convert.EGPersistHelper;
import org.fudaa.ebli.visuallibrary.persist.EbliSceneSerializeXml;
import org.fudaa.ebli.visuallibrary.persist.EbliWidgetGroupSerializeXml;
import org.fudaa.ebli.visuallibrary.persist.EbliWidgetSerializeXml;
import org.fudaa.fudaa.tr.post.TrPostCourbeTreeModelPersist;
import org.fudaa.fudaa.tr.post.TrPostSourcePersistComposite;
import org.fudaa.fudaa.tr.post.TrPostTimeModelSaver;
import org.fudaa.fudaa.tr.post.TrPostUserVariableSaver;
import org.fudaa.fudaa.tr.post.data.TrPostDataCreatedStatisticSaver;
import org.fudaa.fudaa.tr.post.profile.MvProfileTreeModelPersist;
import org.netbeans.modules.visual.anchor.TriangleAnchorShape;

/**
 *
 * @author Frederic Deniger
 */
public class TrPostPersistenceXstreamConfiguration {

  protected static class CustomXstreamCustomizer implements XstreamCustomizer {

    boolean newVersion;

    public CustomXstreamCustomizer(boolean newVersion) {
      this.newVersion = newVersion;
    }

    @Override
    public void configureXstream(XStream xstream) {
      if (newVersion) {
        configureTraceColors(xstream);
      }
      xstream.omitField(TrPostCourbeTreeModelPersist.class, "varSupported");
      xstream.omitField(TrPostCourbeTreeModelPersist.class, "objIdx");
      xstream.omitField(TrPostCourbeTreeModelPersist.class, "intepolPt");
    }
  }

  private static void configureTraceColors(XStream xstream) {
    final TraceToStringConverter traceToStringConverter = new TraceToStringConverter();
    EGPersistHelper.registerDefaultConverters(xstream, traceToStringConverter);
    new EGAxePersistConverter(traceToStringConverter).initXstream(xstream);
    xstream.processAnnotations(EGCourbeMarqueur.class);
    xstream.registerConverter(new DoubleArrayConverter());
    xstream.registerConverter(new BooleanArrayConverter());
    xstream.registerConverter(new IntegerArrayConverter());
    xstream.registerConverter(new FloatArrayConverter());
    xstream.alias("PostVisuPanelPersistManager", org.fudaa.fudaa.tr.post.TrPostVisuPanelPersistManager.class);
    xstream.alias("MvProfileTreeModelPersist", MvProfileTreeModelPersist.class);
  }

  /**
   * Init le parser avec les alias adequats.
   *
   * @return
   */
  public static XStream createXmlParser(boolean newVersion) {
    final XmlFriendlyReplacer replacer = new XmlFriendlyReplacer("#", "_");
    final XppDriver staxDriver = new XppCustomDriver(replacer);
    final XStream xstream = new XStream(staxDriver);
    xstream.alias("LAYOUT", EbliSceneSerializeXml.class);
    xstream.alias("FRAME", EbliWidgetSerializeXml.class);
    xstream.alias("GROUPE", EbliWidgetGroupSerializeXml.class);
    xstream.alias("FRAMESNAME", EbliWidgetSerializeXml.CoupleNomId.class);
    xstream.alias("SOURCE", TrPostSourcePersist.class);
    xstream.omitField(TrPostSourcePersist.class, "rechargerLesLiens");
    xstream.alias("PROJECTED_SOURCE", TrPostSourceProjectedPersistReplay.class);
    xstream.alias("ONETIME_SOURCE", TrPostSourceOneTimeStepReplayPersist.class);
    xstream.alias("COMPARAISON_SOURCE", TrPostSourceComparaisonPersistReplay.class);
    xstream.alias("SUITECALCUL", TrPostSourcePersistComposite.class);
    xstream.alias("LAYOUTDESCRIPTOR", LayoutDescriptor.class);
    xstream.alias("README", XmlCommentaire.class);
    xstream.alias("TIMEMODEL", TrPostTimeModelSaver.class);
    xstream.alias("USERVAR", TrPostUserVariableSaver.class);
    xstream.omitField(TrPostDataCreatedStatisticSaver.class, "data_");
    xstream.omitField(TriangleAnchorShape.class, "generalPath");
    xstream.registerLocalConverter(TrPostDataCreatedStatisticSaver.class, "data_", new Converter() {
      @Override
      public boolean canConvert(Class type) {
        return true;
      }

      @Override
      public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        return null;
      }

      @Override
      public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
      }
    });
    xstream.omitField(TrPostCourbeTreeModelPersist.class, "varSupported");
    xstream.omitField(TrPostCourbeTreeModelPersist.class, "objIdx");
    xstream.omitField(TrPostCourbeTreeModelPersist.class, "intepolPt");
    if (newVersion) {
      configureTraceColors(xstream);
    }
    return xstream;
  }
}
