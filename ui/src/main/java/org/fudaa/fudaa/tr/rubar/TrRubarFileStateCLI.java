/**
 * @creation 13 oct. 2004 @modification $Date: 2007-05-04 14:01:53 $ @license GNU General Public License 2 @copyright (c)1998-2001
 *     CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryFlowrateGroupType;
import org.fudaa.dodico.mesure.EvolutionListener;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.rubar.io.RubarCLIFileFormat;
import org.fudaa.dodico.rubar.io.RubarCLIWriter;

import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFileStateCLI.java,v 1.15 2007-05-04 14:01:53 deniger Exp $
 */
public class TrRubarFileStateCLI extends TrRubarFileState implements EvolutionListener, H2dRubarBcListener {
  final H2dRubarBcMng mng_;
  private final H2dRubarVentMng ventMng;
  private final H2dRubarApportSpatialMng appMng;

  /**
   * @param _l le listener
   */
  public TrRubarFileStateCLI(final TrRubarFileStateListener _l, final H2dRubarParameters _p) {
    super(RubarCLIFileFormat.getInstance(), _l);
    mng_ = _p.getBcMng();
    ventMng = _p.getVentMng();
    appMng = _p.getAppMng();
    mng_.addEvolutionListener(this);
    mng_.addListener(this);
  }

  @Override
  public void areteTypeChanged() {
    setModified(true);
    updateCli();
  }

  @Override
  public void nodeInGridChanged() {
  }

  @Override
  public void bathyChanged() {
  }

  @Override
  public void fondDurChanged() {
  }

  @Override
  public void evolutionChanged(final EvolutionReguliereInterface _e) {
    if (!appMng.isUsed(_e) && !ventMng.isUsed(_e)) {
      setModified(true);
    }
  }

  @Override
  public void evolutionUsedChanged(final EvolutionReguliereInterface _e, final int _old, final int _new,
                                   final boolean _isAdjusting) {
    setModified(true);
    updateCli();
  }

  @Override
  public void flowrateGroupChanged(final H2dRubarBoundaryFlowrateGroupType _t) {
  }

  @Override
  public void projectTypeChanged() {
    setModified(true);
  }

  @Override
  public void numberOfConcentrationChanged() {

  }

  public boolean isNullOrAllNull(final Object[] _o) {
    if (_o == null) {
      return true;
    }
    for (int i = 0; i < _o.length; i++) {
      if (_o[i] != null) {
        return false;
      }
    }
    return true;
  }

  @Override
  public CtuluIOOperationSynthese save(final File _dir, final String _projectName, final ProgressionInterface _progression,
                                       final TrRubarProject _projet) {
    final RubarCLIFileFormat rubarCLIFileFormat = RubarCLIFileFormat.getInstance();
    final File f = rubarCLIFileFormat.getFileFor(_dir, _projectName);
    final H2dRubarTimeConditionInterface[] cliInOrder = _projet.getH2dRubarParameters().getBcMng().getCliInOrder();
    if (isNullOrAllNull(cliInOrder)) {
      setMarkRemoved(true);
      return null;
    }
    final RubarCLIWriter writer = (RubarCLIWriter) rubarCLIFileFormat.createWriter();
    writer.setNewFormat(_projet.isNewFormatForNumberOfDigits());
    return writer.write(cliInOrder, f, _progression);
  }

  @Override
  public void timeClChanged() {
  }

  public void updateCli() {
    final TrRubarFileStateMng mng = (TrRubarFileStateMng) super.l_;
    setMarkRemoved(!mng_.hasDataForCli());
    if (mng_.hasDataForCli() && !mng.contains(this)) {
      mng.add(this);
    }
  }

  @Override
  public void fileFormatDigitsChanged() {
    setModified(true);
  }
}
