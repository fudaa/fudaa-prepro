/*
 * @creation 3 juil. 2006
 * @modification $Date: 2006-09-19 15:07:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.persistence;

import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueSaverInterface;
import org.fudaa.ebli.calque.BCalqueSaverTargetInterface;
import org.fudaa.fudaa.sig.persistence.FSigLayerLinePersistence;

public final class TrSiPlanLayerPersistence extends FSigLayerLinePersistence {

  public TrSiPlanLayerPersistence() {}

  @Override
  protected BCalque findCalque(final BCalqueSaverInterface _saver, final BCalqueSaverTargetInterface _pn,
      final BCalque _parent) {
    return _parent == null ? null : _parent.getCalqueParNom(_saver.getLayerName());

  }
}