/*
 * @file         PrertPreferences.java
 * @creation     2002-08-30
 * @modification $Date: 2006-09-19 15:07:30 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr.common;

import org.fudaa.fudaa.commun.FudaaPreferencesAbstract;

/**
 * Preferences pour Prert.
 * 
 * @version $Id: TrPreferences.java,v 1.6 2006-09-19 15:07:30 deniger Exp $
 * @author Fred Deniger
 */
public final class TrPreferences extends FudaaPreferencesAbstract {
  public static final String KEY_SERVEUR_PREFIXE = "serveur";
  public static final String KEY_SERVEUR_SHOW_LOG = "serveur.log";
  public static final String KEY_CODE_ID = "tr.code.id";
  /**
   * Singleton.
   */
  public final static TrPreferences TR = new TrPreferences();

  private TrPreferences() {
    super();
  }

  @Override
  public void applyOn(final Object _o) {

  }
}
