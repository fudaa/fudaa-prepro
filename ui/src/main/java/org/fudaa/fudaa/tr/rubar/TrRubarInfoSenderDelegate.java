/**
 * @creation 10 juin 2004
 * @modification $Date: 2007-05-04 14:01:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuTable;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluNumberFormat;
import org.fudaa.ctulu.gui.CtuluCellDoubleRenderer;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarElementPropertyAbstract;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrInfoSenderH2dDelegate;
import org.fudaa.fudaa.tr.data.TrVarTableModel;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarInfoSenderDelegate.java,v 1.39 2007-05-04 14:01:53 deniger Exp $
 */
public class TrRubarInfoSenderDelegate extends TrInfoSenderH2dDelegate {
  /**
   * @author fred deniger
   * @version $Id: TrRubarInfoSenderDelegate.java,v 1.39 2007-05-04 14:01:53 deniger Exp $
   */
  static final class ElementSiTable extends CtuluTable {
    /**
     *
     */
    private final CtuluCellTextRenderer render_;

    /**
     * @param _render
     */
    ElementSiTable(final CtuluCellTextRenderer _render) {
      render_ = _render;
    }

    @Override
    public javax.swing.table.TableCellRenderer getCellRenderer(final int _row, final int _column) {
      if (_column > 0) {
        return render_;
      }
      return super.getCellRenderer(_row, _column);
    }
  }

  /**
   * @author fred deniger
   * @version $Id: TrRubarInfoSenderDelegate.java,v 1.39 2007-05-04 14:01:53 deniger Exp $
   */
  static final class ElementSICellRenderer extends CtuluCellTextRenderer {
    /**
     *
     */
    private final ElementSITableModel model_;

    /**
     * @param _model
     */
    ElementSICellRenderer(final ElementSITableModel _model) {
      model_ = _model;
    }

    @Override
    public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected,
                                                   final boolean _hasFocus, final int _row, final int _column) {
      super.getTableCellRendererComponent(_table, _value, _isSelected, _hasFocus, _row, _column);
      if (_value == null) {
        super.setText(CtuluLibString.EMPTY_STRING);
      } else {
        final CtuluNumberFormat fmt = model_.getNumberFormat(_column);
        super.setText(fmt == null ? _value.toString() : fmt.format(((Double) _value).doubleValue()));
      }
      return this;
    }
  }

  final class ElementSITableModel extends TrVarTableModel {
    ElementSITableModel() {
      super(getRub().getSi(), null);
    }

    public CtuluNumberFormat getNumberFormat(final int _column) {
      if (getSrc() == null) {
        return null;
      }
      if (_column == 1 || _column == 2) {
        return getRub().getFmt().getXY().getNumberFormat();
      }
      final H2dVariableType t = getVarAt(_column - 3);
      return getRub().getFmt().getCINFormat(t).getNumberFormat();
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: TrRubarInfoSenderDelegate.java,v 1.39 2007-05-04 14:01:53 deniger Exp $
   */
  final class ElementTableModel extends AbstractTableModel {
    final H2dVariableProviderInterface vars_;
    List varsList_;

    ElementTableModel() {
      vars_ = getRub().getElementData();
      varsList_ = new ArrayList(vars_.getUsableVariables());
    }

    @Override
    public Class getColumnClass(final int _columnIndex) {
      if (_columnIndex == 0) {
        return Integer.class;
      } else if (_columnIndex <= varsList_.size() + 2) {
        return Double.class;
      }
      return String.class;
    }

    @Override
    public int getColumnCount() {
      return 6 + varsList_.size();
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return MvResource.getS("Indice");
      }
      if (_column == 1) {
        return "X";
      }
      if (_column == 2) {
        return "Y";
      }

      if (_column >= 3 && _column <= varsList_.size() + 2) {
        return ((H2dVariableType) varsList_.get(_column - 3))
            .getName();
      }
      int idxApportWind = _column - varsList_.size() - 3;
      if (idxApportWind == 0) {
        return H2dVariableTransType.APPORT_PLUIE.getName();
      } else if (idxApportWind == 1) {
        return H2dVariableTransType.VENT_X.getName();
      } else {
        return H2dVariableTransType.VENT_Y.getName();
      }
    }

    @Override
    public int getRowCount() {
      return TrRubarInfoSenderDelegate.this.getGrid().getEltNb();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _column) {
      if (_column == 0) {
        return new Integer(_rowIndex + 1);
      }
      if (_column == 1) {
        return CtuluLib.getDouble(getRub().getGridVolume().getMoyCentreXElement(_rowIndex));
      }
      if (_column == 2) {
        return CtuluLib.getDouble(getRub().getGridVolume().getMoyCentreYElement(_rowIndex));
      }
      if (_column >= 3 && _column <= varsList_.size() + 2) {
        return CtuluLib.getDouble(vars_.getViewedModel(
            (H2dVariableType) varsList_.get(_column - 3)).getValue(_rowIndex));
      }
      int idxApportWind = _column - varsList_.size() - 3;
      if (idxApportWind == 0) {
        final EvolutionReguliereInterface e = getRub().getAppMng() == null ? null : getRub().getAppMng().getEvol(
            _rowIndex);
        if (e != null) {
          return e.getNom();
        }
      } else if (idxApportWind == 1) {
        final EvolutionReguliereInterface e = getRub().getVentMng() == null ? null : getRub().getVentMng().getEvolutionAlongX(
            _rowIndex);
        if (e != null) {
          return e.getNom();
        }
      } else {
        final EvolutionReguliereInterface e = getRub().getVentMng() == null ? null : getRub().getVentMng().getEvolutionAlongY(
            _rowIndex);
        if (e != null) {
          return e.getNom();
        }
      }

      return CtuluLibString.EMPTY_STRING;
    }

    public void updateCellTextRenderer(final JTable _r) {
      final TableColumnModel model = _r.getColumnModel();
      for (int i = 1; i <= 2; i++) {
        model.getColumn(i).setCellRenderer(new CtuluCellDoubleRenderer(getRub().getFmt().getXY().getNumberFormat()));
      }
      for (int i = 3; i <= 2 + varsList_.size(); i++) {
        model.getColumn(i).setCellRenderer(
            new CtuluCellDoubleRenderer(getRub().getFmt().getFormatterFor((H2dVariableType) varsList_.get(i - 3))
                .getNumberFormat()));
      }
      final CtuluCellTextRenderer ctuluCellTextRenderer = new CtuluCellTextRenderer();
      ctuluCellTextRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
      model.getColumn(3 + varsList_.size()).setCellRenderer(ctuluCellTextRenderer);
      model.getColumn(0).setCellRenderer(ctuluCellTextRenderer);
    }
  }

  /**
   * @param _p les parametres a considerer
   */
  public TrRubarInfoSenderDelegate(final TrRubarProject _p, final EbliFormatterInterface _fmt) {
    super(_fmt, _p.getH2dRubarParameters());
  }

  protected H2dRubarParameters getRub() {
    return (H2dRubarParameters) params_;
  }

  public BuTable createElementSITable() {
    final ElementSITableModel model = new ElementSITableModel();
    final CtuluCellTextRenderer render = new ElementSICellRenderer(model);
    final BuTable r = new ElementSiTable(render);

    r.setModel(model);
    return r;
  }

  public BuTable createElementTable() {
    final BuTable r = new CtuluTable();
    final ElementTableModel model = new ElementTableModel();
    r.setModel(model);
    model.updateCellTextRenderer(r);
    return r;
  }

  @Override
  public void fillWithElementInfo(final InfoData _m, final int _i) {
    super.fillWithElementInfo(_m, _i);
    if (getRub().getFriction() != null) {
      final H2dVariableType v = H2dVariableType.COEF_FROTTEMENT;
      _m.put(v.getName(), getRub().getFmt().getFormatterFor(v).format(getRub().getFriction().getValue(_i)));
    }
    final EvolutionReguliereInterface e = getRub().getAppMng().getEvol(_i);
    if (e != null) {
      _m.put(H2dResource.getS("Chronique d'apport"), e.getNom());
    }
    final H2dRubarElementPropertyAbstract si = getRub().getSi();
    if (si == null) {
      return;
    }
    for (final Iterator it = si.getVars().iterator(); it.hasNext(); ) {
      final H2dVariableType t = (H2dVariableType) it.next();
      final CtuluNumberFormat fmt = getRub().getFmt().getCINFormat(t).getNumberFormat();
      final String pref = TrLib.getSIDisplayName() + ": ";
      _m.put(pref + t.getName(), fmt.format(si.getValue(t, _i)));
    }
  }

  public void fillWithElementSIInfo(final InfoData _m, final CtuluListSelectionInterface _selection, final String _title) {
    super.fillWithElementInfo(_m, _selection, _title);
  }

  public void fillWithElementSIInfo(final InfoData _m, final int _i) {
    fillWithElementInfo(_m, _i);
  }

  /**
   * @param _m le receveur des infos
   * @param _selection la selection pour laquelle les rens sont demandes
   */
  public void fillWithInitInfo(final InfoData _m, final CtuluListSelectionInterface _selection) {
    if (_selection.isOnlyOnIndexSelected()) {
      final int i = _selection.getMinIndex();
      _m.setTitle(TrResource.getS("Noeud") + CtuluLibString.ESPACE + CtuluLibString.getString(i));
    } else {
      _m.setTitle(TrResource.getS("Conditions initiales"));
      _m.put(MvResource.getS("Nombre de noeuds"), CtuluLibString.getString(getGrid().getPtsNb()));
    }
  }

  /**
   * Redefinie car rubar gere les points. frontieres sous formes d'aretes.
   */
  @Override
  public void fillWithPointInfo(final InfoData _m, final int _idx) {
    fillWithPointInfo(_m, _idx, -1, 0, 0);
    fillNodalParametersInfo(_m, _idx);
  }

  @Override
  public EfGridInterface getGrid() {
    return getRub().getGridVolume();
  }
}
