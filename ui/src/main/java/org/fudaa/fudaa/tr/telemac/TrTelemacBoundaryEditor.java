/**
 * @creation 1 oct. 2003
 * @modification $Date: 2007-05-04 14:01:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluCommandCompositeInverse;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcManager;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcParameter;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBordParFrontiere;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBoundary;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.fudaa.fdico.FDicoEditorInterface;
import org.fudaa.fudaa.fdico.FDicoLib;
import org.fudaa.fudaa.fdico.FDicoTableEditorChooser;
import org.fudaa.fudaa.tr.common.TrCourbeTemporelleManager;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrTelemacBoundaryEditor.java,v 1.29 2007-05-04 14:01:46 deniger Exp $
 */
public class TrTelemacBoundaryEditor extends CtuluDialogPanel implements ItemListener {

  private abstract class BcParameterEdit extends JPanel implements MouseListener {

    JLabel lb_;

    H2dTelemacBcParameter t_;

    public BcParameterEdit(final H2dTelemacBcParameter _t) {
      t_ = _t;
    }

    protected void build() {
      lb_ = addLabel(this, t_.getVariable().getName());
      if (!isDataValid()) {
        lb_.setForeground(Color.red);
        lb_.setToolTipText(getErrorMessage());
      }
      lb_.addMouseListener(this);
    }

    public abstract void apply(CtuluCommandContainer _c);

    public abstract void fillWithEvol(Map<H2dVariableType, List<EvolutionReguliereAbstract>> _m);

    public String getErrorMessage() {
      return t_.getErrorString(clMng_.getDicoParams());
    }

    public Dimension getSecondCompDim() {
      return null;
    }

    public boolean isDataValid() {
      return t_.isValid(clMng_.getDicoParams());
    }

    @Override
    public void mouseClicked(final MouseEvent _e) {
      help_.setText(t_.getEntite().getNom() + ":" + CtuluLibString.LINE_SEP + t_.getEntite().getAide());
    }

    @Override
    public void mouseEntered(final MouseEvent _e) {
    }

    @Override
    public void mouseExited(final MouseEvent _e) {
    }

    @Override
    public void mousePressed(final MouseEvent _e) {
    }

    @Override
    public void mouseReleased(final MouseEvent _e) {
    }

    public void setSecondCompDim(final Dimension _d) {

    }

    public void updateEnableState() {
      final boolean b = t_.isParameterForType(getSelectedBoundary());
      for (int i = getComponentCount() - 1; i >= 0; i--) {
        getComponent(i).setEnabled(b);
      }
    }
  }

  private class LineComponent implements ItemListener {
    private JComboBox transientCombobox;

    public JComboBox getTransientCombobox() {
      return transientCombobox;
    }

    public void setTransientCombobox(JComboBox cb) {
      this.transientCombobox = cb;
      cb.addItemListener(this);
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
      setTransient(isTransientSelected());
      TrTelemacBoundaryEditor.this.revalidate();
      TrTelemacBoundaryEditor.this.repaint();
    }

    public boolean isTransientSelected() {
      return transientCombobox.getSelectedIndex() == 1;
    }

    public JComponent getSecondComponent() {
      if (transientCombobox != null) {
        return transientCombobox;
      }
      return editorPanel;
    }

    private JPanel editorPanel = new JPanel(new BorderLayout());

    LineComponent() {
      editorPanel.addPropertyChangeListener("enabled", new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
          updateEnableState();

        }
      });

    }

    public JPanel getEditorPanel() {
      return editorPanel;
    }

    FDicoEditorInterface fixeEditor_;
    JComboBox courbeSelector;

    public void setTransient(boolean b) {
      editorPanel.removeAll();
      if (b) {
        editorPanel.add(courbeSelector);
      } else {
        editorPanel.add(fixeEditor_.getComponent());
      }
      updateEnableState();
      editorPanel.invalidate();
    }

    private void updateEnableState() {
      int componentCount = editorPanel.getComponentCount();
      for (int i = 0; i < componentCount; i++) {
        ((JComponent) editorPanel.getComponent(i)).setEnabled(editorPanel.isEnabled());
      }
    }

  }

  private class BcParameterEditFixedOrTransient extends BcParameterEdit {

    LineComponent[] lines;

    public BcParameterEditFixedOrTransient(final H2dTelemacBcParameter _t) {
      super(_t);
      build();
    }

    @Override
    protected final void build() {
      super.build();
      boolean transientAvailable = clMng_.isTransientAvailable();
      int nbCol = 2;
      if (transientAvailable) {
        nbCol = 3;

      }
      setLayout(buildGridLayout(nbCol));
      int nbInternValues = t_.getNbInternValues();
      lines = new LineComponent[nbInternValues];
      //to go to a new line:
      boolean mutliValues = nbInternValues > 1;
      if (mutliValues) {
        for (int i = 1; i < nbCol; i++) {
          add(new JLabel());
        }
      }
      for (int i = 0; i < nbInternValues; i++) {
        LineComponent line = new LineComponent();
        lines[i] = line;
        line.fixeEditor_ = creator_.createEditor(t_.getEntite().getType());
        line.fixeEditor_.setValue(t_.getValueFromBord(bord_, i));
        if (mutliValues) {
          add(new JLabel(t_.getInternValueName(i)));
        }
        boolean done = false;
        if (transientAvailable) {
          line.setTransientCombobox(new BuComboBox(new String[] { fixe_, trans_ }));
          line.courbeSelector = new BuComboBox();
          evolMng_.createComboBoxModel(t_.getVariable(), line.courbeSelector);
          add(line.transientCombobox);
          if (t_.isValueTransientFor(bord_, i)) {
            line.transientCombobox.setSelectedIndex(1);
            line.courbeSelector.setSelectedItem(bord_.getEvolution(t_.getVariable(), i));
            line.setTransient(true);
            done = true;
          }
        }
        if (!done) {
          line.setTransient(false);
        }
        line.updateEnableState();

        add(line.getEditorPanel());
      }

    }

    @Override
    public void apply(final CtuluCommandContainer _c) {
      String[] values = new String[lines.length];
      for (int i = 0; i < lines.length; i++) {
        LineComponent lineComponent = lines[i];
        values[i] = lineComponent.fixeEditor_.getValue();
      }
      t_.setValues(clMng_, bord_, values, _c);
    }

    @Override
    public void fillWithEvol(final Map<H2dVariableType, List<EvolutionReguliereAbstract>> _varEvol) {
      EvolutionReguliereAbstract[] evol = new EvolutionReguliereAbstract[lines.length];
      boolean containsEvol = false;
      for (int i = 0; i < lines.length; i++) {
        LineComponent lineComponent = lines[i];
        if (lineComponent.isTransientSelected()) {
          evol[i] = (EvolutionReguliereAbstract) lineComponent.courbeSelector.getSelectedItem();
          containsEvol = true;
        }
      }
      if (containsEvol) {
        _varEvol.put(t_.getVariable(), Arrays.asList(evol));
      }
    }

    @Override
    public Dimension getSecondCompDim() {
      Dimension d = lines[0].getSecondComponent().getPreferredSize();
      for (int i = 1; i < lines.length; i++) {
        Dimension size = lines[i].getSecondComponent().getPreferredSize();
        d.width = Math.max(d.width, size.width);
        d.height = Math.max(d.height, size.height);
      }
      return d;
    }

    @Override
    public void setSecondCompDim(final Dimension _d) {
      for (int i = 0; i < lines.length; i++) {
        lines[i].getSecondComponent().setPreferredSize(_d);
      }
    }
  }

  private class BcParameterEditNotSet extends BcParameterEdit {

    public BcParameterEditNotSet(final H2dTelemacBcParameter _p) {
      super(_p);
      build();
    }

    @Override
    protected void build() {
      setLayout(buildGridLayout(2));
      super.build();
      addLabel(this, TrResource.getS("Non utilis�")).setEnabled(false);
      lb_.setEnabled(false);
    }

    @Override
    public void apply(final CtuluCommandContainer _c) {
    }

    @Override
    public void fillWithEvol(final Map<H2dVariableType, List<EvolutionReguliereAbstract>> _m) {
    }

    @Override
    public void updateEnableState() {

    }
  }

  private class ParametersBloc implements MouseListener {

    FDicoEditorInterface[] edValues_;

    JLabel[] lbValues_;

    H2dTelemacBcParameter[] params_;

    public ParametersBloc(final H2dTelemacBcParameter[] _params, final BuPanel _pn, final FDicoTableEditorChooser.EditorCreator _creator) {
      params_ = _params;
      if (params_ == null) {
        return;
      }
      final int n = _params.length;
      if (n == 0) {
        return;
      }
      _pn.setLayout(buildGridLayout(2));
      lbValues_ = new JLabel[n];
      edValues_ = new FDicoEditorInterface[n];
      FDicoEditorInterface ed;
      for (int i = 0; i < n; i++) {
        ed = _creator.createEditor(params_[i].getEntite().getType());
        lbValues_[i] = addLabel(_pn, params_[i].getEntite().getNom());
        lbValues_[i].addMouseListener(this);
        lbValues_[i].setLabelFor(ed.getComponent());
        edValues_[i] = ed;
        _pn.add(ed.getComponent());
        ed.setValue(params_[i].getValueFromBord(bord_, 0));
        if (!params_[i].isValid(clMng_.getDicoParams())) {
          lbValues_[i].setForeground(Color.red);
          ((JComponent) edValues_[i].getComponent()).setToolTipText(params_[i].getErrorString(clMng_.getDicoParams()));
        } else if (params_[i].isValueSetInDico(clMng_.getDicoParams())) {
          lbValues_[i].setForeground(Color.blue);
          lbValues_[i].setToolTipText(TrResource.getS("Mot-cl� modifi�"));
        } else {
          lbValues_[i].setForeground(Color.black);
          lbValues_[i].setToolTipText(CtuluLibString.EMPTY_STRING);
        }
      }
    }

    protected void updateStates() {
      final H2dBoundaryType b = (H2dBoundaryType) cbBord_.getSelectedItem();
      final int n = params_.length;
      for (int i = 0; i < n; i++) {
        final boolean enable = params_[i].isParameterForType(b);
        edValues_[i].getComponent().setEnabled(enable);
      }
    }

    public boolean apply(final CtuluCommandCompositeInverse _cmd) {
      final int n = params_.length;
      boolean r = false;
      for (int i = 0; i < n; i++) {
        if (params_[i].isParameterForType((H2dBoundaryType) cbBord_.getSelectedItem())) {
          final String s = edValues_[i].getValue();
          if ((s != null) && (s.length() > 0)) {
            r |= params_[i].setValue(clMng_, bord_, 0, s, _cmd);
          }
        }
      }
      return r;
    }

    public boolean isDataValid() {
      final int n = params_.length;
      for (int i = 0; i < n; i++) {
        if (!params_[i].isValid(clMng_.getDicoParams())) {
          return false;
        }
      }
      return true;
    }

    @Override
    public void mouseClicked(final MouseEvent _e) {
      final Component c = _e.getComponent();
      for (int i = lbValues_.length - 1; i >= 0; i--) {
        if (c == lbValues_[i]) {
          help_.setText(params_[i].getEntite().getNom() + ":" + CtuluLibString.LINE_SEP + params_[i].getEntite().getAide());
        }
      }
    }

    @Override
    public void mouseEntered(final MouseEvent _e) {
    }

    @Override
    public void mouseExited(final MouseEvent _e) {
    }

    @Override
    public void mousePressed(final MouseEvent _e) {
    }

    @Override
    public void mouseReleased(final MouseEvent _e) {
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: TrTelemacBoundaryEditor.java,v 1.29 2007-05-04 14:01:46 deniger Exp $
   */
  public class BcParameterSpaceActivatedEditor extends BcParameterEdit {

    JComboBox cbT_;

    public BcParameterSpaceActivatedEditor(final H2dTelemacBcParameter _bc) {
      super(_bc);
      if (_bc.getVariable() != H2dVariableTransType.TRACEUR) {
        FuLog.warning(new Throwable());
      }
      if (t_.isValueSetInDico(clMng_.getDicoParams())) {
        FuLog.warning(new Throwable());
      }
      build();
    }

    @Override
    protected void build() {
      setLayout(buildGridLayout(2));
      super.build();
      final String[] choiceItem = new String[2];
      choiceItem[0] = TrResource.getS("Libre");
      choiceItem[1] = TrResource.getS("Impos� sur ce bord");
      cbT_ = new BuComboBox(choiceItem);
      cbT_.setSelectedIndex(bord_.isTracerImposed() ? 1 : 0);
      add(cbT_);
    }

    @Override
    public void apply(final CtuluCommandContainer _c) {
      final boolean activated = 1 == (cbT_.getSelectedIndex());
      _c.addCmd(clMng_.setLiquidBordVariableActivatedAndCmd(bord_, super.t_.getVariable(), activated));
    }

    @Override
    public void fillWithEvol(final Map<H2dVariableType, List<EvolutionReguliereAbstract>> _m) {
    }

    @Override
    public Dimension getSecondCompDim() {
      return cbT_.getPreferredSize();
    }

    @Override
    public void setSecondCompDim(final Dimension _d) {
      cbT_.setPreferredSize(_d);
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: TrTelemacBoundaryEditor.java,v 1.29 2007-05-04 14:01:46 deniger Exp $
   */
  public class BcParameterSpaceVelocityEditor extends BcParameterEdit {

    JComboBox cbUV_;

    public BcParameterSpaceVelocityEditor(final H2dTelemacBcParameter _bc) {
      super(_bc);
      if (_bc.getVariable() != H2dVariableType.VITESSE) {
        FuLog.warning(new Throwable());
      }
      if (t_.isValueSetInDico(clMng_.getDicoParams())) {
        FuLog.warning(new Throwable());
      }
      build();
    }

    @Override
    protected final void build() {
      setLayout(buildGridLayout(2));
      super.build();
      final String[] choiceItem = new String[3];
      choiceItem[0] = TrResource.getS("u impos�");
      choiceItem[1] = TrResource.getS("v impos�");
      choiceItem[2] = TrResource.getS("u et v impos�s");
      cbUV_ = new BuComboBox(choiceItem);
      if (bord_.isUTypeImposed()) {
        if (bord_.isVTypeImposed()) {
          cbUV_.setSelectedIndex(2);
        } else {
          cbUV_.setSelectedIndex(0);
        }
      } else if (bord_.isVTypeImposed()) {
        cbUV_.setSelectedIndex(1);
      } else {
        if (t_.isParameterForType(bord_.getType())) {
          FuLog.warning(new Throwable());
        }
        cbUV_.setSelectedIndex(2);
      }
      add(cbUV_);
    }

    @Override
    public void apply(final CtuluCommandContainer _c) {
      final int i = cbUV_.getSelectedIndex();
      final boolean u = i != 1;
      final boolean v = i > 0;
      _c.addCmd(clMng_.setLiquidBordUVAndCmd(bord_, u, v));
    }

    @Override
    public void fillWithEvol(final Map<H2dVariableType, List<EvolutionReguliereAbstract>> _m) {
    }

    @Override
    public Dimension getSecondCompDim() {
      return cbUV_.getPreferredSize();
    }

    @Override
    public void setSecondCompDim(final Dimension _d) {
      cbUV_.setPreferredSize(_d);
    }
  }

  /**
   * @param _nbCol le nombre de colonne du layout / the number of layout column
   * @return un layout commun pour tous les panneaux / a common layout for all the panels
   */
  public static BuGridLayout buildGridLayout(final int _nbCol) {
    return new BuGridLayout(_nbCol, 5, 5, true, false, true, true);
  }

  H2dTelemacBoundary bord_;
  JComboBox cbBord_;
  H2dTelemacBcManager clMng_;
  FDicoTableEditorChooser.EditorCreator creator_;
  BcParameterEdit[] editor_;
  TrCourbeTemporelleManager evolMng_;
  final String fixe_ = TrResource.getS("Fixe");
  BuTextArea help_;
  CtuluCommandCompositeInverse mng_;
  ParametersBloc optionsBloc_;
  final String trans_ = TrResource.getS("Variable en temps");
  TrTelemacSolidBoundaryPtsEditor solidEditor_;

  /**
   * @param _clMng
   * @param _bord
   * @param _mng
   * @param _evolMng
   */
  public TrTelemacBoundaryEditor(final H2dTelemacBcManager _clMng, final H2dTelemacBoundary _bord, final CtuluCommandCompositeInverse _mng,
      final TrCourbeTemporelleManager _evolMng) {
    super();
    bord_ = _bord;
    clMng_ = _clMng;
    mng_ = _mng;
    evolMng_ = _evolMng;
    if (bord_.getType().isLiquide()) {
      init();
    } else {
      setLayout(new BuBorderLayout(5, 5, true, true));
      final H2dTelemacBordParFrontiere fr = (H2dTelemacBordParFrontiere) clMng_.getBlockFrontier(bord_.getIdxFrontiere());
      final int[] idx = fr.getIdxIn(bord_);
      if (idx != null) {
        solidEditor_ = new TrTelemacSolidBoundaryPtsEditor(mng_, fr, bord_, idx, clMng_);
        add(solidEditor_);
      }
    }
  }

  private BcParameterEdit getPanelEditor(final H2dTelemacBcParameter _bc) {
    if ((_bc.isValueSetInDico(clMng_.getDicoParams())) || (_bc.isFixedByUser())) {
      return new BcParameterEditFixedOrTransient(_bc);
    } else if (_bc.getVariable() == H2dVariableType.VITESSE) {
      return new BcParameterSpaceVelocityEditor(_bc);
    } else if (_bc.hasToBeActivatedForBoundary()) {
      return new BcParameterSpaceActivatedEditor(_bc);
    } else if (!_bc.isKeywordImposed()) {
      return new BcParameterEditNotSet(_bc);
    }
    if (_bc.isParameterForType(bord_.getType())) {
      FuLog.warning(new Throwable());
    }
    return new BcParameterEditFixedOrTransient(_bc);
  }

  private void init() {
    creator_ = new FDicoTableEditorChooser.EditorCreator();
    if (clMng_.getIdxLiquidBord(bord_) < 0) {
      setErrorText("bord not found");
    } else {
      addEmptyBorder(5);
      setLayout(new BuVerticalLayout(5, true, false));
      cbBord_ = new JComboBox();
      final List allBordType = clMng_.getBordList();
      final List v = new ArrayList(allBordType.size());
      for (final Iterator it = allBordType.iterator(); it.hasNext();) {
        final H2dBoundaryType t = (H2dBoundaryType) it.next();
        if (t.isLiquide()) {
          v.add(t);
        }
      }
      cbBord_.setModel(new DefaultComboBoxModel(v.toArray()));
      cbBord_.setSelectedItem(bord_.getType());
      add(cbBord_);
      BuPanel values = new BuPanel();
      values.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray), FDicoLib.getS("Mot-cl�s: options")));

      optionsBloc_ = new ParametersBloc(clMng_.getLimitOptions(), values, creator_);
      if (optionsBloc_.lbValues_ != null) {
        add(values);
      }
      values = new BuPanel();
      values.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray), FDicoLib.getS("Variables")));
      final H2dTelemacBcParameter[] params = clMng_.getLimitParameters();
      editor_ = new BcParameterEdit[params.length];
      values.setLayout(new BuVerticalLayout(5, true, false));
      for (int i = 0; i < params.length; i++) {
        editor_[i] = getPanelEditor(params[i]);
        values.add(editor_[i]);
        editor_[i].updateEnableState();
      }
      if (params.length > 0) {
        add(values);
      }
      if ((!isParamValid()) || (!optionsBloc_.isDataValid())) {
        setErrorText(TrResource.getS("Des valeurs sont erron�es.Verifiez la validit� de vos param�tres dans l'�diteur des mot-cl�s"));
      }

      cbBord_.addItemListener(this);
      help_ = new BuTextArea();
      help_.setLineWrap(true);
      help_.setWrapStyleWord(true);
      help_.setRows(5);
      help_.setEditable(false);
      help_.setText(TrResource.getS("clicker sur un label pour avoir de l'aide"));
      add(help_);
      update();
      // maj des taille des label
      // updating the label size
      Dimension common = null, current = null;
      Dimension common2 = null;
      revalidate();
      int width;
      FontMetrics fm = null;
      for (int i = editor_.length - 1; i >= 0; i--) {
        if (common == null) {
          common = editor_[i].lb_.getPreferredSize();
          fm = BuLib.getFontMetrics(editor_[i].lb_);
          common.width = fm.stringWidth(editor_[i].lb_.getText()) + 5;
        } else {
          current = editor_[i].getPreferredSize();
          width = fm == null ? 0 : fm.stringWidth(editor_[i].lb_.getText()) + 5;
          if (width > common.width) {
            common.width = width;
          }
          if (current.height > common.height) {
            common.height = current.height;
          }
        }
        if (common2 == null) {
          common2 = editor_[i].getSecondCompDim();
        } else {
          current = editor_[i].getSecondCompDim();
          if (current != null) {
            if (current.width > common2.width) {
              common2.width = current.width;
            }
            if (current.height > common2.height) {
              common2.height = current.height;
            }
          }
        }
      }
      for (int i = editor_.length - 1; i >= 0; i--) {
        editor_[i].lb_.setPreferredSize(common);
        if (common2 != null) {
          editor_[i].setSecondCompDim(common2);
        }
      }
    }
  }

  protected H2dBoundaryType getSelectedBoundary() {
    return (H2dBoundaryType) cbBord_.getSelectedItem();
  }

  @Override
  public boolean apply() {
    if (!bord_.getType().isLiquide() && solidEditor_ != null) {
      solidEditor_.apply();
      return true;
    }

    final Map<H2dVariableType, List<EvolutionReguliereAbstract>> evols = new HashMap<H2dVariableType, List<EvolutionReguliereAbstract>>(
        editor_.length);
    for (int i = editor_.length - 1; i >= 0; i--) {
      editor_[i].fillWithEvol(evols);
    }
    mng_.addCmd(clMng_.setLiquidBordTypeEvolAndCmd(bord_, (H2dBoundaryType) cbBord_.getSelectedItem(), evols));
    optionsBloc_.apply(mng_);

    for (int i = editor_.length - 1; i >= 0; i--) {
      editor_[i].apply(mng_);
    }
    return true;
  }

  public final boolean isParamValid() {
    for (int i = editor_.length - 1; i >= 0; i--) {
      if (!editor_[i].isDataValid()) {
        return false;
      }
    }
    return true;
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if (_e.getStateChange() == ItemEvent.SELECTED) {
      update();
    }
  }

  public void update() {
    optionsBloc_.updateStates();
    for (int i = editor_.length - 1; i >= 0; i--) {
      editor_[i].updateEnableState();
    }
  }

  @Override
  public boolean isDataValid() {
    if (!bord_.getType().isLiquide() && solidEditor_ != null) {
      return solidEditor_.isDataValid();
    }
    return true;
  }
}
