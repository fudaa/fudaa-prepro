/**
 * @creation 15 d�c. 2004
 * @modification $Date: 2007-05-04 14:01:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.PairString;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.rubar.H2dRubarVentListener;
import org.fudaa.dodico.h2d.rubar.H2dRubarVentMng;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.palette.BPalettePlageAbstract;
import org.fudaa.ebli.palette.BPalettePlageDiscret;
import org.fudaa.ebli.palette.BPlageDiscret;
import org.fudaa.ebli.palette.PaletteCouleurContinu;
import org.fudaa.ebli.trace.BPlageInterface;
import org.fudaa.fudaa.meshviewer.layer.MvGridLayerGroup;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;
import org.fudaa.fudaa.meshviewer.model.MvIsoPainter;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarApportIsoPainter.java,v 1.15 2007-05-04 14:01:53 deniger Exp $
 */
public class TrRubarVentIsoPainter implements MvIsoPainter, H2dRubarVentListener {
  private BPalettePlageDiscret pal_;
  private H2dRubarParameters param_;

  /**
   * @param _param les parametres
   */
  public TrRubarVentIsoPainter(final H2dRubarParameters _param) {
    param_ = _param;
    param_.getVentMng().addVentListener(this);
  }

  @Override
  public boolean isPaletteInitialized() {
    return pal_ != null;
  }

  @Override
  public void setPalette(final BPalettePlageAbstract _palette) {
    if (pal_ == null) {
      initPal();
    }
    pal_.initFrom(_palette);
  }

  private void updatePlage() {
    if (!isPaletteInitialized()) {
      return;
    }
    final EvolutionReguliereInterface[] e = param_.getVentMng().getUsedCourbes();
    if (e == null) {
      pal_.clear();
      return;
    }
    Map<PairString, BPlageInterface> existingPlage = new HashMap<>();
    for (BPlageInterface plage : pal_.getPlages()) {
      existingPlage.put((PairString) plage.getIdentifiant(), plage);
    }
    TreeMap<PairString, BPlageInterface> toUse = new TreeMap<>();

    final EfGridInterface g = param_.getMaillage();
    for (int i = g.getEltNb() - 1; i >= 0; i--) {
      PairString key = getPairStringForVent(i);
      if (key.isBlank()) {
        continue;
      }
      final BPlageInterface plage = toUse.get(key);
      if (plage == null) {
        BPlageInterface plageInterface = existingPlage.get(key);
        if (plageInterface == null) {
          plageInterface = new BPlageDiscret(key);
          plageInterface.setLegende(key.getFormattedString());
        }
        toUse.put(key, plageInterface);
      }
    }
    double nb = toUse.size();
    int idx = 0;
    for (BPlageInterface value : toUse.values()) {
      if (value.getCouleur() == null) {
        ((BPlageDiscret) value).setCouleur(BPalettePlageAbstract.getCouleur(PaletteCouleurContinu.DEFAULT_MIN, PaletteCouleurContinu.DEFAULT_MAX, ((double) idx) / nb));
      }
      idx++;
    }
    pal_.setPlages(toUse.values().toArray(new BPlageInterface[0]));
  }

  private PairString getPairStringForVent(int i) {
    final EvolutionReguliereInterface ventX = param_.getVentMng().getEvolutionAlongX(i);
    final EvolutionReguliereInterface ventY = param_.getVentMng().getEvolutionAlongY(i);
    return new PairString(ventX == null ? null : ventX.getNom(), ventY == null ? null : ventY.getNom());
  }

  private void initPal() {
    if (pal_ == null) {
      pal_ = new BPalettePlageDiscret(null);
      pal_.setTitre(getNom());
      pal_.setSousTitre(CtuluLibString.EMPTY_STRING);
      updatePlage();
    }
  }

  @Override
  public void ventChanged(final H2dRubarVentMng _mng) {
    updatePlage();
  }

  @Override
  public void ventEvolutionContentChanged(final H2dRubarVentMng _mng, final EvolutionReguliereInterface _dest) {

  }

  @Override
  public void ventEvolutionUsedChanged(final H2dRubarVentMng _mng, final EvolutionReguliereInterface _dest) {
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("update plage for vent");
    }
    updatePlage();
  }

  @Override
  public double getMax() {
    return 0;
  }

  @Override
  public double getMin() {
    return 0;
  }

  @Override
  public String getNom() {
    return H2dVariableType.VENT.getName();
  }

  @Override
  public BPalettePlageAbstract getPalette() {
    initPal();
    return pal_;
  }

  @Override
  public boolean isDiscrete() {
    return true;
  }

  @Override
  public void paint(final Graphics2D _g, final GrMorphisme _versEcran, final GrBoite _clipReel, final int _alpha) {
    final GrBoite d = MvGridLayerGroup.getDomaine(param_.getMaillage());
    if (!d.intersectXY(_clipReel)) {
      return;
    }
    final GrPolygone p = new GrPolygone();
    final GrBoite pBoite = new GrBoite();
    final Color old = _g.getColor();
    initPal();
    final EfGridInterface g = param_.getMaillage();
    for (int i = g.getEltNb() - 1; i >= 0; i--) {
      PairString key = getPairStringForVent(i);
      if (key != null && !key.isBlank()) {
        MvElementModelDefault.initGrPolygone(g, g.getElement(i), p);
        p.boite(pBoite);
        if (pBoite.intersectXY(_clipReel)) {
          final BPlageInterface plage = pal_.getPlageFor(key);
          if (plage != null) {
            _g.setColor(EbliLib.getAlphaColor(plage.getCouleur(), _alpha));
          }
          p.autoApplique(_versEcran);
          _g.fill(p.polygon());
        }
      }
    }
    _g.setColor(old);
  }

  @Override
  public String toString() {
    return getNom();
  }
}
