/*
 *  @creation     21 mars 2005
 *  @modification $Date: 2007-05-22 14:20:38 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.CtuluDurationFormatter;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.editor.CtuluValueEditorTime;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPlage;
import org.fudaa.ebli.trace.BPlageInterface;

/**
 * @author Fred Deniger
 * @version $Id: TrPostPaletteTime.java,v 1.9 2007-05-22 14:20:38 deniger Exp $
 */
public class TrPostPaletteTime extends BPalettePlage {

  TrPostSource src_;
  CtuluValueEditorTime editor_;

  public TrPostPaletteTime(final TrPostSource _src) {
    src_ = _src;
  }

  @Override
  public CtuluNumberFormatI getFormatter() {
    return src_.getTimeFormatter() == null ? CtuluDurationFormatter.DEFAULT_TIME : src_.getTimeFormatter();
  }

  @Override
  public CtuluValueEditorI getValueEditor() {
    if (editor_ == null) {
      editor_ = new CtuluValueEditorTime();
    }
    editor_.setFmt(src_.getTimeFormatter());
    return editor_;
  }

  @Override
  public BPlageInterface createPlage(final BPlageInterface _src) {
    if (_src == null) { return new BTimePlage(); }
    return new BTimePlage(_src);
  }

  private class BTimePlage extends BPlage {

    public BTimePlage() {
      super();
    }

    @Override
    public BPlageInterface copy() {
      return new BTimePlage(this);
    }

    /**
     * @param _r
     */
    public BTimePlage(final BPlageInterface _r) {
      super(_r);
    }

    @Override
    public boolean ajusteLegendes(CtuluNumberFormatI formatter, String sep) {
      return super.ajusteLegendes(TrPostPaletteTime.this.getFormatter(), sep);
    }

    /*
     * public void ajusteLegendes() { final StringBuffer bf = new StringBuffer(); final String inf =
     * EbliLib.getS("Infini"); if (getMin() == Double.POSITIVE_INFINITY) { bf.append('+').append(inf); } else if
     * (getMin() == Double.NEGATIVE_INFINITY) { bf.append('-').append(inf); } else { if (src_.getTimeFormatter() ==
     * null) { bf.append(BPlage.PLAGE_NUMBER_FORMAT.format(getMin())); } else {
     * bf.append(src_.getTimeFormatter().format(getMin())); } } // Rajout du deuxieme intervalle si la valeur du
     * min<>valeur du max if (getMin() != getMax()) {
     * bf.append(CtuluLibString.ESPACE).append(EbliLib.getS("�")).append(CtuluLibString.ESPACE); if (getMax() ==
     * Double.POSITIVE_INFINITY) { bf.append('+').append(inf); } else if (getMax() == Double.NEGATIVE_INFINITY) {
     * bf.append('-').append(inf); } else { if (src_.getTimeFormatter() == null) {
     * bf.append(BPlage.PLAGE_NUMBER_FORMAT.format(getMax())); } else {
     * bf.append(src_.getTimeFormatter().format(getMax())); } } } legende_ = bf.toString(); }
     */
  }
}