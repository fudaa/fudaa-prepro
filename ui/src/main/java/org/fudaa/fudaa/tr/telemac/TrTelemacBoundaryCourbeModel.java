/**
 * @creation 6 juil. 2004 @modification $Date: 2007-05-22 14:20:38 $ @license GNU General Public License 2 @copyright (c)1998-2001
 *     CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBordParFrontiere;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBoundary;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBoundaryCondition;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.courbe.EGCourbeModelDefault;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

import java.awt.*;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacBoundaryCourbeModel.java,v 1.20 2007-05-22 14:20:38 deniger Exp $
 */
public class TrTelemacBoundaryCourbeModel implements EGModel {
  private final H2dTelemacBoundary b_;
  private final H2dTelemacBordParFrontiere fr_;
  private boolean isUpdateToDate_;
  private double max_;
  private double min_;
  private int nbIndex_;
  private final H2dVariableType t_;
  private double[] x_;
  String nom_;

  /**
   * @param _fr la frontiere / the boundary
   * @param _bord le bord / the edge
   * @param _t la variable dessine / the drawned varaible
   */
  public TrTelemacBoundaryCourbeModel(final String _nom, final H2dTelemacBordParFrontiere _fr,
                                      final H2dTelemacBoundary _bord, final H2dVariableType _t, final double[] _x) {
    nom_ = _nom;
    t_ = _t;
    fr_ = _fr;
    b_ = _bord;
    nbIndex_ = b_.getNPointInBord(fr_.getNbPt());
    x_ = _x;
  }

  @Override
  public boolean isGenerationSourceVisible() {
    return false;
  }

  @Override
  public Color getSpecificColor(int idx) {
    return null;
  }

  @Override
  public boolean useSpecificIcon(int idx) {
    return false;
  }

  private void computeMinMax() {
    if (isUpdateToDate_) {
      return;
    }
    if (t_ == null) {
      min_ = 0;
      max_ = 1;
    } else {
      min_ = getY(0);
      max_ = min_;
      double t;
      for (int i = nbIndex_ - 1; i > 0; i--) {
        t = getY(i);
        if (t < min_) {
          min_ = t;
        }
        if (t > max_) {
          max_ = t;
        }
      }
    }
    isUpdateToDate_ = true;
  }

  private int[] getReelIdxs(final int[] _idx) {
    final int deb = b_.getIdxDeb();
    for (int i = _idx.length - 1; i >= 0; i--) {
      _idx[i] = fr_.getReelIdxOnFr(_idx[i] + deb);
    }
    return _idx;
  }

  protected H2dVariableType getVar() {
    return t_;
  }

  protected final double[] getX() {
    return x_;
  }

  protected void majAll(final double[] _x) {
    isUpdateToDate_ = false;
    nbIndex_ = b_.getNPointInBord(fr_.getNbPt());
    x_ = _x;
  }

  /**
   * @param _t la variable modifiee / the modified variable
   * @return true si c'est la variable dessinee / true if it is the drawn variable
   */
  protected boolean valueChangedFor(final H2dVariableType _t) {
    if (_t == t_) {
      isUpdateToDate_ = false;
      return true;
    }
    return false;
  }

  @Override
  public boolean addValue(final double _x, final double _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean addValue(final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  public boolean deplace(final int _select, final double _deltaX, final double _deltaY, final CtuluCommandContainer _cmd) {
    final H2dTelemacBoundaryCondition c = fr_.getTelemacCl(fr_.getReelIdxOnFr(_select + b_.getIdxDeb()));
    return setValue(_select, _deltaX, c.getValue(t_) + _deltaY, _cmd);
  }

  @Override
  public boolean deplace(final int[] _select, final double _deltaX, final double _deltaY,
                         final CtuluCommandContainer _cmd) {
    return EGCourbeModelDefault.deplace(this, _select, _deltaX, _deltaY, _cmd);
  }

  public int getActiveTimeIdx() {
    return 0;
  }

  @Override
  public int getNbValues() {
    if (t_ == null) {
      return 0;
    }
    return nbIndex_;
  }

  @Override
  public String getTitle() {
    return nom_;
  }

  @Override
  public double getX(final int _idx) {
    return x_[_idx];
  }

  @Override
  public double getXMax() {
    return x_[nbIndex_ - 1];
  }

  @Override
  public double getXMin() {
    return 0;
  }

  @Override
  public double getY(final int _idx) {
    if (t_ == null) {
      return 0;
    }
    final H2dTelemacBoundaryCondition c = fr_.getTelemacCl(fr_.getReelIdxOnFr(_idx + b_.getIdxDeb()));
    return c.getValue(t_);
  }

  @Override
  public double getYMax() {
    computeMinMax();
    return max_;
  }

  @Override
  public double getYMin() {
    computeMinMax();
    return min_;
  }

  public boolean isActiveTimeEnable() {
    return false;
  }

  @Override
  public boolean isDuplicatable() {
    return true;
  }

  @Override
  public boolean isModifiable() {
    return t_ != null;
  }

  @Override
  public boolean isPointDrawn(final int _i) {
    return true;
  }

  @Override
  public boolean isRemovable() {
    return false;
  }

  @Override
  public boolean isSegmentDrawn(final int _i) {
    return true;
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  public boolean isVisibleLong() {
    return false;
  }

  @Override
  public boolean isXModifiable() {
    return false;
  }

  @Override
  public boolean removeValue(final int _i, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean removeValue(final int[] _i, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean setTitle(final String _newName) {
    return false;
  }

  /**
   * On n'utilise pas le _x. We don't use the _x
   */
  @Override
  public boolean setValue(final int _i, final double _x, final double _y, final CtuluCommandContainer _cmd) {
    if (t_.getParentVariable() == H2dVariableType.VITESSE && (_y < -1 || _y > 1)) {
      return false;
    }
    if (_cmd != null) {
      _cmd.addCmd(fr_.setValuesForBoundaryPoints(new int[]{fr_.getReelIdxOnFr(_i + b_.getIdxDeb())}, t_, _y));
    }
    return true;
  }

  @Override
  public boolean setValues(final int[] _idx, final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    if (_cmd != null) {
      _cmd.addCmd(fr_.setValuesForBoundaryPoints(getReelIdxs(_idx), t_, _y));
    }
    return true;
  }

  @Override
  public void fillWithInfo(final InfoData _table, final CtuluListSelectionInterface _selectedPt) {
  }

  @Override
  public EGModel duplicate() {

    return new TrTelemacBoundaryCourbeModel(this.nom_, this.fr_, this.b_, this.t_, CtuluLibArray.copy(this.x_));
  }

  @Override
  public Object savePersistSpecificDatas() {
    return null;
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
  }

  @Override
  public void replayData(EGGrapheModel model, Map infos, CtuluUI impl) {
  }

  @Override
  public boolean isReplayable() {
    return false;
  }

  @Override
  public String getPointLabel(int i) {
    return null;
  }

  @Override
  public int[] getInitRows() {
    return null;
  }
}
