/**
 * @creation 17 d�c. 2003
 * @modification $Date: 2007-05-04 14:01:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.h2d.telemac.H2dTelemacParameters;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import java.text.DecimalFormat;

/**
 * Classe permettant de gerer la duree d'une simulation telemac.
 * Class allowing to manage the duration of a telemac simulation
 *
 * @author deniger
 * @version $Id: TrTelemacDurationEditor.java,v 1.13 2007-05-04 14:01:50 deniger Exp $
 */
public class TrTelemacDurationEditor extends CtuluDialogPanel implements DocumentListener {
  private H2dTelemacParameters params_;
  private BuTextField[] txtYMDhms_;
  private BuTextField txtDuration_;
  private BuTextField txtNbTimeStep_;
  private BuTextField txtTimeStep_;
  private int[] temp_;
  private DicoEntite entDuration_;
  private DicoEntite entNbTimeSteps_;
  private DicoEntite entTimeStep_;
  private boolean duration_;
  private DecimalFormat ft_ = new DecimalFormat("0.#");

  public TrTelemacDurationEditor(final H2dTelemacParameters _params) {
    params_ = _params;
    final long initTime = params_.getComputationDuration();
    setLayout(new BuGridLayout(2, 10, 10));
    txtYMDhms_ = new BuTextField[6];
    temp_ = new int[6];
    TrLib.getYearMonthDayTime(temp_, initTime);

    addLabel(TrResource.getS("Ans, mois, jours"));
    BuPanel p = new BuPanel();
    p.setLayout(new BuGridLayout(3, 2, 2));
    int i = 0;
    txtYMDhms_[i] = addIntegerText(p, temp_[i++]);
    txtYMDhms_[i] = addIntegerText(p, temp_[i++]);
    txtYMDhms_[i] = addIntegerText(p, temp_[i++]);
    add(p);
    addLabel(TrResource.getS("Heures, minutes, secondes"));
    p = new BuPanel();
    p.setLayout(new BuGridLayout(3, 2, 2));
    txtYMDhms_[i] = addIntegerText(p, temp_[i++]);
    txtYMDhms_[i] = addIntegerText(p, temp_[i++]);
    txtYMDhms_[i] = addIntegerText(p, temp_[i++]);
    for (int j = txtYMDhms_.length - 1; j >= 0; j--) {
      txtYMDhms_[j].setColumns(4);
      txtYMDhms_[j].getDocument().addDocumentListener(this);
    }
    add(p);
    entDuration_ = params_.getTelemacVersion().getDureeDuCalcul();
    entNbTimeSteps_ = params_.getTelemacVersion().getNbPasTemps();
    entTimeStep_ = params_.getTelemacVersion().getPasTemps();
    if ((entNbTimeSteps_ != null) && (entTimeStep_ != null)) {
      final DicoParams dicoPar = params_.getDicoParams();
      final double d = Double.parseDouble(dicoPar.getValue(entTimeStep_));
      // un peu complique. on test si le mot_cle duration est initialise et si
      // c'est lui qui est utilise pour calcul le temps de calcul
      // a little complicated. We verify if the keyword duration is initialized and if
      // it is used to compute the time calculation
      if ((entDuration_ != null)
          && (dicoPar.isValueSetFor(entDuration_))
          && ((!dicoPar.isValueSetFor(entNbTimeSteps_)) || (Double.parseDouble(dicoPar.getValue(entDuration_)) > (Double
          .parseDouble(dicoPar.getValue(entNbTimeSteps_)) * d)))) {
        duration_ = true;
      }
      final JLabel l = duration_ ? addLabel(entDuration_ == null ? "" : entDuration_.getNom()) : addLabel(TrResource.getS("Dur�e (sec):"));
      txtDuration_ = addLongText(initTime);
      l.setLabelFor(txtDuration_);
      txtDuration_.getDocument().addDocumentListener(this);

      addLabel(entNbTimeSteps_.getNom());
      int nb = 0;
      if (d > 0) {
        nb = (int) (initTime / d);
      }
      if ((!duration_) && (dicoPar.isValueSetFor(entNbTimeSteps_))) {
        nb = Integer.parseInt(dicoPar.getValue(entNbTimeSteps_));
      }
      txtNbTimeStep_ = addIntegerText(nb);
      txtNbTimeStep_.getDocument().addDocumentListener(this);
      addLabel(entTimeStep_.getNom());
      txtTimeStep_ = addDoubleText(dicoPar.getValue(entTimeStep_));
      txtTimeStep_.getDocument().addDocumentListener(this);
    }
  }

  private long getDurationFromNb() {
    return (long) Math.ceil(getValueTxtField(txtTimeStep_) * getValueTxtField(txtNbTimeStep_));
  }

  private int getValueFromTxtYmd(final int _txtIdx) {
    final String s = txtYMDhms_[_txtIdx].getText();
    return s.length() == 0 ? 0 : Integer.parseInt(s);
  }

  private double getValueTxtField(final BuTextField _bu) {
    final String s = _bu.getText();
    return s.length() == 0 ? 0 : Double.parseDouble(s);
  }

  private long getDuration() {
    long newDuration = 0;
    try {
      final String s = txtDuration_.getText();
      newDuration = s.length() == 0 ? 0 : Long.parseLong(s);
    } catch (final NumberFormatException e) {
    }
    return newDuration;
  }

  public boolean isDuration() {
    return duration_;
  }

  public String getTimeStep() {
    return txtTimeStep_.getText();
  }

  public String getNbTimeStep() {
    return txtNbTimeStep_.getText();
  }

  public String getDurationString() {
    return txtDuration_.getText();
  }

  private void updateYMH() {
    final long newDuration = getDuration();
    TrLib.getYearMonthDayTime(temp_, newDuration);
    for (int i = txtYMDhms_.length - 1; i >= 0; i--) {
      final String s = CtuluLibString.getString(temp_[i]);
      if (!s.equals(txtYMDhms_[i].getText())) {
        txtYMDhms_[i].setText(s);
      }
    }
  }

  private int istxtYmdDoc(final Document _d) {
    for (int i = txtYMDhms_.length - 1; i >= 0; i--) {
      if (txtYMDhms_[i].getDocument() == _d) {
        return i;
      }
    }
    return -1;
  }

  private void updateTimeStep() {
    final long l = getDuration();
    final double timeStep = getValueTxtField(txtTimeStep_);
    String timeStepString = ft_.format(timeStep);
    final int nbTimeStep = timeStep == 0 ? 0 : (int) (l / timeStep);
    if (nbTimeStep > 0) {
      timeStepString = ft_.format((double) l / (double) nbTimeStep);
    }
    final double newTimeStep = Double.parseDouble(timeStepString);
    final long newl = (long) Math.ceil(newTimeStep * nbTimeStep);
    if (newl != l) {
      CtuluLibMessage.error(getClass().getName() + " ceil error " + l + " must be " + newl);
      if (txtDuration_.hasFocus()) {
        CtuluLibMessage.error(getClass().getName() + " ceil error no update");
      } else {
        txtDuration_.setText(Long.toString(newl));
        updateYMH();
      }
    }
    final String s = CtuluLibString.getString(nbTimeStep);
    if (!s.equals(txtNbTimeStep_.getText())) {
      txtNbTimeStep_.setText(s);
    }
    if (!timeStepString.equals(txtTimeStep_.getText())) {
      txtTimeStep_.setText(timeStepString);
    }
  }

  public void update(final Document _e) {
    if (_e == txtDuration_.getDocument()) {
      if (txtDuration_.hasFocus()) {
        updateYMH();
        updateTimeStep();
      }
    } else if ((txtTimeStep_.getDocument() == _e) || (txtNbTimeStep_.getDocument() == _e)) {
      if (((txtTimeStep_.getDocument() == _e) && (txtTimeStep_.hasFocus()))
          || ((txtNbTimeStep_.getDocument() == _e) && (txtNbTimeStep_.hasFocus()))) {
        final String s = Long.toString(getDurationFromNb());
        if (!txtDuration_.getText().equals(s)) {
          txtDuration_.setText(s);
        }
        updateYMH();
      }
    } else {
      final int idx = istxtYmdDoc(_e);
      if (idx >= 0 && txtYMDhms_[idx].hasFocus()) {
        final String s = Long.toString(TrLib.getDuration(getValueFromTxtYmd(0), getValueFromTxtYmd(1),
            getValueFromTxtYmd(2), getValueFromTxtYmd(3), getValueFromTxtYmd(4), getValueFromTxtYmd(5)));
        if (!s.equals(txtDuration_.getText())) {
          txtDuration_.setText(s);
        }
        updateTimeStep();
      }
    }
  }

  @Override
  public void changedUpdate(final DocumentEvent _e) {
    update(_e.getDocument());
  }

  @Override
  public void insertUpdate(final DocumentEvent _e) {
    update(_e.getDocument());
  }

  @Override
  public void removeUpdate(final DocumentEvent _e) {
    update(_e.getDocument());
  }
}
