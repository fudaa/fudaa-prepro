/**
 * @creation 4 oct. 2004
 * @modification $Date: 2006-09-19 15:07:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.H2dRegularGrid;
import org.fudaa.dodico.h2d.H2dRegularGridInterface;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarRegularGridEditor.java,v 1.9 2006-09-19 15:07:26 deniger Exp $
 */
public class TrRubarRegularGridEditor extends CtuluDialogPanel {

  BuTextField x0_;
  BuTextField y0_;
  BuTextField dx_;
  BuTextField dy_;
  BuTextField nbX_;
  BuTextField nbY_;

  public TrRubarRegularGridEditor(final H2dRegularGridInterface _grid) {
    setLayout(new BuGridLayout(2, 5, 5));
    // Xo
    x0_ = addLabelDoubleText("X0");
    x0_.setText(Double.toString(_grid.getX0()));
    // yO
    y0_ = addLabelDoubleText("Y0");
    y0_.setText(Double.toString(_grid.getY0()));
    final BuValueValidator pos = BuValueValidator.MIN(0);
    // dx
    dx_ = addLabelDoubleText("Delta X");
    dx_.setValueValidator(pos);
    dx_.setText(Double.toString(_grid.getDX()));
    // dy
    dy_ = addLabelDoubleText("Delta Y");
    dy_.setValueValidator(pos);
    dy_.setText(Double.toString(_grid.getDY()));
    // nbX
    nbX_ = addLabelIntegerText(TrResource.getS("Nombre de points sur X"));
    nbX_.setText(Integer.toString(_grid.getNbPtOnX()));
    nbX_.setValueValidator(pos);
    // nbY
    nbY_ = addLabelIntegerText(TrResource.getS("Nombre de points sur Y"));
    nbY_.setText(Integer.toString(_grid.getNbPtOnY()));
    nbY_.setValueValidator(pos);
  }

  @Override
  public boolean isDataValid() {
    return dx_.isValid() && dy_.isValid() && nbX_.isValid() && nbY_.isValid();
  }

  protected H2dRegularGridInterface getResult() {
    return new H2dRegularGrid(((Integer) nbX_.getValue()).intValue(), ((Integer) nbY_.getValue()).intValue(),
        ((Double) dx_.getValue()).doubleValue(), ((Double) dy_.getValue()).doubleValue(), ((Double) x0_.getValue())
            .doubleValue(), ((Double) y0_.getValue()).doubleValue());

  }

}