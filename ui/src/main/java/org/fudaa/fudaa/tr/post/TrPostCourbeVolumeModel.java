/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.*;
import org.fudaa.dodico.ef.EfFilter;
import org.fudaa.dodico.ef.EfFilterNone;
import org.fudaa.dodico.ef.EfFilterSelectedElement;
import org.fudaa.dodico.ef.cubature.EfComputeVolumeSeuil;
import org.fudaa.dodico.ef.cubature.EfDataIntegrale;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimePersistBuilder;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.dialogSpec.EnumThresholdCalculation;
import org.fudaa.fudaa.tr.post.persist.TrPostReloadParameter;
import org.fudaa.fudaa.tr.post.replay.TrReplayerVolume;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public class TrPostCourbeVolumeModel extends AbstractPostCourbeModel {
  private EnumThresholdCalculation computation = EnumThresholdCalculation.ALL;
  private FastBitSet selectedMeshes;
  private double seuil;
  private int minTimeIdx = -1;
  private int maxTimeIdx = -1;

  @Override
  public boolean isCourbeForInterpolation(TrPostInterpolatePoint _int) {
    return false;
  }

  public void setNom(String nom) {
    this.nom_ = nom;
  }

  public EnumThresholdCalculation getComputation() {
    return computation;
  }

  public void setComputation(EnumThresholdCalculation computation) {
    this.computation = computation;
  }

  public FastBitSet getSelectedMeshes() {
    return selectedMeshes;
  }

  public void setSelectedMeshes(FastBitSet selectedMeshes) {
    this.selectedMeshes = selectedMeshes;
  }

  public double getSeuil() {
    return seuil;
  }

  public void setSeuil(double seuil) {
    this.seuil = seuil;
  }

  public int getMinTimeIdx() {
    return minTimeIdx;
  }

  public void setMinTimeIdx(int minTimeIdx) {
    this.minTimeIdx = minTimeIdx;
  }

  public int getMaxTimeIdx() {
    return maxTimeIdx;
  }

  public void setMaxTimeIdx(int maxTimeIdx) {
    this.maxTimeIdx = maxTimeIdx;
  }

  @Override
  public boolean isCourbeForObject(int _idxObj) {
    return false;
  }

  public int getNbTimeSteps() {
    if (minTimeIdx < 0 || maxTimeIdx < 0) {
      return source_.getNbTimeStep();
    }
    return Math.abs(maxTimeIdx - minTimeIdx) + 1;
  }

  @Override
  public Object savePersistSpecificDatas() {
    Map savedData = new HashMap();
    savedData.put("sourceId", source_.getId());
    int[] selected = null;
    if (selectedMeshes != null) {
      CtuluListSelection list = new CtuluListSelection(selectedMeshes);
      selected = list.getSelectedIndex();
    }
    savedData.put("selectedMeshes", selected);
    savedData.put("var", var_.getID());
    savedData.put("seuil", seuil);
    savedData.put("startTime", minTimeIdx);
    savedData.put("endTime", maxTimeIdx);
    if (EnumThresholdCalculation.ALL != computation) {
      savedData.put("type", computation.getPersistName());
    }
    return savedData;
  }

  private EfFilter getFilter() {
    if (selectedMeshes != null) {
      return new EfFilterSelectedElement(new CtuluListSelection(selectedMeshes), source_.getGrid());
    }
    return new EfFilterNone();
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
    super.restoreFromSpecificDatas(data, infos);
    if (data == null || !(data instanceof Map)) {
      return;
    }
    Map mapData = (Map) data;
    String idSource = (String) mapData.get("sourceId");
    TrPostProjet projet = (TrPostProjet) infos.get(TrPostReloadParameter.POST_PROJET);
    TrPostSource src = null;
    if (projet != null) {
      // -- etape 1: recherche du source qui contient le path donn� --//
      src = projet.getSources().findSourceById(idSource);
      if (src != null) {
        this.source_ = src;
      } else {
        ((List<String>) infos.get("errorMsg")).add("Erreur, la frame graphe ne trouve pas le fichier r�sultat qui correspond � l'ID " + idSource);
        return;
      }
    } else {
      ((List<String>) infos.get("errorMsg")).add("Erreur, la frame graphe ne trouve pas le fichier r�sultat qui correspond � l'ID " + idSource);
      return;
    }

    String idVar = (String) mapData.get("var");
    H2dVariableType var = null;
    infos.put(FudaaCourbeTimePersistBuilder.TIME_MODEL, src.getNewTimeListModel());
    // -- recherche dans les sources de la variable par id --//
    for (int i = 0; i < src.getAvailableVar().length; i++) {
      if (src.getAvailableVar()[i].getID().equals(idVar)) {
        var = src.getAvailableVar()[i];
      }
    }
    if (var != null) {
      this.var_ = var;
    } else {
      ((List<String>) infos.get("errorMsg")).add(TrResource.TR.getString("La frame graphe ne trouve pas la variable qui correspond � l'ID {0}", idVar));
      return;
    }
    int[] idx = (int[]) mapData.get("selectedMeshes");
    selectedMeshes = null;
    if (idx != null) {
      selectedMeshes = new FastBitSet();
      for (int i : idx) {
        selectedMeshes.set(i);
      }
    }
    seuil = (Double) mapData.get("seuil");
    minTimeIdx = getCorrectedTime((Integer) mapData.get("startTime"));
    maxTimeIdx = getCorrectedTime((Integer) mapData.get("endTime"));
    String typeString = (String) mapData.get("type");
    if (typeString != null) {
      computation = EnumThresholdCalculation.getFromSaved(typeString);
    }
    if (computation == null) {
      computation = EnumThresholdCalculation.ALL;
    }
    if (TrPostReloadParameter.isForceRecompute(infos)) {
      updateData(null);
    }
  }

  private int getCorrectedTime(int i) {
    int res = Math.max(0, i);
    return Math.min(res, source_.getTime().getNbTimeStep() - 1);
  }

  @Override
  public void fillWithInfo(InfoData _table, CtuluListSelectionInterface _selectedPt) {
    final String fonctionName = TrLib.getString("Volume");
    _table.put(TrResource.getS("Type"), fonctionName);
    super.fillWithInfo(_table, _selectedPt);
    if (computation != EnumThresholdCalculation.ALL) {
      _table.put(TrResource.getS("Seuil"), (EnumThresholdCalculation.NEGATIVE.equals(computation) ? "<= " : ">= ") + Double.toString(seuil));
    }
    _table.put(TrResource.getS("Temps de d�but"), source_.getFormattedTime(minTimeIdx));
    _table.put(TrResource.getS("Temps de fin"), source_.getFormattedTime(maxTimeIdx));
    _table.put(TrResource.getS("Variable source"), this.getVar().getName() + " [" + this.getVar().getCommonUnitString() + "]");
    if ("m".equals(getVar().getCommonUnitString())) {
      _table.put(TrResource.getS("Variable"), TrLib.getString("Volume") + " [m3]");
    } else {
      _table.put(TrResource.getS("Variable"), fonctionName + "(" + this.getVar().getName() + ")");
    }
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
    if (!(impl instanceof TrPostCommonImplementation)) {
      impl.error(TrResource.getS("Impossible de r�cup�rer la bonne interface fudaa"));
      return;
    }
    TrPostCommonImplementation implementation = (TrPostCommonImplementation) impl;
    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    new TrReplayerVolume(source_, implementation).displayVolumeProfilOrigin(this, implementation);
  }

  @Override
  protected void createY() {
    if (y_ == null || y_.length != getNbTimeSteps()) {
      y_ = new double[getNbTimeSteps()];
    }
  }

  @Override
  public boolean isValid() {
    return true;
  }

  @Override
  public void replayData(EGGrapheModel grapheTreeModel, Map infos, CtuluUI impl) {
    if (!(impl instanceof TrPostCommonImplementation)) {
      impl.error(TrResource.getS("Impossible de r�cup�rer la bonne interface fudaa"));
      return;
    }
    TrPostCommonImplementation implementation = (TrPostCommonImplementation) impl;
    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    new TrReplayerVolume(source_, implementation).displayVolumeReplayData((TrPostCourbeTreeModel) grapheTreeModel, this);
  }

  public void updateData(ProgressionInterface prog) {
    int firstIdx = Math.max(0, minTimeIdx);
    int lastIdx = source_.getNbTimeStep() - 1;
    if (maxTimeIdx >= 0) {
      lastIdx = Math.min(maxTimeIdx, lastIdx);
    }
    if (firstIdx > lastIdx) {
      int tmp = lastIdx;
      lastIdx = firstIdx;
      firstIdx = tmp;
    }
    time_ = new double[getNbTimeSteps()];
    int idx = 0;
    for (int timeStepIdx = firstIdx; timeStepIdx <= lastIdx; timeStepIdx++) {
      time_[idx++] = source_.getTime().getTimeStep(timeStepIdx);
    }
    updateY(prog);
  }

  @Override
  public void updateY() {
    updateY(null);
  }

  public void updateY(ProgressionInterface prog) {
    createY();
    int firstIdx = Math.max(0, minTimeIdx);
    int lastIdx = source_.getNbTimeStep() - 1;
    if (maxTimeIdx >= 0) {
      lastIdx = Math.min(maxTimeIdx, lastIdx);
    }
    if (firstIdx > lastIdx) {
      int tmp = lastIdx;
      lastIdx = firstIdx;
      firstIdx = tmp;
    }
    // le builder permet de corriger les erreurs concernant les cote d'eau / bathymetrie
    List<DataIntegraleCallable> callables = createCallables(prog, firstIdx, lastIdx, var_);
    ExecutorService newFixedThreadPool = null;
    try {
      newFixedThreadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
      List<Future<Result>> invokeAll = newFixedThreadPool.invokeAll(callables);
      for (Future<Result> future : invokeAll) {
        Result result = future.get();
        switch (computation) {
          case NEGATIVE:
            y_[result.idx] = result.dataIntegrale.getZoneMoins();
          case POSITIVE:
            y_[result.idx] = result.dataIntegrale.getZonePlus();
          default:
            y_[result.idx] = result.dataIntegrale.getResultat();
        }
      }
    } catch (Exception e) {
      Logger.getLogger(TrPostCourbeBilanModel.class.getName()).log(Level.INFO, "updateY", e);
    } finally {
      if (newFixedThreadPool != null) {
        newFixedThreadPool.shutdownNow();
      }
    }
  }

  private List<DataIntegraleCallable> createCallables(ProgressionInterface prog, int firstIdx, int lastIdx, final H2dVariableType selectedVariable) {
    int idx = 0;
    if (prog != null) {
      prog.setProgression(0);
    }
    double nb = getNbTimeSteps();
    ProgressionTarget progTarget = new ProgressionTarget(nb, prog);
    //a revoir pour multi-thread.
    EfFilter filter = getFilter();
    List<DataIntegraleCallable> callables = new ArrayList<>();
    for (int timeStepIdx = firstIdx; timeStepIdx <= lastIdx; timeStepIdx++) {
      DataIntegraleCallable callable = new DataIntegraleCallable(idx, filter, timeStepIdx, selectedVariable);
      callable.setProgTarget(progTarget);
      callables.add(callable);
      idx++;
    }
    return callables;
  }

  private static class ProgressionTarget {
    int currentIdx = 0;
    double nb;
    ProgressionInterface progression;

    public ProgressionTarget(double nb, ProgressionInterface progression) {
      this.progression = progression;
      this.nb = nb;
    }

    public void inc() {
      EventQueue.invokeLater(() -> {
        ++currentIdx;
        if (progression != null) {
          progression.setProgression((int) (((double) currentIdx) / nb * 100d));
        }
      });
    }
  }

  public static class Result {
    EfDataIntegrale dataIntegrale;
    int idx;

    public Result(EfDataIntegrale dataIntegrale, int ix) {
      this.dataIntegrale = dataIntegrale;
      this.idx = ix;
    }
  }

  private class DataIntegraleCallable implements Callable<Result> {
    int timeStepIdx;
    final H2dVariableType selectedVariable;
    int idx;
    ProgressionTarget progTarget;
    EfFilter eltFilter;

    public DataIntegraleCallable(int idx, EfFilter eltFilter, int timeStepIdx, H2dVariableType selectedVariable) {
      this.idx = idx;
      this.eltFilter = eltFilter;
      this.timeStepIdx = timeStepIdx;
      this.selectedVariable = selectedVariable;
    }

    public void setProgTarget(ProgressionTarget progTarget) {
      this.progTarget = progTarget;
    }

    @Override
    public Result call() throws Exception {
      final EfComputeVolumeSeuil compute = EfComputeVolumeSeuil.getVolumeComputer(eltFilter, null, source_,
          selectedVariable, getSeuil(), new CtuluAnalyze());
      EfDataIntegrale resCalcul = compute.getVolume(timeStepIdx);
      if (progTarget != null) {
        progTarget.inc();
      }
      return new Result(resCalcul, idx);
    }
  }
}
