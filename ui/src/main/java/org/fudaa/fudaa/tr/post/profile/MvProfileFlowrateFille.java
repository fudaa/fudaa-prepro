/*
 * @creation 31 janv. 07
 * 
 * @modification $Date: 2007-05-04 13:59:50 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import java.awt.Color;
import java.awt.Dimension;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheTreeModel;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTime;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModelDefault;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeModel;
import org.fudaa.fudaa.commun.courbe.FudaaGrapheTimeAnimatedFille;
import org.fudaa.fudaa.commun.courbe.FudaaGrapheTimeAnimatedVisuPanel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * @author fred deniger
 * @version $Id: MvProfileFlowrateFille.java,v 1.5 2007-05-04 13:59:50 deniger Exp $
 */
public final class MvProfileFlowrateFille extends FudaaGrapheTimeAnimatedFille {
  private int idx_;
  private int idxVolume_;

  // private static int idxFrame_;

  protected static class FlowrateTreeModel extends EGGrapheTreeModel {

    @Override
    public boolean canDeleteCourbes() {
      return true;
    }

  }

  public static String getCommonTitle(final FudaaCommonImplementation _impl) {
    // on enregistre le compteur dans une propriete du JComponent.
    final Integer idxFrame = (Integer) _impl.getRootPane().getClientProperty("fudaa.flowrate.frame.idx");

    return H2dVariableType.DEBIT.getName() + '/' + DodicoLib.getS("Volumes")
        + CtuluLibString.getEspaceString(idxFrame == null ? 1 : idxFrame.intValue());
  }

  public static MvProfileFlowrateFille createBaseFille(final FudaaCommonImplementation _impl, final String _title,
      final FudaaCourbeTimeListModel _timeFmt) {
    final String title = CtuluLibString.isEmpty(_title) ? getCommonTitle(_impl) : _title;
    final MvProfileFlowrateFille fille = new MvProfileFlowrateFille(new FudaaGrapheTimeAnimatedVisuPanel(new EGGraphe(
        new FlowrateTreeModel())), title, _impl, _timeFmt);
    fille.setPreferredSize(new Dimension(600, 500));
    fille.setFrameIcon(FudaaResource.FUDAA.getFrameIcon("debit"));
    fille.putClientProperty("fudaa.profile.flowrate", Boolean.TRUE);
    fille.pack();
    _impl.addInternalFrame(fille);
    final Integer idxFrame = (Integer) _impl.getRootPane().getClientProperty("fudaa.flowrate.frame.idx");
    final int next = idxFrame == null ? 2 : (idxFrame.intValue() + 1);
    _impl.getRootPane().putClientProperty("fudaa.flowrate.frame.idx", new Integer(next));
    return fille;
  }

  EGGroup grQ_;
  EGGroup grVolume_;
  double[] commonTime_;
  FudaaCourbeTimeListModel commonTimeModel_;

  protected MvProfileFlowrateFille(final FudaaGrapheTimeAnimatedVisuPanel _g, final String _titre,
      final FudaaCommonImplementation _appli, final FudaaCourbeTimeListModel _timeFmt) {
    super(_g, _titre, _appli, null);
    getGraphe().setXAxe(EGAxeHorizontal.buildDefautTimeAxe(_timeFmt == null ? null : _timeFmt.getTimeFmt()));
  }

  protected double[] getTime(final double[] _t) {
    if (commonTime_ == null) {
      commonTime_ = _t;
      commonTimeModel_ = new FudaaCourbeTimeListModelDefault(commonTime_,getTimeFmt());
      return commonTime_;
    }
    return (CtuluLibArray.isDoubleEquals(commonTime_, _t, 1E-4)) ? commonTime_ : _t;

  }

  protected FudaaCourbeTimeListModel getTimeModel(final double[] _t) {
    if (_t == commonTime_ && commonTime_ != null) { return commonTimeModel_; }
    return new FudaaCourbeTimeListModelDefault(_t,getTimeFmt());
  }

  protected void addCourbe(final double[] _time, final double[] _q, final double[] _vCumule, final String _title) {
    final String cTitle = _title == null ? (MvResource.getS("Profil") + CtuluLibString.getEspaceString(++idx_))
        : _title;
    final double[] time = getTime(_time);
    final FudaaCourbeTimeModel mQ = new FudaaCourbeTimeModel(time);
    mQ.setY(_q);
    mQ.setTitle(cTitle);
    final FudaaCourbeTimeModel mQCumulue = new FudaaCourbeTimeModel(time);
    mQCumulue.setY(_vCumule);

    mQCumulue.setTitle(cTitle);
    createFlowrateGroup();
    final FudaaCourbeTimeListModel timeModel = getTimeModel(time);
    final EGCourbeChild courbeChild = new FudaaCourbeTime(grQ_, mQ, timeModel);
    grQ_.addEGComponent(courbeChild);
    createVolumeGroup();
    final EGCourbeChild courbeVolumeCumulee = new FudaaCourbeTime(grVolume_, mQCumulue, timeModel);
    courbeVolumeCumulee.setAspectContour(Color.GREEN);
    grVolume_.addEGComponent(courbeVolumeCumulee);
    getGrapheTree().fireStructureChanged();
    getGrapheTree().setSelectedComponent(courbeChild);
    getGraphe().restore();

  }

  private void createFlowrateGroup() {
    if (grQ_ == null) {
      grQ_ = new EGGroup();
      grQ_.setTitle(H2dVariableType.DEBIT_M3.getName());
      grQ_
          .setAxeY(new EGAxeVertical(H2dVariableType.DEBIT_M3.getName(), H2dVariableType.DEBIT_M3.getCommonUnitString()));
      getGrapheTree().add(grQ_);
    }
  }

  public void addVolumeCourbe(final double[] _time, final double[] _volume, final String _title) {
    final String cTitle = _title == null ? (DodicoLib.getS("Volume") + CtuluLibString.getEspaceString(++idxVolume_))
        : _title;
    final double[] time = getTime(_time);
    final FudaaCourbeTimeModel mV = new FudaaCourbeTimeModel(time);
    mV.setY(_volume);
    mV.setTitle(cTitle);
    createVolumeGroup();
    final EGCourbeChild courbeV = new FudaaCourbeTime(grVolume_, mV, getTimeModel(time));
    courbeV.setAspectContour(Color.BLUE);
    grVolume_.addEGComponent(courbeV);
    getGrapheTree().fireStructureChanged();
    getGrapheTree().setSelectedComponent(courbeV);
    getGraphe().restore();

  }

  private void createVolumeGroup() {
    if (grVolume_ == null) {
      grVolume_ = new EGGroup();
      grVolume_.setTitle(DodicoLib.getS("Volume"));
      grVolume_.setAxeY(new EGAxeVertical(DodicoLib.getS("Volume"), "m3"));
      getGrapheTree().add(grVolume_);
    }
  }

}
