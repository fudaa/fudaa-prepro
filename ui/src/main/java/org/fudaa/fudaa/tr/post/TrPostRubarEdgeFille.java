/*
 * @creation 12 sept. 06
 * @modification $Date: 2007-01-19 13:14:11 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuInformationsDocument;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrGrapheSimpleTimeFille;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;

public class TrPostRubarEdgeFille extends TrGrapheSimpleTimeFille {
  TrPostRubarEdgeNuaAction exportAction;

  /**
   * @param _g
   * @param _titre
   * @param _appli
   * @param _id
   */
  public TrPostRubarEdgeFille(final EGGraphe _g, final String _titre, final FudaaCommonImplementation _appli,
                              final BuInformationsDocument _id, TrPostRubarEdgeNuaAction exportAction) {
    super(_g, _titre, _appli, _id, new EGTableGraphePanel());
    setName("ifPostFilleEdge");
    this.exportAction = exportAction;
    this.p_.setPersonnalAction(new EbliActionInterface[]{exportAction});
  }

  @Override
  protected void fillSpecificMenu(JMenu _m) {
    super.fillSpecificMenu(_m);
    if (exportAction != null) {
      _m.add(exportAction);
    }
  }

  @Override
  protected String getMenuTitle() {
    return H2dResource.getS("Ar�tes");
  }

  @Override
  public String getShortHtmlHelp() {
    return TrPostRubarEdgeFille.getHelpForAreteFrame(impl_, getTitle());
  }

  public static void getErrorSelectedEdges(final FudaaCommonImplementation _impl) {
    _impl.error(TrResource.getS("Afficher les �volutions sur les ar�tes s�lectionn�es"), TrResource
        .getS("Aucun r�sultat disponible pour les ar�tes s�lectionn�es"));
  }

  public static void getErrorSelectedEdge(final FudaaCommonImplementation _impl) {
    _impl.error(TrResource.getS("Afficher les �volutions sur les ar�tes s�lectionn�es"), TrResource
        .getS("Aucun r�sultat disponible pour l'ar�te s�lectionn�e"));
  }

  public static String getHelpForAreteFrame(final FudaaCommonImplementation _impl, final String _title) {
    final CtuluHtmlWriter buf = new CtuluHtmlWriter();
    buf.h2Center(_title);
    buf.close(buf.para(), TrResource.getS("Affiche les �volutions sur l'ar�te s�lectionn�e"));
    buf.h3(TrResource.getS("Documents associ�s"));
    buf.addTag("lu");
    buf.close(buf.addTag("li"), _impl.buildLink(TrResource.getS("Description du composant d'affichage des courbes"),
        "common-curves"));
    buf.close(buf.addTag("li"), _impl.buildLink(TrResource.getS("Comment utiliser la vue 2D"), "post-vue2d"));
    return buf.toString();
  }
}
