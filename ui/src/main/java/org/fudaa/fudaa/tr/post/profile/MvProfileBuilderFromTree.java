/*
 * @creation 13 juin 07
 * 
 * @modification $Date: 2007-06-13 12:58:07 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGlassPaneStop;
import com.memoire.bu.BuHorizontalLayout;
import org.locationtech.jts.geom.LineString;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsCorrectionTester;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.export.MvExportChooseVarAndTime;

public class MvProfileBuilderFromTree extends MvProfileBuilder {

  final MvLineChooser lineChooser_;

  /**
   * panel qui contient les calques des isolignes
   */
  JComponent panelTreeIsoLignes;

  /**
   * panel des pas de temps et variables
   */
  MvExportChooseVarAndTime varTime_;

  public MvProfileBuilderFromTree(final MvProfileTarget _data, final CtuluUI _ui, final LineString _selected,
      final EfLineIntersectionsCorrectionTester _tester) {
    super(_data, _ui, _tester, _selected);
    lineChooser_ = new MvLineChooser(_selected,_data.getData().getGrid(),_ui);
    lineChooser_.setCreateLigneOuverte(true);

  }

  @Override
  public void close() {
    lineChooser_.close();
  }

  /**
   * @return the panelTreeIsoLignes
   */
  public JComponent getPanelTreeIsoLignes() {
    return panelTreeIsoLignes;
  }

  /**
   * @return
   * @see org.fudaa.fudaa.tr.post.profile.MvLineChooser#getSelectedLine()
   */
  public LineString getSelectedLine() {
    return lineChooser_.getSelectedLine();
  }

  /**
   * @return
   * @see org.fudaa.fudaa.tr.post.profile.MvLineChooser#getSelectedLineName()
   */
  public String getSelectedLineName() {
    return lineChooser_.getSelectedLineName();
  }

  public MvExportChooseVarAndTime getVarTime() {
    return varTime_;
  }

  protected void intersectTest(final JDialog _d, final MvExportChooseVarAndTime _var, final ProgressionInterface _prog) {
    final EfLineIntersectionsResultsI res = getDefaultRes((CtuluVariable) initVar_.getElementAt(0), _prog);
    if (res == null || res.isEmpty()) {
      showNoIntersectionFound();
    } else {
      _d.dispose();
      actBuildGroup(_prog, _var);
    }

  }

  
  
   public void buildVartime() {
	   varTime_ = createVarTimeChooser(getHelpForVarTime());
   }
  // public JTree tree_;

  /**
   * construit les panels des parametres.
   */
  @SuppressWarnings("serial")
  @Override
  public void stepOne() {
    if (panelTreeIsoLignes != null) return;

    panelTreeIsoLignes = lineChooser_.getPanel(MvResource.getS("S�lectionner la ligne � utiliser pour le profil"));
    buildVartime();

    final CtuluDialogPanel pn = new CtuluDialogPanel() {
      @Override
      public boolean isDataValid() {
        if (!CtuluLibString.isEmpty(getErrorText())) { return false; }
        MvProfileBuilderFromTree.this.selectedLine_ = lineChooser_.getSelectedLine();
        MvProfileBuilderFromTree.this.name_ = lineChooser_.getSelectedLineName();
        final JDialog d = (JDialog) SwingUtilities.getAncestorOfClass(JDialog.class, this);
        final CtuluTaskDelegate task = MvProfileBuilderFromTree.this.ui_.createTask(FudaaLib
            .getS("Construction des courbes"));
        final BuGlassPaneStop glassPaneStop = new BuGlassPaneStop();
        d.setGlassPane(glassPaneStop);
        task.start(new Runnable() {
          @Override
          public void run() {
            intersectTest(d, varTime_, task.getStateReceiver());
            glassPaneStop.setVisible(false);
            d.getRootPane().remove(glassPaneStop);
          }
        });

        return false;
      }
    };
    final ListSelectionListener listListener = new ListSelectionListener() {

      @Override
      public void valueChanged(final ListSelectionEvent _e) {
        updateError(pn, varTime_.computeErrorText());
      }

    };
    if (varTime_.getTimeSelectionModel() != null) {
      varTime_.getTimeSelectionModel().addListSelectionListener(listListener);
    }
    varTime_.getVarSelectionModel().addListSelectionListener(listListener);

    pn.setLayout(new BuHorizontalLayout(5));
    panelTreeIsoLignes.setBorder(CtuluLibSwing.createTitleBorder(CtuluLib.getS("Etape {0}:", CtuluLibString.UN)));
    varTime_.setBorder(CtuluLibSwing.createTitleBorder(CtuluLib.getS("Etape {0}:", CtuluLibString.DEUX)));
    pn.add(panelTreeIsoLignes);
    pn.add(varTime_);
    pn.setBorder(BuBorders.EMPTY3333);
    lineChooser_.addObserver(new Observer() {

      @Override
      public void update(final Observable _o, final Object _arg) {
        updateError(pn, lineChooser_.getError());

      }
    });
    final String error = lineChooser_.getError();
    updateError(pn, error);
    if (error == null) {
      updateError(pn, varTime_.getErrorText());
    }

    // -- integration du parent component dans le wizard des profils spatiaux
    // --//

    // pn.afficheModale(ui_.getParentComponent(),
    // MvResource.getS("Profils spatiaux"), varTime.getListRunnable());
    close();

  }

  protected void updateError(final CtuluDialogPanel _target, final String _err) {
    if (_err == null) {
      _target.cancelErrorText();
    } else {
      _target.setErrorText(_err);
    }
  }

public MvExportChooseVarAndTime getVarTime_() {
	return varTime_;
}

public void setVarTime_(MvExportChooseVarAndTime varTime_) {
	this.varTime_ = varTime_;
}
}
