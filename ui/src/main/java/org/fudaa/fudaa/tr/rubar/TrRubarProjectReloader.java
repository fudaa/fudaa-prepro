/*
 * @creation 26 avr. 2006
 *
 * @modification $Date: 2007-05-04 14:01:53 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.gui.CtuluAnalyzeGUI;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.rubar.io.*;
import org.fudaa.fudaa.tr.common.TrResource;

import java.io.File;

/**
 * @author fred deniger
 * @version $Id: TrRubarProjectReloader.java,v 1.16 2007-05-04 14:01:53 deniger Exp $
 */
public final class TrRubarProjectReloader {
  private TrRubarProjectReloader() {
  }

  protected static void reloadDTR(final ProgressionInterface _prog, final EfGridInterface _newGrid, final File _dir, final String _project,
                                  final TrRubarProject _proj) {
    // limnigrammes
    final RubarDTRFileFormat dtrFmt = new RubarDTRFileFormat();
    TrRubarFileStateDTR dtrState = (TrRubarFileStateDTR) _proj.fileStates_.contains(dtrFmt);
    // pas besoin de recharger les points limni
    if (dtrState != null && dtrState.isLoadedFileUpToDate()) {
      return;
    }
    final File dtrFile = dtrFmt.getFileExistFor(_dir, _project);
    // lecture du frottement
    if (dtrFile != null) {
      CtuluIOOperationSynthese s = null;
      EfGridInterface g = null;
      s = dtrFmt.read(dtrFile, _prog);
      // le maillage est modifie : il faut recharger meme si le fichier dtr n'est pas modifie
      if (_newGrid == null) {
        g = _proj.getMaillage();
      } else {
        g = _newGrid;
      }
      if (s != null && g != null && _proj.isReloadFileSyntheseOk(s)) {
        final CtuluAnalyze an = new CtuluAnalyze();
        final boolean mod = _proj.getH2dRubarParameters().getLimniMng().initLimni((H2dRubarDTRResult) s.getSource(), an);
        if (mod && dtrState != null) {
          dtrState.setModified(true);
        }
        if (an.containsWarnings()) {
          CtuluAnalyzeGUI.showDialogErrorFiltre(an, _proj.getImpl(), dtrFile.getName(), false);
        }
      }
      // a la fin
      if (dtrState == null) {
        dtrState = new TrRubarFileStateDTR(_proj.fileStates_, _proj.getH2dRubarParameters());
      }
      // on lit a chaque fois
      dtrState.setInitFile(dtrFile);
    }
  }

  /**
   * Mise a jour des ouvrages apres modif par vf2m.
   *
   * @param _prog
   * @param _isGridModified true si le fichier dat est modifie
   * @param _dir
   * @param _project
   */
  protected static void reloadOuv(final ProgressionInterface _prog, final boolean _isGridModified, final File _dir, final String _project,
                                  final TrRubarProject _proj) {
    // ouvrages
    final RubarOUVFileFormat ouvFmt = new RubarOUVFileFormat();
    TrRubarFileStateOUV ouvState = (TrRubarFileStateOUV) _proj.fileStates_.contains(ouvFmt);
    // pas besoin de recharger les points limni
    if (!_isGridModified && ouvState != null && ouvState.isLoadedFileUpToDate()) {
      return;
    }
    final File ouvFile = ouvFmt.getFileExistFor(_dir, _project);
    // lecture du frottement
    if (ouvFile != null) {
      CtuluIOOperationSynthese s = null;
      // on lit a chaque fois
      s = ouvFmt.read(ouvFile, _prog);
      // le maillage est modifie : il faut recharger meme si le fichier dtr n'est pas modifier
      if (s != null && _proj.isReloadFileSyntheseOk(s)) {
        final CtuluAnalyze an = new CtuluAnalyze();
        _proj.getH2dRubarParameters().initOuvrageMng((H2dRubarOuvrageContainer) s.getSource(), an);
        if (an.containsWarnings()) {
          CtuluAnalyzeGUI.showDialogErrorFiltre(an, _proj.getImpl(), ouvFile.getName(), false);
        }
      }
      if (ouvState == null) {
        ouvState = new TrRubarFileStateOUV(_proj.fileStates_, _proj.getH2dRubarParameters().getOuvrageMng(), _proj.getH2dRubarParameters().getBcMng());
        ouvState.setInitFile(ouvFile);
      }
    }
  }

  /**
   * Cette m�thode ecrase tout.
   */
  protected static void reloadAfterVF2MAction(final ProgressionInterface _prog, final TrRubarProject _proj) {
    // le repertoire de base
    final File dir = _proj.params_.getDirBase();
    // le nom du projet
    final String project = _proj.params_.getTitle();
    // la synthese utilisee pour tous les type de fichiers
    // //lecture du fichier dat/////
    final File dat = _proj.params_.getDATFile();
    H2dRubarGridAreteSource newGrid = null;
    final RubarDATFileFormat fmtDat = RubarDATFileFormat.getInstance();
    final TrRubarFileStateMng fileStates = _proj.fileStates_;
    TrRubarFileStateDAT stateDat = (TrRubarFileStateDAT) fileStates.contains(fmtDat);
    boolean isMaillageModifie = false;
    final H2dRubarParameters params = _proj.getH2dRubarParameters();
    if (dat.exists()) {
      boolean datState = false;
      if (stateDat == null) {
        stateDat = new TrRubarFileStateDAT(fileStates, params);
        datState = true;
        fileStates.add(stateDat);
      }
      final CtuluIOOperationSynthese s = stateDat.loadIfNeeded(dat, _prog);
      if (s != null) {
        isMaillageModifie = true;
      }
      if (_proj.isReloadFileSyntheseOk(s)) {
        newGrid = sameGrid(_proj, s, stateDat, params);
        // le fichier charge est erron� on l'oubli
      } else if (datState) {
        params.getBcMng().removeListener(stateDat);
        fileStates.remove(stateDat);
      }
    }
    final int nbElt = newGrid == null ? _proj.getMaillage().getEltNb() : newGrid.getGrid().getEltNb();

    CtuluArrayDouble frtValues = null;
    final RubarFRTFileFormat frtFmt = RubarFRTFileFormat.getInstance();
    final File frtFile = frtFmt.getFileFor(dir, project);
    TrRubarFileStateFRT frtState = (TrRubarFileStateFRT) fileStates.contains(frtFmt);
    // lecture du frottement
    if (frtFile.exists()) {
      if (frtState == null) {
        frtState = new TrRubarFileStateFRT(fileStates, params);
      }
      final CtuluIOOperationSynthese s = frtState.loadIfNeeded(frtFile, _prog, nbElt);
      if (_proj.isReloadFileSyntheseOk(s)) {
        frtValues = getFrtDifValues(nbElt, s);
      }
    }
    CtuluArrayDouble difValues = null;
    final RubarDIFFileFormat difFmt = RubarDIFFileFormat.getInstance();
    final File difFile = difFmt.getFileFor(dir, project);
    TrRubarFileStateDIF difState = (TrRubarFileStateDIF) fileStates.contains(difFmt);
    // lecture diffusion
    if (difFile.exists()) {
      if (difState == null) {
        difState = new TrRubarFileStateDIF(fileStates, params);
      }
      final CtuluIOOperationSynthese s = difState.loadIfNeeded(difFile, _prog, nbElt);
      if (_proj.isReloadFileSyntheseOk(s)) {
        difValues = getFrtDifValues(nbElt, s);
      }
    }
    H2DRubarSolutionsInitialesInterface cinValues = null;
    final File cinFile = RubarCINFileFormat.getInstance().getFileFor(dir, project);
    final RubarCINFileFormat cinFmt = RubarCINFileFormat.getInstance();
    TrRubarFileStateCIN cinState = (TrRubarFileStateCIN) fileStates.contains(cinFmt);
    if (cinFile.exists()) {
      if (cinState == null) {
        cinState = new TrRubarFileStateCIN(fileStates, params);
      }

      int nbConcentrationBlocks = params.getBcMng().getNbConcentrationBlocks();
      if (!params.isTransport()) {
        nbConcentrationBlocks = -1;
      }

      final CtuluIOOperationSynthese s = cinState.loadIfNeeded(cinFile, _prog, nbElt, nbConcentrationBlocks);
      if (_proj.isReloadFileSyntheseOk(s)) {
        cinValues = getCinValues(nbElt, s);
      }
    }
    TrRubarFileStateCLI cliState = null;
    // si dat est d�fini
    if (stateDat != null) {
      cliState = (TrRubarFileStateCLI) fileStates.contains(RubarCLIFileFormat.getInstance());
      if (cliState == null) {
        cliState = new TrRubarFileStateCLI(fileStates, params);
        fileStates.add(cliState);
        // doit etre cr��
        cliState.setModified(true);
      }
      if ((fileStates.contains(RubarTARFileFormat.getInstance())) == null) {
        // on enregistre l'ecouter de modification
        new TrRubarFileStateTAR(fileStates, params);
      }
    }
    // on doit tout recharger
    final CtuluAnalyze an = new CtuluAnalyze();

    if (newGrid == null) {
      final boolean[] r = params.reinitSiFrt(cinValues, frtValues);
      cinState.setModified(r[0]);
      frtState.setModified(r[1]);
      if (difValues != null) {
        difState.setModified(params.reinitDIF(difValues));
      }
    } else {
      _proj.params_.clearAll();
      an.clear();
      final boolean[] r = params.reinitAll(newGrid, frtValues, cinValues, _prog, an);
      _proj.getImpl().manageAnalyzeAndIsFatal(an);
      if (stateDat != null) {
        stateDat.setModified(false);
      }
      cinState.setModified(r[0]);
      frtState.setModified(r[1]);
      // on sauvegarde les nouvelles cli
      if (cliState != null) {
        cliState.setModified(true);
      }
      if (params.reinitApp()) {
        CtuluLibMessage.info("APP cleared");
      }
      if (difValues != null) {
        difState.setModified(params.reinitDIF(difValues));
      }
      reloadDTR(_prog, newGrid.getGrid(), dir, project, _proj);
      reloadOuv(_prog, isMaillageModifie, dir, project, _proj);
      _proj.getTrRubarParams().initAll();
      // CLI TAR
    }

    final File par = _proj.params_.getPARFile();
    if (par.exists()) {
      loadPar(_prog, _proj, par);
    }
    updateProjAndVisu(_prog, _proj);
  }

  private static void updateProjAndVisu(final ProgressionInterface _prog, final TrRubarProject _proj) {
    if (_proj.getVisuPanel() != null) {
      _proj.getVisuPanel().areteTypeChanged();
      _proj.getVisuPanel().varToDisplayInIsoChanged();
    }
    final CtuluAnalyze ana = new CtuluAnalyze();
    _proj.getTrRubarParams().getH2dRubarParametres().getGridVolume().computeBord(_prog, ana);
    if (_proj.getImpl() != null) {
      _proj.getImpl().manageAnalyzeAndIsFatal(ana);
    }
    _proj.getCalculActions().setEnableCalcul(_proj.fileStates_.isContained(RubarDATFileFormat.getInstance()));
  }

  private static void loadPar(final ProgressionInterface _prog, final TrRubarProject _proj, final File _par) {
    final RubarPARFileFormat fmtPar = RubarPARFileFormat.getInstance();
    TrRubarFileState state = _proj.fileStates_.contains(fmtPar);
    if (state == null) {
      state = new TrRubarFileStatePAR(_proj.fileStates_, _proj.getH2dRubarParameters());
    }
    final CtuluIOOperationSynthese s = state.loadIfNeeded(_par, _prog);
    if (_proj.isReloadFileSyntheseOk(s)) {
      final DicoParams p = RubarPARFileFormat.createParams((TIntObjectHashMap) s.getSource());
      _proj.getH2dRubarParameters().reinitDicoParams(p);
      state.setModified(false);
    }
  }

  private static CtuluArrayDouble getFrtDifValues(final int _nbElt, final CtuluIOOperationSynthese _s) {
    CtuluArrayDouble frtValues = (CtuluArrayDouble) _s.getSource();

    // en cas d'erreur dans la lecture
    if (frtValues.getSize() != _nbElt) {
      CtuluLibMessage.error("RELOAD VF2M: ERROR BAD SIZE");
      frtValues = null;
    }
    return frtValues;
  }

  private static H2DRubarSolutionsInitialesInterface getCinValues(final int _nbElt, final CtuluIOOperationSynthese _s) {
    H2DRubarSolutionsInitialesInterface cinValues;
    cinValues = (H2DRubarSolutionsInitialesInterface) _s.getSource();
    if (cinValues.getCommonValueSize() != _nbElt) {
      CtuluLibMessage.error("RELOAD VF2M: ERROR CIN BAD SIZE");
      cinValues = null;
    }
    return cinValues;
  }

  private static H2dRubarGridAreteSource sameGrid(final TrRubarProject _proj, final CtuluIOOperationSynthese _s, final TrRubarFileStateDAT _stateDat,
                                                  final H2dRubarParameters _params) {
    H2dRubarGridAreteSource newGrid;
    newGrid = (H2dRubarGridAreteSource) _s.getSource();
    if (_params.getGridVolume().isSameStructure(newGrid.getRubarGrid())) {
      final String message = TrResource.getS("Le fichier dat a �t� modifi�.") + CtuluLibString.LINE_SEP
          + TrResource.getS("Le nouveau maillage est �quivalent � l'actuel.") + CtuluLibString.LINE_SEP
          + TrResource.getS("Voulez-vous conserver les anciennes donn�es (conditions limites) ?");
      if (_proj.getImpl().question(TrResource.getS("Fichier DAT"), message)) {
        CtuluLibMessage.info("RELOAD VF2M: initialize grid nodes only");
        _params.initPt(newGrid.getRubarGrid());
        // nouveau maillage oublie
        newGrid = null;
        _stateDat.setModified(true);
      }
    }
    return newGrid;
  }
}
