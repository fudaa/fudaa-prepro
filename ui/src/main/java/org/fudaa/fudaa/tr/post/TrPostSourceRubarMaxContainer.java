/**
 * @creation 17 d�c. 2004
 * @modification $Date: 2007-05-04 14:01:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import org.fudaa.ctulu.PairString;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.helper.LRUCache;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.rubar.io.RubarMaxContainer;
import org.fudaa.dodico.rubar.io.RubarSolutionSequentielReader;
import org.fudaa.fudaa.tr.post.data.TrPostExtremVisitor;

import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

/**
 * Pour les pas de temps du fichier env.
 *
 * @author Fred Deniger
 * @version $Id: TrPostSourceRubarMaxContainer.java,v 1.11 2007-05-04 14:01:51 deniger Exp $
 */
public class TrPostSourceRubarMaxContainer {
  protected static class ElementContent implements Comparable<ElementContent> {
    int idxElt;
    double u;
    double v;
    double h;

    @Override
    public int compareTo(final ElementContent _o) {
      if (_o == null) {
        return -1;
      }
      if (_o == this) {
        return 0;
      }
      return idxElt - _o.idxElt;
    }

    @Override
    public boolean equals(final Object _obj) {
      if (_obj == null) {
        return false;
      }
      if (_obj.getClass().equals(ElementContent.class)) {
        return compareTo((ElementContent) _obj) == 0;
      }
      return false;
    }

    @Override
    public int hashCode() {
      return idxElt;
    }

    protected double getTPSValue(final int _i) {
      if (_i == 0) {
        return h;
      }
      if (_i == 1) {
        return u;
      }
      if (_i == 2) {
        return v;
      }
      throw new IllegalArgumentException();
    }
  }

  int indexInfInInitFile;
  private double timeInf;
  private double timeSup;
  private double tMax;
  private ElementContent[] elementContents;
  private static Map<PairString, SoftReference<double[][]>> valuesCached = Collections.synchronizedMap(new LRUCache<PairString, SoftReference<double[][]>>(20));

  protected static void clearLoadingCache() {
    valuesCached.clear();
  }

  protected double extrapol(final int _var, final RubarSolutionSequentielReader _reader, final int _ie)
    throws IOException {
    if (outOfBound_) {
      return readValue(_reader, _var, indexInfInInitFile, _ie);
    }
    final double v1 = readValue(_reader, _var, indexInfInInitFile, _ie);
    final double v2 = readValue(_reader, _var, indexInfInInitFile + 1, _ie);
    return extrapol(v1, v2);
  }

  private double readValue(RubarSolutionSequentielReader reader, int varId, int timeStep, int meshOrPtIdx) throws IOException {
    if (reader.getFile() == null) {
      return reader.read(varId, timeStep, meshOrPtIdx);
    }
    PairString key = new PairString(reader.getFile().getAbsolutePath(), Integer.toString(timeStep));
    final SoftReference<double[][]> softReference = valuesCached.get(key);
    if (softReference != null && softReference.get() != null) {
      double[][] data = softReference.get();
      return data[varId][meshOrPtIdx];
    }
    double[][] allValues = new double[reader.getNbVar()][reader.getNbElt()];
    reader.read(timeStep, allValues);
    valuesCached.put(key, new SoftReference<>(allValues));
    return allValues[varId][meshOrPtIdx];
  }

  protected double extrapol(final double _valTInf, final double _valTSup) {
    return _valTSup - (timeSup - tMax) * (_valTSup - _valTInf) / (timeSup - timeInf);
  }

  public int indexOfContent(final int _ie) {
    int lowIndex = 0;
    int highIndex = elementContents.length - 1;
    int temp, tempMid;
    while (lowIndex <= highIndex) {
      tempMid = (lowIndex + highIndex) >> 1;
      temp = elementContents[tempMid].idxElt - _ie;
      if (temp < 0) {
        lowIndex = tempMid + 1;
      } else if (temp > 0) {
        highIndex = tempMid - 1;
      } else {
        return tempMid;
      }
    }
    return -1;
  }

  public int getNbContent() {
    return elementContents.length;
  }

  public ElementContent getContentAt(final int _idx) {
    return elementContents[_idx];
  }

  protected ElementContent getContent(final int _ie) {
    final int i = indexOfContent(_ie);
    if (i >= 0) {
      if (elementContents[i].idxElt != _ie) {
        FuLog.warning(new Throwable());
      }
      return elementContents[i];
    }
    return null;
  }

  protected double getValueTPS(final int _varIdx, final int _idxEle, final RubarSolutionSequentielReader _tpsReader)
    throws IOException {
    final ElementContent c = getContent(_idxEle);
    if (c != null) {
      return c.getTPSValue(_varIdx);
    }
    return extrapol(_varIdx, _tpsReader, _idxEle);
  }

  protected double getValueTPC(final int _varIdx, final int _idxEle, final RubarSolutionSequentielReader _tpcReader)
    throws IOException {
    return extrapol(_varIdx, _tpcReader, _idxEle);
  }

  protected double getValueZFN(final int _idxPt, final RubarSolutionSequentielReader zfnReader) throws IOException {
    return extrapol(0, zfnReader, _idxPt);
  }

  protected double getValueZFNForElement(EfElement efElement, final RubarSolutionSequentielReader zfnReader) throws IOException {
    double value = 0;
    final int ptNb = efElement.getPtNb();
    for (int idxPt = 0; idxPt < ptNb; idxPt++) {
      value += getValueZFN(efElement.getPtIndex(idxPt), zfnReader);
    }
    return value / ((double) ptNb);
  }

  protected void getValuesinTPC(final double[][] _d, final RubarSolutionSequentielReader reader) throws IOException {
    if (outOfBound_) {
      reader.read(indexInfInInitFile, _d);
    } else {
      // on enregistre la valeur a tInf
      reader.read(indexInfInInitFile, _d);
      // tableau temp pour les valeur a t+1
      final double[][] d2 = new double[_d.length][_d[0].length];
      // les valeurs a tInf+1
      reader.read(indexInfInInitFile + 1, d2);
      for (int i = _d.length - 1; i >= 0; i--) {
        for (int j = _d[i].length - 1; j >= 0; j--) {
          _d[i][j] = extrapol(_d[i][j], d2[i][j]);
        }
      }
    }
  }

  protected void getValueTPS(final double[][] _d, final RubarSolutionSequentielReader _tpsReader) throws IOException {
    // valeur hors des limites : pas besoin d'extrapoler
    if (outOfBound_) {
      _tpsReader.read(indexInfInInitFile, _d);
      for (int i = _d.length - 1; i >= 0; i--) {
        for (int j = _d[i].length - 1; j >= 0; j--) {
          final ElementContent c = getContent(j);
          if (c != null) {
            _d[i][j] = c.getTPSValue(i);
          }
        }
      }
      return;
    }
    // on enregistre la valeur a tInf
    _tpsReader.read(indexInfInInitFile, _d);
    final double[][] d2 = new double[_d.length][_d[0].length];
    // les valeurs a tInf+1
    _tpsReader.read(indexInfInInitFile + 1, d2);
    for (int i = _d.length - 1; i >= 0; i--) {
      for (int j = _d[i].length - 1; j >= 0; j--) {
        _d[i][j] = extrapol(_d[i][j], d2[i][j]);
      }
    }
    for (int i = _d.length - 1; i >= 0; i--) {
      for (int j = elementContents.length - 1; j >= 0; j--) {
        final ElementContent c = elementContents[j];
        _d[i][c.idxElt] = c.getTPSValue(i);
      }
    }
  }

  public void iterate(final TrPostExtremVisitor _visitor, final H2dVariableType _t, final int _timeIdx,
                      final TrPostSource _src) {
    try {
      for (int i = elementContents.length - 1; i >= 0; i--) {
        _visitor.accept(elementContents[i].idxElt, _src.getData(_t, _timeIdx, elementContents[i].idxElt));
      }
    } catch (final IOException e) {
      FuLog.warning(e);
    }
  }

  protected void getValueZFN(final double[][] _d, final RubarSolutionSequentielReader _zfnReader) throws IOException {
    getValuesinTPC(_d, _zfnReader);
  }

  boolean outOfBound_;

  /**
   * @param _t le pas de temps de demande
   * @param _map la table ie->max values
   * @param _tpsReader le lecteur des valeurs du fichier tps
   * @param _initTimeStep les pas de temps initiaux du fichier tps
   * @return le container
   */
  public static TrPostSourceRubarMaxContainer compute(final double _t, final TIntObjectHashMap _map,
                                                      final RubarSolutionSequentielReader _tpsReader, final double[] _initTimeStep) throws IOException {
    final TrPostSourceRubarMaxContainer r = new TrPostSourceRubarMaxContainer();
    r.indexInfInInitFile = -Arrays.binarySearch(_initTimeStep, _t) - 2;
    if (r.indexInfInInitFile < 0) {
      r.indexInfInInitFile = 0;
      r.outOfBound_ = true;
      r.timeInf = -1;
      r.timeSup = -1;
    } else if (r.indexInfInInitFile >= _initTimeStep.length - 1) {
      r.indexInfInInitFile = _initTimeStep.length - 1;
      r.outOfBound_ = true;
      r.timeInf = -1;
      r.timeSup = -1;
    } else {
      r.timeInf = _initTimeStep[r.indexInfInInitFile];
      r.timeSup = _initTimeStep[r.indexInfInInitFile + 1];
    }
    r.tMax = _t;
    r.elementContents = new ElementContent[_map.size()];
    final TIntObjectIterator it = _map.iterator();
    for (int i = _map.size(); i-- > 0; ) {
      it.advance();
      final RubarMaxContainer c = (RubarMaxContainer) it.value();
      final ElementContent cont = new ElementContent();
      r.elementContents[i] = cont;
      cont.idxElt = it.key();
      if (c.isHSet()) {
        cont.h = c.getH();
      } else {
        cont.h = r.extrapol(0, _tpsReader, cont.idxElt);
      }
      if (c.isQSet()) {
        cont.u = c.getQu();
        cont.v = c.getQv();
      } else {
        cont.u = r.extrapol(1, _tpsReader, cont.idxElt);
        cont.v = r.extrapol(2, _tpsReader, cont.idxElt);
      }
    }
    Arrays.sort(r.elementContents);
    return r;
  }
}
