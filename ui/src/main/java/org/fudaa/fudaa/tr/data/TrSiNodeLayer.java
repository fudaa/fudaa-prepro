/*
 *  @creation     20 avr. 2005
 *  @modification $Date: 2007-05-04 14:01:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.fu.FuLog;
import org.fudaa.dodico.h2d.H2dSiSourceInterface;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindable;
import org.fudaa.fudaa.meshviewer.layer.MvFindActionNodeElt;
import org.fudaa.fudaa.meshviewer.layer.MvNodeLayer;

/**
 * @author Fred Deniger
 * @version $Id: TrSiNodeLayer.java,v 1.9 2007-05-04 14:01:51 deniger Exp $
 */
public class TrSiNodeLayer extends MvNodeLayer implements TrSiLayer {

  /**
   * @param _modele
   */
  public TrSiNodeLayer(final TrSiNodeModel _modele) {
    super(_modele);
    super.setName(getDefaultName());
  }

  @Override
  public H2dSiSourceInterface getSource() {
    return ((TrSiNodeModel) modele_).getSource();
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return new MvFindActionNodeElt(this) {

      @Override
      public String editSelected(final EbliFindable _parent) {
        return ((TrVisuPanelEditor) _parent).editSiNodeOrElement();
      }

      @Override
      public boolean isEditableEnable(final String _searchId, final EbliFindable _parent) {
        final H2dVariableProviderInterface si = getSource().getVariableDataProvider();
        return si != null && si.getVarToModify() != null && si.getVarToModify().length > 0;
      }
    };
  }

  /**
   * Ne pas appeler.
   */
  @Override
  public void setName(final String _name) {
    FuLog.warning(new Throwable());
  }

  public final static String getDefaultName() {
    return "cqPtSi";
  }

}
