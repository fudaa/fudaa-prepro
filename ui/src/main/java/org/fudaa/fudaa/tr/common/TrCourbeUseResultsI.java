/*
 * @creation 14 mars 07
 * @modification $Date: 2007-03-19 13:28:03 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import javax.swing.JTable;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author fred deniger
 * @version $Id: TrCourbeUseResultsI.java,v 1.2 2007-03-19 13:28:03 deniger Exp $
 */
public interface TrCourbeUseResultsI {

  JTable getTableOfUse(EvolutionReguliereInterface[] _selectedRow);

  String isUsedBy(EvolutionReguliereInterface _eve);

  void selectInView(EvolutionReguliereInterface _eve);

  void validSearch();

  void setFilter(EvolutionReguliereInterface _evol);

}
