/*
 *  @file         TrModeleBordAdapter.java
 *  @creation     10 sept. 2003
 *  @modification $Date: 2006-09-19 15:07:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuTable;
import org.locationtech.jts.geom.Envelope;
import java.util.List;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dBcFrontierBlockInterface;
import org.fudaa.dodico.h2d.H2dBcManagerBlockInterface;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModeleDonnesAbstract;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * @author deniger
 * @version $Id: TrBcBoundaryBlockModelDefault.java,v 1.15 2006-09-19 15:07:28 deniger Exp $
 */
public class TrBcBoundaryBlockModelDefault extends ZModeleDonnesAbstract implements TrBcBoundaryBlockModel {

  @Override
  public List getAllBoundaryType() {
    return clMng_.getBordList();
  }

  protected H2dBcManagerBlockInterface clMng_;
  private TrInfoSenderDelegate delegate_;

  public TrBcBoundaryBlockModelDefault(final H2dBcManagerBlockInterface _clMng) {
    if (_clMng == null) { throw new IllegalArgumentException("Param is null"); }
    clMng_ = _clMng;
  }

  @Override
  public int getFrontiereIndice(final int _idxFr, final int _idxOnFr) {
    return clMng_.getGrid().getFrontiers().getFrontiereIndice(_idxFr, _idxOnFr);
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    return null;
  }

  @Override
  public boolean isValuesTableAvailable() {
    return false;
  }

  @Override
  public int getNbFrontier() {
    return clMng_.getGrid().getFrontiers().getNbFrontier();
  }

  @Override
  public int getNbLiquidFrontier() {
    return clMng_.getNbLiquideBoundaries();
  }

  @Override
  public H2dBcFrontierBlockInterface getFrontier(final int _idxFrontier) {
    return clMng_.getBlockFrontier(_idxFrontier);
  }

  @Override
  public void getPoint(final GrPoint _p, final int _idxFrontier, final int _idxOnFrontier) {
    final EfGridInterface g = clMng_.getGrid();
    final int idx = g.getFrontiers().getIdxGlobal(_idxFrontier, _idxOnFrontier);
    _p.setCoordonnees(g.getPtX(idx), g.getPtY(idx), g.getPtZ(idx));
  }

  @Override
  public GrBoite getDomaine() {
    final EfGridInterface f = clMng_.getGrid();
    final GrBoite b = new GrBoite();
    final Envelope e = new Envelope();
    f.getEnvelope(e);
    b.o_ = new GrPoint(e.getMinX(), e.getMinY(), 0);
    b.e_ = new GrPoint(e.getMaxX(), e.getMaxY(), 0);
    return b;
  }

  @Override
  public int getNombre() {
    return getNbFrontier();
  }

  @Override
  public Object getObject(final int _ind) {
    return null;
  }

  @Override
  public int getNbElementIn(final int _i) {
    return clMng_.getGrid().getFrontiers().getNbPt(_i);
  }

  @Override
  public int getNbTotalPt() {
    return clMng_.getGrid().getFrontiers().getNbTotalPt();
  }

  @Override
  public int getIdxGlobal(final int _idxFr, final int _idxOnFr) {
    return clMng_.getGrid().getFrontiers().getIdxGlobal(_idxFr, _idxOnFr);
  }

  public TrInfoSenderDelegate getDelegate() {
    return delegate_;
  }

  public void setDelegate(final TrInfoSenderDelegate _delegate) {
    delegate_ = _delegate;
  }

  @Override
  public void fillWithInfo(final InfoData _m, final ZCalqueAffichageDonneesInterface _s) {
    if (delegate_ != null) {
      delegate_.fillWithBoundaryBlockInfo(_m, _s.getLayerSelectionMulti());
    }
  }

  @Override
  public int getNbBoundaryType() {
    return clMng_.getNbBoundaryType();
  }

  @Override
  public List getUsedBoundaryType() {
    return clMng_.getUsedBoundaryType();
  }
}
