package org.fudaa.fudaa.tr.post.actions;

import java.util.Iterator;
import java.util.List;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionDuplicateLayout;
import org.fudaa.fudaa.tr.post.TrPostLayoutFille;
import org.fudaa.fudaa.tr.post.TrPostProjet;



/**
 * Action qui duplique le layout en en creant un nouveau.
 * @author Adrien Hadoux
 *
 */
public class TrPostActionDuplicateLayout extends EbliWidgetActionDuplicateLayout{

	  TrPostProjet projet_;

	  public TrPostActionDuplicateLayout(final EbliScene _scene, final TrPostProjet _projet) {
	    super(_scene);

	    projet_ = _projet;
	  }

	  /**
	   * surcharge de la methode. Creation d'un nouveau layout avec le contenu du duplicata
	   */
	  @Override
	  protected EbliScene selectionneSceneNode() {
		  EbliScene dest = null;
		  TrPostLayoutFille fenetre = projet_.createNewLayoutFrame();
		  dest = fenetre.controller_.getSceneCourante();
	      return dest;
	  }

	  public static String[] formatteFilles(final List<TrPostLayoutFille> ls) {
	    final String[] noms = new String[ls.size()];
	    int cpt = 0;
	    for (final Iterator<TrPostLayoutFille> it = ls.iterator(); it.hasNext();) {
	      noms[cpt++] = it.next().getTitle();
	    }
	    return noms;
	  }

	  // public String[] formatteLayout(TrPostLayoutPanelController _controller) {
	  // String[] noms = new String[_controller.listeScenes_.size()];
	  //    
	  // for (int cpt = 0; cpt < _controller.listeScenes_.size(); cpt++) {
	  // noms[cpt] = "Layout " + (cpt + 1);
	  // }
	  // return noms;
	  // }

	}
