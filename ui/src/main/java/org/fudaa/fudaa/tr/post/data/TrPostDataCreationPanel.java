/*
 * @creation 10 ao�t 2005
 *
 * @modification $Date: 2007-06-14 12:01:40 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import com.memoire.fu.FuSort;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.*;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;
import org.fudaa.fudaa.fdico.FDicoLib;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.*;
import org.fudaa.fudaa.tr.post.persist.TrPostSourceProjectedPersistReplay;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;

/**
 * @author Fred Deniger
 * @version $Id: TrPostDataCreationPanel.java,v 1.32 2007-06-14 12:01:40 deniger Exp $
 */
@SuppressWarnings("serial")
public class TrPostDataCreationPanel extends BuPanel implements TrPostDataListener, ActionListener {
  /**
   * @author fred deniger
   * @version $Id: TrPostDataCreationPanel.java,v 1.32 2007-06-14 12:01:40 deniger Exp $
   */
  protected static final class ProjectTitleComparator implements Comparator {
    @Override
    public int compare(final Object _o1, final Object _o2) {
      return ((TrPostSource) _o1).getTitle().compareTo(((TrPostSource) _o2).getTitle());
    }
  }

  protected final CtuluCommandManager getCmd() {
    return cmd_;
  }

  public void showExportError(final String _msg) {
    TrPostDataCreationActions.showExportError(ui_, _msg);
  }

  public void export() {
    if (getNbVar() == 0) {
      showExportError(CtuluLib.getS("Pas de variables � exporter"));
      return;
    }
    final String txt = "txt";
    final String txtName = CtuluLib.getS("Texte");
    final CtuluFileChooser ch = new CtuluFileChooser();
    ch.setAcceptAllFileFilterUsed(false);
    ch.setApproveButtonToolTipText(CtuluLib
        .getS("Si aucune extension n'est utilis�e, l'extension txt sera automatiquement utilis�e"));
    final CtuluFileChooserTestWritable tester = new CtuluFileChooserTestWritable(ui_);
    tester.setAppendExtension(true, txt);
    ch.setTester(tester);
    ch.setAcceptAllFileFilterUsed(true);
    ch.addChoosableFileFilter(new BuFileFilter(txt, txtName));
    final File f = CtuluLibFile.appendExtensionIfNeeded(FudaaGuiLib.chooseFile(ui_.getFrame(), true, ch), txt);
    if (f != null) {
      export(f);
    }
  }

  public void importer() {
    final BuFileFilter txtFilter = new BuFileFilter("txt", CtuluLib.getS("Texte"));
    final File f = FudaaGuiLib.ouvrirFileChooser(TrPostDataCreationActions.getImporterStr(), txtFilter, ui_.getFrame(),
        false, null);
    if (f != null) {
      importFile(f);
    }
  }

  CtuluCommandManager cmd_;
  final BuLabel lbGValue_;
  final TrPostDataUserVarModel model_;
  final TrPostSource src_;
  final TrPostProjet proj;
  final JTable tb_;
  final TrPostCommonImplementation ui_;

  /**
   * @param _src
   * @param _cmd
   */
  public TrPostDataCreationPanel(TrPostProjet proj, final TrPostSource _src, final CtuluCommandManager _cmd,
                                 final TrPostCommonImplementation _ui) {
    super(new BuBorderLayout());
    src_ = _src;
    this.proj = proj;
    ui_ = _ui;
    cmd_ = _cmd;
    model_ = new TrPostDataUserVarModel(this, src_.getUserCreatedVar(), false);
    final CtuluListEditorPanel edi = new CtuluListEditorPanel(model_, true, true, false, true, false) {
      @Override
      protected void buildAjouterButton() {
        btAjouter_ = buildButton(BuResource.BU.getIcon("ajouter"), TrResource.getS("Ajouter une expression"),
            "AJOUTER", TrResource.getS("Ajouter une nouvelle variable d�finie par une expression"), KeyEvent.VK_A);
      }

      @Override
      protected void addBtAdd(final BuPanel _pn) {
        super.addBtAdd(_pn);
        _pn.add(buildButton(BuResource.BU.getIcon("ajouter"), TrResource.getS("Ajouter un vecteur"), "ADD_FLECHE",
            TrResource.getS("Ajouter un vecteur d�fini par 2 variables d�j� d�finies"), 0));
        _pn.add(buildButton(BuResource.BU.getIcon("ajouter"), TrResource
            .getS("Ajouter une variable d�finie par un pas de temps"), "ADD_TIME", TrResource
            .getS("Ajouter une nouvelle variable d�finie par un pas de temps et une variable d�j� d�finie"), 0));
        _pn.add(buildButton(BuResource.BU.getIcon("ajouter"), TrResource
                .getS("Ajouter une variable d�finie sur la max, min..."), "ADD_STATS", TrResource
                .getS("Ajouter une nouvelle variable calculant le max, min, la moyenne, ... d'une variable d�j� d�finie"),
            0));
      }

      @Override
      public void actionPerformed(final ActionEvent _evt) {
        if ("ADD_TIME".equals(_evt.getActionCommand())) {
          ((TrPostDataUserVarModel) getTableModel()).actionAddCst();
        } else if ("ADD_STATS".equals(_evt.getActionCommand())) {
          ((TrPostDataUserVarModel) getTableModel()).actionAddStatistic();
        } else if ("ADD_FLECHE".equals(_evt.getActionCommand())) {
          ((TrPostDataUserVarModel) getTableModel()).actionAddFleche();
        } else {
          super.actionPerformed(_evt);
        }
      }
    };
    tb_ = edi.getTable();
    edi.setDoubleClickEdit(true);

    src_.addVariableListener(this);
    // La gestion de la constane gravit�
    final BuPanel pn = new BuPanel(new BuGridLayout(3, 5, 5, false, false, false, false));
    pn.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BuBorders.EMPTY3333));
    pn.add(new BuLabel(H2dResource.getS("Acc�l�ration de la pesanteur")));
    lbGValue_ = new BuLabel();
    pn.add(lbGValue_);
    initLabelGravity();
    BuButton bt = new BuButton(BuResource.BU.getString("Editer"));
    bt.setActionCommand("MODIFY_GRAVITY");
    bt.addActionListener(this);
    pn.add(bt);

    src_.getGravity().addObserver(new Observer() {
      @Override
      public void update(final Observable _o, final Object _arg) {
        initLabelGravity();
      }
    });
    final BuPanel pnImpExp = new BuPanel(new BuButtonLayout(5, SwingConstants.CENTER));
    bt = new BuButton(TrPostDataCreationActions.getImporterStr());
    bt.setIcon(BuResource.BU.getIcon("importer"));
    bt.setActionCommand("IMPORTER");
    bt.addActionListener(this);
    pnImpExp.add(bt);
    bt = new BuButton(TrPostDataCreationActions.getExpStr());
    bt.setIcon(BuResource.BU.getIcon("exporter"));
    bt.setActionCommand("EXPORTER");
    bt.addActionListener(this);
    pnImpExp.add(bt);
    final BuPanel pnImport = new BuPanel();
    pnImport.setBorder(CtuluLibSwing.createTitleBorder(TrResource.getS("Importer des r�sultats d'autres projets"),
        BuBorders.EMPTY2222));
    pnImport.setLayout(new BuButtonLayout(2, SwingConstants.LEFT));
    bt = new BuButton(TrResource.getS("Importer des r�sultats"));
    bt.setActionCommand("IMPORTER_RESULTS");
    bt.addActionListener(this);
    pnImport.add(bt);
    bt = new BuButton(TrResource.getS("Lister les projets import�s"));
    bt.setActionCommand("LIST_IMPORT");
    bt.addActionListener(this);
    pnImport.add(bt);

    final BuPanel north = new BuPanel(new BuVerticalLayout(5));
    north.add(pnImport);
    pn.setBorder(CtuluLibSwing.createTitleBorder(CtuluLib.getS("Constantes"), BuBorders.EMPTY2222));
    north.add(pn);

    add(north, BuBorderLayout.NORTH);
    edi.setBorder(CtuluLibSwing.createTitleBorder(CtuluLib.getS("Variables"), BuBorders.EMPTY2222));
    add(edi, BuBorderLayout.CENTER);
    edi.add(pnImpExp, BuBorderLayout.SOUTH);
  }

  private void changeGravity() {
    final BuTextField tfG = BuTextField.createDoubleField();
    final Double d = (Double) src_.getGravity().getValue();
    tfG.setValue(d);
    tfG.setColumns(10);
    tfG.setValueValidator(BuValueValidator.MIN(0));
    final CtuluDialogPanel pnDial = new CtuluDialogPanel() {
      @Override
      public boolean apply() {
        final Double gravity = (Double) tfG.getValue();
        if (gravity != null && !gravity.equals(src_.getGravity().getValue())) {
          final Double old = (Double) src_.getGravity().getValue();
          src_.getGravity().setValue(gravity);
          cmd_.addCmd(new CtuluCommand() {
            @Override
            public void redo() {
              src_.getGravity().setValue(gravity);
            }

            @Override
            public void undo() {
              src_.getGravity().setValue(old);
            }
          });
        }
        return true;
      }

      @Override
      public boolean isDataValid() {
        return tfG.getValueValidator().isValueValid(tfG.getValue());
      }
    };
    pnDial.setLayout(new BuGridLayout(2, 4, 5));
    pnDial.addLabel("<html>" + H2dResource.getS("Acc�l�ration de la pesanteur")
        + " (m.s<font size=\"-2\"><sup>-2</sup></font> )" + "</html>");
    pnDial.add(tfG);
    pnDial.addLabel(TrResource.getS("Utilis�e pour calculer le nombre de froude"));
    pnDial.afficheModale(ui_.getParentComponent(), H2dResource.getS("Acc�l�ration de la pesanteur"));
  }

  public void importerVars(final TrPostSource _dest) {
    final CtuluTaskDelegate task = ui_.createTask(BuResource.BU.getString("Importer"));
    final ProgressionInterface prog = task.getStateReceiver();
    final Runnable r = new Runnable() {
      @Override
      public void run() {
        File file = ui_.ouvrirFileChooser(TrResource.getS("Fichier post"), null);
        TrPostProjet trPostProjet = TrPostDataCreationPanel.this.proj;
        TrPostSource toProject = trPostProjet.getSources().findSource(file);
        if (toProject == null) {
          toProject = TrPostSourceBuilder.activeSourceAction(file, ui_, prog);
          trPostProjet.addSource(toProject);
        }
        if (toProject == null) {
          return;
        }

        final TrPostSource toProjectFinal = toProject;
        BuLib.invokeLater(new Runnable() {
          @Override
          public void run() {
            activeImport(_dest, toProjectFinal, EfLib.isDiffQuick(src_.getGrid(), toProjectFinal.getGrid()));
          }
        });
      }
    };
    task.start(r);
  }

  protected void addVar(final Map _varString, final ProgressionInterface _prog, final FudaaCommonImplementation _impl) {
    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {
        final CtuluAnalyze analyze = new CtuluAnalyze();
        int idx = 0;
        final Object[][] vals = new Object[_varString.size()][2];
        for (final Iterator it = _varString.entrySet().iterator(); it.hasNext(); ) {
          final Entry e = (Entry) it.next();
          vals[idx][0] = e.getKey();
          vals[idx++][1] = e.getValue();
        }
        final BuTableSortModel model = new BuTableSortModel(new BuTableStaticModel(vals, new String[]{
            CtuluLib.getS("Variables"), CtuluLib.getS("Expressions")}));
        final BuTable table = new CtuluTable();
        table.setModel(model);
        table.setColumnSelectionAllowed(false);
        table.setRowSelectionAllowed(true);
        final CtuluDialogPanel pn = new CtuluDialogPanel() {
          @Override
          public boolean isDataValid() {
            if (table.getSelectedRowCount() == 0) {
              setErrorText(TrResource.getS("Aucune variable s�lectionn�e"));
              return false;
            }
            setErrorText(null);
            return true;
          }

          ;
        };
        pn.setLayout(new BuBorderLayout(1, 6));
        pn.add(new BuLabel(CtuluLib.getS("S�lectionner les variables � importer:")), BuBorderLayout.NORTH);
        pn.add(new BuScrollPane(table), BuBorderLayout.CENTER);
        final BuPanel pnBt = new BuPanel(new BuButtonLayout(2, SwingConstants.CENTER));
        final BuButton bt = new BuButton(BuResource.BU.getString("Tout s�lectionner"));
        pnBt.add(bt);
        bt.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent _e) {
            table.setRowSelectionInterval(0, model.getRowCount() - 1);
          }
        });
        pn.add(pnBt, BuBorderLayout.SOUTH);
        Map newMap = _varString;
        table.selectAll();
        if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_impl.getFrame(), TrPostDataCreationActions.getImporterStr()))) {
          final int nbSelected = table.getSelectedRowCount();
          if (nbSelected == 0) {
            return;
          }
          // des variables ne seront pas import�es
          if (nbSelected < table.getRowCount()) {
            newMap = new HashMap(table.getSelectedRowCount());
            final int[] rows = table.getSelectedRows();
            for (int i = rows.length - 1; i >= 0; i--) {
              final H2dVariableTypeCreated cr = (H2dVariableTypeCreated) table.getValueAt(rows[i], 0);
              newMap.put(cr, _varString.get(cr));
            }
          }
          TrPostUserVariableSaver.addNormalUserVar(newMap, src_, analyze, cmd_);
          _impl.manageAnalyzeAndIsFatal(analyze);
        }
      }
    });
  }

  protected BuInternalFrame createInternalFrame() {
    return new TrPostDataCreationFille(this);
  }

  final void initLabelGravity() {
    lbGValue_.setText(src_.getGravity().getValue().toString());
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final String com = _e.getActionCommand();
    if ("MODIFY_GRAVITY".equals(com)) {
      changeGravity();
    } else if ("IMPORTER".equals(com)) {
      importer();
    } else if ("EXPORTER".equals(com)) {
      export();
    } else if ("LIST_IMPORT".equals(com)) {
      listImports();
    } else if ("IMPORTER_RESULTS".equals(com)) {
      importerVars(src_);
    }
  }

  private void listImports() {
    final H2dVariableType[] created = src_.getUserCreatedVar();

    boolean isImport = created != null && created.length > 0;
    if (isImport && created != null) {
      final Map projectVar = new HashMap();
      for (int i = 0; i < created.length; i++) {
        final TrPostDataCreated createdData = src_.getUserCreatedVar(created[i]);
        if (createdData instanceof TrPostDataCreatedImport) {
          final TrPostDataCreatedImport res = (TrPostDataCreatedImport) createdData;
          List l = (List) projectVar.get(res.imported_);
          if (l == null) {
            l = new ArrayList();
            projectVar.put(res.imported_, l);
          }
          l.add(created[i]);
        }
      }
      final int nb = projectVar.size();
      isImport = nb > 0;
      if (isImport) {
        final TrPostSource[] src = (TrPostSource[]) projectVar.keySet().toArray(new TrPostSource[projectVar.size()]);
        Arrays.sort(src, new ProjectTitleComparator());
        final Boolean[] delete = new Boolean[nb];
        Arrays.fill(delete, Boolean.FALSE);
        final String[] title = new String[nb];
        final String[] path = new String[nb];
        final String[] vars = new String[nb];
        final String[] varsFirst = new String[nb];
        for (int i = 0; i < nb; i++) {
          title[i] = src[i].getTitle();
          path[i] = src[i].getFiles().iterator().next().getAbsolutePath();
          final Object[] varsUsed = ((List) projectVar.get(src[i])).toArray();
          vars[i] = CtuluLibString.arrayToHtmlString(varsUsed);
          varsFirst[i] = varsUsed[0].toString() + "...";
        }
        final AbstractTableModel model = new TrPostImportProjetTableModel(varsFirst, delete, path, title);
        final CtuluDialogPanel pn = new CtuluDialogPanel();
        pn.setLayout(new BuBorderLayout(5, 5));
        final BuLabel lb = new BuLabel(TrResource.getS("Les projets import�s"));
        lb.setHorizontalAlignment(SwingConstants.CENTER);
        lb.setHorizontalTextPosition(SwingConstants.CENTER);
        pn.add(lb, BuBorderLayout.NORTH);
        final CtuluTable table = new CtuluTable(model);
        final TableColumnModel colModel = table.getColumnModel();
        colModel.getColumn(0).setCellEditor(new CtuluCellBooleanEditor());
        colModel.getColumn(0).setCellRenderer(new CtuluCellBooleanRenderer());
        colModel.getColumn(3).setCellRenderer(new CtuluCellTextRenderer() {
          @Override
          public java.awt.Component getTableCellRendererComponent(final JTable _table, final Object _value,
                                                                  final boolean _isSelected, final boolean _hasFocus, final int _row, final int _column) {
            final Component c = super.getTableCellRendererComponent(_table, _value, _isSelected, _hasFocus, _row,
                _column);
            setToolTipText(vars[_row]);
            return c;
          }
        });
        pn.add(new BuScrollPane(table));
        if (pn.afficheModaleOk(TrPostDataCreationPanel.this, TrResource.getS("Les projets import�s"))) {
          final List varToRemove = new ArrayList();
          for (int i = 0; i < nb; i++) {
            if (delete[i].booleanValue()) {
              varToRemove.addAll((List) projectVar.get(src[i]));
            }
          }
          src_.removeUserVar((H2dVariableTypeCreated[]) varToRemove.toArray(new H2dVariableTypeCreated[varToRemove
              .size()]), cmd_);
        }

        return;
      }
    }
    CtuluLibDialog.showMessage(this, TrResource.getS("Les projets import�s"), TrResource.getS("Aucun projet import�"));
  }

  @Override
  public void dataAdded(final boolean _isFleche) {
    if (FuLog.isDebug()) {
      FuLog.debug("TRP: variable added event received");
    }
    model_.restore();
  }

  @Override
  public void dataChanged(final H2dVariableType _old, final H2dVariableType _new, final boolean _contentChanged,
                          final boolean _isFleche, final Set _varDepending) {
    model_.restore();
  }

  @Override
  public void dataRemoved(final H2dVariableType[] _vars, final boolean _isFleche) {
    model_.restore();
  }

  public void export(final File _dest) {
    int[] rows = tb_.getSelectedRows();
    boolean all = false;
    if (rows == null || rows.length == 0) {
      if (!ui_.question(TrPostDataCreationActions.getExpStr(), TrResource
          .getS("Voulez-vous exporter toutes les variables ?"))) {
        return;
      }
      all = true;
    } else {
      final String[] choices = new String[]{FSigLib.getS("Les variables s�lectionn�es"),
          FSigLib.getS("Toutes les variables"), BuResource.BU.getString("Annuler")};
      final CtuluOptionPane p = new CtuluOptionPane(TrResource.getS("Voulez-vous exporter:"),
          JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_CANCEL_OPTION, null, choices, choices[0]);
      CtuluOptionPane.showDialog(ui_.getFrame(), TrResource.getS("Exporter"), p);
      final Object val = p.getValue();
      if (val == null || choices[2].equals(val)) {
        return;
      }
      all = choices[1].equals(val);
    }
    if (all) {
      rows = new int[getNbVar()];
      for (int i = rows.length - 1; i >= 0; i--) {
        rows[i] = i;
      }
    }
    if (rows == null || rows.length == 0) {
      return;
    }
    final List cr = new ArrayList(rows.length);
    final List expr = new ArrayList(rows.length);
    final int nb = rows.length;
    for (int i = 0; i < nb; i++) {
      final int rowIdx = rows[i];
      if (model_.isExpr(rowIdx)) {
        cr.add(model_.getVariable(rowIdx));
        expr.add(model_.getFormule(rowIdx));
      }
    }
    TrPostDataCreationActions.export(_dest, (H2dVariableTypeCreated[]) cr
        .toArray(new H2dVariableTypeCreated[cr.size()]), (String[]) expr.toArray(new String[expr.size()]), ui_);
  }

  public int getNbVar() {
    return model_.getRowCount();
  }

  public void importFile(final File _file) {
    if (_file == null || !_file.exists()) {
      ui_.error(TrPostDataCreationActions.getImporterStr(), CtuluLib.getS("Le fichier {0} n'existe pas", _file == null ? "?"
          : _file.getName()), false);
    }
    new CtuluTaskOperationGUI(ui_, TrPostDataCreationActions.getImporterStr() + CtuluLibString.ESPACE
        + (_file == null ? "?" : _file.getName())) {
      @Override
      public void act() {
        final Map varString = new HashMap();
        final CtuluAnalyze analyze = new CtuluAnalyze();
        final ProgressionInterface prog = ui_.createProgressionInterface(this);
        TrPostDataCreationActions.importAct(_file, varString, analyze, prog);
        addVar(varString, prog, ui_);
        if (varString.size() > 0) {
          BuLib.invokeLater(new Runnable() {
            @Override
            public void run() {
              ui_.manageAnalyzeAndIsFatal(analyze);
            }
          });
        }
      }

      ;
    }.start();
  }

  public static void activeVarFrame(final TrPostProjet projet, final TrPostSource _target,
                                    final TrPostCommonImplementation _impl) {
    final Runnable r = new Runnable() {
      @Override
      public void run() {

        final CtuluCommandManager cmd = new CtuluCommandManager();

        final JPanel pn = new TrPostDataCreationPanel(projet, _target, cmd, _impl);
        final JDialog dial = CtuluLibSwing.createDialogOnActiveWindow(TrLib.getString("G�rer les variables"));
        dial.setContentPane(pn);
        final JMenu tb = new JMenu(TrLib.getString("Edition"));
        final JMenuBar bar = new JMenuBar();
        bar.add(tb);
        dial.setJMenuBar(bar);
        dial.pack();
        dial.setLocationRelativeTo(_impl.getFrame());
        dial.setModal(true);
        dial.setVisible(true);
      }
    };
    BuLib.invokeNow(r);
  }

  protected void activeImport(final TrPostSource _ref, final TrPostSource _toProject, final String _quickDiff) {
    final TrPostProjectCompPanel pn = new TrPostProjectCompPanel(_ref.getGrid(), _toProject.getGrid(), _quickDiff, ui_);
    final TrPostProjetCompTimeStepPanel timeStep = new TrPostProjetCompTimeStepPanel(null, _toProject);
    pn.add(timeStep.getPn());

    String f = _toProject.getTitle();
    Collection<File> files = _toProject.getFiles();
    if (CtuluLibArray.isNotEmpty(files)) {
      f = files.iterator().next().getName();
    }
    String title = null;
    if (f != null) {
      title = '_' + f;
    }

    List<H2dVariableType> varToImport = new ArrayList<>(Arrays.asList(_toProject.getAvailableVar()));
    H2dVariableType[] userCreatedVar = _toProject.getUserCreatedVar();
    if (userCreatedVar != null) {
      varToImport.removeAll(Arrays.asList(userCreatedVar));
    }
    final H2dVariableType[] varsToImport = varToImport.toArray(new H2dVariableType[varToImport.size()]);
    FuSort.sort(varsToImport);
    pn.addImportPanel(varsToImport, title, _ref
        .getAvailableVar());
    final CtuluDialog dial = pn.createDialog(ui_.getFrame());
    pn.setParent(dial);
    dial.setTitle(FDicoLib.getS("Importer"));
    if (dial.afficheAndIsOk()) {
      final H2dVariableType[][] newVarOldVarInImported = pn.createVarToImport();
      if (newVarOldVarInImported == null || newVarOldVarInImported.length == 0 || newVarOldVarInImported[0] == null
          || newVarOldVarInImported[0].length == 0) {
        return;
      }
      final CtuluTaskDelegate task = ui_.createTask(dial.getTitle());
      final TrPostSource toProject = TrPostSourceComparatorBuilder.createSrcForSelectedTime(_toProject, ui_, timeStep
          .useOneTimeStepImported(), timeStep.getImportedTimeStepToUse());
      if (toProject != _toProject) {
        proj.getSources().addSourceBuilt((TrPostSourceBuilt) toProject);
      }
      task.start(new Runnable() {
        @Override
        public void run() {
          TrPostDataCreationPanel.importVarAction(proj, _ref, newVarOldVarInImported, toProject, pn.isGridEquals(),
              task.getStateReceiver(), getCmd());
        }
      });
    }
  }

  protected static void importVarAction(final TrPostProjet _impl, final TrPostSource _dest,
                                        final H2dVariableType[][] newVarOldVarInImported, final TrPostSource _toProject, final boolean _sameGrid,
                                        final ProgressionInterface _prog, final CtuluCommandContainer _cmd) {
    _toProject.buildDefaultVarUpdateLists();
    final TrPostSourceProjected pro = new TrPostSourceProjected(_toProject, _dest.getGrid(), _dest.getTime()
        .getInitTimeSteps(), _sameGrid, _impl.getImpl(), new TrPostSourceProjectedPersistReplay(
        _toProject.getId(), _dest.getId(), _sameGrid));

    pro.openDatas(_prog, new CtuluAnalyze(), _impl.getImpl());
    _impl.getSources().addSourceBuilt(pro);
    pro.buildDefaultVectors();
    final H2dVariableTypeCreated[] vars = new H2dVariableTypeCreated[newVarOldVarInImported[0].length];
    System.arraycopy(newVarOldVarInImported[0], 0, vars, 0, vars.length);
    final TrPostDataCreatedImport[] datas = new TrPostDataCreatedImport[vars.length];
    for (int i = datas.length - 1; i >= 0; i--) {
      datas[i] = new TrPostDataCreatedImport(pro, newVarOldVarInImported[1][i]);
    }
    _dest.addUserVar(vars, datas, _cmd);
  }
}
