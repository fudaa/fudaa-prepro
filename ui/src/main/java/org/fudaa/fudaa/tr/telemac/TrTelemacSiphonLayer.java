/**
 * @creation 1 d�c. 2004
 * @modification $Date: 2007-05-04 14:01:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSourceMng;
import org.fudaa.dodico.h2d.telemac.H2dtelemacSiponListener;
import org.fudaa.ebli.calque.ZCalqueSegment;
import org.fudaa.ebli.calque.ZSelectionTrace;
import org.fudaa.ebli.commun.EbliActionChangeState;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrVisuPanel;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LinearRing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacSiphonLayer.java,v 1.22 2007-05-04 14:01:46 deniger Exp $
 */
public final class TrTelemacSiphonLayer extends ZCalqueSegment implements H2dtelemacSiponListener {
  @Override
  public void culvertAdded() {
    // inutile de remettre a jour l'icone
    repaint();
  }

  @Override
  public void culvertRemoved() {
    // on remet a jour l'icone
    if (getM().getNombre() == 0) {
      firePropertyChange("iconeChanged", true, false);
    }
    repaint();
  }

  @Override
  public void culvertValueChanged(final boolean _connexionChanged) {
    if (_connexionChanged) {
      repaint();
    }
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return null;
  }

  /**
   * Utilise uniquement pour remettre a jour l'icone dans l'arbre des calques.
   */
  @Override
  public void sourcesChanged(final DicoEntite _keyword) {
    if (getM().getMng().isCoordonnesEnt(_keyword)) {
      final int nb = getM().getMng().getNbSource();
      if (nb == 0 || nb == 1) {
        firePropertyChange("iconeChanged", true, false);
      }
    }
  }

  protected boolean isWorkOnSourcePoint() {
    return getM().isWorkOnSourcePoint();
  }

  private class AddCulvert extends EbliActionSimple {
    protected AddCulvert() {
      super(TrResource.getS("Ajouter un siphon"), null, "ADD_CULVERT");
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if (isWorkOnSourcePoint() && !isSelectionEmpty() && getLayerSelection().getNbSelectedIndex() == 2) {
        int i1 = getLayerSelection().getMinIndex();
        int i2 = getLayerSelection().getMaxIndex();
        final boolean i1Used = getMng().isSiphonUsingSource(new int[]{i1});
        final boolean i2Used = getMng().isSiphonUsingSource(new int[]{i2});
        final CtuluCommandComposite cmd = new CtuluCommandComposite();
        String txt = null;
        if (i1Used && i2Used) {
          txt = TrResource.getS("Les sources s�lectionn�es sont d�j� utilis�es.") + CtuluLibString.LINE_SEP
              + TrResource.getS("Voulez-vous les dupliquer?");
        }
        if (i1Used || i2Used) {
          txt = TrResource.getS("La source {0} est d�j� utilis�e.", CtuluLibString.getString((i1Used ? i1 : i2) + 1)) + CtuluLibString.LINE_SEP
              + TrResource.getS("Voulez-vous la dupliquer?");
        }
        if (txt != null) {
          if (!TrTelemacSiphonLayer.this.parent_.getImpl().question(BuResource.BU.getString("Dupliquer") + "?", txt)) {
            return;
          }

          if (i1Used) {
            // ajouter a la fin donc i2 reste correct
            i1 = getMng().duplicateSource(i1, cmd);
          }
          if (i2Used) {
            i2 = getMng().duplicateSource(i2, cmd);
          }
        }
        final int siphon = getMng().addSiphon(i1, i2, cmd);
        if (siphon >= 0) {
          paintSelectedSiphonIdx_ = siphon;
          TrTelemacSiphonLayer.this.repaint();
          final TrTelemacSiphonEditor editor = new TrTelemacSiphonEditor(getMng(), cmd, new int[]{siphon});
          if (CtuluDialogPanel.isOkResponse(editor.afficheModale(TrTelemacSiphonLayer.this.parent_.getFrame(), TrResource.getS("Ajouter un siphon")))) {
            TrTelemacSiphonLayer.this.parent_.getCmdMng().addCmd(cmd);
          } else {
            cmd.undo();
          }
          paintSelectedSiphonIdx_ = -1;
          TrTelemacSiphonLayer.this.repaint();
        }
      }
    }

    @Override
    public String getEnableCondition() {
      return TrResource.getS("Activer le mode \"Source\"") + getBr() + TrResource.getS("S�lectionner deux sources");
    }

    @Override
    public void updateStateBeforeShow() {
      this.setEnabled(isWorkOnSourcePoint() && !isSelectionEmpty() && getLayerSelection().getNbSelectedIndex() == 2);
    }
  }

  private class RelaxationAction extends EbliActionSimple {
    protected RelaxationAction() {
      super(TrResource.getS("Modifier la relaxation"), null, "MODIFY_RELAXATION");
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final BuTextField f = BuTextField.createDoubleField();
      final CtuluDialogPanel pn = new CtuluDialogPanel() {
        @Override
        public boolean isDataValid() {
          final String txt = f.getText().trim();
          if (txt.length() == 0) {
            return true;
          }
          final double d = Double.parseDouble(txt);
          if (d < 0) {
            f.setForeground(Color.RED);
            setErrorText(TrResource.getS("Valeur positive attendue"));
            return false;
          }
          return true;
        }
      };
      pn.setLayout(new BuGridLayout(2, 5, 5));
      String txt = null;
      if (CtuluLib.isFrenchLanguageSelected()) {
        txt = "Le coefficient de relaxation sert � imposer progressivement au d�but le d�bit\n dans le siphon, pour �viter la formation d'un vortex.";
      } else {
        txt = "The relaxation coefficient is initially used to prescribe the discharge in the culvert\n on a progressive basis, in order to avoid the formation of an eddy.";
      }
      pn.setHelpText(txt);
      pn.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
      pn.add(new BuLabel(H2dResource.getS("Coefficient de relaxation")));
      pn.add(f);
      f.setText(Double.toString(getMng().getSiphonRelaxation()));
      if (CtuluDialogPanel.isOkResponse(pn.afficheModale(TrTelemacSiphonLayer.this.parent_.getFrame(), TrResource.getS("Modifier la relaxation")))) {
        final String val = f.getText().trim();
        if (val.length() > 0) {
          getMng().modifySiphonRelaxation(Double.parseDouble(val), TrTelemacSiphonLayer.this.parent_.getCmdMng());
        }
      }
    }
  }

  private class EditAction extends EbliActionSimple {
    protected EditAction() {
      super(TrResource.getS("Editer siphon(s)"), null, "EDIT_CULVERT");
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if (isSelectionEmpty()) {
        return;
      }
      final int[] selec = TrTelemacSiphonLayer.this.getSelectedIndex();
      final TrTelemacSiphonEditor ed = new TrTelemacSiphonEditor(TrTelemacSiphonLayer.this.getMng(), TrTelemacSiphonLayer.this.parent_.getCmdMng(),
          selec);
      final String titre = selec.length == 1 ? (TrResource.getS("Edition siphon") + " " + (selec[0] + 1)) : (TrResource.getS("Edition siphons"));
      if (selec.length == 1) {
        paintSelectedSiphonIdx_ = selec[0];
        // TrTelemacSiphonLayer.this.paintSiphonIdx();
        TrTelemacSiphonLayer.this.repaint();
      }
      ed.afficheModale(parent_.getFrame(), titre);
      if (selec.length == 1) {
        paintSelectedSiphonIdx_ = -1;
        TrTelemacSiphonLayer.this.repaint();
      }
    }

    @Override
    public String getEnableCondition() {
      return TrResource.getS("D�sactiver le mode \"Source\"") + getBr() + TrResource.getS("Au moins un siphon doit �tre s�lectionn�");
    }

    @Override
    public void updateStateBeforeShow() {
      this.setEnabled(!isWorkOnSourcePoint() && !isSelectionEmpty());
    }
  }

  private class EditExtremite extends EbliActionSimple {
    protected EditExtremite() {
      super(TrResource.getS("Editer extremit�(s)"), null, "EDIT_EXTREMS");
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if (isWorkOnSourcePoint() && !isSelectionEmpty()) {
        final TIntIntHashMap map = getMng().getSiphonUsingSourceAndEnds(getSelectedIndex());
        if (map.size() > 0) {
          final TrTelemacCulvertEndEditor ed = new TrTelemacCulvertEndEditor(map, getMng(), TrTelemacSiphonLayer.this.parent_.getCmdMng());
          ed.afficheModale(TrTelemacSiphonLayer.this.parent_.getFrame(), TrResource.getS("Editer extremit�(s)"));
        }
      }
    }

    @Override
    public String getEnableCondition() {
      return TrResource.getS("Activer le mode \"Source\"") + getBr() + TrResource.getS("S�lectionner au moins une extremit� de siphon");
    }

    @Override
    public void updateStateBeforeShow() {
      if (isWorkOnSourcePoint() && !isSelectionEmpty()) {
        this.setEnabled(getMng().isSiphonUsingSource(TrTelemacSiphonLayer.this.getSelectedIndex()));
      } else {
        this.setEnabled(false);
      }
    }
  }

  private class RemoveAction extends EbliActionSimple {
    protected RemoveAction() {
      super(TrResource.getS("Enlever le(s) siphon(s)"), null, "REMOVE_CULVERT");
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if (isSelectionEmpty()) {
        return;
      }
      // on demande s'il faut enlever les sources qui vont avec
      final int[] idx = TrTelemacSiphonLayer.this.getSelectedIndex();
      String txt = null;
      // pour construire le texte qui va bien
      if (idx.length > 0) {
        txt = TrResource.getS("Voulez-vous enlever les sources utilis�es par les siphons � supprimer ?");
      } else {
        txt = TrResource.getS("Voulez-vous enlever les sources utilis�es par le siphon � supprimer ?");
      }
      // les sources doivent etre enlevees
      if (parent_.getImpl().question(TrResource.getS("Enlever les sources"), txt)) {
        getMng().removeSource(getMng().getSrcUsedByCulvert(idx), parent_.getCmdMng());
      }
      // enlever que les siphons
      else {
        getMng().removeSiphons(idx, TrTelemacSiphonLayer.this.parent_.getCmdMng());
      }
      getLayerSelection().clear();
    }

    @Override
    public void updateStateBeforeShow() {
      this.setEnabled(!isWorkOnSourcePoint() && !isSelectionEmpty());
    }

    @Override
    public String getEnableCondition() {
      return TrResource.getS("D�sactiver le mode \"Source\"") + getBr() + TrResource.getS("S�lectionner au moins un siphon");
    }
  }

  private class WorkOnSourceAction extends EbliActionChangeState {
    protected WorkOnSourceAction() {
      super(TrResource.getS("Travailler sur les sources"), null, "EDIT_CULVERT");
    }

    @Override
    public void changeAction() {
      setWorkOnSourcePoint(WorkOnSourceAction.this.isSelected());
    }
  }

  int paintSelectedSiphonIdx_ = -1;
  TrVisuPanel parent_;

  /**
   * @param _m
   */
  public TrTelemacSiphonLayer(final TrTelemacSiphonModel _m) {
    super(_m);
    _m.getMng().addListener(this);
    iconModel_ = new TraceIconModel(TraceIcon.DISQUE, 3, Color.ORANGE);
    ligneModel_.setEpaisseur(2);
    setForeground(Color.ORANGE);
  }

  @Override
  public void setForeground(final Color _v) {
    iconModel_.setCouleur(_v);
    ligneModel_.setColor(_v);
    super.setForeground(_v);
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    final int w = getIconWidth();
    final int h = getIconHeight();
    final Graphics2D g2d = (Graphics2D) _g;
    final Color old = _g.getColor();
    _g.setColor(Color.white);
    _g.fillRect(_x + 1, _y + 1, w - 1, h - 1);
    if (!isPaletteCouleurUsed_) {
      _g.setColor(getForeground());
      _g.drawRect(_x, _y, w, h);
    }
    int ray = w / 5;
    if ((h / 5) < ray) {
      ray = h / 5;
    }
    boolean fill = false;
    if (isWorkOnSourcePoint()) {
      fill = getM().getMng().getNbSource() > 0;
    } else {
      fill = getM().getMng().getNbSiphon() > 0;
    }
    // point 1
    if (fill) {
      g2d.fillOval(_x + 1, _y + 2, ray, ray);
    } else {
      g2d.drawOval(_x + 1, _y + 2, ray, ray);
    }

    // point 2
    if (fill) {
      g2d.fillOval(_x + w - 1 - ray, _y + h / 5, ray, ray);
    } else {
      g2d.drawOval(_x + w - 1 - ray, _y + h / 5, ray, ray);
    }

    // point 3
    if (fill) {
      g2d.fillOval(_x + w / 3 - ray, _y + 3 * h / 5, ray, ray);
    } else {
      g2d.drawOval(_x + w / 3 - ray, _y + 3 * h / 5, ray, ray);
    }
    // point 4
    if (fill) {
      g2d.fillOval(_x + 2 * w / 3, _y + 3 * h / 5, ray, ray);
    } else {
      g2d.drawOval(_x + 2 * w / 3, _y + 3 * h / 5, ray, ray);
    }
    if (!isWorkOnSourcePoint()) {
      // pt1->pt2
      g2d.drawLine(_x + 1 + ray / 2, _y + 2 + ray / 2, _x + w - 1 - ray + ray / 2, _y + h / 5 + ray / 2);
      // pt3->pt4
      g2d.drawLine(_x + w / 3 - ray + ray / 2, _y + 3 * h / 5 + ray / 2, _x + 2 * w / 3 + ray / 2, _y + 3 * h / 5 + ray / 2);
    }
    _g.setColor(old);
  }

  protected H2dTelemacSourceMng getMng() {
    return ((TrTelemacSiphonModel) modele_).getMng();
  }

  protected CtuluListSelection getSelectedPointForSource(final GrPoint _p, final int _tolerance) {
    final GrBoite bClip = getSourceDomaine();
    final double distanceReel = GrMorphisme.convertDistanceXY(getVersReel(), _tolerance);
    if ((!bClip.contientXY(_p)) && (bClip.distanceXY(_p) > distanceReel)) {
      return null;
    }
    final GrPoint p = new GrPoint();
    final GrBoite clipReel = getClipReel(getGraphics());
    final TrTelemacSiphonModel m = (TrTelemacSiphonModel) modele_;
    for (int i = getMng().getNbSource() - 1; i >= 0; i--) {
      m.point(p, i, true);
      if (clipReel.contientXY(p) && GrPoint.estSelectionne(p, distanceReel, _p)) {
        final CtuluListSelection r = new CtuluListSelection();
        r.setSelectionInterval(i, i);
        return r;
      }
    }
    return null;
  }

  protected GrBoite getSourceDomaine() {
    return ((TrTelemacSiphonModel) modele_).getSourceDomaine();
  }

  protected void paintSiphonIdx(final Graphics2D _g, final GrMorphisme _versEcran) {
    if (paintSelectedSiphonIdx_ >= 0) {
      modele_.segment(super.seg_, paintSelectedSiphonIdx_, true);
      seg_.autoApplique(_versEcran);
      final TraceBox b = new TraceBox();
      b.setColorFond(Color.WHITE);
      b.setHMargin(3);
      b.setHPosition(SwingConstants.RIGHT);
      b.setVPosition(SwingConstants.CENTER);
      b.paintBox(_g, (int) seg_.o_.x_ - 3, (int) seg_.o_.y_, CtuluLibString.UN);
      b.setHPosition(SwingConstants.LEFT);
      b.paintBox(_g, (int) seg_.e_.x_ + 3, (int) seg_.e_.y_, CtuluLibString.DEUX);
    }
  }

  TrTelemacSiphonModel getM() {
    return (TrTelemacSiphonModel) modele_;
  }

  /**
   * @param _p le panneau parent
   * @return les actions associ�es a ce calque
   */
  public EbliActionInterface[] getActions(final TrVisuPanel _p) {
    parent_ = _p;

    return new EbliActionInterface[]{new EditAction(), new RemoveAction(), new RelaxationAction(), null, new WorkOnSourceAction(),
        new EditExtremite(), new AddCulvert(), null, parent_.getEditor().getExportAction()};
  }

  public int[] getSelectedPtIdx() {
    return null;
  }

  public CtuluListSelection getSelectionForSource(final LinearRing _poly) {
    if (getMng().getNbSource() == 0) {
      return null;
    }

    if (modele_.getNombre() == 0 || !isVisible()) {
      return null;
    }
    final Envelope polyEnv = _poly.getEnvelopeInternal();
    final GrBoite domaineBoite = getDomaine();
    if (domaineBoite == null) {
      return null;
    }
    final Envelope domaine = new Envelope(domaineBoite.e_.x_, domaineBoite.o_.x_, domaineBoite.e_.y_, domaineBoite.o_.y_);
    // si l'envelop du polygone n'intersect pas le domaine, il n'y a pas de selection
    if (!polyEnv.intersects(domaine)) {
      return null;
    }
    final CtuluListSelection r = creeSelection();
    final GrPoint p = new GrPoint();
    final Coordinate c = new Coordinate();
    // un testeur optimis�e
    final IndexedPointInAreaLocator tester = new IndexedPointInAreaLocator(_poly);
    for (int i = getMng().getNbSource() - 1; i >= 0; i--) {
      ((TrTelemacSiphonModel) modele_).point(p, i, true);
      c.x = p.x_;
      c.y = p.y_;
      if ((polyEnv.contains(p.x_, p.y_)) && (GISLib.isInside(tester, c))) {
        r.add(i);
      }
    }
    if (r.isEmpty()) {
      return null;
    }
    return r;
  }

  public boolean isSelectionPointEmpty() {
    return true;
  }

  @Override
  protected int getNombreSegment() {
    return getM().getMng().getNbSiphon();
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel, final GrBoite _clipReel) {
    super.paintDonnees(_g, _versEcran, _versReel, _clipReel);
    paintSiphonIdx(_g, _versEcran);
  }

  @Override
  public void doPaintSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran, final GrBoite _clipReel) {
    if (isWorkOnSourcePoint()) {
      final GrBoite clip = _clipReel;
      /*
       * if (!getDomaine().intersectXY(clip)) { return; }
       */
      final GrMorphisme versEcran = _versEcran;
      Color cs = _trace.getColor();
      if (isAttenue()) {
        cs = EbliLib.getAlphaColor(attenueCouleur(cs), alpha_);
      } else if (EbliLib.isAlphaChanged(alpha_)) {
        cs = EbliLib.getAlphaColor(cs, alpha_);
      }
      final TraceIcon ic = _trace.getIcone();
      _g.setColor(cs);
      final GrPoint p = new GrPoint();
      final int nb = selection_.getMaxIndex();
      for (int i = nb; i >= 0; i--) {
        if (selection_.isSelected(i)) {
          ((TrTelemacSiphonModel) modele_).point(p, i, true);
          if (clip.contientXY(p)) {
            p.autoApplique(versEcran);
            ic.paintIconCentre(_g, p.x_, p.y_);
          }
        }
      }
    } else {
      super.paintSelection(_g, _trace, _versEcran, _clipReel);
    }
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    if (getM().getNombre() == 0) {
      return null;
    }
    if (isWorkOnSourcePoint()) {
      return getSelectedPointForSource(_pt, _tolerance);
    }

    return super.selection(_pt, _tolerance);
  }

  @Override
  public CtuluListSelection selection(final LinearRing _poly, final int _mode) {
    if (getM().getNombre() == 0) {
      return null;
    }
    if (isWorkOnSourcePoint()) {
      return getSelectionForSource(_poly);
    }
    return super.selection(_poly, _mode);
  }

  /**
   * @param _workOnSourcePoint true si on travaille sur les sources
   */
  public void setWorkOnSourcePoint(final boolean _workOnSourcePoint) {
    // si changement on fait les modifs qui vont bien
    if (getM().setWorkOnSourcePoint(_workOnSourcePoint)) {
      clearSelection();
      if (isWorkOnSourcePoint()) {
        ligneModel_.setTypeTrait(TraceLigne.TIRETE);
        iconModel_.setTaille(8);
      } else {
        ligneModel_.setTypeTrait(TraceLigne.LISSE);
        iconModel_.setTaille(4);
        iconModel_.setType(TraceIcon.DISQUE);
      }
      // pour mettre a jour l'arbre des calques
      firePropertyChange("iconeChanged", true, false);
      // pour mettre � jour le panel d'info
      fireSelectionEvent();
      repaint();
    }
  }

  public static String getBr() {
    return "<br>";
  }
}
