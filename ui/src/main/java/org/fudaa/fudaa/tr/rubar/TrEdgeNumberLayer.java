/**
 * 
 */
package org.fudaa.fudaa.tr.rubar;

import gnu.trove.TIntArrayList;
import org.fudaa.ebli.calque.ZModeleSegment;
import org.fudaa.fudaa.meshviewer.layer.MvEdgeNumberLayer;
import org.fudaa.fudaa.meshviewer.model.TrNumberFrontierLayerI;

/**
 * @author CANEL Christophe (Genesis)
 */
public class TrEdgeNumberLayer extends MvEdgeNumberLayer implements TrNumberFrontierLayerI {
  private final TrRubarAreteLayer layer_;

  public TrEdgeNumberLayer(final ZModeleSegment _m, TrRubarAreteLayer _l) {
    super(_m);

    this.layer_ = _l;
  }

  @Override
  public void setFrontiersOnly() {
    final TrRubarAreteModel model = this.layer_.getRubarModel();
    final int nombre = model.getNombre();
    final TIntArrayList aretes = new TIntArrayList();

    for (int i = 0; i < nombre; i++) {
      if (model.getRubarArete(i).isExtern()) {
        aretes.add(i);
      }
    }

    this.setItem(aretes.toNativeArray());
  }
}
