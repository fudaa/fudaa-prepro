/*
 * @creation 16 mars 07
 * @modification $Date: 2007-05-04 14:01:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.fu.FuLog;
import java.io.IOException;
import java.util.Set;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.TrPostSource;

/**
 * @author fred deniger
 * @version $Id: TrPostDataCreatedNorme3D.java,v 1.3 2007-05-04 14:01:51 deniger Exp $
 */
public class TrPostDataCreatedNorme3D extends TrPostDataCreatedDefault {

  final H2dVariableType w_;
  final H2dVariableType u_;
  final H2dVariableType v_;
  final String desc_;

  public TrPostDataCreatedNorme3D(final String _desc, final TrPostSource _src, final H2dVariableType _x,
      final H2dVariableType _y, final H2dVariableType _w) {
    super(_src);
    desc_ = _desc;
    u_ = _x;
    v_ = _y;
    w_ = _w;
  }

  @Override
  public double buildDataFor(final int _idxTime, final int _idxObject) {
    double vx;
    double vy;
    double vw;
    try {
      vx = src_.getData(u_, _idxTime, _idxObject);
      vy = src_.getData(v_, _idxTime, _idxObject);
      vw = src_.getData(w_, _idxTime, _idxObject);
    } catch (final IOException _evt) {
      FuLog.error(_evt);
      return 0;

    }
    return Math.sqrt(vx * vx + vy * vy + vw * vw);
  }

  @Override
  public EfData buildDataFor(final int _idxTime) {
    final EfData vxData = src_.getData(u_, _idxTime);
    if (vxData == null) { return null; }
    final EfData vyData = src_.getData(v_, _idxTime);
    final EfData vwData = src_.getData(w_, _idxTime);
    final double[] valeur = new double[vxData.getSize()];
    for (int i = valeur.length - 1; i >= 0; i--) {
      final double vx = vxData.getValue(i);
      final double vy = vyData.getValue(i);
      final double vw = vwData.getValue(i);
      valeur[i] = Math.sqrt(vx * vx + vy * vy + vw * vw);
    }
    return vxData.isElementData() ? (EfData) new EfDataElement(valeur) : new EfDataNode(valeur);
  }

  @Override
  public void fillWhithAllUsedVar(final Set _res) {
    _res.add(u_);
    _res.add(v_);
    _res.add(w_);

  }

  @Override
  public String getDescription() {
    return desc_;
  }

}
