/**
 * @creation 10 juin 2003
 * @modification $Date: 2007-06-11 13:08:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.*;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.calcul.CalculLauncher;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dGridDataAdapter;
import org.fudaa.dodico.h2d.H2dParameters;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.courbe.EGFilleTree;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaProjetStateInterface;
import org.fudaa.fudaa.commun.FudaaUI;
import org.fudaa.fudaa.commun.calcul.FudaaCalculAction;
import org.fudaa.fudaa.commun.courbe.FudaaGrapheTimeAnimatedVisuPanel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.impl.FudaaProjetInformationPanel;
import org.fudaa.fudaa.fdico.FDicoProjectState;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.MvSelectionNodeOrEltData;
import org.fudaa.fudaa.meshviewer.export.MvExportFactory;
import org.fudaa.fudaa.sig.FSigProjectPersistence;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;
import org.fudaa.fudaa.tr.data.TrFilleVisu;
import org.fudaa.fudaa.tr.data.TrVisuPanelEditor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.DateFormat;
import java.util.*;
import java.util.List;

/**
 * @author deniger
 * @version $Id: TrProjetCommon.java,v 1.44 2007-06-11 13:08:20 deniger Exp $
 */
public abstract class TrProjetCommon implements TrProjet, ActionListener {
  public static boolean followComputation(final CtuluUI _ui) {
    return _ui.question(getS("Suivi du calcul"), getS("Voulez-vous afficher les r�sultats en temps r�el ?")); //Do you wnat results in real time
  }

  public static final String getCurvesHelp(final FudaaCommonImplementation _impl, final String[] _links) {
    final CtuluHtmlWriter help = new CtuluHtmlWriter();
    CtuluHtmlWriter.Tag para = help.para();
    help.addi18n(getS("Cette fen�tre permet de cr�er et d'�diter des courbes.")); //This window allows to create and to edit plots
    help.nl();
    help.addi18n(_impl.buildLink(CtuluLib.getS("Voir la documentation..."), "common-curves"));
    para.close();
    para = help.para();
    help.addi18n(getS("Les courbes temporelles sont utilis�es par:")); //the temporaly curves are used by
    para.close();
    help.addTag("ul");
    for (int i = 0; i < _links.length; i++) {
      para = help.addTag("li");
      help.append(_links[i]);
      para.close();
    }
    return help.getHtml();
  }

  public static final String getS(final String _s) {
    return TrResource.getS(_s);
  }

  public static final String getS(final String _s, final String _v) {
    return TrResource.getS(_s, _v);
  }

  public static void startExport(final H2dParameters _params, final FudaaCommonImplementation _impl,
                                 final MvSelectionNodeOrEltData _selected, FSigVisuPanel pn) {
    final H2dGridDataAdapter data = _params.createGridDataAdapter();
    final H2dVariableType[] vars = data.getVar();
    final AbstractListModel var = new AbstractListModel() {
      @Override
      public Object getElementAt(final int _index) {
        return vars[_index];
      }

      @Override
      public int getSize() {
        return vars.length;
      }
    };

    boolean isInvalid = _params.isGridInvalid();
    int nbBlockConcentration = 0;
    if (data.isDefined(H2dVariableTransType.CONCENTRATION)) {
      nbBlockConcentration = 1;
    }
    if (_params instanceof H2dRubarParameters) {
      nbBlockConcentration = ((H2dRubarParameters) _params).getBcMng().getNbConcentrationBlocks();
    }
    final MvExportFactory fac = new MvExportFactory(_impl, var, null, data, _params.getVectorContainer(), pn, nbBlockConcentration);
    MvExportFactory.startExport(fac, _impl, _selected);
  }

  private BuInformationsDocument infos_;
  FDicoProjectState state_;
  protected FudaaCalculAction calculActions_;
  protected EbliActionSimple courbeAction_;
  protected EGFilleTree courbeFille_;
  protected BuInternalFrame fille_;
  protected EbliActionSimple projectAction_;
  protected TrFilleVisu visu_;
  protected EbliActionSimple visuAction_;

  /**
   * Il est conseille d'utiliser l'usine pour construire un projet.
   * We advice to use the factory to build a plan
   */
  public TrProjetCommon() {
    buildFilleActions();
  }

  protected void addDocMenuItem(final BuMenu _menu) {
    _menu.addMenuItem(getS("Modifier les informations"), "MODIFY_DOC", BuResource.BU.getIcon("document"), true)
        .addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent _e) {
            modifyDoc(getImpl().getFrame());
          }

          ;
        });
  }

  @Override
  public boolean isAlreadyUsed(final File _f) {
    if (_f == null) {
      return false;
    }
    return _f.equals(getParamsFile());
  }

  protected void buildFilleActions() {
    if (projectAction_ == null) {
      projectAction_ = new EbliActionSimple(getS("Param�tres g�n�raux"), BuResource.BU.getToolIcon("maison"),
          "MAIN_PROJECT_VIEW") {
        @Override
        public void actionPerformed(final ActionEvent _ae) {
          showGeneralFille();
        }
      };
      projectAction_.putValue(Action.SHORT_DESCRIPTION, getS("Param�tres g�n�raux"));
      projectAction_.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.ALT_MASK
          + InputEvent.CTRL_MASK));
    }
    if (visuAction_ == null) {
      visuAction_ = new EbliActionSimple(getS("Editeur 2D"), MvResource.MV.getToolIcon("maillage"), "GRID_PROJECT_VIEW") {
        @Override
        public void actionPerformed(final ActionEvent _ae) {
          showVisuFille();
        }
      };
      visuAction_.putValue(Action.SHORT_DESCRIPTION, getS("Vue du maillage"));
      visuAction_.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.ALT_MASK
          + InputEvent.CTRL_MASK));
    }
    if (courbeAction_ == null) {
      courbeAction_ = new EbliActionSimple(H2dResource.getS("Courbes temporelles"), EbliResource.EBLI
          .getToolIcon("curves"), "TIME_CURVES") {
        @Override
        public void actionPerformed(final ActionEvent _ae) {
          showGrapheFille();
        }
      };
      courbeAction_.putValue(Action.SHORT_DESCRIPTION, getS("Afficher la fen�tre des courbes temporelles"));
      // courbeAction_.setEnabled((getEvolMng() != null) && (getEvolMng().getNbEvol() > 0));
      courbeAction_.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.ALT_MASK
          + InputEvent.CTRL_MASK));
    }
  }

  protected abstract BuInternalFrame buildGeneralFille();

  protected abstract TrFilleVisu buildVisuFille();

  protected final String getHelpStringForCurveFille() {
    return getCurvesHelp(getImpl(), getLinksToCurvesRelatedDoc());
  }

  protected abstract String[] getLinksToCurvesRelatedDoc();

  protected TrProjectDispatcherListener getUIObserver() {
    return (TrProjectDispatcherListener) getTrParams().getH2dParametres().getMainListener();
  }

  @Override
  public CalculLauncher actionCalcul() {
    return null;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
  }

  @Override
  public  void active() {
    getImpl().setGlassPaneStop();
    getImpl().addMenu(getProjectMenu(), getImpl().getNbMenuInMenuBar() - 2);
    showGeneralFille();
    showVisuFille();
  }

  public void applicationPreferencesChanged() {
  }

  @Override
  public final void close() {
    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {
        final TrImplementationEditorAbstract impl = getEditorImpl();
        impl.getMainMenuBar().removeMenu(getProjectMenu());
        TrLib.closeInternalFrame(impl, fille_);
        fille_ = null;
        TrLib.closeInternalFrame(impl, visu_);
        visu_ = null;
        TrLib.closeInternalFrame(impl, courbeFille_);
        courbeFille_ = null;
      }
    });
  }

  /**
   * @return true
   */
  public final boolean containsVisuFille() {
    return true;
  }

  @Override
  public final void exportMaillage() {
    if (visu_ == null) {
      startExport(TrProjetCommon.this.getTrParams().getH2dParametres(), getImpl(), null, visu_.getTrVisuPanel());
    } else {
      ((TrVisuPanelEditor) visu_.getVisuPanel()).startExport(getImpl());
    }
  }

  @Override
  public void finishCreation(final ProgressionInterface _prog, final Properties _options) {
  }

  /**
   * @return les actions de calcul / the actions of computations
   */
  public FudaaCalculAction getCalculActions() {
    if (calculActions_ == null) {
      calculActions_ = new FudaaCalculAction(this, false);
      calculActions_.setEnableCalcul(true);
    }
    return calculActions_;
  }

  @Override
  public final String getCodeName() {
    return getID();
  }

  /**
   * @return l'action permettant d'afficher la fenetre des courbes transitoire / the action allowing to put the window of transition plots
   */
  public final EbliActionInterface getCourbeFilleAction() {
    return courbeAction_;
  }

  public abstract TrImplementationEditorAbstract getEditorImpl();

  public final TrCourbeTemporelleManager getEvolMng() {
    return getTrParams().getEvolMng();
  }

  @Override
  public String getFrameTitle() {
    final File dest = getParamsFile();
    return getCodeName() + ": " + (dest == null ? "?" : dest.getAbsolutePath());
  }

  /**
   * @return FileFormatSoftware.REFLUX_IS.name
   */
  public abstract String getID();

  /**
   * @return les infos sur le document / the news on the document
   */
  @Override
  public final BuInformationsDocument getInformationsDocument() {
    if (infos_ == null) {
      infos_ = new BuInformationsDocument();
    }
    return infos_;
  }

  @Override
  public String getLastSaveDate() {
    final File f = getParamsFile();
    return f == null ? null : DateFormat.getDateTimeInstance().format(new Date(f.lastModified()));
  }

  /**
   * @return le maillage des parametres / the mesh of parameters
   */
  public final EfGridInterface getMaillage() {
    return getTrParams().getH2dParametres().getMaillage();
  }

  @Override
  public CtuluSavable[] getSavableComponent() {
    return new CtuluSavable[]{visu_};
  }

  public File getSaveFile() {
    return TrLib.getPreSaveFile(getParamsFile());
  }

  public File getSaveFileOld() {
    return FSigProjectPersistence.getPreSaveFileOld(getParamsFile());
  }

  @Override
  public FDicoProjectState getState() {
    if (state_ == null) {
      state_ = new FDicoProjectState(getDicoParams().createState());
    }
    return state_;
  }

  @Override
  public FudaaProjetStateInterface getProjectState() {
    return getState();
  }

  /**
   * @return TrImplementation
   */
  public final FudaaUI getUI() {
    return getImpl();
  }

  @Override
  public final TrFilleVisuInterface getVisuFille() {
    return visu_;
  }

  /**
   * @return l'action permettant d'afficher la fenetre du maillage / the action allowing to put the mesh window
   */
  public final EbliActionInterface getVisuFilleAction() {
    return visuAction_;
  }

  @Override
  public void importCourbesTemporelles(final EvolutionReguliereInterface[] _r) {
    if ((getImpl() == null) || (getEvolMng() == null)) {
      return;
    }
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        showGrapheFille();
        if (courbeFille_ != null) {
          getEvolMng().add(_r, courbeFille_.getCmdMng());
        }
      }
    });
  }

  @Override
  public void initInformationsDocument(final BuInformationsDocument _info) {
    infos_ = _info;
  }

  @Override
  public boolean loadAll(final ProgressionInterface _inter) {
    return true;
  }

  /**
   * @param _f la fenetre parente / the parent window
   */
  public void modifyDoc(final Frame _f) {
    final BuInformationsDocument d = FudaaProjetInformationPanel.editInfoDoc(_f, getInformationsDocument());
    if (d != null) {
      setDocument(d);
    }
  }

  /**
   * @param _doc la nouvelle doc.
   */
  public void setDocument(final BuInformationsDocument _doc) {
    if (_doc != infos_) {
      infos_ = _doc;
    }
  }

  @Override
  public final void showGeneralFille() {
    if (fille_ == null) {
      fille_ = buildGeneralFille();
      fille_.setName("ifMain");
      fille_.setTitle(FudaaLib.getS("Param�tres g�n�raux"));
      fille_.setFrameIcon(BuResource.BU.getFrameIcon("maison"));
      getImpl().addInternalFrame(fille_);
      // TrLib.initFrameDimensionWithPref(fille_, getImpl().getMainPanel().getDesktop().getSize());
    } else if (fille_.isClosed()) {
      getImpl().addInternalFrame(fille_);
    } else {
      getImpl().activateInternalFrame(fille_);
    }
  }

  @Override
  public final void showGrapheFille() {
    if ((getImpl() == null) || (getEvolMng() == null)) {
      return;
    }
    if (this.courbeFille_ == null) {
      final CtuluTaskDelegate task = getImpl().createTask(H2dResource.getS("Courbes temporelles"));
      task.start(new Runnable() {
        @Override
        public void run() {
          courbeAction_.setEnabled(true);
          final EGGraphe graphe = getEvolMng().createGraphe();
          final List acts = new ArrayList();
          acts.add(new TrCourbeUseAction(TrProjetCommon.this.getUsedCourbeFinder(graphe.getModel())));
          acts.add(null);
          acts.addAll(Arrays.asList(TrCourbeImporter.getImportExportAction(getEvolMng(), graphe, getImpl())));
          final FudaaGrapheTimeAnimatedVisuPanel panel = new FudaaGrapheTimeAnimatedVisuPanel(graphe,
              (EbliActionInterface[]) acts.toArray(new EbliActionInterface[acts.size()]));

          courbeFille_ = new TrGrapheTreeTimeFille(panel, H2dResource.getS("Courbes temporelles"), getImpl(),
              getInformationsDocument()) {
            @Override
            public String getShortHtmlHelp() {
              return getHelpStringForCurveFille();
            }
          };
          courbeFille_.setVisible(true);
          courbeFille_.setName("ifCourbe");
          panel.getSpecificActions();
          BuLib.invokeLater(new Runnable() {
            @Override
            public void run() {
              TrLib.initFrameDimensionWithPref(courbeFille_, getImpl().getMainPanel().getDesktop().getSize());
              courbeFille_.pack();
              getImpl().addInternalFrame(courbeFille_);
              courbeFille_.getGraphe().restore();
            }
          });
        }
      });
    } else {
      getImpl().addInternalFrame(courbeFille_);
    }
  }

  @Override
  public synchronized final void showVisuFille() {
    if (visu_ == null) {
      visu_ = buildVisuFille();
      if (visu_ == null) {
        return;
      }
      getProjectMenu();
      new CtuluTaskOperationGUI(null, getS("Chargement fichiers compl�mentaires")) { //Loading of complementaries files
        @Override
        public void act() {
          if (visu_.getArbreCalqueModel() == null) {
            return;
          }
          final Runnable finish = TrLib.restoreEditorMainFille(getImpl(), getParamsFile(), visu_, getStateReceiver());
          setDesc(getS("Construction Vue 2D"));
          if (visu_ == null) {
            return;
          }
          visu_.setFrameIcon(MvResource.MV.getFrameIcon("maillage"));
          // Performance: pour charger les icones dans un thread
          // Performance to  load the icons in a threat
          visu_.getVisuPanel().getController().buildActions();
          SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

              visu_.pack();
              getImpl().addInternalFrame(visu_);
              SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                  try {
                    visu_.restaurer();
                    if (finish != null) {
                      finish.run();
                    }
                  } catch (final RuntimeException _evt) {
                  } finally {
                    getImpl().unsetGlassPaneStop();
                    visu_.getArbreCalqueModel().getObservable().addObserver(TrProjetCommon.this.getUIObserver());
                  }
                }
              });
            }
          });
        }
      }.start();
    } else if (visu_.isClosed()) {
      getImpl().addInternalFrame(visu_);
    } else {
      getImpl().activateInternalFrame(visu_);
    }
  }
}
