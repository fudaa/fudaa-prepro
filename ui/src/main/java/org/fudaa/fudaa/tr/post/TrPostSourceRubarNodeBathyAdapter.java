/*
 * @creation 16 avr. 07
 * @modification $Date: 2007-06-11 13:08:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * Pour l'exportation: permet de donner aux classes d'exportation la vue normale des donn�es bathy: d�finies aux noeuds.
 * 
 * @author fred deniger
 * @version $Id: TrPostSourceRubarNodeBathyAdapter.java,v 1.3 2007-06-11 13:08:17 deniger Exp $
 */
public class TrPostSourceRubarNodeBathyAdapter implements EfGridData {
  final TrPostSourceRubar rubar_;

  public TrPostSourceRubarNodeBathyAdapter(final TrPostSourceRubar _rubar) {
    super();
    rubar_ = _rubar;
  }

  @Override
  public EfData getData(final CtuluVariable _o, final int _timeIdx) throws IOException {
    if (_o == H2dVariableType.BATHYMETRIE) { return new EfDataNode(rubar_.getBathyValues(_timeIdx)[0]); }
    return rubar_.getData(_o, _timeIdx);
  }

  @Override
  public double getData(final CtuluVariable _o, final int _timeIdx, final int _idxObjet) throws IOException {
    if (_o == H2dVariableType.BATHYMETRIE) { return rubar_.getBathyValues(_timeIdx)[0][_idxObjet]; }
    return rubar_.getData(_o, _timeIdx, _idxObjet);
  }

  @Override
  public EfGridInterface getGrid() {
    return rubar_.getGrid();
  }

  @Override
  public boolean isDefined(final CtuluVariable _var) {
    return rubar_.isDefined(_var);
  }

  @Override
  public boolean isElementVar(final CtuluVariable _idxVar) {
    return rubar_.isElementVar(_idxVar);
  }

}
