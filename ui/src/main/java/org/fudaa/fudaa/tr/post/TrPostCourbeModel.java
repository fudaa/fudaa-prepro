/**
 * @creation 19 oct. 2004
 * @modification $Date: 2007-06-11 13:08:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISPrecision;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimePersistBuilder;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.persist.TrPostReloadParameter;
import org.fudaa.fudaa.tr.post.replay.TrReplayerTimeEvolution;

/**
 * A revoir surement. Modele contenant l'evolution temporelle.
 *
 * @author Fred Deniger
 * @version $Id: TrPostCourbeModel.java,v 1.21 2007-06-11 13:08:17 deniger Exp $
 */
public abstract class TrPostCourbeModel extends AbstractPostCourbeModel {

  public final static class Interpolated extends TrPostCourbeModel {

    TrPostInterpolatePoint ptIdx_;

    /**
     * ACHTUNG: constructeur utilis� uniquement pour la persistance des donn�es!!!
     */
    public Interpolated() {
      super();
    }

    /**
     * @param _ptIdx
     * @param _varIdx
     * @param _timeIdx
     */
    public Interpolated(final TrPostSource _src, final TrPostInterpolatePoint _ptIdx, final H2dVariableType _varIdx) {
      super(_src, _varIdx, _src.getTime().getTimeListModel().getTimesInSec());
      ptIdx_ = _ptIdx;
    }

    @Override
    public double getPtX() {
      return ptIdx_.getX();
    }

    @Override
    public boolean isValid() {
      return ptIdx_ != null && ptIdx_.idxElt_ < super.source_.getGrid().getEltNb();
    }

    @Override
    public double getPtY() {
      return ptIdx_.getY();
    }

    @Override
    protected void updateY() {
      createY();
      // pour chaque pas de temps
      for (int tIdx = 0; tIdx < y_.length; tIdx++) {
        // on recupere la valeur
        final EfData data = source_.getData(var_, tIdx);
        if (data == null) {
          FuLog.warning("TRP: data is null for " + var_);
        } else // on interpole
        {
          super.y_[tIdx] = ptIdx_.getInterpolateValue(var_, data, tIdx, source_);
        }
      }
    }

    public TrPostInterpolatePoint getInterpolatePt() {
      return ptIdx_;
    }

    @Override
    public boolean isCourbeForInterpolation(final TrPostInterpolatePoint _int) {
      return ptIdx_.equals(_int);
    }

    @Override
    public boolean isCourbeForObject(final int _idxObj) {
      return false;
    }

    @Override
    public Object getSelectionPoint() {
      return ptIdx_;
    }

    @Override
    public String getSelectionPointName() {
      CtuluNumberFormatI formatter = source_.getPrecisionModel().getFormatter();
      return "(" + formatter.format(ptIdx_.x_) + ";" + formatter.format(ptIdx_.y_) + ")";
    }

    @Override
    public void setSelectionPoint(Object point) {
      if (point != null && (point instanceof TrPostInterpolatePoint)) {
        ptIdx_ = (TrPostInterpolatePoint) point;
      }
    }

    @Override
    public String getInfosPoint() {
      if (source_.isElementVar(getVar())) {
        return TrResource.getS("El�ment") + " " + (ptIdx_.idxElt_ + 1);
      }
      return TrResource.getS("Point interpol�:") + " " + getSelectionPointName();
    }

    @Override
    public EGModel duplicate() {
      Interpolated duplic = new Interpolated(this.source_, new TrPostInterpolatePoint(this.ptIdx_), this.var_);
      if (this.yRange_ != null) {
        duplic.yRange_ = new CtuluRange(this.yRange_);
      }
      if (this.nom_ != null) {
        duplic.nom_ = nom_;
      }
      if (this.y_ != null) {
        duplic.y_ = CtuluLibArray.copy(y_);
      }

      return duplic;
    }
  }

  public final static class Node extends TrPostCourbeModel {

    int ptIdxOrElement_;

    /**
     * @param _ptIdx
     * @param _varIdx
     * @param _timeIdx
     */
    public Node(final TrPostSource _src, final int _ptIdx, final H2dVariableType _varIdx, final double[] _timeIdx) {
      super(_src, _varIdx, _timeIdx);
      ptIdxOrElement_ = _ptIdx;
    }

    /**
     * ACHTUNG: constructeur utilis� uniquement pour la persistance des donn�es!!!
     */
    public Node() {
      super();
    }

    @Override
    protected void updateY() {
      createY();
      for (int timeIdx = 0; timeIdx < y_.length; timeIdx++) {
        try {
          y_[timeIdx] = source_.getData(var_, timeIdx, ptIdxOrElement_);
        } catch (final IOException e) {
          FuLog.error(e);
        }
        // }
      }
    }

    @Override
    public double getPtX() {
      return source_.isElementVar(getVar()) ? source_.getGrid().getMoyCentreXElement(ptIdxOrElement_) : source_.getGrid()
              .getPtX(ptIdxOrElement_);
    }

    @Override
    public boolean isValid() {
      if (source_.isElementVar(getVar())) {
        return ptIdxOrElement_ < source_.getGrid().getEltNb();
      }
      return ptIdxOrElement_ < source_.getGrid().getPtsNb();
    }

    @Override
    public double getPtY() {
      return source_.isElementVar(getVar()) ? source_.getGrid().getMoyCentreYElement(ptIdxOrElement_) : source_.getGrid()
              .getPtY(ptIdxOrElement_);
    }

    public int getPtIdx() {
      return ptIdxOrElement_;
    }

    @Override
    public boolean isCourbeForInterpolation(final TrPostInterpolatePoint _int) {
      return false;
    }

    @Override
    public boolean isCourbeForObject(final int _idxObj) {
      return _idxObj == ptIdxOrElement_;
    }

    @Override
    public Object getSelectionPoint() {
      return Integer.valueOf(ptIdxOrElement_);
    }

    @Override
    public String getSelectionPointName() {
      return "" + ptIdxOrElement_;
    }

    @Override
    public void setSelectionPoint(Object point) {
      if (point != null && (point instanceof Integer)) {
        ptIdxOrElement_ = ((Integer) point).intValue();
      }
    }

    @Override
    public EGModel duplicate() {
      Node duplic = new Node(this.source_, this.ptIdxOrElement_, this.var_, CtuluLibArray.copy(time_));
      if (this.yRange_ != null) {
        duplic.yRange_ = new CtuluRange(this.yRange_);
      }
      if (this.nom_ != null) {
        duplic.nom_ = nom_;
      }
      if (this.y_ != null) {
        duplic.y_ = CtuluLibArray.copy(y_);
      }

      return duplic;
    }

    @Override
    public String getInfosPoint() {
      Coordinate coor = new Coordinate();
      boolean isElt = source_.isElementVar(getVar());
      if (isElt) {
        source_.getGrid().getMoyCentreElement(ptIdxOrElement_, coor);
      } else {
        source_.getGrid().getCoord(ptIdxOrElement_, coor);
      }
      GISPrecision precision = source_.getPrecisionModel();

      return TrResource.getS(isElt ? "El�ment" : "Point") +" "+ (ptIdxOrElement_+1) + ":(" + precision.round(coor.x) + ";"
              + precision.round(coor.y) + ")";
    }
  }

  public TrPostCourbeModel(final TrPostSource _src, final H2dVariableType _varIdx, final double[] _timeIdx) {
    super(_timeIdx);
    source_ = _src;
    var_ = _varIdx;
  }

  /**
   * ACTHUNG: constructeur utilis� uniquement pour la persistance des donn�es!!!
   */
  public TrPostCourbeModel() {
    super();
  }

  @Override
  public void fillWithInfo(InfoData _table, CtuluListSelectionInterface _selectedPt) {
    _table.put(TrResource.getS("Type"), TrLib.getString("Evolution temporelle"));
    super.fillWithInfo(_table, _selectedPt);
    _table.put(TrResource.getS("Infos Source"), this.getInfosPoint());
  }

  @Override
  public void replayData(EGGrapheModel grapheTreeModel, Map infos, CtuluUI impl) {
    if (!(impl instanceof TrPostCommonImplementation)) {
      impl.error(TrResource.getS("Impossible de r�cup�rer la bonne interface fudaa"));
      return;
    }
    TrPostCommonImplementation implementation = (TrPostCommonImplementation) impl;
    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    new TrReplayerTimeEvolution(source_, implementation).displayTimeEvolutionReplayData(this.source_, (TrPostCourbeTreeModel) grapheTreeModel, this, implementation);
  }

  @Override
  public Object savePersistSpecificDatas() {
    // -- retourne le quatuor point, pdt,variable et fichier source
    ArrayList<Object> listeData = new ArrayList<Object>();
    listeData.add(this.source_.getId());
    listeData.add(this.var_.getID());
    // -- recupere le noeud ou point interpol� --//
    listeData.add(getSelectionPoint());
    return listeData;
  }

  public abstract void setSelectionPoint(Object point);

  public abstract double getPtX();

  public abstract double getPtY();

  public abstract String getInfosPoint();

  public abstract Object getSelectionPoint();

  public abstract String getSelectionPointName();

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
    if (!(impl instanceof TrPostCommonImplementation)) {
      impl.error(TrResource.getS("Impossible de r�cup�rer la bonne interface fudaa"));
      return;
    }
    TrPostCommonImplementation implementation = (TrPostCommonImplementation) impl;
    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    new TrReplayerTimeEvolution(source_, implementation).displayTimeEvolutionOrigin(this, implementation);
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
    if (data == null || !(data instanceof ArrayList)) {
      return;
    }
    ArrayList<Object> liste = (ArrayList<Object>) data;
    String idSource = (String) liste.get(0);
    TrPostProjet projet = (TrPostProjet) infos.get(TrPostReloadParameter.POST_PROJET);
    TrPostSource src = null;
    if (projet != null) {
      // -- etape 1: recherche du source qui contient le path donn� --//
      src = projet.getSources().findSourceById(idSource);
      if (src != null) {
        this.source_ = src;
      } else {
        ((List<String>) infos.get("errorMsg")).add("Erreur, la frame graphe ne trouve pas le fichier r�sultat qui correspond � l'ID " + idSource);
        return;
      }
    } else {
      ((List<String>) infos.get("errorMsg")).add("Erreur, la frame graphe ne trouve pas le fichier r�sultat qui correspond � l'ID " + idSource);
      return;
    }
    // -- etape 2:times: ce sont les x donn�es indirectement. --//
    // -- etape 3:variable --//
    String idVar = (String) liste.get(1);
    H2dVariableType var = null;
    infos.put(FudaaCourbeTimePersistBuilder.TIME_MODEL, src.getNewTimeListModel());
    // -- recherche dans les sources de la variable par id --//
    for (int i = 0; i < src.getAvailableVar().length; i++) {
      if (src.getAvailableVar()[i].getID().equals(idVar)) {
        var = src.getAvailableVar()[i];
      }
    }
    if (var != null) {
      this.var_ = var;
    } else {
      ((List<String>) infos.get("errorMsg")).add(TrResource.TR.getString("La frame graphe ne trouve pas la variable qui correspond � l'ID {0}", idVar));
      return;
    }
    Object point = liste.get(2);
    if (point != null) {
      setSelectionPoint(point);
    }
    double[] oldY = y_;
    super.setTime(src.getTime().getTimeListModel().getTimesInSec());
    y_ = oldY;
    boolean forceRecompute = TrPostReloadParameter.isForceRecompute(infos);
    if (forceRecompute || y_ == null) {
      try {
        this.updateY();
      } catch (Exception e) {
        FuLog.error(e);
        ((List<String>) infos.get("errorMsg")).add(TrResource.TR.getString("La tentative de rejouer les donn�es pour le graphe �volution temporel {0} a �chou� avec le fichier d'id {1}", getTitle(), idVar));
      }
    }
  }
}
