/*
 *  @creation     9 ao�t 2005
 *  @modification $Date: 2007-01-17 10:44:30 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.bu.BuComboBox;
import org.fudaa.ctulu.gui.CtuluComboBoxModelAdapter;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostSource;

/**
 * Permet l'edition des variables creees par l'utilisateur.
 * 
 * @author Fred Deniger
 * @version $Id: TrPostDataEditCstPanel.java,v 1.4 2007-01-17 10:44:30 deniger Exp $
 */
@SuppressWarnings("serial")
public class TrPostDataEditCstPanel extends TrPostDataEditCommonPanel {

  final BuComboBox cbTime_;
  final BuComboBox cbVar_;
  final H2dVariableType initVar_;
  final int initTimeIdx_;

  /**
   * @param _src la source
   * @param _var la variable a modifier: si null, phase de creation.
   */
  public TrPostDataEditCstPanel(final TrPostSource _src, final H2dVariableTypeCreated _var) {
    super(_src, _var);
    cbTime_ = new BuComboBox();
    cbTime_.setModel(new CtuluComboBoxModelAdapter(_src.getNewTimeListModel()));
    cbVar_ = new BuComboBox(super.varToAdd_.toArray());
    if (_var == null) {
      initVar_ = null;
      initTimeIdx_ = -1;
    } else {
      final TrPostDataCreatedCstTime time = (TrPostDataCreatedCstTime) _src.getUserCreatedVar(_var);
      initVar_ = time.getVar();
      initTimeIdx_ = time.getTimeIdx();
      cbTime_.setSelectedIndex(initTimeIdx_);
      cbVar_.setSelectedItem(initVar_);

    }
    addLabel(TrResource.getS("Variable"));
    add(cbVar_);
    addLabel(TrResource.getS("Pas de temps"));
    add(cbTime_);

  }

  public H2dVariableType getSelectedVar() {
    return (H2dVariableType) cbVar_.getSelectedItem();
  }

  @Override
  public TrPostDataCreated createData() {
    return new TrPostDataCreatedCstTime(src_, getSelectedVar(), getSelectedIdx());
  }

  public int getSelectedIdx() {
    return cbTime_.getSelectedIndex();
  }

  /**
   * @return true si des modifications ont ete faites.
   */
  @Override
  public boolean isModified() {
    if (var_ == null || initVar_ == null) { return true; }
    return super.isVarModified() || cbTime_.getSelectedIndex() != initTimeIdx_
        || initVar_.equals(cbVar_.getSelectedItem());
  }

}
