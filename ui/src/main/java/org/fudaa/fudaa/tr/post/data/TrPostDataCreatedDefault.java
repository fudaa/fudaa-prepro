/*
 * @creation 3 mai 2005
 * 
 * @modification $Date: 2007-05-04 14:01:51 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import java.util.Comparator;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.fudaa.tr.post.TrPostFlecheContent;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id: TrPostDataCreatedDefault.java,v 1.12 2007-05-04 14:01:51 deniger Exp $
 */
public abstract class TrPostDataCreatedDefault implements TrPostDataCreated {

  public static class NameComparator implements Comparator {

    @Override
    public int compare(final Object _o1, final Object _o2) {
      if (_o1 == _o2 || CtuluLib.isEquals(_o1, _o2))
        return 0;
      if (_o1 == null) {
        return 1;
      }
      if (_o2 == null) {
        return -1;
      }
      final int i = ((TrPostDataCreated) _o1).getDescription().compareTo(((TrPostDataCreated) _o2).getDescription());
      // normalement il sont egaux
      if (i == 0) {
        return _o1.hashCode() - _o2.hashCode();
      }
      return i;
    }

  }

  public final static Comparator NAME_COMPARATOR = new NameComparator();

  @Override
  public boolean updateConstantVar(final Variable _var) {
    return false;
  }


  protected transient EfData current_;

  protected transient int currentTime_ = -1;

  protected final transient TrPostSource src_;

  public TrPostDataCreatedDefault(final TrPostSource _src) {
    super();
    src_ = _src;
  }

  public abstract EfData buildDataFor(final int _idxTime);

  public abstract double buildDataFor(final int _idxTime, final int _idxObject);

  @Override
  public void clearCache() {
    current_ = null;
    currentTime_ = -1;
  }

  @Override
  public void restore() {
  }

  @Override
  public TrPostDataCreatedSaver createSaver() {
    return null;
  }

  @Override
  public EfData getDataFor(final int _idxTime) {
    if (_idxTime != currentTime_) {
      currentTime_ = _idxTime;
      current_ = buildDataFor(_idxTime);
    }
    return current_;
  }

  @Override
  public double getValue(final int _idxTime, final int _idxObject) {
    return (_idxTime == currentTime_) ? current_.getValue(_idxObject) : buildDataFor(_idxTime, _idxObject);
  }

  @Override
  public boolean isEditable() {
    return false;
  }

  @Override
  public boolean isExpr() {
    return false;
  }

  @Override
  public TrPostFlecheContent isFleche() {
    return null;
  }
}
