/**
 *  @creation     24 mai 2004
 *  @modification $Date: 2007-06-05 09:01:14 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.common;

import com.memoire.bu.BuCommonInterface;
import java.awt.Component;
import java.awt.event.WindowListener;
import java.io.File;
import org.fudaa.fudaa.tr.TrEditorImplementation;
import org.fudaa.fudaa.tr.TrPostImplementation;

/**
 * Interface permettant de lancer des applications: devrait etre remplacee par jdistro !
 * User interface allowing to launch applications: should be replaced by jdistro
 * @author Fred Deniger
 * @version $Id: TrLauncher.java,v 1.13 2007-06-05 09:01:14 deniger Exp $
 */
public interface TrLauncher extends WindowListener {

  /**
   * @param _hydId l'identifiant de la chaine de calcul / the identifiant from the computations chain
   * @param _projectFile le fichier principal du projet hydraulique. / the principal file from hydraulics project
   */
  void ouvrirHydEditor(String _hydId, File _projectFile);

  /**
   * @param _gridFile le fichier contenant le maillage / the list containing the mesh
   */
  void createHydProjet(File _gridFile);

  /**
   * Ouvre une fenetre du superviseur ou active si deja present.
   * Open a window from the supervisor or active if already present
   */
  void openSupervisor();

  void closeAll(BuCommonInterface _app);

  /**
   * @param _f le fichier a ouvrir dans la fenetre de post. / the file to open in thje window of post
   */
  TrPostImplementation openPost(File _f);

  /**
   * @param _f le fichier layout a ouvrir dans la fenetre de post. / the file layout in the window of post
   * @param forceRecompute TODO
   */
  TrPostImplementation openLayoutPost(File _f, boolean forceRecompute);

  TrPostImplementation openScopPost(File _f);
  
  /**
   * @param _f le fichier a ouvrir dans la fenetre de visu de maillage. / the file to open in the window of  visu  of meshing
   */
  void openMeshView(File _f);

  /**
   * @param _f le fichier a ouvrir dans la fenetre de log. / the file to open in the window of  log
   */
  void openLogFrame(File _f);

  /**
   * @return le manager de format
   */
  TrFileFormatManager getFileFormatManager();

  /**
   * @return l'identifiant de la chaine de calcul en cours. / @return the identifiant  of the computation chain
   */
  String getCurrentPrefHydId();

  void changeChaineCalcul(Component _p);

  TrImplHelperAbstract getCurrentImplHelper();

  TrImplHelperAbstract getImplHelper(String _id);

  TrEditorImplementation findImplWithOpenedFile(final File _f);

  TrPostImplementation findPostWithOpenedFile(final File _f);

  boolean isAlreadyUsed(File _f);

  String[] getDefaultArgs();

}