/**
 * @creation 16 nov. 2004 @modification $Date: 2007-06-20 12:23:40 $ @license GNU General Public License 2 @copyright (c)1998-2001
 * CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuTable;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import javax.swing.ListModel;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfFilterTime;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.operation.EfIndexVisitorNearestElt;
import org.fudaa.dodico.ef.operation.EfIndexVisitorNearestNode;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueFleche;
import org.fudaa.ebli.calque.ZModeleFleche;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.layer.MvGridLayerGroup;
import org.fudaa.fudaa.meshviewer.layer.MvIsoModelInterface;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;
import org.fudaa.fudaa.meshviewer.model.MvInfoDelegateAbstract;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.data.TrPostDataVecteur;

/**
 * @author Fred Deniger
 * @version $Id: TrPostFlecheModel.java,v 1.32 2007-06-20 12:23:40 deniger Exp $
 */
public class TrPostFlecheModel implements ZModeleFleche, MvIsoModelInterface {

  TrPostFilterLayer cond_;
  TrPostFlecheContent current_;
  TrPostDataVecteur data_;
  ListModel fleche_;
  Coordinate pt_;
  TrPostSource s_;
  ListModel time_;
  int timeIdx_;
  private EfFilterTime filterForCurrentVar;

  /**
   * @param _proj la source
   */
  public TrPostFlecheModel(final TrPostSource _proj) {
    super();
    s_ = _proj;
    fleche_ = s_.getFlecheListModel();
    time_ = s_.getTimeListModel();
  }

  @Override
  public EfGridInterface getGrid() {
    return s_.getGrid();
  }

  @Override
  public double[] fillWithData(final int _idxElement, final double[] _l) {
    return TrIsoModelAbstract.fillWithData(s_.getGrid().getElement(_idxElement), data_, _l);
  }

  @Override
  public double getDatatFor(int idxElt, int idxPtOnElt) {
    return data_.getValue(s_.getGrid().getElement(idxElt).getPtIndex(idxPtOnElt));
  }

  @Override
  public boolean interpolate(final GrSegment _seg, final double _x, final double _y) {
    if (data_ != null) {
      final EfIndexVisitorNearestElt visitor = new EfIndexVisitorNearestElt(s_.getGrid(), _x, _y, 0);
      visitor.setOnlyInSearch(true);
      s_.getGrid().getIndex().query(EfIndexVisitorNearestNode.getEnvelope(_x, _y, 0), visitor);
      if (visitor.isIn()) {
        final int idxElt = visitor.getSelected();
        final EfFilterTime cond = getFilterForCurrentVar();
        // on filtre sur le point interpol�
        if (cond != null && !cond.isActivatedElt(idxElt)) {
          return false;
        }
        data_.interpolateValues(_seg, s_.getGrid(), idxElt, _x, _y);
        return true;
      }
    }
    return false;
  }

  public void fillInterpolateInfo(final InfoData _m, final int _element, final double _x, final double _y,
                                  final String _layerTitle) {
    s_.fillInterpolateInfo(_m, _element, _x, _y, timeIdx_, _layerTitle);
  }

  @Override
  public double getZ1(final int _i) {
    return 0;
  }

  @Override
  public double getZ2(final int _i) {
    return 0;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    if (current_ == null) {
      return null;
    }
    final BuTable b = new CtuluTable();
    final String s = TrResource.getS("<u>Variable</u>: {0}<br><u>Pas de temps</u>: {1}", current_.getVar().getName(),
                                     getSelectedTime());
    EbliTableInfoPanel.setTitle(b, "<html>" + s + "</html>");
    b.setModel(new ZCalqueFleche.ValueTableModel(this) {

      @Override
      public int getColumnCount() {
        if (data_ == null) {
          return 3;
        }
        return super.getColumnCount();
      }

      @Override
      public String getColumnName(final int _column) {
        if (_column == 0) {
          return EbliLib.getS("Indices");
        }
        if (_column == 1) {
          return "X";
        }
        // Y
        if (_column == 2) {
          return "Y";
        }
        if (_column == 3) {
          return current_.getVar().getName();
        }
        if (_column == 4) {
          return current_.getVxDesc();
        }
        if (_column == 5) {
          return current_.getVyDesc();
        }
        return CtuluLibString.EMPTY_STRING;
      }
    });
    return b;
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
    final ZCalqueFleche.StringInfo info = new ZCalqueFleche.StringInfo();
    if (timeIdx_ >= 0) {
      _d.put(TrResource.getS("Pas de temps"), Double.toString(s_.getTimeStep(timeIdx_)));
    }

    if (current_ != null && data_ != null) {
      info.norme_ = current_.getVar().getName();
      info.vx_ = current_.getVxDesc();
      info.vy_ = current_.getVyDesc();
    }
    if (data_ != null && data_.isElementData()) {
      MvInfoDelegateAbstract.fillWithElementInfo(_d, _layer.getLayerSelection(), s_.getGrid(), null, false, _layer.getTitle());
      if (!_layer.isOnlyOneObjectSelected()) {
        return;
      }
      final int i = _layer.getLayerSelection().getMinIndex();
      _d.put(info.norme_, info.formatVect(getNorme(i)));
      _d.put(info.vx_, info.formatVect(getVx(i)));
      _d.put(info.vy_, info.formatVect(getVy(i)));
    } else {

      info.titleIfOne_ = MvResource.getS("Noeud n�");
      info.title_ = MvResource.getS("Noeuds");
      ZCalqueFleche.fillWithInfo(_d, _layer.getLayerSelection(), this, info);
    }

  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  public final TrPostFilterLayer getCond() {
    if (cond_ == null) {
      cond_ = new TrPostFilterLayer(s_);
      cond_.updateTimeStep(timeIdx_, s_);

    }
    return cond_;
  }

  public final TrPostDataVecteur getData() {
    return data_;
  }

  public boolean getDataRange(final CtuluRange _r) {
    if (current_ == null) {
      return false;
    }
    _r.setToNill();
    final H2dVariableType v = current_.getVar();
    s_.getExtrema(_r, v, getFilterForCurrentVar(), null);
    return true;
  }

  @Override
  public GrBoite getDomaine() {
    return MvGridLayerGroup.getDomaine(s_.getGrid());
  }

  public CtuluRange getExtremaForTimeStep() {
    return s_.getExtremaForTimeStep(null, current_.getVar(), timeIdx_, getFilterForCurrentVar());
  }

  public EfFilterTime getFilterForCurrentVar() {
    if (current_ == null) {
      return null;
    }
    final TrPostFilterLayer filter = getCond();
    if (filter != null && filter.isVarModifiedByFilter(current_.getVar())) {
      return filter;
    }
    return null;
  }

  /**
   * @return la liste des variables fleches
   */
  public ListModel getListModel() {
    return fleche_;
  }

  @Override
  public int getNbElt() {
    return s_.getGrid().getEltNb();
  }

  @Override
  public int getNombre() {
    return data_ == null ? 0 : data_.getSize();
  }

  @Override
  public double getNorme(final int _i) {
    return data_ == null ? 0 : data_.getValue(_i);
  }

  @Override
  public double getVx(final int _i) {
    return data_ == null ? 0 : data_.getVx(_i);
  }

  @Override
  public double getVy(final int _i) {
    return data_ == null ? 0 : data_.getVy(_i);
  }

  @Override
  public double getX(final int _i) {
    if (data_.isElementData()) {
      return s_.getGrid().getMoyCentreXElement(_i);
    }
    return s_.getGrid().getPtX(_i);
  }

  @Override
  public double getY(final int _i) {
    if (data_.isElementData()) {
      return s_.getGrid().getMoyCentreYElement(_i);
    }
    return s_.getGrid().getPtY(_i);
  }

  @Override
  public Object getObject(final int _ind) {
    return null;
  }

  /**
   * @return le pas de temps selectionne
   */
  public String getSelectedTime() {
    return Double.toString(s_.getTimeStep(timeIdx_));
  }

  public H2dVariableType getSelectedVar() {
    if (s_.getFlecheListModel().getSize() == 0) {
      return null;
    }
    if (current_ != null) {
      return current_.getVar();
    }
    return null;
  }

  /**
   * @return le nom de la variable selectionnee.
   */
  public String getSelectedVarName() {
    if (s_.getFlecheListModel().getSize() == 0) {
      return null;
    }
    if (current_ != null) {
      return current_.getVar().getName();
    }
    return null;
  }

  public boolean getTimeDataRange(final CtuluRange _r) {
    if (current_ == null) {
      return false;
    }
    _r.setToNill();
    final H2dVariableType v = current_.getVar();
    s_.getExtremaForTimeStep(_r, v, timeIdx_, getFilterForCurrentVar());
    return true;
  }

  /**
   * @return l'indice du temps affich�
   */
  public final int getTimeIdx() {
    return timeIdx_;
  }

  public final double getTimeStep(final int _i) {
    return s_.getTimeStep(_i);

  }

  /**
   * @return la liste des pas de temps.
   */
  public ListModel getTimeListModel() {
    return time_;
  }

  @Override
  public void polygone(final GrPolygone _p, final int _i, final boolean _force) {
    MvElementModelDefault.initGrPolygone(s_.getGrid(), s_.getGrid().getElement(_i), _p);
  }

  @Override
  public int getNbPt(int idxElt) {
    return s_.getGrid().getElement(idxElt).getPtNb();
  }

  @Override
  public double getX(int idxElt, int idxPtInElt) {
    int idxPt = s_.getGrid().getElement(idxElt).getPtIndex(idxPtInElt);
    return s_.getGrid().getPtX(idxPt);
  }

  @Override
  public GrPoint getVertexForObject(int ind, int idVertex) {

    return new GrPoint(getX(ind, idVertex), getY(ind, idVertex), 0);
  }

  @Override
  public Envelope getEnvelopeForElement(int idxElt) {
    return s_.getGrid().getEnvelopeForElement(idxElt);
  }

  @Override
  public double getY(int idxElt, int idxPtInElt) {
    int idxPt = s_.getGrid().getElement(idxElt).getPtIndex(idxPtInElt);
    return s_.getGrid().getPtY(idxPt);
  }

  @Override
  public boolean isPainted(int idxElt) {
    return true;
  }

  @Override
  public boolean getCentre(GrPoint pt, int i, boolean force) {
    MvElementModelDefault.cente(s_.getGrid(), pt, i);
    return true;
  }

  @Override
  public boolean segment(final GrSegment _s, final int _i, final boolean _force) {
    if (fleche_.getSize() == 0 || data_ == null) {
      return false;
    }
    if (!isActivated(_i)) {
      return false;
    }

    if (data_.isElementData()) {
      if (pt_ == null) {
        pt_ = new Coordinate();
      }
      final EfElement el = s_.getGrid().getElement(_i);
      el.getMilieuXY(s_.getGrid(), pt_);

      _s.e_.setCoordonnees(pt_.x, pt_.y, 0);
      _s.o_.initialiseAvec(_s.e_);
    } else {
      final EfGridInterface g = s_.getGrid();
      _s.e_.setCoordonnees(g.getPtX(_i), g.getPtY(_i), 0);
      _s.o_.initialiseAvec(_s.e_);
    }
    _s.e_.x_ += data_.getVx(_i);
    _s.e_.y_ += data_.getVy(_i);
    return true;
  }

  @Override
  public void prepare() {
    updateFilter();
  }

  private boolean isActivated(final int _idxPtOrMesh) {
    if (filterForCurrentVar != null
        && ((data_.isElementData() && !filterForCurrentVar.isActivatedElt(_idxPtOrMesh)) || (!data_.isElementData() && !filterForCurrentVar.isActivated(
                                                                                             _idxPtOrMesh)))) {
      return false;
    }
    return true;
  }

  private void updateFilter() {
    filterForCurrentVar = getFilterForCurrentVar();
  }

  /**
   * @param _timeIdx le nouveau temps a afficher
   */
  public final void setTimeIdx(final int _timeIdx) {
    timeIdx_ = _timeIdx;
    if (current_ != null) {
      data_ = current_.getValues(timeIdx_);
    }
    if (cond_ != null) {
      cond_.updateTimeStep(timeIdx_, s_);
    }
  }

  public int getIdxOfVelocity() {
    final int max = s_.getFlecheListModel().getSize();
    for (int i = 0; i < max; i++) {
      if (H2dVariableType.VITESSE == getModelFlecheContent(i).getVar()) {
        return i;
      }
    }
    return -1;
  }

  protected TrPostFlecheContent getModelFlecheContent(final int _varIdx) {
    return (TrPostFlecheContent) s_.getFlecheListModel().getElementAt(_varIdx);
  }

  /**
   * @param _varIdx le nouvel contenu a afficher
   */
  public final void setVarIdx(final int _varIdx) {
    current_ = null;
    data_ = null;
    if (_varIdx >= 0) {

      // on recupere la donnee pour l'indice de la variable
      final TrPostFlecheContent newFleche = getModelFlecheContent(_varIdx);
      current_ = newFleche;
      if (current_ != null) {
        data_ = current_.getValues(timeIdx_);
      }
    }
  }
}
