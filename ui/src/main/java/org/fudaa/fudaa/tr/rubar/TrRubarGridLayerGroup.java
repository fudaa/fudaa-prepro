/**
 * @creation 10 juin 2004
 * @modification $Date: 2007-01-19 13:14:12 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuTable;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dVariableProviderContainerInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarGrid;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.calque.find.CalqueFindPointExpression;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.meshviewer.layer.MvGridLayerGroup;
import org.fudaa.fudaa.meshviewer.model.MvElementModel;
import org.fudaa.fudaa.meshviewer.model.MvInfoDelegate;
import org.fudaa.fudaa.meshviewer.model.MvNodeModel;
import org.fudaa.fudaa.meshviewer.model.MvNodeModelDefault;
import org.fudaa.fudaa.tr.data.TrExpressionSupplierForData;
import org.fudaa.fudaa.tr.data.TrInfoSenderDelegate;
import org.fudaa.fudaa.tr.data.TrNodeLayerEditable;
import org.fudaa.fudaa.tr.data.TrVisuPanelEditor;

import java.awt.*;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarGridLayerGroup.java,v 1.23 2007-01-19 13:14:12 deniger Exp $
 */
public class TrRubarGridLayerGroup extends MvGridLayerGroup {
  TrRubarAreteLayer areteLayer_;

  private static class NodeModel extends MvNodeModelDefault {
    final H2dVariableProviderContainerInterface prov_;

    /**
     * @param _g
     * @param _d
     */
    public NodeModel(final EfGridInterface _g, final MvInfoDelegate _d,
                     final H2dVariableProviderContainerInterface _prov) {
      super(_g, _d);
      prov_ = _prov;
    }

    @Override
    public EbliFindExpressionContainerInterface getExpressionContainer() {
      return new TrExpressionSupplierForData.Node(prov_, this);
    }

    @Override
    public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
      return createZTable(this);
    }
  }

  static BuTable createZTable(final MvNodeModelDefault _model) {
    return new CtuluTable(new ZCalquePoint.DefaultTableModelZ(_model) {
      @Override
      public String getColumnName(final int _column) {
        if (_column == 3) {
          return H2dVariableType.BATHYMETRIE.getName();
        }
        return super.getColumnName(_column);
      }
    });
  }

  private static class ZNodeModel extends MvNodeModelDefault {
    public ZNodeModel(final EfGridInterface _g, final MvInfoDelegate _d) {
      super(_g, _d);
    }

    @Override
    public EbliFindExpressionContainerInterface getExpressionContainer() {
      return new CalqueFindPointExpression(this);
    }

    @Override
    public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
      return createZTable(this);
    }
  }

  public static TrRubarGridLayerGroup createLayer3DGroup(final H2dRubarGrid _grid, final TrInfoSenderDelegate _d,TrVisuPanelEditor editor) {
    return new TrRubarGridLayerGroup(_grid, new ZNodeModel(_grid, _d), new TrRubarElementModel(null, _grid, _d),
        new TrRubarAreteModelDefault(_grid, null),editor);
  }

  /**
   * @param _param
   * @param _d
   */
  public TrRubarGridLayerGroup(final H2dRubarParameters _param, final TrInfoSenderDelegate _d, TrVisuPanelEditor editor) {
    this(_param.getGridVolume(), new NodeModel(_param.getGridVolume(), _d, _param.getNodalProvider()),
        new TrRubarElementModel(_param.getElementProvider(), _param.getGridVolume(), _d), new TrRubarAreteModelProject(
            _param), editor);
  }

  public TrRubarGridLayerGroup(final H2dRubarGrid _param, final MvNodeModel _model, final MvElementModel _eltModel,
                               final TrRubarAreteModel _arete, TrVisuPanelEditor editor) {
    super(_param, new TrNodeLayerEditable(_model, editor), new MvElementLayer(_eltModel));
    areteLayer_ = new TrRubarAreteLayer(_arete);
    areteLayer_.setName("cqAretes");
    areteLayer_.setTitle(MvResource.getS("Ar�tes"));
    areteLayer_.setDestructible(false);
    add(areteLayer_);
    enPremier(cqPt_);
  }

  /**
   * @return le calque des aretes
   */
  public TrRubarAreteLayer getAreteLayer() {
    return areteLayer_;
  }

  @Override
  public void determinePaintState() {
    Color top = null;
    boolean pointPainted = true;
    // les aretes doivent etre visibles et presentes (>0)
    if (areteLayer_.isVisible() && areteLayer_.modeleDonnees().getNombre() > 0) {
      pointPainted = false;
      cqPoly_.setPainted(false);
      top = areteLayer_.getPaletteCouleur() == null ? areteLayer_.getForeground() : null;
    } else {
      cqPoly_.setPainted(true);
      if (cqPoly_.isVisible()) {
        pointPainted = false;
        top = cqPoly_.getPaletteCouleur() == null ? cqPoly_.getForeground() : null;
      }
    }
    if (pointPainted || ((cqPt_.getIconModel(0) != null) && (cqPt_.getIconModel(0).getTaille() > 1))
        || (cqPt_.getPaletteCouleur() != null) || (cqPt_.getForeground() != top)) {
      cqPt_.setPainted(true);
    } else {
      cqPt_.setPainted(false);
    }
  }
}
