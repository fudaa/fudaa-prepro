/**
 *  @creation     25 ao�t 2003
 *  @modification $Date: 2006-09-19 15:07:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.tr.data;

import java.util.List;
import org.fudaa.dodico.h2d.H2dBcFrontierBlockInterface;
import org.fudaa.ebli.calque.ZModeleDonneesMulti;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * @author deniger
 * @version $Id: TrBcBoundaryBlockModel.java,v 1.14 2006-09-19 15:07:28 deniger Exp $
 */
public interface TrBcBoundaryBlockModel extends ZModeleDonneesMulti {
  /**
   * Le nombre de frontiere du maillage.
   */
  int getNbFrontier();

  int getNbBoundaryType();

  /**
   * Renvoie les bords de la frontiere.
   */
  H2dBcFrontierBlockInterface getFrontier(int _idxFrontier);

  /**
   * Initialise <code>_p</code> a partir des donnees du point d'indice <code>_idxGlobal</code> sur le domaine.
   */
  void getPoint(GrPoint _p, int _idxFr, int _idxPtOnFrontier);

  int getNbTotalPt();

  int getIdxGlobal(int _idxFr, int _idxOnFr);

  int getFrontiereIndice(int _idxFr, int _idxOnFr);

  int getNbLiquidFrontier();

  /**
   * @return la liste des listes utilisees.
   */
  List getUsedBoundaryType();

  List getAllBoundaryType();
}
