/**
 * @creation 10 juin 2003
 * @modification $Date: 2007-04-30 14:22:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluUndoRedoInterface;
import org.fudaa.ctulu.gui.CtuluHelpComponent;
import org.fudaa.dodico.h2d.rubar.H2dRubarBcListener;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryFlowrateGroupType;
import org.fudaa.dodico.h2d.type.H2dRubarFileType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.fudaa.commun.FudaaProjectStateSupportLabelListener;
import org.fudaa.fudaa.commun.exec.FudaaEditor;
import org.fudaa.fudaa.commun.undo.FudaaUndoPaneFille;
import org.fudaa.fudaa.fdico.FDicoEntitePanel;
import org.fudaa.fudaa.fdico.FDicoEntiteTableModel;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * @author deniger
 * @version $Id: TrRubarFilleProjet.java,v 1.28 2007-04-30 14:22:39 deniger Exp $
 */
public class TrRubarFilleProjet extends FudaaUndoPaneFille implements CtuluHelpComponent, H2dRubarBcListener {
  TrRubarProject proj_;
  JComponent[] specificComponents_;
  BuLabel txtDate_;
  BuLabel txtFile_;
  BuLabel txtSave_;
  BuComboBox cbType_;
  BuComboBox cbFileFormat_;
  JSpinner spinnerNbTransportBlock;
  boolean isUpdating_;

  protected void typeChangedFromGUI() {
    if (isUpdating_) {
      return;
    }
    proj_.getTrRubarParams().getH2dRubarParametres().changeProjetType((H2dRubarProjetType) cbType_.getSelectedItem(), getCmdMng());
  }

  protected void fileFormatChangedFromGUI() {
    proj_.setNewFormatForNumberOfDigits(cbFileFormat_.getSelectedItem() == H2dRubarFileType.NEW_FORMAT_9_DIGITS, true, getCmdMng());
  }

  @Override
  public void areteTypeChanged() {
  }

  @Override
  public void bathyChanged() {
  }

  @Override
  public void nodeInGridChanged() {

  }

  @Override
  public void fondDurChanged() {
  }

  @Override
  public void flowrateGroupChanged(final H2dRubarBoundaryFlowrateGroupType _t) {
  }

  /**
   * Changement depuis le modele (lors d'un undo par exemple).
   */
  @Override
  public void projectTypeChanged() {
    isUpdating_ = true;
    cbType_.setSelectedItem(proj_.getH2dRubarParameters().getProjetType());
    isUpdating_ = false;
  }

  @Override
  public void numberOfConcentrationChanged() {
    isUpdating_ = true;
    spinnerNbTransportBlock.setValue(proj_.getH2dRubarParameters().getBcMng().getNbConcentrationBlocks());
    isUpdating_ = false;
  }

  @Override
  public void timeClChanged() {
  }

  /**
   * @param _proj le projet a afficher
   */
  public TrRubarFilleProjet(final TrRubarProject _proj) {
    super(_proj.getTitle(), true, true, true, true, _proj.getImpl().getUndoCmdListener());
    proj_ = _proj;
    proj_.getH2dRubarParameters().getBcMng().addListener(this);
    final BuPanel princ = new PrincipalPanel();
    princ.setLayout(new BuBorderLayout(5, 15));
    final BuPanel north = new BuPanel();
    north.setLayout(new BuGridLayout(2, 5, 5, true, true));

    north.add(new BuLabel(BuResource.BU.getString("Projet:")));
    txtFile_ = new BuLabel();
    north.add(txtFile_);

    north.add(new BuLabel(BuResource.BU.getString("Type:")));
    cbType_ = new BuComboBox(H2dRubarProjetType.getConstantArray());
    cbType_.setSelectedItem(proj_.getH2dRubarParameters().getProjetType());
    north.add(cbType_);
    cbType_.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent _e) {
        typeChangedFromGUI();
      }
    });

    cbFileFormat_ = new BuComboBox(H2dRubarFileType.getConstantArray());
    if (_proj.isNewFormatForNumberOfDigits()) {
      cbFileFormat_.setSelectedItem(H2dRubarFileType.NEW_FORMAT_9_DIGITS);
    } else {
      cbFileFormat_.setSelectedItem(H2dRubarFileType.OLD_FORMAT_6_DIGITS);
    }
    cbFileFormat_.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent _e) {
        fileFormatChangedFromGUI();
      }
    });

    north.add(new BuLabel(BuResource.BU.getString("Format:")));
    north.add(cbFileFormat_);
    spinnerNbTransportBlock = new JSpinner(new SpinnerNumberModel(proj_.getH2dRubarParameters().getBcMng().getNbConcentrationBlocks(), 1, 1000, 1));
    spinnerNbTransportBlock.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        nbTransportBlocksChangedFromGUI();
      }
    });

    north.add(new BuLabel(BuResource.BU.getString(TrLib.getString("Nombre de concentrations") + " :")));
    north.add(spinnerNbTransportBlock);

    north.add(new BuLabel(TrResource.getS("Dernier enregistrement:")));
    txtDate_ = new BuLabel();
    north.add(txtDate_);
    north.add(new BuLabel(TrResource.getS("Etat:")));
    txtSave_ = new BuLabel();
    north.add(txtSave_);
    princ.add(north, BuBorderLayout.NORTH);
    final BuTable b = new BuTable();
    b.setModel(_proj.fileStates_);
    b.getColumnModel().getColumn(1).setCellRenderer(_proj.fileStates_.getSecondCellRenderer());
    princ.add(new BuScrollPane(b), BuBorderLayout.CENTER);
    princ.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    addTab(BuResource.BU.getString("Projet"), null, (CtuluUndoRedoInterface) princ, BuResource.BU.getString("Projet"));
    final CtuluCommandManager cmd = new CtuluCommandManager();
    FDicoEntiteTableModel dicoModel = new FDicoEntiteTableModel(proj_, cmd);
    proj_.getDicoParams().addModelListener(new TrRubarParEnableStateUpdater(dicoModel));
    addTab(TrResource.getS("Propri�t�s du calcul"), null, new FDicoEntitePanel(dicoModel, false, false, false),
        TrResource.getS("Propri�t�s du calcul"));
    addTab(TrResource.getS("Groupe d�bit"), null, new TrRubarFlowrateGroupEditorPanel(_proj.getH2dRubarParameters(), proj_.getEvolMng()), null);
    addTab(TrResource.getS("Groupe tarage"), null, new TrRubarTarageGroupEditorPanel(_proj.getH2dRubarParameters(), proj_.getTrRubarParams()
        .getTarageMng()), null);
    final FudaaProjectStateSupportLabelListener listener = new FudaaProjectStateSupportLabelListener(txtSave_, txtDate_, this, txtFile_);
    listener.setSupport(proj_);
    listener.initWith(proj_.getState());
  }

  private void nbTransportBlocksChangedFromGUI() {
    if (isUpdating_) {
      return;
    }
    Object value = spinnerNbTransportBlock.getValue();
    if (value != null) {
      int nbBlocks = (Integer) value;
      proj_.getH2dRubarParameters().setNbConcentrationBlocks(nbBlocks, getCmdMng());
    }
  }

  @Override
  public void actionPerformed(final ActionEvent _ae) {
    if ("EDITER".equals(_ae.getActionCommand())) {
      FudaaEditor.getInstance().edit(proj_.getParamsFile());
    }
  }

  @Override
  public String getShortHtmlHelp() {
    final CtuluHtmlWriter buf = new CtuluHtmlWriter();
    buf.h2(CtuluResource.CTULU.getString("Description"));
    buf.close(buf.para(), TrResource.getS("le premier onglet donne des informations g�n�rales sur le projet"));
    buf.close(buf.para(), TrResource.getS("le deuxi�me onglet permet de modifier les propri�t�s g�n�rales du projet"));
    buf.close(buf.para(), TrResource.getS("le dernier onglet est utilis� pour d�finir les groupes de d�bit"));
    buf.h2(TrResource.getS("Documents associ�s"));
    buf.addTag("ul");
    buf.close(buf.addTag("li"),
        proj_.getImpl().buildLink(TrResource.getS("Description de l'�diteur des param�tres g�n�raux"), "rubar-editor-params-gen"));
    buf.close(buf.addTag("li"), proj_.getImpl().buildLink(TrResource.getS("Edition des conditions limites"), "rubar-editor-bc"));
    return buf.toString();
  }

  @Override
  public JComponent[] getSpecificTools() {
    if (specificComponents_ == null) {
      specificComponents_ = new JComponent[7];
      specificComponents_[0] = proj_.getCalculActions().getCalcul().buildToolButton(EbliComponentFactory.INSTANCE);
      specificComponents_[1] = null;
      specificComponents_[2] = proj_.getFV2MAction().buildToolButton(EbliComponentFactory.INSTANCE);
      specificComponents_[3] = null;
      specificComponents_[4] = proj_.getVisuFilleAction().buildToolButton(EbliComponentFactory.INSTANCE);
      specificComponents_[5] = proj_.getCourbeFilleAction().buildToolButton(EbliComponentFactory.INSTANCE);
      specificComponents_[6] = proj_.getTarageFilleAction().buildToolButton(EbliComponentFactory.INSTANCE);
    }
    return specificComponents_;
  }

  static final class PrincipalPanel extends BuPanel implements CtuluUndoRedoInterface {
    CtuluCommandManager cmd_;

    @Override
    public void clearCmd(final CtuluCommandManager _source) {
      if ((cmd_ != null) && (_source != cmd_)) {
        cmd_.clean();
      }
    }

    @Override
    public CtuluCommandManager getCmdMng() {
      if (cmd_ == null) {
        cmd_ = new CtuluCommandManager();
      }
      return cmd_;
    }

    @Override
    public void setActive(final boolean _b) {
    }
  }
}
