/**
 *  @creation     18 mars 2004
 *  @modification $Date: 2007-01-19 13:14:08 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.fudaa.meshviewer.model.MvNodeModel;

/**
 * @author Fred Deniger
 * @version $Id: TrSolutionInitModel.java,v 1.9 2007-01-19 13:14:08 deniger Exp $
 */
public interface TrSolutionInitModel extends MvNodeModel {

  /**
   * 3 elements.
   * 
   * @param _poly
   * @param _i
   * @return true si ok.
   */
  boolean polygone(GrPolygone _poly, int _i);

  @Override
  EfGridInterface getGrid();

  /**
   * @return le nombre d'element
   */
  int getEltNb();

  /**
   * Remplit le tableau avec les 3 cotes au 3 points extremites.
   * 
   * @param _v le tableau a modifier de taille 3
   * @param _i l'indice de l'element
   * @return true si le tableau a ete initialise
   */
  boolean getCote(double[] _v, int _i);

}
