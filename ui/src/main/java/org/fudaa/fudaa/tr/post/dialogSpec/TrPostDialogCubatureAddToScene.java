/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.dialogSpec;

import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.post.TrPostCourbe;
import org.fudaa.fudaa.tr.post.TrPostCourbeTreeModel;
import org.fudaa.fudaa.tr.post.TrPostCourbeVolumeModel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class TrPostDialogCubatureAddToScene extends AbstractTrPostDialogAddToScene<TrPostDialogCubature> {
  public TrPostDialogCubatureAddToScene(TrPostDialogCubature from) {
    super(from);
  }

  @Override
  protected void updateNames() {
    H2dVariableType selectedVariable = from.getSelectedVariable();
    final String prefix = TrLib.getString("Volume") + " " + selectedVariable.getName();
    tfResult.setText(prefix);
    tfPositive.setText(prefix + " >= " + from.getSeuil());
    tfNegative.setText(prefix + " <= " + from.getSeuil());
  }

  @Override
  protected void addCurves(ProgressionInterface prog) {
    final List<TrPostCourbeVolumeModel> models = new ArrayList<TrPostCourbeVolumeModel>();
    if (cbResult.isSelected()) {
      models.add(createModel(EnumThresholdCalculation.ALL, tfResult.getText(), prog));
    }
    if (cbNegative.isSelected()) {
      models.add(createModel(EnumThresholdCalculation.NEGATIVE, tfNegative.getText(), prog));
    }
    if (cbPositive.isSelected()) {
      models.add(createModel(EnumThresholdCalculation.POSITIVE, tfPositive.getText(), prog));
    }
    JLabel lb = (JLabel) targetGraph.getSelectedItem();
    final EGGraphe graphe = (EGGraphe) lb.getClientProperty("GRAPH");
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        TrPostDialogCubatureAddToScene.this.buttonAddCurves.setEnabled(true);
        TrPostDialogCubatureAddToScene.this.buttonAddCurves.setText(TrLib.getString("Ajouter les courbes"));
        TrPostCourbeTreeModel model = null;
        if (graphe == null) {
          model = new TrPostCourbeTreeModel(from.impl_.getProject());
          from.impl_.getProject().addFille(model, TrPostCourbeTreeModel.getNewName(), null, from.calque_);
        } else {
          model = (TrPostCourbeTreeModel) graphe.getModel();
        }
        EGGroup groupFor = model.getGroupFor(computeVariableForCourbe());
        for (TrPostCourbeVolumeModel courbeModel : models) {
          groupFor.addEGComponent(new TrPostCourbe(groupFor, courbeModel, from.source_.getNewTimeListModel()));
        }
        model.fireStructureChanged();
        getScene().stopEditing();
        getScene().refresh();
      }
    });
  }

  private H2dVariableType computeVariableForCourbe() {
    final H2dVariableType selectedVariable = from.getSelectedVariable();

    String title = TrLib.getString("Volume") + "(" + selectedVariable.getName() + ")";
    String shortName = "vol(" + selectedVariable.getShortName() + ")";
    String unit = "m2*" + selectedVariable.getCommonUnitString();
    if ("m".equals(selectedVariable.getCommonUnit())) {
      unit = "m3";
      title = TrLib.getString("Volume");
      shortName = "volume";
    }

    return new H2dVariableTypeCreated(title, shortName, unit, null);
  }

  private TrPostCourbeVolumeModel createModel(final EnumThresholdCalculation type, String nom, ProgressionInterface prog) {
    TrPostCourbeVolumeModel model = new TrPostCourbeVolumeModel();
    model.setSelectedMeshes(from.getSelectedMeshes());
    model.setMinTimeIdx(startTime.getSelectedIndex());
    model.setMaxTimeIdx(endTime.getSelectedIndex());
    model.setSeuil(from.getSeuil());
    model.setSource(from.source_);
    model.setVar(from.getSelectedVariable());
    model.setComputation(type);
    model.setNom(nom);
    model.updateData(prog);
    return model;
  }
};
