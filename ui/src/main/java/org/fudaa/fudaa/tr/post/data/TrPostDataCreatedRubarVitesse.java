/*
 *  @creation     3 mai 2005
 *  @modification $Date: 2007-02-02 11:22:21 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.fu.FuLog;
import java.io.IOException;
import java.util.Set;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.TrPostSource;

public final class TrPostDataCreatedRubarVitesse extends TrPostDataCreatedDefault {

  final H2dVariableType debitVar_;

  /**
   * @param _debitVar la variable d�bit
   */
  public TrPostDataCreatedRubarVitesse(final TrPostSource _src, final H2dVariableType _debitVar) {
    super(_src);
    debitVar_ = _debitVar;
  }

  @Override
  public double buildDataFor(final int _idxTime, final int _idxObject) {
    try {
      final double d = src_.getData(H2dVariableType.HAUTEUR_EAU, _idxTime, _idxObject);
      if (d == 0) { return 0; }
      return src_.getData(debitVar_, _idxTime, _idxObject) / d;
    } catch (final IOException _e) {
      FuLog.warning(_e);
    }
    return 0;
  }

  @Override
  public String getDescription() {
    return H2dVariableType.VITESSE.toString();
  }

  @Override
  public EfData buildDataFor(final int _idxTime) {
    final EfData h = src_.getData(H2dVariableType.HAUTEUR_EAU, _idxTime);
    final EfData q = src_.getData(debitVar_, _idxTime);
    if (q == null) { return null; }
    final double[] v = new double[q.getSize()];
    for (int i = v.length - 1; i >= 0; i--) {
      final double hi = h.getValue(i);
      if (hi == 0) {
        v[i] = 0;
      } else {
        v[i] = q.getValue(i) / hi;
      }
    }
    return new EfDataElement(v);
  }

  @Override
  public void fillWhithAllUsedVar(final Set _res) {
    _res.add(debitVar_);

  }
}