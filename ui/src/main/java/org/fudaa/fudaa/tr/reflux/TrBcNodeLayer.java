/*
 * @creation 29 janv. 07
 * @modification $Date: 2007-06-05 09:01:13 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.bu.BuTable;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.layer.MvFrontierPointLayer;
import org.fudaa.fudaa.meshviewer.model.MvFrontierModel;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author fred deniger
 * @version $Id: TrBcNodeLayer.java,v 1.4 2007-06-05 09:01:13 deniger Exp $
 */
public class TrBcNodeLayer extends MvFrontierPointLayer {

  protected ZCalqueFleche cqNormale_;

  public static class ZFlecheModelSegmentAdapter implements ZModeleFleche {

    final ZModeleSegment seg_;

    public ZFlecheModelSegmentAdapter(final ZModeleSegment _seg) {
      super();
      seg_ = _seg;
    }

    @Override
    public void prepare() {
    }

    @Override
    public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
      return seg_.createValuesTable(_layer);
    }

    @Override
    public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
      seg_.fillWithInfo(_d, _layer);
    }

    @Override
    public GrBoite getDomaine() {
      return seg_.getDomaine();
    }

    @Override
    public int getNombre() {
      return seg_.getNombre();
    }

    @Override
    public double getNorme(final int _i) {
      return seg_.getNorme(_i);
    }

    @Override
    public GrPoint getVertexForObject(int ind, int idVertex) {
      int i = idVertex % 2;
      if (i == 0) {
        return new GrPoint(getX(ind), getY(ind), getZ1(ind));
      }
      return new GrPoint(getX(ind), getY(ind), getZ2(ind));
    }

    @Override
    public Object getObject(final int _ind) {
      return seg_.getObject(_ind);
    }

    @Override
    public double getVx(final int _i) {
      return seg_.getVx(_i);
    }

    @Override
    public double getVy(final int _i) {
      return seg_.getVy(_i);
    }

    @Override
    public double getX(final int _i) {
      return seg_.getX(_i);
    }

    @Override
    public double getY(final int _i) {
      return seg_.getY(_i);
    }

    @Override
    public double getZ1(final int _i) {
      return seg_.getZ1(_i);
    }

    @Override
    public double getZ2(final int _i) {
      return seg_.getZ2(_i);
    }

    @Override
    public boolean isValuesTableAvailable() {
      return seg_.isValuesTableAvailable();
    }

    @Override
    public boolean segment(final GrSegment _s, final int _i, final boolean _force) {
      return seg_.segment(_s, _i, _force);
    }

    @Override
    public boolean interpolate(final GrSegment _seg, final double _x, final double _y) {
      return false;
    }
  }

  public TrBcNodeLayer(final MvFrontierModel _cl, final ZModeleSegment _seg) {
    super(_cl);
    cqNormale_ = new ZCalqueFleche(new ZFlecheModelSegmentAdapter(_seg));
    cqNormale_.setVisible(false);
    add(cqNormale_);
  }

  private BConfigurableComposite getConfigureForNormales() {
    final BConfigurableInterface[] sect = new BConfigurableInterface[2 + getNbSet()];

    sect[0] = new ZCalqueAffichageDonneesConfigure(cqNormale_, true);
    for (int i = 1; i < sect.length; i++) {
      sect[i] = new ZCalqueAffichageDonneesTraceConfigure(cqNormale_, i - 1);
    }
    sect[sect.length - 1] = new ZCalqueFlecheScaleSection(cqNormale_);
    return new BConfigurableComposite(sect, TrResource.getS("Normales"));
  }

  @Override
  public BConfigurableInterface[] getConfigureInterfaces() {
    return new BConfigurableInterface[]{new BConfigurableComposite(getAffichageConf(), getConfigureForNormales(),
              null)};
  }
}
