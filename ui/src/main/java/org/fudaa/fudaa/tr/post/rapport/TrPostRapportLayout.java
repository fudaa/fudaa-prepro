package org.fudaa.fudaa.tr.post.rapport;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import javax.swing.JFileChooser;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.pdf.CtuluPdfPsExport;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetVueCalque;
import org.fudaa.ebli.visuallibrary.graphe.EbliWidgetGraphe;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostScene;
import org.netbeans.api.visual.widget.Widget;

/**
 * Genere un rapport a partir du layout.
 * le rapport est en pdf
 * @author Adrien Hadoux
 *
 */
public class TrPostRapportLayout {

	TrPostScene scene_=null;
	EbliActionSimple actionGenerate_;
	public TrPostRapportLayout(TrPostScene scene){
		scene_=scene;
		actionGenerate_=new EbliActionSimple(TrResource.getS("Rapport"),EbliResource.EBLI.getIcon("text"),"RAPPORTPDF"){
      @Override
			public void actionPerformed(ActionEvent e){
						
				JFileChooser chooser=new JFileChooser();
				int reponse=chooser.showOpenDialog(null);
				if(reponse!= JFileChooser.APPROVE_OPTION)
					return;
				
				File f=chooser.getSelectedFile();
				writeDoc(f);
			}
		};
		
	}
		
	public EbliActionSimple getAction(){
		return actionGenerate_;
	}
	
	
	
	void writeDoc(File file){
		Document document = new Document();
		
		//-- ajout des infos relatives au document --//
		document.addTitle(CtuluResource.CTULU.getString("Exportation PDF"));
		document.addAuthor("FUDAA generating document");
	
		try {
			PdfWriter writer =PdfWriter.getInstance(document, new FileOutputStream(file));
			document.open();
		
				int w=scene_.getView().getSize().width;
				int h=scene_.getView().getSize().height;
				CtuluPdfPsExport.addImageToPdf(scene_.produceImage(w,h, new HashMap()), writer, w, h);
				
		for(Widget widget:scene_.getChildren()){
			if(widget instanceof EbliWidget){
				EbliWidget wid=(EbliWidget)widget;
				
				if(wid.getIntern() instanceof EbliWidgetVueCalque){
					EbliWidgetVueCalque vue=(EbliWidgetVueCalque)wid.getIntern();
					CtuluPdfPsExport.addImageToPdf(vue.getCalquePanel().produceImage(new HashMap()), writer, vue.getCalquePanel().getSize().width,vue.getCalquePanel().getSize().height);
					
					CtuluPdfPsExport.addJTableToPdf(vue.getCalquePanel().createTable() , writer);
					
				}else 
					if(wid.getIntern() instanceof EbliWidgetGraphe){
						EbliWidgetGraphe vue=(EbliWidgetGraphe)wid.getIntern();
					CtuluPdfPsExport.addImageToPdf(vue.getGraphe().produceImage(new HashMap()), writer, vue.getGraphe().getSize().width,vue.getGraphe().getSize().height);
					
					//CtuluPdfPsExport.addJTableToPdf(vue.getGraphe().get , writer);
					
				}
				
				
			}
		}
				
				
		
		} catch (FileNotFoundException|DocumentException e) {
			FuLog.error(e);
		}
	
		
		
	}
	
}
