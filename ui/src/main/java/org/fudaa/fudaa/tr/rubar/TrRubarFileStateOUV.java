/**
 * @creation 3 janv. 2005
 * @modification $Date: 2007-05-04 14:01:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryFlowrateGroupType;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryTarageGroupType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.rubar.io.RubarOUVFileFormat;
import org.fudaa.dodico.rubar.io.RubarOUVWriter;

import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarFileStateOUV.java,v 1.9 2007-05-04 14:01:53 deniger Exp $
 */
public class TrRubarFileStateOUV extends TrRubarFileState implements H2dRubarOuvrageListener, H2dRubarBcListener, H2dRubarTarageListener {
  final H2dRubarOuvrageMng ouvrageMng;

  /**
   * @param ouvrageMng le manager des ouvrages
   * @param _l le listener
   */
  public TrRubarFileStateOUV(final TrRubarFileStateMng _l, final H2dRubarOuvrageMng ouvrageMng, H2dRubarBcMng bcMng) {
    super(new RubarOUVFileFormat(), _l);
    this.ouvrageMng = ouvrageMng;
    if (ouvrageMng != null && !ouvrageMng.isEmpty()) {
      _l.add(this);
    }
    if (ouvrageMng != null) {
      ouvrageMng.addListener(this);
    }
    if (bcMng != null) {
      bcMng.addListener(this);
      bcMng.getTarageMng().addTarageListener(this);
    }
  }

  @Override
  public void nodeInGridChanged() {
    if (((TrRubarFileStateMng) super.l_).contains(this)) {
      setModified(true);
    }
  }

  @Override
  public void bathyChanged() {

  }

  @Override
  public void fondDurChanged() {

  }

  @Override
  public void projectTypeChanged() {

  }

  @Override
  public void numberOfConcentrationChanged() {

  }

  @Override
  public void timeClChanged() {

  }

  @Override
  public void areteTypeChanged() {

  }

  @Override
  public void flowrateGroupChanged(H2dRubarBoundaryFlowrateGroupType _t) {

  }

  @Override
  public void ouvrageAdded(final H2dRubarOuvrageMng _mng) {
    setModified(true);
    if (isMarkRemoved() || (!((TrRubarFileStateMng) (super.l_)).contains(this))) {
      setMarkRemoved(false);
      ((TrRubarFileStateMng) (super.l_)).add(this);
    }
  }

  @Override
  public void ouvrageChanged(final H2dRubarOuvrageMng _mng, final H2dRubarOuvrage _ouv, final boolean _ouvrageElementaireAddedOrRemoved) {
    setModified(true);
  }

  @Override
  public void ouvrageElementaireChanged(final H2dRubarOuvrageMng _mng, final H2dRubarOuvrage _ouv, final H2dRubarOuvrageElementaireInterface _i) {
    setModified(true);
  }

  @Override
  public void ouvrageRemoved(final H2dRubarOuvrageMng _mng) {
    setModified(true);
    if (_mng.isEmpty()) {
      setMarkRemoved(true);
    }
  }

  @Override
  public CtuluIOOperationSynthese save(final File _dir, final String _projectName, final ProgressionInterface _progression,
                                       final TrRubarProject _projet) {
    final H2dRubarOuvrageMng mng = _projet.getH2dRubarParameters().getOuvrageMng();
    if (mng != null && mng.isInitialized()) {
      final RubarOUVWriter writer = (RubarOUVWriter) ((RubarOUVFileFormat) fmt_).createWriter();
      writer.setNewFormat(_projet.isNewFormatForNumberOfDigits());
      return writer.write(mng, fmt_.getFileFor(_dir, _projectName), _progression);
    }
    FuLog.warning(new Throwable());
    return null;
  }

  @Override
  public void tarageCourbeChanged(H2dRubarTarageMng _mng, EvolutionReguliereInterface _e) {
    if (ouvrageMng.containsTarage(_e)) {
      setModified(true);
    }
  }

  @Override
  public void tarageUsedChanged(H2dRubarTarageMng _mng) {

  }

  @Override
  public void TarageGroupeUsedChanged(H2dRubarTarageMng _mng, H2dRubarBoundaryTarageGroupType tarageGroupeType) {

  }

  @Override
  public void fileFormatDigitsChanged() {
    setModified(true);
  }
}
