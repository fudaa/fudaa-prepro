/*
 *  @creation     11 mars 2005
 *  @modification $Date: 2006-09-19 15:07:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.persistence;

import org.fudaa.ebli.calque.BCalque;
import org.fudaa.fudaa.tr.post.TrPostFlecheLayer;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;

/**
 * @author Fred Deniger
 * @version $Id: TrPostFlecheLayerPersistence.java,v 1.3 2006-09-19 15:07:28 deniger Exp $
 */
public final class TrPostFlecheLayerPersistence extends TrPostIsoLayerPersistence {

  /**
   * @param _cqToSave
   * @param _prog
   */
  public TrPostFlecheLayerPersistence() {}

  @Override
  public boolean isBase(final BCalque _cq) {
    return ((TrPostFlecheLayer) _cq).isBase();
  }

  @Override
  protected BCalque createLayer(final TrPostVisuPanel _pn) {
    return _pn.getFlecheLayer().duplicate();
  }

}