/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.replay;

import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.FastBitSet;
import org.fudaa.ebli.commun.EbliSelectionState;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.tr.post.TrPostCourbeVolumeModel;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;

/**
 *
 * @author Frederic Deniger
 */
public final class TrReplayVolumeVisuCustomizer implements TrReplayVisuCustomizer {

  final TrPostCourbeVolumeModel modele;
  final String title;

  /**
   * @param listePoints
   */
  /**
   * @param listePoints
   */
  public TrReplayVolumeVisuCustomizer(final TrPostCourbeVolumeModel modele, String title) {
    super();
    this.modele = modele;
    this.title = title;
  }

  public String getTitle() {
    return title;
  }

  @Override
  public void customizeVisu(final TrPostVisuPanel vue2d) {
    MvElementLayer polygonLayer = vue2d.getGridGroup().getPolygonLayer();
    polygonLayer.setVisible(true);
    FastBitSet selectedMeshes = modele.getSelectedMeshes();
    if (selectedMeshes != null) {
      polygonLayer.changeSelection(new CtuluListSelection(selectedMeshes), EbliSelectionState.ACTION_REPLACE);
    } else {
      polygonLayer.selectAll();
    }
    vue2d.setCalqueActif(polygonLayer);
  }
}
