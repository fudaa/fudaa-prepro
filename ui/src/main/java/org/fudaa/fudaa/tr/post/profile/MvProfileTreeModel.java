/*
 * @creation 23 nov. 06
 *
 * @modification $Date: 2007-06-13 14:46:13 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import com.memoire.bu.BuLib;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.ef.operation.*;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.courbe.*;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeModel;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.tr.common.TrCourbeImporter.Target;
import org.fudaa.fudaa.tr.post.*;
import org.fudaa.fudaa.tr.post.actions.TrPostProfileAction;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;

import javax.swing.tree.TreePath;
import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * @author fred deniger
 * @version $Id: MvProfileTreeModel.java,v 1.12 2007-06-13 14:46:13 deniger Exp $
 */
@SuppressWarnings("serial")
public class MvProfileTreeModel extends EGGrapheTreeModel implements Target {
  FudaaCourbeTimeListModel timeModel_;
  EfLineIntersectionsCorrectionTester tester_;
  public MvProfileTarget target_;

  public MvProfileTreeModel(final MvProfileTarget _target, final EfLineIntersectionsCorrectionTester _tester) {
    super();
    timeModel_ = _target.getTimeModel();
    tester_ = _tester;
    target_ = _target;
  }

  public MvProfileTreeModel(final MvProfileTreeModel _model) {
    super();
    timeModel_ = _model.target_.getTimeModel();
    tester_ = _model.tester_;
    target_ = _model.target_;
  }

  @Override
  public boolean isContentModifiable() {
    return false;
  }

  /**
   * ACHTUNG! Constructuer uniquement utilis� pour la serialization des graphes!! Il faut imp�rativement initialiser la variable target_ avec un
   * ProfilDapter apres coup avec le trpostsource qui convient
   */
  public MvProfileTreeModel() {
    tester_ = new MvProfileCoteTester();
  }

  public H2dVariableType[] getVariables() {
    return null;
  }

  @Override
  public boolean isStructureModifiable() {
    return true;
  }

  public void updateTimeStep(final int[] _idx, final ProgressionInterface _prog) {
    blockEvents_ = true;
    final FudaaCourbeTimeListModel newModel = target_.getTimeModel();

    if (newModel != null) {
      final boolean structureChanged = newModel.getSize() != timeModel_.getSize();
      // timeModel_.updateFrom(newModel);
      if (structureChanged) {
        timeModel_.fireStructureChanged();
      } else {
        timeModel_.fireContentChanged();
      }
      blockEvents_ = false;
      BuLib.invokeLater(new Runnable() {
        @Override
        public void run() {
          fireCourbeContentChanged(null, true);
          if (structureChanged) {
            final EGObject[] cs = getSelectedObjects();
            if (cs != null) {
              for (int i = 0; i < cs.length; i++) {

                if (cs[i] instanceof MvProfileCourbeTime) {
                  ((MvProfileCourbeTime) cs[i]).selection_.setSelectionInterval(timeModel_.getSize() - 1, timeModel_
                      .getSize() - 1);
                }
              }
            }
          }
        }
      });
    }
  }

  /**
   * Rejoue les donn�es de la polyligne avec les nouvelles infos. Si boolean ecraser est a false, Cree sune nouvelle courbe avec le mod�le qui
   * convient. Dans le cas ou ecraser est � true, on ecrase les infos du modele par ceux des param d'entree. Version r�serv�s aux points r�els
   *
   * @param _model
   * @param _src
   * @param _cmd
   * @param _prog
   * @param newVariable
   * @param ecraser
   */
  public void replayPoints(TrPostCommonImplementation impl, TrPostVisuPanel vue2d, MVProfileCourbeModel _model,
                           final TrPostSource _src, final GISZoneCollectionPoint points, final CtuluCommandContainer _cmd,
                           final ProgressionInterface _prog, H2dVariableType newVariable, int newTimeStep, boolean ecraser) {
    LineString polyligne = new LineString(points.getAttachedSequence(), GISGeometryFactory.INSTANCE);
    // -- ajout de la variable --//
    final MvProfileCourbeGroup groupVar = getGroup(newVariable, true);

    // -- on cree la nouvelle courbe pour la variable correspondante --//
    MvProfileTarget target = new TrPostProfileAction.ProfileAdapter(_src, impl.getCurrentProject());
    MvProfileBuilder builder = new MvProfileBuilderFromLine(target, impl, polyligne, new MvProfileCoteTester());
    // -- intersectionResultI --//
    EfLineIntersectionsResultsI res = builder.getDefaultRes(newVariable, impl.getMainProgression());
    // -- EfLineIntersectionsResultsBuilder --//
    EfLineIntersectionsResultsBuilder efbuilder = new EfLineIntersectionsResultsBuilder(polyligne, res,
        new MvProfileCoteTester());
    MVProfileCourbeModel newModele = new MVProfileCourbeModel(newVariable, _src, newTimeStep, efbuilder, impl
        .getMainProgression());

    // -- creation de la nouvelle courbe --//
    MvProfileCourbeTime newchild = new MvProfileCourbeTime(groupVar, newModele, target.getTimeModel());
    newchild.updateTitle();
    groupVar.addEGComponent(newchild);

    // -- si ecraser, on supprime la courbe --//
    if (ecraser) {
      EGCourbeChild courbeToRemove = getCourbeWithModel(_model);
      if (courbeToRemove != null) {
        courbeToRemove.copyGraphicsConfiguration(newchild);
      }
      if (courbeToRemove instanceof MvProfileCourbe) {
        this.removeCourbe((MvProfileCourbe) courbeToRemove, _cmd);
      } else {
        removeCurves(new EGCourbeChild[]{courbeToRemove}, _cmd);
      }
    }

    fireStructureChanged();
  }

  EGCourbeChild getCourbeWithModel(final MVProfileCourbeModel model) {
    final EGGroup g = getGroup(model.getVariable(), false);
    if (g == null) {
      return null;
    }
    for (int i = g.getChildCount() - 1; i >= 0; i--) {
      if (g.getCourbeAt(i).getInitialModel() == model) {
        return g.getCourbeAt(i);
      }
    }
    return null;
  }

  public void replayPoints(TrPostCommonImplementation impl, TrPostVisuPanel vue2d, MVProfileCourbeModel _model,
                           final TrPostSource _src, final int[] _idxToAdd, final CtuluCommandContainer _cmd,
                           final ProgressionInterface _prog, H2dVariableType newVariable, int newTimeStep, boolean ecraser) {
    // -- creation de la polyligne --//
    GISZoneCollectionPoint points = new GISZoneCollectionPoint();
    for (int i = 0; i < _idxToAdd.length; i++) {
      points.add(_src.getGrid().getPtX(_idxToAdd[i]), _src.getGrid().getPtY(_idxToAdd[i]), 0);
    }

    replayPoints(impl, vue2d, _model, _src, points, _cmd, _prog, newVariable, newTimeStep, ecraser);
  }

  public void replayPoints(TrPostCommonImplementation impl, TrPostVisuPanel vue2d, MVProfileCourbeModel _model,
                           final TrPostSource _src, final List<GrPoint> _idxToAdd, final CtuluCommandContainer _cmd,
                           final ProgressionInterface _prog, H2dVariableType newVariable, int newTimeStep, boolean ecraser) {
    // -- creation de la polyligne --//
    GISZoneCollectionPoint points = new GISZoneCollectionPoint();
    for (GrPoint point : _idxToAdd) {
      points.add(point.x_, point.y_, point.z_);
    }

    replayPoints(impl, vue2d, _model, _src, points, _cmd, _prog, newVariable, newTimeStep, ecraser);
  }

  /**
   * Methode qui supprime la courbe pour son modele du profil spatial. Supprime �galement le groupe de variable si ce dernier ne contient pas d'autres
   * courbes.
   *
   * @param model de la courbe a supprimer
   * @param mng
   */
  public void removeModele(MVProfileCourbeModel model, CtuluCommandContainer mng) {
    removeCourbe((MvProfileCourbe) getCourbeWithModel(model), mng);
  }

  /**
   * Methode qui supprime la courbe pour son modele du profil spatial. Supprime �galement le groupe de variable si ce dernier ne contient pas d'autres
   * courbes.
   *
   * @param model de la courbe a supprimer
   * @param mng
   */
  public void removeCourbe(MvProfileCourbe courbe, CtuluCommandContainer mng) {
    if (courbe == null) {
      return;
    }
    courbe.getM().isRemovable_ = true;
    // -- on recupere le groupe du modele --//
    final MvProfileCourbeGroup groupVar = getGroup(courbe.getM().getVariable(), false);
    removeCurves(new EGCourbeChild[]{courbe}, mng);
    // -- test si le groupe ne contient rien, on le degage --//
    if (groupVar.getEGChilds() == null || groupVar.getEGChilds().length == 0) {
      this.remove(groupVar);
    }
  }
//  MvProfileGridPalette paletteGrid_;

  //  public EfLineIntersectionsResultsI[] getDefaultRes() {
//    return new EfLineIntersectionsResultsI[] { resNode_ == null ? null : resNode_.getDefaultRes(),
//        resMesh_ == null ? null : resMesh_.getDefaultRes() };
//  }
  public static LineString createLineString(final EfLineIntersectionsResultsMng _res) {
    if (_res == null) {
      return null;
    }
    return createLineString(_res.getDefaultRes());
  }

  private static LineString createLineString(final EfLineIntersectionsResultsI _res) {
    final Coordinate[] cs = new Coordinate[_res.getNbIntersect()];
    for (int i = cs.length - 1; i >= 0; i--) {
      final EfLineIntersection intersect = _res.getIntersect(i);
      cs[i] = new Coordinate(intersect.getX(), intersect.getY());
    }
    return GISGeometryFactory.INSTANCE.createLineString(cs);
  }

  /**
   * Recherche le bon groupe pour les profils spatiaux. peut creer le groupe si booleen est a true.
   *
   * @param _v
   * @param _create
   */
  public MvProfileCourbeGroup getGroup(final CtuluVariable _v, final boolean _create) {
    for (int i = getNbEGObject() - 1; i >= 0; i--) {
      final EGGroup g = getGroup(i);
      if ((g instanceof MvProfileCourbeGroup) && ((MvProfileCourbeGroup) g).getVar() == _v) {
        return (MvProfileCourbeGroup) g;
      }
    }
    if (_create) {
      final MvProfileCourbeGroup g = MvProfileFillePanel.createGroupFor(_v);
      super.add(g);
      return g;
    }
    return null;
  }

  @Override
  public boolean canDeleteCourbes() {
    return true;
  }

  @Override
  public void duplicateCourbe(final CtuluCommandManager _mng, final EGCourbe _c) {
    if (!(_c instanceof MvProfileCourbeTime)) {
      return;
    }
    super.duplicateCourbe(_mng, _c);
  }

  @Override
  protected EGCourbeChild duplicateCourbe(final EGCourbeChild _c) {
    return ((MvProfileCourbeTime) _c).duplicate();
  }

  @Override
  public boolean canAddCourbe() {
    return false;
  }

//  public boolean canComputeFlowrate() {
//    final boolean isVarContained = target_.getData().isDefined(H2dVariableType.HAUTEUR_EAU)
//        && target_.getData().isDefined(H2dVariableType.VITESSE_U)
//        && target_.getData().isDefined(H2dVariableType.VITESSE_V);
//    if (isVarContained) {
//      final boolean elt = target_.getData().isElementVar(H2dVariableType.HAUTEUR_EAU);
//      if (target_.getData().isElementVar(H2dVariableType.VITESSE_U) != elt) { return false; }
//      if (target_.getData().isElementVar(H2dVariableType.VITESSE_V) != elt) { return false; }
//      if (elt && resMesh_ == null) { return false; }
//      if (!elt && resNode_ == null) { return false; }
//    }
//    return true;
//  }

  /**
   * retourne la liste des variables du graphe.
   */
  public List getShownVar() {
    final List res = new ArrayList(getNbEGObject() - 1);
    for (int i = getNbEGObject() - 1; i >= 0; i--) {
      final EGGroup g = getGroup(i);
      if (g.getChildCount() > 0 && (g instanceof MvProfileCourbeGroup)) {
        res.add(((MvProfileCourbeGroup) g).getVar());
      }
    }
    return res;
  }

  //  public void addNewCourbe(final CtuluCommandManager _cmd, final Component _c, final EGGraphe _graphe, final CtuluUI _ui) {
//    CtuluVariable[] vars = target_.getVars();
//    if (timeModel_ == null) {
//      final List l = new ArrayList(Arrays.asList(vars));
//      l.removeAll(getShownVar());
//      vars = (CtuluVariable[]) l.toArray(new CtuluVariable[l.size()]);
//
//    }
//    final MvExportChooseVarAndTime res = new MvExportChooseVarAndTime(CtuluLibSwing.createListModel(vars), timeModel_,
//        null);
//    if (timeModel_ != null) {
//      final int i = timeModel_.getSize() - 1;
//      res.getTimeSelectionModel().setSelectionInterval(i, i);
//    }
//    if (res.afficheModaleOk(_c)) {
//      new CtuluRunnable(BuResource.BU.getString("Ajouter"), _ui) {
//        @Override
//        public boolean run(final ProgressionInterface _proj) {
//          addNewCourbes(_graphe, res, _proj);
//          return true;
//        };
//      }.run();
//
//    }
//
//  }
//  void addNewCourbes(final EGGraphe _graphe, final MvExportChooseVarAndTime _res, final ProgressionInterface _prog) {
//
//    final CtuluVariable[] var = _res.getSelectedVar();
//    if (CtuluLibArray.isEmpty(var)) { return; }
//    int[] ts = _res.getSelectedTimeStepIdx();
//    if (ts == null) {
//      if (timeModel_ != null) { return; }
//      ts = new int[0];
//    }
//    final boolean isTime = timeModel_ != null;
//    for (int i = 0; i < var.length; i++) {
//      final MvProfileCourbeGroup g = getGroup(var[i], true);
//      final Color c = MvProfileFillePanel.getColorFor(i + 1, var[i]);
//      final EfLineIntersectionsResultsBuilder results = getBuilderFor(var[i]);
//      EGCourbeChild child;
//      for (int t = 0; t < ts.length; t++) {
//        final MVProfileCourbeModel model = new MVProfileCourbeModel(var[i], target_.getData(), ts[t], results, _prog);
//        if (isTime) {
//          model.setTitle(var[i].toString() + CtuluLibString.ESPACE + timeModel_.getElementAt(ts[t]));
//          child = new MvProfileCourbeTime(g, model, timeModel_);
//        } else {
//          child = new MvProfileCourbe(g, model);
//        }
//        final EGCourbeChild childFinal = child;
//        EventQueue.invokeLater(new Runnable() {
//          @Override
//          public void run() {
//            childFinal.setAspectContour(c);
//            g.addEGComponent(childFinal);
//
//          }
//
//        });
//
//      }
//    }
//    finishCreation(_graphe);
//  }
  private void finishCreation(final EGGraphe _graphe) {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        final TreePath[] old = getSelectionModel().getSelectionPaths();
        fireStructureChanged();
        if (old != null) {
          getSelectionModel().setSelectionPaths(old);
        }
      }
    });
  }

  public boolean containsTime() {
    return timeModel_ != null;
  }

  public FudaaCourbeTimeListModel getTimeModel() {
    return timeModel_;
  }

  //  public boolean isOutRemoved() {
//    if (resNode_ != null) { return resNode_.isOutRemoved(); }
//    return resMesh_.isOutRemoved();
//  }
//
//  public boolean isInit() {
//    if (resNode_ != null) { return resNode_.isInit(); }
//    return resMesh_.isInit();
//  }
//  private class UndoRes implements CtuluCommand {
//    final EfLineIntersectionsResultsMng old_;
//    final EfLineIntersectionsResultsMng oldMesh_;
//    final EfLineIntersectionsResultsMng new_;
//    final EfLineIntersectionsResultsMng newMesh_;
//    final CtuluUI ui_;
//
//    public UndoRes(final EfLineIntersectionsResultsMng _old, final EfLineIntersectionsResultsMng _new,
//        final EfLineIntersectionsResultsMng _oldMesh, final EfLineIntersectionsResultsMng _newMesh, final CtuluUI _ui) {
//      super();
//      old_ = _old;
//      ui_ = _ui;
//      new_ = _new;
//      oldMesh_ = _oldMesh;
//      newMesh_ = _newMesh;
//    }
//
//    @Override
//    public void redo() {
//      setRes(old_, oldMesh_, ui_);
//
//    }
//
//    @Override
//    public void undo() {
//      setRes(new_, newMesh_, ui_);
//
//    }
//
//  }
  protected void sendMessage(final int _nbPointRemoved, final CtuluUI _ui) {
    _ui.message(CtuluLibString.EMPTY_STRING, MvResource.getS("{0} point(s) enlev�(s)", CtuluLibString
        .getString(_nbPointRemoved)), true);
  }

  //  public void inverse(final CtuluCommandContainer _cmd, final CtuluUI _ui) {
//    final EfLineIntersectionsResultsMng newRes = resNode_ == null ? null : resNode_.inversion();
//    final EfLineIntersectionsResultsMng newResMesh = resMesh_ == null ? null : resMesh_.inversion();
//    if (_cmd != null) {
//      _cmd.addCmd(new UndoRes(resNode_, newRes, resMesh_, newResMesh, _ui));
//    }
//    setRes(newRes, newResMesh, _ui);
//    _ui.message(CtuluLibString.EMPTY_STRING, MvResource.getS("inversion r�ussie"), true);
//  }
//  public void removeExtPoint(final CtuluCommandContainer _cmd, final CtuluUI _ui) {
//    if (isOutRemoved()) {
//      sendMessage(0, _ui);
//      return;
//    }
//    final EfLineIntersectionsResultsMng newRes = resNode_ == null ? null : resNode_.removeExt();
//    final EfLineIntersectionsResultsMng newResMesh = resMesh_ == null ? null : resMesh_.removeExt();
//    if ((newResMesh == resMesh_) && (newRes == resNode_)) {
//      sendMessage(0, _ui);
//    } else {
//      if (_cmd != null) {
//        _cmd.addCmd(new UndoRes(resNode_, newRes, resMesh_, newResMesh, _ui));
//      }
//      setRes(newRes, newResMesh, _ui);
//      _ui.message(CtuluLibString.EMPTY_STRING, MvResource.getS("extraction r�ussie"), true);
//    }
//
//  }
//  public void extractFrom(final CtuluCommandContainer _cmd, final CtuluUI _ui, final int _i1, final int _i2) {
//    if (_i1 < _i2) {
//      final EfLineIntersectionsResultsMng newRes = resNode_ == null ? null : resNode_.extract(_i1, _i2);
//      final EfLineIntersectionsResultsMng newResMesh = resMesh_ == null ? null : resMesh_.extract(_i1, _i2);
//      if ((newResMesh == resMesh_) && (newRes == resNode_)) {
//        _ui.error(CtuluLibString.EMPTY_STRING, MvResource.getS("Nombre de points insuffisants"), true);
//      } else {
//        if (_cmd != null) {
//          _cmd.addCmd(new UndoRes(resNode_, newRes, resMesh_, newResMesh, _ui));
//        }
//        setRes(newRes, newResMesh, _ui);
//        _ui.message(CtuluLibString.EMPTY_STRING, MvResource.getS("extraction r�ussie"), true);
//      }
//
//    }
//  }
//  public void reinitRes(final CtuluCommandContainer _cmd, final CtuluUI _ui) {
//    if (isInit()) { return; }
//    final CtuluTaskDelegate task = _ui.createTask(DodicoLib.getS("Recherche intersections"));
//    task.start(new Runnable() {
//
//      @Override
//      public void run() {
//        final EfLineIntersectionsResultsMng newResultsNode = resNode_ == null ? null : resNode_.restoreInit(task
//            .getStateReceiver());
//        final EfLineIntersectionsResultsMng newResultsMesh = resMesh_ == null ? null : resMesh_.restoreInit(task
//            .getStateReceiver());
//        if ((newResultsMesh != resMesh_) || (newResultsNode != resNode_)) {
//          if (_cmd != null) {
//            _cmd.addCmd(new UndoRes(resNode_, newResultsNode, resMesh_, newResultsMesh, _ui));
//          }
//          setRes(newResultsNode, newResultsMesh, _ui);
//          _ui.message(CtuluLibString.EMPTY_STRING, MvResource.getS("Initialisation r�ussie"), true);
//        }
//
//      }
//
//    });
//
//  }
  public void varRemoved(final CtuluVariable[] _var) {
    if (_var != null) {
      for (int i = _var.length - 1; i >= 0; i--) {
        final EGGroup g = getGroup(_var[i], false);
        if (g != null) {
          super.remove(g);
        }
      }
    }
  }

  public void varUpdated(final CtuluVariable _var) {
    if (_var == null) {
      reupdateAllVar();
    }
    final EGGroup g = getGroup(_var, false);
    if (g != null) {
      // la variable n'existe plus
      if (this.target_.getData().isDefined(_var)) {
        for (int i = g.getChildCount() - 1; i >= 0; i--) {
          ((MvProfileCourbe) g.getCourbeAt(i)).setVar(_var, true);
        }
      } else {
        super.remove(g);
      }
    }
  }

  public void updateCache(final CtuluVariable _old, final CtuluVariable _new, final boolean _contentChanged,
                          final Set _varsUsing) {
    EGGroup g = getGroup(_old, false);
    if (g != null) {
      if (_old != _new) {
        g.setTitle(_new.getName());
        g.getAxeY().setTitre(_new.getName());
        g.fireAxeAspectChanged(g.getAxeY());
        g.fireCourbeContentChanged();
      }
      for (int i = g.getChildCount() - 1; i >= 0; i--) {
        ((MvProfileCourbe) g.getCourbeAt(i)).setVar(_new, _contentChanged);
      }
    }
    if (_varsUsing != null) {
      for (final Iterator it = _varsUsing.iterator(); it.hasNext(); ) {
        final CtuluVariable v = (CtuluVariable) it.next();
        g = getGroup(v, false);
        if (g != null) {
          for (int i = g.getChildCount() - 1; i >= 0; i--) {
            ((MvProfileCourbe) g.getCourbeAt(i)).setVar(v, _contentChanged);
          }
        }
      }
    }
  }

  public void reupdateAllVar() {
    if (isEmpty()) {
      return;
    }
    for (int i = getNbEGObject() - 1; i >= 0; i--) {
      final EGGroup g = getGroup(i);
      if (g instanceof MvProfileCourbeGroup) {
        varUpdated(((MvProfileCourbeGroup) g).getVar());
      }
    }
  }

//  protected void setRes(final EfLineIntersectionsResultsMng _newRes, final EfLineIntersectionsResultsMng _newResMesh,
//      final CtuluUI _ui) {
//    new CtuluRunnable("", _ui) {
//      @Override
//      public boolean run(final ProgressionInterface _proj) {
//        setRes(_newRes, _newResMesh, _proj);
//        return true;
//      }
//    }.run();
//
//  }
//  protected void setRes(final EfLineIntersectionsResultsMng _newRes, final EfLineIntersectionsResultsMng _newResMesh,
//      final ProgressionInterface _prog) {
//    builderNode_ = null;
//    builderMesh_ = null;
//    resNode_ = _newRes;
//    resMesh_ = _newResMesh;
//    if (resNode_ != null) builderNode_ = new EfLineIntersectionsResultsBuilder(_newRes.getInitLine(), resNode_
//        .getDefaultRes(), tester_);
//    if (resMesh_ != null) builderMesh_ = new EfLineIntersectionsResultsBuilder(_newRes.getInitLine(), resMesh_
//        .getDefaultRes(), tester_);
//    if (paletteGrid_ != null) {
//      BuLib.invokeLater(new Runnable() {
//        @Override
//        public void run() {
//          paletteGrid_.reupdateLines(getDefaultRes(),_newRes.getInitLine());
//        }
//      });
//
//    }
//    if (isEmpty()) { return; }
//    final EGCourbe[] cs = getCourbes();
//    for (int i = cs.length - 1; i >= 0; i--) {
//      final MvProfileCourbeModelInterface profileModel = ((MvProfileCourbeInterface) cs[i]).getProfileModel();
//      profileModel.setRes(profileModel.isDefinedOnMeshes() ? builderMesh_ : builderNode_, _prog);
//    }
//    BuLib.invokeLater(new Runnable() {
//      @Override
//      public void run() {
//        fireCourbeContentChanged(null, true);
//      }
//    });
//  }

  /**
   * Methode qui permet de fusionner le model courant avec un autre model. Utilsier poru la fusion de courbes spatiales et l ajout dans d autres
   * courbes.
   *
   * @param anotherModel
   * @author Adrien Hadoux
   */
  public void mergeWithAnotherTreeModel(final MvProfileTreeModel anotherModel) {
    // -- parcours de la liste des variables du graphe a fusionner --//
    for (int i = 0; i < anotherModel.target_.getVars().length; i++) {
      final H2dVariableType var = (H2dVariableType) anotherModel.target_.getVars()[i];

      // -- on recupere toutes les courbes associees a la var pour le
      // graphe merges
      final MvProfileCourbeGroup g = anotherModel.getGroup(var, false);
      if (g != null) {
        // -- on recherche le group associe a la variale dans l autre graphe
        // sinon on le cree --//
        final MvProfileCourbeGroup group = this.getGroup(var, true);

        for (int k = g.getChildCount() - 1; k >= 0; k--) {
          if (g.getCourbeAt(k).getModel() instanceof MVProfileCourbeModel) {
            if (((MVProfileCourbeModel) g.getCourbeAt(k).getModel()).getVariable() == var) {

              // -- duplication de la courbe dans le groupe --//
              final MvProfileCourbe newChild = new MvProfileCourbe(group, (MVProfileCourbeModel) g.getCourbeAt(k)
                  .getModel().duplicate());
              group.addEGComponent(newChild);
            }
          }
        }
      }
    }
    this.fireStructureChanged();
  }

  /**
   * Methode qui permet de fusionner le model courant avec un autre model. Utiliser pour la fusion de courbes spatiales et l ajout dans d autres
   * courbes.
   *
   * @param anotherModel
   * @author Adrien Hadoux
   */
  public void mergeWithAnotherScopeTreeModel(final TrPostScopCourbeTreeModel anotherModel) {
    // -- parcours de la liste des variables du graphe a fusionner --//

    // -- on recherche le group associe a la variale dans l autre graphe
    // sinon on le cree --//
    final MvProfileCourbeGroup group = this.getGroup(H2dVariableType.SANS, true);

    // -- on recupere toutes les courbes associees a la var pour le
    // graphe merges

    final EGGroup g = anotherModel.getGroupFor(H2dVariableType.SANS);
    for (int i = g.getChildCount() - 1; i >= 0; i--) {
      if (g.getCourbeAt(i).getModel() instanceof TrPostScopeCourbeModel) {

        group.addEGComponent((EGCourbeChild) g.getCourbeAt(i).duplicate(group, new EGGrapheDuplicator()));
      }
    }
    this.fireStructureChanged();
  }

//  protected MvProfileGridPalette getPaletteGrid() {
//    return paletteGrid_;
//  }
//
//  protected void setPaletteGrid(final MvProfileGridPalette _paletteGrid) {
//    paletteGrid_ = _paletteGrid;
//  }

  @Override
  public void importCourbes(final EvolutionReguliereInterface[] _crb, final CtuluCommandManager _mng,
                            final ProgressionInterface _prog) {
    // TODO Auto-generated method stub

    if (_crb == null) {
      return;
    }
    final EGGroup gr = getGroup(H2dVariableType.SANS, true);
    final List<EGCourbeChild> childs = new ArrayList<EGCourbeChild>(_crb.length);
    for (int i = 0; i < _crb.length; i++) {

      EGModel modelCourbe = null;
      if ((_crb[i] instanceof EvolutionReguliere) && ((EvolutionReguliere) _crb[i]).isScope_) {
        modelCourbe = new TrPostScopeCourbeModel((EvolutionReguliere) _crb[i]);
      } else {
        modelCourbe = new FudaaCourbeModel(new EvolutionReguliere(_crb[i]));
      }

      final EGCourbeChild child = new EGCourbeChild(gr, modelCourbe);

      // -- on met a jour si la courbe est un nuage de points ou non --//
      child.setNuagePoints(_crb[i].isNuagePoints());

      childs.add(child);
      gr.addEGComponent(child);
    }

    if (_mng != null) {
      _mng.addCmd(new CommandAddCourbesMulti(childs.toArray(new EGCourbeChild[childs.size()]), new EGGroup[]{gr}));
    }
    fireStructureChanged();
  }

  @Override
  public void finalizePersistance() {

    // -- recupere les groupes de la persistance qui doivent etre supprim� car ne sont pas des liens directs avec les
    // variables H2dVariableType du projet--//
    Set<EGGroup> listeGroupeAsupprimer = new HashSet<EGGroup>();

    for (int i = 0; i < this.getNbEGObject(); i++) {
      EGObject eg = this.getEGObject(i);
      if (this.getEGObject(i) instanceof MvProfileCourbeGroup) {
        MvProfileCourbeGroup groupe = (MvProfileCourbeGroup) this.getEGObject(i);
        if (groupe.getChildCount() > 0) {
          EGCourbeChild courbe = groupe.getCourbeAt(0);
          if (courbe.getModel() instanceof MVProfileCourbeModel) {
            groupe.setVar(((MVProfileCourbeModel) courbe.getModel()).getVariable());
          }
        }
      } else if ((this.getEGObject(i) instanceof EGGroup)) {

        EGGroup groupe = (EGGroup) this.getEGObject(i);
        ArrayList<EGCourbeChild> listeCourbeToAdd = new ArrayList<EGCourbeChild>();
        MvProfileCourbeGroup nvGroupe = null;
        for (int k = 0; k < groupe.getChildCount(); k++) {
          if (groupe.getCourbeAt(k) != null) {
            EGCourbeChild courbe = groupe.getCourbeAt(k);

            if (courbe.getModel() != null && (courbe.getModel() instanceof MVProfileCourbeModel)) {
              // List childrens=groupe.getChildren();
              MVProfileCourbeModel model = (MVProfileCourbeModel) courbe.getModel();
              // -- recherche ou creation du bon groupe correspondant--//
              nvGroupe = getGroup(model.variable_, true);

              // -- on recopie l'axe Y pour ses propri�t�s (grilles marqueurs...)--//
              if (nvGroupe != null) {
                nvGroupe.setAxeY(groupe.getAxeY());
              }
              if (!(courbe instanceof MvProfileCourbe)) {
                MvProfileCourbe newCourbe = new MvProfileCourbe(nvGroupe, model);
                courbe.copyGraphicsConfiguration(newCourbe);
                courbe = newCourbe;
              }

              listeCourbeToAdd.add(courbe);

              // -- ajout du groupe dans la liste a supprimer --//
              listeGroupeAsupprimer.add(groupe);
            } else if (courbe.getModel() != null && (courbe.getModel() instanceof TrPostScopeCourbeModel)) {
              // -- on cree le groupe sans car la courbe n'est pas profil spatioal, elel peut etre socpgen--//
              nvGroupe = getGroup(H2dVariableType.SANS, true);

              // -- on recopie l'axe Y pour ses propri�t�s (grilles marqueurs...)--//
              if (nvGroupe != null) {
                nvGroupe.setAxeY(groupe.getAxeY());
              }

              listeCourbeToAdd.add(courbe);

              // -- ajout du groupe dans la liste a supprimer --//
              listeGroupeAsupprimer.add(groupe);
            }
          }
        }

        // -- ajout de la liste des children dans le bon groupe --//
        if (nvGroupe != null) {
          nvGroupe.addEGComponent(listeCourbeToAdd);
        }
      }
    }

    this.fireStructureChanged();
    // -- ensuite on vire tout les eggroup et on les remplies dans les goup --//
    for (EGGroup groupe : listeGroupeAsupprimer) {

      super.remove(groupe);
    }
  }

  @Override
  public EGGrapheModel duplicate(final EGGrapheDuplicator _duplicator) {
    final MvProfileTreeModel duplic = new MvProfileTreeModel(this);
    duplic.setAxeX(this.getAxeX().duplicate());

    duplic.getSelectionModel().setSelectionMode(this.getSelectionModel().getSelectionMode());

    final GrapheTreeNode root = this.getGrapheTreeNode();

    for (int i = 0; i < root.components_.size(); i++) {
      duplic.add(root.getGroup(i).duplicate(_duplicator));
    }

    // -- duplication du target --//
    duplic.target_ = this.target_;
//    duplic.builderMesh_=builderMesh_;
//    duplic.builderNode_=builderNode_;
//    duplic.resMesh_=resMesh_;
//    duplic.resNode_=resNode_;

    return duplic;
  }

  @Override
  public boolean isSpatial() {
    // TODO Auto-generated method stub
    return true;
  }

  public void setTarget_(final MvProfileTarget target_) {
    this.target_ = target_;
  }

  @Override
  public Object getSpecificPersitDatas(Map Params) {
    // TODO Auto-generated method stub
    MvProfileTreeModelPersist dataPersistante = new MvProfileTreeModelPersist();

    // -- remplissage des don�nes persistantes --//
    dataPersistante.fillDataWithModel(this);

    return dataPersistante;
  }

  @Override
  public void setSpecificPersitDatas(final Object sepcPersitData, Map Params) {
    // TODO Auto-generated method stub
    MvProfileTreeModelPersist dataPersistante = (MvProfileTreeModelPersist) sepcPersitData;
    // -- remplissage du model avec les datas--//
    dataPersistante.fillModelWithData(this, Params);
  }

  /**
   * Les actions des courbes specifiques: l'ajout de variable.
   */
  @Override
  public List<EbliActionInterface> getSpecificsActionsCurvesOnly(EGGraphe _target, CtuluUI ui) {
    List<EbliActionInterface> listeActions = new ArrayList<EbliActionInterface>();

    // -- action replay data --//
    listeActions.add(new EGActionReplayDataCourbe.CourbeOnly(this, this.getSelectedComponent().getModel(), ui));
    return listeActions;
  }
}
