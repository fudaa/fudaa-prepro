package org.fudaa.fudaa.tr.post;

import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGCourbePersistBuilder;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModelDefault;

public class TrPostScopeCourbe extends EGCourbeChild {

  public TrPostScopeCourbe(EGGroup _m, EGModel _model) {
    super(_m, _model);
  }

  public TrPostScopeCourbe(EGGroup _m) {
    super(_m);
  }

  public static EGCourbeChild createCourbe(final EGGroup gr, EvolutionReguliere evol) {
    if (evol.isxAxeIsTime()) {
      TrPostScopeTimeCourbe res = new TrPostScopeTimeCourbe(gr, new TrPostScopeCourbeModel(evol), new FudaaCourbeTimeListModelDefault(evol.getArrayX(), null));
      return res;
    }
    return new TrPostScopeCourbe(gr, new TrPostScopeCourbeModel(evol));
  }

  @Override
  protected EGCourbePersistBuilder<? extends EGCourbeChild> createPersistBuilder() {
    return new TrPostScopeCourbePersistBuilder();
  }

}
