/**
 * @creation 24 mars 2004
 * @modification $Date: 2007-05-04 14:01:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.io.serafin.SerafinNewReaderInfo;
import org.fudaa.dodico.h2d.type.H2dVariableType;

import java.io.File;
import java.io.IOException;

/**
 * @author Fred Deniger
 * @version $Id: TrPostSourceSerafin.java,v 1.15 2007-05-04 14:01:52 deniger Exp $
 */
public final class TrPostSourceReaderSerafin extends TrPostSourceReaderFromFileAbstract {

  private final SerafinNewReaderInfo data_;

  String id = "";
  final long idate;

  protected TrPostSourceReaderSerafin(final File _file, final double[] _time, H2dVariableType[] _initVar, final SerafinNewReaderInfo _info, long idate) {
    super(_file, _time, _initVar);
    data_ = _info;
    this.idate = idate;
  }

  @Override
  public void close() {
    if (data_ != null) {
      data_.close();
    }
  }

  @Override
  public String getId() {
    return this.id;
  }

  /**
   * 
   * @return the value set in the serafin file.
   */
  public long getReferenceDateInMillis() {
    return idate;
  }

  @Override
  public EfData getInitData(final int _data, final int _timeStep) {
    try {
      final double[] v = data_.getDouble(_data, _timeStep);


      return data_.isVolumique() ? new EfDataElement(v) : new EfDataNode(v);
    } catch (final IOException _e) {
      FuLog.warning(_e);
      throw new Error(_e);
    }
  }

  @Override
  public double getInitData(final int _varIdx, final int _timeIdx, final int _ptIdx) throws IOException {
    return data_.getDouble(_varIdx, _timeIdx, _ptIdx);
  }

  public boolean isRubar() {
    return false;
  }

  @Override
  public boolean isVolumique() {
    return data_.isVolumique();
  }

  @Override
  public void SetId(String _id) {
    this.id = _id;

  }

}
