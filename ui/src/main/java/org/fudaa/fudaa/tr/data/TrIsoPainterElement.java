/*
 *  @creation     3 ao�t 2005
 *  @modification $Date: 2007-06-14 12:01:41 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.meshviewer.layer.MvIsoElementPainter;

/**
 * Un painter pour des variables d�finies sur les �l�ments.
 * 
 * @author Fred Deniger
 * @version $Id: TrIsoPainterElement.java,v 1.1 2007-06-14 12:01:41 deniger Exp $
 */
public class TrIsoPainterElement extends MvIsoElementPainter {

  CtuluCollectionDouble model_;

  final H2dVariableProviderInterface provider_;

  final H2dVariableType t_;
  final String name_;

  /**
   * @param _params les parametres
   * @param _v la variables
   */
  public TrIsoPainterElement(final EfGridInterface _g, final H2dVariableProviderInterface _si,
      final H2dVariableType _t, final String _name) {
    super(_g);
    t_ = _t;
    provider_ = _si;
    name_ = _name;
  }

  private void buildModel() {
    model_ = provider_.getViewedModel(t_);
  }

  @Override
  public double getMax() {
    if (model_ == null) {
      buildModel();
    }
    return model_.getMax();
  }

  @Override
  public double getMin() {
    if (model_ == null) {
      buildModel();
    }
    return model_.getMin();
  }

  @Override
  public String getNom() {
    return name_;
  }

  @Override
  public double getValue(final int _idxPoint) {
    if (model_ == null) {
      buildModel();
    }
    return model_.getValue(_idxPoint);
  }

  @Override
  public boolean isDiscrete() {
    return false;
  }

}
