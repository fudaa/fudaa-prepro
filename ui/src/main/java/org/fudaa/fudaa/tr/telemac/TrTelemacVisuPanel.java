/**
 * @creation 21 nov. 2003
 * @modification $Date: 2007-06-14 12:01:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuInternalFrame;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.dico.DicoDataType;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dParameters;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.*;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.meshviewer.layer.MvFrontierPointLayer;
import org.fudaa.fudaa.meshviewer.layer.MvGridLayerGroup;
import org.fudaa.fudaa.meshviewer.layer.MvNodeLayer;
import org.fudaa.fudaa.meshviewer.model.*;
import org.fudaa.fudaa.tr.common.TrCourbeTemporelleManager;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.*;
import org.fudaa.fudaa.tr.post.profile.MvVolumeAction;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 * @version $Id: TrTelemacVisuPanel.java,v 1.44 2007-06-14 12:01:41 deniger Exp $
 */
public final class TrTelemacVisuPanel extends TrVisuPanelEditor {
  TrTelemacInfoSenderDefault delegateInfo_;
  TrTelemacCommunProjet proj_;

  /**
   * @param _impl l'impl parente
   * @param _p le projet concern�
   */
  public TrTelemacVisuPanel(final FudaaCommonImplementation _impl, final TrTelemacCommunProjet _p) {
    super(_impl);
    proj_ = _p;
    delegateInfo_ = new TrTelemacInfoSenderDefault(proj_.getTrTelemacParams(), getEbliFormatter());
    // gestion des siphons
    addSiphonSeuil();
    addCqInfos(proj_.getTrTelemacParams().getMaillage());
    addLayerSiPoints();
    addBcLayerIfNot();
    addCqMaillage(proj_.getTrTelemacParams().getMaillage(), delegateInfo_);

    addIsoLayer();
    addActionsForSiNodeLayer();
    addActionsForNodeLayer();
    new TrTelemacVisuPanelModelListener(this);
  }

  @Override
  public EfGridInterface getGrid() {
    return proj_.getTrTelemacParams().getMaillage();
  }

  @Override
  public String editSiNodeOrElement() {
    final H2dTelemacSIProperties prop = proj_.getTelemacParametres().getSolutionsInit();
    if (!proj_.getTrTelemacParams().isRepriseActivatedByUser() || prop.getVariableDataProvider() == null) {
      return TrResource.getS("Les conditions initiales ne sont pas activ�es");
    }
    final H2dVariableType[] vars = prop.getVariableDataProvider().getVarToModify();
    if (vars == null || vars.length == 0) {
      return TrResource.getS("Aucune variable � �diter: utiliser l'action \"G�rer les variables\"");
    }
    return super.editSiNodeOrElement();
  }

  @Override
  protected void fireGridPointModified() {
    proj_.getTelemacParametres().fireGridNodeXYChanged();
  }

  protected void addLayerSiPoints() {
    buildGroupSiLayers(proj_.getTelemacParametres().getSolutionsInit());
    final BGroupeCalque gr = buildGroupSI();
    gr.putClientProperty(CtuluLib.getHelpProperty(), getImpl().buildHelpPathFor("telemac-editor-ic"));
    final TrSiNodeLayer lay = new TrSiNodeLayer(new TrSiNodeModel(proj_.getTelemacParametres().getSolutionsInit(), delegateInfo_));
    lay.setTitle(TrResource.getS("CI: Noeuds"));
    gr.add(lay);
  }

  private void addSiphonSeuil() {
    if (proj_.getTelemacFileFormatVersion().isSiphonAvailable()) {
      final BGroupeCalque gr = addSeuilSiphonGroup();
      final H2dTelemacSourceMng src = proj_.getTrTelemacParams().getTelemacParametres().getSourceSiphonMng();
      final H2dTelemacBcManager bcManager = proj_.getTrTelemacParams().getTelemacParametres().getTelemacCLManager();
      final TrTelemacSiphonLayer siphon = new TrTelemacSiphonLayer(new TrTelemacSiphonModel(src, bcManager, proj_.getTrTelemacParams().getMaillage()));
      siphon.setTitle(TrResource.getS("Siphons"));
      gr.putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Affiche et permet l'�dition des siphons"));
      siphon.putClientProperty(CtuluLib.getHelpProperty(), getImpl().buildHelpPathFor("telemac-editor-siphons"));
      siphon.setName("cqSiphons");
      gr.add(siphon);
      final TrTelemacSourceLayer la = new TrTelemacSourceLayer(new TrTelemacSourceModel(src, bcManager, proj_.getTrTelemacParams().getMaillage()));
      la.setTitle(TrResource.getS("Sources"));
      la.setName("cqSources");
      la.putClientProperty(CtuluLib.getHelpProperty(), getImpl().buildHelpPathFor("telemac-editor-sources"));
      la.putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Affiche et permet l'�dition des sources"));
      gr.add(la);
      addCalqueActions(la, la.getActions(this));
      addCalqueActions(siphon, siphon.getActions(this));
      final TrTelemacWeirLayer weir = new TrTelemacWeirLayer(new TrTelemacWeirModel(proj_.getTrTelemacParams().getTelemacParametres().getSeuil(),
          delegateInfo_));
      weir.setName("cqWeir");
      weir.putClientProperty(CtuluLib.getHelpProperty(), getImpl().buildHelpPathFor("telemac-editor-seuils"));
      weir.putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Edition des seuils"));
      weir.setTitle(H2dResource.getS("Seuils"));
      addCalqueActions(weir, weir.getActions(this));
      gr.add(weir);
    }
  }

  protected void addActionsForNodeLayer() {
    final EbliActionSimple acEditerCommon = new NodalEditAction();
    final String enableCondition = "<ul><li>" + TrResource.getS("Cr�er des propri�t�s nodales") + "</li><li>"
        + TrResource.getS("S�lectionner au moins un noeud") + "</li></ul>";
    acEditerCommon.putValue(EbliActionInterface.UNABLE_TOOLTIP, enableCondition);
    final EbliActionSimple acInitFromGeom = new NodalEditGeomAction();
    acInitFromGeom.putValue(EbliActionInterface.UNABLE_TOOLTIP, TrResource.getS("Cr�er des propri�t�s nodales"));
    final EbliActionSimple a1 = new EbliActionSimple(TrResource.getS("G�rer les propri�t�s nodales"), null, "EDIT_GRID_POINT_INIT") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        new TrTelemacDefaultPropertiesEditor(TrTelemacVisuPanel.this).afficheModale(getFrame(), super.getTitle());
      }
    };
    final MvNodeLayer l = getGridGroup().getPointLayer();
    final String desc = TrResource.getS("Affiche les noeuds du maillage") + "<br>" + TrResource.getS("Permet de modifier les propri�t�s nodales");
    l.putClientProperty(CtuluLib.getHelpProperty(), getImpl().buildHelpPathFor("telemac-editor-params-nodal"));
    l.putClientProperty(Action.SHORT_DESCRIPTION, desc);
    addCalqueActions(l, new EbliActionInterface[]{acEditerCommon, acInitFromGeom, null, a1, new TrEditPointAction(this), new TrValidGridAction(this)});
  }

  protected TrTelemacCommunParametres getTrParams() {
    return proj_.getTrTelemacParams();
  }

  protected void addActionsForSiNodeLayer() {
    final EbliActionSimple activeSi = new EbliActionSimple(TrResource.getS("Activer les conditions initiales"), null, "ACTIVE_SI_POINT_INIT") {
      @Override
      public void actionPerformed(final ActionEvent _e) {

        proj_.getDicoParams().setCheckedValue(proj_.getTrTelemacParams().getRepriseBooleanKw(), DicoDataType.Binaire.TRUE_VALUE, getCmdMng());
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("D�j� activ�es");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(!proj_.getTrTelemacParams().isRepriseActivatedByUser());
      }
    };
    activeSi.setDefaultToolTip(TrResource.getS("Active la reprise de calcul (ou conditions initiales)"));
    final EbliActionSimple desactiveSi = new EbliActionSimple(TrResource.getS("D�sactiver les conditions initiales"), null, "ACTIVE_SI_POINT_INIT") {
      @Override
      public void actionPerformed(final ActionEvent _e) {

        proj_.getDicoParams().setCheckedValue(proj_.getTrTelemacParams().getRepriseBooleanKw(), DicoDataType.Binaire.FALSE_VALUE, getCmdMng());
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("D�j� unactiv�es");
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(proj_.getTrTelemacParams().isRepriseActivatedByUser());
      }
    };
    desactiveSi.setDefaultToolTip(TrResource.getS("D�sactive la reprise de calcul (ou conditions initiales)"));
    final EbliActionSimple initSi = new EbliActionSimple(TrResource.getS("G�rer les variables"), null, "EDIT_SI_POINT_INIT") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        new TrTelemacDefaultPropertiesEditor(TrTelemacVisuPanel.this).afficheModale(getFrame(), super.getTitle());
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(proj_.getTrTelemacParams().isRepriseActivatedByUser());
      }
    };
    final TrSiProfilLayer pro = (TrSiProfilLayer) getGroupSI().getCalqueParNom(TrSiProfilLayer.getDefaultName());
    final String enableCondition = "<ul><li>" + TrResource.getS("Activer les conditions initiales") + "</li><li>"
        + TrResource.getS("Cr�er des variables pour les conditions initiales") + "</li><li>" + TrResource.getS("S�lectionner au moins un noeud")
        + "</li></ul>";
    final EbliActionSimple acEditerCommon = new SiEditAction();
    acEditerCommon.putValue(EbliActionInterface.UNABLE_TOOLTIP, enableCondition);
    final EbliActionSimple acSitest = new SiCheckHauteurEau() {
      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(((H2dTelemacSIProperties) (getSiPointLayer().getSource())).containsValuesFor(H2dVariableType.COTE_EAU));
      }
    };
    final EbliActionSimple acInitFromGeom = new SiEditGeomAction();
    acInitFromGeom.putValue(EbliActionInterface.UNABLE_TOOLTIP, TrResource.getS("Cr�er des variables pour les conditions initiales"));

    final EbliActionInterface compute = pro == null ? null : pro.getComputeAction();
    addCalqueActions((BCalque) getSiPointLayer(), new EbliActionInterface[]{acEditerCommon, acInitFromGeom, null, compute, acSitest,
        new SiClearHAction(), initSi, null, activeSi, desactiveSi, null,
        new MvVolumeAction(getParams().createGridDataAdapter(), TrTelemacVisuPanel.this)});
  }

  @Override
  public void addCqMaillage(final EfGridInterface _m, final MvInfoDelegate _d) {
    final MvNodeModelDefault pt = new TrTelemacNodeModel(_m, proj_.getTelemacParametres().getNodalProperties(), _d);
    pt.setDelegate(_d);
    final MvElementModelDefault elt = new MvElementModelDefault(_m);
    elt.setDelegate(_d);
    addCqMaillage(_m, pt, elt);
  }

  @Override
  protected void addCqMaillage(final EfGridInterface _m, final MvNodeModel _ptModel, final MvElementModel _eltModel) {
    final MvNodeLayer node = new TrNodeLayerEditable(_ptModel, this) {
      @Override
      public EbliFindExpressionContainerInterface getExpressionContainer() {
        return ((TrTelemacNodeModel) super.modele_).getExpressionContainer();
      }
    };
    if (getGridGroup() != null) {
      return;
    }
    addCqGroupeMaillage(new MvGridLayerGroup(_m, node, new MvElementLayer(_eltModel)));
  }

  protected BGroupeCalque addSeuilSiphonGroup() {
    BGroupeCalque gr = getSeuilSiphonGroup();
    if (gr != null) {
      return gr;
    }
    gr = new BGroupeCalque();
    gr.setTitle(TrResource.getS("Seuils / Siphons"));
    gr.setName("grWeirCulvert");
    addCalque(gr);
    return gr;
  }

  /*
   * protected void buildFindDialog(){ MvFindAction[] ac = new MvFindAction[4]; ac[0] = new MvFindActionNodeElt(this,
   * getGridGroup().getPointLayer()); ac[1] = new MvFindActionNodeElt(this, getGridGroup().getPolygonLayer()); ac[2] =
   * new MVFindActionFrontierPt(this, getLayerBcPoint()); ac[3] = new TrTelemacFindBoundary(this,
   * (TrTelemacBcBoundaryBlockLayer) getLayerBcBoundary()); findDialog_ = new MvFindDialog(this, ac); }
   */

  protected TrTelemacBcBoundaryBlockLayer createBcBlockLayer() {
    final TrTelemacBoundaryBlockLayer r = new TrTelemacBoundaryBlockLayer(this);
    r.putClientProperty(Action.SHORT_DESCRIPTION, TrResource.getS("Affiche et permet l'�dition des bords"));
    r.putClientProperty(CtuluLib.getHelpProperty(), "telemac-editor-bc");
    getBcMng().addClChangedListener(r);
    return r;
  }

  protected MvFrontierPointLayer createBcPointLayer() {
    return new TrTelemacBcPointLayer(proj_.getTrTelemacParams().getTelemacParametres(), delegateInfo_);
  }

  protected void createProfilFille() {
    final TrTelemacBcBoundaryBlockLayer layer = (TrTelemacBcBoundaryBlockLayer) getLayerBcBoundary();
    final int i = layer.isSelectionInUniqueBloc();
    if (i < 0) {
      return;
    }
    final CtuluListSelectionInterface selec = layer.getLayerSelectionMulti().getSelection(i);
    if ((selec != null) && (selec.isOnlyOnIndexSelected())) {
      final int idxOnFr = selec.getMaxIndex();
      final H2dTelemacBcManager mng = getBcMng();
      final H2dTelemacBordParFrontiere fr = (H2dTelemacBordParFrontiere) mng.getBlockFrontier(i);
      final H2dTelemacBoundary b = (H2dTelemacBoundary) mng.getBoundary(i, idxOnFr);
      final H2dVariableType[] ls = mng.getVariablesForPoint(b);
      if (ls != null && ls.length > 0) {
        final TrTelemacBoundaryGrapheSimpleModel f = new TrTelemacBoundaryGrapheSimpleModel(mng, fr, b);
        final BuInternalFrame frame = f.buildFille(getImpl(), proj_.getInformationsDocument());
        frame.pack();
        getImpl().addInternalFrame(frame);
      }
    }
  }

  protected String editBordBcPoint() {
    final MvFrontierPointLayer layer = getLayerBcPoint();
    final int i = layer.isSelectionInUniqueBloc();
    if (i >= 0) {
      final H2dTelemacBoundary b = getBcMng().getUniqueSelectedBoundary(i, layer.getLayerSelectionMulti().getSelection(i).getSelectedIndex());
      editBoundary(b, new CtuluCommandCompositeInverse());
    } else {
      return TrResource.getS("Une seule fronti�re doit �tre s�lectionn�e");
    }
    return null;
  }

  protected void editBoundary(final H2dTelemacBoundary _b) {
    editBoundary(_b, new CtuluCommandCompositeInverse());
  }

  protected void editBoundary(final H2dTelemacBoundary _b, final CtuluCommandCompositeInverse _cmd) {
    if (_b == null) {
      return;
    }
    final TrCourbeTemporelleManager e = proj_.getEvolMng();
    if (CtuluDialogPanel.isOkResponse(new TrTelemacBoundaryEditor(getBcMng(), _b, _cmd, e).afficheModale(getFrame()))) {
      if (!_cmd.isEmpty()) {
        getCmdMng().addCmd(_cmd.getSimplify());
      }
    } else {
      _cmd.undo();
    }
  }

  protected void editBoundary(final int _idxFr, final int _idxOnFr) {
    editBoundary(_idxFr, _idxOnFr, new CtuluCommandCompositeInverse());
  }

  protected void editBoundary(final int _idxFr, final int _idxOnFr, final CtuluCommandCompositeInverse _cmd) {
    final H2dTelemacBoundary b = (H2dTelemacBoundary) getBcMng().getBoundary(_idxFr, _idxOnFr);
    editBoundary(b, _cmd);
  }

  @Override
  public String getProjectName() {
    return proj_.getParamsFile().getAbsolutePath();
  }

  protected BGroupeCalque getSeuilSiphonGroup() {
    return (BGroupeCalque) getDonneesCalqueByName("grWeirCulvert");
  }

  protected H2dTelemacParameters getTelemacParameters() {
    return proj_.getTrTelemacParams().getTelemacParametres();
  }

  protected void increaseLiquidPoint() {
    final MvFrontierPointLayer layer = getLayerBcPoint();
    final int i = layer.isSelectionInUniqueBloc();
    if (i >= 0) {
      final int[] debFin = layer.isSelectionContiguous(i);
      if (debFin != null) {
        final H2dTelemacBcManager cl = getBcMng();
        if (CtuluLibMessage.DEBUG) {
          CtuluLibMessage.debug("increase liquid [" + debFin[0] + CtuluLibString.VIR + debFin[1] + "]");
        }
        final CtuluCommand cmd = cl.increaseLiquidBorder(i, debFin[0], debFin[1]);
        if (cmd != null) {
          if (CtuluLibMessage.DEBUG) {
            CtuluLibMessage.debug(cl.getBlockFrontier(i).toString());
          }
          getCmdMng().addCmd(cmd);
          return;
        }
      }
    }
    getImpl().message(
        "<html>"
            + TrResource.TR.getString("Pour agrandir un bord liquide:<br> s�lectionner des noeuds adjacents,"
            + "<br> un seul bord liquide doit �tre s�lectionn�," + "<br> la taille d'un bord solide est au minimum de 3 noeuds.") + "</html>");
  }

  protected void insertLiquidPointsBcPoint(final boolean _editor) {
    final MvFrontierPointLayer layer = getLayerBcPoint();
    final int i = layer.isSelectionInUniqueBloc();
    if (i >= 0) {
      final int[] debFin = layer.isSelectionContiguous(i);
      if (debFin != null) {
        final H2dTelemacBcManager cl = getBcMng();
        if (CtuluLibMessage.DEBUG) {
          CtuluLibMessage.debug("add liquid [" + debFin[0] + CtuluLibString.VIR + debFin[1] + "]");
        }
        final H2dBoundaryType toAdd = cl.getDefaultLiquidType();
        if (toAdd == null) {
          getImpl().error("No liquid boundary's type found");
          return;
        }
        final CtuluCommand cmd = cl.addLiquidPts(debFin[0], debFin[1], i, toAdd);
        if (cmd != null) {
          if (CtuluLibMessage.DEBUG) {
            CtuluLibMessage.debug("OK for " + cl.getBlockFrontier(i).toString());
          }
          if (_editor) {
            final CtuluCommandCompositeInverse cmdR = new CtuluCommandCompositeInverse();
            cmdR.addCmd(cmd);
            final H2dTelemacBoundary b = cl.getLiquidBord(i, debFin[0], debFin[1]);
            if (b == null) {
              FuLog.warning(new Throwable());
              cmd.undo();
            }
            editBoundary(b, cmdR);
            return;
          }
          getCmdMng().addCmd(cmd);
          return;
        }
      }
    }
    getImpl().message(
        "<html>"
            + TrResource.TR.getString("Pour ins�rer des noeuds liquides:" + "<br> s�lectionner des noeuds adjacents (au moins 2),<br> "
            + "les noeuds doivent �tre strictement inclus dans un bord solide," + "<br> la taille d'un bord solide est au minimum de 3 noeuds.")
            + "</html>");
  }

  protected void removeLiquidPointsBcPoint() {
    final MvFrontierPointLayer layer = getLayerBcPoint();
    final int i = layer.isSelectionInUniqueBloc();
    CtuluCommand cmd = null;
    if (i >= 0) {
      final int[] debFin = layer.isSelectionContiguous(i);
      if (debFin != null) {
        if (CtuluLibMessage.DEBUG) {
          CtuluLibMessage.debug("remove liquid [" + debFin[0] + CtuluLibString.VIR + debFin[1] + "]");
        }
        final H2dTelemacBcManager cl = getBcMng();
        cmd = cl.removeLiquidPts(debFin[0], debFin[1], i);
        if (cmd != null && CtuluLibMessage.DEBUG) {
          CtuluLibMessage.debug(cl.getBlockFrontier(i).toString());
        }
      }
    }
    if (cmd == null) {
      getImpl().message(
          "<html>"
              + TrResource.TR.getString("Pour supprimer des noeuds liquides:" + "<br> choisir des noeuds adjacents appartenant � la m�me fronti�re")
              + "</html>");
    } else {
      getCmdMng().addCmd(cmd);
    }
  }

  /**
   * Ajoute les calques cl si non presents.
   */
  public void addBcLayerIfNot() {
    if (getGroupBoundary() != null) {
      return;
    }
    if (getBcMng() != null) {
      final TrBcLayerGroup gpCL = buildGroupConlim();
      final MvFrontierPointLayer cqCL = createBcPointLayer();
      final TrTelemacBcBoundaryBlockLayer cqBord = createBcBlockLayer();
      gpCL.addBcBoundaryLayer(cqBord);
      gpCL.addBcPointLayer(cqCL);
      addCalqueActions(cqCL, buildBcPointActions());
      addCalqueActions(cqBord, buildBcBoundaryBlockActions());
    }
  }

  public static String getEditBordEnableCond() {
    return TrResource.getS("Un seul bord doit �tre s�lectionn�");
  }

  public EbliActionInterface[] buildBcBoundaryBlockActions() {
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("build bcBoundaryBlockLayer actions");
    }
    final List<EbliActionInterface> actions = new ArrayList<EbliActionInterface>();
    actions.add(new EbliActionSimple(TrResource.getS("Editer le bord s�lectionn�"), null, "FRONTIER_EDIT") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        editBoundary();
      }

      @Override
      public String getEnableCondition() {
        return getEditBordEnableCond();
      }

      @Override
      public void updateStateBeforeShow() {
        boolean enable = false;
        final TrTelemacBcBoundaryBlockLayer layer = (TrTelemacBcBoundaryBlockLayer) getLayerBcBoundary();
        final int i = layer.isSelectionInUniqueBloc();
        if (i >= 0) {
          final CtuluListSelectionInterface selec = layer.getLayerSelectionMulti().getSelection(i);
          if ((selec != null) && (selec.isOnlyOnIndexSelected())) {
            enable = true;
          }
        }
        super.setEnabled(enable);
      }
    });
    actions.add(new EbliActionSimple(TrResource.getS("Supprimer un bord liquide"), null, "FRONTIER_REMOVE") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        final TrBcBoundaryLayerAbstract layer = (TrBcBoundaryLayerAbstract) getLayerBcBoundary();
        final int i = layer.isSelectionInUniqueBloc();
        if (i < 0) {
          return;
        }
        final CtuluListSelectionInterface l = layer.getLayerSelectionMulti().getSelection(i);
        if ((l != null) && (l.isOnlyOnIndexSelected())) {
          final H2dTelemacBcManager cl = getBcMng();
          final H2dTelemacBoundary b = (H2dTelemacBoundary) cl.getBoundary(i, l.getMaxIndex());
          if (b.getType().isLiquide()) {
            if (CtuluLibMessage.DEBUG) {
              CtuluLibMessage.debug("remove b " + b + " index =" + l.getMaxIndex());
            }
            getCmdMng().addCmd(cl.removeLiquidBorder(b));
          }
        }
      }

      @Override
      public String getEnableCondition() {
        return TrResource.getS("Un seul bord liquide doit �tre s�lectionn�");
      }

      @Override
      public void updateStateBeforeShow() {
        boolean enable = false;
        final TrTelemacBcBoundaryBlockLayer layer = (TrTelemacBcBoundaryBlockLayer) getLayerBcBoundary();
        final int i = layer.isSelectionInUniqueBloc();
        if (i >= 0) {
          final CtuluListSelectionInterface selec = layer.getLayerSelectionMulti().getSelection(i);
          if ((selec != null) && (selec.isOnlyOnIndexSelected())) {
            final H2dTelemacBcManager cl = getBcMng();
            final H2dTelemacBoundary b = (H2dTelemacBoundary) cl.getBoundary(i, selec.getMaxIndex());
            if (b.getType().isLiquide()) {
              enable = true;
            }
          }
        }
        super.setEnabled(enable);
      }
    });
    actions.add(new EbliActionSimple(TrResource.getS("Visualiser les profils"), null, "PROFIL_EDIT") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        createProfilFille();
      }

      @Override
      public String getEnableCondition() {
        return getEditBordEnableCond() + "<br>" + TrResource.getS("Ce bord doit contenir des variables �ditables spatiallement");
      }

      @Override
      public void updateStateBeforeShow() {
        boolean enable = false;
        final TrTelemacBcBoundaryBlockLayer layer = (TrTelemacBcBoundaryBlockLayer) getLayerBcBoundary();
        final int i = layer.isSelectionInUniqueBloc();
        if (i >= 0) {
          final CtuluListSelectionInterface selec = layer.getLayerSelectionMulti().getSelection(i);
          if ((selec != null) && (selec.isOnlyOnIndexSelected())) {
            final int idxOnFr = selec.getMaxIndex();
            final H2dTelemacBcManager mng = getBcMng();
            final H2dTelemacBoundary b = (H2dTelemacBoundary) mng.getBoundary(i, idxOnFr);
            final H2dVariableType[] ls = mng.getVariablesForPoint(b);
            if (ls != null && ls.length > 0) {
              enable = true;
            }
          }
        }
        super.setEnabled(enable);
      }
    });
    actions.add(null);
    actions.add(new TrTelemacSourceAddTracerAction(getTelemacParameters().getTracerManager(), getCmdMng()));
    actions.add(new TrTelemacSourceModifyTracerAction(getTelemacParameters().getTracerManager(), getCmdMng()));
    actions.add(new TrTelemacSourceRemoveTracerAction(getTelemacParameters().getTracerManager(), getCmdMng()));
    actions.add(null);
    return actions.toArray(new EbliActionInterface[actions.size()]);
  }

  /**
   * build the actions (if needed) and return them.
   */
  public EbliActionInterface[] buildBcPointActions() {
    final List r = new ArrayList(9);
    // Edit the selected boundary points
    if (isBcPointEditable()) {
      r.add(new EbliActionSimple(TrResource.getS("Editer noeuds"), null, "FRONTIER_EDIT") {
        @Override
        public void actionPerformed(final ActionEvent _arg) {
          editBcPoint();
        }

        @Override
        public String getEnableCondition() {
          return EbliLib.getS("S�lectionner au moins un noeud");
        }

        @Override
        public void updateStateBeforeShow() {
          super.setEnabled(!getLayerBcPoint().isSelectionEmpty());
        }
      });
    }
    // Edit the selected boundary
    r.add(new EbliActionSimple(TrResource.getS("Editer bords"), null, "FRONTIER_EDIT") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        editBordBcPoint();
      }

      @Override
      public String getEnableCondition() {
        return EbliLib.getS("Un seul bord doit �tre s�lectionn�");
      }

      @Override
      public void updateStateBeforeShow() {
        boolean valide = false;
        final MvFrontierPointLayer layer = getLayerBcPoint();
        final int i = layer.isSelectionInUniqueBloc();
        if (i >= 0) {
          valide = getBcMng().getUniqueSelectedBoundary(i, layer.getLayerSelectionMulti().getSelection(i).getSelectedIndex()) != null;
        }
        super.setEnabled(valide);
      }
    });
    // separator
    r.add(null);
    r.add(new EbliActionSimple(TrResource.getS("Supprimer les noeuds liquides"), null, "REMOVE_LIQUID_POINT") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        removeLiquidPointsBcPoint();
      }

      @Override
      public String getEnableCondition() {
        return TrResource.TR.getString("Pour supprimer des noeuds liquides:" + "<br> choisir des noeuds adjacents appartenant � la m�me fronti�re");
      }

      @Override
      public void updateStateBeforeShow() {
        boolean enable = false;
        final MvFrontierPointLayer layer = getLayerBcPoint();
        final int i = layer.isSelectionInUniqueBloc();
        if (i >= 0) {
          enable = layer.isSelectionContiguous(i) != null;
        }
        super.setEnabled(enable);
      }
    });
    r.add(new EbliActionSimple(TrResource.getS("Ins�rer des noeuds liquides..."), null, "ADD_LIQUID_POINTS") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        insertLiquidPointsBcPoint(true);
      }

      @Override
      public String getEnableCondition() {
        return TrResource.TR.getString("Pour ins�rer des noeuds liquides:<br> " + "s�lectionner des noeuds adjacents (au moins 2),<br> "
            + "les noeuds doivent �tre strictement inclus dans un bord solide,<br> " + "la taille d'un bord solide est au minimum de 3 noeuds.");
      }

      @Override
      public void updateStateBeforeShow() {
        boolean enable = false;
        final MvFrontierPointLayer layer = getLayerBcPoint();
        final int i = layer.isSelectionInUniqueBloc();
        if (i >= 0) {
          enable = layer.isSelectionContiguous(i) != null;
        }
        super.setEnabled(enable);
      }
    });
    r.add(new EbliActionSimple(TrResource.getS("Agrandir un bord liquide"), null, "INCREASE_LIQUID_BORDER") {
      @Override
      public void actionPerformed(final ActionEvent _arg) {
        increaseLiquidPoint();
      }

      @Override
      public String getEnableCondition() {
        return TrResource.TR.getString("Pour agrandir un bord liquide:<br> s�lectionner des noeuds adjacents,"
            + "<br> un seul bord liquide doit �tre s�lectionn�,<br> " + "la taille d'un bord solide est au minimum de 3 noeuds.");
      }

      @Override
      public void updateStateBeforeShow() {
        boolean enable = false;
        final MvFrontierPointLayer layer = getLayerBcPoint();
        final int i = layer.isSelectionInUniqueBloc();
        if (i >= 0) {
          enable = layer.isSelectionContiguous(i) != null;
        }
        super.setEnabled(enable);
      }
    });
    r.add(null);
    r.add(getLayerBcPoint().create3DAction(getImpl(), getCmdMng()));
    r.add(null);
    r.add(new TrTelemacSourceAddTracerAction(getTelemacParameters().getTracerManager(), getCmdMng()));
    r.add(new TrTelemacSourceModifyTracerAction(getTelemacParameters().getTracerManager(), getCmdMng()));
    r.add(new TrTelemacSourceRemoveTracerAction(getTelemacParameters().getTracerManager(), getCmdMng()));
    r.add(null);
    return (EbliActionInterface[]) r.toArray(new EbliActionInterface[r.size()]);
  }

  @Override
  public String editBcPoint() {
    final MvFrontierPointLayer layer = getLayerBcPoint();
    final int i = layer.isSelectionInUniqueBloc();
    if (i >= 0) {
      final int[] select = layer.getLayerSelectionMulti().getSelection(i).getSelectedIndex();
      final H2dTelemacBoundary b = getBcMng().getUniqueSelectedBoundary(i, select);
      if (b != null) {
        CtuluDialogPanel pn = null;
        if (b.getType().isSolide()) {
          pn = new TrTelemacSolidBoundaryPtsEditor(getCmdMng(), getFrontier(i), b, select, getBcMng());
        } else {
          pn = new TrTelemacBoundaryPtsEditor(getCmdMng(), getFrontier(i), b, select, getBcMng());
        }
        pn.afficheModale(getFrame(), TrResource.getS("Editer noeuds"));
      }
    } else {
      return TrResource.getS("Une seule fronti�re doit �tre s�lectionn�e");
    }
    return null;
  }

  public String editBoundary() {
    final TrTelemacBcBoundaryBlockLayer layer = (TrTelemacBcBoundaryBlockLayer) getLayerBcBoundary();
    final int i = layer.isSelectionInUniqueBloc();
    if (i >= 0) {

      final CtuluListSelectionInterface selec = layer.getLayerSelectionMulti().getSelection(i);
      if ((selec != null) && (selec.isOnlyOnIndexSelected())) {
        editBoundary(i, selec.getMaxIndex());
        return null;
      }
    }
    return getEditBordEnableCond();
  }

  @Override
  public String editGridPoly() {
    return null;
  }

  /**
   * @return le mng de cl.
   */
  public H2dTelemacBcManager getBcMng() {
    return proj_.getTrTelemacParams().getClManager();
  }

  /**
   * @param _i l'indice de la frontiere
   * @return la frontiere demande
   */
  public H2dTelemacBordParFrontiere getFrontier(final int _i) {
    return (H2dTelemacBordParFrontiere) getBcMng().getBlockFrontier(_i);
  }

  @Override
  public boolean isBcPointEditable() {
    return proj_.getTelemacFileFormatVersion().isBcPointEditable();
  }

  @Override
  public boolean isGridPointEditable() {
    return proj_.getTelemacParametres().isNodalPropertiesModifiable();
  }

  @Override
  public H2dParameters getParams() {
    return proj_.getTelemacParametres();
  }
}
