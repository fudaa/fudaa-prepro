/**
 * @creation 21 oct. 2003
 * @modification $Date: 2007-05-04 14:01:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.telemac;

import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.dico.DicoParamsListener;
import org.fudaa.dodico.h2d.H2dSIListener;
import org.fudaa.dodico.h2d.telemac.H2dTelemacNodalPropListener;
import org.fudaa.dodico.h2d.telemac.H2dTelemacParameters;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuilListener;
import org.fudaa.dodico.h2d.telemac.H2dtelemacSiponListener;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author deniger
 * @version $Id: TrTelemacVisuPanelModelListener.java,v 1.2 2007-05-04 14:01:46 deniger Exp $
 */
public class TrTelemacVisuPanelModelListener implements DicoParamsListener, H2dTelemacSeuilListener, H2dSIListener,
    H2dtelemacSiponListener, H2dTelemacNodalPropListener {

  final TrTelemacVisuPanel pn_;

  public TrTelemacVisuPanelModelListener(final TrTelemacVisuPanel _pn) {
    super();
    pn_ = _pn;
    final H2dTelemacParameters telemacParameters = pn_.getTelemacParameters();
    telemacParameters.getDicoParams().addModelListener(this);
    if (telemacParameters.getSeuil() != null) telemacParameters.getSeuil().addListener(this);
    telemacParameters.getSolutionsInit().addListener(this);
    if (telemacParameters.getSourceSiphonMng() != null) telemacParameters.getSourceSiphonMng().addListener(this);
    telemacParameters.getTelemacNodalProperties().addListener(this);

  }

  private void majSiAndList() {
    pn_.updateIsoVariables();
    pn_.updateInfoAndIso();
  }

  private void updateSi(final DicoEntite _ent) {
    if (pn_.getTrParams().getRepriseBooleanKw() == _ent) {
      pn_.updateIsoVariables();
    }
  }

  @Override
  public void culvertAdded() {
    pn_.updateInfoAndIso();
  }

  @Override
  public void culvertRemoved() {
    pn_.updateInfoAndIso();
  }

  @Override
  public void culvertValueChanged(final boolean _connexionChanged) {
    pn_.updateInfoAndIso();
  }

  @Override
  public void dicoParamsEntiteAdded(final DicoParams _cas, final DicoEntite _ent) {
    pn_.updateInfoAndIso();
    updateSi(_ent);
  }

  @Override
  public void dicoParamsEntiteCommentUpdated(final DicoParams _cas, final DicoEntite _ent) {}

  @Override
  public void dicoParamsEntiteRemoved(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    pn_.updateInfoAndIso();
    updateSi(_ent);
  }

  @Override
  public void dicoParamsEntiteUpdated(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    pn_.updateInfoAndIso();
    updateSi(_ent);
  }

  @Override
  public void dicoParamsValidStateEntiteUpdated(final DicoParams _cas, final DicoEntite _ent) {}

  @Override
  public void dicoParamsVersionChanged(final DicoParams _cas) {}

  @Override
  public void nodalPropAdded(final H2dVariableType _t) {
    pn_.updateIsoVariables();
  }

  @Override
  public void nodalPropChanged(final H2dVariableType _t) {
    pn_.updateInfoAndIso();

  }

  @Override
  public void nodalPropRemoved(final H2dVariableType _t) {
    pn_.updateIsoVariables();
  }

  @Override
  public void seuilAdded() {
    pn_.updateInfoAndIso();

  }

  @Override
  public void seuilChanged(final boolean _xyChanged) {
    pn_.updateInfoAndIso();
  }

  @Override
  public void seuilRemoved() {
    pn_.updateInfoAndIso();
  }

  @Override
  public void siAdded(final H2dVariableType _var) {
    majSiAndList();
  }

  @Override
  public void siChanged(final H2dVariableType _var) {
    pn_.updateInfoAndIso();
  }

  @Override
  public void siRemoved(final H2dVariableType _var) {
    majSiAndList();
  }

  @Override
  public void sourcesChanged(final DicoEntite _v) {
    pn_.updateInfoComponent();

  }
}