package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.ZEbliCalquesPanelPersistManager;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.calque.CalqueLegendeWidgetAdapter;
import org.fudaa.fudaa.commun.save.FudaaFilleVisuPersistence;
import org.fudaa.fudaa.commun.save.FudaaSaveZipLoader;
import org.fudaa.fudaa.commun.save.FudaaSaveZipWriter;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.persist.TrPostReloadParameter;

/**
 * Version persistante du trpostVisuPanel. Peut etre sauvegard�, a partir d'un trpostvisupanel et permet de sauvegarder un autre
 * trpostvisupanel
 *
 * @author Adrien Hadoux
 */
public class TrPostVisuPanelPersistManager implements ZEbliCalquesPanelPersistManager {

  boolean simplified;
  String idSource;
  GrBoite zoom_;
  public Map<String, String> infos_ = new HashMap<String, String>();

  TrPostVisuPanelPersistManager() {
  }

  @Override
  public void fillInfoWith(final ZEbliCalquesPanel p, final File dirGeneralData) {
    final TrPostVisuPanel panel = (TrPostVisuPanel) p;
    simplified = panel.simplifiedInit;
    idSource = panel.getSource().getId();
    zoom_ = panel.getVueCalque().getViewBoite();
    FudaaSaveZipWriter writer = null;
    try {
      dirGeneralData.mkdirs();
      writer = new FudaaSaveZipWriter(new File(dirGeneralData, "calques.zip"));
      new FudaaFilleVisuPersistence(panel).saveIn(writer, panel.getCtuluUI().getMainProgression());
    } catch (final IOException e) {
      FuLog.error(e);
    } finally {
      if (writer != null) {
        writer.safeClose();
      }
    }

  }

  /**
   * Methode qui reconstruit le trpostvisupanel a partir des donn�es persistantes.
   *
   * @param parameters
   * @return
   */
  @Override
  public ZEbliCalquesPanel generateCalquePanel(final Map parameters, final File dirGeneralData) {

    final TrPostProjet projet = (TrPostProjet) parameters.get(TrPostReloadParameter.POST_PROJET);

    if (projet == null) {
      return null;
    }

    // -- etape 1: recherche du source qui contient le path donn� --//
    TrPostSource src = projet.getSources().findSourceById(idSource);

    // -- etape 2 si source pas trouv�, chargement du source --//
    if (src == null) {
      ((List<String>) parameters.get("errorMsg")).add(TrResource.getS("Erreur, la frame calque ne trouve pas le fichier r�sultat qui correspond � l'ID ") + idSource);
      if (projet.getSources().isNotEmpty()) {
        ((List<String>) parameters.get("errorMsg")).add("Le calque chargera en contrepartie le fichier r�sultat "
                + projet.getSource(0).getFiles().iterator().next().getPath());
        // return null;
        src = projet.getSource(0);
      } else {
        return null;
      }
    }

    // -- etape 3: creation du legende adapter --//
    final CalqueLegendeWidgetAdapter legendeCalque = new CalqueLegendeWidgetAdapter((EbliScene) parameters.get("scene"));

    // -- etape 3: creation du panel --//
    if (src == null) {
      return null;
    }

    final TrPostVisuPanel newPanel = (src instanceof TrPostSourceRubar) ? new TrPostVisuPanelRubar(projet.getImpl(), projet, legendeCalque,
            (TrPostSourceRubar) src) : new TrPostVisuPanel(projet.getImpl(), projet, legendeCalque, src);

    // -- on remplit les infos --//
    src.fillWithSourceCreationInfo(CtuluLibString.EMPTY_STRING, newPanel.getInfosCreation());

    // -- on verifie si il faut charger les donnees zippees ou laisser tout se recalculer --//

    // -- restorer les donn�es --//
    FudaaSaveZipLoader loader = null;
    try {
      loader = new FudaaSaveZipLoader(new File(dirGeneralData, "calques.zip"));
      //to indicate that data
      loader.setOption(TrPostReloadParameter.RECOMPUTE, Boolean.toString(TrPostReloadParameter.isForceRecompute(parameters)));
      FudaaFilleVisuPersistence fudaaFilleVisuPersistence = new FudaaFilleVisuPersistence(newPanel);
      final Runnable restore = fudaaFilleVisuPersistence.restore(newPanel.getImpl(), loader, newPanel.getImpl().getMainProgression());
      if (restore != null) {
        restore.run();
      }
    } catch (final IOException e) {
      FuLog.error(e);
    } finally {
      if (loader != null) {
        loader.safeClose();
      }
    }
    newPanel.getController().buildActions();

    return newPanel;
  }

  @Override
  public GrBoite getZoom() {
    return zoom_;
  }
}
