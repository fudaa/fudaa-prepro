/*
 * @creation 9 nov. 06
 * 
 * @modification $Date: 2006-12-05 10:18:16 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BCalqueConfigureSectionAbstract;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesAbstract;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetVueCalque;

/**
 * @author fred deniger
 * @version $Id: TrPostFilterConfigure.java,v 1.2 2006-12-05 10:18:16 deniger Exp $
 */
public class TrPostFilterConfigure extends BCalqueConfigureSectionAbstract {
  final TrPostFilterLayer filter_;
  final TrPostSource src_;

  public TrPostFilterConfigure(final TrPostSource _src, final ZCalqueAffichageDonneesAbstract _target,
      final TrPostFilterLayer _filter) {
    super(_target, EbliLib.getS("Filtres d'affichage"));
    filter_ = _filter;
    src_ = _src;
  }

  final static String KEY_STRICT = "filterStrict";
  final static String KEY_SRC = "filterSrc";
  static final String KEY_DEPTH_ACTIVE = "filterWaterDepth";
  static final String KEY_DEPTH_VALUE = "filterWaterDepthValue";
  static final String KEY_AVOID_VAR = "filterAvoidVar";

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    return new BSelecteurInterface[] { new TrPostFilterSelecteur() };
  }

  @Override
  public Object getProperty(final String _key) {
    // if (KEY_STRICT == _key) { return Boolean.valueOf(filter_.isStrict_); }
    if (KEY_DEPTH_ACTIVE == _key) { return Boolean.valueOf(filter_.isHauteurActivated_); }
    if (KEY_DEPTH_VALUE == _key) { return filter_.getHauteur(); }
    if (KEY_AVOID_VAR == _key) { return filter_.getAvoidVar(); }
    if (KEY_SRC == _key) { return src_; }
    return null;
  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    boolean res = false;
    if (KEY_DEPTH_ACTIVE == _key) {
      if (_newProp != null) {
        res = filter_.setHauteurActivated(getBooleanValue(_newProp), src_);
      }
    } else if (KEY_DEPTH_VALUE == _key) {
      if (_newProp != null) {
        res = filter_.initFor((TrPostFilterHauteur) _newProp, src_);
      }
    } else if (KEY_AVOID_VAR == _key) {
      filter_.setAvoidVar((H2dVariableType[]) _newProp);
      res = true;
    }
    if (res) {
      super.target_.firePropertyChange(EbliWidgetVueCalque.CLEAR_CACHE, Boolean.TRUE, Boolean.FALSE);
      super.target_.repaint(0);
    }
    return res;

  }

  public static boolean getBooleanValue(final Object _newProp) {
    return ((Boolean) _newProp).booleanValue();
  }

}
