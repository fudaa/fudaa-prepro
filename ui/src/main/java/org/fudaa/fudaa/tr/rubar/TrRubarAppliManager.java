/**
 * @creation 9 juin 2004
 * @modification $Date: 2007-02-15 17:10:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuResource;
import org.fudaa.dodico.calcul.CalculExecBatch;
import org.fudaa.dodico.calcul.CalculLauncher;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.rubar.RubarRubarExec;
import org.fudaa.dodico.rubar.RubarRubarSedimentExec;
import org.fudaa.dodico.rubar.RubarVF2MExec;
import org.fudaa.fudaa.commun.FudaaUI;
import org.fudaa.fudaa.commun.exec.FudaaExec;
import org.fudaa.fudaa.tr.common.TrApplicationManager;
import org.fudaa.fudaa.tr.common.TrExplorer;
import org.fudaa.fudaa.tr.common.TrLauncher;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarAppliManager.java,v 1.19 2007-02-15 17:10:48 deniger Exp $
 */
public class TrRubarAppliManager extends TrApplicationManager {
  @Override
  public String getSoftId() {
    return TrRubarImplHelper.getID();
  }

  private class FudaaExecLaunchBatch extends FudaaExec {
    CalculExecBatch[] batch_;

    /**
     * @param _b le batch a lancer.
     */
    public FudaaExecLaunchBatch(final CalculExecBatch[] _b) {
      super(DodicoLib.getS("Ex�cuter {0}", _b[0].getExecName()));
      batch_ = _b;
    }

    @Override
    public void execInDir(final File _dir, final FudaaUI _ui, final Runnable _nexProcess) {
    }

    @Override
    public void execOnFile(final File _target, final FudaaUI _ui, final Runnable _nexProcess) {
      final CalculLauncher l = TrRubarProject.launchCalcul(batch_, _ui, _target, getLauncher());
      if (l != null) {
        l.execute();
      }
    }

    @Override
    public Icon getIcon() {
      return BuResource.BU.getIcon("executer");
    }
  }

  private static class FudaaExecVFMBatch extends FudaaExec {
    CalculExecBatch batch_;

    /**
     * @param _b le batch a lancer.
     */
    public FudaaExecVFMBatch(final CalculExecBatch _b) {
      super(DodicoLib.getS("Ex�cuter {0}", _b.getExecName()));
      batch_ = _b;
    }

    @Override
    public void execInDir(final File _dir, final FudaaUI _ui, final Runnable _nexProcess) {
      final CalculLauncher l = TrRubarProject.launchCalcul(batch_, _ui, _dir);
      if (l != null) {
        l.execute();
      }
    }

    @Override
    public void execOnFile(final File _target, final FudaaUI _ui, final Runnable _nexProcess) {
      final CalculLauncher l = TrRubarProject.launchCalcul(batch_, _ui, _target);
      if (l != null) {
        l.execute();
      }
    }

    @Override
    public Icon getIcon() {
      return BuResource.BU.getIcon("executer");
    }
  }

  BuMenuItem launchVf2m_;
  BuMenuItem launchRubar_;

  // BuMenuItem createProject_;

  /**
   * @param _r le lanceur
   */
  public TrRubarAppliManager(final TrLauncher _r) {
    super(_r);
  }

  protected TrLauncher getLauncher() {
    return launcher_;
  }

  @Override
  public void buildCmdForMenuFile(final JPopupMenu _m, final TrExplorer _explor) {
    newProject_ = _explor.createFileAction(new FudaaExecCreateProjectH2d(TrResource.getS("Cr�er projet Rubar"),
        launcher_));
    _m.add(newProject_);
    launchVf2m_ = _explor.createFileAction(new FudaaExecVFMBatch(new RubarVF2MExec()));
    launchRubar_ = _explor.createFileAction(new FudaaExecLaunchBatch(new CalculExecBatch[]{new RubarRubarExec(),
        new RubarRubarSedimentExec()}));
    _m.add(launchVf2m_);
    _m.add(launchRubar_);
    super.buildCmdForMenuFile(_m, _explor);
  }
}
