package org.fudaa.fudaa.tr.post.replay;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import java.awt.event.ActionEvent;
import java.util.concurrent.Callable;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.fudaa.ctulu.gui.CtuluComboBoxModelAdapter;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostCourbeTreeModel;
import org.fudaa.fudaa.tr.post.TrPostCourbeVolumeModel;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;

/**
 * Classe qui se charge d'afficher et rejouer les donn�es d'origine des courbes TR. C'set a dire les courbes spatiales et evolutions temporelles.
 *
 * @author Adrien Hadoux
 */
public class TrReplayerVolume extends AbstractTrReplayer {

  public TrReplayerVolume(TrPostSource source,
          final TrPostCommonImplementation implementation) {
    super(source, implementation);
  }

  private boolean replayVolume(H2dVariableType newVariable, final TrPostVisuPanel vue2d, final TrPostCourbeTreeModel tree, final TrPostCourbeVolumeModel modele, int startTime, int endTime, String title) {
    MvElementLayer polygonLayer = vue2d.getGridGroup().getPolygonLayer();
    if (!polygonLayer.isSelectionEmpty()) {
      final String nbElements = Integer.toString(polygonLayer.getSelectedElementIdx().length);
      // -- on rejoue les donn�es avec les nouvelles valeurs --//
      if (this.ecraser_.isSelected()) {
        implementation.message(
                TrResource.getS("La courbe actuelle va �tre recalcul�e pour les donn�es suivantes:\n Variable: {0}\n Nombre d'�l�ments: {1}", newVariable.getName(), nbElements));
      } else {
        String msg = TrResource.getS("Une nouvelle courbe de cubature va �tre recalcul�e pour les donn�es suivantes:\n Variable: {0}\n Nombre d'�l�ments: {1}.\nLa courbe actuelle ne sera pas �cras�e", newVariable.getName(), nbElements);
        implementation.message(msg);
      }
      // -- on rejoue les donn�es avec ou sans ecrasement --//
      tree.replayVolume(implementation, vue2d, modele, source, polygonLayer.getSelectedElementIdx(), vue2d.getCmdMng(), implementation.getMainProgression(), newVariable, startTime, endTime, this.ecraser_.isSelected(), title);

      return true;


    } else {
      implementation.error(TrLib.getString("Il faut s�lectionner au moins 1 �l�ment afin de calculer un volume.\nPour s�lectionner plusieurs �l�ments, maintenir la touche shift"));
      return false;
    }


  }

  @SuppressWarnings("serial")
  public void displayVolumeReplayData(final TrPostCourbeTreeModel treeModel, final TrPostCourbeVolumeModel modele) {

    final TrReplayVolumeVisuCustomizer customizer = new TrReplayVolumeVisuCustomizer(modele, TrResource.TR.getString("El�ments utilis�s pour le calcul de cubature"));
    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    final TrPostVisuPanel vue2d = TrReplayVue2dBuilder.constructVue2d(implementation, this.source, customizer);
    vue2d.removeCalqueLegend();
    //
    // -- creation du panel de choix des variables a selectionner --//
    rejouerSrc_ = new BuButton();
    JPanel pnTime = new JPanel(new BuGridLayout(2));
    final JTextField tfTitle = new JTextField(10);
    tfTitle.setText(modele.getTitle());
    pnTime.add(new JLabel(TrLib.getString("Titre")));
    pnTime.add(tfTitle);
    pnTime.add(new JLabel(TrLib.getString("Variable")));
    final JComboBox cb = new JComboBox();
    cb.setModel(new DefaultComboBoxModel(this.source.getAvailableVar()));
    pnTime.add(cb);
    pnTime.add(new JLabel(TrLib.getString("Pas de temps d�part de la courbe")));
    final JComboBox startTime = new JComboBox();
    startTime.setModel(new CtuluComboBoxModelAdapter(source.getNewTimeListModel()));
    startTime.setSelectedIndex(0);
    final JComboBox endTime = new JComboBox();
    endTime.setModel(new CtuluComboBoxModelAdapter(this.source.getNewTimeListModel()));
    startTime.setSelectedIndex(Math.max(0, modele.getMinTimeIdx()));
    endTime.setSelectedIndex(Math.min(this.source.getTime().getNbTimeStep() - 1, modele.getMaxTimeIdx()));
    pnTime.add(startTime);
    pnTime.add(new JLabel(TrLib.getString("Pas de temps fin")));
    pnTime.add(endTime);
    Callable<Boolean> okAction = new Callable<Boolean>() {
      @Override
      public Boolean call() throws Exception {
        return replayVolume((H2dVariableType) cb.getSelectedItem(), vue2d, treeModel, modele, startTime.getSelectedIndex(), endTime.getSelectedIndex(), tfTitle.getText());
      }
    };
    final TrReplayDialog dialog = constructEditionDialog(okAction, vue2d, modele, pnTime, TrLib.getString("Volume"));
    rejouerSrc_.setAction(new AbstractAction(TrLib.getString("Fichier r�sultat")) {
      @Override
      public void actionPerformed(final ActionEvent e) {
        final TrPostSource selectedSource = selectSource(implementation.getCurrentProject(),
                (String) getValue(Action.NAME), implementation.getFrame(), source);
        if (selectedSource != null) {
//          replayWithAnotherSource = true;
          // -- on ferme l'interface courante --//
          if (dialog != null) {
            dialog.getDialog().cancel();
          }
          // -- on rappelle l'interface --//
          new TrReplayerVolume(selectedSource, implementation).displayVolumeReplayData(treeModel, modele);
        }

      }
    });

    super.comboVar_.setVisible(false);
    super.comboTime.setVisible(false);
    // -- on positionne la bonne variable et le bon pas de temps --//
    cb.setSelectedItem(modele.getVar());
    // -- ajout du calque ligne bris�e pour r�aliser une polyligne --//
    dialog.afficheModale(implementation.getParentComponent(), TrResource.getS("Rejouer ")
            + " " + modele.getTitle(), createRestaurerRunnable(vue2d));

  }

  /**
   * Affiche une dialog avec une vue 2d qui affiche l'ensemble des points de la polyligne utilis�e pour la construction du graphe.
   *
   * @param modele
   * @param implementation
   */
  public void displayVolumeProfilOrigin(final TrPostCourbeVolumeModel modele,
          final TrPostCommonImplementation implementation) {

    if (modele.getSource() == null) {
      implementation.error(TrResource.getS("Erreur"), TrResource.getS("Impossible de r�cup�rer le fichier r�sultat"),
              true);
      return;
    }
    // -- on cree une vue 2d a partir du fichier source fourni et on le centre sur le point interpol� ou non --//
    final TrReplayVolumeVisuCustomizer customizer = new TrReplayVolumeVisuCustomizer(modele, TrResource.TR.getString("El�ments utilis�s pour le calcul de cubature"));
    final TrPostVisuPanel vue2d = TrReplayVue2dBuilder.constructVue2d(implementation, modele.getSource(), customizer);
    vue2d.removeCalqueLegend();
    final CtuluDialogPanel panel = constructDialog(getTrueCallable(), vue2d, modele, null, false, customizer.getTitle(), null);
    // -- on positionne la bonne variable et le bon pas de temps --//
    comboVar_.getCb().setSelectedItem(modele.getVar());
    panel.afficheModale(implementation.getParentComponent(), TrResource.getS("Origine ") + " " + modele.getTitle(),
            createRestaurerRunnable(vue2d));
  }
}
