package org.fudaa.fudaa.tr.post.persist;

import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.XmlFriendlyReplacer;
import com.thoughtworks.xstream.io.xml.XppDriver;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import org.fudaa.ctulu.CtuluLibString;

/**
 * @author deniger
 */
public class XppCustomDriver extends XppDriver {

  /**
   * 
   */
  public XppCustomDriver() {}

  /**
   * @param replacer
   */
  public XppCustomDriver(XmlFriendlyReplacer replacer) {
    super(replacer);
  }

  private static char[] INDENT = new char[] { ' ', ' ' };

  @Override
  public HierarchicalStreamWriter createWriter(Writer out) {
    return new PrettyPrintWriter(out, INDENT, CtuluLibString.LINE_SEP, xmlFriendlyReplacer());
  }

  /**
   *
   */
  @Override
  public HierarchicalStreamWriter createWriter(OutputStream out) {
    return createWriter(new OutputStreamWriter(out));
  }

}
