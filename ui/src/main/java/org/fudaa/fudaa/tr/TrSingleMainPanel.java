/**
 *  @creation     26 mai 2004
 *  @modification $Date: 2006-09-19 15:07:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuColumn;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuMainPanel;
import com.memoire.bu.BuSpecificBar;
import javax.swing.JComponent;

/**
 * @author Fred Deniger
 * @version $Id: TrSingleMainPanel.java,v 1.7 2006-09-19 15:07:28 deniger Exp $
 */
public class TrSingleMainPanel extends BuMainPanel {

  /**
   * @param _middle
   */
  public TrSingleMainPanel(final JComponent _middle) {
    super(null);
    // removeAll();
    specificpanel_.remove(splitpane_);
    splitpane_ = null;
    // specificpanel_.setLayout(new BuBorderLayout());
    specificpanel_.add(_middle, BuBorderLayout.CENTER);
    // setSpecificBar(new BuSpecificBar());
    // setStatusBar(new BuStatusBar());
    revalidate();
  }

  @Override
  public BuDesktop getDesktop() {
    return null;
  }

  @Override
  public BuSpecificBar getSpecificBar() {
    return null;
  }

  @Override
  public void setSpecificBar(final BuSpecificBar _specificbar) {}

  @Override
  public void setLeftColumn(final BuColumn _leftcolumn) {}

  @Override
  public void setRightColumn(final BuColumn _rightcolumn) {}

  @Override
  public void swapColumns() {}

  @Override
  public void updateSplits() {}
}