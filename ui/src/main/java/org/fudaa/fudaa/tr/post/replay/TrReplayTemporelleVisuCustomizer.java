/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.replay;

import org.locationtech.jts.geom.Coordinate;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCourbeModel;
import org.fudaa.fudaa.tr.post.TrPostInterpolatePoint;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostWizardCourbeTemporelle;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostWizardProfilSpatial;

/**
 *
 * @author Frederic Deniger
 */
public final class TrReplayTemporelleVisuCustomizer implements TrReplayVisuCustomizer {
  TrPostCourbeModel modele;

  /**
   * @param listePoints
   */
  /**
   * @param listePoints
   */
  public TrReplayTemporelleVisuCustomizer(final TrPostCourbeModel modele) {
    super();
    this.modele = modele;
  }

  @Override
  public void customizeVisu(final TrPostVisuPanel vue2d) {
    final boolean isInterpolated = modele instanceof TrPostCourbeModel.Interpolated;
    // -- on affiche sur la vue 2d les points initialement choisis --//
    final GISZoneCollectionPoint collectionPoint = new GISZoneCollectionPoint();
    final Object point = modele.getSelectionPoint();
    Coordinate coordonneesPoint = null;
    if (point instanceof Integer) {
      // point reel
      final int indiceNoeud = ((Integer) point).intValue();
      // recherche du point cot� grille
      coordonneesPoint = modele.source_.getGrid().getCoor(indiceNoeud);
    } else {
      final TrPostInterpolatePoint pt = (TrPostInterpolatePoint) point;
      coordonneesPoint = new Coordinate(pt.getX(), pt.getY(), 0);
    }
    // -- ajout des coordonnees --//
    collectionPoint.add(coordonneesPoint.x, coordonneesPoint.y, 0);
    final FSigLayerGroup group = TrPostWizardProfilSpatial.getGroup(TrPostWizardCourbeTemporelle.idTemporel, vue2d);
    final ZCalquePointEditable calqueZ = group.addPointLayerAct(TrResource.TR.getString("Origine de la courbe"), collectionPoint);
    calqueZ.setIconModel(0, TrReplayVue2dBuilder.createTraceIconModel());
    group.add(calqueZ);
    calqueZ.setVisible(true);
    calqueZ.setDestructible(true);
    vue2d.setCalqueActif(calqueZ);
  }
  
}
