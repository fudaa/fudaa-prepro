/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.replay;

import java.awt.Component;
import java.awt.Frame;
import java.util.concurrent.Callable;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;

/**
 *
 * @author Frederic Deniger
 */
@SuppressWarnings(value = "serial")
public class TrReplayDialog extends CtuluDialogPanel {

  Callable<Boolean> okAction;

  TrReplayDialog(final boolean b, final Component _parent, Callable<Boolean> okAction) {
    super(b);
    this.okAction = okAction;
  }

  @Override
  public boolean ok() {
    try {
      return okAction.call();
    } catch (Exception ex) {
      FuLog.error(ex);
    }
    return true;
  }

  @Override
  public int afficheModale(final Component _parent, final String _title, final int _option, final Runnable _run) {
    dialog_ = createDialog(_parent);
    dialog_.setOption(_option);
    if (_title != null) {
      dialog_.setTitle(_title);
    }
    dialog_.setInitParent(_parent);
    return dialog_.afficheDialogModal(_run);
  }

  @Override
  public int afficheModale(final Frame _parent, final String _title, final int _option, final Runnable _run) {
    dialog_ = new CtuluDialog(_parent, this);
    dialog_.setOption(_option);
    if (_title != null) {
      dialog_.setTitle(_title);
    }
    dialog_.setInitParent(_parent);
    return dialog_.afficheDialogModal(_run);
  }
}
