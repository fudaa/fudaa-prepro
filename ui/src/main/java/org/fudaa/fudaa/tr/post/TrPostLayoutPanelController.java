package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuSeparator;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluSelectorPopupButton;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.commun.EbliActionAbstract;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliNodeDefault;
import org.fudaa.ebli.visuallibrary.SceneHelper;
import org.fudaa.ebli.visuallibrary.WidgetResource;
import org.fudaa.ebli.visuallibrary.actions.CommandUndoRedoCreation;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionAlign;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionColorBackground;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionConfigure;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionDelete;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionGroup;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionImageChooser;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionMoveToBack;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionMoveToFirst;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionPrintDisplayConfigure;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionRetaillageHorizontal;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionRetaillageVertical;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActiontextEditor;
import org.fudaa.ebli.visuallibrary.animation.EbliWidgetAnimAdapter;
import org.fudaa.ebli.visuallibrary.calque.CalqueLegendeWidgetAdapter;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetControllerCalque;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetCreatorVueCalque;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreatorArrow;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreatorShape;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreatorTextLabel;
import org.fudaa.ebli.visuallibrary.creator.ShapeCreatorCircle;
import org.fudaa.ebli.visuallibrary.creator.ShapeCreatorDblFleche;
import org.fudaa.ebli.visuallibrary.creator.ShapeCreatorEllipse;
import org.fudaa.ebli.visuallibrary.creator.ShapeCreatorFleche;
import org.fudaa.ebli.visuallibrary.creator.ShapeCreatorRectangle;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.sig.layer.FSigImageImportAction;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrProjectPersistence;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.actions.TrPostActionChangeSceneForWidget;
import org.fudaa.fudaa.tr.post.actions.TrPostActionChooseAndCreateCalque;
import org.fudaa.fudaa.tr.post.actions.TrPostActionDuplicate;
import org.fudaa.fudaa.tr.post.actions.TrPostActionDuplicateLayout;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostWizardCreateScope;
import org.netbeans.api.visual.widget.Widget;

/**
 * Controller des actions pour les ebliWidget. Gere les multi layout.
 * 
 * @author Adrien Hadoux
 */
public class TrPostLayoutPanelController {

  List<EbliActionAbstract> actions_;

  /**
   * Calque principal associ� a la vue layout.
   */
  private ZEbliCalquesPanel calquePrincipal;

  JPanel conteneur_;

  /**
   * combobox qui gere le multiscene
   */
  JScrollPane conteneurSceneView_;
  ArrayList<BuMenuItem> listeMenusScenes_ = new ArrayList<BuMenuItem>();

  JTabbedPane pn_;

  TrPostProjet projet_;

  /**
   * scene seelctionnee par l utilisateur
   */
  TrPostScene sceneCourante_;

  TrPostLayoutPanelController(final TrPostScene _scene, final TrPostProjet _projet) {
    sceneCourante_ = _scene;
    _scene.setCmdMng(new CtuluCommandManager());
    projet_ = _projet;
  }

  public void addSource(TrPostSource srcChoisie) {
    // -- TRES IMPORTANT: on cree le calque en precisant l indice de la SOURCE
    // SELECTIONNEE --//
    final CalqueLegendeWidgetAdapter legendeCalque = new CalqueLegendeWidgetAdapter(getSceneCourante());

    final TrPostVisuPanel pnVisu = new TrPostVisuPanel(projet_.getImpl(), projet_, legendeCalque, srcChoisie);

    Point optimalePosition = SceneHelper.getOptimalePosition(sceneCourante_);
    final EbliNode node = addCalque(TrResource.getS("Calque") + (getSceneCourante().getAllVue2d().size() + 1), optimalePosition,
        pnVisu.getPreferredSize(), pnVisu);
    // -- ajout de l INFO de la source utilis�e --//
    node.setDescription("Source: " + projet_.formatInfoSource(srcChoisie));

    // -- ajout des infos de cr�ation --//
    pnVisu.addInfosCreation(ZEbliCalquesPanel.TITRE_FIC, srcChoisie.getTitle());

    // TrPostSourceAbstractFromIdx.fillWithSourceInfo( srcChoisie);
    srcChoisie.fillWithSourceCreationInfo(CtuluLibString.EMPTY_STRING, pnVisu.getInfosCreation());
  }

  /**
   * Methode specialisee dans l ajout d un ndoe de type calque a la scene.
   * 
   * @param title de la widget
   * @param preferredLocation de la widget
   * @param preferedDimension de la widget
   * @param calque contenu de la widget
   * @return le node cree
   */
  public final EbliNode addCalque(final String title, final Point preferredLocation, final Dimension preferedDimension, final TrPostVisuPanel calque) {
    // TODO a eviter
    // -- enregistrement du calque principal --//
    if (calquePrincipal == null)
      // -- alors le calque recupere sera considere comem le calque principal
      // --//
      calquePrincipal = calque;

    // on ajoute le calque sous forme d'un node

    // construction du node correspondant
    final EbliNode nodeCalque = new EbliNodeDefault();
    nodeCalque.setTitle(title);

    final EbliWidgetCreatorVueCalque creator = new EbliWidgetCreatorVueCalque(calque);
    creator.setPreferredSize(preferedDimension == null ? new Dimension(300, 300) : preferedDimension);// new Dimension(400, 200)
    creator.setPreferredLocation(preferredLocation == null ? SceneHelper.getOptimalePosition(sceneCourante_) : preferredLocation);
    nodeCalque.setCreator(creator);
    // ajout du node au layout
    addNode(nodeCalque);

    nodeCalque.setDescription(projet_.formatInfoSource(calque.getSource()));

    // -- construction des actions sans passer par la frame fille --//
    calque.getController().buildActions();

    // -- ajout de la legende dans le calque associe --//
    EbliWidgetControllerCalque controllerCalque = (EbliWidgetControllerCalque) nodeCalque.getController();
    controllerCalque.ajoutLegende();
    controllerCalque.registerListenerForNewLegend();
    controllerCalque.addMenuSpecifique(createImportMenu(calque));
    return nodeCalque;
  }

  private JMenu createImportMenu(final TrPostVisuPanel calque) {
    BuMenu importMenu = new BuMenu();
    importMenu.setText(BuResource.BU.getString("Importer"));
    importMenu.setIcon(null);
    importMenu.addMenuItem(FudaaLib.getS("Importer un projet"), "IMPORT_PROJECT", BuResource.BU.getMenuIcon("importer"), new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent arg0) {
        TrProjectPersistence.importForFrame(calque, projet_.impl_);
      }
    });

    importMenu.addMenuItem(FSigImageImportAction.getCommonTitle(), "IMPORT_IMAGE", FSigImageImportAction.getCommonImage(), new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent arg0) {
        calque.importImage();
      }
    });
    return importMenu;

  }

  public EbliNode addArrow() {
    // -- ajout du rectangle --//
    final EbliNode nodeLigne = new EbliNodeDefault();
    nodeLigne.setTitle(TrLib.getString("Fl�che"));
    nodeLigne.setCreator(new EbliWidgetCreatorArrow());
    nodeLigne.getCreator().setPreferredSize(new Dimension(20, 20));
    nodeLigne.getCreator().setPreferredLocation(new Point(350, 125));
    // ajout du node au layout
    addNode(nodeLigne);

    return nodeLigne;
  }

  public EbliNode addCercle() {
    // -- ajout du rectangle --//
    final EbliNode nodeCercle = new EbliNodeDefault();
    nodeCercle.setTitle(TrLib.getString("Cercle"));
    nodeCercle.setCreator(new EbliWidgetCreatorShape(new ShapeCreatorCircle()));
    nodeCercle.getCreator().setPreferredSize(new Dimension(200, 100));
    nodeCercle.getCreator().setPreferredLocation(new Point(350, 125));
    // ajout du node au layout
    addNode(nodeCercle);

    return nodeCercle;
  }

  public EbliNode addEllipse() {
    // -- ajout du rectangle --//
    final EbliNode nodeEllipse = new EbliNodeDefault();
    nodeEllipse.setTitle(TrLib.getString("Ellipse"));
    nodeEllipse.setCreator(new EbliWidgetCreatorShape(new ShapeCreatorEllipse()));
    nodeEllipse.getCreator().setPreferredSize(new Dimension(200, 100));
    nodeEllipse.getCreator().setPreferredLocation(new Point(350, 125));
    // ajout du node au layout
    addNode(nodeEllipse);

    return nodeEllipse;
  }

  /**
   * Methode d ajout de composant de base graphique.
   * 
   * @return
   */
  public EbliNode addFleche() {
    // -- ajout du rectangle --//
    final EbliNode nodeFleche = new EbliNodeDefault();
    nodeFleche.setTitle(TrLib.getString("Fl�che"));
    nodeFleche.setCreator(new EbliWidgetCreatorShape(new ShapeCreatorFleche()));
    nodeFleche.getCreator().setPreferredSize(new Dimension(100, 50));
    nodeFleche.getCreator().setPreferredLocation(new Point(350, 125));

    // ajout du node au layout
    addNode(nodeFleche);

    return nodeFleche;
  }

  public EbliNode addDblFleche() {
    // -- ajout du rectangle --//
    final EbliNode nodeFleche = new EbliNodeDefault();
    nodeFleche.setTitle(TrLib.getString("Double fl�che"));
    nodeFleche.setCreator(new EbliWidgetCreatorShape(new ShapeCreatorDblFleche()));
    nodeFleche.getCreator().setPreferredSize(new Dimension(100, 50));
    nodeFleche.getCreator().setPreferredLocation(new Point(350, 125));

    // ajout du node au layout
    addNode(nodeFleche);

    return nodeFleche;
  }

  /**
   * Methode generique d ajout d un node widget a la scene.
   * 
   * @param node
   */
  public Widget addNode(final EbliNode node) {
    final Widget addNode = getSceneCourante().addNode(node);

    // -- rafraichissement de la scene pour eviter les plantages --//
    getSceneCourante().refresh();

    // -- commande undo/redo pour la creation de widget --//
    getSceneCourante().getCmdMng().addCmd(new CommandUndoRedoCreation(node, getSceneCourante()));
    return addNode;
  }

  public EbliNode addRectangle() {
    // -- ajout du rectangle --//
    final EbliNode nodeCercle = new EbliNodeDefault();
    nodeCercle.setTitle("rectangle");
    nodeCercle.setCreator(new EbliWidgetCreatorShape(new ShapeCreatorRectangle()));
    nodeCercle.getCreator().setPreferredSize(new Dimension(200, 100));
    nodeCercle.getCreator().setPreferredLocation(new Point(350, 125));
    // ajout du node au layout
    addNode(nodeCercle);

    return nodeCercle;
  }

  /**
   * Methode d ajout de composant de base graphique.
   * 
   * @return
   */
  public EbliNode addRectangleTexte() {
    // -- ajout du rectangle --//
    final EbliNode nodeRect = new EbliNodeDefault();
    nodeRect.setTitle("Rectangle texte");
    nodeRect.setCreator(new EbliWidgetCreatorTextLabel("Tapez votre texte ici"));
    nodeRect.getCreator().setPreferredSize(new Dimension(200, 100));
    nodeRect.getCreator().setPreferredLocation(new Point(350, 125));
    // ajout du node au layout
    addNode(nodeRect);

    return nodeRect;
  }

  @SuppressWarnings("serial")
  protected void addShapeActions(final List<EbliActionAbstract> _l) {
    // -- palette rectangle texte--//
    // widget texte
    // TODO a revoir
    _l.add(new EbliWidgetActiontextEditor(getSceneCourante()));

    // -- ligne --//
    _l.add(new EbliActionSimple(TrLib.getString("Fl�che"), WidgetResource.WIDGET.getToolIcon("arrow_16"), "WIDGETLINE") {
      @Override
      public void actionPerformed(final ActionEvent _evt) {
        addArrow();
      }
    });

    // -- palette fleche --//
    _l.add(new EbliActionSimple(TrLib.getString("Fl�che pleine"), WidgetResource.WIDGET.getToolIcon("arrow-left_16"), "WIDGETFLECHE") {
      @Override
      public void actionPerformed(final ActionEvent _evt) {
        addFleche();
      }
    });

    // -- palette double fleche --//
    // crystal_bu_scrollpane_corner.png
    _l.add(new EbliActionSimple(TrLib.getString("Double fl�che"), WidgetResource.WIDGET.getToolIcon("double-arrow_16"), "WIDGETDBLFLECHE") {
      @Override
      public void actionPerformed(final ActionEvent _evt) {
        addDblFleche();
      }
    });
    // -- palette Ellipse --//
    _l.add(new EbliActionSimple(TrLib.getString("Ellipse"), EbliResource.EBLI.getToolIcon("ellip"), "WIDGETELLIPSE") {
      @Override
      public void actionPerformed(final ActionEvent _evt) {

        addEllipse();
      }
    });
    _l.add(new EbliActionSimple(TrLib.getString("Cercle"), EbliResource.EBLI.getToolIcon("cerc"), "WIDGETCERCLE") {
      @Override
      public void actionPerformed(final ActionEvent _evt) {

        addCercle();
      }
    });
    _l.add(new EbliActionSimple(TrLib.getString("Rectangle"), EbliResource.EBLI.getToolIcon("rect"), "WIDGETRECT") {
      @Override
      public void actionPerformed(final ActionEvent _evt) {

        addRectangle();
      }
    });

    // widget image
    _l.add(new EbliWidgetActionImageChooser(getSceneCourante()));

  }

  protected JMenu createMenu() {
    final BuMenu m = new BuMenu();
    final List<EbliActionAbstract> acts = getActions();
    if (acts == null) {
      return null;
    }
    for (final EbliActionAbstract a : acts) {
      if (a == null) {
        m.add(new BuSeparator());
      } else {
        m.add(a.buildMenuItem(EbliComponentFactory.INSTANCE));
      }
    }
    return m;
  }

  protected JComponent[] createSpecificComponent(final JDesktopPane j, final JComponent parent) {
    final List<EbliActionAbstract> acts = getActions();
    if (acts == null) {
      return null;
    }
    EbliLib.updateMapKeyStroke(parent, acts.toArray(new EbliActionInterface[acts.size()]));
    // JComponent[] r = new JComponent[n];
    BuDesktop buJ = null;
    // JComponent c;
    if (j instanceof BuDesktop) {
      buJ = (BuDesktop) j;
    }
    final List<JComponent> res = new ArrayList<JComponent>(acts.size());
    for (final EbliActionAbstract a : acts) {
      if (a == null) {
        res.add(null);
      } else {
        if ((buJ != null) && (a instanceof EbliActionPaletteAbstract)) {
          ((EbliActionPaletteAbstract) a).setDesktop(buJ);
        }
        res.add(a.buildToolButton(EbliComponentFactory.INSTANCE));
      }
    }

    return res.toArray(new JComponent[res.size()]);
  }

  public List<EbliActionAbstract> getActions() {
    if (actions_ == null) {
      final List<EbliActionAbstract> init = new ArrayList<EbliActionAbstract>(20);
      final TrPostScene sceneCourante = getSceneCourante();
      init.add(new EbliWidgetActionAlign.Left(sceneCourante));
      init.add(new EbliWidgetActionAlign.Right(sceneCourante));
      init.add(new EbliWidgetActionAlign.Middle(sceneCourante));
      init.add(new EbliWidgetActionAlign.Center(sceneCourante));
      init.add(new EbliWidgetActionAlign.Top(sceneCourante));
      init.add(new EbliWidgetActionAlign.Bottom(sceneCourante));
      init.add(null);

      init.add(new EbliWidgetActionMoveToFirst(sceneCourante));
      init.add(new EbliWidgetActionMoveToBack(sceneCourante));
      init.add(null);

      // -- actions de retaillage min et max --//
      init.add(new EbliWidgetActionRetaillageHorizontal(sceneCourante, EbliWidgetActionRetaillageHorizontal.RETAIILLAGE_MAX));
      init.add(new EbliWidgetActionRetaillageVertical(sceneCourante, EbliWidgetActionRetaillageVertical.RETAIILLAGE_MAX));
      init.add(null);

      // -- blocage des widgets --//
      init.add(new EbliWidgetActionGroup(sceneCourante));
      init.add(null);
      init.add(new EbliWidgetActionDelete(sceneCourante));
      init.add(null);
      // -- action de duplication --//
      init.add(new TrPostActionDuplicate(sceneCourante, projet_));
      init.add(new TrPostActionDuplicateLayout(sceneCourante, projet_));
      init.add(new TrPostActionChangeSceneForWidget(sceneCourante, projet_));
      init.add(null);

      // -- Action sur le format --//
      init.add(new EbliWidgetActionColorBackground.ForScene(sceneCourante));
      // -- action de configuration des composants graphiques--//
      init.add(new EbliWidgetActionConfigure(sceneCourante));
      init.add(new EbliWidgetActionPrintDisplayConfigure(sceneCourante));
      init.add(null);

      // -- objets graphiques de base --//
      addShapeActions(init);
      // -- ajout d'une action specifique de recreation du calque en cas de
      // suppression, on choisit la source a prendre en compte --//
      init.add(null);
      init.add(new EbliActionSimple(EbliResource.EBLI.getString("Gestion Multi-Sources"), EbliResource.EBLI.getToolIcon("tableau"), "MULTI SOURCES") {
        @Override
        public void actionPerformed(final ActionEvent _evt) {
          // -- affichage de la fenetre de gestion multi projet --//
          if (!projet_.filleProjetctManager_.isVisible()) {
            projet_.impl_.addInternalFrame(projet_.filleProjetctManager_);
          } else projet_.filleProjetctManager_.moveToFront();
        }
      });

      init.add(new TrPostActionChooseAndCreateCalque(projet_, this));
      init.add(null);
      init.add(new EbliWidgetAnimAdapter(sceneCourante).createAction());

      init.add(null);
      init.add(new TrPostWizardCreateScope.ImportAction(projet_));
      // init.add((new TrPostRapportLayout(getSceneCourante())).getAction());
      actions_ = Collections.unmodifiableList(init);

    }
    return actions_;
  }

  /**
   * @return the calquePrincipal
   */
  public ZEbliCalquesPanel getCalquePrincipal() {
    return calquePrincipal;
  }

  /**
   * affichage de la scene.
   * 
   * @return
   */
  public JComponent getPanel() {
    if (conteneur_ == null) {
      // -- creation du conteneur de base qui contient tout --//
      // pn_ = new BuPanel(new BuBorderLayout());
      pn_ = new JTabbedPane();
      pn_.setTabPlacement(SwingConstants.BOTTOM);
      // ajout de la scene au centre
      conteneurSceneView_ = new JScrollPane(getSceneCourante().createView());
      new CtuluSelectorPopupButton() {
        @Override
        public JComponent createComponent() {
          return getSceneCourante().createSatelliteView();
        }

      }.installButton(conteneurSceneView_);
      // pn_.add(conteneurSceneView_, BuBorderLayout.CENTER);
      pn_.addTab("Layout 1", conteneurSceneView_);

      // // -- construction de la barre des commandes de base --//

      conteneur_ = new JPanel(new BorderLayout());

      // -- decomenter cela pour reprendre le systeme d onglet --//
      // conteneur_.add(pn_, BorderLayout.CENTER);
      conteneur_.add(conteneurSceneView_, BorderLayout.CENTER);

    }
    return conteneur_;

  }

  public TrPostScene getSceneCourante() {
    return sceneCourante_;
  }

}
