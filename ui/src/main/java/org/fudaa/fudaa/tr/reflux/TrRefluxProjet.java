/*
 * @creation 10 juin 2003
 * @modification $Date: 2007-06-05 09:01:13 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.calcul.CalculLauncher;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.reflux.H2dRefluxElementPropertyMngAbstract;
import org.fudaa.dodico.h2d.reflux.H2dRefluxParameters;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.reflux.exec.RefluxExec;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaUI;
import org.fudaa.fudaa.commun.calcul.FudaaCalculOp;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.*;
import org.fudaa.fudaa.tr.data.TrFilleVisu;
import org.fudaa.fudaa.tr.post.TrPostInspector;
import org.fudaa.fudaa.tr.post.TrPostInspectorReaderReflux;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @author deniger
 * @version $Id: TrRefluxProjet.java,v 1.59 2007-06-05 09:01:13 deniger Exp $
 */
public class TrRefluxProjet extends TrProjetCommon {
  protected static class RefluxExecLauncher extends FudaaCalculOp {
    private final File f_;
    private final TrLauncher launcher_;

    protected RefluxExecLauncher(final TrLauncher _launcher, final File _f, final FudaaUI _ui) {
      super(new RefluxExec());
      launcher_ = _launcher;
      f_ = _f;
      ui_ = _ui;
    }

    @Override
    protected final String getTaskName() {
      return getS("lancement {0}", "Reflux");
    }

    @Override
    public final void execute() {
      final LaunchOptions op = showLaunchOptions(ui_);
      if (op == null) {
        return;
      }
      final boolean follow = op.isInspected();
      if (follow) {
        launchFollow(f_, launcher_);
      }
      File out = null;
      if (op.isOutFollowed()) {
        out = new File(CtuluLibFile.getSansExtension(f_.getAbsolutePath()) + ".out");
        try {
          Files.deleteIfExists(out.toPath());
        } catch (IOException e) {
          FuLog.error(e);
        }
      }
      super.execute();
      if (out != null && op.isOutFollowed()) {
        try {
          Thread.sleep(2000);
        } catch (final InterruptedException e) {
          Thread.currentThread().interrupt();
          // on ne fait rien
        }
        if (!out.exists()) {
          try {
            Thread.sleep(2000);
          } catch (final InterruptedException e) {
            // on ne fait rien
            Thread.currentThread().interrupt();
          }
        }
        launcher_.openLogFrame(out);
      }
    }

    @Override
    public File proceedParamFile(final ProgressionInterface _inter) {
      return f_;
    }
  }

  public static class LaunchOptions extends CtuluDialogPanel {
    BuCheckBox cbFollow_;
    BuCheckBox cbInspect_;

    public LaunchOptions() {
      super(false);
      setLayout(new BuVerticalLayout(5));
      cbInspect_ = new BuCheckBox("Voulez-vous afficher les r�sultats en temps r�el ?");
      cbFollow_ = new BuCheckBox("Voulez-vous afficher le contenu du fichier out?");
      cbFollow_.setSelected(true);
      add(cbFollow_);
      add(cbInspect_);
    }

    public boolean isInspected() {
      return cbInspect_.isSelected();
    }

    public boolean isOutFollowed() {
      return cbFollow_.isSelected();
    }
  }

  @Override
  public TrCourbeUseFinder getUsedCourbeFinder(final EGGrapheModel _model) {
    return new TrRefluxCourbeUseFinder(getH2dRefluxParametres(), (TrRefluxVisuPanel) visu_.getVisuPanel(), _model);
  }

  static void launchFollow(final File _f, final TrLauncher _impl) {
    final File file = new File(CtuluLibFile.getSansExtension(_f.getAbsolutePath()) + ".sov");
    try {
      Files.deleteIfExists(file.toPath());
    } catch (IOException e) {
      FuLog.error(e);
    }
    new TrPostInspector(new TrPostInspectorReaderReflux(file, _f), "reflux" + CtuluLibString.ESPACE + _f.getName(),
        _impl).start();
  }

  /**
   * Permet de lancer reflux sur un fichier donne.
   *
   * @param _f le fichier inp
   * @param _impl l'implementation parent
   * @param _ui l'interface utilisateur
   * @return le lanceur du calcul
   */
  public static CalculLauncher launchCalcul(final FudaaUI _ui, final File _f, final TrLauncher _impl) {
    final RefluxExec exec = new RefluxExec();
    if (!exec.isExecFileExists()) {
      _ui.error("reflux", getS(
          "L'ex�cutable '{0}' est introuvable. Pr�ciser le chemin de '{0}' dans le panneau des pr�f�rences (Alt+F2)",
          "reflux")
          + CtuluLibString.ESPACE + getS("Section") + ": reflux", false);
      return null;
    }
    final FudaaCalculOp op = new RefluxExecLauncher(_impl, _f, _ui);
    op.setUi(_ui);
    return op;
  }

  public static LaunchOptions showLaunchOptions(final CtuluUI _ui) {
    final LaunchOptions op = new LaunchOptions();
    if (op.afficheModaleOk(_ui.getParentComponent(), getS("Lancement de Reflux"))) {
      return op;
    }
    return null;
  }

  TrRefluxParametres params_;
  public BuMenu projectMenu_;

  /**
   * Il est conseille d'utiliser l'usine pour construire un projet.
   *
   * @param _params les parametres de ce projet.
   */
  public TrRefluxProjet(final TrRefluxParametres _params) {
    params_ = _params;
  }

  @Override
  protected BuInternalFrame buildGeneralFille() {
    return new TrRefluxFilleProjet(this);
  }

  @Override
  protected TrFilleVisu buildVisuFille() {
    return new TrFilleVisu(new TrRefluxVisuPanel(getImpl(), this));
  }

  protected void editTimeSteps() {
    final H2dRefluxParameters p = params_.getH2dRefluxParametres();
    final TrRefluxTimeStepsEditor editor = new TrRefluxTimeStepsEditor(p.getGroupePasTempsTab(), p
        .getBeginTimeForTrans());
    if (CtuluDialogPanel.isOkResponse(editor.afficheModale(getFrame(), getS("Int�gration en temps")))) {
      p.setTimeGroups(editor.getTimeSteps());
      p.setBeginTimeForTrans(editor.getBeginning());
    }
  }

  @Override
  protected String[] getLinksToCurvesRelatedDoc() {
    return new String[]{getImpl().buildLink(H2dResource.getS("Les conditions limites"), "reflux-editor-bc"),
        getImpl().buildLink(H2dResource.getS("Les propri�t�s �l�mentaires"), "reflux-editor-prop")};
  }

  @Override
  public CalculLauncher actionCalcul() {
    final FudaaCalculOp op = new RefluxExecLauncher(((TrCommonImplementation) getImpl()).getLauncher(), params_
        .getFile(), getImpl()) {
      @Override
      public File proceedParamFile(final ProgressionInterface _inter) {
        params_.save(_inter);
        return params_.getFile();
      }
    };
    op.setUi(getImpl());
    return op;
  }

  /**
   * Gere l'action TIME_STEP.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if ("TIME_STEP".equals(_e.getActionCommand())) {
      editTimeSteps();
    }
  }

  @Override
  public void applicationPreferencesChanged() {
  }

  /**
   * Enregistre les donnees sous un nouveau fichier.
   *
   * @param _d le repertoire de destination
   * @return le fichier inp ecrit
   */
  public File enregistrerParametresUniqueSousRep(final File _d) {
    return params_.enregistrerUniqueSousRepertoire(_d);
  }

  @Override
  public String getCodeExecDir() {
    return getCodeName();
  }

  @Override
  public DicoParams getDicoParams() {
    return params_.getDicoParam();
  }

  @Override
  public File getDirBase() {
    return params_.getFile().getParentFile();
  }

  /**
   * @return l'impl parente
   */
  @Override
  public final TrImplementationEditorAbstract getEditorImpl() {
    return params_.getImpl();
  }

  /**
   * @return le gestionnaire des prop elementaires
   */
  public H2dRefluxElementPropertyMngAbstract getElementPropMng() {
    return params_.getH2dRefluxParametres().getElementPropMng();
  }

  /**
   * @return la fenetre parent
   */
  public final Frame getFrame() {
    return params_.getFrame();
  }

  public TrRefluxProjectDispatcherListener getGlobalListener() {
    return (TrRefluxProjectDispatcherListener) getH2dRefluxParametres().getListener();
  }

  /**
   * @return les params
   */
  public H2dRefluxParameters getH2dRefluxParametres() {
    return params_.getH2dRefluxParametres();
  }

  /**
   * @return FileFormatSoftware.REFLUX_IS.name
   */
  @Override
  public String getID() {
    return FileFormatSoftware.REFLUX_IS.name;
  }

  @Override
  public FudaaCommonImplementation getImpl() {
    return params_.getImpl();
  }

  @Override
  public File getParamsFile() {
    return getRefluxParametres().getFile();
  }

  @Override
  public BuMenu getProjectMenu() {
    if (projectMenu_ == null) {
      projectMenu_ = new BuMenu(getS("Projet"), "PROJET");
      projectMenu_.setName("mnProject");
      projectMenu_.setIcon(null);
      projectMenu_.addMenuItem(getS("Int�gration en temps"), "TIME_STEP", TrResource.TR.getIcon("temps"), true)
          .addActionListener(TrRefluxProjet.this);
      addDocMenuItem(projectMenu_);
      buildFilleActions();
      projectMenu_.addSeparator();
      projectMenu_.add(projectAction_.buildMenuItem(EbliComponentFactory.INSTANCE));
      projectMenu_.add(visuAction_.buildMenuItem(EbliComponentFactory.INSTANCE));
      projectMenu_.add(courbeAction_.buildMenuItem(EbliComponentFactory.INSTANCE));
      projectMenu_.addSeparator();
      projectMenu_.add(getCalculActions().getCalcul());
    }
    return projectMenu_;
  }

  /**
   * @return les parametres correspondants
   */
  public TrRefluxParametres getRefluxParametres() {
    return params_;
  }

  /**
   * @return FileFormatSoftware.REFLUX_IS.name
   */
  @Override
  public String getSoftwareID() {
    return FileFormatSoftware.REFLUX_IS.name;
  }

  @Override
  public FileFormatSoftware getSystemVersion() {
    return params_.getDicoParam().getDicoFileFormatVersion().getSoftVersion();
  }

  @Override
  public String getTitle() {
    return params_.getTitle();
  }

  @Override
  public TrParametres getTrParams() {
    return params_;
  }

  /**
   * Ne fait rien.
   */
  @Override
  public boolean loadAll(final ProgressionInterface _inter) {
    return true;
  }

  @Override
  public boolean save(final ProgressionInterface _prog) {
    final File dest = params_.getFile();
    if (dest == null) {
      return saveAs(_prog);
    }
    if (getState().isModified()) {
      final boolean res = params_.save(_prog);
      if (res) {
        getState().setParamsModified(false);
      }
      final boolean ui = TrProjectPersistence.saveProject(getImpl(), this, getParamsFile(), _prog);
      if (ui) {
        getState().setUIModified(false);
      }

      return res;
    }
    return true;
  }

  @Override
  public boolean saveAs(final ProgressionInterface _prog) {
    File fic = getUI().ouvrirFileChooser(BuResource.BU.getString("Enregistrer sous"), null, true);
    if (fic == null) {
      return false;
    }
    if (!fic.getName().endsWith(".inp")) {
      fic = new File(fic.getAbsolutePath() + ".inp");
    }
    final boolean res = params_.saveAs(_prog, fic);
    if (!res) {
      return saveAs(_prog);
    }

    getState().setParamsModified(false);
    if (TrProjectPersistence.saveProject(getImpl(), this, getParamsFile(), _prog)) {
      getState().setUIModified(false);
    }
    return res;
  }

  @Override
  public File saveCopy(final ProgressionInterface _prog, final File _f) {
    File fic = _f;
    if (fic == null) {
      fic = getUI().ouvrirFileChooser(FudaaLib.getS("Enregistrer une copie"), null, true);
      if (fic != null && !fic.getName().endsWith(".inp")) {
        fic = new File(fic.getAbsolutePath() + ".inp");
      }
    }
    if (fic == null) {
      return null;
    }
    fic = params_.saveCopy(_prog, fic);
    // pour recommencer l'op�ration dans le cas d'une erreur
    if (_f == null && fic == null) {
      return saveCopy(_prog, null);
    }
    if (fic != null) {
      TrProjectPersistence.saveProject(getImpl(), this, fic, _prog);
    }
    return fic;
  }
}
