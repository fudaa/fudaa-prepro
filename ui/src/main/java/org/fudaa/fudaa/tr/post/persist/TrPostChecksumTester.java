package org.fudaa.fudaa.tr.post.persist;

import com.memoire.fu.FuLog;
import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;

/**
 * test that files are not modified since last save operation
 * 
 * @author deniger
 */
public class TrPostChecksumTester {

  private final TrPostPersistenceFileFinder fileFinder;

  private static List<String> read(File dest) {
    if (dest == null || !dest.exists()) {
      return Collections.emptyList();
    }
    FortranReader reader = null;
    List<String> lines = new ArrayList<>();
    try {
      reader = new FortranReader(new FileReader(dest));
      reader.setCommentInOneField("#");
      reader.setJumpCommentLine(true);
      reader.setJumpBlankLine(true);
      try {
        while (true && reader.getLine()!=null) {
          lines.add(reader.readLine());
        }
      } catch (EOFException e) {//oblige...
      }
    } catch (IOException e) {
      FuLog.error(e);
    } finally {
      FortranLib.close(reader);
    }
    return lines;

  }

  private final TrPostProjet projet;

  private final TrPostChecksumWriter writer;

  /**
   * @param fileFinder
   * @param projet
   */
  public TrPostChecksumTester(TrPostPersistenceFileFinder fileFinder, TrPostProjet projet) {
    super();
    this.fileFinder = fileFinder;
    this.writer = new TrPostChecksumWriter(fileFinder, projet);
    this.projet = projet;
  }

  boolean modified;

  public boolean isModified() {
    return modified;
  }

  public void testFiles() {
    modified = isSetupFileModified();
    if (!modified) {
      modified = isVariablesFilesModified();
    }
    if (!modified) {
      modified = isSourcesFilesModified();
    }

  }

  private boolean isSourcesFilesModified() {
    List<TrPostSource> sources = projet.getSources().getLstSources();
    for (TrPostSource source : sources) {
      File checksumFileForSources = fileFinder.getChecksumFileForSources(source);
      if (!CtuluLibFile.exists(checksumFileForSources)) {
        return true;
      }
      List<String> read = read(checksumFileForSources);
      List<String> checksumForSources = writer.getChecksumForSources(source);
      if (isDifferent(read, checksumForSources)) {
        return true;
      }
    }
    return false;
  }

  private boolean isVariablesFilesModified() {
    List<String> variablesChecksums = writer.getVariablesChecksums();
    List<String> read = read(fileFinder.getChecksumFileForVariables());
    return isDifferent(variablesChecksums, read);
  }

  private boolean isDifferent(List<String> variablesChecksums, List<String> read) {
    if (read.size() != variablesChecksums.size()) {
      return true;
    }
    for (int i = 0; i < read.size(); i++) {
      if (!CtuluLibString.isEquals(read.get(i), variablesChecksums.get(i))) {
        return true;
      }
    }
    return false;
  }

  private boolean isSetupFileModified() {
    File setupFile = fileFinder.getMainSetupFile();
    String checksum = getChecksum(setupFile);
    List<String> read = read(fileFinder.getChecksumFileForSetup());
    if (read.size() != 1) {
      return true;
    }
    return !checksum.equals(read.get(0));

  }

  private String getChecksum(File setupFile) {
    if (setupFile == null || !setupFile.exists()) {
      return CtuluLibString.EMPTY_STRING;
    }
    return TrPostCheckSumGenerator.getMD5Checksum(setupFile);
  }

}
