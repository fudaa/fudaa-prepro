/**
 * @creation 24 mars 2004
 * @modification $Date: 2007-05-04 14:01:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * Classe implementant les methodes de bases.
 * 
 * @author Fred Deniger
 * @version $Id: TrPostSourceSerafin.java,v 1.15 2007-05-04 14:01:52 deniger Exp $
 */
public abstract class TrPostSourceReaderCommonAbstract implements TrPostSourceReaderInterface {

  Collection<File> file_;

  final List<H2dVariableType> initVar_;

  final double[] times_;

  protected TrPostSourceReaderCommonAbstract(Collection<File> _file, final double[] _time, H2dVariableType[] _initVar) {
    file_ = _file;
    times_ = _time;
    initVar_ = Collections.unmodifiableList(Arrays.asList(_initVar));
  }

  protected TrPostSourceReaderCommonAbstract(final File _file, final double[] _time, H2dVariableType[] _initVar) {
    this(Collections.unmodifiableCollection(Arrays.asList(_file)), _time, _initVar);
  }

  @Override
  public final Collection<File> getFiles() {
    return file_;
  }

  /**
   * @return the initVar
   */
  @Override
  public final List<H2dVariableType> getInitVar() {
    return initVar_;
  }

  @Override
  public final int getNbTimeStep() {
    return times_ == null ? 0 : times_.length;
  }

  @Override
  public final double getTimeStep(int _idx) {
    return times_ == null ? 0 : times_[_idx];
  }

  public final boolean isOpened(final File _f) {
    return _f != null && getFiles().contains(_f);
  }

//  public boolean isRubar() {
//    return false;
//  }

}