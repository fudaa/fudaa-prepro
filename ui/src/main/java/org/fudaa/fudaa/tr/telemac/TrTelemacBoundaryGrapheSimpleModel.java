/**
 * @creation 2 sept. 2004
 * @modification $Date: 2007-05-04 14:01:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInternalFrame;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.gui.CtuluHelpComponent;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.dico.DicoParamsListener;
import org.fudaa.dodico.h2d.H2dBcFrontierInterface;
import org.fudaa.dodico.h2d.H2dBoundary;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcListener;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcManager;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBordParFrontiere;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBoundary;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.courbe.*;
import org.fudaa.ebli.palette.BPalettePlageDefault;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacBoundaryGrapheSimpleModel.java,v 1.20 2007-05-04 14:01:46 deniger Exp $
 */
public class TrTelemacBoundaryGrapheSimpleModel extends EGGrapheSimpleModel implements H2dTelemacBcListener,
    DicoParamsListener {
  private class TrTelemacBoundaryCourbeFille extends EGFilleSimple implements CtuluHelpComponent {
    FudaaCommonImplementation impl_;

    /**
     * @param _impl l'impl parent utilise pour recupere les infos du software / the parent impl used to get the software info
     * @param _s les infos sur le document en cours / the news on the current document
     */
    public TrTelemacBoundaryCourbeFille(final FudaaCommonImplementation _impl, final BuInformationsDocument _s) {
      super(TrTelemacBoundaryGrapheSimpleModel.this, "", _impl, _s, new EGTableGraphePanel());
      impl_ = _impl;
      setFrameIcon(EbliResource.EBLI.getToolIcon("curves"));
      setPreferredSize(new Dimension(400, 300));
      double[] x = null;
      final TrTelemacBoundaryGrapheSimpleModel model = TrTelemacBoundaryGrapheSimpleModel.this;
      if (model.getNbEGObject() > 0) {
        final EGCourbe c = model.getCourbe(0);
        if (c != null) {
          x = ((TrTelemacBoundaryCourbeModel) c.getModel()).getX();
        }
      }
      hAxeIt_ = new TrTelemacFrontierNumberIterator(bcMng_.getGrid(), fr_, b_, x);
      final EGAxeHorizontal hAxe = new EGAxeHorizontal(true);
      hAxe.setSpecificFormat(hAxeIt_);
      hAxe.setAxisIterator(hAxeIt_);
      hAxe.setValueEditor(hAxeIt_);
      hAxe.setTitre(EbliLib.getS("Indices"));
      hAxe.setGraduations(true);
      getGraphe().setXAxe(hAxe);
      getGraphe().setCmd(new CtuluCommandManager());
      setName("ifCourbesFr" + nameIdx_++);
      tablePanel_.setNameForColX(TrResource.getS("index"));
      tablePanel_.setNameForColY(TrResource.getS("valeur"));
      getGraphe().restore();
      updateTitre();
    }

    @Override
    protected void fireInternalFrameEvent(final int _id) {
      if (_id == InternalFrameEvent.INTERNAL_FRAME_CLOSED) {
        removeListeners();
      }
      super.fireInternalFrameEvent(_id);
    }

    @Override
    public String getComponentTitle() {
      return getTitle();
    }

    @Override
    public String getShortHtmlHelp() {
      final CtuluHtmlWriter r = new CtuluHtmlWriter();
      r.h2(CtuluResource.CTULU.getString("Description"));
      r.para();
      r.append(TrResource.getS("Affiche les �volutions des variables le long du bord"));
      r.nl();
      r.close(r.addTag("b"), TrResource.getS("Type du bord:")).append(CtuluLibString.ESPACE).append(
          b_.getType().getName());
      r.h2(TrResource.getS("Documents associ�s"));
      r.addTag("ul");
      r.close(r.addTag("li"), impl_.buildLink(TrResource.getS("Description du composant d'affichage des courbes"),
          "common-curves"));
      r.close(r.addTag("li"), impl_.buildLink(TrResource.getS("Edition des conditions limites"), "telemac-editor-bc"));
      return r.toString();
    }
  }

  /**
   * Utilise pour avoir un nom unique
   * .used to get a uniqe name
   */
  static int nameIdx_;
  transient H2dTelemacBoundary b_;
  transient H2dTelemacBcManager bcMng_;
  transient H2dTelemacBoundary bSave_;
  transient H2dTelemacBordParFrontiere fr_;
  transient BuInternalFrame frame_;
  transient TrTelemacFrontierNumberIterator hAxeIt_;

  /*
   * private class TelemacSpeciAxeX extends EGAxeHorizontal {
   */

  /**
   * specifie qu'un axe d'entier est utilise
   * specify that an entire axis is used
   */
  /*
   * public TelemacSpeciAxeX() { super(true); } public String getReelString(double _val){ return
   * getGraduationFor((int)_val); } protected String getGraduationFor(int _i){ return
   * CtuluLibString.getString(bcMng_.getGrid().getFrontiers().getFrontiereIndice( fr_.getFrIdx(), fr_.getReelIdxOnFr(_i
   * + b_.getIdxDeb())) + 1); } }
   */
  public TrTelemacBoundaryGrapheSimpleModel(final H2dTelemacBcManager _bcMng, final H2dTelemacBordParFrontiere _fr,
                                            final H2dTelemacBoundary _bord) {
    bcMng_ = _bcMng;
    bcMng_.addClChangedListener(this);
    bcMng_.getDicoParams().addModelListener(this);
    fr_ = _fr;
    b_ = _bord;
    bSave_ = b_.createMemento();
    buildCourbes();
  }

  private double[] buildCourbes() {

    final H2dVariableType[] vars = bcMng_.getVariablesForPoint(b_);
    if (vars == null) {
      return null;
    }
    Arrays.sort(vars);
    final Map m = bcMng_.getVariablesForPointInfo(b_);
    final double[] x = fr_.getAbsCurviligne(b_);
    final int nb = vars.length;
    if (nb > 0) {
      for (int i = 0; i < nb; i++) {
        final H2dVariableType varReel = vars[i];
        final EGAxeVertical v = new EGAxeVertical();
        v.setTitre(varReel.getName());
        v.setGraduations(true);

        final String info = m == null ? null : (String) m.get(varReel);
        final String nom = info == null ? varReel.getName() : varReel.getName() + " (" + info + ")";
        final EGCourbeSimple c = new EGCourbeSimple(v, new TrTelemacBoundaryCourbeModel(nom, fr_, b_, varReel, x));

        c.setAspectContour(BPalettePlageDefault.getColor(i));
        internalAddElement(c);
      }
    } else {
      final EGAxeVertical v = new EGAxeVertical();
      v.setTitre(TrResource.getS("Aucune variable editable"));
      v.setGraduations(false);
      final EGCourbeSimple c = new EGCourbeSimple(v, new TrTelemacBoundaryCourbeModel(CtuluLibString.EMPTY_STRING, fr_,
          b_, null, x));
      internalAddElement(c);
    }
    fireStructureChanged();
    return x;
  }

  private Collection getDisplayedVar() {
    final List r = new ArrayList();
    for (final Iterator it = element_.iterator(); it.hasNext(); ) {
      final TrTelemacBoundaryCourbeModel m = (TrTelemacBoundaryCourbeModel) ((EGCourbe) it.next()).getModel();
      if (m.getVar() != null) {
        r.add(m.getVar());
      }
    }
    return r;
  }

  private boolean isVarChanged() {
    final Collection c = getDisplayedVar();
    final H2dVariableType[] vars = bcMng_.getVariablesForPoint(b_);
    if (vars == null) {
      return c.size() > 0;
    } else if (vars.length == c.size()) {
      return !c.containsAll(Arrays.asList(vars));
    } else {
      return true;
    }
  }

  private void rebuild() {
    element_.clear();
    fireStructureChanged();
    final double[] x = buildCourbes();
    if (hAxeIt_ != null) {
      hAxeIt_.setFrAndBound(fr_, b_, x);
    }
  }

  protected void removeListeners() {
    bcMng_.removeClChangedListener(this);
    bcMng_.getDicoParams().removeModelListener(this);
  }

  protected void updateTitre() {
    if (frame_ != null) {
      frame_.setTitle(H2dResource.getS("Fronti�re") + CtuluLibString.ESPACE + (fr_.getBoundaryPosition(b_) + 1) + " ("
          + b_.getType() + ")");
    }
  }

  @Override
  public void bcBoundaryTypeChanged(final H2dBoundary _b, final H2dBoundaryType _old) {
    if (_b == b_) {
      rebuild();
    }
  }

  @Override
  public void bcFrontierStructureChanged(final H2dBcFrontierInterface _b) {
    if (_b == fr_) {
      // on recupere le bord contenant
      // we get the containing edge 
      if (!fr_.contains(b_)) {
        H2dTelemacBoundary newBoundary = null;
        H2dTelemacBoundary new2 = null;
        for (int i = 0; i < fr_.getNbBord(); i++) {
          final H2dTelemacBoundary b = (H2dTelemacBoundary) fr_.getBord(i);
          if ((b.containsIdx(bSave_.getIdxDeb())) || (b.containsIdx(bSave_.getIdxFin()))) {
            if (new2 == null) {
              new2 = b;
            }
            if (b.getType() == bSave_.getType()) {
              newBoundary = b;
              break;
            }
          }
        }
        b_ = newBoundary == null ? new2 : newBoundary;
        bSave_ = b_ == null ? null : b_.createMemento();
      }
      updateTitre();
      rebuild();
    }
  }

  @Override
  public void bcParametersChanged(final H2dBoundary _b, final H2dVariableType _t) {
    if (_b == b_) {
      bcPointsParametersChanged(_t);
      updateTitre();
    }
  }

  @Override
  public void bcPointsParametersChanged(final H2dVariableType _t) {
    // la liste des variables est modifiee
    // the list of variables is modified
    if (isVarChanged()) {
      rebuild();
    }
    // sinon, on met les courbes a jour
    //otherwise curves are update
    else {
      for (final Iterator it = element_.iterator(); it.hasNext(); ) {
        final EGCourbe courbe = (EGCourbe) it.next();
        final TrTelemacBoundaryCourbeModel m = (TrTelemacBoundaryCourbeModel) courbe.getModel();
        if (m.valueChangedFor(_t)) {
          fireCourbeContentChanged(courbe);
          break;
        }
      }
    }
  }

  public BuInternalFrame buildFille(final FudaaCommonImplementation _impl, final BuInformationsDocument _s) {
    if (frame_ == null) {
      frame_ = new TrTelemacBoundaryCourbeFille(_impl, _s);
      updateTitre();
    }
    return frame_;
  }

  @Override
  public boolean canAddCourbe() {
    return false;
  }

  @Override
  public void dicoParamsEntiteAdded(final DicoParams _cas, final DicoEntite _ent) {
    if (bcMng_.isKeywordUsedByBc(_ent) && isVarChanged()) {
      rebuild();
    }
  }

  @Override
  public void dicoParamsEntiteCommentUpdated(final DicoParams _cas, final DicoEntite _ent) {
  }

  @Override
  public void dicoParamsEntiteRemoved(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    if (bcMng_.isKeywordUsedByBc(_ent) && isVarChanged()) {
      rebuild();
    }
  }

  @Override
  public void dicoParamsEntiteUpdated(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    if (bcMng_.isKeywordUsedByBc(_ent) && isVarChanged()) {
      rebuild();
    }
  }

  public void dicoParamsProjectModifyStateChanged(final DicoParams _cas) {
  }

  public void dicoParamsStateLoadedEntiteChanged(final DicoParams _cas, final DicoEntite _ent) {
  }

  @Override
  public void dicoParamsValidStateEntiteUpdated(final DicoParams _cas, final DicoEntite _ent) {
  }

  @Override
  public void dicoParamsVersionChanged(final DicoParams _cas) {
  }

  @Override
  public boolean isContentModifiable() {
    return true;
  }

  @Override
  public boolean isStructureModifiable() {
    return false;
  }
}
