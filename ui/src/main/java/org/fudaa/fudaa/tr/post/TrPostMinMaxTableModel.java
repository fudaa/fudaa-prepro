/**
 * @creation 24 nov. 2004
 * @modification $Date: 2007-03-30 15:39:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextArea;
import gnu.trove.TIntArrayList;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseEvent;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.common.TrCommonImplementation;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.data.TrPostDataMinMaxGlobalItem;

/**
 * @author Fred Deniger
 * @version $Id: TrPostMinMaxTableModel.java,v 1.14 2007-03-30 15:39:29 deniger Exp $
 */
public class TrPostMinMaxTableModel extends AbstractTableModel {

  TrPostSource src_;
  boolean[] isAlreadyComputed_;
  H2dVariableType[] varId_;
  double[] min_;
  double[] max_;
  Integer zero_ = new Integer(0);
  Integer un_ = new Integer(1);
  String[] valueFirstCol_ = new String[] { TrResource.getS("Ne pas calculer"), TrResource.getS("Calculer") };
  String[] valueFirstColIfComputed_ = new String[] { TrResource.getS("Ne pas calculer"), TrResource.getS("Recalculer") };
  Integer[] computeState_;

  ComboBoxModel cbModelFirstCol_;
  ComboBoxModel cbModelFirstColIfComputed_;

  private class InfoPanel extends BuPanel implements ListSelectionListener {

    JTextArea area_;

    protected InfoPanel(final JTable _t) {
      _t.getSelectionModel().addListSelectionListener(this);
      area_ = new BuTextArea();
      area_.setRows(6);
      area_.setColumns(60);
      area_.setWrapStyleWord(true);
      area_.setLineWrap(true);
      area_.setEditable(false);
      add(new BuScrollPane(area_));
      updateText(_t.getSelectedRow());
    }

    private void updateText(final int _row) {
      if (_row < 0) {
        area_.setText(CtuluLibString.EMPTY_STRING);
      } else {
        final StringBuffer buf = new StringBuffer();
        buf.append(varId_[_row].getName()).append(CtuluLibString.LINE_SEP);
        if (src_.isMinMaxCompute(varId_[_row])) {
          final TrPostDataMinMaxGlobalItem it = src_.getGlobalExtrema(varId_[_row], null);
          buf.append(CtuluLib.getS("Maximum:")).append(CtuluLibString.LINE_SEP);
          final String sep = " - ";
          buf.append(sep).append(
              TrResource.getS("La valeur est atteinte {0} fois", CtuluLibString.getString(it.getGlobalMaxOccurence())));
          buf.append(CtuluLibString.LINE_SEP);
          buf.append(sep).append(TrResource.getS("Premier pas de temps:")).append(CtuluLibString.ESPACE).append(
              src_.getTimeFormatter().format(it.getGlobalMaxFirstTimeStep())).append(CtuluLibString.LINE_SEP).append(
              CtuluLibString.LINE_SEP);
          buf.append(CtuluLib.getS("Minimum:")).append(CtuluLibString.LINE_SEP);
          buf.append(sep).append(
              TrResource.getS("La valeur est atteinte {0} fois", CtuluLibString.getString(it.getGlobalMinOccurence())));
          buf.append(CtuluLibString.LINE_SEP);
          buf.append(sep).append(TrResource.getS("Premier pas de temps:")).append(CtuluLibString.ESPACE).append(
              src_.getTimeFormatter().format(it.getGlobalMinFirstTimeStep()));
        } else {
          buf.append(TrResource.getS("Maxima non calcul�s"));
        }
        area_.setText(buf.toString());
      }

    }

    @Override
    public void valueChanged(final ListSelectionEvent _e) {
      if (!_e.getValueIsAdjusting()) {
        final ListSelectionModel s = (ListSelectionModel) _e.getSource();
        if (s.getMaxSelectionIndex() == s.getMinSelectionIndex()) {
          updateText(s.getMaxSelectionIndex());
        }
      }
    }
  }

  private class CustomTable extends JTable {

    protected CustomTable() {}

    @Override
    public String getToolTipText(final MouseEvent _event) {
      final Point p = _event.getPoint();
      if (p != null) {
        final int col = convertColumnIndexToModel(columnAtPoint(p));
        if (col == 1) {
          final int row = rowAtPoint(p);
          if (isAlreadyComputed_[row]) {
            final TrPostDataMinMaxGlobalItem it = src_.getGlobalExtrema(varId_[row], null);
            return TrResource.getS("Valeur maximale obtenue � t= {0}", src_.getTimeFormatter().format(
                it.getGlobalMaxFirstTimeStep()));
          }
        }
        if ((col == 2) || (col == 3)) {
          final int row = rowAtPoint(p);
          if (isAlreadyComputed_[row]) {
            final double v = col == 2 ? min_[row] : max_[row];
            return Double.toString(v);
          }
        }
      }
      return null;
    }
  }

  protected ComboBoxModel getModelCb() {
    if (cbModelFirstCol_ == null) {
      cbModelFirstCol_ = new DefaultComboBoxModel(valueFirstCol_);
    }
    return cbModelFirstCol_;
  }

  protected ComboBoxModel getModelCbCompute() {
    if (cbModelFirstColIfComputed_ == null) {
      cbModelFirstColIfComputed_ = new DefaultComboBoxModel(valueFirstColIfComputed_);
    }
    return cbModelFirstColIfComputed_;
  }

  protected TableCellEditor getFirstColEditor() {
    return new FirstColEditor();
  }

  @Override
  public String getColumnName(final int _column) {
    if (_column == 0) {
      return TrResource.getS("Action");
    } else if (_column == 1) {
      return TrResource.getS("Variable");
    } else if (_column == 2) {
      return TrResource.getS("Min");
    } else if (_column == 3) { return TrResource.getS("Max"); }
    return null;
  }

  /**
   * @param _s la source
   * @return le model
   */
  public static JTable createTableFor(final TrPostSource _s) {
    final TrPostMinMaxTableModel r = new TrPostMinMaxTableModel();
    r.src_ = _s;
    r.varId_ = _s.getAvailableVar();
    if (r.varId_ == null) {
      r.varId_ = new H2dVariableType[0];
    }
    r.recomputeAll();
    final JTable table = r.createTable();
    table.setModel(r);
    table.getColumnModel().getColumn(0).setCellEditor(r.getFirstColEditor());
    return table;
  }

  private JTable createTable() {
    return new CustomTable();
  }

  /**
   * @param _t la table qui sert a affichier les extrema
   * @return le panneau d'info
   */
  public JPanel createInfoPanel(final JTable _t) {
    return new InfoPanel(_t);

  }

  private class FirstColEditor extends DefaultCellEditor {

    JComboBox cb_;

    /**
     * Constructeur par defaut.
     */
    public FirstColEditor() {
      super(new JComboBox());
      cb_ = (JComboBox) super.editorComponent;
    }

    @Override
    public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _isSelected,
        final int _row, final int _column) {
      final int i = ((computeState_ == null) || (computeState_[_row] == null)) ? 0 : computeState_[_row].intValue();
      final ComboBoxModel m = isAlreadyComputed_[_row] ? getModelCbCompute() : getModelCb();
      cb_.setModel(m);
      cb_.setSelectedIndex(i);
      return cb_;
    }

    @Override
    public Object getCellEditorValue() {
      return cb_.getSelectedIndex() == 0 ? zero_ : un_;
    }
  }

  @Override
  public int getColumnCount() {
    return 4;
  }

  @Override
  public int getRowCount() {
    return varId_.length;
  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    return _columnIndex == 0;
  }

  @Override
  public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
    if (_columnIndex == 0 && (_value instanceof Integer)) {
      final int i = ((Integer) _value).intValue();
      if (i == 0) {
        if (computeState_ != null) {
          computeState_[_rowIndex] = zero_;
        }
      } else if (i == 1) {
        if (computeState_ == null) {
          computeState_ = new Integer[getRowCount()];
        }
        computeState_[_rowIndex] = un_;
      }

    }
  }

  boolean apply_;

  protected void apply(final ProgressionInterface _inter, final TrCommonImplementation _impl) {

    if (apply_) { return; }

    apply_ = true;
    _impl.setMainMessage(TrResource.getS("Calcul des extrema"));
    final TIntArrayList list = new TIntArrayList();
    if (computeState_ == null) { return; }
    for (int i = varId_.length - 1; i >= 0; i--) {
      if ((computeState_[i] != null) && (computeState_[i].intValue() != 0)) {
        list.add(i);
      }
    }
    final int nb = list.size();
    if (nb == 0) return;
    final ProgressionUpdater up = new ProgressionUpdater(_impl.createProgressionForMainPanel());
    up.setValue(nb, 10);
    for (int i = 0; i < nb; i++) {
      final int idx = list.getQuick(i);
      // on efface les anciennes valeurs
      if (isAlreadyComputed_[idx]) {
        src_.removeMinMax(varId_[idx].getShortName());
      }
      src_.getGlobalExtrema(varId_[idx], _inter);
      computeState_[idx] = null;
    }
    recomputeAll();
    fireTableDataChanged();
    apply_ = false;
  }

  protected void recomputeAll() {
    final int nb = varId_.length;
    if (isAlreadyComputed_ == null) {
      isAlreadyComputed_ = new boolean[nb];
    }
    if (min_ == null) {
      min_ = new double[nb];
    }
    if (max_ == null) {
      max_ = new double[nb];
    }
    for (int i = nb - 1; i >= 0; i--) {
      isAlreadyComputed_[i] = src_.isMinMaxCompute(varId_[i]);
      if (isAlreadyComputed_[i]) {
        final TrPostDataMinMaxGlobalItem val = src_.getGlobalExtrema(varId_[i], null);
        min_[i] = val.getMin();
        max_[i] = val.getMax();
      }
    }
  }

  @Override
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {
    // la premier colonne
    if (_columnIndex == 0) {
      // rien n'a ete modifie ou rien n'est demande
      if (computeState_ == null || computeState_[_rowIndex] != un_) { return valueFirstCol_[0]; }
      return isAlreadyComputed_[_rowIndex] ? valueFirstColIfComputed_[1] : valueFirstCol_[1];
    } else if (_columnIndex == 1) {
      return varId_[_rowIndex];
    } else if (isAlreadyComputed_[_rowIndex]) {
      if (_columnIndex == 2) {
        return CtuluLib.DEFAULT_NUMBER_FORMAT.format(min_[_rowIndex]);
      } else if (_columnIndex == 3) { return CtuluLib.DEFAULT_NUMBER_FORMAT.format(max_[_rowIndex]); }
    }
    return CtuluLibString.EMPTY_STRING;
  }
}