package org.fudaa.fudaa.tr.post.dialogSpec;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuValueValidator;
import com.memoire.bu.BuVerticalLayout;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluAnalyzeGroup;
import org.fudaa.ctulu.CtuluDurationFormatter;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.editor.CtuluValueEditorInteger;
import org.fudaa.ctulu.editor.CtuluValueEditorTime;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gui.CtuluComboBoxModelAdapter;
import org.fudaa.dodico.ef.operation.EfIndexHelper;
import org.fudaa.dodico.ef.operation.EfTrajectoireParameters;
import org.fudaa.dodico.ef.operation.EfTrajectoireParametersMarqueur;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.commun.impl.FudaaPanelTaskModel;
import org.fudaa.fudaa.sig.layer.FSigTempLineInLayer;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostFlecheContent;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostTrajectoireComputeAndDisplayActivity;
import org.fudaa.fudaa.tr.post.TrPostTrajectoireLineLayer;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.post.persist.CommonProperties;
import org.fudaa.fudaa.tr.post.profile.MvLineChooser;

public class TrPostTrajectoireTaskModel implements FudaaPanelTaskModel {

  /**
	 * 
	 */
  private static final double MAXDIST = 1E-3;

  TrPostTrajectoireComputeAndDisplayActivity algoCalcul_;

  /**
   * booleen qui indique si le wizard a �t�t lanc� depuis un calque
   */

  // ArrayList<TrPostSource> source_;
  TrPostVisuPanel calque_;
  TrPostCommonImplementation impl_;
  // choix mode
  JComboBox choixMode_ = new JComboBox(new String[] { TrLib.getString("Lignes de courant"), TrLib.getString("Trajectoires") });

  JComponent duree_;

  JComponent finesse_;

  final MvLineChooser lineChooser_;
  JComboBox cbFirstTime_;
  JList listVar_;
  ListModel modelPdt_;

  DefaultTableModel modelResult_;

  /**
   * Creation du panel graphique.
   * 
   * @return
   */
  TrPostSource source_;
  CtuluValueEditorDouble doubleEditor_ = new CtuluValueEditorDouble(false);
  CtuluValueEditorDouble doubleMarkEditor_ = new CtuluValueEditorDouble(false);
  CtuluValueEditorInteger integerEditor_ = new CtuluValueEditorInteger(false);
  JComboBox cbVitesse_;
  JComponent nbPoints_;

  // choix segment
  JComponent x_;
  JComponent x2_;
  JComponent y_;
  JComponent y2_;
  CtuluValueEditorTime dureeEditor_;

  /**
   * Constructeur reserv� au calques
   * 
   * @param _builderParams
   * @param _calque
   * @param _modelGraphe
   * @param _panelVariables
   * @param _source
   */
  public TrPostTrajectoireTaskModel(final TrPostVisuPanel calque, TrPostCommonImplementation impl) {
    calque_ = calque;
    impl_ = impl;
    source_ = calque.getSource();
    int nbTimeStep = source_.getNbTimeStep();
    if (nbTimeStep < 2)
      choixMode_.setEnabled(false);
    doubleEditor_.setFormatter(CtuluNumberFormatDefault.buildNoneFormatter(3, true));
    x_ = doubleEditor_.createEditorComponent();
    x2_ = doubleEditor_.createEditorComponent();
    y_ = doubleEditor_.createEditorComponent();
    y2_ = doubleEditor_.createEditorComponent();

    dureeEditor_ = new CtuluValueEditorTime();
    dureeEditor_.setFmt(new CtuluDurationFormatter(true, false));
    duree_ = dureeEditor_.createEditorComponent();
    double defaut = 60;
    if (nbTimeStep > 2) {
      defaut = (source_.getTimeStep(nbTimeStep - 1) - source_.getTimeStep(0)) / 2;
    }
    dureeEditor_.setValue(defaut, duree_);
    integerEditor_.setVal(BuValueValidator.MIN(1));
    integerEditor_.setEditable(true);
    nbPoints_ = integerEditor_.createEditorComponent();
    finesse_ = integerEditor_.createEditorComponent();
    integerEditor_.setValue(Integer.valueOf(1), nbPoints_);
    integerEditor_.setValue(Integer.valueOf(1), finesse_);
    lineChooser_ = new MvLineChooser(calque.getSelectedLine(), calque.getSource().getGrid(), calque.getCtuluUI());
    FSigTempLineInLayer tmpLine = new FSigTempLineInLayer(calque);
    lineChooser_.setTmpLine(tmpLine);
    modelPdt_ = source_.getNewTimeListModel();
    
    
  }

  
  /**
   * reference du layer a ecraser dans le cas ou l'on rejoue les donn�es. Null si utilisation classique.
   */
  TrPostTrajectoireLineLayer layerAEcraser_ = null;

  /**
   * Constructeur appel� pour rejouer les donn�es. Passe en parametre la structure efTrajectoire qui contient toutes les
   * infos de pr� remplissage
   * 
   * @param calque
   * @param dataReplay qui contient toutes les infos de pr� remplissage
   * @param calqueAEcraser calque a ecraser en recopiant le r�sultat par dessus
   */
  public TrPostTrajectoireTaskModel(final TrPostVisuPanel calque, EfTrajectoireParameters dataReplay, TrPostTrajectoireLineLayer calqueAEcraser) {
    this(calque,null);

    // -- on pr�remplit les donn�es avec les infos du dataReplay --//
    layerAEcraser_ = calqueAEcraser;

    // -- on reinitialise les infos avec les donn�es des trajectoires/lignes de courant. --//
    // -- on initialise les params graphiques --//
    getPanel();

    // -- selection du vecteur vitesse --//
    for (int k = 0; k < cbVitesse_.getItemCount(); k++)
      if (((TrPostFlecheContent) cbVitesse_.getItemAt(k)).getVx() == dataReplay.vx)
        if (((TrPostFlecheContent) cbVitesse_.getSelectedItem()).getVy() == dataReplay.vy)
          cbVitesse_.setSelectedIndex(k);

    // -- duree itegration --//
    dureeEditor_.setValue(dataReplay.dureeIntegration_, duree_);

    // -- finesse --//
    integerEditor_.setValue(dataReplay.finesse_, finesse_);

    // -- pas de temps initial --//
    cbFirstTime_.setSelectedIndex(dataReplay.firstTimeStepIdx_);

    // -- type traj ou ligne de courant --//
    if (dataReplay.isLigneDeCourant)
      choixMode_.setSelectedIndex(0);
    else choixMode_.setSelectedIndex(1);

    // -- choix des variables --//
    if (dataReplay.varsASuivre_.size() >= 1) {

      int[] indicesToselect = new int[dataReplay.varsASuivre_.size()];
      for (int i = 0; i < dataReplay.varsASuivre_.size(); i++) {

        for (int k = 0; k < listVar_.getModel().getSize(); k++) {
          if (((CtuluVariable) listVar_.getModel().getElementAt(k)) == dataReplay.varsASuivre_.get(i))
            indicesToselect[i] = k;
        }
      }
      listVar_.setSelectedIndices(indicesToselect);
    }

    // -- premier point --//
    doubleEditor_.setValue(dataReplay.segment_.get(0).x, x_);
    doubleEditor_.setValue(dataReplay.segment_.get(0).y, y_);
    integerEditor_.setValue(dataReplay.nbPointsInitiaux_, nbPoints_);
    // -- dernier point --//
    doubleEditor_.setValue(dataReplay.segment_.get(dataReplay.segment_.size() - 1).x, x2_);
    doubleEditor_.setValue(dataReplay.segment_.get(dataReplay.segment_.size() - 1).y, y2_);

    // -- marqueurs --//
    if (dataReplay.marqueur_ != null) {
      if (dataReplay.marqueur_.timeStep_)
        cbMarks_.setSelectedIndex(1);
      else cbMarks_.setSelectedIndex(2);

      doubleMarkEditor_.setValue(dataReplay.marqueur_.deltaMax_, markValue_);
    } else cbMarks_.setSelectedIndex(0);

  }

  private List<Coordinate> getPoints() {
    Coordinate c = getFirsCoordinate();
    Coordinate cEnd = getEndCoordinate();
    int nbLigne = (Integer) integerEditor_.getValue(nbPoints_);
    return getPointsFromSegment(c, cEnd, nbLigne);

  }

  public static List<Coordinate> getPointsFromSegment(Coordinate c, Coordinate cEnd, int nbLigne) {
    List<Coordinate> res = new ArrayList<Coordinate>(nbLigne);
    if (c.distance(cEnd) < MAXDIST) {
      res.add(c);
      return res;
    }
    if (nbLigne == 1) {
      c.x = (c.x + cEnd.x) / 2D;
      c.y = (c.y + cEnd.y) / 2D;
      res.add(c);
      return res;
    }
    int nbIteration = nbLigne - 1;
    double deltaX = (cEnd.x - c.x) / nbIteration;
    double deltaY = (cEnd.y - c.y) / nbIteration;
    res.add(c);
    for (int i = 1; i < nbIteration; i++) {
      Coordinate ci = new Coordinate(c.x + i * deltaX, c.y + i * deltaY);
      res.add(ci);
    }
    res.add(cEnd);
    return res;
  }

  private Coordinate getEndCoordinate() {
    return new Coordinate(getDoubleValue(x2_), getDoubleValue(y2_));
  }

  private Coordinate getFirsCoordinate() {
    return new Coordinate(getDoubleValue(x_), getDoubleValue(y_));
  }

  @Override
  public void actTask(final ProgressionInterface _prog, final CtuluAnalyzeGroup analyzeGroup, final String[] _messages) {
    CtuluAnalyze log = new CtuluAnalyze();
    log.setDesc(getTitre());
    
    saveContextParams();
    
    analyzeGroup.addAnalyzer(log);
    // on recupere les points
    List<Coordinate> points = getPoints();
    _prog.setDesc(TrResource.getS("Construction des points"));
    int idxRemove = 0;
    // on enleve les points en dehors
    for (Iterator<Coordinate> it = points.iterator(); it.hasNext();) {
      Coordinate c = it.next();
      int idx = EfIndexHelper.getElementEnglobant(source_.getGrid(), c.x, c.y, _prog);
      if (idx < 0) {
        idxRemove++;
        it.remove();
      }
    }
    // si pas de point pas de calcul
    if (points.size() == 0) {
      log.addFatalError(TrResource.getS("Aucun point de d�part n'appartient au maillage"));
      return;
    }
    if (idxRemove > 0) {
      String txt = null;
      if (idxRemove == 1) {
        txt = TrResource.getS("1 point de d�part a �t� ignor� car il en dehors du maillage");
      } else {
        txt = TrResource.getS("{0} points de d�part ont �t� ignor�s car ils en dehors du maillage", CtuluLibString.getString(idxRemove));
      }
      _messages[0] = txt;
    }

    Object[] selectedValues = listVar_.getSelectedValues();
    // creation de l algo
    _prog.setDesc(TrResource.getS("Calcul"));
    algoCalcul_ = new TrPostTrajectoireComputeAndDisplayActivity(calque_, layerAEcraser_);

    final TrPostFlecheContent flecheContent = getSelectedVariable();
    final EfTrajectoireParameters data = new EfTrajectoireParameters();
    data.segment_ = new ArrayList<Coordinate>();
    data.segment_.add(getFirsCoordinate());
    data.segment_.add(getEndCoordinate());
    data.nbPointsInitiaux_ = (Integer) integerEditor_.getValue(nbPoints_);

    data.vx = flecheContent.getVx();
    data.vy = flecheContent.getVy();
    data.dureeIntegration_ = getDuree();
    data.finesse_ = (Integer) integerEditor_.getValue(finesse_);
    data.firstTimeStepIdx_ = getSelectedTimeStep();
    data.firstTimeStep_ = source_.getTimeStep(data.firstTimeStepIdx_);
    data.isLigneDeCourant = isLigneCourant();
    if (!listVar_.isSelectionEmpty()) {

      data.varsASuivre_ = new ArrayList<CtuluVariable>(selectedValues.length);
      for (int i = 0; i < selectedValues.length; i++) {
        data.varsASuivre_.add((CtuluVariable) selectedValues[i]);
      }
    }

    data.points_ = points;
    if (isMarqueurAvailable()) {
      data.marqueur_ = new EfTrajectoireParametersMarqueur();
      data.marqueur_.timeStep_ = isMarqueurTimeStep();
      data.marqueur_.deltaMax_ = (Double) doubleMarkEditor_.getValue(markValue_);

    }
    algoCalcul_.computeLigneCourant(calque_, data, log, _prog, _messages);

    // -- mise a jour du tableau de res --//
    // modelResult_.fireTableDataChanged();

  }

  
  /**
   * Methode qui enregistre les param�tres saisies pour les recharger � la prochaine ouverture.
   * @author Adrien Hadoux
   */
  private void saveContextParams() {
	if(impl_ == null)
		return;
	  CommonProperties properties = impl_.getProject().getCommonProperties();
	  if(properties == null){
		  properties = new CommonProperties();
		  impl_.getProject().setCommonProperties(properties);
	  }
	  properties.trajectoireProperties = new CommonProperties.TrajectoireProperties();
	  CommonProperties.TrajectoireProperties prop = properties.trajectoireProperties;
	  prop.mode = choixMode_.getSelectedIndex();
	  prop.vecteur = cbVitesse_.getSelectedIndex();
	  prop.pdt = this.cbFirstTime_.getSelectedIndex();
	  prop.duree = ""+this.getDuree();
	  prop.precision =  integerEditor_.getStringValue(finesse_);
	  prop.p1x = doubleEditor_.getStringValue(x_);
	  prop.p1y = doubleEditor_.getStringValue(y_);
	  prop.p2x = doubleEditor_.getStringValue(x2_);
	  prop.p2y = doubleEditor_.getStringValue(y2_);
	  prop.nbTrajectoires = integerEditor_.getStringValue(nbPoints_);
	  prop.choixVariables = listVar_.getSelectedIndices();
	  prop.type = cbMarks_.getSelectedIndex();
	  prop.delta = doubleMarkEditor_.getStringValue(markValue_);
	  
  }
  /**
   * Methode qui enregistre les param�tres saisies pour les recharger � la prochaine ouverture.
   * @author Adrien Hadoux
   */
  private void loadContextParams() {
	  if(impl_ == null || impl_.getProject().getCommonProperties() == null || impl_.getProject().getCommonProperties().trajectoireProperties == null)
			return;
	  CommonProperties properties = impl_.getProject().getCommonProperties();
	  CommonProperties.TrajectoireProperties prop = properties.trajectoireProperties;
	  
	  if(prop.mode< choixMode_.getItemCount())
		  choixMode_.setSelectedIndex(prop.mode);
	  
	  if(prop.vecteur < cbVitesse_.getItemCount())
			cbVitesse_.setSelectedIndex(prop.vecteur);
	  
	  if(prop.pdt < cbFirstTime_.getItemCount())
		  cbFirstTime_.setSelectedIndex(prop.pdt);
	  
	  dureeEditor_.setValue(prop.duree,duree_);
	  integerEditor_.setValue(prop.precision, finesse_);
	  doubleEditor_.setValue( prop.p1x, x_);
	  doubleEditor_.setValue( prop.p1y, y_);
	  doubleEditor_.setValue( prop.p2x, x2_);
	  doubleEditor_.setValue( prop.p2y, y2_);
	  integerEditor_.setValue(prop.nbTrajectoires,nbPoints_);
	  try{
	  if(prop.choixVariables != null)
		  listVar_.setSelectedIndices(prop.choixVariables);
	  }catch(Exception e) {
		  System.err.print(e);
	  }
	  
	  if(prop.type< cbMarks_.getItemCount())
		  cbMarks_.setSelectedIndex(prop.type);
	  doubleMarkEditor_.setValue(prop.delta,markValue_);
	  
  }

JPanel content_;
  JComponent markValue_;
  JComboBox cbMarks_ = new JComboBox(new String[] { TrLib.getString("Aucun"), TrLib.getString("A pas de temps constant"),
      TrLib.getString("A distance constante") });

  public boolean isMarqueurAvailable() {
    return cbMarks_.getSelectedIndex() != 0;
  }

  public boolean isMarqueurTimeStep() {
    return cbMarks_.getSelectedIndex() == 1;
  }

  private JPanel buildMarqueur() {
    doubleMarkEditor_.setVal(BuValueValidator.MIN(1E-4));
    final JPanel conteneur = new JPanel(new BuGridLayout(2, 3, 3));
    conteneur.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Marqueurs")));
    conteneur.add(new JLabel(TrLib.getString("Type")));
    conteneur.add(cbMarks_);
    conteneur.add(new JLabel(TrLib.getString("Delta (valeur absolue)")));
    markValue_ = doubleMarkEditor_.createEditorComponent();
    conteneur.add(markValue_);
    markValue_.setEnabled(false);
    cbMarks_.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(ItemEvent _e) {
        markValue_.setEnabled(isMarqueurAvailable());

      }
    });

    return conteneur;
  }

  public boolean isSegmentValid() {
    final boolean segValid = !doubleEditor_.isEmpty(x_) && !doubleEditor_.isEmpty(x2_) && !doubleEditor_.isEmpty(y2_) && !doubleEditor_.isEmpty(y_);
    return segValid && integerEditor_.getValue(nbPoints_) != null;
  }

  public double getDoubleValue(final JComponent c) {
    return ((Double) doubleEditor_.getValue(c)).doubleValue();
  }

  protected void updateView(final boolean _zoom) {
    if (isSegmentValid()) {
      lineChooser_.update(
          GISGeometryFactory.INSTANCE.createSegment(getDoubleValue(x_), getDoubleValue(y_), getDoubleValue(x2_), getDoubleValue(y2_)), _zoom);
    }
  }

  private JPanel buildSegments() {

    final JPanel conteneur = new JPanel(new BuGridLayout(3, 5, 5, false, false, false, false, false));
    conteneur.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Choix du segment")));
    conteneur.add(new JLabel(""));
    conteneur.add(new JLabel("x:"));
    conteneur.add(new JLabel("y:"));
    conteneur.add(new JLabel("Point 1:"));
    conteneur.add(x_);
    conteneur.add(y_);
    conteneur.add(new JLabel("Point 2:"));
    conteneur.add(x2_);
    conteneur.add(y2_);
    conteneur.add(new JLabel(TrLib.getString("Nombre de trajectoire/lignes de courant")));
    conteneur.add(nbPoints_);
    final LineString initSelected = lineChooser_.getInitSelected();
    if (initSelected != null) {
      final int nbCoordinate = initSelected.getNumPoints();
      doubleEditor_.setValue(initSelected.getCoordinateSequence().getX(0), x_);
      doubleEditor_.setValue(initSelected.getCoordinateSequence().getY(0), y_);
      doubleEditor_.setValue(initSelected.getCoordinateSequence().getX(nbCoordinate - 1), x2_);
      doubleEditor_.setValue(initSelected.getCoordinateSequence().getY(nbCoordinate - 1), y2_);
      updateView(false);
    }
    JPanel mainSegment = new JPanel(new BuVerticalLayout(5, false, true));
    mainSegment.add(conteneur);
    JPanel pn = new JPanel(new BuGridLayout(3, 2, 0));
    JButton bt = new JButton(TrLib.getString("zoomer sur le segment"));
    bt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        updateView(true);

      }
    });
    pn.add(bt);
    bt = new JButton(TrLib.getString("Zoom initial"));
    bt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        lineChooser_.zoomInitial();

      }
    });
    pn.add(bt);
    bt = new JButton(EbliLib.getS("Restaurer"));
    bt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        lineChooser_.restaurer();

      }
    });
    pn.add(bt);
    mainSegment.add(pn);
    return mainSegment;
  }

  private JPanel buildVariables() {
    final JPanel conteneur = new JPanel(new BorderLayout());
    listVar_ = new JList(source_.getNewVarListModel());
    listVar_.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    conteneur.add(new JScrollPane(listVar_));
    conteneur.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Choix des variables")));
    return conteneur;
  }

  @Override
  public void decoreBtApply(final JButton _bt) {
    _bt.setText(TrLib.getString("Calculer"));
  }

  @Override
  public int getNbMessageMax() {
    return 1;
  }

  @Override
  public JComponent getPanel() {
    if (content_ != null)
      return content_;
    content_ = new BuPanel(new BuVerticalLayout(5));
    content_.add(buildMainProperties());

    // -- panel des segements --//
    content_.add(buildSegments());
    // -- panel des variables --//
    content_.add(buildVariables());
    content_.add(buildMarqueur());
    
    loadContextParams();
    
    return content_;
  }

  private JPanel buildMainProperties() {
    JPanel pn = new JPanel(new BuGridLayout(2, 5, 5));
    pn.add(new JLabel(TrResource.getS("Mode")));
    pn.add(choixMode_);
    pn.add(new JLabel(TrResource.getS("Vecteur")));
    ListModel flecheListModel = source_.getNewFlecheListModel();
    CtuluComboBoxModelAdapter cb = new CtuluComboBoxModelAdapter(flecheListModel);
    cbVitesse_ = new BuComboBox(cb);
    cb.setSelectedItem(flecheListModel.getElementAt(0));
    // on s�lectionne la vitesse par defaut
    if (flecheListModel.getSize() > 1) {
      for (int i = flecheListModel.getSize() - 1; i >= 0; i--) {
        TrPostFlecheContent flecheContent = (TrPostFlecheContent) flecheListModel.getElementAt(i);
        if (flecheContent.getVar() == H2dVariableType.VITESSE) {
          cb.setSelectedItem(flecheListModel.getElementAt(i));
          break;
        }
      }
    }
    pn.add(cbVitesse_);
    pn.add(new JLabel(TrResource.getS("Pas de temps intial")));
    cbFirstTime_ = new BuComboBox(new CtuluComboBoxModelAdapter(source_.getNewTimeListModel()));
    cbFirstTime_.setSelectedIndex(0);
    pn.add(cbFirstTime_);
    pn.add(new JLabel(TrResource.getS("Dur�e d'int�gration:")));
    pn.add(duree_);
    pn.add(new JLabel(TrResource.getS("Pr�cision du trac�:")));
    pn.add(finesse_);
    return pn;
  }

  /**
   * retourne l indice du pas de temps.
   * 
   * @return
   */
  protected int getSelectedTimeStep() {
    return cbFirstTime_.getSelectedIndex();
  }

  /**
   * retourne la variable selectionnee
   * 
   * @return
   */
  protected TrPostFlecheContent getSelectedVariable() {

    return (TrPostFlecheContent) cbVitesse_.getSelectedItem();
  }

  @Override
  public String getTitre() {
    return TrResource.getS("Lignes de courants") + "/" + TrResource.getS("Trajectoires");
  }

  protected boolean isLigneCourant() {
    return choixMode_.getSelectedIndex() == 0;
  }

  protected boolean isTrajectoire() {
    return choixMode_.getSelectedIndex() == 1;
  }

  @Override
  public String isTakDataValid() {
    if (!this.integerEditor_.isValueValidFromComponent(finesse_)) {
      return TrResource.getS("La finesse doit �tre d�fini par un entier sup�rieur � 1");
    }
    if (!isSegmentValid()) {
      return TrResource.getS("Le segment n'est pas d�fini");
    }
    if (integerEditor_.getValue(nbPoints_) == null) {
      return TrResource.getS("Pr�ciser le nombre de trajectoires");
    }
    if (dureeEditor_.isEmpty(duree_)) {
      return TrResource.getS("La dur�e doit �tre d�finie par un r�el en secondes");
    }
    if (isMarqueurAvailable() && !doubleMarkEditor_.isValueValidFromComponent(markValue_)) {
      return TrResource.getS("Le delta du marqueur doit �tre renseign�");
    }
    // on doit verifier que la duree est bien comprise dans les pas de temps du projet
    if (isTrajectoire()) {
      final int duree = getDuree();
      final double timeSelected = source_.getTimeStep(cbFirstTime_.getSelectedIndex());
      final double timeEnd = timeSelected + duree;
      final double lastTimeStep = source_.getTimeStep(source_.getNbTimeStep() - 1);
      if (timeEnd < source_.getTimeStep(0) || timeEnd > lastTimeStep) {
        final String min = "-".concat(source_.getTimeFormatter().format(timeSelected - source_.getTimeStep(0)));
        final String max = source_.getTimeFormatter().format(lastTimeStep - timeSelected);
        return TrResource.getS("la dur�e du calcul doit �tre comprise entre {0} et {1}", min, max);
      }

    }
    if (getFirsCoordinate().distance(getEndCoordinate()) < MAXDIST) {
      boolean res = calque_.getCtuluUI().question(TrResource.getS("Calculer une seule trajectoire"),
          TrResource.getS("Les extremit�s du segment sont confondues. Une seule trajectoire sera calcul�e.\nVoulez-vous continuer ?"));
      if (!res) {
        return TrResource.getS("Choisir 2 points diff�rents");
      }

    }

    return null;
  }

  private Integer getDuree() {
    return ((Integer) dureeEditor_.getValue(duree_));
  }

  // EfLigneDeCourantActivity activity_;

  @Override
  public void stopTask() {
    if (algoCalcul_ != null)
      algoCalcul_.stop();

  }

  @Override
  public void dialogClosed() {
    lineChooser_.close();

  }

public TrPostVisuPanel getCalque_() {
	return calque_;
}

public void setCalque_(TrPostVisuPanel calque_) {
	this.calque_ = calque_;
}

}
