package org.fudaa.fudaa.tr.post;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.courbe.EGActionReplayDataCourbe;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheDuplicator;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.fudaa.tr.post.persist.TrPostReloadParameter;
import org.fudaa.fudaa.tr.post.profile.MVProfileCourbeModel;
import org.fudaa.fudaa.tr.post.profile.MvProfileCourbeGroup;
import org.fudaa.fudaa.tr.post.profile.MvProfileTreeModel;

/**
 * Tree Model des formats scope
 * 
 * @author genesis
 */
@SuppressWarnings("serial")
public class TrPostScopCourbeTreeModel extends TrPostCourbeTreeModel {

  @Override
  public void importCourbes(final EvolutionReguliereInterface[] _crb, final CtuluCommandManager _mng, final ProgressionInterface _prog) {
    if (_crb == null)
      return;
    final EGGroup gr = getGroupFor(H2dVariableType.SANS);
    final List<EGCourbeChild> childs = new ArrayList<EGCourbeChild>(_crb.length);
    for (int i = 0; i < _crb.length; i++) {

      EvolutionReguliere evol = null;
      if (_crb[i] instanceof EvolutionReguliere)
        evol = (EvolutionReguliere) _crb[i];
      else evol = new EvolutionReguliere(_crb[i]);
      final EGCourbeChild child = createCourbe(gr, evol);
      child.setAspectContour(child.getLigneModel().getCouleur());
      childs.add(child);

      //-- ajout des titres et des unit�s --//
      if (evol.titreAxeX_ != null)
        child.getAxeX().setTitre(evol.titreAxeX_);
      if (evol.titreAxeY_ != null)
        child.getAxeY().setTitre(evol.titreAxeY_);
      if (evol.uniteAxeX_ != null)
        child.getAxeX().setUnite(evol.uniteAxeX_);
      if (evol.uniteAxeY_ != null)
        child.getAxeY().setUnite(evol.uniteAxeY_);

      //-- on met a jour si la courbe est un nuage de points ou non --//
      child.setNuagePoints(_crb[i].isNuagePoints());

      gr.addEGComponent(child);
    }

    if (_mng != null) {
      _mng.addCmd(new CommandAddCourbesMulti(childs.toArray(new EGCourbeChild[childs.size()]), new EGGroup[] { gr }));
    }
    fireStructureChanged();

  }

  private EGCourbeChild createCourbe(final EGGroup gr, EvolutionReguliere evol) {
    return TrPostScopeCourbe.createCourbe(gr, evol);
  }

  @Override
  public Object getSpecificPersitDatas(Map Params) {
    TrPostScopeCourbeTreeModelPersist dataPersist = new TrPostScopeCourbeTreeModelPersist();
    dataPersist.fillDataWithModel(this);
    return dataPersist;
  }

  @Override
  public void setSpecificPersitDatas(Object specPersitData, Map infos) {
    // TODO Auto-generated method stub
    TrPostScopeCourbeTreeModelPersist data = (TrPostScopeCourbeTreeModelPersist) specPersitData;

    // -- remplissage du model avec les data persistantes --//
    data.fillModelWith(this);

    // -- recuperation du trpostprojet specifique au chargement --//
    TrPostProjet projet = (TrPostProjet) infos.get(TrPostReloadParameter.POST_PROJET);
    this.projet_ = projet;
    this.impl_ = projet.impl_;
    CtuluNumberFormatI durationFormatter = data.getDurationFormatter();
    if (durationFormatter != null) {
      updateAxeXFormatter(durationFormatter);
    } else {
      EGCourbe[] courbes = getCourbes();
      if (!CtuluLibArray.isEmpty(courbes)) {
        EGCourbe egCourbe = courbes[courbes.length - 1];
        EGModel egModel = egCourbe.getModel();
        if (egModel instanceof TrPostCourbeModel) {
          TrPostCourbeModel model = ((TrPostCourbeModel) egModel);
          updateAxeXFormatter(model.getSource().getTimeFormatter());
        }
      }
    }
  }

  /**
   * Methode qui permet de fusionner le model courant avec un autre model SPATIAL. Utilsier poru la fusion de courbes
   * spatiales et l ajout dans d autres courbes.
   * 
   * @author Adrien Hadoux
   * @param anotherModel
   */
  public void mergeWithAnotherMvProfileTreeModel(final MvProfileTreeModel anotherModel) {
    // -- parcours de la liste des variables du graphe a fusionner --//
    for (int i = 0; i < anotherModel.target_.getVars().length; i++) {
      final H2dVariableType var = (H2dVariableType) anotherModel.target_.getVars()[i];

      // -- on recupere toutes les courbes associees a la var pour le
      // graphe merges
      final MvProfileCourbeGroup g = anotherModel.getGroup(var, false);
      if (g != null) {
        // -- on recherche le group associe a la variale dans l autre graphe
        // sinon on le cree --//
        final EGGroup group = this.getGroupFor(var);
        //--  ahx - Resolve item 1.1.3 - il faut dupliquer le range des y pour avoir un graphe similaire. --//
        group.setAxeY(g.getAxeY().duplicate());
        for (int k = g.getChildCount() - 1; k >= 0; k--) {
          if (g.getCourbeAt(k).getModel() instanceof MVProfileCourbeModel) {
            if (((MVProfileCourbeModel) g.getCourbeAt(k).getModel()).getVariable() == var) {

              // -- duplication de la courbe dans le groupe --//

              group.addEGComponent((EGCourbeChild) g.getCourbeAt(k).duplicate(group, new EGGrapheDuplicator()));

            }
          }

        }
      }

    }
    this.fireStructureChanged();

  }

  @Override
  public void finalizePersistance() {
    for (int i = 0; i < this.getNbEGObject(); i++) {

      if (this.getEGObject(i) instanceof EGGroup) {

        EGGroup groupe = (EGGroup) this.getEGObject(i);
        for (int k = 0; k < groupe.getChildCount(); k++) {
          if (groupe.getCourbeAt(k) != null) {
            EGCourbeChild courbe = groupe.getCourbeAt(k);
            if (courbe.getModel() != null && (courbe.getModel() instanceof TrPostScopeCourbeModel)) {

              varGroup_.put(H2dVariableType.SANS, groupe);
            }
          }
        }
      }
    }
  }

  @Override
  public EGGrapheModel duplicate(final EGGrapheDuplicator _duplicator) {
    final TrPostScopCourbeTreeModel duplic = new TrPostScopCourbeTreeModel();
    duplic.setAxeX(this.getAxeX().duplicate());

    duplic.getSelectionModel().setSelectionMode(this.getSelectionModel().getSelectionMode());

    final GrapheTreeNode root = this.getGrapheTreeNode();
    //		    final GrapheTreeNode rootDuplique = duplic.getGrapheTreeNode();

    for (int i = 0; i < root.components_.size(); i++) {

      // -- ajout du groupe duplique --//
      duplic.add(root.getGroup(i).duplicate(_duplicator));

    }

    return duplic;
  }

  @Override
  public String getSelectedSpecificCourbeInfos() {
    return null;
  }

  /**
   * Les actions des courbes specifiques: l'ajout de variable.
   */
  @Override
  public List<EbliActionInterface> getSpecificsActionsCurvesOnly(EGGraphe _target, CtuluUI ui) {
    List<EbliActionInterface> listeActions = new ArrayList<EbliActionInterface>();

    //-- action replay data --//
    listeActions.add(new EGActionReplayDataCourbe.CourbeOnly(this, this.getSelectedComponent().getModel(), ui));
    return listeActions;

  }

}
