/*
 * @creation 12 sept. 06
 * @modification $Date: 2007-01-19 13:14:11 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuInformationsDocument;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.tr.common.TrGrapheTreeTimeFille;

import javax.swing.*;

public class TrPostRubarEdgeTreeFille extends TrGrapheTreeTimeFille {
  private final TrPostRubarEdgeNuaAction exportAction;

  /**
   * @param _g
   * @param _titre
   * @param _appli
   * @param _id
   */
  public TrPostRubarEdgeTreeFille(final EGGraphe _g, final String _titre, final FudaaCommonImplementation _appli,
                                  final BuInformationsDocument _id, TrPostRubarEdgeNuaAction exportAction) {
    super(_g, _titre, _appli, _id);
    setName("ifPostFilleEdge");
    this.exportAction = exportAction;
    this.p_.setPersonnalAction(new EbliActionInterface[]{exportAction});
  }

  @Override
  protected String getMenuTitle() {
    return H2dResource.getS("Ar�tes");
  }

  @Override
  public String getShortHtmlHelp() {
    return TrPostRubarEdgeFille.getHelpForAreteFrame(impl_, getTitle());
  }

  @Override
  protected void fillSpecificMenu(JMenu _m) {
    super.fillSpecificMenu(_m);
    if (exportAction != null) {
      _m.add(exportAction);
    }
  }
}
