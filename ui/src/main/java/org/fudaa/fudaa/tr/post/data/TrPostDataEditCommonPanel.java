/*
 *  @creation     9 ao�t 2005
 *  @modification $Date: 2006-12-20 16:13:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post.data;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import com.memoire.fu.FuComparator;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeCreated;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostUserVariableSaver;
import org.nfunk.jep.Variable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Permet l'edition des variables creees par l'utilisateur.
 *
 * @author Fred Deniger
 * @version $Id: TrPostDataEditCommonPanel.java,v 1.4 2006-12-20 16:13:19 deniger Exp $
 */
@SuppressWarnings("serial")
public abstract class TrPostDataEditCommonPanel extends CtuluDialogPanel {
  class NameDocumentListener implements DocumentListener {
    protected void update() {
      updateNomState();
    }

    @Override
    public void changedUpdate(final DocumentEvent _e) {
      update();
    }

    @Override
    public void insertUpdate(final DocumentEvent _e) {
      update();
    }

    @Override
    public void removeUpdate(final DocumentEvent _e) {
      update();
    }
  }

  class ShortNameDocumentListener implements DocumentListener {
    protected void updateShortName() {
      updateShortNameState();
    }

    @Override
    public void changedUpdate(final DocumentEvent _e) {
      updateShortName();
    }

    @Override
    public void insertUpdate(final DocumentEvent _e) {
      updateShortName();
    }

    @Override
    public void removeUpdate(final DocumentEvent _e) {
      updateShortName();
    }
  }

  H2dVariableTypeCreated var_;
  final Set forbiddenName_;
  final Set forbiddenShortName_;
  final TrPostSource src_;
  final private BuTextField tfName_;
  final private BuTextField tfShortName_;
  final Set varToAdd_;

  /**
   * @param _src la source
   * @param _var la variable a modifier: si null, phase de creation.
   */
  public TrPostDataEditCommonPanel(final TrPostSource _src, final H2dVariableTypeCreated _var) {
    super(true);
    src_ = _src;
    var_ = _var;
    final H2dVariableType[] vs = _src.getAllVariablesNonVec();
    // les noms interdits: ceux deja utilises.
    forbiddenName_ = new HashSet(vs.length);
    forbiddenShortName_ = new HashSet(vs.length);
    // les variables pouvant etre utilis�e : on enleve la variable et les variables dependantes de
    // cette derniere. Les variables utilisables sont ajoute � l'expression
    varToAdd_ = new TreeSet(FuComparator.STRING_IGNORE_CASE_COMPARATOR);
    varToAdd_.addAll(Arrays.asList(vs));
    // true si la variable est utilisee
    boolean varIsUsed = false;
    if (_var != null) {
      varToAdd_.remove(_var);
    }
    TrPostUserVariableSaver.fillWithForbidden(varToAdd_, forbiddenName_, forbiddenShortName_);
    final Variable[] constant = _src.getConstantVar();
    if (constant != null) {
      for (int i = constant.length - 1; i >= 0; i--) {
        forbiddenShortName_.add(constant[i].getName());
      }
    }
    if (_var != null) {
      varIsUsed = TrPostDataHelper.removeUserVarDepending(varToAdd_, _var, _src);
    }

    setLayout(new BuGridLayout(2, 5, 5));
    tfName_ = new BuTextField();
    tfShortName_ = new BuTextField();
    if (_var != null) {
      tfName_.setText(_var.getName());
      tfShortName_.setText(_var.getShortName());
      if (varIsUsed) {
        tfName_.setEditable(false);
        tfShortName_.setEditable(false);
        final String txt = CtuluLib.getS("Ce champ n'est pas �ditable, car la variable est utilis�e");
        tfName_.setToolTipText(txt);
        tfShortName_.setToolTipText(txt);
      }
    }
    JLabel lb = addLabel(CtuluLib.getS("Nom de la variable"));
    lb.setLabelFor(tfName_);
    add(tfName_);
    lb = addLabel(CtuluLib.getS("Nom court de la variable"));
    lb.setToolTipText(CtuluLib.getS("L'identifiant utilis� dans les expressions"));
    lb.setLabelFor(tfShortName_);
    add(tfShortName_);
    tfName_.getDocument().addDocumentListener(new NameDocumentListener());
    tfShortName_.getDocument().addDocumentListener(new ShortNameDocumentListener());
    updateNomState();
    updateShortNameState();
  }

  protected void updateNomState() {
    final boolean valide = isNameOk();
    tfName_.setForeground(valide ? CtuluLibSwing.getDefaultTextFieldForegroundColor() : Color.RED);
    final JComponent c = (JComponent) tfName_.getClientProperty("labeledBy");
    if (c != null) {
      c.setForeground(valide ? CtuluLibSwing.getDefaultLabelForegroundColor() : Color.RED);
    }
    if (tfName_.getText() == null || tfName_.getText().trim().length() == 0) {
      tfName_.setToolTipText(CtuluLib.getS("Le nom ne doit pas �tre vide"));
    } else if (!valide) {
      tfName_.setToolTipText(TrResource.getS("Ce nom est d�j� utilis�"));
    } else {
      tfName_.setToolTipText(c == null ? null : c.getToolTipText());
    }
  }

  protected void updateShortNameState() {
    final boolean valide = isShortNameOk();
    tfShortName_.setForeground(valide ? CtuluLibSwing.getDefaultTextFieldForegroundColor() : Color.RED);
    final JComponent c = (JComponent) tfShortName_.getClientProperty("labeledBy");
    if (c != null) {
      c.setForeground(valide ? CtuluLibSwing.getDefaultLabelForegroundColor() : Color.RED);
    }
    if (valide) {
      tfShortName_.setToolTipText(c == null ? null : c.getToolTipText());
    } else {
      tfShortName_.setToolTipText(CtuluLib.getS("Ce nom court est d�j� utilis�"));
    }
  }

  H2dVariableTypeCreated getNewVar() {
    if (!isVarModified()) {
      return var_;
    }
    String shortName = tfShortName_.getText();
    if (shortName != null) {
      shortName = shortName.trim();
      if (shortName.length() == 0) {
        shortName = null;
      }
    }
    return new H2dVariableTypeCreated(tfName_.getText(), shortName, null);
  }

  @Override
  public boolean apply() {
    return true;
  }

  /**
   * @return true si des modifications ont ete faites.
   */
  public abstract boolean isModified();

  public boolean isNameOk() {
    if (tfName_.isEditable()) {
      String txt = tfName_.getText();
      if (txt == null) {
        return false;
      }
      txt = txt.trim();
      return txt.length() > 0 && !forbiddenName_.contains(txt);
    }
    return true;
  }

  public boolean isShortNameOk() {
    if (tfShortName_.isEditable()) {
      String txt = tfShortName_.getText();
      if (txt == null) {
        return false;
      }
      txt = txt.trim();
      return txt.length() > 0 && !forbiddenShortName_.contains(txt);
    }
    return true;
  }

  public boolean isVarModified() {
    if (var_ == null) {
      return true;
    }
    return (!var_.getName().equals(tfName_.getText())) || (!var_.getShortName().equals(tfShortName_.getText()));
  }

  public boolean addError(final StringBuffer _b) {
    return false;
  }

  public abstract TrPostDataCreated createData();

  @Override
  public final boolean isDataValid() {
    if (!isModified()) {
      return true;
    }
    final StringBuffer buf = new StringBuffer();
    boolean r = true;
    if (!isNameOk()) {
      r = false;
      buf.append(CtuluLib.getS("Ce nom est d�j� utilis�"));
    }
    if (!isShortNameOk()) {
      r = false;
      if (buf.length() > 0) {
        buf.append(CtuluLibString.LINE_SEP);
      }
      buf.append(CtuluLib.getS("Ce nom court est d�j� utilis�"));
    }
    if (addError(buf)) {
      r = false;
    }
    if (!r) {
      setErrorText(buf.toString());
    }
    return r;
  }
}
