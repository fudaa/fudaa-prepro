/**
 * @creation 29 nov. 2004
 * @modification $Date: 2007-02-15 17:10:40 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuTable;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBcManager;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSourceMng;
import org.fudaa.dodico.h2d.telemac.H2dTelemacTracerMng;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModelGeometryListener;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.model.MvNodeModel;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacSourceModel.java,v 1.16 2007-02-15 17:10:40 deniger Exp $
 */
public class TrTelemacSourceModel implements MvNodeModel, ZModeleGeometry {

  class SiphonTable extends AbstractTableModel {

    @Override
    public int getColumnCount() {
      int r = 4;
      r += 2;
      r += mng_.getNbTraceurs();
      return r;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return TrResource.getS("Index");
      }
      if (_column == 1) {
        return "X";
      }
      if (_column == 2) {
        return "Y";
      }
      if (_column == 3) {
        return H2dResource.getS("D�bit");
      }
      if (_column == 4) {
        return H2dResource.getS("Vitesse selon X");
      }
      if (_column == 5) {
        return H2dResource.getS("Vitesse selon Y");
      }
      if (_column >= 6) {
        return mng_.getTraceurName(_column - 6);
      }
      return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public int getRowCount() {
      return mng_.getNbSource();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _column) {
      if (_column == 0) {
        return CtuluLibString.getString(_rowIndex + 1);
      }
      final H2dTelemacSource s = mng_.getSource(_rowIndex);
      if (_column == 1) {
        return Double.toString(s.getX());
      }
      if (_column == 2) {
        return Double.toString(s.getY());
      }
      if (_column == 3) {
        return Double.toString(s.getQ());
      }
      if (_column == 4) {
        return Double.toString(s.getVx());
      }
      if (_column == 5) {
        return Double.toString(s.getVy());
      }
      if (_column >= 6) {
        int traceurIdx = _column - 6;
        double[] t = s.getTracers();
        if (traceurIdx < t.length) {
          return Double.toString(t[traceurIdx]);
        }
      }
      return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return false;
    }
  }

  protected H2dTelemacSourceMng mng_;
  private final H2dTelemacBcManager bcManager;

  final EfGridInterface grid_;

  /**
   * @param _mng
   */
  public TrTelemacSourceModel(final H2dTelemacSourceMng _mng, H2dTelemacBcManager bcManager, final EfGridInterface _grid) {
    super();
    mng_ = _mng;
    this.bcManager = bcManager;
    grid_ = _grid;
  }

  protected final H2dTelemacSourceMng getMng() {
    return mng_;
  }
  
  protected final H2dTelemacTracerMng getTracerManager(){
    return mng_.getTracerManager();
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    final BuTable t = new CtuluTable();
    EbliTableInfoPanel.setTitle(t, TrResource.getS("Sources"));
    t.setModel(new SiphonTable());
    return t;
  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  @Override
  public boolean point(GrPoint _pt, int _idxGeom, int _pointIdx) {
    return false;
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
    fillWithInfo(_d, _layer.getLayerSelection());
  }

  public void fillWithInfo(final InfoData _m, final CtuluListSelectionInterface _l) {
    final int nb = CtuluLibArray.getSelectedIdxNb(_l);
    _m.put(TrResource.getS("Nombre de sources d�finies"), CtuluLibString.getString(mng_.getNbSource()));
    _m.put(TrResource.getS("Nombre de sources s�lectionn�es"), CtuluLibString.getString(nb));
    if (nb == 1) {
      final int i = _l.getMaxIndex();
      _m.setTitle(TrResource.getS("Source") + CtuluLibString.ESPACE + CtuluLibString.getString(i + 1));
      final H2dTelemacSource s = mng_.getSource(i);
      _m.put(TrResource.getS("X"), Double.toString(s.getX()));
      _m.put(TrResource.getS("Y"), Double.toString(s.getY()));
      _m.put(H2dResource.getS("D�bit"), Double.toString(s.getQ()));
      if (mng_.isVitesseDefined()) {
        _m.put(H2dResource.getS("Vitesse selon X"), Double.toString(s.getVx()));
        _m.put(H2dResource.getS("Vitesse selon Y"), Double.toString(s.getVy()));
      }
      int nbTraceur = mng_.getNbTraceurs();
      for (int traceurIdx = 0; traceurIdx < nbTraceur; traceurIdx++) {
        String value = Double.toString(s.getTracers()[traceurIdx]);
        _m.put(H2dResource.getS("Traceur") + CtuluLibString.ESPACE + mng_.getTraceurName(traceurIdx), value);
      }
    } else {
      _m.setTitle(TrResource.getS("Sources"));
    }

  }

  @Override
  public boolean getDataRange(final CtuluRange _b) {
    return false;
  }

  @Override
  public GrBoite getDomaine() {
    if (mng_.getNbSource() == 0) {
      return null;
    }
    H2dTelemacSource s = mng_.getSource(0);
    double xmax = s.getX();
    double ymax = s.getY();
    double xmin = xmax;
    double ymin = ymax;
    double x, y;
    for (int i = mng_.getNbSource() - 1; i > 0; i--) {
      s = mng_.getSource(i);
      x = s.getX();
      y = s.getY();
      if (x > xmax) {
        xmax = x;
      } else if (x < xmin) {
        xmin = x;
      }
      if (y > ymax) {
        ymax = y;
      } else if (y < ymin) {
        ymin = y;
      }
    }
    return new GrBoite(new GrPoint(xmin, ymin, 0), new GrPoint(xmax, ymax, 0));
  }

  // on retourne le maillage
  @Override
  public EfGridInterface getGrid() {
    return grid_;
  }

  @Override
  public int getNombre() {
    return mng_.getNbSource();
  }

  @Override
  public Object getObject(final int _ind) {
    return null;
  }

  @Override
  public int getNbPointForGeometry(int _idxGeom) {
    return 1;
  }

  @Override
  public boolean isGeometryFermee(int _idxGeom) {
    return false;
  }

  @Override
  public boolean isGeometryReliee(int _idxGeom) {
    return false;
  }

  @Override
  public double getX(final int _i) {
    return mng_.getSource(_i).getX();
  }

  @Override
  public double getY(final int _i) {
    return mng_.getSource(_i).getY();
  }

  @Override
  public double getZ(final int _i) {
    return 0;
  }

  @Override
  public void getDomaineForGeometry(int _idxGeom, GrBoite _target) {
    if (!isGeometryVisible(_idxGeom))
      return;
    _target.o_.x_ = getX(_idxGeom);
    _target.o_.y_ = getY(_idxGeom);
    _target.e_.x_ = _target.o_.x_;
    _target.e_.y_ = _target.o_.y_;
  }

  public boolean isValuesTableAvailableFromModel() {
    return true;
  }

  @Override
  public void objectAction(Object _source, int _indexObj, Object _obj, int _action) {

  }

  @Override
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {

  }

  @Override
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexObj, Object _newValue) {

  }

  @Override
  public void prepareExport() {

  }

  @Override
  public boolean isGeometryVisible(int _idxGeom) {
    return true;
  }

  @Override
  public void addModelListener(ZModelGeometryListener _listener) {

  }

  @Override
  public void removeModelListener(ZModelGeometryListener _listener) {

  }

  @Override
  public GISZoneCollection getGeomData() {
    GISZoneCollectionPoint collectionPts = new GISZoneCollectionPoint();
    GISPoint[] pts = new GISPoint[getNombre()];
    for (int i = 0; i < pts.length; i++) {
      pts[i] = new GISPoint(getX(i), getY(i), getZ(i));
    }
    collectionPts.addAll(pts, null, null);

    return collectionPts;
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return null;
  }

  @Override
  public boolean point(final GrPoint _p, final int _i, final boolean _force) {
    _p.setCoordonnees(getX(_i), getY(_i), 0);
    return true;
  }

  @Override
  public void setGrid(final EfGrid _g) {
  }

  @Override
  public GrPoint getVertexForObject(int ind, int idVertex) {
    return new GrPoint(getX(ind), getY(ind), getZ(ind));
  }

  public H2dTelemacBcManager getBcManager() {
    return bcManager;
  }
}