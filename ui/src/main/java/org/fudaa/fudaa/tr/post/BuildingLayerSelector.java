/**
 * 
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuVerticalLayout;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.fudaa.sig.wizard.FSigWizardImportHelper.CalqueCellRender;
import org.fudaa.fudaa.tr.common.TrLib;

/**
 * @author CANEL Christophe (Genesis)
 */
public class BuildingLayerSelector extends CtuluDialogPanel {
  //  private static class ComboRenderer extends CtuluCellTextRenderer {
  //    @Override
  //    protected void setValue(final Object _value) {
  //      if (_value instanceof ZCalqueAffichageDonneesInterface) {
  //        super.setText(((ZCalqueAffichageDonneesInterface) _value).getTitle());
  //      } else {
  //        super.setValue(_value);
  //      }
  //    }
  //  }

  private JList jlist;
  private JCheckBox cbUseFrontierLayer;

  public BuildingLayerSelector(List<ZCalqueLigneBrisee> calques, boolean hasBoundaryLayer) {
    setLayout(new BuVerticalLayout(5));

    if (hasBoundaryLayer) {
      cbUseFrontierLayer = new JCheckBox();
      cbUseFrontierLayer.setText(TrLib.getString("Utiliser le calque des frontières pour afficher les batiments"));
      add(cbUseFrontierLayer);
    }
    if (!CtuluLibArray.isEmpty(calques)) {
      this.add(new JLabel(TrLib.getString("Sélectionner des calques SIG pour afficher les batiments")));
      jlist = new JList(calques.toArray());
      jlist.setCellRenderer(new CalqueCellRender());
      jlist.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
      this.add(new JScrollPane(jlist));
    }
  }

  public boolean useBoundaryLayer() {
    return cbUseFrontierLayer != null && cbUseFrontierLayer.isSelected();
  }

  public ZCalqueLigneBrisee[] getSelectedCalque() {
    if (jlist == null) {
      return null;
    }
    Object[] selectedValues = jlist.getSelectedValues();
    if (selectedValues == null) {
      return null;
    }
    ZCalqueLigneBrisee[] res = new ZCalqueLigneBrisee[selectedValues.length];
    for (int i = 0; i < res.length; i++) {
      res[i] = (ZCalqueLigneBrisee) selectedValues[i];

    }

    return res;
  }
}
