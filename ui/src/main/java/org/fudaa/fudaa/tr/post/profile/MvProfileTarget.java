/*
 * @creation 23 nov. 06
 * @modification $Date: 2007-06-05 09:01:15 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.profile;

import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;

/**
 * Une interface donnant tout ce qui est n�cessaire � la g�n�ral de profil spatial.
 * 
 * @author fred deniger
 * @version $Id: MvProfileTarget.java,v 1.4 2007-06-05 09:01:15 deniger Exp $
 */
public interface MvProfileTarget {

  /**
   * @return les donn�es
   */
  EfGridData getData();

  /**
   * @return les variables disponibles:non null ...
   */
  CtuluVariable[] getVars();

  EfGridDataInterpolator getInterpolator();

  /**
   * @return les pas de temps: peut-etre null
   */
  FudaaCourbeTimeListModel getTimeModel();

  void profilPanelCreated(MvProfileFillePanel _panel, ProgressionInterface _prog, String _title);

}
