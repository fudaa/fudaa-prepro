/*
 *  @creation     3 mai 2005
 *  @modification $Date: 2007-04-30 14:22:38 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurAbstract;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Fred Deniger
 * @version $Id: TrPostFilterSelecteur.java,v 1.5 2007-04-30 14:22:38 deniger Exp $
 */
public class TrPostFilterSelecteur extends BSelecteurAbstract implements ActionListener {

  public static String getDefaultProperty() {
    return "FILTER";
  }

  JButton btModifyDefault_;
  // BuLabel lbVars_;
  JButton btModifyDepth_;
  JButton btModifyVar_;
  BSelecteurCheckBox cbStrict_;
  // BuCheckBox cbStrict_;
  // BuCheckBox cbValidHauteur_;
  BSelecteurCheckBox cbValidHauteur_;

  /**
   */
  public TrPostFilterSelecteur() {
    super(getDefaultProperty());
    super.setAddListenerToTarget(false);

    cbValidHauteur_ = new BSelecteurCheckBox(TrPostFilterConfigure.KEY_DEPTH_ACTIVE);
    cbValidHauteur_.setTitle(TrResource.getS("Hauteur d'eau minimale"));
    cbValidHauteur_.setTooltip(TrResource.getS("Activer le filtre sur la hauteur d'eau"));
    cbValidHauteur_.getCb().setActionCommand("CHANGE_COND");
    cbValidHauteur_.getCb().addActionListener(this);
    btModifyDepth_ = new BuButton();
    btModifyDepth_.setText(BuResource.BU.getString("Modifier"));
    btModifyDepth_.setIcon(FudaaResource.FUDAA.getIcon("modifier"));
    btModifyDepth_.setToolTipText(TrResource.getS("Modifier la hauteur d'eau minimale"));
    btModifyDepth_.setActionCommand("MODIFY_DEPTH");
    btModifyDepth_.addActionListener(this);
    btModifyVar_ = new BuButton(BuResource.BU.getString("Modifier"));
    btModifyVar_.setIcon(FudaaResource.FUDAA.getIcon("modifier"));
    btModifyVar_.setActionCommand("MODIFY_VAR");
    btModifyVar_.addActionListener(this);
    btModifyDefault_ = new BuButton(TrLib.getString("Modifier les valeurs par d�faut"));
    btModifyDefault_.setHorizontalAlignment(SwingConstants.CENTER);
    btModifyDefault_.setIcon(BuResource.BU.getIcon("preference"));
    btModifyDefault_.setToolTipText(btModifyDefault_.getText());
    btModifyDefault_.setActionCommand("MODIFY_DEF");
    btModifyDefault_.addActionListener(this);
    cbStrict_ = new BSelecteurCheckBox(TrPostFilterConfigure.KEY_STRICT);
    cbStrict_.setTitle(TrResource.getS("Filtre strict"));
    cbStrict_.setTooltip(TrResource
        .getS("Si activ�, un �l�ment n'est dessin� que si tous ses sommets sont accept�s par les filtres"));
    // cbStrict_.getCb().setText(cbStrict_.getTitle());
    // lbVars_ = new BuLabel(TrResource.getS("Variables non affect�s"));
    updateChangeStateDepth();
    updateTooltipForVar();
  }

  private TrPostFilterHauteur getDepthFilter() {
    return target_ == null ? null : (TrPostFilterHauteur) target_.getProperty(TrPostFilterConfigure.KEY_DEPTH_VALUE);
  }

  private double getHauteurEps() {
    if (super.target_ == null) { return 0; }
    final TrPostFilterHauteur h = getDepthFilter();
    return h == null ? 0 : h.eps_;
  }

  private void updateChangeStateDepth() {
    btModifyDepth_.setEnabled(cbValidHauteur_.getCb().isSelected());
    updateTooltipForDepth();
  }

  final void updateTooltipForDepth() {
    btModifyDepth_.setToolTipText("<html>" + TrResource.getS("Modifier la hauteur d'eau minimale") + "<br>"
        + TrResource.getS("Valeur minimale pour la hauteur d'eau:") + CtuluLibString.ESPACE + getHauteurEps());
  }

  final void updateTooltipForVar() {
    final StringBuffer buf = new StringBuffer(100);
    buf.append("<html>").append(
        TrResource.getS("Les variables s�lectionn�es ne seront pas affect�es par les filtres d'affichage:"));
    TrPostFilterLayer.fillWithHauteurAvoidVar(buf, target_ == null ? null : (H2dVariableType[]) target_
        .getProperty(TrPostFilterConfigure.KEY_AVOID_VAR));
    buf.append("</html>");
    btModifyVar_.setToolTipText(buf.toString());

  }

  @Override
  protected void updateTarget(final BSelecteurTargetInterface _target) {
    super.updateTarget(_target);
    cbStrict_.setSelecteurTarget(_target);
    cbValidHauteur_.setSelecteurTarget(_target);
    updateChangeStateDepth();
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (target_ == null) { return; }
    if ("CHANGE_COND".equals(_e.getActionCommand())) {
      updateChangeStateDepth();
    }
    if (btModifyDepth_ == _e.getSource()) {
      final TrPostFilterHauteurPanel panel = new TrPostFilterHauteurPanel((TrPostSource) target_
          .getProperty(TrPostFilterConfigure.KEY_SRC));
      panel.updateFor(getDepthFilter());
      final CtuluDialogPanel pn = new CtuluDialogPanel(false) {

        @Override
        public boolean apply() {
          TrPostFilterSelecteur.this.firePropertyChange(TrPostFilterConfigure.KEY_DEPTH_VALUE, panel.getCondition());
          return true;
        }

        @Override
        public boolean isDataValid() {
          return panel.isDataValid();
        }
      };
      pn.setLayout(new BuBorderLayout());
      pn.add(panel.buildPanel(), BuBorderLayout.CENTER);
      pn.afficheModale(btModifyDefault_, TrResource.getS("Modifier la hauteur d'eau minimale"));
    } else if (btModifyVar_ == _e.getSource()) {
      final TrPostFilterVarPanel panel = new TrPostFilterVarPanel((TrPostSource) target_
          .getProperty(TrPostFilterConfigure.KEY_SRC));
      panel.updateFor((H2dVariableType[]) target_.getProperty(TrPostFilterConfigure.KEY_AVOID_VAR));
      final CtuluDialogPanel pn = new CtuluDialogPanel(false) {

        @Override
        public boolean apply() {
          TrPostFilterSelecteur.this.firePropertyChange(TrPostFilterConfigure.KEY_AVOID_VAR, panel.getVar());
          TrPostFilterSelecteur.this.updateTooltipForVar();
          return true;
        }
      };
      pn.setLayout(new BuBorderLayout());
      pn.add(panel.buildPanel(), BuBorderLayout.CENTER);
      pn.afficheModale(btModifyDefault_, TrResource.getS("Variables non modifi�es:"));

    } else if (btModifyDefault_ == _e.getSource()) {
      final TrPostFilterDefaultValuePanel panel = new TrPostFilterDefaultValuePanel((TrPostSource) target_
          .getProperty(TrPostFilterConfigure.KEY_SRC));
      final CtuluDialogPanel pn = new CtuluDialogPanel(false) {

        @Override
        public boolean apply() {
          panel.apply();
          return true;
        }

        @Override
        public boolean isDataValid() {
          return panel.isDataValid();
        }
      };
      pn.setLayout(new BuBorderLayout());
      pn.add(panel.buildPanel(), BuBorderLayout.CENTER);
      pn.afficheModale(btModifyDefault_, FudaaLib.getS("Valeurs par d�fault"));
    }
  }

  BuPanel pnMain_;

  /**
   * @return le panneau d'affichage
   */
  public void buildPanel() {
    pnMain_ = new BuPanel(new BuBorderLayout(0, 10));
    final BuPanel pnFilter = new BuPanel();

    pnFilter.setLayout(new BuGridLayout(2, 4, 4));
    BuLabel lb = null;
    lb = new BuLabel(cbValidHauteur_.getTitle());
    lb.setToolTipText(cbValidHauteur_.getTooltip());
    pnFilter.add(lb);
    final BuPanel lgHauteur = new BuPanel(new BuGridLayout(2, 2, 2));
    lgHauteur.add(cbValidHauteur_.getCb());
    lgHauteur.add(btModifyDepth_);
    pnFilter.add(lgHauteur);
    if (cbStrict_ != null) {
      lb = new BuLabel(cbStrict_.getTitle());
      lb.setToolTipText(cbStrict_.getTooltip());
      pnFilter.add(lb);
      pnFilter.add(cbStrict_.getCb());
    }
    lb = new BuLabel(TrResource.getS("Variables non modifi�es"));
    lb.setToolTipText(TrResource.getS("Variables non modifi�es par les fitres"));
    pnFilter.add(lb);
    pnFilter.add(btModifyVar_);
    pnMain_.add(new JLabel(TrResource.getS("Ce filtre est �galement appliqu� sur la variable {0}","|\u0394H|")), BorderLayout.NORTH);
    pnMain_.add(pnFilter);
    pnMain_.add(btModifyDefault_, BuBorderLayout.SOUTH);
  }

  public JComponent getComponent() {
    if (pnMain_ == null) {
      buildPanel();
    }
    return pnMain_;
  }

  @Override
  public JComponent[] getComponents() {
    return BSelecteurAbstract.createComponents(getComponent());
  }

  @Override
  public String getTitle() {
    return EbliLib.getS("Filtres d'affichage");
  }

  @Override
  public String getTooltip() {
    return null;
  }

  @Override
  public boolean needAllWidth() {
    return true;
  }

  @Override
  public void reupdateFromTarget() {}

  @Override
  public void updateFromTarget() {
    cbStrict_.updateFromTarget();
    cbValidHauteur_.updateFromTarget();
    btModifyDepth_.setEnabled(cbValidHauteur_.getCb().isSelected());
    updateTooltipForVar();
    updateChangeStateDepth();
    updateTooltipForDepth();

  }

}
