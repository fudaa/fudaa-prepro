package org.fudaa.fudaa.tr.post;

import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.io.File;
import java.util.Observable;
import java.util.Observer;
import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.CtuluLibString;
import org.jdesktop.swingx.JXList;

/**
 * chooser qui demande a l user de choisir la simulation a prendre pour la selection.
 * 
 * @author Adrien Hadoux
 */
public class TrPostProjetChooser extends BuInternalFrame implements ListSelectionListener, Observer {

  /**
   * projet contenant la liste des sources.
   */
  TrPostProjet projet_;

  /**
   * La liste des projet afffichage graphique.
   */
  JXList listProjets_;

  JScrollPane listScroller_;

  /**
   * Classe Model de la jlist
   */
  class ModelProjets extends AbstractListModel implements /* ListModel, */ListCellRenderer {

    TrPostProjet p;
    BuLabel labelTexte = new BuLabel("");
    Color background;
    Color foreground;

    public ModelProjets(final TrPostProjet _p) {
      p = _p;
      labelTexte.setOpaque(true);
    }

    @Override
    public Object getElementAt(final int index) {
      return p.getSource(index);
    }

    @Override
    public int getSize() {
      return p.getSources().getSrcSize();
    }

    // TODO utiliser CtuluCellTextRenderer
    @Override
    public Component getListCellRendererComponent(final JList list, final Object value, final int index,
        final boolean isSelected, final boolean cellHasFocus) {
      final Border border = null;

      // -- recuperation du simple nom du fichier --//
      String nomFichier = ((TrPostSource) value).getMainFile().getName();
      int position = nomFichier.lastIndexOf(File.separator) + 1;
      if (position != -1) {
        nomFichier = nomFichier.substring(position);
      }
      position = nomFichier.lastIndexOf(CtuluLibString.DOT);
      if (position != -1) {
        nomFichier = nomFichier.substring(0, position);
      }

      // --suppression des trop long espaces du titre --//
      String title = ((TrPostSource) value).getTitle();
      title = title.replaceAll("  ", "");

      // -- on ajoute un indicateur pour le projet selectionné --//
      // if ((TrPostSource) value != projet_.src_)
      labelTexte.setText(title + " | Fichier: " + nomFichier);
      // else {
      // labelTexte.setText("-> " + title + " | Fichier: " + nomFichier);
      // // labelTexte.setForeground(Color.YELLOW);
      // }

      labelTexte.setBackground(list.getBackground());
      labelTexte.setForeground(list.getForeground());
      labelTexte.setOpaque(true);
      if (isSelected) {
        labelTexte.setBackground(list.getSelectionBackground());
        labelTexte.setForeground(list.getSelectionForeground());
      }

      return labelTexte;
    }

  }

  ModelProjets modelList_;

  public TrPostProjetChooser(final TrPostProjet _projet) {
    super();

    // -- recuperation du projet --//
    projet_ = _projet;

    // -- ajout comme observer --//
    projet_.getObservable().addObserver(this);

    // --creation de la liste graphique des projets --//
    modelList_ = new ModelProjets(projet_);
    listProjets_ = new JXList(modelList_);
    listProjets_.setCellRenderer(modelList_);

    listProjets_.setBorder(BorderFactory.createEtchedBorder());

    // -- ajoute un selecteur --//
    // listProjets_.addListSelectionListener(this);

    // -- oblige la selection unique pour ne pas ajouter qu un seul projet a la
    // fois--//
    listProjets_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    // -- selectionne par default le src courant --//
    // listProjets_.setSelectedValue(projet_.src_, true);

    // --creation graphique --//

    // setSize(500, 250);
    // setPreferredSize(new Dimension(500, 250));
    setLayout(new BorderLayout());
    listScroller_ = new JScrollPane(listProjets_);
    this.add(listScroller_, BorderLayout.CENTER);
  }

  @Override
  public void valueChanged(final ListSelectionEvent e) {

  // -- recuperation de l element selectionné --//

  }

  @Override
  public void update(final Observable o, final Object arg) {

    // mise a jour de la liste
    listProjets_.revalidate();

  }

}
