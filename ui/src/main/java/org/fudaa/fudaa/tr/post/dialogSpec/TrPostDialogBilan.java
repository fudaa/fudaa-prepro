package org.fudaa.fudaa.tr.post.dialogSpec;

import com.memoire.bu.*;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import org.apache.commons.lang.StringUtils;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gui.CtuluComboBoxModelAdapter;
import org.fudaa.ctulu.gui.CtuluDecimalFormatEditPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.cubature.EfBilanHelper;
import org.fudaa.dodico.ef.cubature.EfDataIntegrale;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsCorrectionActivity;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;
import org.fudaa.dodico.ef.operation.EfLineIntersectorActivity;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGObject;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostProjet;
import org.fudaa.fudaa.tr.post.TrPostSource;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.post.data.TrPostDataVecteur;
import org.fudaa.fudaa.tr.post.persist.CommonProperties;
import org.fudaa.fudaa.tr.post.profile.MVProfileCourbeModel;
import org.fudaa.fudaa.tr.post.profile.MvProfileCoteTester;
import org.fudaa.fudaa.tr.post.profile.MvProfileCourbe;
import org.fudaa.fudaa.tr.post.profile.MvProfileTreeModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Wizard pour gerer les bilans des courbes.
 *
 * @author Adrien Hadoux
 */
public class TrPostDialogBilan implements AbstractTrPostDialogAddToSceneDialogContent {
  TrPostSource source_;
  TrPostVisuPanel visuPanel;
  MvProfileTreeModel modelGraphe_;
  TrPostCommonImplementation impl_;
  // donnees graphiques
  private JPanel panelSeuil_;
  BuTextField seuil_ = new BuTextField(5);
  // variables et pdt
  private JPanel panelVariables_;
  CtuluComboBoxModelAdapter modelVariables_;
  JComboBox listVar_;
  CtuluComboBoxModelAdapter modelPdt_;
  JComboBox listPdt_;
  JRadioButton cbVectoriel;
  JRadioButton cbScalar;
  // isolignes
  private JPanel panelIsolignes_;
  JCheckBox boxSelectLineAuto_ = new JCheckBox(TrResource.getS("Choisir l'isoligne du calque"));
  private final JCheckBox boxSelectLineManu_ = new JCheckBox(TrResource.getS("Cr�er manuellement lisoligne"));
  BuTextField textX1 = new BuTextField(10);
  BuTextField textX2 = new BuTextField(10);
  BuTextField textY1 = new BuTextField(10);
  BuTextField textY2 = new BuTextField(10);
  // results
  JPanel panelResults_;
  JLabel results_;
  JButton ajouter_ = new JButton(TrLib.getString("Calculer"), EbliResource.EBLI.getIcon("crystal_valider"));
  JButton btChangeFmt_ = new JButton(TrLib.getString("Changer le formattage des nombres"));
  JTextField tfRes_;
  JTextField tfResMin_;
  JTextField tfResMax_;
  CtuluNumberFormatI currentFmt_ = CtuluNumberFormatDefault.DEFAULT_FMT;
  double res_;
  double resMax_;
  double resMin_;
  private EbliScene scene;

  private void updateResValues() {
    tfRes_.setText(currentFmt_.format(res_));
    tfResMax_.setText(currentFmt_.format(resMax_));
    tfResMin_.setText("-" + currentFmt_.format(resMin_));
  }

  /**
   * Constructeur reserv� au calques
   */
  public TrPostDialogBilan(final TrPostVisuPanel _calque, final TrPostCommonImplementation impl) {
    impl_ = impl;
    visuPanel = _calque;
    modelGraphe_ = null;
    source_ = _calque.getSource();
    modelVariables_ = new CtuluComboBoxModelAdapter(source_.getNewVarListModel());
    modelPdt_ = new CtuluComboBoxModelAdapter(source_.getNewTimeListModel());
  }

  /**
   * constructeur reserv� aux graphes spatiaux
   */
  public TrPostDialogBilan(EbliScene scene, final MvProfileTreeModel _modelGraphe, final TrPostCommonImplementation impl) {
    visuPanel = null;
    impl_ = impl;
    this.scene = scene;
    modelGraphe_ = _modelGraphe;
    source_ = (TrPostSource) _modelGraphe.target_.getData();
    modelVariables_ = new CtuluComboBoxModelAdapter(source_.getNewVarListModel());
    modelPdt_ = new CtuluComboBoxModelAdapter(source_.getNewTimeListModel());
    // -- construction de la dialog --//
    buildDialog();
  }

  /**
   * Methode qui enregistre les param�tres saisies pour les recharger � la prochaine ouverture.
   *
   * @author Adrien Hadoux
   */
  private void saveContextParams() {
    if (impl_ == null) {
      return;
    }
    CommonProperties properties = impl_.getProject().getCommonProperties();
    if (properties == null) {
      properties = new CommonProperties();
      impl_.getProject().setCommonProperties(properties);
    }

    properties.bilanProperties = new CommonProperties.BilanProperties();
    CommonProperties.BilanProperties p = properties.bilanProperties;

    p.createLine = this.boxSelectLineManu_.isSelected();
    p.selectedLine = this.boxSelectLineAuto_.isSelected();
    p.p1x = textX1.getText();
    p.p1y = textY1.getText();
    p.p2x = textX2.getText();
    p.p2y = textY2.getText();
    p.variable = this.listVar_.getSelectedIndex();
    p.pdt = this.listPdt_.getSelectedIndex();
    p.seuil = seuil_.getText();
    p.calculVectoriel = this.cbVectoriel.isSelected();
    p.createCurve1 = createGraphe.getCbResult().isSelected();
    p.createCurve2 = createGraphe.getCbPositive().isSelected();
    p.createCurve3 = createGraphe.getCbNegative().isSelected();
    p.pdtDepart = createGraphe.getStartTime().getSelectedIndex();
    p.pdtFin = createGraphe.getEndTime().getSelectedIndex();
    p.grapheCible = createGraphe.getTargetGraph().getSelectedIndex();
  }

  /**
   * Methode qui enregistre les param�tres saisies pour les recharger � la prochaine ouverture.
   *
   * @author Adrien Hadoux
   */
  private void loadContextParams() {
    if (impl_ == null || impl_.getProject().getCommonProperties() == null || impl_.getProject().getCommonProperties().cubatureProperties == null) {
      return;
    }
    CommonProperties properties = impl_.getProject().getCommonProperties();
    CommonProperties.BilanProperties p = properties.bilanProperties;

    this.boxSelectLineManu_.setSelected(p.createLine);
    this.boxSelectLineAuto_.setSelected(p.selectedLine);

    textX1.setText(p.p1x);
    textY1.setText(p.p1y);
    textX2.setText(p.p2x);
    textY2.setText(p.p2y);

    if (p.variable < listVar_.getItemCount()) {
      listVar_.setSelectedIndex(p.variable);
    }

    if (p.pdt < listPdt_.getItemCount()) {
      listPdt_.setSelectedIndex(p.pdt);
    }

    this.seuil_.setText(p.seuil);
    this.cbVectoriel.setSelected(p.calculVectoriel);
    createGraphe.getCbResult().setSelected(p.createCurve1);
    createGraphe.getCbPositive().setSelected(p.createCurve2);
    createGraphe.getCbNegative().setSelected(p.createCurve3);

    if (p.pdtDepart < createGraphe.getStartTime().getItemCount()) {
      createGraphe.getStartTime().setSelectedIndex(p.pdtDepart);
    }
    if (p.pdtFin < createGraphe.getEndTime().getItemCount()) {
      createGraphe.getEndTime().setSelectedIndex(p.pdtFin);
    }
    if (p.grapheCible < createGraphe.getTargetGraph().getItemCount()) {
      createGraphe.getTargetGraph().setSelectedIndex(p.grapheCible);
    }

    //-- on v�rifie si une isoligne a �t� saisie par l'user et on remplit les champs le cas �ch�ant --//
    feedSelectedLine();
  }

  /**
   * Construit le panel des variables
   */
  private JPanel buildIsoLignes() {
    final JPanel content = new JPanel(new BorderLayout());
    final JPanel panelChek = new JPanel(new FlowLayout(FlowLayout.CENTER));
    String s = null;
    if (visuPanel != null) {
      s = TrResource.getS("Choisir la ligne calque");
    } else {
      s = TrResource.getS("Choisir la ligne de la courbe s�lectionn�e");
    }
    panelChek.add(new JLabel(s));
    panelChek.add(boxSelectLineAuto_);
    panelChek.add(new JLabel(TrResource.getS("Cr�er la ligne")));
    panelChek.add(boxSelectLineManu_);
    content.add(panelChek, BorderLayout.NORTH);

    final JPanel center = new JPanel(new GridLayout(2, 1));
    final JPanel center1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
    center1.add(new JLabel("Point 1 X:"));
    center1.add(textX1);
    center1.add(new JLabel("Y:"));
    center1.add(textY1);
    final JPanel center2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
    center2.add(new JLabel("Point 2 X:"));
    center2.add(textX2);
    center2.add(new JLabel("Y:"));
    center2.add(textY2);
    center.add(center1);
    center.add(center2);
    content.add(center, BorderLayout.CENTER);
    content.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Choix de la ligne")));

    // -- init et gestion des evenements --//
    textX1.setCharValidator(BuCharValidator.FLOAT);
    textX1.setStringValidator(BuStringValidator.FLOAT);
    textX1.setValueValidator(BuValueValidator.FLOAT);

    textX2.setCharValidator(BuCharValidator.FLOAT);
    textX2.setStringValidator(BuStringValidator.FLOAT);
    textX2.setValueValidator(BuValueValidator.FLOAT);

    textY1.setCharValidator(BuCharValidator.FLOAT);
    textY1.setStringValidator(BuStringValidator.FLOAT);
    textY1.setValueValidator(BuValueValidator.FLOAT);

    textY2.setCharValidator(BuCharValidator.FLOAT);
    textY2.setStringValidator(BuStringValidator.FLOAT);
    textY2.setValueValidator(BuValueValidator.FLOAT);

    boxSelectLineAuto_.setAction(new AbstractAction() {
      @Override
      public void actionPerformed(final ActionEvent actionEvent) {
        final boolean enabled = false;
        textX1.setEnabled(enabled);
        textX2.setEnabled(enabled);
        textY1.setEnabled(enabled);
        textY2.setEnabled(enabled);

        // -- remplissage des coordonn�es par les points --//
        LineString res = getSelectedLine();
        if (res != null && res.getNumPoints() > 0) {
          Coordinate coor = res.getCoordinateN(0);
          Coordinate coor2 = res.getCoordinateN(res.getNumPoints() - 1);
          textX1.setText("" + coor.x);
          textX2.setText("" + coor2.x);
          textY1.setText("" + coor.y);
          textY2.setText("" + coor2.y);
        }

        boxSelectLineManu_.setSelected(false);
        if (boxSelectLineAuto_.isSelected()) {
          ajouter_.setEnabled(true);
        } else {
          ajouter_.setEnabled(false);
        }
      }
    });
    boxSelectLineManu_.setAction(new AbstractAction() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        final boolean enabled = true;
        textX1.setEnabled(enabled);
        textX2.setEnabled(enabled);
        textY1.setEnabled(enabled);
        textY2.setEnabled(enabled);
        boxSelectLineAuto_.setSelected(false);
        if (boxSelectLineManu_.isSelected()) {
          ajouter_.setEnabled(true);
        } else {
          ajouter_.setEnabled(false);
        }
      }
    });
    feedSelectedLine();

    return content;
  }

  public void feedSelectedLine() {

    LineString selectedLineOfCalque = getSelectedLine();
    // -- test si une ligne a ete au prealable selectionnee par l user sur le
    // calque --//
    if (selectedLineOfCalque != null) {
      boxSelectLineAuto_.setSelected(true);
      final boolean enabled = false;
      textX1.setEnabled(enabled);
      textX2.setEnabled(enabled);
      textY1.setEnabled(enabled);
      textY2.setEnabled(enabled);
      boxSelectLineManu_.setSelected(false);

      // -- remplissage des coordonn�es par les points --//
      if (selectedLineOfCalque != null && selectedLineOfCalque.getNumPoints() > 0) {
        Coordinate coor = selectedLineOfCalque.getCoordinateN(0);
        Coordinate coor2 = selectedLineOfCalque.getCoordinateN(selectedLineOfCalque.getNumPoints() - 1);
        textX1.setText("" + coor.x);
        textX2.setText("" + coor2.x);
        textY1.setText("" + coor.y);
        textY2.setText("" + coor2.y);
      }
    } else {
      boxSelectLineAuto_.setSelected(false);
      boxSelectLineManu_.setSelected(true);
      boxSelectLineAuto_.setEnabled(false);
    }
  }

  /**
   * Methode qui genere une lineString en entrant 2 points.
   */
  LineString createLineString() {
    final double x1 = Double.parseDouble(textX1.getText());
    final double y1 = Double.parseDouble(textY1.getText());
    final double x2 = Double.parseDouble(textX2.getText());
    final double y2 = Double.parseDouble(textY2.getText());

    final Coordinate[] cs = new Coordinate[2];
    cs[0] = new Coordinate(x1, y1);
    cs[1] = new Coordinate(x2, y2);

    return GISGeometryFactory.INSTANCE.createLineString(cs);
  }

  private JPanel buildresults() {
    final JPanel conteneur = new JPanel(new BorderLayout());

    final JPanel content = new JPanel(new FlowLayout(FlowLayout.CENTER));

    results_ = new JLabel("");
    content.add(ajouter_);
    // content.add(new JLabel(TrResource.getS("R�sultat:")));
    content.add(results_);

    conteneur.setBorder(BorderFactory.createTitledBorder(TrResource.getS("R�sultats")));

    // -- action relatives aux resultats --//
    ajouter_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        if (controleDataOk()) {
          computeResults();
        }
      }
    });
    conteneur.add(content, BorderLayout.NORTH);
    JPanel res = new BuPanel(new BuGridLayout(2, 5, 5));

    tfRes_ = new BuTextField();
    tfRes_.setEditable(false);
    tfRes_.setHorizontalAlignment(JTextField.RIGHT);
    tfResMax_ = new BuTextField();
    tfResMax_.setEditable(false);
    tfResMax_.setHorizontalAlignment(JTextField.RIGHT);
    tfResMin_ = new BuTextField();
    tfResMin_.setEditable(false);
    tfResMin_.setHorizontalAlignment(JTextField.RIGHT);
    res.add(new BuLabel(TrLib.getString("R�sultat")));
    res.add(tfRes_);
    res.add(new BuLabel(TrLib.getString("Positif")));
    res.add(tfResMax_);
    res.add(new BuLabel(TrLib.getString("N�gatif")));
    res.add(tfResMin_);
    res.add(new BuLabel(""));
    res.add(btChangeFmt_);
    btChangeFmt_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent _e) {
        CtuluNumberFormatI newFmt = CtuluDecimalFormatEditPanel.chooseNumberFormat(currentFmt_);
        if (newFmt != currentFmt_) {
          currentFmt_ = newFmt;
          if (!CtuluLibString.isEmpty(tfRes_.getText())) {
            updateResValues();
          }
        }
      }
    });
    conteneur.add(res, BorderLayout.CENTER);
    return conteneur;
  }

  protected void reformat(JTextField tf) {
    tf.setText(currentFmt_.format(Double.parseDouble(tf.getText())));
  }

  public boolean controleDataOk() {
    if (boxSelectLineManu_.isSelected()) {
      if (textX1.getText().equals("")) {
        impl_.error(TrResource.getS("Point non saisi correctement"));
        return false;
      }
      if (textX2.getText().equals("")) {
        impl_.error(TrResource.getS("Point non saisi correctement"));
        return false;
      }
      if (textY1.getText().equals("")) {
        impl_.error(TrResource.getS("Point non saisi correctement"));
        return false;
      }
      if (textY2.getText().equals("")) {
        impl_.error(TrResource.getS("Point non saisi correctement"));
        return false;
      }
    }

    if (seuil_.getText().equals("")) {
      impl_.error(TrResource.getS("Le seuil doit �tre d�fini par un r�el"));
      return false;
    }
    if (listVar_.getSelectedIndex() == -1) {
      impl_.error(TrResource.getS("Il faut choisir une variable"));
      return false;
    }
    if (listPdt_.getSelectedIndex() == -1) {
      impl_.error(TrResource.getS("Il faut choisir un pas de temps"));
      return false;
    }
    return true;
  }

  /**
   * Retourne la ligne selectionnee dans le calque.
   */
  protected LineString getSelectedLine() {
    if (this.modelGraphe_ != null) {
      EGObject selectedObject = modelGraphe_.getSelectedObject();
      if (selectedObject == null) {
        List<EGCourbeChild> allCourbesChild = modelGraphe_.getAllCourbesChild();
        if (allCourbesChild.size() == 1) {
          selectedObject = allCourbesChild.get(0);
        }
      }
      if (selectedObject instanceof MvProfileCourbe) {
        MvProfileCourbe profile = (MvProfileCourbe) selectedObject;
        if (profile.getProfileModel() instanceof MVProfileCourbeModel) {
          return ((MVProfileCourbeModel) profile.getProfileModel()).getInitLine();
        }
      }
    }
    return visuPanel == null ? null : visuPanel.getSelectedLine();
  }

  /**
   * retourne la variable selectionnee
   */
  @Override
  public H2dVariableType getSelectedVariable() {
    return (H2dVariableType) modelVariables_.getElementAt(listVar_.getSelectedIndex());
  }

  /**
   * retourne l indice du pas de temps.
   */
  protected int getSelectedTimeStep() {
    return listPdt_.getSelectedIndex();
  }

  @Override
  public EbliScene getScene() {
    if (scene != null) {
      return scene;
    }
    return getVisuPanel().getPostImpl().getCurrentLayoutFille().getScene();
  }

  @Override
  public JTextField getTxtSeuil() {
    return seuil_;
  }

  @Override
  public AbstractListModel getModelVariables() {
    return modelVariables_;
  }

  @Override
  public TrPostSource getSource() {
    return source_;
  }

  @Override
  public TrPostProjet getProject() {
    return impl_.getCurrentProject();
  }

  @Override
  public TrPostVisuPanel getVisuPanel() {
    return visuPanel;
  }

  @Override
  public double getSeuil() {

    try {
      return StringUtils.isBlank(seuil_.getText()) ? 0 : Double.parseDouble(seuil_.getText());
    } catch (NumberFormatException e) {
      System.err.print(e);
      return 0;
    }
  }

  private JPanel buildVariables() {
    final JPanel content = new JPanel(new GridLayout(3, 2));
    listVar_ = new BuComboBox(modelVariables_);
    listVar_.setSelectedIndex(0);
    listPdt_ = new BuComboBox(modelPdt_);
    listPdt_.setSelectedIndex(0);

    content.add(new JLabel(TrLib.getString("Variable")));
    content.add(listVar_);
    content.add(new JLabel(TrLib.getString("Pas de temps")));
    content.add(listPdt_);

    content.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Choix de la variable et du pas de temps")));
    return content;
  }

  private void updateCbVectorielState() {
    H2dVariableType selectedItem = (H2dVariableType) listVar_.getSelectedItem();
    if (selectedItem == null) {
      cbVectoriel.setEnabled(false);
      cbScalar.setEnabled(false);
    } else {
      cbScalar.setEnabled(true);
      final boolean isVector = source_.getFlecheContent(selectedItem) != null;
      cbVectoriel.setEnabled(isVector);
      if (!isVector && cbVectoriel.isSelected()) {
        cbScalar.setSelected(true);
      }
    }
  }

  private JPanel buildSeuil() {
    cbVectoriel = new JRadioButton();
    cbScalar = new JRadioButton();
    cbScalar.setSelected(true);
    seuil_.setCharValidator(BuCharValidator.FLOAT);
    seuil_.setStringValidator(BuStringValidator.FLOAT);
    seuil_.setValueValidator(BuValueValidator.FLOAT);
    seuil_.setText(CtuluLibString.ZERO);

    final JPanel content = new JPanel(new GridLayout(3, 2));
    content.add(new JLabel(TrLib.getString("Seuil")));
    content.add(seuil_);
    content.add(new JLabel(TrLib.getString("Calcul vectoriel")));
    content.add(cbVectoriel);
    content.add(new JLabel(TrLib.getString("Calcul scalaire")));
    content.add(cbScalar);
    ButtonGroup bg = new ButtonGroup();
    bg.add(cbScalar);
    bg.add(cbVectoriel);
    updateCbVectorielState();
    listVar_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateCbVectorielState();
      }
    });
    content.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Calcul")));

    return content;
  }

  /**
   * Creation du panel graphique.
   */
  public Box buildContent() {

    final Box content = Box.createVerticalBox();

    // -- panel des isolignes --//
    if (panelIsolignes_ == null) {
      panelIsolignes_ = buildIsoLignes();
    }
    content.add(panelIsolignes_);

    // -- panel des variables --//
    if (panelVariables_ == null) {
      panelVariables_ = buildVariables();
    }
    content.add(panelVariables_);

    // -- panelSeuil
    if (panelSeuil_ == null) {
      panelSeuil_ = buildSeuil();
    }
    content.add(panelSeuil_);

    // -- panel des resultats --//
    if (panelResults_ == null) {
      panelResults_ = buildresults();
    }
    content.add(panelResults_);

    createGraphe = new TrPostDialogBilanAddToScene(this);
    JPanel createPanel = createGraphe.createPanel();
    createPanel.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Construire courbes")));
    content.add(createPanel);

    loadContextParams();

    return content;
  }

  TrPostDialogBilanAddToScene createGraphe;

  public void buildDialog() {

    final Frame f = CtuluLibSwing.getFrameAncestorHelper(impl_.getCurrentInternalFrame());
    JDialog dialog_ = new JDialog(f);
    dialog_.setModal(true);

    dialog_.setTitle(getTitle());
    dialog_.setContentPane(buildContent());
    dialog_.pack();
    dialog_.setLocationRelativeTo(CtuluLibSwing.getFrameAncestor(impl_.getCurrentInternalFrame()));

    dialog_.setVisible(true);
  }

  public String getTitle() {
    return TrResource.getS("Bilans");
  }

  /**
   * Methode qui realise les calculs d'int�grale par rapport aux donn�es
   */
  private void computeResults() {

    new CtuluTaskOperationGUI(impl_, FudaaLib.getS("Calcul Bilan")) {
      @Override
      public void act() {
        final ProgressionInterface prog = getStateReceiver();
        // -- calcul de la EfLineIntersectionsResultsMng --//
        impl_.setMainMessage(FudaaLib.getS("Calcul des intersections"));
        saveContextParams();
        impl_.setMainProgression(10);
        final H2dVariableType selectedVariable = getSelectedVariable();
        final EfLineIntersectorActivity lineIntersect = new EfLineIntersectorActivity(getSource());
        final LineString lineString = boxSelectLineAuto_.isSelected() ? getSelectedLine()
            : createLineString();

        EfLineIntersectionsResultsI interfaceRes = (source_.isElementVar(selectedVariable)) ? lineIntersect.computeForMeshes(lineString, prog).getDefaultRes() : lineIntersect
            .computeForNodes(lineString, prog).getDefaultRes();

        // le builder permet de corriger les erreurs concernant les cote d'eau / bathymetrie
        EfLineIntersectionsCorrectionActivity act = new EfLineIntersectionsCorrectionActivity();
        interfaceRes = act.correct(interfaceRes, new MvProfileCoteTester(), getSelectedTimeStep(), prog);
        // -- calcul des efdata --//
        impl_.setMainProgression(30);
        impl_.setMainMessage(CtuluLib.getS("Calcul des donn�es pour les pas de temps et variables r�cup�r�s"));

        final EfData data = source_.getData(selectedVariable, getSelectedTimeStep());

        // -- calcul de l'int�grale --//
        impl_.setMainProgression(70);
        impl_.setMainMessage(CtuluLib.getS("Calcul de l'int�grale"));
        EfDataIntegrale resCalcul = null;
        if (isVectorCalculation()) {
          TrPostDataVecteur vectData = (TrPostDataVecteur) data;
          resCalcul = EfBilanHelper.integrerMethodeTrapezeVector(interfaceRes, vectData.getVxData(), vectData.getVyData(), getSeuil());
        } else {
          resCalcul = EfBilanHelper.integrerMethodeTrapeze(interfaceRes, data, getSeuil());
        }

        res_ = resCalcul.getResultat();
        resMax_ = resCalcul.getZonePlus();
        resMin_ = resCalcul.getZoneMoins();
        updateResValues();
        impl_.unsetMainMessage();
        impl_.unsetMainProgression();
      }

      protected TrPostSource getSource() {
        return source_;
      }
    }.start();
  }

  public boolean isVectorCalculation() {
    return cbVectoriel.isEnabled() && cbVectoriel.isSelected();
  }
}
