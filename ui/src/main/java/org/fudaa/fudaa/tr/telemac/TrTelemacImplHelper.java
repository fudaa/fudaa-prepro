/**
 * @creation 29 avr. 2003
 * @modification $Date: 2007-04-30 14:22:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuMenuItem;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResult;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluAnalyzeGUI;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.dodico.calcul.CalculExecBatch;
import org.fudaa.dodico.dico.DicoModelAbstract;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.olb.exec.OLBExec;
import org.fudaa.dodico.telemac.Telemac2dFileFormat;
import org.fudaa.dodico.telemac.TelemacDicoFileFormat;
import org.fudaa.dodico.telemac.TelemacDicoFileFormatVersion;
import org.fudaa.dodico.telemac.TelemacDicoManager;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.fdico.FDicoNewProjectPanel;
import org.fudaa.fudaa.fdico.FDicoOpenProjectPanel;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.tr.common.TrExecBatchPreferencesPanel;
import org.fudaa.fudaa.tr.common.TrImplHelperAbstract;
import org.fudaa.fudaa.tr.common.TrImplementationEditorAbstract;
import org.fudaa.fudaa.tr.common.TrLauncher;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrPreferences;
import org.fudaa.fudaa.tr.common.TrProjet;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrTelemacImplHelper.java,v 1.50 2007-04-30 14:22:37 deniger Exp $
 */
public class TrTelemacImplHelper extends TrImplHelperAbstract {

  /**
   * @param _r le lanceur d'appli
   */
  public TrTelemacImplHelper(final TrLauncher _r) {
    super(new TrTelemacAppliManager(_r));
  }

  private TrProjet ouvrirTelemac2dInternal(final TrImplementationEditorAbstract _impl, final ProgressionInterface _op,
      final File _fic, final Telemac2dFileFormat.TelemacVersion _ftVersion) {
    if (_fic != null) {
      _op.setDesc(TrResource.getS("Import fichier cas"));
      final TrProjet projet = TrTelemacProjectFactory.createTelemac2dProject(_fic, _ftVersion, _op, _impl);
      _op.setProgression(90);
      return projet;
    }
    return null;
  }

  private TrProjet ouvrirInternal(final TrImplementationEditorAbstract _impl, final ProgressionInterface _op,
      final File _fic, final TelemacDicoFileFormat _ft, final String _v, final File _f, final int _language) {
    if (_fic != null) {
      final CtuluResult<TelemacDicoFileFormatVersion> resFt = TelemacDicoManager.getINSTANCE()
          .createTelemacVersionImpl(_ft, _v, _f, _language, null);
      if (resFt.getAnalyze() != null && !resFt.getAnalyze().isEmpty()) {
        CtuluAnalyzeGUI.showDialogErrorFiltre(resFt.getAnalyze(), _impl, TrResource.getS("Chargement du fichier dico"),
            false);

      }
      final TelemacDicoFileFormatVersion version = resFt.getResultat();
      if (_ft == Telemac2dFileFormat.getInstance()) { return ouvrirTelemac2dInternal(_impl, _op, _fic,
          (Telemac2dFileFormat.TelemacVersion) version); }
      _op.setDesc(TrResource.getS("import fichier cas pour") + CtuluLibString.ESPACE + _ft.getName());

      if (version == null) {
        _impl.error(TrResource.getS("Erreur interne"),
            TrResource.getS("Impossible de cr�er le modele du dictionnaire"), false);
        return null;
      }
      final DicoModelAbstract m = version.getDico();
      if (m == null) {
        _impl.error(TrResource.getS("Erreur interne"),
            TrResource.getS("Impossible de cr�er le modele du dictionnaire"), false);
        return null;
      }
      final TrProjet projet = TrTelemacProjectFactory.createTelemacProjet(_fic, version, _op, _impl);
      _op.setProgression(90);
      return projet;
    }
    return null;
  }

  @Override
  public TrProjet creer(final TrImplementationEditorAbstract _impl, final ProgressionInterface _op,
      final File _gridFile, final Properties _options) {
    final CtuluFileChooserPanel pnConditionLim = new CtuluFileChooserPanel(_gridFile);
    pnConditionLim.setToolTipText(TrResource.getS("Option: choisir le fichier des conditions limites"));
    final BuButton btOlb = new BuButton(FudaaLib.getS("Configurer"));
    final BuCheckBox cbOlb = new BuCheckBox() {

      @Override
      protected void fireItemStateChanged(final ItemEvent _event) {
        super.fireItemStateChanged(_event);
        btOlb.setEnabled(isSelected());
      }
    };
    cbOlb.setSelected(TrPreferences.TR.getBooleanProperty("tr.telemac.use.olb", false));
    cbOlb.setToolTipText(TrResource.getS("Option: OLB permet d'optimiser la structure du maillage (largeur de bande)"));
    final OLBExec exec = new OLBExec();
    btOlb.setEnabled(cbOlb.isSelected());
    btOlb.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _e) {
        final TrExecBatchPreferencesPanel pn = new TrExecBatchPreferencesPanel(new CalculExecBatch[] { exec });
        final CtuluDialogPanel panel = new CtuluDialogPanel() {

          @Override
          public boolean isDataValid() {
            return pn.isDataValide();
          }

          @Override
          public boolean apply() {
            pn.applyPreferences();
            if (exec.isExecFileExists()) {
              btOlb.setForeground(Color.BLACK);
            } else {
              btOlb.setForeground(Color.RED);
            }
            return true;
          }
        };
        panel.setLayout(new BuBorderLayout());
        panel.add(pn, BuBorderLayout.CENTER);
        panel.afficheModale(_impl.getFrame(), "OLB");

      }
    });
    cbOlb.setText(TrResource.getS("Optimiser avec OLB"));

    if (!exec.isExecFileExists()) {
      btOlb.setForeground(Color.RED);
    }

    final FDicoNewProjectPanel p = new FDicoNewProjectPanel(TelemacDicoManager.getINSTANCE(), _gridFile) {

      @Override
      public boolean isDataValid() {
        final File newCas = getSelectedFile();
        if (getAppliMng().isEditorAlreadyOpen(newCas)) {
          setErrorText(TrLib.getMessageAlreadyOpen(newCas));
          return false;
        }
        boolean r = super.isDataValid();
        String s = getErrorText();
        if (cbOlb.isSelected() && !exec.isExecFileExists()) {
          if (s.length() > 0) {
            s += CtuluLibString.LINE_SEP;
          }
          s += TrResource.getS("L'ex�cutable OLB n'est pas correctement d�fini");
          r = false;
        }
        final File conlimi = pnConditionLim.getFile();
        if (conlimi != null && !conlimi.exists()) {
          if (s.length() > 0) {
            s += CtuluLibString.LINE_SEP;
          }
          s += CtuluLibString.LINE_SEP + TrResource.getS("Le fichier des conditions limites n'existe pas");
          r = false;
        }
        if (!r) {
          setErrorText(s);
        }
        return r;
      }
    };

    if (_gridFile != null) {
      p.setDefaultFile(new File(_gridFile.getParentFile(), "cas").getAbsolutePath());
    }
    p.setDicoSelected(Telemac2dFileFormat.getInstance());
    p.setHelpText(TrResource.getS("<html>1) Optionnel: choisir le fichier serafin<br>"
        + "2) Choisir le nom du nouveau fichier cas"
        + "<br>&nbsp;&nbsp;<b>Attention: ce fichier sera �cras� lors de la premi�re sauvegarde"
        + "</b><br>3) Choisir le type du projet<br>4) Choisir la version du logiciel telemac "
        + "<br>5) Sp�cifier le langage du projet </html>"));
    // support de olb
	// support of olb
    p.addLabel(TrResource.getS("Fichier des conditions limites") + ": ");
    p.add(pnConditionLim);
    p.add(cbOlb);
    p.add(btOlb);
    p.invalidate();
    // creation panel
    final int r = p.afficheModale(_impl.getFrame(), TrResource.getS("Cr�er un nouveau projet T�l�mac"), p
        .createRunnableForFileText());
    if (CtuluDialogPanel.isOkResponse(r)) {
      // _impl.initTaskView(_bu);
      // on enregistre la propriete olb
	  // we save the propiety olb
      TrPreferences.TR.putBooleanProperty("tr.telemac.use.olb", cbOlb.isSelected());
      final File f = p.getFile();
      if (f == null || f.getParentFile() == null) { return null; }
      final CtuluResult<TelemacDicoFileFormatVersion> resFt = TelemacDicoManager.getINSTANCE()
          .createTelemacVersionImpl(p.getFileFormatSelected(), p.getVersionSelected(), p.getDicoFile(),
              p.getLanguageSelected(), null);
      if (resFt.getAnalyze() != null && !resFt.getAnalyze().isEmpty()) {
        CtuluAnalyzeGUI.showDialogErrorFiltre(resFt.getAnalyze(), _impl, TrResource.getS("Chargement du fichier dico"),
            false);

      }
      TelemacDicoFileFormatVersion ft = resFt.getResultat();
      if (ft != null) {
        File gridFile = new File(p.getGridFileField().getText());
        if (!gridFile.exists()) {
          gridFile = null;
        }
        final File ficCl = pnConditionLim.getFile();
        Map keyword = null;
        if (ficCl != null) {
          keyword = new HashMap();
          keyword.put(ft.getCLEntiteFile(), CtuluLibFile.getRelativeFile(ficCl, f.getParentFile(), 100));
        }
        _options.put("use.olb", Boolean.toString(cbOlb.isSelected()));
        return TrTelemacProjectFactory.createNewTelemacProjet(f, ft, _impl, keyword, null, gridFile);
      }
    }
    return null;
  }

  /**
   * @return les menuitem a rajouter dans le menu ouvrir / the menu items to add in the open menu
   */
  @Override
  public BuMenuItem[] getMenuItemsOuvrir(final TrImplementationEditorAbstract _impl) {
    final BuMenuItem r = new BuMenuItem(FudaaResource.FUDAA.getToolIcon("appli/reflux"), TrResource
        .getS("Projet Reflux"));
    r.setActionCommand(TrImplementationEditorAbstract.PREF_OUVRIR + FileFormatSoftware.REFLUX_IS.name);
    final BuMenuItem rO = new BuMenuItem(null, TrResource.getS("Projet Rubar"));
    rO.setActionCommand(TrImplementationEditorAbstract.PREF_OUVRIR + FileFormatSoftware.RUBAR_IS.name);
    return new BuMenuItem[] { r, rO };
  }

  /**
   * @return l'id du software / the software id
   */
  @Override
  public String getSoftwareID() {
    return getID();
  }

  public static String getID() {
    return FileFormatSoftware.TELEMAC_IS.name;
  }

  /**
   * Ouvre le fichier _f (Telemac 2D).
   * Open the file _f (Telemac 2D).
   */
  @Override
  public TrProjet ouvrir(final TrImplementationEditorAbstract _impl, final ProgressionInterface _op, final File _f,
      final Map _params) {
    final FDicoOpenProjectPanel s = new FDicoOpenProjectPanel(TelemacDicoManager.getINSTANCE()) {
      @Override
      public boolean isDataValid() {
        if (getAppliMng().isEditorAlreadyOpen(getSelectedFile())) {
          setErrorText(TrLib.getMessageAlreadyOpen(getSelectedFile()));
          return false;
        }
        return super.isDataValid();
      }
    };
    s.setHelpText(TrResource.getS("<html>1) Choisir le fichier cas � ouvrir<br>2) Choisir le type du projet"
        + "<br>3) Choisir la version du logiciel telemac "
        + "<br>4) Sp�cifier le langage du projet (optionnel)<br>&nbsp;&nbsp;&nbsp;"
        + "Le pr�processeur peut d�terminer le langage � partir du fichier cas</html>"));
    s.setDicoSelected(TelemacDicoManager.getINSTANCE().getFileFormat("telemac2d"));
    if (_f != null) {
      s.setSelectedFic(_f.getAbsolutePath());
    }
    final String tltip = TrResource
        .getS("Si activ�, les fichiers seront charg�s et la vue 2D sera affich�e au d�marrage");
    s.addLabel(TrResource.getS("Editeur 2D")).setToolTipText(tltip);
    final BuCheckBox cb = new BuCheckBox();
    cb.setText(TrResource.getS("Afficher au d�marrage"));
    cb.setToolTipText(tltip);
    cb.setSelected(TrPreferences.TR.getBooleanProperty("telemac.open2d.auto", false));
    s.add(cb);

    final FileFormatSoftware soft = TrLib.findPreMainVersion(_f);
    if (soft != null && FileFormatSoftware.TELEMAC_IS.name.equals(soft.system_)) {
      s.setSelected(soft);
    }
    final int r = s.afficheModale(_impl.getFrame(), TrResource.getS("Ouvrir un fichier cas"), s
        .createRunnableForFileText());
    if (CtuluDialogPanel.isOkResponse(r)) {
      TrPreferences.TR.putBooleanProperty("telemac.open2d.auto", cb.isSelected());
      if (_params != null) {
        _params.put("loadall", Boolean.valueOf(cb.isSelected()));
      }
      return ouvrirInternal(_impl, _op, s.getSelectedFile(), (TelemacDicoFileFormat) s.getFileFormatSelected(), s
          .getVersionSelected(), s.getDicoFile(), s.getLanguageSelected());
    }
    return null;
  }

  /**
   * @param _op l'ecouteur pour l'avancement / the listener for the progression
   * @return null is aucun projet ouvert / null if no project opened
   */
  @Override
  public TrProjet ouvrir(final TrImplementationEditorAbstract _impl, final ProgressionInterface _op) {
    return ouvrir(_impl, _op, null, null);
  }
}
