/**
 *  @creation     8 juil. 2004
 *  @modification $Date: 2007-05-04 14:01:46 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import java.io.File;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacSaveBackupGeometryFile.java,v 1.11 2007-05-04 14:01:46 deniger Exp $
 */
public class TrTelemacSaveBackupGeometryFile extends CtuluDialogPanel implements DocumentListener {

  String newName_;
  JTextField file_;
  BuLabel lbIco_;
  BuLabel lbTxt_;

  @Override
  public void changedUpdate(final DocumentEvent _e) {
    changed();
  }

  @Override
  public void insertUpdate(final DocumentEvent _e) {
    changed();
  }

  @Override
  public void removeUpdate(final DocumentEvent _e) {
    changed();
  }

  private void changed() {
    final File f = new File(file_.getText());
    file_.setToolTipText(f.getAbsolutePath());
    final StringBuffer b = new StringBuffer(100);
    b.append("<html><b>");
    b.append(TrResource.getS("Aucune sauvegarde ne sera effectu�e en annulant cette bo�te de dialogue"));
    b.append("</b>");
    if (f.exists()) {
      b.append("<br><u>").append(TrResource.getS("Le fichier existe d�j�")).append("</u>. ").append(
          TrResource.getS("Il sera �cras�."));
    }
    b.append("</html>");
    lbTxt_.setText(b.toString());
    if (f.isDirectory()) {
      setErrorText(TrResource.getS("Le fichier s�lectionn� est un r�pertoire"));
    }
    if (initFile_.equals(f)) {
      setErrorText(TrResource.getS("Vous ne pouvez pas sauvegarder sur le fichier d'origine."));
    } else if ((f.getParentFile() != null && !f.getParentFile().canWrite()) || (f.exists() && !f.canWrite())) {
      setErrorText(TrResource.getS("Vous n'avez pas les droits n�cessaires pour " + "cr�er le fichier de sauvegarde"));
    } else {
      cancelErrorText();
    }
  }

  File initFile_;

  public TrTelemacSaveBackupGeometryFile(final File _initFile) {
    super(true);
    initFile_ = _initFile;
    setLayout(new BuGridLayout(2, 3, 5));
    final File destFile = new File(_initFile.getParentFile(), _initFile.getName() + ".old");
    file_ = addLabelFileChooserPanel(TrResource.getS("Fichier de sauvegarde"), destFile, false, true);
    file_.setToolTipText(destFile.getAbsolutePath());
    lbIco_ = new BuLabel(UIManager.getIcon("OptionPane.warningIcon"));
    final boolean warn = destFile.exists();
    lbIco_.setVisible(warn);
    add(lbIco_);
    lbTxt_ = new BuLabel();
    add(lbTxt_);
    file_.getDocument().addDocumentListener(this);
    changed();
  }

  @Override
  public boolean apply() {
    final File f = new File(file_.getText());
    CtuluLibFile.copyFile(initFile_, f);
    CtuluLibMessage.info("save old geometry file in " + f);
    return true;
  }

  @Override
  public boolean isDataValid() {
    return !getError().isVisible();
  }
}
