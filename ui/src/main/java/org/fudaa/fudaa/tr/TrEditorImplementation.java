/*
 * @creation 2002-08-29
 * @modification $Date: 2008-01-15 11:38:49 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.tr;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluFileChooserFileTester;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.ebli.calque.EbliFilleCalquesInterface;
import org.fudaa.ebli.impression.EbliMiseEnPagePreferencesPanel;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;
import org.fudaa.fudaa.commun.impl.FudaaLookPreferencesPanel;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.tr.common.*;
import org.fudaa.fudaa.tr.reflux.TrRefluxExecPreferencesPanel;
import org.fudaa.fudaa.tr.reflux.TrRefluxImplHelper;
import org.fudaa.fudaa.tr.rubar.TrRubarExecPreferencesPanel;
import org.fudaa.fudaa.tr.rubar.TrRubarImplHelper;
import org.fudaa.fudaa.tr.telemac.TrTelemacExecPreferencesPanel;
import org.fudaa.fudaa.tr.telemac.TrTelemacImplHelper;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * L'implementation du client Tr.
 *
 * @author Fred Deniger
 * @version $Id: TrEditorImplementation.java,v 1.48 2008-01-15 11:38:49 bmarchan Exp $
 */
public class TrEditorImplementation extends TrImplementationEditorAbstract {
  public static final String ACTION_VALID_GRID = "VALID_GRID";

  /**
   * @return les informations sur le logiciel
   */
  public static BuInformationsSoftware informationsSoftware() {
    return TrLauncherDefault.getInfosSoftware();
  }

  // protected BArbreCalque arbre_;
  public TrProjet currentProject_;
  TrImplHelperAbstract implHelper_;

  /**
   * Ne fait rien.
   */
  public TrEditorImplementation() {
    super();
  }

  @Override
  public void cmdOuvrirFile(final File _f) {
    ouvrir(_f);
  }

  @Override
  public void doImportProject() {
    if (currentProject_ != null && currentProject_.getVisuFille() != null) {
      TrProjectPersistence.importForFrame((JInternalFrame) currentProject_.getVisuFille(), this);
    }
  }

  private void updateAfterChaineChanged() {
  }

  @Override
  protected void activePostFrame(final File _f) {
    launcher_.openPost(_f);
  }

  /**
   * Methode appelee une seule fois pour construire le menu d'export.
   */
  @Override
  protected void buildExporterMenu() {
    final BuMenu m = (BuMenu) getMainMenuBar().getMenu(FudaaGuiLib.getExporterName());
    m.addMenuItem(MvResource.getS("Maillage"), "EXPORT_MAILLAGE");
    addExportCurrentFrame(m);
  }

  @Override
  protected void buildPreferences(final List _prefs) {
    _prefs.add(new BuUserPreferencesPanel(this));
    _prefs.add(new BuBrowserPreferencesPanel(this));
    _prefs.add(new BuLanguagePreferencesPanel(this));
    _prefs.add(new BuDesktopPreferencesPanel(this));
    _prefs.add(new FudaaLookPreferencesPanel(this));
    _prefs.add(new EbliMiseEnPagePreferencesPanel());
    final String soft = launcher_.getCurrentImplHelper().getSoftwareID();
    if (soft.equals(TrTelemacImplHelper.getID())) {
      _prefs.add(new TrTelemacExecPreferencesPanel());
    } else if (soft.equals(TrRefluxImplHelper.getID())) {
      _prefs.add(new TrRefluxExecPreferencesPanel());
    } else if (soft.equals(TrRubarImplHelper.getID())) {
      _prefs.add(new TrRubarExecPreferencesPanel());
    }
    _prefs.add(new TrChainePreferencePanel());
  }

  @Override
  protected void creer() {
    creer(null);
  }

  @Override
  protected void creer(final File _f) {
    setGlassPaneStop();
    new CtuluTaskOperationGUI(this, CtuluLibString.EMPTY_STRING) {
      @Override
      public void act() {
        try {
          final ProgressionInterface prog = createProgressionInterface(this);
          if (currentProject_ != null && !saveAndCloseProjet(prog)) {
            return;
          }
          final Properties options = new Properties();
          final TrProjet pr = getImplHelper().creer(TrEditorImplementation.this, prog, _f, options);
          if (pr != null) {

            setProjet(pr);
            pr.finishCreation(prog, options);
          }
        } finally {
          unsetGlassPaneStop();
        }
      }
    }.start();
  }

  @Override
  protected String getCurrentProjectHelp() {
    return getInformationsSoftware().man
        + ((projetCourant() == null) ? (implHelper_.getSoftwareID()) : (projetCourant().getSoftwareID())) + '/';
  }

  protected TrImplHelperAbstract getImplHelper() {
    return getLauncher().getCurrentImplHelper();
  }

  @Override
  public BuIcon getSpecificIcon() {
    return TrResource.getEditorIcon();
  }

  @Override
  public String getSpecificName() {
    return TrResource.getEditorName();
  }

  @Override
  protected void ouvrir() {
    ouvrir(getImplHelper());
  }

  protected void ouvrir(final TrImplHelperAbstract _helper) {
    setGlassPaneStop();
    new CtuluTaskOperationGUI(this, CtuluLibString.EMPTY_STRING) {
      @Override
      public void act() {
        try {
          final ProgressionInterface prog = createProgressionInterface(this);
          // initTaskView(this);
          final TrProjet pr = _helper.ouvrir(TrEditorImplementation.this, prog);
          if (pr != null) {
            if (currentProject_ != null) {
              saveAndCloseProjet(prog);
            }
            setProjet(pr);
          }
        } finally {
          unsetGlassPaneStop();
        }
      }
    }.start();
  }

  @Override
  protected void ouvrir(final File _f) {
    ouvrir(_f, getCurrentImplHelper());
  }

  protected BuGlassPaneStop getStopGlassPane() {
    return glassPaneStop_;
  }

  protected void ouvrir(final File _f, final TrImplHelperAbstract _impl) {
    if (_f == null) {
      ouvrir(_impl);
      return;
    }
    setGlassPaneStop();
    new CtuluRunnable(DodicoLib.getS("Lecture") + CtuluLibString.ESPACE + _f.getName(), this) {
      @Override
      public boolean run(final ProgressionInterface _proj) {
        try {
          if ((currentProject_ != null) && !saveAndCloseProjet(_proj)) {
            return false;
          }
          final Map m = new HashMap();
          final TrProjet pr = _impl.ouvrir(TrEditorImplementation.this, _proj, _f, m);
          if (pr == null) {
            BuLib.invokeNow(new Runnable() {
              @Override
              public void run() {
                unsetGlassPaneStop();
                getFrame().setTitle(TrResource.getS("Editeur"));
              }
            });
            return true;
          }
          active(pr, m);
        } finally {
          unsetGlassPaneStop();
        }
        return true;
      }
    }.run();
  }

  protected void active(final TrProjet _pr, final Map _m) {
    final boolean loadAll = (_m.get("loadall") == Boolean.TRUE);
    final CtuluRunnable runnable = new CtuluRunnable(TrResource.getS("Chargement"), this) {
      @Override
      public boolean run(final ProgressionInterface _proj) {
        if (loadAll) {
          _pr.loadAll(_proj);
        }
        return true;
      }
    };
    runnable.setBeforeRunnable(new Runnable() {
      @Override
      public void run() {
        setGlassPaneStop();
        setProjet(_pr);
      }
    }, true);

    runnable.setAfterRunnable(new Runnable() {
      @Override
      public void run() {
        if (loadAll) {
          _pr.showVisuFille();
        }
        unsetGlassPaneStop();
      }
    }, true);

    runnable.run();
  }

  protected void ouvrirId(final String _id) {
    ouvrirId(_id, null);
  }

  @Override
  protected void ouvrirId(final String _id, final File _f) {

    final TrImplHelperAbstract h = launcher_.getImplHelper(_id);
    if (h == null) {
      error(TrResource.getS("Erreur interne"), TrResource.getS("Impossible de trouver l'implantation"), false);
    } else {
      ouvrir(_f, h);
    }
  }

  protected TrProjet projetCourant() {
    return currentProject_;
  }

  /**
   * @param _o le nouveau projet courant
   */
  protected void setProjet(final TrProjet _o) {
    BuLib.invokeNow(new Runnable() {
      @Override
      public void run() {
        if (currentProject_ != null) {
          currentProject_.close();
        }
        currentProject_ = _o;
        if (currentProject_ == null) {
          return;
        }
        setEnabledForAction("FERMER", true);
        setEnabledForAction("ENREGISTRER", true);
        setEnabledForAction("ENREGISTRERSOUS", true);
        setEnabledForAction(FudaaGuiLib.getSaveCopy(), true);
        setEnabledForAction(FudaaGuiLib.getArchiver(), true);
        setEnabledForAction(FudaaGuiLib.getExporterName(), true);
        setEnabledForAction("IMPORTER", true);
        setEnabledForAction("IMPORT_PROJECT", true);
        _o.active();
      }
    });
    TrProjectPersistence.loadProject(this, _o.getParamsFile());
  }

  public final void archiverProjet() {
    final TrProjet projet = projetCourant();
    if (projet == null) {
      return;
    }
    final String title = getProjectArchiveTitle();
    // le testeur pour le choix du fichier d'archivage
    final CtuluFileChooserFileTester tester = new CtuluFileChooserFileTester() {
      @Override
      public boolean isFileOk(final File _selectedFile, final CtuluFileChooser _ch) {
        if (_selectedFile == null) {
          return false;
        }
        final File f = CtuluLibFile.appendStrictExtensionIfNeeded(_selectedFile, "zip");
        String err = null;
        if (f.isDirectory()) {
          err = TrResource.getS("Pr�ciser le nom du fichier");
        } else {
          err = CtuluLibFile.canWrite(f);
        }
        if (err != null) {
          TrEditorImplementation.this.error(title, err);
          return false;
        }
        return true;
      }
    };
    final CtuluFileChooser fileChooser = FudaaGuiLib.getFileChooser(title + ": "
        + TrResource.getS("Choisir le fichier zip"), new BuFileFilter[]{new BuFileFilter("zip")}, tester);
    final File paramsFile = projetCourant().getParamsFile();
    if (paramsFile != null) {

      fileChooser.setSelectedFile(new File(paramsFile.getParentFile(), CtuluLibFile.getSansExtension(paramsFile
          .getName())
          + '_' + FudaaLib.getCurrentDateForFile() + ".zip"));
    }
    final File zip = FudaaGuiLib.chooseFile(getFrame(), true, fileChooser);
    if (zip == null) {
      return;
    }

    final CtuluTaskDelegate task = createTask(title);
    final ProgressionInterface prog = task.getStateReceiver();
    task.start(new Runnable() {
      @Override
      public void run() {
        File dir = null;
        try {
          String name = "project";
          final File f = projet.getParamsFile();
          if (f != null) {
            name = f.getName();
          }
          dir = CtuluLibFile.createTempDir();
          final File dest = new File(dir, name);
          final File destReal = projet.saveCopy(prog, dest);
          if (destReal == null || !destReal.equals(dest)) {
            TrEditorImplementation.this.error(TrResource.getS("Impossible de cr�er une copie du projet"));
            return;
          }
          if (!CtuluLibFile.zip(dir.listFiles(), zip)) {
            TrEditorImplementation.this.error(title, CtuluLib.getS("L'op�ration a echou�"));
          }
        } catch (final IOException _e) {
          error(title, CtuluLib.getS("Impossible de cr�er un dossier temporaire"));
          FuLog.warning(_e);
        } finally {
          if (dir != null) {
            CtuluLibFile.deleteDir(dir);
          }
        }
      }
    });
  }

  public String getProjectArchiveTitle() {
    return TrResource.getS("Archiver le projet");
  }

  /**
   * Les actions courantes.
   */
  @Override
  public void actionPerformed(final ActionEvent event) {
    final String action = event.getActionCommand();
    if (action == null) {
      return;
    }
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("ACTION=" + action);
    }
    if ("OUVRIR".equals(action)) {
      ouvrir();
    } else if ("CREER".equals(action)) {
      creer();
    } else if ("ENREGISTRER".equals(action)) {
      if (projetCourant().getTrParams().getH2dParametres().isGridInvalid()) {
        new TrProjectExportAction(this).manageExport();
        return;
      }
      new CtuluTaskOperationGUI(this, CtuluLibString.EMPTY_STRING) {
        @Override
        public void act() {
          projetCourant().save(createProgressionInterface(this));
        }
      }.start();
    } else if ("ENREGISTRERSOUS".equals(action)) {
      if (projetCourant().getTrParams().getH2dParametres().isGridInvalid()) {
        return;
      }
      new CtuluRunnable(CtuluLibString.EMPTY_STRING, this) {
        @Override
        public boolean run(final ProgressionInterface _proj) {
          projetCourant().saveAs(_proj);
          return true;
        }
      }.run();
    } else if (FudaaGuiLib.getSaveCopy().equals(action)) {
      if (projetCourant().getTrParams().getH2dParametres().isGridInvalid()) {
        return;
      }
      new CtuluTaskOperationGUI(this, CtuluLibString.EMPTY_STRING) {
        @Override
        public void act() {
          projetCourant().saveCopy(createProgressionInterface(this), null);
        }
      }.start();
    } else if (FudaaGuiLib.getArchiver().equals(action)) {
      archiverProjet();
    } else if ("FERMER".equals(action)) {
      new CtuluTaskOperationGUI(this, CtuluLibString.EMPTY_STRING) {
        @Override
        public void act() {
          saveAndCloseProjet(createProgressionInterface(this));
        }
      }.start();
    } else if ("MODIFIER_CHAINE".equals(action)) {
      getLauncher().changeChaineCalcul(getFrame());
      updateAfterChaineChanged();
    } else if ("EXPORT_MAILLAGE".equals(action)) {
      projetCourant().exportMaillage();
    } else if (action.startsWith(TrImplementationEditorAbstract.PREF_OUVRIR)) {
      final String id = action.substring(TrImplementationEditorAbstract.PREF_OUVRIR.length());
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("id= " + id);
      }
      ouvrirId(id);
    } else {
      super.actionPerformed(event);
    }
  }

  @Override
  public void addInternalFrame(final JInternalFrame jInternalFrame) {
    super.addInternalFrame(jInternalFrame);
    if ((arbre_ != null) && (jInternalFrame instanceof EbliFilleCalquesInterface)) {
      jInternalFrame.addInternalFrameListener(arbre_);
    }
  }

  @Override
  public boolean confirmExit() {
    if (currentProject_ != null) {
      if (currentProject_.getState().isModified()) {
        return saveAndCloseProjet(null);
      } else if (super.confirmExit()) {
        currentProject_.close();
        return true;
      }
    }
    return super.confirmExit();
  }

  /**
   * @return l'implementation en cours
   */
  @Override
  public TrImplHelperAbstract getCurrentImplHelper() {
    return implHelper_;
  }

  /**
   * @return les infos sur le doc en cours
   */
  public BuInformationsDocument getInformationsDocument() {
    if (projetCourant() == null) {
      return null;
    }
    return projetCourant().getInformationsDocument();
  }

  @Override
  public BuInformationsSoftware getInformationsSoftware() {
    return TrLauncherDefault.getInfosSoftware();
  }

  /**
   * Construction des menus suppl�mentaires.
   */
  @Override
  public void init() {
    super.init();
    getUndoCmdListener();
    final BuMenuBar mb = getApp().getMainMenuBar();
    final JMenu menuFile = mb.getMenu("MENU_FICHIER");

    BuMenuItem it = new BuMenuItem(getProjectArchiveTitle() + " (zip)");
    it.setIcon(CtuluResource.CTULU.getIcon("zip"));
    it.setActionCommand(FudaaGuiLib.getArchiver());
    it.addActionListener(TrEditorImplementation.this);
    final int indexInsert = 6;
    menuFile.add(it, indexInsert);
    it = new BuMenuItem(TrResource.getS("Enregister une copie"));
    it.setIcon(FudaaResource.FUDAA.getIcon("enregistrer-copie"));
    it.setActionCommand(FudaaGuiLib.getSaveCopy());
    it.addActionListener(TrEditorImplementation.this);
    menuFile.add(it, indexInsert);
    buildTaskView();

    setEnabledForAction("CREER", true);
    setEnabledForAction("OUVRIR", true);
    setEnabledForAction(FudaaGuiLib.getExporterName(), false);
    setEnabledForAction("PREFERENCE", true);
    setEnabledForAction(FudaaGuiLib.getSaveCopy(), false);
    setEnabledForAction(FudaaGuiLib.getArchiver(), false);
    setEnabledForAction("ASTUCE", false);
    // fileFormatMng_ = new TrFileFormatManager();
    mb.computeMnemonics();

    unsetMainProgression();
    unsetMainMessage();
  }

  /**
   * Pour Eviter que la commande fermer concerne uniquement les internal frame.
   */
  @Override
  public boolean isCloseFrameMode() {
    return false;
  }

  @Override
  public void removeInternalFrame(final JInternalFrame _f) {
    if ((arbre_ != null) && (_f instanceof EbliFilleCalquesInterface)) {
      _f.removeInternalFrameListener(arbre_);
    }
    super.removeInternalFrame(_f);
  }

  /**
   * @param _interface maj de progression
   * @return false si l'utilisateur a annul� l'action
   */
  public boolean saveAndCloseProjet(final ProgressionInterface _interface) {
    if (currentProject_ != null && currentProject_.getState().isModified()) {
      if (projetCourant().getTrParams().getH2dParametres().isGridInvalid()) {
        return question(
            TrLib.getString("Voulez-vous fermer le projet sans sauvegarder ?"),
            "<html><body>"
                + TrLib.getString("Le projet courant courant contient un maillage avec une structure modifi�e et ne peut pas �tre sauvegard�.") + "<br>"
                + TrLib.getString("Les donn�es du maillage peuvent �tre export�es via le menu Fichier > Export > Maillage") + "<br><br>"
                + TrLib.getString("En cliquant sur Oui, le projet sera ferm� et vos modifications ne seront pas persist�es") + "<br>"
                + "</body></html>"

        );
      }
      final int i = CtuluLibDialog.confirmExitIfProjectisModified(getFrame());
      if (i == JOptionPane.CANCEL_OPTION) {
        return false;
      } else if (i != JOptionPane.NO_OPTION) {
        currentProject_.save(_interface);
      }
    }
    final Runnable r = new Runnable() {
      @Override
      public void run() {
        currentProject_.close();
        currentProject_ = null;
        getMainMenuBar().revalidate();
        getFrame().setTitle(TrResource.getS("Editeur"));
        setEnabledForAction("ENREGISTRER", false);
        setEnabledForAction("ENREGISTRERSOUS", false);
        setEnabledForAction(FudaaGuiLib.getSaveCopy(), false);
        setEnabledForAction("FERMER", false);
        setEnabledForAction(FudaaGuiLib.getExporterName(), false);
        setEnabledForAction("IMPORTER", false);
        setEnabledForAction("IMPORT_PROJECT", false);
        removeInternalFrames(getAllInternalFrames());
      }
    };
    BuLib.invokeNow(r);
    return true;
  }

  @Override
  public void start() {
    super.start();
    implHelper_ = launcher_.getCurrentImplHelper();
    implHelper_.active(this);
    // pour utiliser le browser externe par defaut.
    if (BuPreferences.BU.getIntegerProperty("browser.exec", -1) < 0) {
      BuPreferences.BU.putIntegerProperty("browser.exec", 2);
    }
  }
}
