package org.fudaa.fudaa.tr.rubar;

import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

public class TrRubarBlockHelper {
  public static H2dVariableType getVar(final int _column) {
    if (_column == 2) {
      return H2dVariableType.DEBIT_NORMAL;
    }
    if (_column == 3) {
      return H2dVariableType.DEBIT_TANGENTIEL;
    }
    if (_column == 4) {
      return H2dVariableType.COTE_EAU;
    }
    //for concentration, there are 3 columns by block
    int concentrationIdx = (_column - 5) % 3;

    if (concentrationIdx == 0) {
      return H2dVariableTransType.CONCENTRATION;
    }
    if (concentrationIdx == 1) {
      return H2dVariableTransType.DIAMETRE;
    }
    if (concentrationIdx == 2) {
      return H2dVariableTransType.ETENDUE;
    }
    return null;
  }

  public static int getBlockIdx(final int column) {
    int concentrationIdx = (column - 5);
    int offset = concentrationIdx % 3;
    return (concentrationIdx - offset) / 3;
  }
}
