/**
 * @creation 21 oct. 2003
 * @modification $Date: 2007-05-04 14:01:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.fu.FuLog;
import java.util.Set;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.h2d.H2dBcFrontierInterface;
import org.fudaa.dodico.h2d.H2dBoundary;
import org.fudaa.dodico.h2d.telemac.H2dTelemacParameters;
import org.fudaa.dodico.h2d.telemac.H2dTelemacProjectDispatcherListener;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.fudaa.tr.common.TrCourbeTemporelleManager;
import org.fudaa.fudaa.tr.common.TrProjectDispatcherListener;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * @author deniger
 * @version $Id: TrTelemacCommunProjectListener.java,v 1.36 2007-05-04 14:01:46 deniger Exp $
 */
public class TrTelemacCommunProjectListener extends TrProjectDispatcherListener implements H2dTelemacProjectDispatcherListener {

  public TrTelemacCommunProjectListener() {
  }

  protected TrTelemacCommunProjet getTProj() {
    return (TrTelemacCommunProjet) proj_;
  }

  private void modifyBCFile() {
    final DicoEntite e = getTProj().getTrTelemacParams().getDicoFileFormatVersion().getCLEntiteFile();
    if (e == null) {
      FuLog.warning(new Throwable());
    } else {
      super.setParamsChanged();
      getProjectState().setLoadedModified(e, true);
    }
  }

  private void modifyCulvertWeirFile() {
    final DicoEntite e = getTProj().getTelemacFileFormatVersion().getSeuilSiphonFile();
    if (e == null) {
      FuLog.warning(new Throwable());
    } else {
      super.setParamsChanged();
      getProjectState().setLoadedModified(e, true);
    }
  }

  private void modifyGeometryFile() {
    final DicoEntite e = getTProj().getTrTelemacParams().getDicoFileFormatVersion().getMaillageEntiteFile();
    if (e == null) {
      FuLog.warning(new Throwable());
    } else {
      super.setParamsChanged();
      getProjectState().setLoadedModified(e, true);
    }
  }

  private void modifyLiquidFile() {
    if (getTProj().isTransientAvailable()) {
      final DicoEntite e = getTProj().getCLLiquideEntiteFile();
      if (e == null) {
        FuLog.warning(new Throwable());
      } else {
        super.setParamsChanged();
        getProjectState().setLoadedModified(e, true);
      }
    }
  }

  private void modifyRepriseFile() {
    final DicoEntite e = getTProj().getTrTelemacParams().getRepriseFileKeyword();
    if (e == null) {
      FuLog.warning(new Throwable());
    } else {
      super.setParamsChanged();
      getProjectState().setLoadedModified(e, true);
    }
  }

  private void updateLiqCurve(final DicoEntite _e) {
    if (getTProj().isTransientAvailable()
        && ((_e == getTProj().getTelemacFileFormatVersion().getDureeDuCalcul()) || (_e == getTProj().getTelemacFileFormatVersion().getNbPasTemps()) || (_e == getTProj()
            .getTelemacFileFormatVersion().getPasTemps()))) {
      final H2dTelemacParameters p = getTProj().getTrTelemacParams().getTelemacParametres();
      final Set s = p.getUsedEvol();
      if ((s != null) && (s.size() > 0)) {
        final EvolutionReguliereAbstract[] r = new EvolutionReguliereAbstract[s.size()];
        s.toArray(r);
        final long time = getTProj().getTrTelemacParams().getTelemacParametres().getComputationDuration();
        if (TrCourbeTemporelleManager.isCurveNotEnoughLarge(0, (int) time, r)) {
          modifyLiquidFile();
        }
        CtuluLibMessage.info("LIQUID CURVES NO WIDTH ENOUGH");
      }
    }
  }

  private void updateSi(final DicoEntite _ent) {
    if (getTProj().getTrTelemacParams().getRepriseBooleanKw() == _ent) {
      fireGridDataChanged(null);
      super.setParamsChanged();
    }
  }

  @Override
  public void bcBoundaryTypeChanged(final H2dBoundary _bc, final H2dBoundaryType _old) {
    if (proj_ == null) {
      return;
    }
    super.bcBoundaryTypeChanged(_bc, _old);
    modifyBCFile();
    modifyLiquidFile();
  }

  @Override
  public void bcFrontierStructureChanged(final H2dBcFrontierInterface _b) {
    if (proj_ == null) {
      return;
    }
    super.bcFrontierStructureChanged(_b);
    modifyBCFile();
    modifyLiquidFile();
  }

  @Override
  public void bcParametersChanged(final H2dBoundary _b, final H2dVariableType _t) {
    if (proj_ == null) {
      return;
    }
    super.bcParametersChanged(_b, _t);
    modifyBCFile();
    modifyLiquidFile();
  }

  /**
   *
   */
  @Override
  public void bcPointsParametersChanged(final H2dVariableType _t) {
    if (proj_ == null) {
      return;
    }
    super.bcPointsParametersChanged(_t);
    modifyBCFile();
  }

  @Override
  public void culvertAdded() {
    culvertValueChanged(true);
  }

  @Override
  public void culvertRemoved() {
    culvertValueChanged(true);
  }

  @Override
  public void culvertValueChanged(final boolean _connexionChanged) {
    modifyCulvertWeirFile();
  }

  @Override
  public void dicoParamsEntiteAdded(final DicoParams _cas, final DicoEntite _ent) {
    if (proj_ == null) {
      return;
    }
    super.dicoParamsEntiteAdded(_cas, _ent);
    updateLiqCurve(_ent);
    updateSi(_ent);
  }

  @Override
  public void dicoParamsEntiteRemoved(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    if (proj_ == null) {
      return;
    }
    super.dicoParamsEntiteRemoved(_cas, _ent, _oldValue);
    updateLiqCurve(_ent);
    updateSi(_ent);
    if (_ent.isFileType()) {
      final TrTelemacCommunParametres p = getTProj().getTrTelemacParams();
      if (p.isParametersLoaded(_ent)) {
        if (_ent.isRequired()) {
          proj_
              .getImpl()
              .message(
                  TrResource.getS("fichier utilis� et requis"),
                  TrResource.getS("La pr�sence du  fichier {0} est obligatoire.", _ent.getNom())
                      + CtuluLibString.LINE_SEP
                      + TrResource
                          .getS(
                              "Le projet contient des donn�es concernant le fichier {0}. Lors de la prochaine sauvegarde, un nouveau fichier sera cr�e et le mot-cl� {0} sera modifi�",
                              _ent.getNom()), false);
        } else {
          proj_
              .getImpl()
              .message(
                  TrResource.getS("fichier utilis�"),
                  TrResource
                      .getS(
                          "Le projet contient des donn�es concernant le fichier {0}. Lors de la prochaine sauvegarde, un nouveau fichier sera cr�e et le mot-cl� {0} sera modifi�",
                          _ent.getNom()), false);
        }
      } else if (_ent.isRequired()) {
        proj_.getImpl().message(
            TrResource.getS("fichier requis"),
            TrResource.getS("La pr�sence du  fichier {0} est obligatoire.", _ent.getNom()) + CtuluLibString.LINE_SEP
                + TrResource.getS("Il est conseill� d'annuler la suppression du mot-cl�"), false);
      }
    }
  }

  @Override
  public void dicoParamsEntiteUpdated(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    if (proj_ == null) {
      return;
    }
    super.dicoParamsEntiteUpdated(_cas, _ent, _oldValue);
    updateLiqCurve(_ent);
  }

  @Override
  public void dicoParamsVersionChanged(final DicoParams _cas) {
  }

  @Override
  public void evolutionChanged(final EvolutionReguliereInterface _e) {
    if (proj_ == null) {
      return;
    }
    if (_e.getUsed() > 0) {
      modifyLiquidFile();
    }
  }

  @Override
  public void evolutionUsedChanged(final EvolutionReguliereInterface _e, final int _old, final int _new, final boolean _isAdjusting) {
    if (proj_ == null) {
      return;
    }
    if (!_isAdjusting) {
      modifyLiquidFile();
    }
  }

  @Override
  public void globalEvolutionUsedChanged() {
    if (proj_ == null) {
      return;
    }
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("INTERN CURVES USED STATE CHANGED");
    }
    getTProj().getTrTelemacParams().verifyLiquidBoundarFile();
  }

  @Override
  public void gridNodeXYChanged() {
    if (proj_ == null) {
      return;
    }
    modifyGeometryFile();
    if (proj_.getVisuFille() != null) {
      proj_.getVisuFille().updateIsoVariables();
    }
    fireGridDataChanged();
  }

  @Override
  public void nodalPropAdded(final H2dVariableType _t) {
    if (proj_ == null) {
      return;
    }
    modifyGeometryFile();
    if (proj_.getVisuFille() != null) {
      proj_.getVisuFille().updateIsoVariables();
    }
    fireGridDataChanged(_t);
  }

  @Override
  public void nodalPropChanged(final H2dVariableType _t) {
    if (proj_ == null) {
      return;
    }
    modifyGeometryFile();
    fireGridDataChanged(_t);
  }

  @Override
  public void nodalPropRemoved(final H2dVariableType _t) {
    if (proj_ == null) {
      return;
    }
    modifyGeometryFile();
    fireGridDataChanged(_t);
  }

  @Override
  public void seuilAdded() {
    modifyCulvertWeirFile();
    setParamsChanged();

  }

  @Override
  public void seuilChanged(final boolean _xyChanged) {
    CtuluLibMessage.info("weirs values changed");
    setParamsChanged();
    modifyCulvertWeirFile();
  }

  @Override
  public void seuilRemoved() {
    setParamsChanged();
    modifyCulvertWeirFile();
  }

  @Override
  public void siAdded(final H2dVariableType _var) {
    setParamsChanged();
    modifyRepriseFile();
    fireGridDataChanged(_var);
  }

  @Override
  public void siChanged(final H2dVariableType _var) {
    modifyRepriseFile();
    fireGridDataChanged(_var);
  }

  @Override
  public void siRemoved(final H2dVariableType _var) {
    setParamsChanged();
    modifyRepriseFile();
    fireGridDataChanged(_var);
  }

  @Override
  public void sourcesChanged(final DicoEntite _v) {
    setParamsChanged();

  }
}