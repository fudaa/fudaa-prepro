package org.fudaa.fudaa.tr.common;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.mesure.EvolutionFileFormat;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.telemac.io.ScopeGENEFileFormat;
import org.fudaa.dodico.telemac.io.ScopeSFileFormat;
import org.fudaa.dodico.telemac.io.ScopeStructure;
import org.fudaa.dodico.telemac.io.ScopeTFileFormat;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheTreeModel;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;
import org.fudaa.fudaa.ressource.FudaaResource;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/**
 * Wizard qui se charge d'importer des courbes au format scope dans le post. Wizard which imports plots in the scope format in the post Pour
 * personnaliser le format final du graphe et son affichage, il faut surcharger la methode To customize the final format of the graph and its posting,
 * we must overload the method createGraphImported()
 *
 * @author Adrien Hadoux
 */
public class TrCourbeWizardImportScope extends BuWizardTask {
  //donnees haut niveau
  //high level data
  FudaaCommonImplementation impl_;
  TrCourbeImporter.Target target_;
  /**
   * type de fomat: 0 => scops 1 =>scopT 2=>scopgene
   */
  int typeFormat_;
  File fileChoosen_ = null;
  protected ScopeStructure data_;
  EGGraphe result_;
  //choix du fichier
  //choice of file
  JPanel panelFichier_;
  BuTextField filePath_;
  //choix parametres s t ou gene
  //choice parameters s, t or gene
  JPanel panelParametres_;
  //choix donnees S et T
  //choice datas S and T
  BuTextField plageDeb_ = new BuTextField(5);
  BuTextField plageFin_ = new BuTextField(5);
  double xMin;
  double xMax;
  JList listVariablesST_;
  /**
   * Boolean qui implique si on rejoue et dans ce cas si on ecrase les datas. Boolean which implies if we replay and in this case we squeeze the
   * datas.
   */
  boolean replayData_;

  public TrCourbeWizardImportScope(FudaaCommonImplementation impl, TrCourbeImporter.Target target) {
    this(impl, target, false, null);
  }

  public TrCourbeWizardImportScope(FudaaCommonImplementation impl, TrCourbeImporter.Target target, boolean replayData, File fileToReload) {
    super();
    target_ = target;
    impl_ = impl;
    replayData_ = replayData;
    fileChoosen_ = fileToReload;

    //-- on avance � l'�tape 2 directement --//
    //--we go to 2nd step directly
    if (replayData_ || fileChoosen_ != null) {
      panelFichier_ = buildFilePanel();
      this.filePath_.setText("" + fileChoosen_.getAbsolutePath());
      typeFormat_ = devineTypeWithExtension(fileChoosen_);
      if (typeFormat_ == -1) {
        typeFormat_ = selectType("Choix du format", impl.getFrame());
      }
      try {
        computeData(impl.getMainProgression());
        current_ = 1;
      } catch (Exception e) {
        FuLog.error(e);
        impl_
            .error("Impossible de charger le fichier avec l'algorithme " + getType(typeFormat_) + ".\n Le format n'est pas compatible. Veuillez essayer avec une autre extension.");
        //Impossible to charge the list with the algorithm... the format is niot compatible. Try with another extension
      }
    }
  }

  public int devineTypeWithExtension(File _file) {
    BuFileFilter filterScopeS = ScopeSFileFormat.getInstance().createFileFilter();
    BuFileFilter filterScopeT = ScopeTFileFormat.getInstance().createFileFilter();
    BuFileFilter filterScopeGENE = ScopeGENEFileFormat.getInstance().createFileFilter();

    if (filterScopeS.accept(_file)) {
      return 0;
    }
    if (filterScopeT.accept(_file)) {
      return 1;
    }
    if (filterScopeGENE.accept(_file)) {
      return 2;
    }
    return -1;
  }

  @Override
  public JComponent getStepComponent() {
    switch (current_) {
      case 0: {
        return getStepComponentFile();
      }
      case 1: {
        return getStepComponentImportation();
      }
      default:
        return null;
    }
  }

  private JComponent getStepComponentFile() {
    if (panelFichier_ == null) {
      panelFichier_ = buildFilePanel();
    }
    return panelFichier_;
  }

  private JComponent getStepComponentImportation() {
    if (data_ == null) {
      return new JLabel(TrResource.getS("Erreur. Veuillez choisir un fichier valide"));
      //Choose a valid file
    }

    return buildPanelParametresImportation();
  }

  @Override
  public int getStepCount() {
    return 2;
  }

  @Override
  public String getStepText() {
    String r = null;

    switch (current_) {
      case 0:

        break;
      case 1:

        break;
    }
    return r;
  }

  ScopeStructure.SorT dataST() {
    return (ScopeStructure.SorT) data_;
  }

  ScopeStructure.Gene dataG() {
    return (ScopeStructure.Gene) data_;
  }

  @Override
  public String getStepTitle() {
    String r = null;

    switch (current_) {
      case 0:
        r = TrResource.getS("Choisir le fichier");
        break;
      case 1:
        r = TrResource.getS("Param�tres de cr�ation");
        break;
    }
    return r;
  }

  private BuRadioButton typeScopeS = new BuRadioButton("Scop S");
  private BuRadioButton typeScopeT = new BuRadioButton("Scop T");
  private BuRadioButton typeScopeGENE = new BuRadioButton("Scop GENE");
  private int typeRadioChoisi_ = -1;

  /**
   * construit le panel de selection du fichier
   */
  JPanel buildFilePanel() {

    BuPanel conteneur = new BuPanel(new BorderLayout());
    final JLabel labelAlerte = new JLabel("");
    JPanel content = new JPanel(new FlowLayout(FlowLayout.CENTER));
    content.add(new JLabel(TrResource.getS("Choisir le fichier:")));
    filePath_ = new BuTextField(20);
    filePath_.setEnabled(false);
    content.add(filePath_);
    BuButton parcourir = new BuButton(TrResource.getS("Parcourir..."));
    parcourir.addActionListener(new ParcourirActionListener(labelAlerte));
    content.add(parcourir);

    conteneur.add(content, BorderLayout.NORTH);

    BuPanel boutonsPanel = new BuPanel(new BuGridLayout(1, 5, 5));
    boutonsPanel.add(labelAlerte);
    boutonsPanel.add(typeScopeS);
    boutonsPanel.add(typeScopeT);
    boutonsPanel.add(typeScopeGENE);

    typeScopeS.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        typeRadioChoisi_ = 0;
        labelAlerte.setText(TrLib.getString("Scope S choisi"));
      }
    });
    typeScopeT.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        typeRadioChoisi_ = 1;
        labelAlerte.setText(TrLib.getString("Scope T choisi"));
      }
    });
    typeScopeGENE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        typeRadioChoisi_ = 2;
        labelAlerte.setText(TrLib.getString("Scope GENE choisi"));
      }
    });
    ButtonGroup group = new ButtonGroup();
    group.add(typeScopeS);
    group.add(typeScopeT);
    group.add(typeScopeGENE);

    conteneur.add(boutonsPanel, BorderLayout.CENTER);

    return conteneur;
  }

  public String getType(int i) {
    switch (i) {
      case 0:
        return "Scope S";
      case 1:
        return "Scope T";
      case 2:
        return "Scope GENE";
      default:
        return "";
    }
  }

  public static int selectType(final String _title, final Component _parent) {

    final Object[] _init = new String[]{"Scope S", "Scope T", "ScopeGENE"};
    final BuList l = CtuluLibSwing.createBuList(_init);
    final CtuluDialogPanel pn = new CtuluDialogPanel(false);
    pn.setLayout(new BuBorderLayout());
    pn.setBorder(BuBorders.EMPTY3333);
    pn.add(new BuLabel(TrLib.getString("S�lectionner le fichier r�sultat")), BuBorderLayout.NORTH);
    pn.add(new BuScrollPane(l));
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_parent, _title))) {
      return l.getSelectedIndex();
    }
    return -1;
  }

  /**
   * Retourne les formats de fichiers disponibles Return the formats of free files
   */
  public static FileFormat[] getFileFormatForImport() {
    final ArrayList<FileFormatUnique> r = new ArrayList<>(10);
    r.add(ScopeSFileFormat.getInstance());
    r.add(ScopeTFileFormat.getInstance());
    r.add(ScopeGENEFileFormat.getInstance());
    final FileFormat[] rf = new FileFormat[r.size()];
    r.toArray(rf);
    return rf;
  }

  /**
   * Methode iumportante d'importation des datas en fonction du fichier choisi et du type. Remplit la structure scope Important method of importation
   * of the datas function of the chosen and the type file Edits the scope structure
   */
  private void importData() {
    new CtuluTaskOperationGUI(impl_, FudaaLib.getS("Importation graphe")) {
      @Override
      public void act() {

        computeData(getMainStateReceiver());
      }
    }.start();
  }

  /**
   * Methode qui realise le calcul de la structure. Method which realizes the computation of the structure
   */
  private void computeData(ProgressionInterface prog) {

    switch (typeFormat_) {
      case 0:
        //cas s
        data_ = lectureScopeS(fileChoosen_, prog);
        break;
      case 1:
        //cas scopT
        data_ = lectureScopeT(fileChoosen_, prog);
        break;
      case 2:
        //cas scopGENE
        data_ = lectureScopeGENE(fileChoosen_, prog);
        break;
    }
  }

  private ScopeStructure.SorT lectureScopeS(final File _f, ProgressionInterface prog) {
    final CtuluIOOperationSynthese op = ScopeSFileFormat.getInstance().getLastVersionInstance(null).read(_f, prog);
    final ScopeStructure.SorT struct = (ScopeStructure.SorT) op.getSource();
    return struct;
  }

  private ScopeStructure.SorT lectureScopeT(final File _f, ProgressionInterface prog) {
    final CtuluIOOperationSynthese op = ScopeTFileFormat.getInstance().getLastVersionInstance(null).read(_f, prog);
    final ScopeStructure.SorT struct = (ScopeStructure.SorT) op.getSource();
    return struct;
  }

  private ScopeStructure.Gene lectureScopeGENE(final File _f, ProgressionInterface prog) {
    final CtuluIOOperationSynthese op = ScopeGENEFileFormat.getInstance().getLastVersionInstance(null).read(_f, prog);
    final ScopeStructure.Gene struct = (ScopeStructure.Gene) op.getSource();
    return struct;
  }

  /**
   * Construit le panel de choix des donn�es a importer pour les formats s et t Build the panel of choice of datas to import for the formats sand t
   */
  JPanel buildPanelScopeSorT() {
    JPanel content = new JPanel(new BorderLayout());

    String[] listeVar = new String[dataST().getNbVariables() - 1];

    //on ne prends pas les x
    //We don't take the x
    for (int i = 1; i < dataST().getNbVariables(); i++) {
      listeVar[i - 1] = dataST().getVariable(i);
    }

    listVariablesST_ = new JList(listeVar);
    listVariablesST_.setSelectedIndex(0);
    content.add(listVariablesST_, BorderLayout.CENTER);
    listVariablesST_.setBorder(BorderFactory.createTitledBorder(TrResource.getS("S�lection des variables"))); //Selection of the variables
    plageDeb_.setCharValidator(BuCharValidator.FLOAT);
    plageDeb_.setStringValidator(BuStringValidator.FLOAT);
    plageDeb_.setValueValidator(BuValueValidator.FLOAT);
    plageFin_.setCharValidator(BuCharValidator.FLOAT);
    plageFin_.setStringValidator(BuStringValidator.FLOAT);
    plageFin_.setValueValidator(BuValueValidator.FLOAT);

    //-- on init les plages --//
    //	We init the ranges
    xMin = dataST().getXMin();
    plageDeb_.setText("" + xMin);
    xMax = dataST().getXMax();
    plageFin_.setText("" + xMax);

    JPanel plages = new JPanel(new FlowLayout(FlowLayout.CENTER));
    plages.add(new JLabel(TrResource.getS("Choisir la plage des abscisses")));
    plages.add(plageDeb_);
    plages.add(new JLabel("-"));
    plages.add(plageFin_);
    plages.add(new JLabel("(Min: " + xMin + ", Max: " + xMax + ")"));
    content.add(plages, BorderLayout.SOUTH);

    return content;
  }

  JComboBox comBoChoixTypeGraphe_ = new JComboBox(new String[]{TrResource.getS("Profil Spatial"), TrResource.getS("Evolution temporelle"), TrResource.getS("Corr�lation")});
  private JList listSeparator_, listSeparatorDebPlage_, listSeparatorFinPlage_, listSeparatorCorrel1_, listSeparatorCorrel2_;
  private JList listVariablesGENE_, listVariablesGENE2_, listVariablesGENECorrlX_, listVariablesGENECorrlY_;
  private CardLayout layoutParamGene = new CardLayout();
  private BuTextField fieldX = new BuTextField(10);
  /**
   * Liste des couples a importer. Ces couples utilisent la classe coupleVarSepartor List of the couples to import these couples use the
   * coupleVarSepartor class
   */
  private JList listCoupleSpatiauxGENE_;
  private DefaultListModel modelCoupleSpatiauxGENE_;

  public class CoupleVarSepartor {
    String nomVar, nomSeparator;
    ArrayList<Double> listX;
    ArrayList<Double> listY;

    public CoupleVarSepartor(String nomVar, String nomSeparator,
                             ArrayList<Double> listX, ArrayList<Double> listY) {
      this.nomVar = nomVar;
      this.nomSeparator = nomSeparator;
      this.listX = listX;
      this.listY = listY;
    }

    @Override
    public String toString() {
      // TODO Auto-generated method stub
      return "(" + nomVar + "/" + nomSeparator + ")";
    }

    public EvolutionReguliere transform(boolean _time) {
      double[] tabX = new double[listX.size()];
      double[] tabY = new double[listY.size()];
      for (int j = 0; j < listX.size(); j++) {
        tabX[j] = listX.get(j);
        tabY[j] = listY.get(j);
      }
      EvolutionReguliere evol = new EvolutionReguliere(tabX, tabY, _time);
      evol.setNom(nomVar + " " + nomSeparator);
      evol.isScope_ = true;

      evol.titreAxeX_ = nomVar.replace("  ", "");
      evol.uniteAxeX_ = null;
      evol.titreAxeY_ = "T";
      evol.uniteAxeY_ = TrLib.getString("secondes");

      //-- ajout des infos specifiques --//
      //add of spcific news
      Map infos = evol.infos_;
      infos.put(TrLib.getString("Type"), TrLib.getString("Profil spatial scopGene"));
      infos.put(TrLib.getString("Fichier scopGENE"), fileChoosen_.getName());
      infos.put(ScopeStructure.NOM_FICHIER, fileChoosen_.getAbsolutePath());
      infos.put(TrLib.getString("Variable"), nomVar.replace("  ", ""));
      infos.put(TrLib.getString("Plage de valeurs"), nomSeparator.replace("  ", ""));

      return evol;
    }
  }

  /**
   * donne la description du fonctionnement de l'import gene specifique give the description of the working of the import gene specific
   */
  public String getDescriptionImportGENE() {
    switch (comBoChoixTypeGraphe_.getSelectedIndex()) {
      case 0:
        return "<html><body>" + TrResource.getS(
            "S�lectionnez des couples variable/valeurs num�riques de s�parateur. <br /> Cliquez sur ajouter pour le prendre en compte. On peut ajouter plusieurs couples.<br /> Chaque couple g�n�rera une courbe en prenant les valeurs num�riques du s�parateur choisi.") + "</body></html> ";
      case 1:
        return "<html><body>" + TrResource.getS(
            "S�lectionner toutes les variables voulues ainsi q'une plage de d�part et une plage de fin.<br /> Cela g�n�rera une courbe par variables. Chaque courbe prendra pour Y une interpolation par rapport au x choisi. <br />Il y aura donc autant de couples (x,y) que de s�parateurs dans l'intervalle d�but/fin choisi.") + "</body></html> ";
      case 2:
        return TrResource.getS("S�lectionner une variable et une plage de valeur num�rique pour repr�senter les valeurs de X. Idem pour Y.");
    }
    return "";
  }

  private JCheckBox choixNuagePoints_;

  /**
   * Creation du panel scopGene pour choisir ses parametres
   */
  JPanel buildPanelScopeGENE() {
    final JPanel content2 = new JPanel(new BorderLayout());
    content2.add(comBoChoixTypeGraphe_, BorderLayout.NORTH);
    final JPanel content = new JPanel(new BorderLayout());
    content2.add(content, BorderLayout.CENTER);
    final JLabel description = new JLabel(getDescriptionImportGENE());

    content.add(description, BorderLayout.NORTH);
    final JPanel container = new JPanel(layoutParamGene);
    content.add(container, BorderLayout.CENTER);
    comBoChoixTypeGraphe_.setAction(new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        String val = (String) comBoChoixTypeGraphe_.getSelectedItem();
        layoutParamGene.show(container, val);
        //layoutParamGene.next(container);
        //				content.revalidate();
        //				container.revalidate();
        description.setText(getDescriptionImportGENE());
      }
    });

    //-- ACHTUNG!!! DANS LES SPEC ON VEUT POUVOIR SAISIR DES COUPLES SEPARATOR/VARIABLES --//
    //-- creation de la liste des variables --//
    //--Beware!!! In the spec we want to power over separated/variabbles couples            
    //--creation of the list of variables 				
    String[] listeVar = new String[dataG().getNbVariables() - 1];
    //on ne prends pas les x
    //we don't take the x
    for (int i = 1; i < dataG().getNbVariables(); i++) {
      listeVar[i - 1] = dataG().getVariable(i);
    }
    listVariablesGENE_ = new JList(listeVar);
    listVariablesGENE_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    //-- creation de la liste des separators --//
    //--creation of the liste of eparators
    listeVar = new String[dataG().getNbSeparator()];
    //on ne prends pas les x
    //we don't take the x
    for (int i = 0; i < dataG().getNbSeparator(); i++) {
      listeVar[i] = dataG().getSeparator(i);
    }
    listSeparator_ = new JList(listeVar);
    listSeparator_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    //-- panel reserv� aux profils spatiaux --//
    //--panel reserved to the spatial profiles
    JPanel paramProfils = new JPanel(new BorderLayout());
    container.add(paramProfils, TrResource.getS("Profil Spatial"));
    JPanel choixVar = new JPanel(new GridLayout(1, 2));
    JScrollPane pane = new JScrollPane(listVariablesGENE_);
    pane.setBorder(BorderFactory.createTitledBorder(TrResource.getS("S�lection des variables"))); //Selection of variables	
    choixVar.add(pane);
    pane = new JScrollPane(listSeparator_);
    pane.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Valeurs num�riques du s�parateur de bloc"))); //Numerical values of the bloc seperator
    choixVar.add(pane);
    paramProfils.add(choixVar, BorderLayout.NORTH);
    //--panel du milieu: liste de couples VAR/SEPARATOR --//
    //--panel of the middle list of couples VAR/SEPARATOR
    modelCoupleSpatiauxGENE_ = new DefaultListModel();
    listCoupleSpatiauxGENE_ = new JList(modelCoupleSpatiauxGENE_);
    pane = new JScrollPane(listCoupleSpatiauxGENE_);
    pane.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Liste des couples variable/separateur"))); //liste of variables couples/seperator
    paramProfils.add(pane, BorderLayout.CENTER);
    //-- boutons du bas d'ajout/supression dans la liste --//
    //--buttons from bottom added/suppression in the list
    BuButton ajouter = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ajouter"), TrResource.getS("Ajouter"));
    ajouter.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (listSeparator_.getSelectedIndex() == -1) {
          impl_.error(TrResource.getS("Il faut choisir une valeur num�rique de s�parateur.")); //we must choose a numerical value of seperator	
        } else if (listVariablesGENE_.getSelectedIndex() == -1) {
          impl_.error(TrResource.getS("Il faut choisir une variable."));
        } else {
          //-- creation du couple choisi en recherchant les bonnes donn�es dans la structure --//
          //--creation of chosen couple in finding the good datas in the structure
          int indiceVarChoisie = listVariablesGENE_.getSelectedIndex() + 1;
          String varChoisie = dataG().getVariable(indiceVarChoisie);
          String separatorChoisi = dataG().getSeparator(listSeparator_.getSelectedIndex());
          CoupleVarSepartor newCouple = new CoupleVarSepartor(varChoisie, separatorChoisi, dataG().getAllX(separatorChoisi),
              dataG().getListValueForVariableForSeparator(separatorChoisi, indiceVarChoisie));
          modelCoupleSpatiauxGENE_.addElement(newCouple);
        }
      }
    });
    BuButton supprimer = new BuButton(FudaaResource.FUDAA.getIcon("crystal_enlever"), TrResource.getS("Enlever"));
    supprimer.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (listCoupleSpatiauxGENE_.getSelectedIndex() == -1) {
          impl_.error(TrResource.getS("Il faut choisir une valeur du couple � supprimer")); //We must a value of the couple to suppress
        } else {
          modelCoupleSpatiauxGENE_.remove(listCoupleSpatiauxGENE_.getSelectedIndex());
        }
      }
    });
    //ajout des boutons
    //Adds the buttons
    JPanel panelActions = new JPanel(new FlowLayout());
    panelActions.add(ajouter);
    panelActions.add(supprimer);
    paramProfils.add(panelActions, BorderLayout.SOUTH);

    //-- panel reserv� aux courbes temporelles --//
    //panel reserved to temporal plots
    JPanel paramTempo = new JPanel(new BorderLayout());
    container.add(paramTempo, TrResource.getS("Evolution temporelle"));
    listeVar = new String[dataG().getNbVariables() - 1];
    for (int i = 1; i < dataG().getNbVariables(); i++) {
      listeVar[i - 1] = dataG().getVariable(i);
    }
    listVariablesGENE2_ = new JList(listeVar);
    listVariablesGENE2_.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    listeVar = new String[dataG().getNbSeparator()];
    for (int i = 0; i < dataG().getNbSeparator(); i++) {
      listeVar[i] = dataG().getSeparator(i);
    }
    listSeparatorDebPlage_ = new JList(listeVar);
    listSeparatorDebPlage_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    listSeparatorDebPlage_.setSelectedIndex(0);
    listSeparatorFinPlage_ = new JList(listeVar);
    listSeparatorFinPlage_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    listSeparatorFinPlage_.setSelectedIndex(listSeparatorDebPlage_.getModel().getSize() - 1);

    JPanel listData = new JPanel(new GridLayout(1, 2));
    JPanel listData2 = new JPanel(new GridLayout(2, 1));
    pane = new JScrollPane(listVariablesGENE2_);
    pane.setBorder(BorderFactory.createTitledBorder(TrResource.getS("S�lection des variables")));

    listData.add(pane);
    listData.add(listData2);
    pane = new JScrollPane(listSeparatorDebPlage_);
    pane.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Plage d�part du s�parateur de bloc")));
    listData2.add(pane);
    pane = new JScrollPane(listSeparatorFinPlage_);
    pane.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Plage fin du s�parateur de bloc")));
    listData2.add(pane);
    paramTempo.add(listData, BorderLayout.CENTER);

    //textField contenant le x choisi
    xMax = dataG().getXMax();
    xMin = dataG().getXMin();
    JPanel panelInfo = new JPanel();
    panelInfo.add(new JLabel(TrResource.getS("Veuillez saisir le X:") + xMin + "<=X<=" + xMax));
    fieldX.setCharValidator(BuCharValidator.FLOAT);
    fieldX.setStringValidator(BuStringValidator.FLOAT);
    fieldX.setValueValidator(BuValueValidator.FLOAT);
    panelInfo.add(fieldX);
    paramTempo.add(panelInfo, BorderLayout.SOUTH);
    fieldX.setText("" + ((xMax + xMin) / 2));

    //-- panel reserv�s au corr�lations --//
    //--panel reserved to the correlation--//
    JPanel panelCorrelation = new JPanel(new BorderLayout());
    JPanel paramCorrel = new JPanel(new GridLayout(2, 2));
    panelCorrelation.add(paramCorrel, BorderLayout.CENTER);
    container.add(panelCorrelation, TrResource.getS("Corr�lation"));

    listeVar = new String[dataG().getNbVariables() - 1];
    for (int i = 1; i < dataG().getNbVariables(); i++) {
      listeVar[i - 1] = dataG().getVariable(i);
    }
    listVariablesGENECorrlX_ = new JList(listeVar);
    listVariablesGENECorrlX_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    listVariablesGENECorrlY_ = new JList(listeVar);
    listVariablesGENECorrlY_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    listeVar = new String[dataG().getNbSeparator()];
    for (int i = 0; i < dataG().getNbSeparator(); i++) {
      listeVar[i] = dataG().getSeparator(i);
    }
    listSeparatorCorrel1_ = new JList(listeVar);
    listSeparatorCorrel1_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    listSeparatorCorrel1_.setSelectedIndex(0);

    listSeparatorCorrel2_ = new JList(listeVar);
    listSeparatorCorrel2_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    listSeparatorCorrel2_.setSelectedIndex(0);

    pane = new JScrollPane(listVariablesGENECorrlX_);
    pane.setBorder(BorderFactory.createTitledBorder(TrResource.getS("S�lection la variable pour l'axe X"))); //Selection of the variable for the X axis
    paramCorrel.add(pane);
    pane = new JScrollPane(listSeparatorCorrel1_);
    pane.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Plage associ�e � la variable X"))); //Range associated to the variable X
    paramCorrel.add(pane);
    pane = new JScrollPane(listVariablesGENECorrlY_);
    pane.setBorder(BorderFactory.createTitledBorder(TrResource.getS("S�lection la variable pour l'axe Y"))); //Selection pf the variable for the Y axis
    paramCorrel.add(pane);
    pane = new JScrollPane(listSeparatorCorrel2_);
    pane.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Plage associ�e � la variable Y"))); //Range associated to the variable Y
    paramCorrel.add(pane);

    choixNuagePoints_ = new JCheckBox(EbliResource.EBLI.getString("Nuage de points"));
    panelCorrelation.add(choixNuagePoints_, BorderLayout.SOUTH);

    return content2;
  }

  JPanel buildPanelParametresImportation() {
    switch (typeFormat_) {
      case 0:
        //cas s
        return buildPanelScopeSorT();
      case 1:
        //cas scopT
        return buildPanelScopeSorT();
      case 2:
        //cas scopGENE
        return buildPanelScopeGENE();
      default:
        return null;
    }
  }

  @Override
  public String getTaskTitle() {
    return TrResource.getS("Importation depuis les formats Scop S, T et GENE");  //Importation from the Scop S,T and Gene formats
  }

  /**
   * Methode utilisee pour disabler ou non les boutons suivants Method used to disable or not the following buttons
   */
  @Override
  public int getStepDisabledButtons() {
    int r = super.getStepDisabledButtons();
    if (current_ == 1) {

      //cas scope s ou t 
      if (typeFormat_ == 1 || typeFormat_ == 0) {
        if (data_ == null) {
          r |= BuButtonPanel.TERMINER;
        } else if (plageDeb_.getText().equals("") || plageFin_.getText().equals("")) {
          r |= BuButtonPanel.TERMINER;
        } else {
          //verifier que les plages ne sont pas depassees
          //verify than ranges are not out of date
          double valDeb = Double.parseDouble(plageDeb_.getText());
          double valFin = Double.parseDouble(plageFin_.getText());
          if (valDeb < xMin || xMax < valFin) {
            r |= BuButtonPanel.TERMINER;
          }
        }
      } else {
        //cas scope gene
      }
    }
    return r;
  }

  @Override
  public void doTask() {
    done_ = true;
    new CtuluTaskOperationGUI(impl_, FudaaLib.getS("Importation graphe")) {
      @Override
      public void act() {

        /*
          Cas de rejoue des donn�es, on supprime le contenu du graphe Case of adding of datas, we delate the content of the graph
         */
        if (replayData_) {
          if (target_ instanceof EGGrapheTreeModel) {
            java.util.List<EGCourbeChild> listecb = ((EGGrapheTreeModel) target_).getAllCourbesChild();

            ((EGGrapheTreeModel) target_).removeCurves(listecb.toArray(new EGCourbeChild[listecb.size()]), new CtuluCommandManager());
          }
        }

        final ProgressionInterface prog = getMainStateReceiver();
        //mise a jour de la structure
        //update of the structure
        impl_.setMainProgression(10);
        impl_.setMainMessage(CtuluLib.getS("Structure modification"));
        prog.setProgression(10);
        prog.setDesc(CtuluLib.getS("Structure modification"));
        if (typeFormat_ <= 1) {
          dataST().restreindreStructure(listVariablesST_.getSelectedIndices(), Double.parseDouble(plageDeb_.getText()), Double.parseDouble(plageFin_.getText()));
          impl_.setMainProgression(30);
          impl_.setMainMessage(CtuluLib.getS("Cr�ation du graphe"));
          TrCourbeImporterScope.createGraphe(fileChoosen_, target_, data_, null, prog, false);
        } else {
          //-- format scopeGen, selon le type de format g�n�r�--//
          //format scopeGen, according the type of format generated
          if (comBoChoixTypeGraphe_.getSelectedIndex() == 0) {
            //profil spatial, on recupere la liste des couples solutions
            //spatial profil we recuperate the list of solutions couples
            EvolutionReguliere[] tabEvol = new EvolutionReguliere[modelCoupleSpatiauxGENE_.getSize()];
            for (int i = 0; i < modelCoupleSpatiauxGENE_.getSize(); i++) {
              tabEvol[i] = ((CoupleVarSepartor) modelCoupleSpatiauxGENE_.getElementAt(i)).transform(false);
            }

            impl_.setMainProgression(30);
            impl_.setMainMessage(CtuluLib.getS("Cr�ation du graphe"));
            TrCourbeImporterScope.createGraphe(target_, tabEvol, null, prog, false);
          } else if (comBoChoixTypeGraphe_.getSelectedIndex() == 1) {
            //evol temporelle
            int debSeparator = listSeparatorDebPlage_.getSelectedIndex();
            int finSeparator = listSeparatorFinPlage_.getSelectedIndex();
            int[] selectedVariables = listVariablesGENE2_.getSelectedIndices();
            double xchoosen = Double.parseDouble(fieldX.getText());
            dataG().restreindreStructure(debSeparator, finSeparator, selectedVariables, xchoosen);

            impl_.setMainProgression(30);
            impl_.setMainMessage(CtuluLib.getS("Cr�ation du graphe"));
            //on cree les evol reguliere SPECIFIQUES pour ces courbes.
            //we create the regular specific for those plots
            TrCourbeImporterScope.createGraphe(target_, dataG().returnEvolRegulierePourTemporelles(xchoosen, fileChoosen_), null, prog, false);
          } else {
            //correlation
            impl_.setMainProgression(30);
            impl_.setMainMessage(CtuluLib.getS("Cr�ation du graphe"));
            //on cree les evol reguliere SPECIFIQUES pour ces courbes.
            //we create the regular specific for those plots
            int varX = listVariablesGENECorrlX_.getSelectedIndex();
            int varY = listVariablesGENECorrlY_.getSelectedIndex();

            int plageX = listSeparatorCorrel1_.getSelectedIndex();
            int plageY = listSeparatorCorrel2_.getSelectedIndex();

            //-- recuperation du choix nuage de points ou non
            //--Recuperation of the choice scatter of points  or not
            boolean nuagePoints = choixNuagePoints_.isSelected();

            TrCourbeImporterScope.createGraphe(target_, dataG().returnEvolReguliereCorrelation(varX, varY, plageX, plageY, fileChoosen_), null, prog, nuagePoints);
          }
        }

        //-- mise en forme du resultata g�n�r� --//		    
        //make in shape the general result 
        createGraphImported();
      }
    }.start();
  }

  /**
   * Methode a surcharger. Method to surcharge Appel�e a la toute fin, gere le resultat Called to the whole end, managed the result
   */
  public void createGraphImported() {
    //placer le graphe ou vous voulez...
    //place the graph where you want
  }

  private class ParcourirActionListener implements ActionListener {
    private final JLabel labelAlerte;

    public ParcourirActionListener(JLabel labelAlerte) {
      this.labelAlerte = labelAlerte;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
      final FileFormat[] ft = getFileFormatForImport();
      Arrays.sort(ft);
      final BuFileFilter[] filters = FileFormat.createFilters(ft);
      final CtuluFileChooser fileChooser = FudaaGuiLib.getFileChooser(BuResource.BU.getString("Importer"), filters,
          null);
      fileChooser.setAcceptAllFileFilterUsed(true);
      for (int i = filters.length - 1; i >= 0; i--) {
        if (ft[i].getID() == EvolutionFileFormat.getInstance().getID()) {
          fileChooser.setFileFilter(filters[i]);
          break;
        }
      }
      final File initFile = FudaaGuiLib.chooseFile(CtuluLibSwing.getFrameAncestor(impl_.getParentComponent()), true, fileChooser);
      if (initFile == null) {
        return;
      }
      filePath_.setText(initFile.getAbsolutePath());

      final FileFilter filter = fileChooser.getFileFilter();
      final int i = CtuluLibArray.getIndex(filter, filters);
      typeFormat_ = i;

      //-- si -1, c'est qu'on a choisi le filtre all, il faut alors le determiner --//
      //-- if -1 it is that we choose the filter all. we must therefore determine it
      typeFormat_ = devineTypeWithExtension(initFile);

      //-- si le format vaut toujours -1, il faut pr�ciser avec le bouton radio --//
      //-- if the format is always worth -1, we must precise with the button radio--//
      if (typeFormat_ == -1) {

        if (typeRadioChoisi_ > -1) {
          if (impl_.question(TrLib.getString("format non reconnu"),
              TrResource.getS("Le format n'est pas reconnu, le fichier sera charg� avec l'algorithme {0}.\n continuer ?", getType(typeRadioChoisi_)))) {
            typeFormat_ = typeRadioChoisi_;
          } else {
            typeFormat_ = selectType(TrLib.getString("S�lection du type de format"), impl_.getFrame());
          }
        } else {
          labelAlerte.setText(TrLib.getString("Attention, format non reconnu, pr�cisez � l'aide des boutons radio"));
          typeFormat_ = selectType(TrLib.getString("S�lection du type de format"), impl_.getFrame());
        }
      }
      //-- on met a jour el bon bouton radio --//
      //--we update the good radio button
      switch (typeFormat_) {
        case 0:
          typeScopeS.setSelected(true);
          break;
        case 1:
          typeScopeT.setSelected(true);
          break;
        case 2:
          typeScopeGENE.setSelected(true);
          break;
        default:
          break;
      }

      fileChoosen_ = initFile;
      try {
        importData();
      } catch (Exception e) {
        FuLog.error(e);
        impl_.error(TrResource
            .getS("Impossible de charger le fichier avec l'algorithme {0}.\n Le format n'est pas compatible. Veuillez essayer avec une autre extension.", getType(typeFormat_)));
        //Impossible to load the file with the algorithm
        //the format is not compatible, try another extension
      }
    }
  }
}
