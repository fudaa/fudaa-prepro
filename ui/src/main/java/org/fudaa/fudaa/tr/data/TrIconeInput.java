/**
 * @creation 4 janv. 2005
 * @modification $Date: 2006-09-19 15:07:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.data;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import org.fudaa.ebli.trace.TraceIcon;

/**
 * @author Fred Deniger
 * @version $Id: TrIconeInput.java,v 1.5 2006-09-19 15:07:28 deniger Exp $
 */
public class TrIconeInput extends TraceIcon {

  @Override
  public void paintIconCentre(final Component _c, final Graphics _g, final int _x, final int _y) {
    Color old = null;
    if (getCouleur() != null) {
      old = _g.getColor();
      _g.setColor(getCouleur());
    }
    final int ray = getTaille() / 2;
    _g.drawOval(_x - ray, _y - ray, getTaille(), getTaille());
    final int inRay = ray / 2;
    _g.fillOval(_x - inRay, _y - inRay, ray, ray);
    _g.setColor(old);
  }
}