/**
 * @creation 26 mars 2004
 * @modification $Date: 2007-06-14 12:01:40 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post;

import com.memoire.fu.FuLog;
import java.util.Set;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.animation.EbliAnimatedInterface;
import org.fudaa.ebli.animation.EbliAnimationSourceInterface;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrFilleVisu;
import org.fudaa.fudaa.tr.post.data.TrPostDataListener;

/**
 * @author Fred Deniger
 * @version $Id: TrPostFille.java,v 1.23 2007-06-14 12:01:40 deniger Exp $
 */
public class TrPostFille extends TrFilleVisu implements TrPostDataListener, EbliAnimatedInterface {

  /**
   * @param _pn la panneau
   */
  public TrPostFille(final TrPostVisuPanel _pn) {
    super(_pn);
    setTitle(TrResource.getS("Vue 2D"));
    super.pn_.getArbreCalqueModel().setSelectionCalque(null);
  }

  @Override
  protected String buildShortHelp() {
    final CtuluHtmlWriter buf = new CtuluHtmlWriter();
    buf.close(buf.para(), TrResource.getS("Fen�tre de visualisation des r�sultats")).h3(
        TrResource.getS("Documents associ�s"));
    buf.addTag("lu");
    final String li = "li";
    buf.close(buf.addTag(li), getImpl().buildLink(TrResource.getS("pr�sentation du post-traitement"), "post-main"))
        .close(buf.addTag(li), getImpl().buildLink(TrResource.getS("Comment utiliser la vue 2D"), "common-layers"))
        .close(buf.addTag(li),
            getImpl().buildLink(TrResource.getS("Les actions sp�cifiques au post-traitement"), "post-vue2d")).close(
            buf.addTag(li), getImpl().buildLink(TrResource.getS("Lancer et sauvegarder une animation"), "post-anim"));
    return buf.toString();
  }

  @Override
  public void dataAdded(final boolean _isFleche) {
    if (FuLog.isDebug()) {
      FuLog.debug("TRP:" + getClass().getName() + " dataAdded");
    }
    ((TrPostDataListener) pn_).dataAdded(_isFleche);

  }

  @Override
  public void dataChanged(final H2dVariableType _old, final H2dVariableType _new, final boolean _contentChanged,
      final boolean _isFleche, final Set _varDepending) {
    if (FuLog.isDebug()) {
      FuLog.debug("TRP:" + getClass().getName() + " dataChanged");
    }
    ((TrPostDataListener) pn_).dataChanged(_old, _new, _contentChanged, _isFleche, _varDepending);

  }

  @Override
  public void dataRemoved(final H2dVariableType[] _vars, final boolean _isFleche) {
    if (FuLog.isDebug()) {
      FuLog.debug("TRP:" + getClass().getName() + " dataRemoved");
    }
    ((TrPostDataListener) pn_).dataRemoved(_vars, _isFleche);

  }

  @Override
  public EbliAnimationSourceInterface getAnimationSrc() {
    return ((EbliAnimatedInterface) pn_).getAnimationSrc();
  }

  @Override
  public void startExport(final CtuluUI _impl) {
    ((TrPostVisuPanel) getVisuPanel()).startExport(_impl);
  }

}