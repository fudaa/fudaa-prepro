/**
 * @creation 2002-08-30
 * @modification $Date: 2006-12-05 10:18:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr;

import org.fudaa.ctulu.CtuluLibMessage;

/**
 * Classe de lancement de l'application Tr. Contient la methode main.
 * 
 * @version $Id: Tr.java,v 1.24 2006-12-05 10:18:20 deniger Exp $
 * @author Fred Deniger
 */
public final class Tr {
  private Tr() {
  }

  public static void main(final String[] _args) {
    if (!CtuluLibMessage.DEBUG) {
      System.setProperty("fulog.level", "warning");
    }
    TrInvocationHandler.initThreaExceptionHandler();
    TrLauncherDefault.init();
    TrLauncherDefault.launch(_args, null, new TrSupervisorImplementation());
    //    RepaintManager.setCurrentManager(new CheckThreadViolationRepaintManager());

  }
}