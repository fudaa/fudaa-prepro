package org.fudaa.fudaa.tr.post.dialogSpec;

import com.memoire.bu.*;
import org.locationtech.jts.geom.Coordinate;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISPrecision;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.ef.operation.EfIndexHelper;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.graphe.EbliWidgetCreatorGraphe;
import org.fudaa.ebli.visuallibrary.graphe.GrapheCellRenderer;
import org.fudaa.fudaa.meshviewer.export.MvExportChooseVarAndTime;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.*;
import org.jdesktop.swingx.JXTable;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.*;

/**
 * Wizard reserv� pour la creation de courbes temporelles.
 *
 * @author Adrien Hadoux
 */
public class TrPostWizardCourbeTemporelle extends BuWizardTask {
  public final static String idTemporel = TrResource.getS("Trace �volution temporelle");
  private final TrPostVisuPanel calque_;
  private final MvExportChooseVarAndTime chooserVarTime_;
  /**
   * les points selectionnes
   */
  // private int[] ptIdx_;
  BuCheckBox checkboxNewGraphe_ = new BuCheckBox(TrResource.TR.getString("Cr�er un nouveau graphe"));
  BuCheckBox traceOrigineDansVue2d_ = new BuCheckBox(
      TrResource.TR.getString("Cr�er calque avec points d'origine de la courbe"));
  BuLabel indicateurPoints_ = new BuLabel("");
  /**
   * model de la jlist des graphes possibles.
   */
  ArrayList<EGGraphe> listeGraphesChoisis_;
  ArrayList<EGGraphe> listeGraphesPossibles_;
  /**
   * la liste des points. Peut etre des points reels mais aussi interpol�s
   */
  @SuppressWarnings("unchecked")
  List listPointOrMeshes;
  JComponent panelSelectiongraphes = null;
  JComponent panelSelectionPoints = null;
  boolean isElement;
  TrPostSource source;
  BuTextField nomCalque_ = new BuTextField(10);
  TrEvolutionBuilderFromTree builderPointsCalque_;

  @SuppressWarnings("unchecked")
  public TrPostWizardCourbeTemporelle(final TrPostVisuPanel vue, TrPostSource source, H2dVariableType variableSelected,
                                      final MvExportChooseVarAndTime _chooserVarTime, final int[] _ptIdx, final TrPostInterpolatePoint _pt) {
    super();
    chooserVarTime_ = _chooserVarTime;
    calque_ = vue;

    assert source != null;
    assert variableSelected != null;
    this.source = source;
    isElement = source.isElementVar(variableSelected);
    if (isElement) {
      assert _pt != null;
    }
    listPointOrMeshes = new ArrayList();
    if (_ptIdx != null) {
      for (int i = 0; i < _ptIdx.length; i++) {
        listPointOrMeshes.add(_ptIdx[i]);
      }
    }

    if (_pt != null) {
      listPointOrMeshes.add(_pt);
    }
  }

  public double getX(int idx) {
    if (isElement) {
      return source.getGrid().getMoyCentreXElement(idx);
    }
    return source.getGrid().getPtX(idx);
  }

  public double getY(int idx) {
    if (isElement) {
      return source.getGrid().getMoyCentreXElement(idx);
    }
    return source.getGrid().getPtY(idx);
  }

  /**
   * construit l interface de choix des graphes.
   */
  public JComponent constructPanelSelectionGraphe() {

    // -- recherche des graphes possibles dans le layout --//
    final DefaultListModel modelGraphesPossibles = rechercheGrapheWidget();
    // -- creation graphique --//
    final DefaultListModel modelGraphesChoisis = new DefaultListModel();

    final JPanel content = new JPanel(new BorderLayout());
    final JList jlisteGraphesChoix = new JList(modelGraphesPossibles);
    jlisteGraphesChoix.setCellRenderer(new GrapheCellRenderer());
    jlisteGraphesChoix.setBorder(BorderFactory.createTitledBorder(TrResource.TR.getString("Graphes possibles")));
    final JList jlisteGraphesSelections = new JList(modelGraphesChoisis);
    jlisteGraphesSelections.setSize(250, 350);
    jlisteGraphesSelections.setBorder(BorderFactory.createTitledBorder(TrResource.TR.getString("Graphes choisis")));
    jlisteGraphesSelections.setCellRenderer(new GrapheCellRenderer());
    final JPanel panelControle = new JPanel(new FlowLayout(FlowLayout.CENTER));
    final JButton select = new JButton(TrResource.TR.getString("Ajouter"),
        EbliResource.EBLI.getIcon("crystal22_avancervite"));
    final JButton unselect = new JButton(TrResource.TR.getString("Enlever"),
        EbliResource.EBLI.getIcon("crystal22_reculervite"));
    select.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        if (jlisteGraphesChoix.getSelectedIndex() != -1) {
          final int indice = jlisteGraphesChoix.getSelectedIndex();
          final EGGraphe graphe = listeGraphesPossibles_.get(indice);
          final JLabel title = (JLabel) modelGraphesPossibles.getElementAt(indice);
          modelGraphesChoisis.addElement(title);
          listeGraphesChoisis_.add(graphe);
          modelGraphesPossibles.removeElementAt(indice);
          listeGraphesPossibles_.remove(indice);
        }
      }
    });
    unselect.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        if (jlisteGraphesSelections.getSelectedIndex() != -1) {
          final int indice = jlisteGraphesSelections.getSelectedIndex();
          final EGGraphe graphe = listeGraphesChoisis_.get(indice);
          final JLabel title = (JLabel) modelGraphesChoisis.getElementAt(indice);
          modelGraphesPossibles.addElement(title);
          listeGraphesPossibles_.add(graphe);
          modelGraphesChoisis.removeElementAt(indice);
          listeGraphesChoisis_.remove(indice);
        }
      }
    });

    final JPanel box = new JPanel(new GridLayout(1, 3));
    box.add(jlisteGraphesChoix);
    final Box boxButton = Box.createVerticalBox();
    JPanel panSelect = new JPanel(new FlowLayout(FlowLayout.CENTER));
    panSelect.add(select);
    boxButton.add(panSelect);
    boxButton.add(Box.createGlue());
    panSelect = new JPanel(new FlowLayout(FlowLayout.CENTER));
    panSelect.add(unselect);
    boxButton.add(panSelect);
    box.add(boxButton);
    box.add(jlisteGraphesSelections);
    content.add(box, BorderLayout.CENTER);
    content.add(panelControle, BorderLayout.SOUTH);

    // -- cas premier node --//
    if (listeGraphesPossibles_.size() == 0 && listeGraphesChoisis_.size() == 0) {
      checkboxNewGraphe_.setSelected(true);
      checkboxNewGraphe_.setEnabled(false);
      jlisteGraphesChoix.setEnabled(false);
      jlisteGraphesSelections.setEnabled(false);
      select.setEnabled(false);
      unselect.setEnabled(false);
    }

    // -- le panel avec les jlists et combobox --//
    final JPanel main = new JPanel(new BorderLayout());
    main.add(content, BorderLayout.CENTER);

    final JPanel bouttonCenter = new JPanel(new FlowLayout(FlowLayout.CENTER));
    bouttonCenter.add(checkboxNewGraphe_);
    main.add(bouttonCenter, BorderLayout.NORTH);

    panelControle.add(traceOrigineDansVue2d_);
    nomCalque_.setText("Origine �volution temporelle");
    panelControle.add(nomCalque_);
    traceOrigineDansVue2d_.setSelected(false);
    return main;
  }

  /**
   * Construit le panel qui s occupe de la gestion des points
   */
  public JComponent constructSelectionPoint() {
    // -- le table des points selectionnes --//
    final JSplitPane splitpanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

    final JPanel content = new JPanel(new BorderLayout());
    final ModelListePoints model = new ModelListePoints(indicateurPoints_);
    final JXTable tableau = new JXTable(model) {
      @Override
      public boolean editCellAt(int _row, int _column) {
        saisirPoints("" + getValueAt(_row, 1), "" + getValueAt(_row, 2), false, _row, model);
        return false;
      }

      @Override
      public boolean editCellAt(int _row, int _column, EventObject _e) {
        if (_e instanceof MouseEvent && ((MouseEvent) _e).getClickCount() == 2) {
          saisirPoints("" + getValueAt(_row, 1),
              "" + getValueAt(_row, 2), false, _row, model);
        }
        return false;
      }
    };
    content.add(new BuScrollPane(tableau), BorderLayout.CENTER);
    final JPanel control = new JPanel(new FlowLayout(FlowLayout.CENTER));
    if (!isElement) {
      content
          .add(
              new JLabel(
                  TrResource
                      .getS("Saisir des points en cliquant sur ajouter. Modifier des points en cliquant sur les valeurs puis modifier")),
              BorderLayout.NORTH);

      // -- ajout du jtext pour ajout uniquement d'un point interpol� --//

      // control.add(new JLabel(TrResource.getS("Ajouter un point:")));
      final JButton ajouter = new JButton(BuResource.BU.getString("Ajouter"), EbliResource.EBLI.getIcon("crystal_ajouter"));
      ajouter.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent _e) {
          saisirPoints(null, null, true, -1, model);
        }
      });

      control.add(ajouter);

      // control.add(new JLabel(TrResource.getS("Modifier un point:")));
      final JButton modifier = new JButton(BuResource.BU.getString("Modifier"), EbliResource.EBLI.getIcon("crystal_modifier"));
      modifier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent _e) {
          final int pointToModify = tableau.getSelectedRow();
          if (pointToModify != -1) {
            saisirPoints("" + model.getValueAt(pointToModify, 1),
                "" + model.getValueAt(pointToModify, 2), false, pointToModify, model);
          }
        }
      });

      control.add(modifier);
    }
    // control.add(new JLabel(TrResource.getS("Supprimer la s�lection:")));
    final JButton supprimer = new JButton(TrLib.getString("Supprimer"), EbliResource.EBLI.getIcon("crystal_detruire"));
    supprimer.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        final int[] listePointToDelete = tableau.getSelectedRows();
        if (listePointToDelete != null) {
          int nbLignesSupprimees = 0;
          for (int i = 0; i < listePointToDelete.length; i++) {
            listPointOrMeshes.remove(listePointToDelete[i] - (nbLignesSupprimees++));
          }
          model.fireTableDataChanged();
          indicateurPoints_.setText(TrResource.getS("Nombre de points: {0}", Integer.toString(listPointOrMeshes.size())));
          // if (listePoints_.size() == 0) pointsSaisis = false;
        }
      }
    });
    control.add(supprimer);

    if (isElement) {
      indicateurPoints_.setText(TrResource.TR.getString("Nb El�ments {0}", Integer.toString(listPointOrMeshes.size())));
    } else {
      indicateurPoints_.setText(TrResource.TR.getString("Nb points {0}", Integer.toString(listPointOrMeshes.size())));
    }
    control.add(indicateurPoints_);
    content.add(control, BorderLayout.SOUTH);

    content.setBorder(BorderFactory.createTitledBorder(TrResource.getS(isElement ? "El�ments s�lectionn�s " : "Points s�lectionn�s")));

    // -- ajout du panel de selection des zcqlauePoints de la vue 2d --//

    builderPointsCalque_ = new TrEvolutionBuilderFromTree.PostEvolutionTemporelles(this.calque_, model,
        listPointOrMeshes);
    JTree buildTree = builderPointsCalque_.buildTree();
    if (buildTree.getModel().getChildCount(buildTree.getModel().getRoot()) == 0) {
      return content;
    }

    splitpanel.setTopComponent(content);

    BuPanel conteneurArbre = new BuPanel(new BorderLayout());
    conteneurArbre.setBorder(BorderFactory.createTitledBorder(TrResource.getS("Points de la vue 2D")));

    conteneurArbre.add(new BuScrollPane(buildTree));

    conteneurArbre
        .add(
            new JLabel(
                TrResource
                    .getS(
                        "<html><body>Pour ajouter les points, double cliquez dessus. Pour ajouter directement tous les points d'un calque,<br /> double-cliquez sur le calque lui m�me</body></html>")),
            BorderLayout.NORTH);

    splitpanel.setBottomComponent(conteneurArbre);
    splitpanel.setDividerLocation(150);
    return splitpanel;
  }

  /**
   * Methode qui cree un courbeModel contenant toutes les courbes pour la selection de l utilisateur.
   */
  public TrPostCourbeTreeModel createCourbeModel(final ProgressionInterface prog) {
    // -- creation auto d un nouveau node widget avec courbe temporelle

    // --recuperation des variables --//
    final H2dVariableType[] var = getSelectedVar();
    TrPostCourbeTreeModel modelPointsReels = null;
    TrPostCourbeTreeModel modelPointsInt = null;

    // -- cas listes de noeuds --//
    // -- on recupere la liste des points reels --//
    final int[] listePtsReels = getAllPOintsReels();
    if (listePtsReels != null && listePtsReels.length != 0) {
      modelPointsReels = TrPostCourbeBuilder.build(getProjet(), getSource(), var, listePtsReels, prog);
    }
    // -- cas sonde --//
    // -- on recupere la liste des points interpol --//
    final TrPostInterpolatePoint[] listePtsInterpoll = getAllPOintsInt();
    if (listePtsInterpoll != null && listePtsInterpoll.length != 0) {
      modelPointsInt = TrPostCourbeBuilder.build(getProjet(), calque_.getSource(), var, listePtsInterpoll, prog);
    }

    TrPostCourbeTreeModel modelTotal = null;

    if (modelPointsReels != null) {
      modelTotal = modelPointsReels;
      if (modelPointsInt != null) // --merge avec le precedent --//
      {
        modelTotal.mergeWithAnotherTreeModel(modelPointsInt);
      }
    } else if (modelPointsInt != null) {
      modelTotal = modelPointsInt;
    }
    if (modelTotal != null) {
      modelTotal.getAxeX().setUnite("s");
    }

    // -- creation du widget avec model total --//

    return modelTotal;
  }

  /**
   * Action de creation d un nouveau widget node en live
   */
  protected void createNewGraphe(final TrPostCourbeTreeModel modelTotal, final ProgressionInterface prog) {
    if (modelTotal != null) {
      getProjet().addFille(modelTotal, TrPostCourbeTreeModel.getNewName(), prog, calque_);
    }
  }

  /**
   * Creer une trace de l'origine du node sous forme dd calque
   */
  private void creerTraceCalque() {
    CtuluDialogPanel pn = new CtuluDialogPanel(false);
    pn.setLayout(new BuGridLayout(2));
    pn.addLabel(TrLib.getString("Indiquer le nom du calque � cr�er"));
    JTextField tf = nomCalque_;
    if (tf.getText() == null || tf.getText().length() == 0) {
      tf.setText("Origine �volution temporelle");
    }

    String titreCalque = tf.getText();
    FSigLayerGroup group = TrPostWizardProfilSpatial.getGroup(idTemporel, calque_);
    // -- cas interpol� --//
    TrPostInterpolatePoint[] listeInterpol = getAllPOintsInt();
    if (listeInterpol != null && listeInterpol.length > 0) {
      GISZoneCollectionPoint collectionPoint = new GISZoneCollectionPoint();
      for (int i = 0; i < listeInterpol.length; i++) {
        collectionPoint.add(listeInterpol[i].getX(), listeInterpol[i].getY(), 0);
      }
      ZModelePointEditable modelePointEdit = new ZModelePointEditable(collectionPoint);
      ZCalquePoint calqueZ = new ZCalquePoint(modelePointEdit);
      calqueZ.setIconModel(0, new TraceIconModel(TraceIcon.PLUS, 4, Color.blue));
      calqueZ.setTitle(titreCalque + " Interpol�s");
      calqueZ.setDestructible(true);
      group.add(calqueZ);
    }
    // -- cas reels --//
    int[] listeReels = getAllPOintsReels();
    if (listeReels != null && listeReels.length > 0) {

      GISZoneCollectionPoint collectionPoint = new GISZoneCollectionPoint();
      for (int i = 0; i < listeReels.length; i++) {
        Coordinate coor = null;
        if (isElement) {
          coor = new Coordinate();
          calque_.getSource().getGrid().getMoyCentreElement(listeReels[i], coor);
        } else {
          coor = calque_.getSource().getGrid().getCoor(listeReels[i]);
        }
        collectionPoint.add(coor.x, coor.y, 0);
      }
      ZModelePointEditable modelePointEdit = new ZModelePointEditable(collectionPoint);
      ZCalquePoint calqueZ = new ZCalquePoint(modelePointEdit);
      calqueZ.setIconModel(0, new TraceIconModel(TraceIcon.CARRE_SELECTION, 3, Color.blue));
      calqueZ.setTitle(titreCalque + " Noeuds R�els");
      calqueZ.setDestructible(true);

      // group.add(calqueZ);

      group.add(calqueZ);
    }
  }

  /**
   * appel� a la fin du wizard
   */
  @Override
  public void doTask() {
    done_ = true;
    final CtuluTaskDelegate task = calque_.getCtuluUI().createTask(TrLib.getString("Graphe"));

    task.start(new Runnable() {
      @Override
      public void run() {

        final ProgressionInterface prog = task.getStateReceiver();

        // -- creation du model pour les donn�es fournies --//
        final TrPostCourbeTreeModel modelTotal = createCourbeModel(prog);

        // -- on enregistre les calques initiaux dans le node
        if (traceOrigineDansVue2d_.isSelected()) {
          creerTraceCalque();
        }

        // -- creation d un nouveau node si coch� --//
        if (checkboxNewGraphe_.isSelected()) {
          createNewGraphe(modelTotal, prog);
        }

        // -- ajout des courbes dans les graphes --//
        for (int i = 0; i < listeGraphesChoisis_.size(); i++) {
          final EGGraphe graphe = listeGraphesChoisis_.get(i);

          EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
              // // -- execution de l action pour le node selectionne --//
              // (new TrPostCourbeAddPointsAction(projet_.impl_, node,
              // calque_)).actionPerformed(new ActionEvent(calque_, 0,
              // "ADDPOINTWIDGET"));
              // -- on merge le nouveau tree
              ((TrPostCourbeTreeModel) graphe.getModel()).mergeWithAnotherTreeModel(modelTotal);
              TrPostScene scene = calque_.getPostImpl().getCurrentLayoutFille().getScene();
              scene.stopEditing();
              scene.refresh();
            }
          });
        }
      }
    });
  }

  /**
   * Methode qui cherche a interpoler le point
   *
   * @param x
   * @param y
   */
  TrPostInterpolatePoint findPointInterpol(final double x, final double y) {
    final GrPoint point = new GrPoint(x, y, 0);
    final int idxElt = TrIsoLayerDefault.sondeSelection(point, calque_.getIsoLayer().getIsoModel());
    if (idxElt > -1) {
      return new TrPostInterpolatePoint(idxElt, x, y, new GISPrecision());
    }
    return null;
  }

  /**
   * MEthode qui recherche si le point est exact pour les params x et y donnes
   *
   * @param x
   * @param y
   */
  int findPointReel(final double x, final double y) {
    if (calque_.isRubar()) {
      return -1;
    }
    CtuluListSelection nearestNode = EfIndexHelper.getNearestNode(getSource().getGrid(), x, y, 1E-2, null);
    return (nearestNode == null || nearestNode.isEmpty()) ? -1 : nearestNode.getMinIndex();
  }

  /**
   * retourne tous les points interpoles de la liste en enlevant les reels
   */
  TrPostInterpolatePoint[] getAllPOintsInt() {

    final ArrayList<TrPostInterpolatePoint> res = new ArrayList<TrPostInterpolatePoint>();

    for (int i = 0; i < listPointOrMeshes.size(); i++) {
      if (isInterpolated(i)) {
        res.add(getPointInt(i));
      }
    }

    final TrPostInterpolatePoint[] listeInt = new TrPostInterpolatePoint[res.size()];
    for (int i = 0; i < res.size(); i++) {
      listeInt[i] = res.get(i);
    }
    return listeInt;
  }

  /**
   * retourne tous les points reels de la liste en enlevant les interpoles
   */
  int[] getAllPOintsReels() {

    final ArrayList<Integer> res = new ArrayList<Integer>();

    for (int i = 0; i < listPointOrMeshes.size(); i++) {
      if (!isInterpolated(i)) {
        res.add(getPointReel(i));
      }
    }

    final int[] listeReels = new int[res.size()];
    for (int i = 0; i < res.size(); i++) {
      listeReels[i] = res.get(i);
    }
    return listeReels;
  }

  public TrPostInterpolatePoint getPointInt(final int i) {

    return (TrPostInterpolatePoint) listPointOrMeshes.get(i);
  }

  public int getPointReel(final int i) {
    return ((Integer) listPointOrMeshes.get(i)).intValue();
  }

  public TrPostProjet getProjet() {
    return calque_.getProjet();
  }

  public EbliScene getScene() {
    return calque_.getPostImpl().getCurrentLayoutFille().getScene();
  }

  /**
   * Recupere les variables selectionnees du panel correspondant.
   */
  protected H2dVariableType[] getSelectedVar() {
    final Object[] obj = chooserVarTime_.getSelectedVar();
    final H2dVariableType[] var = new H2dVariableType[obj.length];
    System.arraycopy(obj, 0, var, 0, var.length);
    return var;
  }

  /**
   * @return @see org.fudaa.fudaa.tr.post.TrPostVisuPanel#getSource()
   */
  public TrPostSource getSource() {
    return source;
  }

  @Override
  public JComponent getStepComponent() {
    switch (current_) {
      case 0: {

        if (panelSelectionPoints == null) {
          panelSelectionPoints = constructSelectionPoint();
        }
        return panelSelectionPoints;
      }
      case 1: {
        if (listPointOrMeshes.size() == 0) {
          return new JLabel(
              TrResource.getS("Erreur, aucun points s�lectionn�s, veuillez appuyer sur pr�c�dent et ajouter des points"));
        }

        return chooserVarTime_;
      }
      case 2: {
        if (panelSelectiongraphes == null) {
          panelSelectiongraphes = constructPanelSelectionGraphe();
        }
        return panelSelectiongraphes;
      }
    }

    return null;
  }

  @Override
  public int getStepCount() {
    return 3;
  }

  private boolean pointsSaisis() {
    return listPointOrMeshes.size() > 0;
  }

  /**
   * MEthode utilisee pour disabler ou non les boutons suivants
   */
  @Override
  public int getStepDisabledButtons() {
    int r = super.getStepDisabledButtons();
    if (current_ == 1) {
      if (!pointsSaisis()) {
        r |= BuButtonPanel.SUIVANT;
      }
    } else if (current_ == 2) {
      if ((listeGraphesChoisis_ == null || listeGraphesChoisis_.size() == 0) && !checkboxNewGraphe_.isSelected()) {
        r |= BuButtonPanel.TERMINER;
      }
      // -- pour empecher d avancer, voir ci dessous
      // r |= BuButtonPanel.SUIVANT;
    }
    return r;
  }

  @Override
  public String getStepText() {
    String r = null;

    switch (current_) {
      case 0:
        if (isElement) {
          r = TrResource.getS("Choisir les �l�ments");
        } else {
          r = TrResource.getS("Choisir les points");
        }
        break;
      case 1:
        r = TrResource.getS("S�lection des variables");
        break;
      case 2:
        r = TrResource.getS("Veuillez s�lectionner les graphes pour la cr�ation des courbes ou cr�er un nouveau graphe");
        break;
    }

    return r;
  }

  @Override
  public String getStepTitle() {
    String r = null;

    switch (current_) {
      case 0:
        if (isElement) {
          r = TrResource.getS("Choisir les �l�ments");
        } else {
          r = TrResource.getS("Choisir les points");
        }
        break;
      case 1:
        r = TrResource.getS("S�lectionner les variables");
        break;
      case 2:
        r = TrResource.getS("S�lectionner les graphes");
        break;
    }
    return r;
  }

  @Override
  public String getTaskTitle() {
    return TrLib.getString("Evolutions temporelles");
  }

  ;

  /**
   * indique si le node est bien temporel
   *
   * @param _model
   */
  public boolean isCorrectGraph(final EGGrapheModel _model) {

    return (_model instanceof TrPostCourbeTreeModel);
  }

  public boolean isInterpolated(final int i) {
    return (listPointOrMeshes.get(i) instanceof TrPostInterpolatePoint);
  }

  /**
   * Methode a appeler pour rechercher les graphes compatibles dans le layout. remplis les model modelGraphes et listeGraphes.
   */
  private DefaultListModel rechercheGrapheWidget() {
    final Map params = new HashMap();
    CtuluLibImage.setCompatibleImageAsked(params);
    // -- recuperation de la liste des nodes de la scene --//
    final DefaultListModel modelGraphesPossibles = new DefaultListModel();
    listeGraphesPossibles_ = new ArrayList<EGGraphe>();
    listeGraphesChoisis_ = new ArrayList<EGGraphe>();
    TrPostTimeWidgetFinder widgetsFinder = new TrPostTimeWidgetFinder(getScene());
    List<EbliNode> findGraphs = widgetsFinder.findGraphTimeNodes();
    for (EbliNode node : findGraphs) {
      EbliWidgetCreatorGraphe graph = (EbliWidgetCreatorGraphe) node.getCreator();
      final JLabel label = new JLabel();
      final BufferedImage image = graph.getGraphe().produceImage(80, 50, params);
      final Icon icone = new ImageIcon(image);
      label.setIcon(icone);
      label.setText(node.getTitle());
      modelGraphesPossibles.addElement(label);
      listeGraphesPossibles_.add(graph.getGraphe());
    }
    return modelGraphesPossibles;
  }

  /**
   * Methode qui ouvre une dialog pour saisir 2 points et verifier si ils sont corrects, interpol�s ou reels.
   *
   * @param create
   */
  public void saisirPoints(final String val1, final String val2, final boolean create, final int indexToModify,
                           final ModelListePoints model) {
    final BuTextField textX = new BuTextField(10);
    textX.setCharValidator(BuCharValidator.FLOAT);
    textX.setStringValidator(BuStringValidator.FLOAT);
    textX.setValueValidator(BuValueValidator.FLOAT);

    final BuTextField textY = new BuTextField(10);
    textY.setCharValidator(BuCharValidator.FLOAT);
    textY.setStringValidator(BuStringValidator.FLOAT);
    textY.setValueValidator(BuValueValidator.FLOAT);
    // -- init --//
    if (val1 != null) {
      textX.setText(val1);
    }
    if (val2 != null) {
      textY.setText(val2);
    }

    final JDialog dialog = CtuluLibSwing.createDialogOnActiveWindow(TrResource.getS("Ajouter des points"));
    final JButton ajouter = new JButton(BuResource.BU.getString("Valider"), EbliResource.EBLI.getIcon("crystal_ajouter"));
    ajouter.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {

        final double newX = Double.parseDouble(textX.getText());
        final double newY = Double.parseDouble(textY.getText());

        // -- on teste si le point est reel --//
        final int indice = findPointReel(newX, newY);

        if (indice == -1) {
          // -- on teste si il est interpole --//

          final TrPostInterpolatePoint interpol = findPointInterpol(newX, newY);

          if (interpol == null) {
            calque_.getCtuluUI().error("Erreur", TrResource.getS("Ce point n'existe pas et ne peut �tre interpol�"),
                true);
            return;
          }

          // -- on ajoute le point interpol� --//
          if (create) {
            listPointOrMeshes.add(interpol);
            model.fireTableDataChanged();
          } else {
            listPointOrMeshes.set(indexToModify, interpol);
            model.fireTableRowsUpdated(indexToModify, indexToModify);
          }
        } else {
          // -- on ajoute le point reel --//
          if (create) {
            listPointOrMeshes.add(indice);
            model.fireTableDataChanged();
          } else {
            listPointOrMeshes.set(indexToModify, indice);
            model.fireTableRowsUpdated(indexToModify, indexToModify);
            // if(panelSelectionPoints!=null)
            // panelSelectionPoints.revalidate();

          }
        }

        indicateurPoints_.setText("Nb points: " + listPointOrMeshes.size());
        // if (listePoints_.size() > 0) pointsSaisis = true;
        dialog.dispose();
      }
    });
    final JPanel control = new JPanel(new FlowLayout(FlowLayout.CENTER));
    control.add(new JLabel("X: "));
    control.add(textX);
    control.add(new JLabel("Y: "));
    control.add(textY);
    control.add(ajouter);

    String title;
    if (create) {
      title = TrResource.getS("Creation de point");
    } else {
      title = TrResource.getS("Modification de point");
    }

    final JPanel content = new JPanel(new BorderLayout());
    content.add(control, BorderLayout.CENTER);

    dialog.setContentPane(content);
    dialog.setModal(true);
    dialog.pack();
    dialog.setTitle(title);
    dialog.setLocationRelativeTo(CtuluLibSwing.getActiveWindow());
    dialog.setVisible(true);
  }

  private String format(double x_) {
    return calque_.getEbliFormatter().getXYFormatter().format(x_);
  }

  public class ModelListePoints extends AbstractTableModel {
    final GrPoint point_ = new GrPoint();
    String[] titre_;
    BuLabel label_;

    public ModelListePoints(BuLabel label) {
      if (isElement) {
        final String[] val = {TrLib.getString("El�ment"), "X", "Y"};
        titre_ = val;
      } else {
        final String[] val = {TrLib.getString("Point"), "X", "Y", TrLib.getString("Interpol�")};
        titre_ = val;
      }
      label_ = label;
    }

    @Override
    public Class<?> getColumnClass(final int _columnIndex) {
      if (_columnIndex == 3) {
        return Boolean.class;
      }
      if (_columnIndex == 2 || _columnIndex == 1) {
        return JButton.class;
      }

      return String.class;
    }

    @Override
    public int getColumnCount() {

      return titre_.length;
    }

    @Override
    public String getColumnName(final int _columnIndex) {
      return titre_[_columnIndex];
    }

    @Override
    public int getRowCount() {
      return listPointOrMeshes.size();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {

      // -- connaitre la bonen liste entre les points reels et interpol�s

      if (_columnIndex == 0) {
        if (isInterpolated(_rowIndex)) {
          return TrLib.getString("Interpol�");// + " " + getPointInt(_rowIndex).idxElt_
        }
        if (isElement) {
          return TrResource.TR.getString("El�ment {0}", Integer.toString(getPointReel(_rowIndex) + 1));
        } else {
          return TrResource.TR.getString("Point {0}", Integer.toString(getPointReel(_rowIndex) + 1));
        }
      } else if (_columnIndex == 1) {
        if (!isInterpolated(_rowIndex)) {
          return format(getX(getPointReel(_rowIndex)));
        } else {
          return format(getPointInt(_rowIndex).getX());
        }
      } else if (_columnIndex == 2) {
        if (!isInterpolated(_rowIndex)) {
          return format(getY(getPointReel(_rowIndex)));
        } else {
          return format(getPointInt(_rowIndex).getY());
        }
      } else if (_columnIndex == 3) {
        return isInterpolated(_rowIndex);
      }

      return null;
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      // if (_columnIndex == 0 || _columnIndex == 3)
      // return false;
      // else
      // return true;
      return true;
    }

    @Override
    public void fireTableDataChanged() {
      super.fireTableDataChanged();
      updatelabel();
    }

    @Override
    public void fireTableStructureChanged() {

      super.fireTableStructureChanged();
      updatelabel();
    }

    void updatelabel() {
      if (label_ != null) {
        if (isElement) {
          label_.setText(TrResource.TR.getString("Nb El�ments {0}", Integer.toString(listPointOrMeshes.size())));
        } else {
          label_.setText(TrResource.TR.getString("Nb points {0}", Integer.toString(listPointOrMeshes.size())));
        }
      }
    }
  }
}
