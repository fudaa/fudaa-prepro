package org.fudaa.fudaa.tr.post.actions;

import java.awt.event.ActionEvent;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.fudaa.tr.common.TrLib;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.post.TrPostCommonImplementation;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;
import org.fudaa.fudaa.tr.post.dialogSpec.TrPostDialogBilan;
import org.fudaa.fudaa.tr.post.profile.MvProfileTreeModel;

/**
 * Action pour le calcul des bilans. Cette action peut etre executee depuis le mode edition du calque pu le mode edition d'un profil spoatial.
 *
 * @author Adrien Hadoux
 */
public class TrPostActionBilan extends EbliActionSimple {

  // final MvProfileTarget src_;
  final TrPostVisuPanel panel_;
  final MvProfileTreeModel modelGraphe_;
  final boolean startWithCalque;
  TrPostCommonImplementation impl_;
  private EbliScene scene;

  public TrPostActionBilan(final TrPostVisuPanel _visu, final TrPostCommonImplementation impl) {
    super(TrLib.getString("Calcul des bilans"), TrResource.TR.getIcon("bilan"), "BILAN");
    panel_ = _visu;
    modelGraphe_ = null;
    startWithCalque = true;
    impl_ = impl;
  }

  public TrPostActionBilan(EbliScene scene, final MvProfileTreeModel _model, final TrPostCommonImplementation impl) {
    super(TrLib.getString("Calcul des bilans"), TrResource.TR.getIcon("bilan"), "BILAN");
    setDefaultToolTip(TrLib.getString("Afin de déterminer la ligne support, sélectionner une courbe avec de lancer le calcul"));
    modelGraphe_ = _model;
    panel_ = null;
    startWithCalque = false;
    impl_ = impl;
    this.scene = scene;
  }

  TrPostDialogBilan bilanCalque = null;
  TrPostDialogBilan bilanSsCalque = null;
  
  
  @Override
  public void actionPerformed(final ActionEvent _e) {
    // -- creation du wizard depuis le calque --//
    if (startWithCalque) {
    	if(bilanCalque == null)
    		bilanCalque = new TrPostDialogBilan(panel_, impl_);
    	bilanCalque.buildDialog();
    } else {
    	if(bilanSsCalque == null)
    		bilanSsCalque = new TrPostDialogBilan(scene,modelGraphe_, impl_);
    	bilanSsCalque.buildDialog();
    }
    
    
    
  }
}
