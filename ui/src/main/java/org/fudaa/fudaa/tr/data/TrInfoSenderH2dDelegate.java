/**
 * @creation 9 d�c. 2003
 * @modification $Date: 2007-06-05 09:01:15 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.data;

import com.memoire.bu.BuTable;
import java.util.Collection;
import java.util.Iterator;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dParameters;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * @author deniger
 * @version $Id: TrInfoSenderH2dDelegate.java,v 1.13 2007-06-05 09:01:15 deniger Exp $
 */
public abstract class TrInfoSenderH2dDelegate extends TrInfoSenderDelegate {

  protected final H2dParameters params_;

  /**
   * @param _fmt
   */
  public TrInfoSenderH2dDelegate(final EbliFormatterInterface _fmt, final H2dParameters _params) {
    super(_fmt);
    params_ = _params;
  }

  @Override
  public final BuTable createValuesTableForNodeLayer() {
    return new CtuluTable(new TrVarTableModel(params_.getNodalData(), null));
  }

  @Override
  public boolean isValuesTableAvailableForNodeLayer() {
    return true;
  }

  protected void fillNodalParametersInfo(final InfoData _m, final int _idxGlobal) {
    fillValuesParametersInfo(_m, _idxGlobal, params_.getNodalData());
  }

  protected void fillValuesParametersInfo(final InfoData _m, final int _idxGlobal,
      final H2dVariableProviderInterface _values) {
    if (_values != null) {
      final Collection t = _values.getUsableVariables();
      if (t != null) {
        for (final Iterator it = t.iterator(); it.hasNext();) {
          final H2dVariableType ti = (H2dVariableType) it.next();
          _m.put(ti.getName(), CtuluLib.DEFAULT_NUMBER_FORMAT.format(_values.getViewedModel(ti).getValue(_idxGlobal)));
        }
      }
    }
  }

  protected void fillMeshAverageValuesParametersInfo(final InfoData _m, final int _idxEltGlobal,
      final H2dVariableProviderInterface _values) {
    if (_values != null) {
      final Collection t = _values.getUsableVariables();
      final String fin = " (" + EbliLib.getS("Moyenne") + ')';
      if (t != null) {
        for (final Iterator it = t.iterator(); it.hasNext();) {
          final H2dVariableType ti = (H2dVariableType) it.next();
          _m.put(ti.getName() + fin, CtuluLib.DEFAULT_NUMBER_FORMAT.format(getGrid().getElement(_idxEltGlobal)
              .getAverage(ti, _values, params_.getVectorContainer())));
        }
      }
    }
  }

  @Override
  public EfGridInterface getGrid() {
    return params_.getMaillage();
  }

}