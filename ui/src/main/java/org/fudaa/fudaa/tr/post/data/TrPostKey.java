package org.fudaa.fudaa.tr.post.data;

import java.util.Objects;

public class TrPostKey {
  public final String key;
  public final int idxVariable;
  private final int idxFile;

  public TrPostKey(String key, int idxVariable) {
    this(key, idxVariable, 0);
  }

  public TrPostKey(String key, int idxVariable, int idxFile) {
    this.key = key;
    this.idxVariable = idxVariable;
    this.idxFile = idxFile;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TrPostKey trPostKey = (TrPostKey) o;
    return idxVariable == trPostKey.idxVariable &&
        idxFile == trPostKey.idxFile &&
        Objects.equals(key, trPostKey.key);
  }

  @Override
  public int hashCode() {
    return Objects.hash(key, idxVariable, idxFile);
  }
}
