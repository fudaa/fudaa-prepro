package org.fudaa.fudaa.tr.rubar;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class TrRubarTarageCourbesListModel extends AbstractListModel implements ComboBoxModel {

  transient Object selected_;
  private final TrRubarTarageCourbesManager courbes;
  private final boolean useNullAsFirst;

  /**
   * @param courbes
   * @param useNullAsFirst
   */
  public TrRubarTarageCourbesListModel(TrRubarTarageCourbesManager courbes, boolean useNullAsFirst) {
    super();
    this.courbes = courbes;
    this.useNullAsFirst = useNullAsFirst;
  }

  @Override
  public int getSize() {
    return useNullAsFirst ? 1 + courbes.getNbEvol() : courbes.getNbEvol();
  }

  @Override
  public Object getElementAt(int index) {
    if (useNullAsFirst) {
      if (index == 0) { return null; }
      return courbes.getEvol(index - 1);
    }
    if(courbes.getSize()==0){
      return null;
    }

    return courbes.getEvol(index);
  }

  @Override
  public void setSelectedItem(Object anItem) {
    if (anItem != selected_) {
      selected_ = anItem;
      fireContentsChanged(this, -1, -1);
    }
  }

  @Override
  public Object getSelectedItem() {
    return selected_;
  }

}
