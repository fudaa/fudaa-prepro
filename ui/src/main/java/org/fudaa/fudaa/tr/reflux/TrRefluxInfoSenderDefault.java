/*
 * @creation 9 d�c. 2003
 * @modification $Date: 2007-03-30 15:39:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.tr.reflux;

import com.memoire.bu.BuTable;
import java.util.Arrays;
import java.util.Set;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBcManager;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBoundaryCondition;
import org.fudaa.dodico.h2d.reflux.H2dRefluxElementProperty;
import org.fudaa.dodico.h2d.reflux.H2dRefluxElementPropertyMngAbstract;
import org.fudaa.dodico.h2d.reflux.H2dRefluxParameters;
import org.fudaa.dodico.h2d.reflux.H2dRefluxValue;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrInfoSenderH2dDelegate;

/**
 * @author deniger
 * @version $Id: TrRefluxInfoSenderDefault.java,v 1.25 2007-03-30 15:39:54 deniger Exp $
 */
public class TrRefluxInfoSenderDefault extends TrInfoSenderH2dDelegate {

  /**
   * @param _params
   */
  public TrRefluxInfoSenderDefault(final H2dRefluxParameters _params, final EbliFormatterInterface _fmt) {
    super(_fmt, _params);
  }

  protected H2dRefluxParameters getReflux() {
    return (H2dRefluxParameters) params_;
  }

  @Override
  public void fillWithElementInfo(final InfoData _receiver, final int _idx) {
    final H2dRefluxElementPropertyMngAbstract eleMng = getReflux().getElementPropMng();
    final CtuluPermanentList l = eleMng.getVarList();
    final int n = l.size();
    for (int i = 0; i < n; i++) {
      final H2dVariableType v = (H2dVariableType) l.get(i);
      final H2dRefluxElementProperty prop = eleMng.getEltProp(v);
      final H2dBcType type = prop.getTypeForEltIdx(_idx);
      if (type == H2dBcType.LIBRE) {
        _receiver.put(v.getName(), type.getName());
      } else if (type == H2dBcType.PERMANENT) {
        _receiver.put(v.getName(), type.getName() + getSep() + prop.getPermanentValueFor(_idx));
      } else {
        _receiver.put(v.getName(), type.getName() + getSep() + prop.getTransitoireEvol(_idx).getNom());
      }
    }
  }

  private String getSep() {
    return ": ";
  }

  @Override
  public void fillWithBoundaryMiddleInfo(final InfoData _receiver, final int _frIdx, final int _idxOnFr) {
    final H2dRefluxBcManager bcMng = getReflux().getRefluxClManager();
    final H2dRefluxBcManager.RefluxMiddleFrontier fr = bcMng.getRefluxMiddleFrontier(_frIdx);
    final H2dRefluxBoundaryCondition bc = fr.getRefluxBc(_idxOnFr);
    if (bc.isMiddle()) {
      _receiver.setTitle(bc.getMiddle().getBoundaryType().getName());
    }
    int temp = _idxOnFr - 1;
    _receiver.put(TrResource.getS("Indice du premier noeud"), fr.getRefluxBc(temp).getIndexPt() + 1 + " ("
        + CtuluLibString.getString(temp + 1) + ")");
    temp = _idxOnFr;
    _receiver.put(TrResource.getS("Indice du noeud milieu"), fr.getRefluxBc(temp).getIndexPt() + 1 + " ("
        + CtuluLibString.getString(temp + 1) + ")");
    temp = _idxOnFr == fr.getNbPt() - 1 ? 0 : _idxOnFr + 1;
    _receiver.put(TrResource.getS("Indice du dernier noeud"), fr.getRefluxBc(temp).getIndexPt() + 1 + " ("
        + CtuluLibString.getString(temp + 1) + ")");

    final Set s = fr.getAvailablesVariables(_idxOnFr);
    final H2dVariableType[] t = new H2dVariableType[s.size()];
    s.toArray(t);
    Arrays.sort(t);
    final H2dRefluxValue value = new H2dRefluxValue();
    final int n = t.length;
    for (int i = 0; i < n; i++) {
      final H2dVariableType v = t[i];
      bc.fillWithValue(v, value);
      final H2dBcType bcType = value.getType();
      if (bcType == H2dBcType.LIBRE) {
        _receiver.put(v.getName(), bcType.getName());
      } else if (bcType == H2dBcType.PERMANENT) {
        _receiver.put(v.getName(), bcType.getName()
            + (value.isDoubleValueConstant() ? getSep() + value.getValue() : CtuluLibString.EMPTY_STRING));
      } else if (bcType == H2dBcType.TRANSITOIRE) {
        _receiver.put(v.getName(), bcType.getName()
            + (value.isEvolutionFixed() ? getSep() + value.getEvolution().getNom() : CtuluLibString.EMPTY_STRING));
      } else {
        _receiver.put(v.getName(), bcType.getName());
      }

    }

  }

  @Override
  public void fillWithPointInfo(final InfoData _m, final int _idxGlobal, final int _idxGlobOnFr, final int _frIdx,
      final int _idxOnFr) {
    super.fillWithPointInfo(_m, _idxGlobal, _idxGlobOnFr, _frIdx, _idxOnFr);
    if (_idxGlobOnFr >= 0) {
      final H2dRefluxBcManager.RefluxMiddleFrontier fr = (H2dRefluxBcManager.RefluxMiddleFrontier) getReflux()
          .getBcManager().getMiddleFrontier(_frIdx);
      final Set s = fr.getAvailablesVariables(_idxOnFr);
      final H2dVariableType[] t = new H2dVariableType[s.size()];
      s.toArray(t);
      Arrays.sort(t);
      final H2dRefluxBoundaryCondition bc = fr.getRefluxBc(_idxOnFr);
      _m.put(H2dResource.getS("Normale"), CtuluLib.DEFAULT_NUMBER_FORMAT.format(bc.getNormale()));
      final int n = t.length;
      final H2dRefluxValue value = new H2dRefluxValue();
      for (int i = 0; i < n; i++) {
        final H2dVariableType v = t[i];
        bc.fillWithValue(v, value);
        final H2dBcType bcType = value.getType();
        if (bcType == H2dBcType.LIBRE) {
          _m.put(v.getName(), H2dBcType.LIBRE.getName());
        } else if (bcType == H2dBcType.PERMANENT) {
          _m.put(v.getName(), H2dBcType.PERMANENT.getName() + getSep() + value.getValue());
        } else {
          _m.put(v.getName(), bcType.getName() + getSep() + value.getEvolution().getNom());
        }
      }
    }
    super.fillNodalParametersInfo(_m, _idxGlobal);
  }

  public BuTable createElementTable() {
    return new CtuluTable(new ElementTableModel());
  }

  /**
   * @author Fred Deniger
   * @version $Id: TrRefluxInfoSenderDefault.java,v 1.25 2007-03-30 15:39:54 deniger Exp $
   */
  final class ElementTableModel extends AbstractTableModel {

    H2dRefluxElementPropertyMngAbstract eleMng_;

    ElementTableModel() {
      eleMng_ = getReflux().getElementPropMng();

    }

    @Override
    public int getColumnCount() {
      return 1 + eleMng_.getVarList().size();
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) { return MvResource.getS("Indice"); }
      return eleMng_.getVarList().get(_column - 1).toString();
    }

    @Override
    public int getRowCount() {
      return getReflux().getMaillage().getEltNb();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _column) {
      if (_column == 0) { return new Integer(_rowIndex + 1); }
      final H2dVariableType v = (H2dVariableType) eleMng_.getVarList().get(_column - 1);
      final H2dRefluxElementProperty prop = eleMng_.getEltProp(v);
      final H2dBcType type = prop.getTypeForEltIdx(_rowIndex);
      if (type == H2dBcType.LIBRE) {
        return type.getName();
      } else if (type == H2dBcType.PERMANENT) {
        return type.getName() + ": " + prop.getPermanentValueFor(_rowIndex);
      } else {
        return type.getName() + ": " + prop.getTransitoireEvol(_rowIndex).getNom();
      }
    }
  }

}