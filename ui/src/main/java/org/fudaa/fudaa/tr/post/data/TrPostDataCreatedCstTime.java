/*
 * @creation 13 juil. 06
 * 
 * @modification $Date: 2007-04-30 14:22:38 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.post.data;

import java.util.Map;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.TrPostSource;

/**
 * @author fred deniger
 * @version $Id: TrPostDataCreatedCstTime.java,v 1.6 2007-04-30 14:22:38 deniger Exp $
 */
public class TrPostDataCreatedCstTime extends TrPostDataCreatedConstant {

  @Override
  public boolean isEditable() {
    return true;
  }

  private final int timeIdx_;
  private String varId;

  public static TrPostDataCreatedCstTime createFrom(H2dVariableType _newVar, final TrPostSource _src,
      final TrPostDataCreatedCstTimeSaver _saver, final Map _shortName) {
    if (_src == null || _saver == null || _shortName == null) { return null; }
    final H2dVariableType var = (H2dVariableType) _shortName.get(_saver.shortName_);
    if (var == null) { return new TrPostDataCreatedCstTime(_src, _saver.shortName_, _saver.tidx_); }
    return new TrPostDataCreatedCstTime(_src, var, _saver.tidx_);

  }

  protected void initVar() {
    if (var_ == null) {
      var_ = TrPostDataHelper.getShortNameVariable(super.src_).get(varId);
    }
  }

  public TrPostDataCreatedCstTime(final TrPostSource _src, final H2dVariableType _var, final int _tidx) {
    super(_src, null, _var);
    timeIdx_ = _tidx;
  }

  public TrPostDataCreatedCstTime(final TrPostSource _src, final String _var, final int _tidx) {
    super(_src, null, null);
    varId = _var;
    timeIdx_ = _tidx;
  }

  @Override
  public void clearCache() {
    data_ = null;
  }

  @Override
  public EfData getDataFor(final int _idxTime) {
    initVar();
    if (data_ == null) {
      data_ = src_.getData(var_, timeIdx_);
    }
    return data_;
  }

  @Override
  public String getDescription() {
    initVar();
    return var_.getName() + CtuluLibString.ESPACE + src_.getTimeListModel().getElementAt(timeIdx_);
  }

  @Override
  public double getValue(final int _idxTime, final int _idxObject) {
    initVar();
    final EfData d = getDataFor(_idxTime);
    return d == null ? 0 : d.getValue(_idxObject);
  }

  public int getTimeIdx() {
    return timeIdx_;
  }

  public H2dVariableType getVar() {
    return var_;
  }

  @Override
  public TrPostDataCreatedSaver createSaver() {
    return new TrPostDataCreatedCstTimeSaver(this);
  }

}
