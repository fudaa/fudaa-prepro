package org.fudaa.fudaa.tr.post;

import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTime;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimePersistBuilder;

/**
 * @author deniger
 */
public class TrPostCourbePersistBuilder extends FudaaCourbeTimePersistBuilder {

  @Override
  public boolean saveXY() {
    return true;
  }

  @Override
  protected FudaaCourbeTime createEGObject(EGCourbePersist target, Map params, CtuluAnalyze log) {
    EGGroup parent = getGroup(params);
    EGModel createModel = createModel(target, params);
    if (createModel instanceof TrPostCourbeModel) {
      TrPostCourbeModel model = (TrPostCourbeModel) createModel;
      if (!model.isValid()) { return null; }
    }
    FudaaCourbeTimeListModel timeModel = (FudaaCourbeTimeListModel) params.get(TIME_MODEL);
    if (timeModel == null) {
      log.addError(FudaaLib.getS("Le temps n'a pas �t� trouv� pour la courbe {0}", target.getTitle()));
      return null;
    }
    return new TrPostCourbe(parent, createModel, timeModel);
  }

}
