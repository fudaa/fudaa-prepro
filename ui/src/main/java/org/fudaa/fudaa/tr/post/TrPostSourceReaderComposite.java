package org.fudaa.fudaa.tr.post;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.fudaa.tr.common.TrResource;

/**
 * TrPostSourceReader composite utilis� pour les suites de calcul. Contient plusieurs TrPostSourceReaderInterface.
 * 
 * @author Adrien Hadoux
 */
public class TrPostSourceReaderComposite extends TrPostSourceReaderCommonAbstract {

  /**
   * Couple data/time step associ�. Permet de retrouver les bonnes donn�es pour un timeStep du composite donn�.
   * 
   * @author Adrien Hadoux
   */
  public static class CoupleTimeStepData {
    public CoupleTimeStepData(TrPostSourceReaderInterface data, int timeStep) {
      this.data_ = data;
      this.timeStep_ = timeStep;
    }

    public TrPostSourceReaderInterface data_;
    public int timeStep_;
  }
  
  public void fillInfosWithComposite(Map<String, String> infos) {

    infos.put(ZEbliCalquesPanel.TITRE_FIC, TrResource.getS("Suite de calcul"));
    // pnVisu.infosCreation_.put(ZEbliCalquesPanel.NOM_FIC+" 1", );
    TrPostSourceAbstractFromIdx.fillWithSourceInfo(CtuluLibString.EMPTY_STRING, infos,this);

  }

  /**
   * La liste des interface source qui compose la suite.
   */
  List<TrPostSourceReaderInterface> listeSourceInterface_ = new ArrayList<TrPostSourceReaderInterface>();

  /**
   * La map qui contient les
   */
  Map<Integer, CoupleTimeStepData> mapTimeStep_ = null;

  // TODO utiliser TINtObjectHashMap: prend moins de place que Map<Integer,...
  public TrPostSourceReaderComposite(double[] doubles, H2dVariableType[] var,
      Map<Integer, CoupleTimeStepData> mapTimeStep, List<TrPostSourceReaderInterface> listeSourceInterface,
      Collection<File> file) {
    super(file, doubles, var);
    listeSourceInterface_ = listeSourceInterface;
    mapTimeStep_ = mapTimeStep;

  }

  public List<TrPostSourceReaderInterface> getListeSourceInterface() {
    return listeSourceInterface_;
  }

  public void setListeSourceInterface_(List<TrPostSourceReaderInterface> listeSourceInterface) {
    this.listeSourceInterface_ = listeSourceInterface;
  }

  @Override
  public void close() {
    for (TrPostSourceReaderInterface interfaceSrc : listeSourceInterface_)
      interfaceSrc.close();

  }

  @Override
  public EfData getInitData(final CtuluVariable _data, final int _timeStep) {
    // -- etape 1: on recherche l'interface qui contient le timestep --//
    CoupleTimeStepData data = mapTimeStep_.get(_timeStep);
    if (data == null) return null;
    return data.data_.getInitData(_data, data.timeStep_);
  }

  @Override
  public boolean isVolumique() {
    return listeSourceInterface_ != null && listeSourceInterface_.size() > 0
        && listeSourceInterface_.get(0).isVolumique();
  }

  @Override
  public double getInitData(CtuluVariable _varIdx, int _timeIdx, int _ptIdx) throws IOException {
    // -- etape 1: on recherche l'interface qui contient le timestep --//
    CoupleTimeStepData data = mapTimeStep_.get(_timeIdx);
    if (data == null) return 0;
    return data.data_.getInitData(_varIdx, data.timeStep_, _ptIdx);
  }

  
  

  @Override
  public String getTitle() {

    String title = TrResource.getS("Suite de calcul");
    title += ":";
    for (File f : file_)
      title += " " + f.getName();

    return title;
  }

  /**
   * Cas particulier: il gere lui meme ses propres ids.
   */
  String id_;

  @Override
  public void SetId(String id) {
    id_ = id;
  }

  @Override
  public String getId() {
    // TODO Auto-generated method stub
    return id_;
  }

}
