/**
 * 
 */
package org.fudaa.fudaa.tr.post;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.JInternalFrame;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.tr.post.data.TrPostDataListener;

public class TrPostDataListenerDispatcher implements TrPostDataListener {
  public transient TrPostProjet projet;

  public List<WeakReference<TrPostDataListener>> weakListener = new ArrayList<WeakReference<TrPostDataListener>>();

  /**
   * @param impl
   */
  public TrPostDataListenerDispatcher(TrPostProjet projet) {
    super();
    this.projet = projet;
  }

  public void clear() {
    weakListener.clear();
  }

  public void addWeakListener(TrPostDataListener listener) {
    weakListener.add(new WeakReference<TrPostDataListener>(listener));
  }

  private Collection<TrPostDataListener> getListeners() {
    List<TrPostDataListener> res = new ArrayList<TrPostDataListener>();
    if (projet.getImpl() != null) {
      final JInternalFrame[] allFrames = projet.getImpl().getAllInternalFrames();
      if (allFrames != null) {
        for (int i = allFrames.length - 1; i >= 0; i--) {
          TrPostDataListener l = null;
          if (allFrames[i] instanceof TrPostDataListener) {
            l = (TrPostDataListener) allFrames[i];
          } else if (allFrames[i].getContentPane() instanceof TrPostDataListener) {
            l = (TrPostDataListener) allFrames[i].getContentPane();
          }
          if (l != null) {
            res.add(l);
          }
        }
      }
    }
    boolean mustClean = false;
    for (WeakReference<TrPostDataListener> weakRef : weakListener) {
      TrPostDataListener trPostDataListener = weakRef.get();
      if (trPostDataListener != null) {
        res.add(trPostDataListener);
      } else {
        mustClean = true;
      }
    }
    if (mustClean) {
      for (Iterator<WeakReference<TrPostDataListener>> iterator = weakListener.iterator(); iterator.hasNext();) {
        if (iterator.next().get() == null) {
          iterator.remove();
        }
      }

    }
    return res;

  }

  public void removeWeakListener(TrPostDataListener listener) {
    for (Iterator<WeakReference<TrPostDataListener>> iterator = weakListener.iterator(); iterator.hasNext();) {
      if (iterator.next().get() == listener) {
        iterator.remove();
      }
    }
  }

  @Override
  public void dataAdded(final boolean _isFleche) {
    Collection<TrPostDataListener> listeners = getListeners();
    for (TrPostDataListener trPostDataListener : listeners) {
      trPostDataListener.dataAdded(_isFleche);
    }
    projet.setProjectModified();
  }

  @Override
  public void dataChanged(final H2dVariableType _old, final H2dVariableType _new, final boolean _contentChanged,
      final boolean _isFleche, final Set _varDepending) {
    Collection<TrPostDataListener> listeners = getListeners();
    for (TrPostDataListener trPostDataListener : listeners) {
      trPostDataListener.dataChanged(_old, _new, _contentChanged, _isFleche, _varDepending);
    }
    projet.setProjectModified();
  }

  @Override
  public void dataRemoved(final H2dVariableType[] _vars, final boolean _isFleche) {
    Collection<TrPostDataListener> listeners = getListeners();
    for (TrPostDataListener trPostDataListener : listeners) {
      trPostDataListener.dataRemoved(_vars, _isFleche);
    }
    projet.setProjectModified();
  }

}