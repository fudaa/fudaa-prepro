/*
 *  @creation     8 ao�t 2005
 *  @modification $Date: 2007-04-16 16:35:32 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.post;

import java.util.HashMap;
import java.util.Map;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.nfunk.jep.Variable;

/**
 * Un fournisseur d'expression pour les donn�es r�sultats.
 * 
 * @author Fred Deniger
 * @version $Id: TrPostExprDataSupplier.java,v 1.8 2007-04-16 16:35:32 deniger Exp $
 */
public class TrPostExprDataSupplier implements EbliFindExpressionContainerInterface {

  final TrIsoModelAbstract model_;
  final Map varSrc_;

  /**
   * @param _src
   */
  public TrPostExprDataSupplier(final TrIsoModelAbstract _src) {
    super();
    model_ = _src;
    varSrc_ = new HashMap();
  }

  @Override
  public void initialiseExpr(final CtuluExpr _exp) {
    varSrc_.clear();
    if (model_ != null) {
      for (int i = model_.varModel_.getSize() - 1; i >= 0; i--) {
        final H2dVariableType vi = (H2dVariableType) model_.varModel_.getElementAt(i);
        varSrc_.put(_exp.addVar(vi.getShortName(), vi.getName()), vi);
      }
    }
  }

  @Override
  public void majVariable(final int _idx, final Variable[] _varToUpdate) {
    if (_varToUpdate != null) {
      for (int i = _varToUpdate.length - 1; i >= 0; i--) {
        final H2dVariableType vari = (H2dVariableType) varSrc_.get(_varToUpdate[i]);
        if (vari != null) {
          _varToUpdate[i].setValue(CtuluLib.getDouble(model_.getValueFor(vari, _idx)));
        }
      }
    }

  }

}
