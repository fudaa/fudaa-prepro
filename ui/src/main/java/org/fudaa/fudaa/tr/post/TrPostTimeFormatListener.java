package org.fudaa.fudaa.tr.post;

import org.fudaa.ctulu.CtuluNumberFormatI;

public interface TrPostTimeFormatListener {
  
  
  void timeFormatChanged(TrPostTimeModel src,CtuluNumberFormatI oldFmt,CtuluNumberFormatI newFmt);

}
