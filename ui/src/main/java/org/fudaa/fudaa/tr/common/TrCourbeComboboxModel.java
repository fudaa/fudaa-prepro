/**
 *  @creation     25 juin 2004
 *  @modification $Date: 2007-05-04 14:01:46 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.common;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import org.fudaa.dodico.h2d.rubar.H2dRubarParameters;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeModel;

/**
 * @author Fred Deniger
 * @version $Id: TrCourbeComboboxModel.java,v 1.15 2007-05-04 14:01:46 deniger Exp $
 */
public final class TrCourbeComboboxModel extends AbstractListModel implements ComboBoxModel {
  transient EvolutionReguliereInterface first_;
  transient EGGroup g1_;
  transient EGGroup g2_;
  transient EvolutionReguliereInterface second_;

  transient Object selected_;

  public TrCourbeComboboxModel(final EGGroup _g1) {
    this(null, null, _g1, null);
  }

  public TrCourbeComboboxModel(final EGGroup _g1, final EGGroup _g2) {
    this(null, null, _g1, _g2);
  }

  public TrCourbeComboboxModel(final EvolutionReguliereInterface _first, final EvolutionReguliereInterface _second,
      final EGGroup _g1, final EGGroup _g2) {
    g1_ = _g1;
    g2_ = _g2;
    first_ = _first;
    second_ = _second;
  }

  protected EvolutionReguliereInterface getEvol(final EGGroup _g, final int _idx) {
    final EGCourbe i = (EGCourbe) _g.getChildAt(_idx);
    return ((FudaaCourbeModel) i.getModel()).getEvolution();
  }

  public boolean containsRubarNonTorrentiel() {
    return (first_ == H2dRubarParameters.NON_TORENTIEL) || (second_ == H2dRubarParameters.NON_TORENTIEL);
  }

  @Override
  public Object getElementAt(final int _index) {
    if (_index < 0) { return null; }
    int firstIdx = 0;
    if (first_ != null) {
      if (_index == 0) { return first_; }
      firstIdx++;
    }
    if (second_ != null) {
      if (_index == 1) { return second_; }
      firstIdx++;
    }
    if (g1_ != null) {
      if (_index < (firstIdx + g1_.getChildCount())) { return getEvol(g1_, _index - firstIdx); }
      firstIdx += g1_.getChildCount();
    }
    if (g2_ != null && _index < (firstIdx + g2_.getChildCount())) { return getEvol(g2_, _index - firstIdx); }
    return null;
  }

  @Override
  public Object getSelectedItem() {
    return selected_;
  }

  @Override
  public int getSize() {
    int r = 0;
    if (first_ != null) {
      r++;
    }
    if (second_ != null) {
      r++;
    }
    if (g1_ != null) {
      r += g1_.getChildCount();
    }
    if (g2_ != null) {
      r += g2_.getChildCount();
    }
    return r;
  }

  @Override
  public void setSelectedItem(final Object _anItem) {
    if (_anItem != selected_) {
      selected_ = _anItem;
      fireContentsChanged(this, -1, -1);
    }
  }
}