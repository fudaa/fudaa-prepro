/*
 GPL 2
 */
package org.fudaa.fudaa.tr.post.replay;

import org.locationtech.jts.geom.LineString;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeDefault;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.tr.post.TrPostVisuPanel;

/**
 *
 * @author Frederic Deniger
 */
public final class TrReplayLineEditCustomizer implements TrReplayVisuCustomizer {
  final LineString listePoints;
  final String title;

  /**
   * @param listePoints
   */
  /**
   * @param listePoints
   */
  public TrReplayLineEditCustomizer(final LineString listePoints, String title) {
    super();
    this.listePoints = listePoints;
    this.title = title;
  }

  @Override
  public void customizeVisu(final TrPostVisuPanel vue2d) {
    final GISZoneCollectionLigneBrisee ligneBrisee = new GISZoneCollectionLigneBrisee();
    ligneBrisee.addCoordinateSequence(listePoints.getCoordinateSequence(), null, null);
    final ZModeleLigneBriseeDefault modeleDefault = new ZModeleLigneBriseeDefault(ligneBrisee);
    final ZCalqueLigneBrisee layer = new ZCalqueLigneBrisee(modeleDefault);
    layer.setTitle(title);
    layer.setVisible(true);
    layer.setIconModel(0, TrReplayVue2dBuilder.createTraceIconModel());
    layer.setEnabled(true);
    final BCalque resultatsCalque = vue2d.getCalqueActif();
    vue2d.getGroupGIS().add(layer);
    vue2d.setCalqueActif(resultatsCalque);
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        vue2d.getSondeAction().setSelected(true);
        if (listePoints != null) {
          for (int i = 0; i < listePoints.getNumPoints(); i++) {
            final GrPoint replayPOint = new GrPoint(listePoints.getCoordinateN(i).x, listePoints.getCoordinateN(i).y, 0);
            vue2d.addSondeToCalque(replayPOint);
          }
        }
      }
    });
  }
  
}
