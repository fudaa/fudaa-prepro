/**
 * @creation 10 juin 2004
 * @modification $Date: 2007-05-04 14:01:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.rubar;

import com.memoire.fu.FuEmptyArrays;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesConfigure;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.meshviewer.layer.MvFrontierLayer;
import org.fudaa.fudaa.meshviewer.layer.MvFrontierLayerAbstract;
import org.fudaa.fudaa.meshviewer.model.Mv3DFrontierData;
import org.fudaa.fudaa.tr.data.TrBcBoundaryLayer;
import org.fudaa.fudaa.tr.data.TrBcBoundaryLegendPanel;
import org.fudaa.fudaa.tr.data.TrBordTraceLigneData;
import org.fudaa.fudaa.tr.data.TrBoundaryLineEditor;

/**
 * @author Fred Deniger
 * @version $Id: TrRubarBcAreteLayer.java,v 1.26 2007-05-04 14:01:53 deniger Exp $
 */
public class TrRubarBcAreteLayer extends TrRubarAreteLayer implements TrBcBoundaryLayer, MvFrontierLayer {

  TrBordTraceLigneData bdTlData_;

  TrBoundaryLineEditor editor_;

  /**
   * @param _m le modele
   * @param _l la legende associee
   */
  public TrRubarBcAreteLayer(final TrRubarBcAreteModel _m, final BCalqueLegende _l) {
    super(_m);
    setLegende(_l);
    _l.ajouteLegendPanel(new TrBcBoundaryLegendPanel(this));
  }

  private TrRubarBcAreteModel getBcModel() {
    return (TrRubarBcAreteModel) modeleDonnees();
  }

  @Override
  protected boolean updateTraceLigne(final TraceLigneModel _ligne, final TraceIconModel _icone, final GrSegment _s,
      final int _idx) {
    final TraceLigneModel d = getTraceLigne(_idx);
    if (d != null) {
      _ligne.updateData(d);
      return true;
    }
    return false;
  }

  @Override
  public void selectEdgesFromSameType() {
    this.selectEdgesFromSameType(getBcModel());
  }

  @Override
  public void boundaryTypeRendererChanged() {
    updateLegende();
    repaint();
  }

  public EbliActionInterface create3DAction(final CtuluUI _ui, final CtuluCommandContainer _cmd) {
    return new MvFrontierLayerAbstract.EditAction(this, _cmd, _ui);
  }

  @Override
  public Mv3DFrontierData createData() {
    return new Mv3DFrontierData(getBcModel().getFr());
  }

  @Override
  public List getAllBoundaryType() {
    return getBcModel().getBordList();
  }

  @Override
  public TrBordTraceLigneData getBordTrace() {
    if (bdTlData_ == null) {
      bdTlData_ = new TrBordTraceLigneData();
    }
    return bdTlData_;
  }

  @Override
  protected BConfigurableInterface getAffichageConf() {
    if (editor_ == null) {
      editor_ = new TrBoundaryLineEditor(this);
    }
    return new BConfigurableComposite(new ZCalqueAffichageDonneesConfigure(this), editor_, EbliLib.getS("Affichage"));
  }

  @Override
  public LineString getSelectedLine() {
    if (isSelectionEmpty()) { return null; }
    final int[] frSelected = getSelectedFrontier();
    if (CtuluLibArray.isEmpty(frSelected) || frSelected.length > 2) { return null; }
    if (frSelected.length == 1) {
      final int idxFr = frSelected[0];
      final TrRubarAreteModel m = getRubarModel();
      final EfFrontierInterface fr = getBcModel().getFr();
      final int nbPt = fr.getNbPt(idxFr);
      final TIntHashSet set = new TIntHashSet(nbPt);
      final int max = selection_.getMaxIndex();
      for (int i = selection_.getMinIndex(); i <= max; i++) {
        set.add(fr.getQuickIdxOnFrontier(m.getRubarArete(i).getPt1Idx(), idxFr));
        set.add(fr.getQuickIdxOnFrontier(m.getRubarArete(i).getPt2Idx(), idxFr));
      }
      final int[] selected = set.toArray();
      Arrays.sort(selected);
      final int[] res = CtuluListSelection.isSelectionContiguous(new CtuluListSelection(selected), nbPt);
      if (res != null) {
        final List coordinate = new ArrayList();
        // selection normale
        if (res[0] < res[1]) {
          addCoordinate(idxFr, coordinate, res[0], res[1]);
        } else {
          // fait le tour
          addCoordinate(idxFr, coordinate, res[0], nbPt);
          addCoordinate(idxFr, coordinate, 0, res[1]);
        }
        return GISGeometryFactory.INSTANCE.createLineString((Coordinate[]) coordinate.toArray(new Coordinate[coordinate
            .size()]));
      }

    }

    return super.getSelectedLine();
  }

  private void addCoordinate(final int _frIdx, final List _coordinate, final int _idx1, final int _idx2) {
    final TrRubarAreteModel m = getRubarModel();
    final EfFrontierInterface fr = getBcModel().getFr();
    for (int j = _idx1; j <= _idx2; j++) {
      final int idxGlobal = fr.getIdxGlobal(_frIdx, j);
      _coordinate.add(new Coordinate(m.getXFromGlobalIdx(idxGlobal), m.getYFromGlobalIdx(idxGlobal)));
    }
  }

  @Override
  public int[] getSelectedFrontier() {
    if (isSelectionEmpty()) { return FuEmptyArrays.INT0; }
    final EfFrontierInterface fr = getBcModel().getFr();
    final TIntHashSet set = new TIntHashSet();
    final TrRubarAreteModel m = getRubarModel();
    final int max = selection_.getMaxIndex();
    for (int i = selection_.getMinIndex(); i <= max; i++) {
      if (selection_.isSelected(i)) {
        set.add(fr.getIdxFrontierFromGlobal(m.getRubarArete(i).getPt1Idx()));
      }
    }
    final int[] res = set.toArray();
    Arrays.sort(res);
    return res;
  }

  public int[] getSelectedGlobalIndex() {
    if (isSelectionEmpty()) { return null; }
    final int max = selection_.getMaxIndex();
    final TIntArrayList l = new TIntArrayList(max);
    final TrRubarBcAreteModel m = getBcModel();
    for (int i = selection_.getMinIndex(); i <= max; i++) {
      if (selection_.isSelected(i)) {
        l.add(m.getGlobalIdx(i));
      }
    }
    return l.toNativeArray();
  }

  @Override
  public TraceLigneModel getTlData(final H2dBoundaryType _t) {
    return getBordTrace().getTlData(_t);
  }

  public TraceLigneModel getTraceLigne(final int _idx) {
    return getTlData(getBcModel().getRubarArete(_idx).getType());
  }

  @Override
  public List getUsedBoundaryType() {
    return getBcModel().getUsedBoundaryType();
  }

  @Override
  public final boolean isConfigurable() {
    return true;
  }

  @Override
  public boolean isFontModifiable() {
    return false;
  }

  @Override
  public boolean isInternFr(final int _idxFr) {
    return !getBcModel().getFr().isExtern(_idxFr);
  }

  @Override
  public boolean isPaletteModifiable() {
    return false;
  }

  @Override
  public void clearCache() {
    super.clearCache();
  }

  /**
   * Met a jour la legende en fonction des bords utilises.
   */
  public void updateLegende() {
    final BCalqueLegende l = getLegende();
    if (l != null) {
      final TrBcBoundaryLegendPanel p = (TrBcBoundaryLegendPanel) l.getLegende(this);
      p.init(this);
      l.revalidate();
      l.repaint();
    }
  }
}
