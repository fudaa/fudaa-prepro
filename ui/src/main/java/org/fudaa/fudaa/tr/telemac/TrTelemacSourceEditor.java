/**
 * @creation 30 nov. 2004
 * @modification $Date: 2007-03-30 15:40:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.tr.telemac;

import com.memoire.bu.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.dico.DicoDataType;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSourceMng;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.calque.ZModelePoint;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.fdico.FDicoEditorInterface;
import org.fudaa.fudaa.fdico.FDicoTableEditorChooser;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.tr.common.TrResource;
import org.fudaa.fudaa.tr.data.TrVisuPanel;

/**
 * @author Fred Deniger
 * @version $Id: TrTelemacSourceEditor.java,v 1.14 2007-03-30 15:40:30 deniger Exp $
 */
public final class TrTelemacSourceEditor extends CtuluDialogPanel implements ActionListener {

  final CtuluCommandContainer cmdMng_;
  JDialog dial_;
  FDicoTableEditorChooser.EditorCreator creator_;
  FDicoEditorInterface[] edits_;
  FDicoEditorInterface[] editsTracers_;
  boolean editXY_;
  H2dTelemacSourceMng mng_;
  BuButton btEdit_;

  int[] selectedIdx_;
  List sourceProps_;
  final TrVisuPanel pn_;
  final TrTelemacSourceLayer layer_;

  /**
   * @param _selectedIdx les siphons selectionnes. Si null on considere que c'est une creation
   * @param _siphonMng le manageur de siphon
   * @param _cmd les commandes
   */
  public TrTelemacSourceEditor(final int[] _selectedIdx, final H2dTelemacSourceMng _siphonMng, final TrVisuPanel _panel) {
    setLayout(new BuGridLayout(2, 5, 5));
    selectedIdx_ = _selectedIdx;
    if (selectedIdx_ != null) {
      addLabel(TrResource.getS("Nombre de sources edit�es"));
      addLabel(CtuluLibString.getString(selectedIdx_.length));
    }
    pn_ = _panel;
    mng_ = _siphonMng;
    layer_ = (TrTelemacSourceLayer) pn_.getArbreCalqueModel().getSelectedCalque();
    cmdMng_ = _panel.getCmdMng();
    sourceProps_ = mng_.getKeywordForSource();
    creator_ = new FDicoTableEditorChooser.EditorCreator();
    final String[] initVal = new String[sourceProps_.size()];
    // creation siphon
    if (selectedIdx_ == null) {
      editXY_ = false;
      for (int i = 0; i < initVal.length; i++) {
        initVal[i] = mng_.getDefaultValue((DicoEntite) sourceProps_.get(i));
      }
    } else {
      editXY_ = true;
      for (int i = 0; i < initVal.length; i++) {
        initVal[i] = mng_.getCommonValues(selectedIdx_, (DicoEntite.Vecteur) sourceProps_.get(i));
      }
    }
    int allValuesButTracer = sourceProps_.size() - 1;
    edits_ = new FDicoEditorInterface[allValuesButTracer];
    if (editXY_) {
      addLabel(TrResource.getS("Modifier les coordonn�es"));
      btEdit_ = new BuButton(TrResource.getS("Commencer la s�lection"));
      btEdit_.addActionListener(this);
      add(btEdit_);

      edits_[0] = addEditFor((DicoEntite) sourceProps_.get(0), initVal[0]);
      edits_[0].getComponent().setEnabled(false);
      edits_[1] = addEditFor((DicoEntite) sourceProps_.get(1), initVal[1]);
      edits_[1].getComponent().setEnabled(false);
    }
    for (int i = 2; i < allValuesButTracer; i++) {//-1 to avoid tracers.
      final DicoEntite.Vecteur v = (DicoEntite.Vecteur) sourceProps_.get(i);
      // si la valeur est deja intialise ou s'il est demandee par defaut.
      if (mng_.isValueSet(v) || (mng_.getNbSource() == 0 && mng_.isSetByDefault(i))) {
        edits_[i] = addEditFor(v, initVal[i]);
      }
    }
    int nbTracers = mng_.getNbTraceurs();
    editsTracers_ = new FDicoEditorInterface[nbTracers];
    String[] values = mng_.getTracerValuesOnSourcesDicoEntite().getValues(initVal[initVal.length - 1]);
    for (int idxTracer = 0; idxTracer < editsTracers_.length; idxTracer++) {
      final FDicoEditorInterface r = creator_.createEditor(mng_.getTracerValuesOnSourcesDicoEntite().getType());
      editsTracers_[idxTracer] = r;
      add(new BuLabel(mng_.getTraceurName(idxTracer)));
      if (values != null && values.length > idxTracer) {
        r.setValue(values[idxTracer]);
      }
      add(r.getComponent());

    }
  }

  protected FDicoEditorInterface addEditFor(final DicoEntite _ent, final String _val) {
    final FDicoEditorInterface r = creator_.createEditor(_ent.getType());
    add(new BuLabel(_ent.getNom()));
    r.setValue(_val);
    add(r.getComponent());
    return r;
  }

  @Override
  public boolean apply() {
    final String[] values = getValues();
    if (selectedIdx_.length == 1) {
      mng_.modifyVecteur(selectedIdx_[0], values, cmdMng_);
    } else {
      final CtuluCommandComposite cmd = new CtuluCommandComposite();
      for (int i = selectedIdx_.length - 1; i >= 0; i--) {
        mng_.modifyVecteur(selectedIdx_[i], values, cmd);
      }
      if (cmdMng_ != null) {
        cmdMng_.addCmd(cmd.getSimplify());
      }
    }
    return true;
  }

  protected String getValueFor(final int _i) {
    return getValue(edits_[_i]);
  }

  private String getValue(FDicoEditorInterface fDicoEditorInterface) {
    if (fDicoEditorInterface == null || fDicoEditorInterface.getValue().trim().length() == 0) {
      return null;
    }
    return fDicoEditorInterface.getValue();
  }

  public String[] getValues() {
    final String[] values = new String[edits_.length + 1];//for tracer

    if (editXY_) {
      values[0] = getValueFor(0);
      values[1] = getValueFor(1);
    }
    for (int i = 2; i < edits_.length; i++) {
      values[i] = getValueFor(i);
    }
    if (editsTracers_ != null) {
      String[] vals = new String[editsTracers_.length];
      for (int i = 0; i < vals.length; i++) {
        vals[i] = getValue(editsTracers_[i]);
        if (vals[i] == null) {
          vals[i] = CtuluLibString.ESPACE;
        }
      }
      values[values.length - 1] = mng_.getTracerValuesOnSourcesDicoEntite().getStringValuesArray(vals);
    }

    return values;
  }

  @Override
  public boolean isDataValid() {
    boolean r = true;
    FDicoEditorInterface[] editorInterfaces = edits_;
    for (int i = editorInterfaces.length - 1; i >= 0; i--) {
      final DicoEntite ent = (DicoEntite) sourceProps_.get(i);
      final String newVal = getValueFor(i);
      if (newVal != null && (!ent.getType().isValide(newVal))) {
        r = false;
        editorInterfaces[i].getComponent().setForeground(Color.RED);
      } else if (editorInterfaces[i] != null) {
        editorInterfaces[i].getComponent().setForeground(Color.BLACK);
      }
    }
    editorInterfaces = editsTracers_;
    final DicoEntite ent = mng_.getTracerValuesOnSourcesDicoEntite();
    for (int i = editorInterfaces.length - 1; i >= 0; i--) {
      final String newVal = getValue(editsTracers_[i]);
      final DicoDataType type = ent.getType();
      if (newVal != null && (!type.isValide(newVal))) {
        r = false;
        editorInterfaces[i].getComponent().setForeground(Color.RED);
      } else if (editorInterfaces[i] != null) {
        editorInterfaces[i].getComponent().setForeground(Color.BLACK);
      }
    }
    if (!r) {
      setErrorText(TrResource.getS("Des valeurs sont erron�es"));
      Toolkit.getDefaultToolkit().beep();
    }
    return r;

  }

  public JDialog getDial() {
    return dial_;
  }

  public void setDial(final JDialog _dial) {
    dial_ = _dial;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    dial_.setVisible(false);

    final BuPanel top = new BuPanel();
    top.setLayout(new BuVerticalLayout());
    final BuLabel lb = new BuLabel(TrResource.getS("Choisir un point en double-cliquant dessus"));
    top.add(lb);
    final BuLabel lbInfo = new BuLabel(CtuluLibString.ESPACE);
    top.add(lbInfo);

    final BuPanel pn = new BuPanel();

    pn.setLayout(new BuGridLayout(2, 5, 5));
    final BuLabel lbX = new BuLabel(getValueFor(0));
    final BuLabel lbY = new BuLabel(getValueFor(1));
    pn.add(new BuLabel("X: "));
    pn.add(lbX);
    pn.add(new BuLabel("Y: "));
    pn.add(lbY);
    final BuButton btOk = new BuButton(BuResource.BU.getString("Valider"));
    final BuButton btCancel = new BuButton(BuResource.BU.getString("Annuler"));
    final BuPanel pnBt = new BuPanel();
    pnBt.setLayout(new BuButtonLayout());
    pnBt.add(btOk);
    pnBt.add(btCancel);

    btOk.setEnabled(false);
    final JDialog dial = new JDialog(pn_.getFrame());
    dial.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    dial.setTitle(TrResource.getS("Sources"));
    dial.setResizable(true);
    final BuPanel content = new BuPanel();
    content.setLayout(new BuBorderLayout(0, 3));
    content.setBorder(BuBorders.EMPTY5555);
    content.add(top, BuBorderLayout.NORTH);
    pn.setBorder(BuBorders.EMPTY0505);
    content.add(pn, BuBorderLayout.CENTER);
    content.add(pnBt, BuBorderLayout.SOUTH);
    dial.setContentPane(content);

    // dial.setUndecorated(true);
    dial.pack();
    final Point pos = pn_.getLocationOnScreen();
    dial.setLocation(pos.x, pos.y - dial.getHeight() - 10);
    dial.setVisible(true);
    // pn_.requestFocus();
    final GrPoint p = new GrPoint();
    final ZModelePoint modele = pn_.getGridGroup().getPointLayer().modele();
    final MouseAdapter mouseAdapter = new MouseAdapter() {

      @Override
      public void mouseClicked(final MouseEvent _evt) {
        p.x_ = _evt.getX();
        p.y_ = _evt.getY();
        p.autoApplique(layer_.getVersReel());
        if (_evt.getClickCount() == 2 && SwingUtilities.isLeftMouseButton(_evt)) {
          final int i = ZCalquePoint.getSelectedPoint(modele, p, 5, layer_.getVersReel(), pn_.getVueCalque().getViewBoite());
          btOk.setEnabled(i >= 0);
          if (i < 0) {
            lbInfo.setForeground(Color.RED);
            lbInfo.setText(MvResource.getS("Acun noeud trouv�"));
            layer_.setTempoNode(null);
          } else {
            lbInfo.setForeground(CtuluLibSwing.getDefaultLabelForegroundColor());
            lbInfo.setText(MvResource.getS("Noeud trouv�"));
            lbX.setText(CtuluLib.DEFAULT_NUMBER_FORMAT.format(modele.getX(i)));
            lbY.setText(CtuluLib.DEFAULT_NUMBER_FORMAT.format(modele.getY(i)));
            layer_.setTempoNode(p);
          }

        }
        layer_.repaint();
      }

    };
    pn_.getVueCalque().addMouseListener(mouseAdapter);
    final TreeSelectionListener treeBlocker = new TreeSelectionListener() {

      @Override
      public void valueChanged(final TreeSelectionEvent _treeEvent) {
        lb.setForeground(Color.RED);
        final Font font = lb.getFont();
        lb.setFont(BuLib.deriveFont(font, Font.BOLD, 0));
        final Timer timer = new Timer(0, new ActionListener() {

          @Override
          public void actionPerformed(final ActionEvent _timerEvent) {
            lb.setForeground(CtuluLibSwing.getDefaultLabelForegroundColor());
            lb.setFont(font);

          }

        });
        timer.setRepeats(false);
        timer.setInitialDelay(1000);
        timer.start();
        pn_.getArbreCalqueModel().setSelectionCalque(layer_);
      }

    };
    btCancel.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _act) {
        pn_.getVueCalque().removeMouseListener(mouseAdapter);
        pn_.getArbreCalqueModel().removeTreeSelectionListener(treeBlocker);
        dial.setVisible(false);
        dial.dispose();
        layer_.setTempoNode(null);
        dial_.setVisible(true);
      }
    });
    btOk.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _act) {
        edits_[0].setValue(lbX.getText());
        edits_[1].setValue(lbY.getText());
        layer_.setTempoNode(null);
        pn_.getVueCalque().removeMouseListener(mouseAdapter);
        pn_.getArbreCalqueModel().removeTreeSelectionListener(treeBlocker);
        dial.setVisible(false);
        dial.dispose();
        dial_.setVisible(true);
      }
    });

    pn_.getArbreCalqueModel().addTreeSelectionListener(treeBlocker);

  }
}
