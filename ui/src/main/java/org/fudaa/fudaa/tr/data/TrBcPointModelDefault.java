/**
 * @creation 11 sept. 2003 
 * @modification $Date: 2007-01-19 13:14:08 $
 * @license GNU General Public License 2 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne 
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.tr.data;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.fudaa.meshviewer.model.MvFrontierModelDefault;

/**
 * @author deniger
 * @version $Id: TrBcPointModelDefault.java,v 1.11 2007-01-19 13:14:08 deniger Exp $
 */
public class TrBcPointModelDefault extends MvFrontierModelDefault {

  /**
   * @param _m le maillage a considerer
   */
  public TrBcPointModelDefault(final EfGridInterface _m) {
    super(_m);
  }

}