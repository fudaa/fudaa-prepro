package org.fudaa.fudaa.fgrid.common;

import javax.swing.JComponent;
import javax.swing.table.DefaultTableCellRenderer;

import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;

@SuppressWarnings("serial")
public class GroupObjectsEditorPanel extends CtuluDialogPanel
{
  private static class GroupObjectsEditorModel extends CtuluListEditorModel
  {
    private final GroupObjectManager manager;

    public GroupObjectsEditorModel(GroupObjectManager manager)
    {
      this.manager = manager;
    }

    @Override
    public Object createNewObject()
    {
      GroupObjectDialogPanel panel = new GroupObjectDialogPanel(this.manager);
      
      panel.afficheModale((JComponent)null);
      
      return panel.getGroupObject();
    }

    @Override
    public void edit(int r)
    {
      GroupObjectDialogPanel panel = new GroupObjectDialogPanel(this.manager, (GroupObject)this.v_.get(r));
      
      panel.afficheModale((JComponent)null);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      return false;
    }

    @Override
    protected void actionBeforeRemove(int i)
    {
      GroupObject groupObject = (GroupObject)this.v_.get(i);
      
      this.manager.removeGroupObject(groupObject);
    }
  }
  
  private final GroupObjectManager manager;
  private final GroupObjectsEditorModel model;
  
  public GroupObjectsEditorPanel(GroupObjectManager manager)
  {
    this.manager = manager;
    this.model = new GroupObjectsEditorModel(this.manager.copy());
    
    CtuluListEditorPanel editor = new CtuluListEditorPanel(this.model, true, true, false, true, false);
    
    editor.setDoubleClickEdit(true);
    
    editor.setValueListCellRenderer(new DefaultTableCellRenderer()
    {

      @Override
      protected void setValue(Object value)
      {
        this.setText(((GroupObject)value).getName());
      }
    });
    
    this.add(editor);
  }

  @Override
  public boolean apply()
  {
    this.manager.initialize(this.model.manager);
    return true;
  }
}
