package org.fudaa.fudaa.fgrid.layer;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;

/**
 * Helper to navigation in Layers.
 * 
 * @author deniger
 */
public final class LayerHelper {

  private LayerHelper() {

  }

  public static class TypePredicate implements Predicate {

    final EnumLayerDataType searchType;

    /**
     * @param searchType
     */
    public TypePredicate(EnumLayerDataType searchType) {
      super();
      this.searchType = searchType;
    }

    @Override
    public boolean evaluate(Object object) {
      return GeometricLayerBuilder.getType((ZCalqueAffichageDonnees) object) == searchType;
    }
  }

  /**
   * @param <T>
   * @param collection
   * @param dataToSearch
   * @return the first layer found with the type <code>dataToSearch</code>
   */
  @SuppressWarnings("unchecked")
  public static <T extends ZCalqueAffichageDonnees> T findLayerOfType(Collection<T> collection,
      EnumLayerDataType dataToSearch) {
    return (T) CollectionUtils.find(collection, new TypePredicate(dataToSearch));
  }

  /**
   * @param <T>
   * @param collection
   * @param dataToSearch
   * @return the first layer found
   */
  public static <T extends ZCalqueAffichageDonnees> Collection<T> selectLayersOfType(Collection<T> collection,
      EnumLayerDataType dataToSearch) {
    Collection<T> res = new ArrayList<T>();
    CollectionUtils.select(collection, new TypePredicate(dataToSearch), res);
    return res;
  }

}
