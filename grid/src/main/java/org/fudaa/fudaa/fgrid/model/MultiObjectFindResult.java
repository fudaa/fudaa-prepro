package org.fudaa.fudaa.fgrid.model;

import java.util.ArrayList;
import java.util.List;

public class MultiObjectFindResult<S, O>
{
  private final S support;
  private final List<O> indexes;
  
  public MultiObjectFindResult(S support)
  {
    this.support = support;
    this.indexes = new ArrayList<O>();
  }
  
  public boolean addObject(O object)
  {
    if (this.indexes.contains(object))
    {
      return false;
    }
    
    this.indexes.add(object);
    
    return true;
  }
  
  public boolean removeObject(O object)
  {
    if (!this.indexes.contains(object))
    {
      return false;
    }
    
    this.indexes.remove(this.indexes.indexOf(object));
    
    return true;
  }
  
  public boolean isEmpty()
  {
    return this.indexes.isEmpty();
  }
  
  public S getSupport()
  {
    return this.support;
  }
  
  public List<O> getObjects()
  {
    List<O> objects = new ArrayList<O>(this.indexes);
    
    return objects;
  }
  
  public int getNbObjects()
  {
    return this.indexes.size();
  }
  
  public boolean containsObject(O object)
  {
    return this.indexes.contains(object);
  }
}
