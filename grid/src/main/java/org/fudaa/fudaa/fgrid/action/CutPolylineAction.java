package org.fudaa.fudaa.fgrid.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.fgrid.layer.ContextGeometricLayersImpl;

@SuppressWarnings("serial")
public class CutPolylineAction extends EbliActionSimple
{
  final ContextGeometricLayersImpl context;
  final CtuluCommandContainer cmdContainer;
  
  public CutPolylineAction(final ContextGeometricLayersImpl context, CtuluCommandContainer cmdContainer) {
    super(Messages.getString("cutPolylineAction.actionText"), EbliResource.EBLI.getToolIcon("draw-add-pt"), "CUT_POLYLINE"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    this.context = context;
    this.cmdContainer = cmdContainer;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    PolylineCutter cutter = new PolylineCutter(this.context.getSelectionInLineLayers(), this.cmdContainer);

    cutter.process();
  }
}
