package org.fudaa.fudaa.fgrid.action;

import java.util.List;
import java.util.Set;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.fudaa.fgrid.model.LineFind;
import org.fudaa.fudaa.fgrid.model.MultiGroupLineFindResult;

public class DuplicateLineCleaner
{
  private List<ZCalqueLigneBriseeEditable> layers;
  private CtuluCommandContainer cmdContainer;
  private int nbLinesCleaned;
  
  public DuplicateLineCleaner(List<ZCalqueLigneBriseeEditable> layers, CtuluCommandContainer cmdContainer)
  {
    super();
    this.layers = layers;
    this.cmdContainer = cmdContainer;
    this.nbLinesCleaned = 0;
  }
  
  public void process()
  {
    this.nbLinesCleaned = 0;

    CtuluCommandComposite cmp = (this.cmdContainer == null) ? null : new CtuluCommandComposite();
    
    DuplicateLineDetector detector = new DuplicateLineDetector(layers);
    
    detector.process();
    
    MultiGroupLineFindResult result = detector.getResult();
    
    Set<ZCalqueLigneBriseeEditable> supports = result.getSupports();
    
    for (ZCalqueLigneBriseeEditable layer : supports)
    {
      List<LineFind> lines = result.getObjects(layer);
      int[] idxs = new int[lines.size()];
      
      for (int i = 0; i < idxs.length; i++)
      {
        idxs[i] = lines.get(i).getIdxLine();
      }
      
      layer.modeleDonnees().removeLigneBrisee(idxs, cmp);

      this.nbLinesCleaned += idxs.length;
    }

    if (this.cmdContainer != null) {
      this.cmdContainer.addCmd(cmp.getSimplify());
    }    
  }

  public int getNbLinesCleaned()
  {
    return nbLinesCleaned;
  }
}
