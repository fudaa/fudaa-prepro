package org.fudaa.fudaa.fgrid.layer;

import java.awt.Color;

import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gis.GISZoneListenerDispatcher;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.fgrid.model.AttributeConstants;
import org.fudaa.fudaa.fgrid.model.GeometricLigneBriseeModel;
import org.fudaa.fudaa.fgrid.model.GeometricPointModel;
import org.fudaa.fudaa.sig.layer.FSigEditor;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.sig.layer.FSigLayerPointEditable;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;

/**
 * Constructeur des calques g�om�triques.
 * Builder for geometric layer.
 * 
 * @author deniger
 */
public class GeometricLayerBuilder {

  private static String TYPE = "GRID_DATA_TYPE";

  /**
   * @param pn the parent visu panel
   * @return created group layer for geometric data.
   */
  protected static FSigLayerGroup createGeomtricGroupLayer(FSigVisuPanel pn) {
    FSigLayerGroup res = new FSigLayerGroup(pn);
    res.setTitle("Donn�es g�ometriques");
    res.setName("grGeometric");
    res.setActions(new EbliActionInterface[] { pn.getEditor().getExportAction() });
    return res;
  }

  /**
   * Build default geometric layers.
   * 
   * @param parent
   * @param ctx
   */
  protected static void buildDefault(FSigLayerGroup parent, ContextMainGeometryModule ctx) {
    GISAttribute[] createPointAttributes = createPointAttributes();
    createPointLayer(parent, ctx, createPointAttributes);
    // TODO traduction of Dure
    createPointDurLayer(parent, ctx, createPointAttributes);
    GISAttribute[] createLineAttributes = createLineAttributes();
    createLineLayer(parent, ctx, createLineAttributes);
    createLineDurLayer(parent, ctx, createLineAttributes);
    // il reste a traiter les surfaces.
  }

  /**
   * @param cq
   * @return le type du calque et null si non trouv�.
   */
  public static EnumLayerDataType getType(ZCalqueAffichageDonnees cq) {
    return (EnumLayerDataType) cq.getClientProperty(TYPE);
  }

  private static void createLineDurLayer(FSigLayerGroup parent, ContextMainGeometryModule ctx,
      GISAttribute[] createLineAttributes) {
    ZCalqueLigneBriseeEditable ligneDurLayer = (ZCalqueLigneBriseeEditable) parent.addLigneBriseeLayerAct(
        "Lignes dures", new GeometricLigneBriseeModel(ctx.getGeometricContext(), createZoneCollectionLigne(
            createLineAttributes, parent.getGisEditor())), null);
    ligneDurLayer.putClientProperty(TYPE, EnumLayerDataType.GEOMETRIC_LINE_DUR);
    ctx.getGeometricContext().register(ligneDurLayer);
  }

  private static void createLineLayer(FSigLayerGroup parent, ContextMainGeometryModule ctx, GISAttribute[] createLineAttributes) {
    ZCalqueLigneBriseeEditable ligneLayer = (ZCalqueLigneBriseeEditable) parent.addLigneBriseeLayerAct("Lignes",
        new GeometricLigneBriseeModel(ctx.getGeometricContext(), createZoneCollectionLigne(createLineAttributes, parent
            .getGisEditor())), null);
    ligneLayer.putClientProperty(TYPE, EnumLayerDataType.GEOMETRIC_LINE);
    ctx.getGeometricContext().register(ligneLayer);
  }

  private static void createPointDurLayer(FSigLayerGroup parent, ContextMainGeometryModule ctx,
      GISAttribute[] createPointAttributes) {
    ZCalquePointEditable pointDurLayer = parent.addPointLayerAct("Points durs", new GeometricPointModel(
        createZoneCollectionPoint(createPointAttributes, parent.getGisEditor()), ctx.getGeometricContext()), null);
    pointDurLayer.putClientProperty(TYPE, EnumLayerDataType.GEOMETRIC_POINT_DUR);
    ctx.getGeometricContext().register((FSigLayerPointEditable) pointDurLayer);
  }

  private static void createPointLayer(FSigLayerGroup parent, ContextMainGeometryModule ctx,
      GISAttribute[] createPointAttributes) {
    ZCalquePointEditable pointLayer = parent.addPointLayerAct("Points", new GeometricPointModel(
        createZoneCollectionPoint(createPointAttributes, parent.getGisEditor()), ctx.getGeometricContext()), null);
    // calque support.
    ctx.getGeometricContext().register((FSigLayerPointEditable) pointLayer, true);
    pointLayer.setIconModel(0, new TraceIconModel(TraceIcon.CROIX, 2, Color.BLACK));
    pointLayer.putClientProperty(TYPE, EnumLayerDataType.GEOMETRIC_POINT);
  }

  protected static GISZoneCollectionPoint createZoneCollectionPoint(GISAttributeInterface[] createPointAttributes,
      FSigEditor editor) {
    GISZoneListenerDispatcher l = new GISZoneListenerDispatcher();
    l.addListener(editor);
    GISZoneCollectionPoint pt = new GISZoneCollectionPoint(l);
    pt.setAttributes(createPointAttributes, null);
    return pt;
  }

  protected static GISZoneCollectionLigneBrisee createZoneCollectionLigne(
      GISAttributeInterface[] createPointAttributes, FSigEditor editor) {
    GISZoneListenerDispatcher l = new GISZoneListenerDispatcher();
    l.addListener(editor);
    GISZoneCollectionLigneBrisee pt = new GISZoneCollectionLigneBrisee(l);
    // TODO Pq createPointAttributes et pas createLineAttributes???
    pt.setAttributes(createPointAttributes, null);
    return pt;
  }

  private static GISAttribute[] createPointAttributes() {
    return new GISAttribute[] { AttributeConstants.ID, GISAttributeConstants.BATHY, AttributeConstants.GRID_PRECISION };
  }

  private static GISAttribute[] createLineAttributes() {
    return new GISAttribute[] { AttributeConstants.ID, AttributeConstants.POINT_ID, AttributeConstants.GRID_PRECISION };
  }
}
