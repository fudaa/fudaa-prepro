package org.fudaa.fudaa.fgrid.model;

import gnu.trove.TIntArrayList;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.gis.GISAttributeModelLongInterface;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gis.GISZoneListenerDispatcher;
import org.fudaa.ebli.calque.edition.ZEditionAttibutesContainer;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.fgrid.layer.ContextGeometricLayersImpl;

/**
 * Index for a node model: used to quick find the modele containing a node with a given id.
 * 
 * @author deniger
 */
public class GeometricPointModel extends ZModelePointEditable {

  final ContextGeometricLayers geoContext;

  final GeometricPointModelIndexer indexer;

  /**
   * @param zone
   */
  public GeometricPointModel(final GISZoneCollectionPoint zone, final ContextGeometricLayers geoContext) {
    super(zone);
    this.geoContext = geoContext;
    ((GISZoneListenerDispatcher) zone.getListener()).addListener(new GeometricLineFromPointUpdater(geoContext, this));
    indexer = new GeometricPointModelIndexer(getGeomData());
  }

  @Override
  public void addPoint(final GrPoint p, final ZEditionAttributesDataI initData, final CtuluCommandContainer cmd) {
    ZEditionAttributesDataI data = initData;
    if (data == null) {
      data = new ZEditionAttibutesContainer(getGeomData().getAttributes()).createPointData();
    }
    final Long id = (Long) data.getValue(AttributeConstants.ID, 0);
    // serait inutile surtout si c'est un nouveau point
    if (ContextGeometricLayersImpl.isNotDefinedID(id)) {
      data.setValue(AttributeConstants.ID, 0, geoContext.createPointId());
    }
    super.addPoint(p, data, cmd);
  }

  /**
   * @param p
   * @param data
   * @param cmd
   * @return l'id du point ajoute
   */
  public long addPointAndReturnId(final GrPoint p, final ZEditionAttributesDataI initData,
      final CtuluCommandContainer cmd) {
    ZEditionAttributesDataI data = initData;
    if (data == null) {
      data = new ZEditionAttibutesContainer(getGeomData().getAttributes()).createPointData();
    }
    final Long id = (Long) data.getValue(AttributeConstants.ID, 0);
    long res = -1l;
    if (ContextGeometricLayersImpl.isNotDefinedID(id)) {
      res = geoContext.createPointId();
      data.setValue(AttributeConstants.ID, 0, res);
    } else {
      res = id.longValue();
    }
    super.addPoint(p, data, cmd);
    return res;
  }

  @Override
  public boolean copyGlobal(final CtuluListSelectionInterface selection, final double dx, final double dy,
      final CtuluCommandContainer cmd) {
    // TODO a continuer pour les lignes utilisant ces points
    return super.copyGlobal(selection, dx, dy, cmd);
  }

  /**
   * @return the indexer
   */
  public GeometricPointModelIndexer getIndexer() {
    return indexer;
  }

  /**
   * @param idxNodeInThisLayer index of the node in this model
   * @return the global unique ID
   */
  public long getPointId(int idxNodeInThisLayer) {
    return ((GISAttributeModelLongInterface) getGeomData().getDataModel(
        getGeomData().getIndiceOf(AttributeConstants.ID))).getValue(idxNodeInThisLayer);
  }

  /**
   * @param pointId ID of the searched point
   * @return the position in this model and -1 if not found
   */
  public int getPositionOfPoint(long pointId) {
    return getIndexer().getPositionOfId(pointId);
  }

  @Override
  public boolean removePoint(final int[] idx, final CtuluCommandContainer cmd) {
    TIntArrayList removablePoints = new TIntArrayList();
    boolean isANodeUsed = false;
    // if the point is used by a node dont delete it
    for (int i = 0; i < idx.length; i++) {
      long nodeId = getPointId(idx[i]);

      if (geoContext.isPointUsedByALine(nodeId)) {
        isANodeUsed = true;
      } else {
        removablePoints.add(idx[i]);
      }
    }
    if (isANodeUsed) {
      if (removablePoints.isEmpty()) {
        // TODO i18n
        geoContext.getUi().error("Op�ration annul�e: tous les points sont utilis�s");
        return false;
      } else {
        // TODO i18n
        geoContext.getUi().warn("Noeuds utilis�s",
            "Op�ration partiellement effectu�e: des points sont utilis�s par des lignes", false);
      }
    }
    return super.removePoint(removablePoints.toNativeArray(), cmd);
  }

  @Override
  public boolean setPoint(final int idx, final double newX, final double newY, final CtuluCommandContainer cmd) {
    // la correspondance est ger�e par les listener.
    return super.setPoint(idx, newX, newY, cmd);
  }

}
