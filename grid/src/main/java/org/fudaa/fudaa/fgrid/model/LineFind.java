package org.fudaa.fudaa.fgrid.model;

public class LineFind
{
  /**
   * position of the found line
   */
  private int idxLine;
  /**
   * Position of the atom on the line. If <0 -> not used
   */
  private int idxPoint;
  /**
   * Idx of the segment on the line. If <0 -> not used
   */
  private int idxSegment;
  
  public LineFind()
  {
    this(-1, -1, -1);
  }
  
  public LineFind(int idxLine)
  {
    this(idxLine, -1, -1);
  }
  
  public LineFind(int idxLine, int idx, boolean isPoint)
  {
    this(idxLine, isPoint ? idx : -1, isPoint ? -1 : idx);
  }
  
  private LineFind(int idxLine, int idxPoint, int idxSegment)
  {
    super();
    this.idxLine = idxLine;
    this.idxPoint = idxPoint;
    this.idxSegment = idxSegment;
  }

  public int getIdxLine()
  {
    return idxLine;
  }

  public void setIdxLine(int idxLine)
  {
    this.idxLine = idxLine;
  }

  public int getIdxPoint()
  {
    return idxPoint;
  }

  public void setIdxPoint(int idxPoint)
  {
    this.idxPoint = idxPoint;
    
    if (this.usePoint() && this.useSegment())
    {
      this.idxSegment = -1;
    }
  }

  public int getIdxSegment()
  {
    return idxSegment;
  }

  public void setIdxSegment(int idxSegment)
  {
    this.idxSegment = idxSegment;

    if (this.usePoint() && this.useSegment())
    {
      this.idxPoint = -1;
    }
  }
  
  public boolean usePoint()
  {
    return this.idxPoint >= 0;
  }
  
  public boolean useSegment()
  {
    return this.idxSegment >= 0;
  }

  public boolean isEmpty()
  {
    return this.idxLine < 0;
  }
  
  @Override
  public int hashCode()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + idxLine;
    result = prime * result + idxPoint;
    result = prime * result + idxSegment;
    return result;
  }

  @Override
  public boolean equals(Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    LineFind other = (LineFind) obj;
    if (idxLine != other.idxLine)
      return false;
    if (idxPoint != other.idxPoint)
      return false;
    if (idxSegment != other.idxSegment)
      return false;
    return true;
  }
}
