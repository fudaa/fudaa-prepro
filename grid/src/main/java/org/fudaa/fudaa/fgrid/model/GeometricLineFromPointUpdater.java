package org.fudaa.fudaa.fgrid.model;

import java.util.Collection;

import org.locationtech.jts.geom.CoordinateSequence;

import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISCollection;
import org.fudaa.ctulu.gis.GISZoneListener;
import org.fudaa.ebli.calque.edition.ZEditorDefault;

/**
 * Update line Layer when point has been changed
 * 
 * @author deniger
 */
public class GeometricLineFromPointUpdater implements GISZoneListener {
  final ContextGeometricLayers context;
  final GeometricPointModel model;

  /**
   * @param context
   * @param model
   */
  public GeometricLineFromPointUpdater(ContextGeometricLayers context, GeometricPointModel model) {
    assert context != null;
    assert model != null;
    this.context = context;
    this.model = model;
  }

  public void attributeChanged(GISCollection src, GISAttributeInterface att) {}
  
  



  private void updateVertex(Collection<LineFindResult> toUpdate, double x, double y) {
    ZEditorDefault editorToRepaint = null;
    for (LineFindResult lineFindResult : toUpdate) {
      GeometricLigneBriseeModel modele = (GeometricLigneBriseeModel) lineFindResult.layer.modeleDonnees();
      CoordinateSequence coords = modele.getGeomData().getCoordinateSequence(lineFindResult.posOfLine);
      int vertexPos = lineFindResult.posOfSommet;
      if (context.getRepere().isDifferentPoint(x, y, coords.getX(vertexPos), coords.getY(vertexPos))) {
        coords.setOrdinate(vertexPos, 0, x);
        coords.setOrdinate(vertexPos, 1, y);
        // il y en a qu'un
        editorToRepaint = (ZEditorDefault) lineFindResult.layer.getEditor();
      }
    }
    if (editorToRepaint != null) {
      editorToRepaint.repaintPanel();
    }

  }

  @Override
  public void attributeAction(Object source, int indexAtt, GISAttributeInterface att, int action) {}

  @Override
  public void attributeValueChangeAction(Object source, int indexAtt, GISAttributeInterface att, int indexObj,
                                         Object newValue) {}

  @Override
  public void objectAction(Object source, int indexObj, Object obj, int action) {}
}
