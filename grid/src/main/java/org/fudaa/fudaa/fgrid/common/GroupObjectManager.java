package org.fudaa.fudaa.fgrid.common;

import java.util.ArrayList;
import java.util.List;

public class GroupObjectManager
{
  // TODO Verifier la valeur.
  private static final int MAX_NOM_LENGTH = 20;
  
  private long nextId = 0;
  private List<GroupObject> groups = new ArrayList<GroupObject>();
  
  public GroupObject createGroupObject(String name, String description)
  {
    if (isUsedName(name))
    {
      return null;
    }
    
    GroupObject groupe = new GroupObject(this, this.nextId++, name, description);
    
    this.groups.add(groupe);
    
    return groupe;
  }
  
  public boolean removeGroupObject(GroupObjectInterface group)
  {
    return this.groups.remove(group);
  }

  public boolean isCorrectNameLength(String name)
  {
    return (name.length() <= GroupObjectManager.MAX_NOM_LENGTH);
  }
  
  public boolean isUsedName(String name)
  {
    for (int i = 0; i < this.groups.size(); i++)
    {
      if (this.groups.get(i).getName().equals(name))
      {
        return true;
      }
    }
    
    return false;
  }

  public boolean isCorrectNameForGroupObject(GroupObjectInterface group, String name)
  {
    if (!this.isCorrectNameLength(name))
    {
      return false;
    }
    
    for (int i = 0; i < this.groups.size(); i++)
    {
      GroupObjectInterface temp = this.groups.get(i);
      
      if ((group != temp) && (temp.getName().equals(name)))
      {
        return false;
      }
    }
    
    return true;
  }

  public boolean isCorrectUserIdForGroupObject(GroupObjectInterface group, int userId)
  {
    if (userId == 0)
    {
      return true;
    }
    
    for (int i = 0; i < this.groups.size(); i++)
    {
      GroupObjectInterface temp = this.groups.get(i);
      
      if ((group != temp) && (temp.getUserId() == userId))
      {
        return false;
      }
    }
    
    return true;
  }
  
  public void initialize(GroupObjectManager manager)
  {
    this.nextId = manager.nextId;
    this.groups = manager.groups;
  }
  
  public GroupObjectManager copy()
  {
    GroupObjectManager manager = new GroupObjectManager();
    
    manager.nextId = nextId;
    manager.groups.addAll(this.groups);
    
    return manager;
  }
}
