package org.fudaa.fudaa.fgrid.action;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISLigneBrisee;
import org.fudaa.ctulu.gis.GISSegment;
import org.fudaa.fudaa.fgrid.action.IntersectionDetector.Intersection;
import org.fudaa.fudaa.fgrid.layer.RepereContainerImpl;
import org.fudaa.fudaa.fgrid.model.GeometricLigneBriseeModel;
import org.fudaa.fudaa.fgrid.model.LineFindResult;
import org.fudaa.fudaa.fgrid.model.LineFindResultComparator;
import org.fudaa.fudaa.fgrid.model.RepereContainer;

import org.locationtech.jts.geom.Coordinate;

public class IntersectionPointsInserter
{
  private static class InsertionPoint
  {
    private int idxPtBefore;
    private long idPtToInsert;
    private double distance;
    
    public InsertionPoint(int idxPtBefore, long idPtToInsert, double distance)
    {
      super();
      this.idxPtBefore = idxPtBefore;
      this.idPtToInsert = idPtToInsert;
      this.distance = distance;
    }
  }
  
  private static class InsertionPointComparator implements Comparator<InsertionPoint>
  {
    @Override
    public int compare(InsertionPoint point1, InsertionPoint point2)
    {
      if (point1.idxPtBefore == point2.idxPtBefore)
      {
        if (point1.distance == point2.distance)
        {
          return 0;
        }
        else if (point1.distance < point2.distance)
        {
          return 1;
        }
      }
      else if (point1.idxPtBefore < point2.idxPtBefore)
      {
        return 1;
      }
      
      return -1;
    } 
  }
  
  private static final InsertionPointComparator comparator = new InsertionPointComparator();

  private final List<Intersection> intersections;
  private final CtuluCommandContainer cmdContainer;
  private final TreeMap<Coordinate, Long> pointsAdded;
  private final RepereContainer repere = new RepereContainerImpl();
  private int nbPointsInserted;
  
  public IntersectionPointsInserter(List<Intersection> intersections, CtuluCommandContainer cmdContainer, TreeMap<Coordinate, Long> pointsAdded)
  {
    super();
    this.intersections = intersections;
    this.cmdContainer = cmdContainer;
    this.pointsAdded = pointsAdded;
    this.nbPointsInserted = 0;
  }
  
  public void process()
  {
    CtuluCommandComposite cmp = (this.cmdContainer == null) ? null : new CtuluCommandComposite();

    TreeMap<LineFindResult, TreeSet<InsertionPoint>> ptsToInsert = this.createLinesMap(this.intersections, this.pointsAdded);
    
    Set<LineFindResult> lines = ptsToInsert.keySet();

    for (LineFindResult line : lines)
    {
      GeometricLigneBriseeModel model = (GeometricLigneBriseeModel)line.layer.modeleDonnees();
      TreeSet<InsertionPoint> points = ptsToInsert.get(line);

      for (InsertionPoint point : points)
      {
        model.addPoint(line.posOfLine, point.idxPtBefore, point.idPtToInsert, cmp);
        
        this.nbPointsInserted++;
      }
    }

    if (this.cmdContainer != null) {
      this.cmdContainer.addCmd(cmp.getSimplify());
    }    
  }

  private TreeMap<LineFindResult, TreeSet<InsertionPoint>> createLinesMap(List<Intersection> intersections, TreeMap<Coordinate, Long> pointsAdded)
  {
    TreeMap<LineFindResult, TreeSet<InsertionPoint>> map = new TreeMap<LineFindResult, TreeSet<InsertionPoint>>(new LineFindResultComparator());
    
    for (int i = 0; i < intersections.size(); i++)
    {
      Intersection intersection = intersections.get(i);
      Coordinate pt = pointsAdded.ceilingKey(intersection.getIntersection());
      long ptId = pointsAdded.get(pt);

      this.addInsertionPoint(map, intersection.getLine1(), pt, ptId);
      this.addInsertionPoint(map, intersection.getLine2(), pt, ptId);
    }
    
    return map;
  }
  
  private void addInsertionPoint(TreeMap<LineFindResult, TreeSet<InsertionPoint>> linesMap, LineFindResult segment, Coordinate pt, long ptId)
  {
    if (!linesMap.containsKey(segment))
    {
      linesMap.put(segment, new TreeSet<InsertionPoint>(comparator));
    }

    GISSegment seg = ((GISLigneBrisee)segment.layer.modeleDonnees().getGeomData().getGeometry(segment.posOfLine)).getSegment(segment.idxOfSegment);
    
    // Si le point � ins�rer fait d�j� partie de la ligne, on l'ins�re pas.
    if (this.repere.isSamePoint(seg.getPt1().x, seg.getPt1().y, pt.x, pt.y) || this.repere.isSamePoint(seg.getPt2().x, seg.getPt2().y, pt.x, pt.y))
    {
      return;
    }
    
    double distance = seg.getPt1().distance(pt);
    
    TreeSet<InsertionPoint> newPts = linesMap.get(segment);
    newPts.add(new InsertionPoint(segment.idxOfSegment, ptId, distance));
  }

  public int getNbPointsInserted()
  {
    return nbPointsInserted;
  }
}
