package org.fudaa.fudaa.fgrid.model;

import java.util.Comparator;

public class LineFindResultComparator implements Comparator<LineFindResult>
{
  @Override
  public int compare(LineFindResult line1, LineFindResult line2)
  {
    if (line1.layer.hashCode() == line2.layer.hashCode())
    {
      if (line1.posOfLine == line2.posOfLine)
      {
        return 0;
      }
      else if (line1.posOfLine < line2.posOfLine)
      {
        return -1;
      }
    }
    else if (line1.layer.hashCode() < line2.layer.hashCode())
    {
      return -1;
    }
    
    return 1;
  } 
}

