package org.fudaa.fudaa.fgrid.layer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISAttributeModelLongInterface;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.commun.EbliListeSelectionMulti;
import org.fudaa.fudaa.fgrid.model.AttributeConstants;
import org.fudaa.fudaa.fgrid.model.ContextGeometricLayers;
import org.fudaa.fudaa.fgrid.model.GeometricLigneBriseeModel;
import org.fudaa.fudaa.fgrid.model.GeometricLineModelIndexer;
import org.fudaa.fudaa.fgrid.model.GeometricPointModel;
import org.fudaa.fudaa.fgrid.model.GeometricPointModelIndexer;
import org.fudaa.fudaa.fgrid.model.LineFind;
import org.fudaa.fudaa.fgrid.model.LineFindResult;
import org.fudaa.fudaa.fgrid.model.MultiGroupLineFindResult;
import org.fudaa.fudaa.fgrid.model.MultiGroupPointFindResult;
import org.fudaa.fudaa.fgrid.model.PointFindResult;
import org.fudaa.fudaa.fgrid.model.RepereContainer;
import org.fudaa.fudaa.sig.layer.FSigLayerLineEditable;
import org.fudaa.fudaa.sig.layer.FSigLayerPointEditable;

/**
 * Context for geometric layers. <br>
 * Permet de retrouver facilement les calques geometriques et de faire des recherches sur les ID des points, lignes et
 * surfaces. Ce sont les indexer {@linkplain GeometricLineModelIndexer} et {@linkplain GeometricPointModelIndexer} qui
 * permettent de recuperer facilement les ID des points/lignes.
 * 
 * @author deniger
 */
public class ContextGeometricLayersImpl implements ContextGeometricLayers {

  /**
   * @param nodeId l'identifiant du noeud a chercher
   * @param pt le calque a parcourir
   * @return toujours non null
   */
  public static PointFindResult findNode(long nodeId, FSigLayerPointEditable pt) {
    PointFindResult res = PointFindResult.EMPTY;
    GISZoneCollection geomData = pt.getModelEditable().getGeomData();
    int indiceOf = geomData.getIndiceOf(AttributeConstants.ID);
    GISAttributeModelLongInterface dataModel = (GISAttributeModelLongInterface) geomData.getDataModel(indiceOf);
    for (int i = dataModel.getSize() - 1; i >= 0; i--) {
      if (dataModel.getValue(i) == nodeId) {
        res = new PointFindResult();
        res.idx = i;
        res.layer = pt;
        return res;
      }
    }
    return res;

  }

  public static boolean isDefinedID(Long id) {
    return id != null && id.longValue() >= 0;
  }

  public static boolean isNotDefinedID(Long id) {
    return !isDefinedID(id);
  }

  long currentFreeNodeId;
  long currentFreeLineId;

  long currentFreeSurfaceId;

  final CtuluUI ui;

  Collection<FSigLayerPointEditable> layerPoints = new HashSet<FSigLayerPointEditable>();
  Collection<FSigLayerPointEditable> layerPointsImmutable = Collections.unmodifiableCollection(layerPoints);

  Collection<ZCalqueLigneBriseeEditable> layerLines = new HashSet<ZCalqueLigneBriseeEditable>();
  Collection<ZCalqueLigneBriseeEditable> layerLinesImmutable = Collections.unmodifiableCollection(layerLines);

  /**
   * The support layer in which the node will be created if needed.
   */
  FSigLayerPointEditable nodeLayerSupport;

  private final RepereContainer repere;
  private GridVisuPanelController visuController;
  /**
   * @param ui
   * @param zScene 
   */
  public ContextGeometricLayersImpl(CtuluUI ui, RepereContainer repere, GridVisuPanelController visuController) {
    super();
    this.ui = ui;
    this.repere = repere;
    this.visuController = visuController;
  }

  /**
   * @return the next id for a new line
   */
  @Override
  public long createLineId() {
    return currentFreeLineId++;
  }

  /**
   * @return the next id for a new node
   */
  @Override
  public long createPointId() {
    return currentFreeNodeId++;
  }

  /**
   * @return the next id for a new surface
   */
  protected long createSurfaceId() {
    return currentFreeSurfaceId++;
  }

  /**
   * @param dataType
   * @return the first (created) line layer of type dataType
   */
  public ZCalqueLigneBriseeEditable findFirstLineLayerOfType(EnumLayerDataType dataType) {
    return LayerHelper.findLayerOfType(layerLines, dataType);
  }

  /**
   * @param dataType
   * @return the first (created) point layer of type dataType
   */
  public FSigLayerPointEditable findFirstPointLayerOfType(EnumLayerDataType dataType) {
    return LayerHelper.findLayerOfType(layerPoints, dataType);
  }

  /**
   * @param nodeId l'identifiant du noeud
   * @return toujours non null.
   */
  public PointFindResult findPoint(long nodeId) {
    for (FSigLayerPointEditable layerPt : layerPoints) {
      PointFindResult res = findNode(nodeId, layerPt);
      if (res.isNotEmpty()) return res;

    }
    return PointFindResult.EMPTY;
  }

  /**
   * @return the layerLines in an immutable list
   */
  public Collection<ZCalqueLigneBriseeEditable> getLineLayers() {
    return layerLinesImmutable;
  }

  @Override
  public Collection<LineFindResult> getLineUsingPoint(long pointID) {
    List<LineFindResult> res = new ArrayList<LineFindResult>();
    for (ZCalqueLigneBriseeEditable layer : layerLines) {
      GeometricLigneBriseeModel model = (GeometricLigneBriseeModel) layer.modeleDonnees();
      model.getIndexer().collectLineUsingPointId(pointID, res, (FSigLayerLineEditable) layer);
    }
    return res;
  }

  /**
   * @return the layerPoints in an immutable list
   */
  public Collection<FSigLayerPointEditable> getPointLayers() {
    return layerPointsImmutable;
  }

  @Override
  public RepereContainer getRepere() {
    return repere;
  }

  public ZScene getScene()
  {
    return this.visuController.getView().getScene();
  }

  @Override
  public GeometricPointModel getSupportModelPoint() {
    return (GeometricPointModel) getSupportPointLayer().getModelEditable();
  }

  /**
   * @return the pointLayerSupport
   */
  public FSigLayerPointEditable getSupportPointLayer() {
    return nodeLayerSupport;
  }

  /**
   * @return the ui
   */
  @Override
  public CtuluUI getUi() {
    return ui;
  }

  @Override
  public boolean isPointUsedByALine(long pointID) {
    for (ZCalqueLigneBriseeEditable layer : layerLines) {
      GeometricLigneBriseeModel model = (GeometricLigneBriseeModel) layer.modeleDonnees();
      if (model.getIndexer().isNodeUsedByALine(pointID)) return true;
    }
    return false;

  }

  // TODO Fred : A verifier et/ou ameliorer.
  @Override
  public LineFindResult locateLine(long lineId)
  {
    for (ZCalqueLigneBriseeEditable layer : layerLines) {
      GeometricLigneBriseeModel model = (GeometricLigneBriseeModel)layer.modeleDonnees();
      
      for (int i = 0; i < model.getNombre(); i++)
      {
        if (model.getLineId(i) == lineId)
        {
          LineFindResult findLine = new LineFindResult();
          findLine.posOfLine = i;
          findLine.layer = layer;
          return findLine;
        }
      }
    }

    return LineFindResult.EMPTY;
  }

  @Override
  public PointFindResult locatePoint(long pointId) {
    for (FSigLayerPointEditable layer : layerPoints) {
      PointFindResult findPoint = ((GeometricPointModel) layer.getModelEditable()).getIndexer().findPoint(pointId);
      if (findPoint.idx >= 0) {
        findPoint.layer = layer;
        return findPoint;
      }
    }

    return PointFindResult.EMPTY;
  }

  /**
   * @param layerPt layer to register in the geometric context.
   */
  protected void register(FSigLayerPointEditable layerPt) {
    this.register(layerPt, false);
  }

  protected void register(FSigLayerPointEditable layerPt, boolean isSupportLayer) {
    layerPoints.add(layerPt);
    if (isSupportLayer) nodeLayerSupport = layerPt;
  }

  protected void register(ZCalqueLigneBriseeEditable layerLine) {
    layerLines.add(layerLine);
  }

  @Override
  public MultiGroupLineFindResult getSelectionInLineLayers()
  {
    MultiGroupLineFindResult result = new MultiGroupLineFindResult();
    
    for (ZCalqueLigneBriseeEditable layer : this.getLineLayers())
    {
      SelectionMode selectionMode = layer.getSelectionMode();
      
      if (selectionMode == SelectionMode.NORMAL)
      {
        CtuluListSelectionInterface selection = layer.getLayerSelection();
        
        if ((selection == null) || selection.isEmpty())
        {
          continue;
        }
        
        int[] idxs = selection.getSelectedIndex();
        
        for (int i = 0; i < idxs.length; i++)
        {
          result.addObject(layer, new LineFind(idxs[i]));
        }
      }
      else
      {
        EbliListeSelectionMulti selection = (EbliListeSelectionMulti)layer.getLayerSelectionMulti();
        int[] idxLines = selection.getIdxSelected();

        for (int i = 0; i < idxLines.length; i++)
        {
          int[] idxSels = selection.get(idxLines[i]).getSelectedIndex();

          for (int j = 0; j < idxSels.length; j++)
          {
            result.addObject(layer, new LineFind(idxLines[i], idxSels[j], selectionMode == SelectionMode.ATOMIC));
          }
        }
      }
    }
    
    return result;
  }

  @Override
  public MultiGroupPointFindResult getSelectionInPointLayers()
  {
    MultiGroupPointFindResult result = new MultiGroupPointFindResult();
    
    for (ZCalquePointEditable layer : this.getPointLayers())
    {
      SelectionMode selectionMode = layer.getSelectionMode();
      
      // Ce type de calque est toujours en selection normal.
      if (selectionMode == SelectionMode.NORMAL)
      {
        CtuluListSelectionInterface selection = layer.getLayerSelection();
        
        if ((selection == null) || selection.isEmpty())
        {
          continue;
        }
        
        int[] idxs = selection.getSelectedIndex();
        
        for (int i = 0; i < idxs.length; i++)
        {
          result.addObject(layer, idxs[i]);
        }
      }
    }
    
    return result;
  }
}
