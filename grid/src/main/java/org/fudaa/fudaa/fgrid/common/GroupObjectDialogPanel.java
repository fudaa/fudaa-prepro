package org.fudaa.fudaa.fgrid.common;

import java.awt.Color;

import javax.swing.JCheckBox;
import javax.swing.JComponent;

import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.controle.BSelecteurColorChooserBt;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;

@SuppressWarnings("serial")
public class GroupObjectDialogPanel extends CtuluDialogPanel
{
  private final GroupObjectManager manager;
  private GroupObject groupObject;
  private BuTextField name;
  private BuTextField description;
  private BuTextField userId;
  private JComponent color;
  private JCheckBox isVisble;
  
  public GroupObjectDialogPanel(GroupObjectManager manager)
  {
    this.setLayout(new BuGridLayout(2));
    
    //TODO Traduire les textes.
    this.manager = manager;
    this.name = this.addLabelStringText("Nom : ");
    this.description = this.addLabelStringText("Description : ");
    this.userId = this.addLabelIntegerText("ID utilisateur : ");
    this.addLabel("Couleur : ");
    this.color = new BSelecteurColorChooserBt().getComponents()[0];
    this.color.setForeground(new Color(Color.TRANSLUCENT));
    this.add(this.color);
    this.addLabel("Visible : ");
    this.isVisble = new JCheckBox();
    this.add(this.isVisble);
    this.userId.setText("0");
    this.isVisble.setSelected(true);
  }

  public GroupObjectDialogPanel(GroupObjectManager manager, GroupObject groupObject)
  {
    this(manager);
    
    this.groupObject = groupObject;
    this.name.setText(this.groupObject.getName());
    this.description.setText(this.groupObject.getDescription());
    this.userId.setText(String.valueOf(this.groupObject.getUserId()));
    this.color.setForeground(this.groupObject.getColor());
    this.isVisble.setSelected(this.groupObject.isVisible());
  }
  
  @Override
  public boolean isDataValid()
  {
    boolean isValid = true;
    
    if (!this.manager.isCorrectNameForGroupObject(this.groupObject, this.name.getText()))
    {
      if (!this.manager.isCorrectNameLength(this.name.getText()))
      {
        this.setErrorText("La taille du nom est incorrect.");
      }
      else
      {
        this.setErrorText("Le nom entr� est d�j� utilis�.");
      }

      isValid = false;
    }
    
    //TODO Traduire les textes.
    if (this.userId.getValue() != null)
    {
      if (!this.manager.isCorrectUserIdForGroupObject(this.groupObject, Integer.parseInt(this.userId.getText())))
      {
        this.setErrorText("L'ID utilisateur entr� est d�j� utilis�.");
  
        isValid = false;
      }
    }
    else
    {
      this.setErrorText("La valeur de l'ID utilisateur doit �tre un entier.");
      
      isValid = false;
    }
    
    return isValid;
  }

  @Override
  public boolean apply()
  {
    if (this.groupObject == null)
    {
      this.groupObject = this.manager.createGroupObject(this.name.getText(), this.description.getText());
    }
    else
    {
      this.groupObject.setName(this.name.getText());
      this.groupObject.setDescription(this.description.getText());
    }
    
    this.groupObject.setUserId(Integer.parseInt(this.userId.getText()));
    this.groupObject.setColor(this.color.getForeground());
    this.groupObject.setVisible(this.isVisble.isSelected());
    return true;
  }
  
  public GroupObject getGroupObject()
  {
    return this.groupObject;
  }
}
