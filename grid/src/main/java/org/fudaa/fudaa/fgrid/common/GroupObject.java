package org.fudaa.fudaa.fgrid.common;

import java.awt.Color;

/**
 * @author Christophe CANEL (Genesis)
 *
 * Cette classe permet g�rer un groupe d'objets.
 */
public class GroupObject implements GroupObjectInterface {
  private final GroupObjectManager manager;
  private final long id;
  private String name;
  private String description;
  private boolean isVisible = true;
  private int userId = 0;
  private Color color = new Color(Color.TRANSLUCENT);

  protected GroupObject(GroupObjectManager manager, long id, String name, String description)
  {
    this.manager = manager;
    this.id = id;
    this.name = name;
    this.description = description;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public long getId()
  {
    return id;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getName()
  {
    return name;
  }

  /**
   * 
   * @param name
   * @return true si le nouveau nom a �t� sett�.
   */
  public boolean setName(String name)
  {
    if (!this.manager.isCorrectNameForGroupObject(this, name))
    {
      return false;
    }
    
    this.name = name;
    
    return true;
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  
  /**
   * {@inheritDoc}
   */
  @Override
  public int getUserId()
  {
    return userId;
  }

  /**
   * 
   * @param userId
   * @return true si le nouvel user id a �t� sett�.
   */
  public boolean setUserId(int userId)
  {
    if (!this.manager.isCorrectUserIdForGroupObject(this, userId))
    {
      return false;
    }
    
    this.userId = userId;
    
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Color getColor()
  {
    return color;
  }

  public void setColor(Color color)
  {
    this.color = color;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isVisible()
  {
    return isVisible;
  }
  
  public void setVisible(boolean isVisible)
  {
    this.isVisible = isVisible;
  }
}
