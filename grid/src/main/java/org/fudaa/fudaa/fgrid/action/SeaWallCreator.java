package org.fudaa.fudaa.fgrid.action;

import java.awt.Component;

import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLigneBrisee;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISSegment;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.fgrid.model.LineFindResult;
import org.fudaa.fudaa.fgrid.model.SeaWall;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import org.locationtech.jts.geom.Coordinate;

public class SeaWallCreator
{
  @SuppressWarnings("serial")
  private static class AddSeaWallDialogPanel extends CtuluDialogPanel
  {
    private BuTextField ridgeWidth;
    private BuTextField footWidth;
    
    public AddSeaWallDialogPanel()
    {
      this.setLayout(new BuGridLayout(2));
      
      this.ridgeWidth = this.addLabelDoubleText(Messages.getString("seaWallCreator.ridgeWidth")); //$NON-NLS-1$
      this.footWidth = this.addLabelDoubleText(Messages.getString("seaWallCreator.footWidth")); //$NON-NLS-1$
      
      this.ridgeWidth.setText("10"); //$NON-NLS-1$
      this.footWidth.setText("10"); //$NON-NLS-1$
    }

    @Override
    public boolean isDataValid()
    {
      boolean isValid = true;
            
      if (this.ridgeWidth.getValue() == null)
      {
        this.setErrorText(Messages.getString("seaWallCreator.ridgeWidthMustBeReal")); //$NON-NLS-1$
        
        isValid = false;
      }
      else if (Double.parseDouble(this.ridgeWidth.getText()) < 1)
      {
        this.setErrorText(Messages.getString("seaWallCreator.ridgeWidthMustBeMin1")); //$NON-NLS-1$
        
        isValid = false;
      }
      
      if (this.footWidth.getValue() == null)
      {
        this.setErrorText(Messages.getString("seaWallCreator.footWidthMustBeReal")); //$NON-NLS-1$
        
        isValid = false;
      }
      else if (Double.parseDouble(this.footWidth.getText()) < 1)
      {
        this.setErrorText(Messages.getString("seaWallCreator.footWidthMustBeMin1")); //$NON-NLS-1$
        
        isValid = false;
      }

      return isValid;
    }
        
    public double getRidgeWidth()
    {
      return Double.parseDouble(this.ridgeWidth.getText());
    }

    public double getFootWidth()
    {
      return Double.parseDouble(this.footWidth.getText());
    }
  }
  
  public static SeaWall createSeaWallWithDialog(LineFindResult line)
  {
    AddSeaWallDialogPanel dialog = new AddSeaWallDialogPanel();
    
    if (dialog.afficheModale((Component)null, Messages.getString("seaWallCreator.seaWallParameters")) == CtuluDialog.OK_OPTION) //$NON-NLS-1$
    {
      return SeaWallCreator.createSeaWall(line, dialog.getRidgeWidth(), dialog.getFootWidth());
    }
    
    return null;
  }
  
  public static SeaWall createSeaWall(LineFindResult line, double ridgeWidth, double footWidth)
  {
    GISLigneBrisee realLine = (GISLigneBrisee)line.layer.modeleDonnees().getGeomData().getGeometry(line.posOfLine);
    SeaWall seaWall = null;
    
    if ((realLine instanceof GISPolyligne) && (line.posOfSommet == -1) && (line.idxOfSegment == -1))
    {
      int nbSegment = realLine.getNbSegment();

      Coordinate[][] coordinates = new Coordinate[nbSegment + 1][4];
      
      GISSegment lastSegment = new GISSegment();
      GISSegment currentSegment = new GISSegment();
      double angle;
      
      realLine.getSegment(0, currentSegment);

      angle = SeaWallCreator.getPerpendicularAngle(currentSegment);
      System.out.println(angle);
      coordinates[0] = SeaWallCreator.getCoordinates(currentSegment.getPt1(), null, null, angle, ridgeWidth, footWidth);
      
      for (int i = 1; i < nbSegment; i++)
      {
        realLine.getSegment(i - 1, lastSegment);
        realLine.getSegment(i, currentSegment);
        
        double angle1 = SeaWallCreator.getPerpendicularAngle(lastSegment);
        double angle2 = SeaWallCreator.getPerpendicularAngle(currentSegment);

        angle = (angle1 + angle2) / 2.0d;
        System.out.println(angle);
        coordinates[i] = SeaWallCreator.getCoordinates(currentSegment.getPt1(), coordinates[i - 1], lastSegment, angle, ridgeWidth, footWidth);
      }

      angle = SeaWallCreator.getPerpendicularAngle(currentSegment);
      System.out.println(angle);
      coordinates[nbSegment] = SeaWallCreator.getCoordinates(currentSegment.getPt2(), coordinates[nbSegment - 1], lastSegment, angle, ridgeWidth, footWidth);

      seaWall = new SeaWall(SeaWallCreator.getPolygone(coordinates, 1), SeaWallCreator.getPolygone(coordinates, 0), SeaWallCreator.getPolygone(coordinates, 2));
    }
    
    return seaWall;
  }
  
  private static GISPolygone getPolygone(Coordinate[][] coordinates, int idx)
  {
    Coordinate[] coordsPoly = new Coordinate[(2 * coordinates.length) + 1];
    
    for (int i = 0; i < 2; i++)
    {
      for (int j = 0; j < coordinates.length; j++)
      {
        coordsPoly[(i * coordinates.length) + j] = coordinates[(i == 0)? j : ((coordinates.length - 1) - j)][(idx + 1) - i];
      }
    }
    
    coordsPoly[2 * coordinates.length] = new Coordinate(coordsPoly[0]);

    return (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(coordsPoly);
  }
  
  private static double getPerpendicularAngle(GISSegment segment)
  {
    Coordinate pt1 = segment.getPt1();
    Coordinate pt2 = segment.getPt2();
    double rayon = pt1.distance(pt2);
    double x = (pt2.x - pt1.x) / rayon;
    double y = pt2.y - pt1.y;
    double angle = Math.toDegrees(Math.acos(x));
    
    if (y < 0)
    {
      angle = 360.0d - angle;
    }
    
    angle += 90.0d;
    angle %= 360;

    return angle;
  }
  
  private static Coordinate[] getCoordinates(Coordinate origin, Coordinate[] previous, GISSegment segment, double angle, double ridgeWidth, double footWidth)
  {
    Coordinate[] coordinates = new Coordinate[4];
    double width = ridgeWidth / 2.0d;
    
    coordinates[0] = SeaWallCreator.getCoordinate(origin, angle, width + footWidth);
    coordinates[1] = SeaWallCreator.getCoordinate(origin, angle, width);
    coordinates[2] = SeaWallCreator.getCoordinate(origin, angle, -(width + footWidth));
    coordinates[3] = SeaWallCreator.getCoordinate(origin, angle, -width);
    
    // TODO Magouille pour evit� l'inversion de points, pas s�r que ca marche tout le temps, � am�liorer.
    if (previous != null)
    {
      GISSegment segment1 = new GISSegment(previous[0], coordinates[0]);
      
      if (IntersectionDetector.getIntersection(segment1, segment) != null)
      {
        coordinates = new Coordinate[]{coordinates[2], coordinates[3], coordinates[0], coordinates[1]};
      }
    }
    
    return coordinates;
  }
  
  private static Coordinate getCoordinate(Coordinate origin, double angle, double distance)
  {
    double x = (distance * Math.cos(Math.toRadians(angle))) + origin.x;
    double y = (distance * Math.sin(Math.toRadians(angle))) + origin.y;

    return new Coordinate(x, y);
  }
}
