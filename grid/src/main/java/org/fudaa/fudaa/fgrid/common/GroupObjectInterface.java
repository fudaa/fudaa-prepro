package org.fudaa.fudaa.fgrid.common;

import java.awt.Color;

/**
 * @author Christophe CANEL (Genesis)
 *
 * Cette interface représente un groupe d'objets.
 */
public interface GroupObjectInterface
{
  long getId();

  String getName();

  String getDescription();

  int getUserId();

  Color getColor();

  boolean isVisible();
}