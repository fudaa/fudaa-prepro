package org.fudaa.fudaa.fgrid.action;

import java.util.List;
import java.util.Set;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.calque.edition.ZModelePointEditableInterface;
import org.fudaa.fudaa.fgrid.model.MultiGroupPointFindResult;

public class DuplicatePointCleaner
{
  private List<ZCalquePointEditable> layers;
  private CtuluCommandContainer cmdContainer;
  private int nbPointsCleaned;
  
  public DuplicatePointCleaner(List<ZCalquePointEditable> layers, CtuluCommandContainer cmdContainer)
  {
    super();
    this.layers = layers;
    this.cmdContainer = cmdContainer;
    this.nbPointsCleaned = 0;
  }

  public void process()
  {
    this.nbPointsCleaned = 0;

    CtuluCommandComposite cmp = (this.cmdContainer == null) ? null : new CtuluCommandComposite();

    DuplicatePointDetector detector = new DuplicatePointDetector(layers);
    
    detector.process();
    
    MultiGroupPointFindResult result = detector.getResult();
    
    Set<ZCalquePointEditable> supports = result.getSupports();
    
    for (ZCalquePointEditable layer : supports)
    {
      List<Integer> points = result.getObjects(layer);
      int[] idxs = new int[points.size()];
      
      for (int i = 0; i < idxs.length; i++)
      {
        idxs[i] = points.get(i);
      }
      
      ((ZModelePointEditableInterface)layer.getModelEditable()).removePoint(idxs, cmp);

      this.nbPointsCleaned += idxs.length;
    }

    if (this.cmdContainer != null) {
      this.cmdContainer.addCmd(cmp.getSimplify());
    }    
  }
  
  public int getNbPointsCleaned()
  {
    return nbPointsCleaned;
  }
}
