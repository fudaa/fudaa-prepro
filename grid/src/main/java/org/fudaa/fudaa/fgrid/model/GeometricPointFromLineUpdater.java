package org.fudaa.fudaa.fgrid.model;

import org.locationtech.jts.geom.CoordinateSequence;

import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISAttributeModelLongInterface;
import org.fudaa.ctulu.gis.GISCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gis.GISZoneListener;
import org.fudaa.ebli.calque.edition.ZEditorDefault;

/**
 * Update point Layers when a line has been changed
 * 
 * @author deniger
 */
public class GeometricPointFromLineUpdater implements GISZoneListener {
  final ContextGeometricLayers context;
  final GeometricLigneBriseeModel model;

  /**
   * @param context the geometric context
   * @param model
   */
  public GeometricPointFromLineUpdater(ContextGeometricLayers context, GeometricLigneBriseeModel model) {
    assert context != null;
    assert model != null;
    this.context = context;
    this.model = model;
  }

  public void attributeChanged(GISCollection src, GISAttributeInterface att) {}


 

  private ZEditorDefault lineChanged(int posOfLine, GISAttributeModel nodeIdLongModel, ZEditorDefault editorToRepaint) {
    GISAttributeModelLongInterface pointIds = (GISAttributeModelLongInterface) nodeIdLongModel
        .getObjectValueAt(posOfLine);
    int nbPoints = pointIds.getSize();
    CoordinateSequence pointXYInLine = model.getGeomData().getCoordinateSequence(posOfLine);
    for (int j = 0; j < nbPoints; j++) {
      //cela peut arriver lorsque le point est en cours d'ajout.
      PointFindResult pointsUsed = context.locatePoint(pointIds.getValue(j));
      if (pointsUsed.isNotEmpty()) {
        GeometricPointModel model = (GeometricPointModel) pointsUsed.layer.modele();
        GISZoneCollectionPoint pt = (GISZoneCollectionPoint) model.getGeomData();
        double x = pointXYInLine.getX(j);
        double y = pointXYInLine.getY(j);
        int idxInPointModel = pointsUsed.idx;
        if (context.getRepere().isDifferentPoint(x, y, pt.getX(idxInPointModel), pt.getY(idxInPointModel))) {
          pt.set(idxInPointModel, x, y, null);
          editorToRepaint = (ZEditorDefault) pointsUsed.layer.getEditor();
        }
      }

    }
    return editorToRepaint;
  }

  @Override
  public void attributeAction(Object source, int indexAtt, GISAttributeInterface att, int action) {}

  @Override
  public void attributeValueChangeAction(Object source, int indexAtt, GISAttributeInterface att, int indexObj,
                                         Object newValue) {}

  @Override
  public void objectAction(Object source, int indexObj, Object obj, int action) {}

}
