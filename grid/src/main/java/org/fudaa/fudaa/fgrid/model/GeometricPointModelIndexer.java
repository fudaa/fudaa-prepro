package org.fudaa.fudaa.fgrid.model;

import org.fudaa.ctulu.gis.GISZoneCollection;

/**
 * Permet de stocker la relation ID<->position dans le calques des points.
 * 
 * @author deniger
 */
public class GeometricPointModelIndexer extends GeometricDefaultIndexer {

  /**
   * @param collection to survey
   */
  public GeometricPointModelIndexer(GISZoneCollection collection) {
    super(collection);
  }

  /**
   * @param pointID l'ID of the point
   * @return if contains by this collection.
   */
  public boolean isPointUsed(long pointID) {
    index();
    return idForPosition.contains(pointID);
  }

  /**
   * @param pointID the id to look for
   * @return le FindResult without le layer set
   */
  public PointFindResult findPoint(long pointID) {
    PointFindResult res = PointFindResult.EMPTY;
    int positionOfId = super.getPositionOfId(pointID);
    if (positionOfId >= 0) {
      res = new PointFindResult();
      res.idx = positionOfId;
    }
    return res;
  }

}
