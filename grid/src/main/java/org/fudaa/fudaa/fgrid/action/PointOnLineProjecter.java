package org.fudaa.fudaa.fgrid.action;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISLigneBrisee;
import org.fudaa.fudaa.fgrid.model.GeometricLigneBriseeModel;
import org.fudaa.fudaa.fgrid.model.GeometricPointModel;
import org.fudaa.fudaa.fgrid.model.LineFindResult;
import org.fudaa.fudaa.fgrid.model.PointFindResult;

import org.locationtech.jts.geom.Coordinate;

public class PointOnLineProjecter
{
  public enum PointOnLineProjecterResult
  {
    SUCCEEDED,
    CANT_DO_PROJECTION,
    PROJECTION_POINT_ALREADY_EXIST,
    INCORRECT_LINE,
    INCORRECT_POINT
  }
  
  private LineFindResult line;
  private PointFindResult point;
  private CtuluCommandContainer cmdContainer;
  private PointOnLineProjecterResult result;
  
  public PointOnLineProjecter(LineFindResult line, PointFindResult point, CtuluCommandContainer cmdContainer)
  {
    super();
    this.line = line;
    this.point = point;
    this.cmdContainer = cmdContainer;
    this.result = PointOnLineProjecterResult.SUCCEEDED;
  }
  
  public void process()
  {
    this.result = PointOnLineProjecterResult.SUCCEEDED;

    if ((this.line == null) || this.line.isEmpty())
    {
      this.result = PointOnLineProjecterResult.INCORRECT_LINE;
      
      return;
    }
    
    if ((this.point == null) || this.point.isEmpty())
    {
      this.result = PointOnLineProjecterResult.INCORRECT_POINT;
      
      return;
    }
    
    GISLigneBrisee line = (GISLigneBrisee)this.line.layer.modeleDonnees().getGeomData().getGeometry(this.line.posOfLine);
    GeometricPointModel modelePts = (GeometricPointModel)this.point.layer.getModelEditable();
    Coordinate point = new Coordinate(modelePts.getX(this.point.idx), modelePts.getY(this.point.idx));
    
    ProjectPointOnLineHelper helper = new ProjectPointOnLineHelper(line, this.line.idxOfSegment, point);
    
    helper.compute();
    
    if (helper.canProject())
    {
      if (helper.isOnSegmentPt())
      {
        this.result = PointOnLineProjecterResult.PROJECTION_POINT_ALREADY_EXIST;
      }
      else
      {
        Coordinate projectedPoint = helper.getProjectedPoint();
        modelePts.setPoint(this.point.idx, projectedPoint.x, projectedPoint.y, this.cmdContainer);
        
        ((GeometricLigneBriseeModel)this.line.layer.modeleDonnees()).addPoint(this.line.posOfLine, helper.getSegmentOfProjectionIdx(), modelePts.getPointId(this.point.idx), this.cmdContainer);
      }
    }
    else
    {
      this.result = PointOnLineProjecterResult.CANT_DO_PROJECTION;
    }
  }

  public PointOnLineProjecterResult getResult()
  {
    return result;
  }
}
