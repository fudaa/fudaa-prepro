package org.fudaa.fudaa.fgrid.action;

import java.util.List;

import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.fudaa.fgrid.model.ContextGeometricLayers;
import org.fudaa.fudaa.fgrid.model.GeometricPointModel;
import org.fudaa.fudaa.fgrid.model.MultiGroupPointFindResult;
/**
 * 
 * @author Christophe CANEL (Genesis)
 *
 */
public class UnusedPointsFinder
{
  private List<ZCalquePointEditable> layers;
  private ContextGeometricLayers context;
  private MultiGroupPointFindResult result;
  
  public UnusedPointsFinder(List<ZCalquePointEditable> layers, ContextGeometricLayers context)
  {
    this.layers = layers;
    this.context = context;
  }
  
  public void process()
  {
    this.result = new MultiGroupPointFindResult();
    
    for (int i = 0; i < this.layers.size(); i++)
    {
      GeometricPointModel model = (GeometricPointModel)this.layers.get(i).getModelEditable();
      
      int nbPts = model.getNombre();
      
      for (int j = 0; j < nbPts; j++)
      {
        long ptId = model.getPointId(j);
       
        if (!this.context.isPointUsedByALine(ptId))
        {
          this.result.addObject(this.layers.get(i), j);
        }
      }
    }
  }
  
  public MultiGroupPointFindResult getResult()
  {
    return this.result;
  }
}
