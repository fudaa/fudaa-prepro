package org.fudaa.fudaa.fgrid.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.fgrid.layer.ContextGeometricLayersImpl;
import org.fudaa.fudaa.fgrid.model.MultiGroupLineFindResult;
import org.fudaa.fudaa.fgrid.model.MultiGroupPointFindResult;
/**
 * 
 * @author Christophe CANEL (Genesis)
 * This action is able to project a point on a line or segment
 */
@SuppressWarnings("serial")
public class ProjectPointOnLineAction extends EbliActionSimple
{
  final ContextGeometricLayersImpl context;
  final CtuluCommandContainer cmdContainer;
  
  public ProjectPointOnLineAction(final ContextGeometricLayersImpl context, CtuluCommandContainer cmdContainer)
  {
    super(Messages.getString("projectPointOnLineAction.actionText"), EbliResource.EBLI.getToolIcon("draw-add-pt"), "PROJECT_POINT_ON_LINE"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    this.context = context;
    this.cmdContainer = cmdContainer;
  }

  @Override
  public void actionPerformed(final ActionEvent _e)
  {
    MultiGroupLineFindResult linesFound = this.context.getSelectionInLineLayers();
    
    if (linesFound.getNbObjects() != 1)
    {
      return;
    }

    MultiGroupPointFindResult pointsFound = this.context.getSelectionInPointLayers();
    
    if (pointsFound.getNbObjects() != 1)
    {
      return;
    }

    PointOnLineProjecter projecter = new PointOnLineProjecter(linesFound.getLineFindResult(0), pointsFound.getPointFindResult(0), this.cmdContainer);
    
    projecter.process();
  }
}
