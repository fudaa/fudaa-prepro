package org.fudaa.fudaa.fgrid.layer;

import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.sig.layer.FSigLayerPointEditable;

/**
 * Builder for altimetry layers.
 * 
 * @author deniger
 */
public class AltimetryLayersBuilder {

  protected static FSigLayerGroup buildGroup(final GridVisulPanel visuPanel) {
    final FSigLayerGroup grAltimetry = new FSigLayerGroup(visuPanel, AltimetryLayersBuilder.buildAttributes());
    grAltimetry.setTitle("Données altimétriques");
    grAltimetry.setName("grAlti");
    return grAltimetry;
  }

  /**
   * @param parent the parent group layer to complete with default layer
   */
  protected static void buildDefault(final FSigLayerGroup parent) {
    addLevelLayerLine(parent);
    addPointsLayer(parent);
    addProfileLayerLine(parent);

  }

  private static ZCalqueLigneBrisee addLevelLayerLine(final FSigLayerGroup parent) {
    final GISZoneCollectionLigneBrisee ligne = GeometricLayerBuilder.createZoneCollectionLigne(parent.getAttributes(),
        parent.getGisEditor());
    return parent.addLigneBriseeLayerAct("Lignes de niveau", ligne);
  }

  private static ZCalqueLigneBrisee addProfileLayerLine(final FSigLayerGroup parent) {
    final GISZoneCollectionLigneBrisee ligne = GeometricLayerBuilder.createZoneCollectionLigne(parent.getAttributes(),
        parent.getGisEditor());
    return parent.addLigneBriseeLayerAct("Profils", ligne);
  }

  private static FSigLayerPointEditable addPointsLayer(final FSigLayerGroup parent) {
    final GISZoneCollectionPoint pointModele = GeometricLayerBuilder.createZoneCollectionPoint(parent.getAttributes(),
        parent.getGisEditor());
    return (FSigLayerPointEditable) parent.addPointLayerAct("Semis de points", pointModele);
  }

  static GISAttribute[] buildAttributes() {
    return new GISAttribute[] { GISAttributeConstants.BATHY };
  }
}
