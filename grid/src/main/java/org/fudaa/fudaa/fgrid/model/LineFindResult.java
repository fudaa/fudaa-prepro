package org.fudaa.fudaa.fgrid.model;

import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;

/**
 * Result of a search operation on a line.
 *
 * @author deniger
 */
public class LineFindResult {
  public static final LineFindResult EMPTY = new LineFindResult() {
    @Override
    public boolean isEmpty() {
      return true;
    }
  };
  /**
   * The layer containing the sarched line
   */
  public ZCalqueLigneBriseeEditable layer;
  /**
   * position of the found line
   */
  public int posOfLine = -1;
  /**
   * Position of the atom on the line. If <0 -> not used
   */
  public int posOfSommet = -1;
  /**
   * Idx of the segment on the line. If <0 -> not used
   */
  public int idxOfSegment = -1;

  public boolean isEmpty() {
    return layer == null || posOfLine < 0;
  }

  public boolean isNotEmpty() {
    return !isEmpty();
  }
}
