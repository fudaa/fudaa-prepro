package org.fudaa.fudaa.fgrid.action;

import java.util.List;

import org.fudaa.ctulu.gis.GISLigneBrisee;
import org.fudaa.ctulu.gis.GISLigneBriseeComparator;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.fudaa.fgrid.model.GeometricLigneBriseeModel;
import org.fudaa.fudaa.fgrid.model.LineFind;
import org.fudaa.fudaa.fgrid.model.MultiGroupLineFindResult;

public class DuplicateLineDetector
{  
  private List<ZCalqueLigneBriseeEditable> layers;
  private MultiGroupLineFindResult result;
  private GISLigneBriseeComparator comparator;
  
  public DuplicateLineDetector(List<ZCalqueLigneBriseeEditable> layers)
  {
    super();
    this.layers = layers;
    this.result = new MultiGroupLineFindResult();
    // TODO Gerer eps.
    this.comparator = new GISLigneBriseeComparator();
  }
  
  public void process()
  {
    LineFind tester = new LineFind();

    for (int i = 0; i < this.layers.size(); i++)
    {
      GeometricLigneBriseeModel model1 = (GeometricLigneBriseeModel)this.layers.get(i).modeleDonnees();
      
      for (int j = 0; j < model1.getNombre(); j++)
      {
        tester.setIdxLine(j);

        // TODO Ne fonctionne pas!!!
//        if (this.result.containsObject(this.layers.get(i), tester))
//        {
//          continue;
//        }
        
        GISLigneBrisee line1 = (GISLigneBrisee)model1.getGeomData().getGeometry(j);
        
        for (int k = i; k < this.layers.size(); k++)
        {
          GeometricLigneBriseeModel model2 = (GeometricLigneBriseeModel)this.layers.get(k).modeleDonnees();

          for (int l = ((i == k) ? j + 1 : 0); l < model2.getNombre(); l++)
          {
            GISLigneBrisee line2 = (GISLigneBrisee)model2.getGeomData().getGeometry(l);
            
            if (comparator.compare(line1, line2) == 0)
            {
              this.result.addObject(this.layers.get(k), new LineFind(l));
            }
          }
        }
      }
    }
  }

  public MultiGroupLineFindResult getResult()
  {
    return this.result;
  }
}
