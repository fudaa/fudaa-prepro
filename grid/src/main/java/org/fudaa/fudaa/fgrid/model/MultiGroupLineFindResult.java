package org.fudaa.fudaa.fgrid.model;

import gnu.trove.TIntArrayList;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;

public class MultiGroupLineFindResult extends MultiGroupObjectFindResult<ZCalqueLigneBriseeEditable, LineFind>
{
  public LineFindResult getLineFindResult(int idx)
  {
    LineFindResult result = new LineFindResult();
    
    if (idx < this.getNbObjects())
    {
      Set<ZCalqueLigneBriseeEditable> calques = this.getSupports();
      int offset = 0;
      
      for (ZCalqueLigneBriseeEditable calque : calques)
      {
        if (idx < (this.getNbObjects(calque) + offset))
        {
          LineFind line = this.getObjects(calque).get(idx - offset);
          result.layer = calque;
          result.posOfLine = line.getIdxLine();
          result.idxOfSegment = line.getIdxSegment();
          result.posOfSommet = line.getIdxPoint();
          
          break;
        }
        else
        {
          offset += this.getNbObjects(calque);
        }
      }
    }
    
    return result;
  }

  public List<LineFindResult> getLines()
  {
    TreeSet<LineFindResult> lines = new TreeSet<LineFindResult>(new LineFindResultComparator());
    
    for (int i = 0; i < this.getNbObjects(); i++)
    {
      LineFindResult line = this.getLineFindResult(i);
      
      if (!lines.contains(line))
      {
        LineFindResult temp = new LineFindResult();
        temp.layer = line.layer;
        temp.posOfLine = line.posOfLine;
        
        lines.add(temp);
      }
    }
    
    return new ArrayList<LineFindResult>(lines);
  }
  
  public int[] getPoints(LineFindResult line)
  {
    List<LineFind> objects = this.getObjects(line.layer);
    TIntArrayList points = new TIntArrayList();
    
    for (int i = 0; i < objects.size(); i++)
    {
      LineFind lineFind = objects.get(i);
      
      if ((lineFind.getIdxLine() == line.posOfLine) && lineFind.usePoint())
      {
        points.add(lineFind.getIdxPoint());
      }
    }
    
    return points.toNativeArray();
  }
  
  public int[] getSegments(LineFindResult line)
  {
    List<LineFind> objects = this.getObjects(line.layer);
    TIntArrayList segments = new TIntArrayList();
    
    for (int i = 0; i < objects.size(); i++)
    {
      LineFind lineFind = objects.get(i);
      
      if ((lineFind.getIdxLine() == line.posOfLine) && lineFind.useSegment())
      {
        segments.add(lineFind.getIdxSegment());
      }
    }
    
    return segments.toNativeArray();
  }
}
