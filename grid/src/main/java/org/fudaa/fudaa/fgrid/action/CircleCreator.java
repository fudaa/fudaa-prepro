package org.fudaa.fudaa.fgrid.action;

import java.awt.Component;

import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import org.locationtech.jts.geom.Coordinate;

public class CircleCreator
{
  @SuppressWarnings("serial")
  private static class AddCircleDialogPanel extends CtuluDialogPanel
  {
    private BuTextField x;
    private BuTextField y;
    private BuTextField rayon;
    private BuTextField nbPts;
    private double startDegree;
    
    public AddCircleDialogPanel(Coordinate center)
    {
      this.setLayout(new BuGridLayout(2));
      
      this.x = this.addLabelDoubleText(Messages.getString("circleCreator.xCenter")); //$NON-NLS-1$
      this.y = this.addLabelDoubleText(Messages.getString("circleCreator.yCenter")); //$NON-NLS-1$
      this.rayon = this.addLabelDoubleText(Messages.getString("circleCreator.radius")); //$NON-NLS-1$
      this.nbPts = this.addLabelIntegerText(Messages.getString("circleCreator.nbPts")); //$NON-NLS-1$
      
      this.x.setText(String.valueOf(center.x));
      this.y.setText(String.valueOf(center.y));
      this.rayon.setText("1"); //$NON-NLS-1$
      this.nbPts.setText("10"); //$NON-NLS-1$
    }

    @Override
    public boolean isDataValid()
    {
      boolean isValid = true;
            
      if (this.x.getValue() == null)
      {
        this.setErrorText(Messages.getString("circleCreator.xMustBeReal")); //$NON-NLS-1$
        
        isValid = false;
      }
      
      if (this.y.getValue() == null)
      {
        this.setErrorText(Messages.getString("circleCreator.yMustBeReal")); //$NON-NLS-1$
        
        isValid = false;
      }

      if (this.rayon.getValue() == null)
      {
        this.setErrorText(Messages.getString("circleCreator.radiusMustBeReal")); //$NON-NLS-1$
        
        isValid = false;
      }

      if (this.nbPts.getValue() == null)
      {
        this.setErrorText(Messages.getString("circleCreator.nbPtsMustBeInteger")); //$NON-NLS-1$
        
        isValid = false;
      }
      else if (Integer.parseInt(this.nbPts.getText()) < 3)
      {
        this.setErrorText(Messages.getString("circleCreator.nbPtsMustBeMin3")); //$NON-NLS-1$
        
        isValid = false;
      }

      return isValid;
    }
    
    public Coordinate getCenter()
    {
      return new Coordinate(Double.parseDouble(this.x.getText()), Double.parseDouble(this.y.getText()));
    }
    
    public double getRayon()
    {
      return Double.parseDouble(this.rayon.getText());
    }
    
    public int getNbPts()
    {
      return Integer.parseInt(this.nbPts.getText());
    }
    
    public double getStartDegree()
    {
      return this.startDegree;
    }
  }

  public static GISPolygone createCircleWithDialog()
  {
    return CircleCreator.createCircleWithDialog(new Coordinate());
  }
  
  public static GISPolygone createCircleWithDialog(Coordinate center)
  {
    AddCircleDialogPanel dialog = new AddCircleDialogPanel(center);
    
    if (dialog.afficheModale((Component)null, Messages.getString("circleCreator.circleParameters")) == CtuluDialog.OK_OPTION) //$NON-NLS-1$
    {
      return CircleCreator.createCircle(dialog.getCenter(), dialog.getRayon(), dialog.getStartDegree(), dialog.getNbPts());
    }
    
    return null;
  }
  
  public static GISPolygone createCircle(Coordinate center, double rayon, double startDegree, int nbPts)
  {
    Coordinate[] coords = new Coordinate[nbPts + 1];
    double degreeInc = ((double)360) / ((double)nbPts);
    double degree = startDegree;
    
    for (int i = 0; i < nbPts; i++)
    {
      double x = (rayon * Math.cos(Math.toRadians(degree))) + center.x;
      double y = (rayon * Math.sin(Math.toRadians(degree))) + center.y;
      
      coords[i] = new Coordinate(x, y);
      
      degree += degreeInc;
    }

    coords[nbPts] = new Coordinate(coords[0]);
    
    return (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(coords);
  }
}
