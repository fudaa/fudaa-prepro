package org.fudaa.fudaa.fgrid.action;

import java.util.List;
import java.util.Set;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.fudaa.fgrid.model.ContextGeometricLayers;
import org.fudaa.fudaa.fgrid.model.GeometricPointModel;
import org.fudaa.fudaa.fgrid.model.MultiGroupPointFindResult;

public class UnusedPointRemover
{
  private List<ZCalquePointEditable> layers;
  private CtuluCommandContainer cmdContainer;
  private ContextGeometricLayers context;
  private int nbPointsRemoved;

  public UnusedPointRemover(List<ZCalquePointEditable> layers, CtuluCommandContainer cmdContainer, ContextGeometricLayers context)
  {
    super();
    this.layers = layers;
    this.cmdContainer = cmdContainer;
    this.context = context;
    this.nbPointsRemoved = 0;
  }

  public void process()
  {
    this.nbPointsRemoved = 0;
    
    CtuluCommandComposite cmp = (this.cmdContainer == null) ? null : new CtuluCommandComposite();
    
    UnusedPointsFinder finder = new UnusedPointsFinder(this.layers, this.context);
    
    finder.process();
    
    MultiGroupPointFindResult result = finder.getResult();
    
    Set<ZCalquePointEditable> supports = result.getSupports();
    
    for (ZCalquePointEditable layer : supports)
    {
      List<Integer> points = result.getObjects(layer);
      int[] idxs = new int[points.size()];
      
      for (int i = 0; i < idxs.length; i++)
      {
        idxs[i] = points.get(i);
      }
      
      ((GeometricPointModel)layer.getModelEditable()).removePoint(idxs, cmp);

      this.nbPointsRemoved += idxs.length;
    }

    if (this.cmdContainer != null) {
      this.cmdContainer.addCmd(cmp.getSimplify());
    }    
  }
  
  public int getNbPointsRemoved()
  {
    return nbPointsRemoved;
  }
}
