package org.fudaa.fudaa.fgrid.model;

import org.fudaa.ebli.calque.edition.ZCalquePointEditable;

/**
 * Result of a search operation on a node.
 * 
 * @author deniger
 */
public class PointFindResult {

  public static final PointFindResult EMPTY = new PointFindResult() {
    @Override
    public boolean isEmpty() {
      return true;
    }
  };

  public ZCalquePointEditable layer;
  public int idx = -1;

  public boolean isEmpty() {
    return layer == null || idx < 0;
  }

  public boolean isNotEmpty() {
    return !isEmpty();
  }

}
