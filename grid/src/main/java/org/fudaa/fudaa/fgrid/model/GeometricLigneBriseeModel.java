package org.fudaa.fudaa.fgrid.model;

import java.util.HashMap;
import java.util.Map;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.LineString;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISAttributeModelLongInterface;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneListenerDispatcher;
import org.fudaa.ebli.calque.edition.ZEditionAttibutesContainer;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.fgrid.layer.ContextGeometricLayersImpl;

/**
 * Model to be used to create or delete nodes and lines.
 * 
 * @author deniger
 */
public class GeometricLigneBriseeModel extends ZModeleLigneBriseeEditable {

  final ContextGeometricLayers geoContext;

  final GeometricLineModelIndexer indexer;

  public GeometricLigneBriseeModel(ContextGeometricLayers geoContext) {
    this(geoContext, null);
  }

  public GeometricLigneBriseeModel(ContextGeometricLayers geoContext, GISZoneCollectionLigneBrisee zone) {
    super(zone);
    this.geoContext = geoContext;
    ((GISZoneListenerDispatcher) zone.getListener()).addListener(new GeometricPointFromLineUpdater(geoContext, this));
    indexer = new GeometricLineModelIndexer(getGeomData());
  }
  
  

  /**
   * Le point sera cr��. {@inheritDoc}
   */
  @Override
  public void addPoint(final int _ligneIdx, final int _idxBefore, final double _x, final double _y,Double _z, 
      final CtuluCommandContainer _cmd) {
    CtuluCommandComposite cmp = new CtuluCommandComposite();
    GeometricPointModel modelEditable = geoContext.getSupportModelPoint();
    long value = modelEditable.addPointAndReturnId(new GrPoint(_x, _y, 0), null, cmp);
    addPoint(_ligneIdx, _idxBefore, _x, _y, value, cmp);
 // Mise a jour du Z si necessaire.
    GISAttributeInterface attZ=geometries_.getAttributeIsZ();
    if (_z!=null && attZ!=null && attZ.isAtomicValue()) {
      GISAttributeModel md=(GISAttributeModel)geometries_.getModel(attZ).getObjectValueAt(_ligneIdx);
      md.setObject(_idxBefore+1, _z, cmp);
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
  }

  /**
   * Le point doit d�j� exister.
   * 
   * @param _ligneIdx
   * @param _idxBefore
   * @param _x
   * @param _y
   * @param id
   * @param _cmd
   */
  private void addPoint(final int _ligneIdx, final int _idxBefore, final double _x, final double _y, long id,
      final CtuluCommandContainer _cmd) {
    if (geometries_ == null) { return; }
    final double x = _x;
    final CoordinateSequence g = ((LineString) geometries_.getGeometry(_ligneIdx)).getCoordinateSequence();
    final double x1 = g.getX(_idxBefore);
    final double y1 = g.getY(_idxBefore);
    final double x2 = g.getX(_idxBefore + 1);
    final double y2 = g.getY(_idxBefore + 1);
    final double y = y2 + (x2 - x) * (y1 - y2) / (x2 - x1);
    Map<GISAttributeInterface, Object> nodeId = new HashMap<GISAttributeInterface, Object>();
    nodeId.put(AttributeConstants.POINT_ID, Long.valueOf(id));
//    geometries_.addAtomic(_ligneIdx, _idxBefore, x, y, nodeId, _cmd);
  }

  public void addPoint(int ligneIdx, int idxBefore, long pointIdx, CtuluCommandContainer cmd) {
    PointFindResult pointsUsed = geoContext.locatePoint(pointIdx);

    this.addPoint(ligneIdx, idxBefore, ((ZModelePointEditable) pointsUsed.layer.getModelEditable())
        .getX(pointsUsed.idx), ((ZModelePointEditable) pointsUsed.layer.getModelEditable()).getY(pointsUsed.idx),
        pointIdx, cmd);
  }

  @Override
  protected Object[] createData(LineString s, ZEditionAttributesDataI initData, CtuluCommandComposite cmp) {
    ZEditionAttributesDataI d = initData;
    // on ne cree rien si on sur que cela va planter par la suite.
    if (d == null && needData()) return null;
    if (d == null) {
      d = new ZEditionAttibutesContainer(getGeomData().getAttributes()).createLigneBriseeDataFor(s);
    }
    // l'id de la ligne ajoutee:
    final Long id = (Long) d.getValue(AttributeConstants.ID, 0);
    // serait inutile surtout si c'est une nouvelle ligne
    if (ContextGeometricLayersImpl.isNotDefinedID(id)) {
      d.setValue(AttributeConstants.ID, 0, geoContext.createLineId());
    }
    createPointIfNeedForLine(s, d, cmp);
    return super.createData(s, d, cmp);
  }

  /**
   * pour tous les points ajout�s et contenu par la ligne on v�rifie si un point est pr�sent dans le calque noeud
   * support<br>
   * for each vertex of the lines we test if the point exist and if not we create a new one and we add the id.
   * 
   * @param s
   * @param d
   * @param cmp
   */
  private void createPointIfNeedForLine(LineString s, ZEditionAttributesDataI d, CtuluCommandComposite cmp) {
    int nbGeom = d.getNbVertex();
    GrPoint pt = new GrPoint();

    for (int i = 0; i < nbGeom; i++) {
      Long value = (Long) d.getValue(AttributeConstants.POINT_ID, i);
      if (value == null || value.longValue() < 0) {
        pt.x_ = s.getCoordinateSequence().getX(i);
        pt.y_ = s.getCoordinateSequence().getY(i);
        GeometricPointModel modelEditable = geoContext.getSupportModelPoint();
        value = modelEditable.addPointAndReturnId(pt, null, cmp);
        d.setValue(AttributeConstants.POINT_ID, i, value);
      }

    }
  }

  /**
   * @return the indexer
   */
  public GeometricLineModelIndexer getIndexer() {
    return indexer;
  }

  /**
   * @param idxLineInThisLayer index in this model
   * @return the global unique ID
   */
  public long getLineId(int idxLineInThisLayer) {
    return ((GISAttributeModelLongInterface) getGeomData().getDataModel(
        getGeomData().getIndiceOf(AttributeConstants.ID))).getValue(idxLineInThisLayer);
  }

  @Override
  public boolean moveAtomic(EbliListeSelectionMultiInterface selection, double dx, double dy, double dz,
      CtuluCommandContainer cmd, CtuluUI ui) {
    return super.moveAtomic(selection, dx, dy, dz, cmd, ui);
  }

  @Override
  public boolean moveGlobal(CtuluListSelectionInterface selection, double dx, double dy, double dz,
      CtuluCommandContainer cmd) {
    return super.moveGlobal(selection, dx, dy, dz, cmd);
  }

}
