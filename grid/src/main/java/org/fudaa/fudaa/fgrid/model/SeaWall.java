package org.fudaa.fudaa.fgrid.model;

import org.fudaa.ctulu.gis.GISPolygone;

public class SeaWall
{
  private GISPolygone ridge;
  private GISPolygone foot1;
  private GISPolygone foot2;
  
  public SeaWall(GISPolygone ridge, GISPolygone foot1, GISPolygone foot2)
  {
    super();
    this.ridge = ridge;
    this.foot1 = foot1;
    this.foot2 = foot2;
  }
  
  public GISPolygone getRidge()
  {
    return ridge;
  }
  
  public GISPolygone getFoot1()
  {
    return foot1;
  }
  
  public GISPolygone getFoot2()
  {
    return foot2;
  }
}
