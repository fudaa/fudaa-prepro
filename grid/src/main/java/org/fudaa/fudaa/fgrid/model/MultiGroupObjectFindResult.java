package org.fudaa.fudaa.fgrid.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MultiGroupObjectFindResult<S, O>
{
  private final Map<S, MultiObjectFindResult<S, O>> container;
  
  public MultiGroupObjectFindResult()
  {
    this.container = new HashMap<S, MultiObjectFindResult<S, O>>();
  }
  
  public int getNbSupport()
  {
    return this.container.size();
  }
  
  public Set<S> getSupports()
  {
    return this.container.keySet();
  }
  
  public List<O> getObjects(S support)
  {
    if (!this.container.containsKey(support))
    {
      return new ArrayList<O>();
    }
    
    return this.container.get(support).getObjects();
  }

  public int getNbObjects(S support)
  {
    if (!this.container.containsKey(support))
    {
      return 0;
    }
    
    return this.container.get(support).getNbObjects();
  }
  
  public int getNbObjects()
  {
    int nbIdx = 0;
    
    Set<S> supports = this.getSupports();
    
    for (S support : supports)
    {
      nbIdx += this.getNbObjects(support);
    }
    
    return nbIdx;
  }
  
  public boolean addObject(S support, O object)
  {
    if (!this.container.containsKey(support))
    {
      this.container.put(support, new MultiObjectFindResult<S, O>(support));
    }
    
    return this.container.get(support).addObject(object);
  }
  
  public boolean removeObject(S support, O object)
  {
    if (!this.container.containsKey(support))
    {
      return false;
    }
    
    return this.container.get(support).removeObject(object);
  }

  public boolean containsObject(S support, O object)
  {
    if (!this.container.containsKey(support))
    {
      return false;
    }
    
    return this.container.get(support).containsObject(object);
  }
  
  public boolean isEmpty()
  {
    if (this.container.isEmpty())
    {
      return true;
    }
    
    Set<S> supports = this.getSupports();
    
    for (S support : supports)
    {
      if (!this.container.get(support).isEmpty())
      {
        return false;
      }
    }
    
    return true;
  }
}
