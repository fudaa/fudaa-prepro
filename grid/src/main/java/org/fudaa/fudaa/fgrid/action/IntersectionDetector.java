package org.fudaa.fudaa.fgrid.action;

import org.fudaa.ctulu.gis.GISLigneBrisee;
import org.fudaa.ctulu.gis.GISSegment;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.fudaa.fgrid.model.LineFindResult;
import org.locationtech.jts.algorithm.RobustLineIntersector;
import org.locationtech.jts.geom.Coordinate;

import java.util.ArrayList;
import java.util.List;

/**
 * Calcul des intersections pour un liste de ZCalqueLigneBriseeEditable
 * @author christophe canel
 *
 */
public class IntersectionDetector
{
  public static class Intersection
  {
    private ZCalqueLigneBriseeEditable layer1;
    private ZCalqueLigneBriseeEditable layer2;
    private int idxLine1;
    private int idxLine2;
    private int idxSegment1;
    private int idxSegment2;
    private Coordinate intersection;

    public Intersection(int idxSegment1, int idxSegment2, Coordinate intersection)
    {
      this(null, -1, idxSegment1, null, -1, idxSegment2, intersection);
    }
    
    public Intersection(ZCalqueLigneBriseeEditable layer1, int idxLine1, int idxSegment1, ZCalqueLigneBriseeEditable layer2, int idxLine2, int idxSegment2, Coordinate intersection)
    {
      this.layer1 = layer1;
      this.layer2 = layer2;
      this.idxLine1 = idxLine1;
      this.idxLine2 = idxLine2;
      this.idxSegment1 = idxSegment1;
      this.idxSegment2 = idxSegment2;
      this.intersection = intersection;
    }

    public ZCalqueLigneBriseeEditable getLayer1()
    {
      return layer1;
    }

    public void setLayer1(ZCalqueLigneBriseeEditable layer1)
    {
      this.layer1 = layer1;
    }

    public ZCalqueLigneBriseeEditable getLayer2()
    {
      return layer2;
    }

    public void setLayer2(ZCalqueLigneBriseeEditable layer2)
    {
      this.layer2 = layer2;
    }

    public int getIdxLine1()
    {
      return idxLine1;
    }

    public void setIdxLine1(int idxLine1)
    {
      this.idxLine1 = idxLine1;
    }

    public int getIdxLine2()
    {
      return idxLine2;
    }

    public void setIdxLine2(int idxLine2)
    {
      this.idxLine2 = idxLine2;
    }

    public int getIdxSegment1()
    {
      return idxSegment1;
    }

    public void setIdxSegment1(int idxSegment1)
    {
      this.idxSegment1 = idxSegment1;
    }

    public int getIdxSegment2()
    {
      return idxSegment2;
    }

    public void setIdxSegment2(int idxSegment2)
    {
      this.idxSegment2 = idxSegment2;
    }

    public Coordinate getIntersection()
    {
      return intersection;
    }

    public void setIntersection(Coordinate intersection)
    {
      this.intersection = intersection;
    }
    
    public LineFindResult getLine1()
    {
      LineFindResult result = new LineFindResult();
      
      result.layer = this.layer1;
      result.posOfLine = this.idxLine1;
      result.idxOfSegment = this.idxSegment1;
      
      return result;
    }
    
    public LineFindResult getLine2()
    {
      LineFindResult result = new LineFindResult();
      
      result.layer = this.layer2;
      result.posOfLine = this.idxLine2;
      result.idxOfSegment = this.idxSegment2;
      
      return result;
    }
  }

  private static final RobustLineIntersector helper = new RobustLineIntersector();
  
  private List<ZCalqueLigneBriseeEditable> layers;
  private List<Intersection> result;
  private List<LineFindResult> realLines;
  
  public IntersectionDetector(List<ZCalqueLigneBriseeEditable> layers)
  {
    super();
    this.layers = layers;
  }
  
  public void process()
  {
    List<GISLigneBrisee> lines = this.getAllLines();
    List<Intersection> intersections;
    
    this.result = new ArrayList<Intersection>();
    
    for (int i = 0; i < lines.size(); i++)
    {
      intersections = IntersectionDetector.getIntersections(lines.get(i));
      
      if (intersections.size() > 0)
      {
        this.addLineInfoToIntersections(this.realLines.get(i), this.realLines.get(i), intersections);
        
        this.result.addAll(intersections);
      }
      
      for (int j = i + 1; j < lines.size(); j++)
      {
        intersections = IntersectionDetector.getIntersections(lines.get(i), lines.get(j));
        
        if (intersections.size() > 0)
        {
          this.addLineInfoToIntersections(this.realLines.get(i), this.realLines.get(j), intersections);
        
          this.result.addAll(intersections);
        }
      }
    }
  }
  
  private void addLineInfoToIntersections(LineFindResult line1, LineFindResult line2, List<Intersection> intersections)
  {
    for (int i = 0; i < intersections.size(); i++)
    {
      Intersection intersection = intersections.get(i);
      
      intersection.layer1 = line1.layer;
      intersection.idxLine1 = line1.posOfLine;
      intersection.layer2 = line2.layer;
      intersection.idxLine2 = line2.posOfLine;
    }
  }
  
  public static List<Intersection> getIntersections(GISLigneBrisee line1, GISLigneBrisee line2)
  {
    if (line1 == line2)
    {
      return IntersectionDetector.getIntersections(line1);
    }
    
    List<Intersection> result = new ArrayList<Intersection>();
    
    GISSegment segment1 = new GISSegment();
    GISSegment segment2 = new GISSegment();
    
    for (int i = 0; i < line1.getNbSegment(); i++)
    {
      line1.getSegment(i, segment1);
      
      for (int j = 0; j < line2.getNbSegment(); j++)
      {
        line2.getSegment(j, segment2);
        
        Coordinate intersection = IntersectionDetector.getIntersection(segment1, segment2);
        
        if (intersection != null)
        {
          result.add(new Intersection(i, j, intersection));
        }
      }
    }
    
    return result;
  }
  
  public static List<Intersection> getIntersections(GISLigneBrisee line)
  {
    List<Intersection> result = new ArrayList<Intersection>();
    
    GISSegment segment1 = new GISSegment();
    GISSegment segment2 = new GISSegment();
    int nbSegment = line.getNbSegment();
    
    for (int i = 0; i < (nbSegment - 2); i++)
    {
      line.getSegment(i, segment1);
      
      // j = i + 2 car on ne test pas les segments qui se suivent.
      for (int j = i + 2; j < (nbSegment - (((i == 0) && line.isClosed()) ? 1 : 0)); j++)
      {
        line.getSegment(j, segment2);
        
        Coordinate intersection = IntersectionDetector.getIntersection(segment1, segment2);
        
        if (intersection != null)
        {
          result.add(new Intersection(i, j, intersection));
        }
      }
    }
    
    return result;
  }
  
  public static Coordinate getIntersection(GISSegment segment1, GISSegment segment2)
  {
    helper.computeIntersection(segment1.getPt1(), segment1.getPt2(), segment2.getPt1(), segment2.getPt2());

    if (helper.hasIntersection())
    {
      return (Coordinate)helper.getIntersection(0).clone();
    }
    
    return null;
  }
    
  public List<Intersection> getResult()
  {
    return result;
  }
  
  /**
   * 
   * @return une liste contenant toutes les lignes bris�es.
   */
  private List<GISLigneBrisee> getAllLines()
  {
    List<GISLigneBrisee> result = new ArrayList<GISLigneBrisee>();
    this.realLines = new ArrayList<LineFindResult>();
        
    for (int i = 0; i < this.layers.size(); i++)
    {
      ZModeleLigneBriseeEditable model = this.layers.get(i).modeleDonnees();
      
      for (int j = 0; j < model.getNombre(); j++)
      {
        result.add((GISLigneBrisee)model.getGeomData().getGeometry(j));
        
        LineFindResult realLine = new LineFindResult();
        realLine.layer = this.layers.get(i);
        realLine.posOfLine = j;
        
        this.realLines.add(realLine);
      }
    }
    
    return result;
  }
}
