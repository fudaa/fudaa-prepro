package org.fudaa.fudaa.fgrid.layer;

import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.fudaa.fgrid.model.RepereContainer;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.sig.layer.FSigVisuPanelController;

/**
 * GeometryModule Contexte. Contexte du module geometrique. Permet de recuperer les groupe altimetrie et geometrie.
 * Permet egalement de r�cup�rer le contexte geometrique: ce dernier facilite les recherches dans les indices.
 * 
 * @author deniger
 */
public class ContextMainGeometryModule {

  private FSigLayerGroup grAltimetry;
  private FSigLayerGroup grGeometric;
  private BGroupeCalque cqMain;
  private GridVisuPanelController visuController;
  private GridVisulPanel visuPanel;

  private final ContextGeometricLayersImpl geometricContext;
  private final RepereContainerImpl repere;

  public RepereContainer getRepere() {
    return repere;
  }

  /**
   * @return the geometricContext
   */
  public ContextGeometricLayersImpl getGeometricContext() {
    return geometricContext;
  }

  public ContextMainGeometryModule(CtuluUI ui) {
    visuController = new GridVisuPanelController(ui);
    // par la suite ce repere devra etre fourni pour le projet.
    repere = new RepereContainerImpl();
    geometricContext = new ContextGeometricLayersImpl(ui, repere, visuController);

    cqMain = new BGroupeCalque();
    cqMain.setName("grMain");
  }

  public FSigLayerGroup getGroupAltimetry() {
    return grAltimetry;
  }

  public BGroupeCalque getMainGroup() {
    return cqMain;
  }

  public FSigVisuPanelController getVisuController() {
    return visuController;
  }

  /**
   * A utiliser � l'initialisation de GridVisuPanel: ne doit etre appele qu'une fois.
   * 
   * @param gridVisulPanel
   */
  void install(GridVisulPanel gridVisulPanel) {
    assert visuPanel == null;
    visuPanel = gridVisulPanel;
    // we can select several objects in several layers.
    visuPanel.getScene().setRestrictedToCalqueActif(false);
    // install altimetry group
    grAltimetry = AltimetryLayersBuilder.buildGroup(gridVisulPanel);
    AltimetryLayersBuilder.buildDefault(grAltimetry);
    gridVisulPanel.getDonneesCalque().add(grAltimetry);
    // install geometric group
    grGeometric = GeometricLayerBuilder.createGeomtricGroupLayer(gridVisulPanel);
    gridVisulPanel.getDonneesCalque().add(grGeometric);
    GeometricLayerBuilder.buildDefault(grGeometric, this);
  }

}
