package org.fudaa.fudaa.fgrid.layer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JComponent;

import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.fudaa.sig.layer.FSigVisuPanelController;

/**
 * The controller of the visu panel.
 * 
 * @author deniger
 */
public class GridVisuPanelController extends FSigVisuPanelController {

  /**
   * palette to be used as constant component
   */
  Collection<String> useAsTab = Arrays.asList("INFOS", "PALETTE_EDTION", "NAVIGATE");
  /**
   * Contient chaque palette enlevee sous la forme l'actionCommand-> l'action
   */
  Map<String, EbliActionPaletteAbstract> mapTabPalettes_ = new HashMap<String, EbliActionPaletteAbstract>(2);

  /**
   * Les palettes utisees dans des composants normaux.
   */
  Collection<EbliActionPaletteAbstract> tabPalettes_ = null;

  /**
   * @param ui
   */
  public GridVisuPanelController(CtuluUI ui) {
    super(ui);
  }

  @Override
  protected EbliActionInterface[] getApplicationActions() {
    return removePalettes(pn_.getApplicationActions());
  }

  @Override
  protected void buildButtonGroupNavigation() {
    if (navigationActionGroup_ != null) { return; }
    super.buildButtonGroupNavigation();
    final EbliActionInterface[] actions = navigationActionGroup_;
    navigationActionGroup_ = removePalettes(actions);
  }

  @Override
  protected void buildButtonGroupStandard() {
    if (standardActionGroup_ != null) return;
    super.buildButtonGroupStandard();
    standardActionGroup_ = removePalettes(standardActionGroup_);
  }

  protected void buildButtonGroupSpecifiques() {
    if (standardActionGroup_ != null) return;
    super.buildButtonGroupStandard();
    standardActionGroup_ = removePalettes(standardActionGroup_);

  }

  /**
   * recupere les palettes et initialise les actions
   */
  @Override
  public Collection<EbliActionPaletteAbstract> getTabPaletteAction() {
    // si deja fait on retourne de suite
    if (tabPalettes_ != null) return tabPalettes_;
    initSpecificActions();
    tabPalettes_ = new ArrayList<EbliActionPaletteAbstract>(3);
    for (final String str : useAsTab) {
      final EbliActionPaletteAbstract o = mapTabPalettes_.get(str);
      if (o != null) tabPalettes_.add(o);
    }
    return tabPalettes_;
  }

  /**
   * @param actions les actions a trier
   * @return la listes des actions a ajouter dans les menus et autres. Les autres ( les palettes) seront visible tout le
   *         temps dans des dock.
   */
  private EbliActionInterface[] removePalettes(final EbliActionInterface[] actions) {

    final List<EbliActionInterface> acts = new ArrayList<EbliActionInterface>(actions.length);

    for (int i = 0; i < actions.length; i++) {
      final EbliActionInterface action = actions[i];
      if (action == null) continue;
      final String value = (String) action.getValue(Action.ACTION_COMMAND_KEY);
      final boolean isPalette = action instanceof EbliActionPaletteAbstract;
      if (isPalette && useAsTab.contains(value)) {
        mapTabPalettes_.put(value, (EbliActionPaletteAbstract) action);
      } else {
        acts.add(action);
      }
    }
    return acts.toArray(new EbliActionInterface[acts.size()]);
  }

  public JComponent getPaletteInfoModel() {
    return mapTabPalettes_.get("PALETTE_EDTION").getPaletteContent();
  }
}
