package org.fudaa.fudaa.fgrid.model;

public interface RepereContainer {

  /**
   * @param x
   * @param y
   * @param x1
   * @param y1
   * @return true si les 2 points x,y et x1,y1 sont les memes a eps pres
   */
  boolean isSamePoint(double x, double y, double x1, double y1);

  /**
   * @param x
   * @param y
   * @param x1
   * @param y1
   * @return resultat inverse de {@link #isSamePoint(double, double, double, double)}
   */
  boolean isDifferentPoint(double x, double y, double x1, double y1);

}