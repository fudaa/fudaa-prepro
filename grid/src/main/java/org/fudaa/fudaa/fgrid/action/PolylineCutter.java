package org.fudaa.fudaa.fgrid.action;

import java.util.List;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.fudaa.fgrid.model.LineFindResult;
import org.fudaa.fudaa.fgrid.model.MultiGroupLineFindResult;

public class PolylineCutter
{
  public enum PolylineCutterResult
  {
    SUCCEEDED,
    NOT_LINE_SELECTED,
    TOO_MANY_LINE_SELECTED,
    TOO_MANY_POINT_SELECTED,
    TOO_MANY_SEGMENT_SELECTED,
    NOT_POINT_OR_SEGMENT_SELECTED,
    POINT_AND_SEGMENT_SELECTED,
    CAN_CUT_ONLY_POLYLINE
  }
  
  private MultiGroupLineFindResult lines;
  private CtuluCommandContainer cmdContainer;
  private PolylineCutterResult result;
  
  public PolylineCutter(MultiGroupLineFindResult lines, CtuluCommandContainer cmdContainer)
  {
    super();
    this.lines = lines;
    this.cmdContainer = cmdContainer;
    this.result = PolylineCutterResult.SUCCEEDED;
  }

  public void process()
  {
    if ((this.lines == null) || this.lines.isEmpty())
    {
      this.result = PolylineCutterResult.NOT_LINE_SELECTED;
      
      return;
    }

    List<LineFindResult> realLines = this.lines.getLines();
    
    if (realLines.size() > 1)
    {
      this.result = PolylineCutterResult.TOO_MANY_LINE_SELECTED;
      
      return;
    }

    LineFindResult line = realLines.get(0);
    ZModeleLigneBriseeEditable model = line.layer.modeleDonnees();

    if (!(model.getGeomData().getGeometry(line.posOfLine) instanceof GISPolyligne))
    {
      this.result = PolylineCutterResult.CAN_CUT_ONLY_POLYLINE;
      
      return;
    }
    
    int[] ptsIdx = this.lines.getPoints(line);
    int[] segIdx = this.lines.getSegments(line);

    this.result = this.testSelectedIdx(ptsIdx, segIdx);
    
    if (this.result != PolylineCutterResult.SUCCEEDED)
    {
      return;
    }
    
    int[] idxs = this.getIdxToCut(ptsIdx, segIdx);
    
    if (idxs.length == 1)
    {
      model.splitGeometry(line.posOfLine, idxs[0], cmdContainer);
    }
    else
    {
      model.splitGeometry(line.posOfLine, idxs, cmdContainer);
    }
  }

  private int[] getIdxToCut(int[] points, int[] segments)
  {
    if (points.length > segments.length)
    {
      return points;
    }
    else
    {
      return new int[]{segments[0], segments[0]};
    }
  }
  
  private PolylineCutterResult testSelectedIdx(int[] points, int[] segments)
  {
    int nbPoints = points.length;
    int nbSegments = segments.length;
    
    if ((nbPoints + nbSegments) == 0)
    {
      return PolylineCutterResult.NOT_POINT_OR_SEGMENT_SELECTED;
    }
    
    if ((nbPoints != 0) && (nbSegments != 0))
    {
      return PolylineCutterResult.POINT_AND_SEGMENT_SELECTED;
    }
    
    if (nbPoints > 2)
    {
      return PolylineCutterResult.TOO_MANY_POINT_SELECTED;
    }

    if (nbSegments > 1)
    {
      return PolylineCutterResult.TOO_MANY_SEGMENT_SELECTED;
    }
    
    return PolylineCutterResult.SUCCEEDED;
  }
  
  public PolylineCutterResult getResult()
  {
    return result;
  }
}
