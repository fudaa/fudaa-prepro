package org.fudaa.fudaa.fgrid.model;

import gnu.trove.TLongObjectHashMap;

import java.util.ArrayList;
import java.util.Collection;

import org.fudaa.ctulu.collection.CtuluCollectionEmpty;
import org.fudaa.ctulu.collection.CtuluCollectionInteger;
import org.fudaa.ctulu.collection.CtuluListInteger;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISAttributeModelLongInterface;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.fudaa.sig.layer.FSigLayerLineEditable;

/**
 * An indexer for line. Used to fine id of used line and of used points.
 * 
 * @author deniger
 */
public class GeometricLineModelIndexer extends GeometricDefaultIndexer {

  /**
   * Use to give the position of the lines using a node with a given id.
   */
  TLongObjectHashMap<CtuluListInteger> idNodeForLinePosition = new TLongObjectHashMap<CtuluListInteger>();

  /**
   * @param collection
   */
  public GeometricLineModelIndexer(GISZoneCollection collection) {
    super(collection);
  }

  @Override
  protected void postIndex() {
    // fill the idNodeForLinePosition map
    idNodeForLinePosition.clear();
    GISAttributeModel dataModel = getNodeIdLongModel();
    int nbLine = dataModel.getSize();
    for (int i = 0; i < nbLine; i++) {
      fillMap(i, (GISAttributeModelLongInterface) dataModel.getObjectValueAt(i));
    }
  }

  protected GISAttributeModel getNodeIdLongModel() {
    int idxOfNodeId = collection.getIndiceOf(AttributeConstants.POINT_ID);
    GISAttributeModel dataModel = collection.getDataModel(idxOfNodeId);
    return dataModel;
  }

  public boolean isNodeUsedByALine(long nodeId) {
    index();
    return idNodeForLinePosition.contains(nodeId);
  }

  public CtuluCollectionInteger getLinePositionUsingThisNode(long nodeId) {
    index();
    return idNodeForLinePosition.contains(nodeId) ? idNodeForLinePosition.get(nodeId) : CtuluCollectionEmpty.INTEGER;
  }

  /**
   * @param id l'id du point
   * @return une collection de LineFindResult pour chaque somment utilisant l'id du point.Attention le calque n'est pas
   *         renseigne dans les LineFindResult.
   */
  public Collection<LineFindResult> getLineUsingPointId(long id) {
    Collection<LineFindResult> res = new ArrayList<LineFindResult>();
    collectLineUsingPointId(id, res, null);
    return res;
  }

  /**
   * @param pointID id searched
   * @param out the list that will be completed
   * @param containerLayer the containing layer used to fill LineFindResult.
   */
  public void collectLineUsingPointId(long pointID, Collection<LineFindResult> out, FSigLayerLineEditable containerLayer) {
    CtuluCollectionInteger lines = getLinePositionUsingThisNode(pointID);
    if (lines == null) return;
    int nb = lines.getSize();
    GISAttributeModel dataModel = getNodeIdLongModel();
    for (int i = 0; i < nb; i++) {
      int lineIdx = lines.getValue(i);
      GISAttributeModelLongInterface ids = ((GISAttributeModelLongInterface) dataModel.getObjectValueAt(lineIdx));
      int nbVertex = ids.getSize();
      for (int j = 0; j < nbVertex; j++) {
        if (ids.getValue(j) == pointID) {
          LineFindResult findRes = new LineFindResult();
          findRes.posOfLine = lineIdx;
          findRes.posOfSommet = j;
          findRes.layer =  containerLayer;
          out.add(findRes);
        }
      }
    }
  }

  /**
   * @param linePos the position of the line in this zone
   * @param nodesId list of nodes id used by the line
   */
  private void fillMap(int linePos, GISAttributeModelLongInterface nodesId) {
    int nbPt = nodesId.getSize();
    for (int i = 0; i < nbPt; i++) {
      long nodeId = nodesId.getValue(i);
      CtuluListInteger lines = idNodeForLinePosition.get(nodeId);
      if (lines == null) {
        lines = new CtuluListInteger();
        idNodeForLinePosition.put(nodeId, lines);
      }
      lines.add(linePos);
    }

  }

  /**
   * @param lineId l'ID of the point
   * @return if contains by this collection.
   */
  public boolean isLineUsed(long lineId) {
    index();
    return idForPosition.contains(lineId);
  }

}
