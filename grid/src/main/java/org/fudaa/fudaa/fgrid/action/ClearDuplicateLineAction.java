package org.fudaa.fudaa.fgrid.action;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.fgrid.layer.ContextGeometricLayersImpl;

@SuppressWarnings("serial")
public class ClearDuplicateLineAction extends EbliActionSimple
{
  final ContextGeometricLayersImpl context;
  final CtuluCommandContainer cmdContainer;
  
  public ClearDuplicateLineAction(final ContextGeometricLayersImpl context, CtuluCommandContainer cmdContainer)
  {
    super(Messages.getString("clearDuplicateLineAction.actionText"), EbliResource.EBLI.getToolIcon("draw-add-pt"), "CLEAR_DUPLICATE_LINE"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    this.context = context;
    this.cmdContainer = cmdContainer;
  }

  @Override
  public void actionPerformed(final ActionEvent _e)
  {
    DuplicateLineCleaner cleaner = new DuplicateLineCleaner(new ArrayList<ZCalqueLigneBriseeEditable>(this.context.getLineLayers()), this.cmdContainer);
    cleaner.process();
  }
}
