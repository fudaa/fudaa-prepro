package org.fudaa.fudaa.fgrid.model;

import java.util.Set;

import org.fudaa.ebli.calque.edition.ZCalquePointEditable;

public class MultiGroupPointFindResult extends MultiGroupObjectFindResult<ZCalquePointEditable, Integer>
{
  public PointFindResult getPointFindResult(int idx)
  {
    PointFindResult result = new PointFindResult();
    
    if (idx < this.getNbObjects())
    {
      Set<ZCalquePointEditable> calques = this.getSupports();
      int offset = 0;
      
      for (ZCalquePointEditable calque : calques)
      {
        if (idx < (this.getNbObjects(calque) + offset))
        {
          result.layer = calque;
          result.idx = this.getObjects(calque).get(idx - offset);
          
          break;
        }
        else
        {
          offset += this.getNbObjects(calque);
        }
      }
    }
    
    return result;
  }
}
