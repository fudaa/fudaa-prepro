package org.fudaa.fudaa.fgrid.action;

import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.fgrid.action.IntersectionDetector.Intersection;
import org.fudaa.fudaa.fgrid.model.GeometricPointModel;

import org.locationtech.jts.geom.Coordinate;

public class IntersectionPointsAdder
{
  private static class CoordinateComparator implements Comparator<Coordinate>
  {
    private double eps;
    
    public CoordinateComparator()
    {
      this(0.1d);
    }
    
    public CoordinateComparator(double eps)
    {
      this.eps = eps;
    }
    
    public double getEps()
    {
      return eps;
    }

    public void setEps(double eps)
    {
      this.eps = eps;
    }

    @Override
    public int compare(Coordinate point1, Coordinate point2)
    {
      if (point1.distance(point2) < this.eps)
      {
        return 0;
      }
      else if (point1.x < point2.x)
      {
        return -1;
      }
      else if (point1.x == point2.x)
      {
        if (point1.y < point2.y)
        {
          return -1;
        }
        else if (point1.y == point2.y)
        {
          if (point1.z < point2.z)
          {
            return -1;
          }
        }
      }

      return 1;
    }
    
  }

  private final GeometricPointModel pointsModel;
  private final List<Intersection> intersections;
  private final CtuluCommandContainer cmdContainer;

  private TreeMap<Coordinate, Long> pointsAdded;

  public IntersectionPointsAdder(GeometricPointModel pointsModel, List<Intersection> intersections, CtuluCommandContainer cmdContainer)
  {
    super();
    this.pointsModel = pointsModel;
    this.intersections = intersections;
    this.cmdContainer = cmdContainer;
  }
  
  public void process()
  {
    CtuluCommandComposite cmp = (this.cmdContainer == null) ? null : new CtuluCommandComposite();

    // TODO Gerer eps.
    this.pointsAdded = new TreeMap<Coordinate, Long>(new CoordinateComparator());
    GeometricPointModel pointsModel = this.pointsModel;

    GrPoint point = new GrPoint();
    
    for (int i = 0; i < this.intersections.size(); i++)
    {
      if (!this.pointsAdded.containsKey(this.intersections.get(i).getIntersection()))
      {
        point.initialiseAvec(this.intersections.get(i).getIntersection());
        
        this.pointsAdded.put(this.intersections.get(i).getIntersection(), pointsModel.addPointAndReturnId(point, null, cmp));
      }
    }

    if (this.cmdContainer != null) {
      this.cmdContainer.addCmd(cmp.getSimplify());
    }    
  }

  public TreeMap<Coordinate, Long> getPointsAdded()
  {
    return this.pointsAdded;
  }
}
