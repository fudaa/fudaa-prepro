package org.fudaa.fudaa.fgrid.model;

import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISAttributeLong;

/**
 * The default attributes.
 * 
 * @author deniger
 */
public final class AttributeConstants {

  /**
   * A voir :sera la donn�e a utiliser par le mailleur.
   */
  public static final GISAttributeDouble GRID_PRECISION = new GISAttributeDouble("PREC", true);
  /**
   * Identifiant d'un point, ligne, surface.
   */
  public static final GISAttributeLong ID = new GISAttributeLong("ID", false);
  /**
   * Contenu par les lignes pour indique le point support
   */
  public static final GISAttributeLong POINT_ID = new GISAttributeLong("POINT_ID", true);
  /**
   * Contenu par les surface pour indiquer l'id de la ligne.
   */
  public static final GISAttributeLong LINE_ID = new GISAttributeLong("LINE_ID", true);

  static {
    AttributeConstants.ID.setUserVisible(false);
    AttributeConstants.POINT_ID.setUserVisible(false);
    AttributeConstants.LINE_ID.setUserVisible(false);
    Long def = Long.valueOf(-1);
    AttributeConstants.ID.setDefaultValue(def);
    AttributeConstants.POINT_ID.setDefaultValue(def);
    AttributeConstants.LINE_ID.setDefaultValue(def);
  }

}
