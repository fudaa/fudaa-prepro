package org.fudaa.fudaa.fgrid.action;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.fgrid.layer.ContextGeometricLayersImpl;

@SuppressWarnings("serial")
public class ClearDuplicatePointAction extends EbliActionSimple
{
  final ContextGeometricLayersImpl context;
  final CtuluCommandContainer cmdContainer;

  public ClearDuplicatePointAction(final ContextGeometricLayersImpl context, CtuluCommandContainer cmdContainer) {
    super(Messages.getString("clearDuplicatePointAction.actionText"), EbliResource.EBLI.getToolIcon("draw-add-pt"), "CLEAR_DUPLICATE_POINT"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    this.context = context;
    this.cmdContainer = cmdContainer;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    DuplicatePointCleaner cleaner = new DuplicatePointCleaner(new ArrayList<ZCalquePointEditable>(this.context.getPointLayers()), this.cmdContainer);
    cleaner.process();
  }
}
