package org.fudaa.fudaa.fgrid.model;

import java.util.Collection;

import org.fudaa.ctulu.CtuluUI;

/**
 * Context of Geometric layers
 * 
 * @author deniger
 */
public interface ContextGeometricLayers {

  long createLineId();

  GeometricPointModel getSupportModelPoint();

  long createPointId();

  /**
   * @param pointId
   * @return true if the point ID is used by a line
   */
  boolean isPointUsedByALine(long pointId);

  CtuluUI getUi();

  /**
   * @param pointId ID of point to look for
   * @return list of layer/line containing this point ID.
   */
  Collection<LineFindResult> getLineUsingPoint(long pointId);

  /**
   * @param pointId ID of point to look for
   * @return the point layer and the position of the point
   */
  PointFindResult locatePoint(long pointId);

  /**
   * @param lineId ID of line to look for
   * @return the line layer and the position of the line
   */
  LineFindResult locateLine(long lineId);

  /**
   * TODO Traduire repere...
   * @return le repere du projet
   */
  public RepereContainer getRepere();

  public MultiGroupPointFindResult getSelectionInPointLayers();

  public MultiGroupLineFindResult getSelectionInLineLayers();
}