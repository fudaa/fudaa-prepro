package org.fudaa.fudaa.fgrid.action;

import java.util.List;
import java.util.TreeMap;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.fudaa.fgrid.action.IntersectionDetector.Intersection;
import org.fudaa.fudaa.fgrid.model.GeometricPointModel;

import org.locationtech.jts.geom.Coordinate;

public class IntersectionCreater
{
  private List<ZCalqueLigneBriseeEditable> layers;
  private GeometricPointModel model;
  private CtuluCommandContainer cmdContainer;
  private int nbPointsAdded;
  private int nbPointsInserted;

  public IntersectionCreater(List<ZCalqueLigneBriseeEditable> layers, GeometricPointModel model, CtuluCommandContainer cmdContainer)
  {
    super();
    this.layers = layers;
    this.model = model;
    this.cmdContainer = cmdContainer;
    this.nbPointsAdded = 0;
    this.nbPointsInserted = 0;
  }

  public void process()
  {
    this.nbPointsAdded = 0;
    this.nbPointsInserted = 0;

    CtuluCommandComposite cmp = (this.cmdContainer == null) ? null : new CtuluCommandComposite();

    IntersectionDetector detector = new IntersectionDetector(layers);
    
    detector.process();
    
    List<Intersection> intersections = detector.getResult();

    IntersectionPointsAdder adder = new IntersectionPointsAdder(this.model, intersections, cmp);

    adder.process();
    
    TreeMap<Coordinate, Long> pointsAdded = adder.getPointsAdded();

    this.nbPointsAdded = pointsAdded.size();

    IntersectionPointsInserter inserter = new IntersectionPointsInserter(intersections, cmp, pointsAdded);
    
    inserter.process();

    this.nbPointsInserted = inserter.getNbPointsInserted();

    if (this.cmdContainer != null) {
      this.cmdContainer.addCmd(cmp.getSimplify());
    }    
  }
  
  public int getNbPointsAdded()
  {
    return nbPointsAdded;
  }

  public int getNbPointsInserted()
  {
    return nbPointsInserted;
  }
}
