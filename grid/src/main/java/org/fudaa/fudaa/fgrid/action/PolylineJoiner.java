package org.fudaa.fudaa.fgrid.action;

import java.util.List;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.fudaa.fgrid.model.LineFindResult;
import org.fudaa.fudaa.fgrid.model.MultiGroupLineFindResult;

public class PolylineJoiner
{
  public enum PolylineJoinerResult
  {
    SUCCEEDED,
    NOT_LINE_SELECTED,
    TOO_MANY_LINE_SELECTED,
    CAN_JOIN_ONLY_POLYLINE,
    NOT_ENOUGH_POINT_SELECTED,
    TOO_MANY_POINT_SELECTED,
    SEGMENT_MUST_NOT_BE_SELECTED,
    ONLY_EXTREMITY_POINT_MUST_BE_SELECTED,
    LINES_MUST_BE_IN_SAME_LAYER
  }
  
  private MultiGroupLineFindResult lines;
  private CtuluCommandContainer cmdContainer;
  private PolylineJoinerResult result;
  
  public PolylineJoiner(MultiGroupLineFindResult lines, CtuluCommandContainer cmdContainer)
  {
    super();
    this.lines = lines;
    this.cmdContainer = cmdContainer;
    this.result = PolylineJoinerResult.SUCCEEDED;
  }
  
  public void process()
  {
    this.result = PolylineJoinerResult.SUCCEEDED;
    
    if ((this.lines == null) || this.lines.isEmpty())
    {
      this.result = PolylineJoinerResult.NOT_LINE_SELECTED;
      
      return;
    }

    List<LineFindResult> realLines = this.lines.getLines();

    if (realLines.size() > 2)
    {
      this.result = PolylineJoinerResult.TOO_MANY_LINE_SELECTED;
      
      return;
    }
    
    ZCalqueLigneBriseeEditable layer = null;
    
    for (int i = 0; i < realLines.size(); i++)
    {
      LineFindResult line = realLines.get(i);
      
      if(!(line.layer.modeleDonnees().getGeomData().getGeometry(line.posOfLine) instanceof GISPolyligne))
      {
        this.result = PolylineJoinerResult.CAN_JOIN_ONLY_POLYLINE;
        
        return;
      }
      
      if (this.lines.getSegments(line).length > 0)
      {
        this.result = PolylineJoinerResult.SEGMENT_MUST_NOT_BE_SELECTED;
        
        return;
      }
      
      if ((layer != null) && (line.layer != layer))
      {
        this.result = PolylineJoinerResult.LINES_MUST_BE_IN_SAME_LAYER;
        
        return;
      }
      
      layer = line.layer;
    }
    
    if (realLines.size() == 1)
    {
      int[] idxPts = this.lines.getPoints(realLines.get(0));
      
      if (idxPts.length < 2)
      {
        this.result = PolylineJoinerResult.NOT_ENOUGH_POINT_SELECTED;
        
        return;
      }
      else if (idxPts.length > 2)
      {
        this.result = PolylineJoinerResult.TOO_MANY_POINT_SELECTED;
        
        return;
      }
      
      LineFindResult line = realLines.get(0);
      int nbPoints = line.layer.modeleDonnees().getGeomData().getGeometry(line.posOfLine).getNumPoints();
      
      if (((idxPts[0] != 0) && (idxPts[0] != nbPoints - 1)) || ((idxPts[1] != 0) && (idxPts[1] != nbPoints - 1)))
      {
        this.result = PolylineJoinerResult.ONLY_EXTREMITY_POINT_MUST_BE_SELECTED;
        
        return;
      }

      line.layer.modeleDonnees().closeGeometry(line.posOfLine, cmdContainer);
    }
    else
    {
      int[] idxPts1 = this.lines.getPoints(realLines.get(0));
      int[] idxPts2 = this.lines.getPoints(realLines.get(1));

      if ((idxPts1.length < 1) || (idxPts2.length < 1))
      {
        this.result = PolylineJoinerResult.NOT_ENOUGH_POINT_SELECTED;
        
        return;
      }
      else if ((idxPts1.length > 1) || (idxPts2.length > 1))
      {
        this.result = PolylineJoinerResult.TOO_MANY_POINT_SELECTED;
        
        return;
      }

      LineFindResult line1 = realLines.get(0);
      int nbPoints1 = line1.layer.modeleDonnees().getGeomData().getGeometry(line1.posOfLine).getNumPoints();
      LineFindResult line2 = realLines.get(1);
      int nbPoints2 = line2.layer.modeleDonnees().getGeomData().getGeometry(line2.posOfLine).getNumPoints();
      
      if (((idxPts1[0] != 0) && (idxPts1[0] != nbPoints1 - 1)) || ((idxPts2[0] != 0) && (idxPts2[0] != nbPoints2 - 1)))
      {
        this.result = PolylineJoinerResult.ONLY_EXTREMITY_POINT_MUST_BE_SELECTED;
        
        return;
      }

      line1.layer.modeleDonnees().joinGeometries(new int[]{line1.posOfLine, line2.posOfLine}, new int[]{idxPts1[0], idxPts2[0]}, null,cmdContainer);
    }
  }

  public PolylineJoinerResult getResult()
  {
    return result;
  }
}
