package org.fudaa.fudaa.fgrid.layer;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.fudaa.fgrid.model.RepereContainer;

/**
 * A renommmer en anglais et a completer
 * 
 * @author deniger
 */
public class RepereContainerImpl implements RepereContainer {

  double epsXY = 1;

  @Override
  public boolean isSamePoint(double x, double y, double x1, double y1) {
    return CtuluLib.isEquals(x, x1, epsXY) && CtuluLib.isEquals(y, y1, epsXY);
  }

  @Override
  public boolean isDifferentPoint(double x, double y, double x1, double y1) {
    return !isSamePoint(x, y, x1, y1);
  }

}
