package org.fudaa.fudaa.fgrid.action;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.fgrid.layer.ContextGeometricLayersImpl;

@SuppressWarnings("serial")
public class CreateIntersectionAction extends EbliActionSimple
{
  final ContextGeometricLayersImpl context;
  final CtuluCommandContainer cmdContainer;
  
  public CreateIntersectionAction(final ContextGeometricLayersImpl context, CtuluCommandContainer cmdContainer) {
    super(Messages.getString("createIntersectionAction.actionText"), EbliResource.EBLI.getToolIcon("draw-add-pt"), "CREATE_INTERSECTION"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    this.context = context;
    this.cmdContainer = cmdContainer;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    IntersectionCreater creater = new IntersectionCreater(new ArrayList<ZCalqueLigneBriseeEditable>(this.context.getLineLayers()), this.context.getSupportModelPoint(), this.cmdContainer);
    
    creater.process();
  }
}
