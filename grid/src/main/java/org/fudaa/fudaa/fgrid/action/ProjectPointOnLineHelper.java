package org.fudaa.fudaa.fgrid.action;

import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISLigneBrisee;
import org.fudaa.ctulu.gis.GISSegment;

import org.locationtech.jts.geom.Coordinate;
/**
 * 
 * @author Christophe CANEL (Genesis)
 * This class is an helper to project a point on a line or a segment.
 * It compute if the point can be projected, the segment where the line is projected, the projected point and if the projected point is on a point of the line.
 */
public class ProjectPointOnLineHelper
{
  private GISLigneBrisee line;
  private Coordinate point;
  private int segmentIdx;
  private boolean canProject;
  private int segmentOfProjectionIdx;
  private Coordinate projectedPoint;
  private boolean isOnSegmentPt;
  private double distance;
  private double eps;

  public ProjectPointOnLineHelper(GISLigneBrisee line, Coordinate point)
  {
    this(line, -1, point);
  }
  
  public ProjectPointOnLineHelper(GISLigneBrisee line, int segmentIdx, Coordinate point)
  {
    this.line = line;
    this.point = point;
    this.segmentIdx = segmentIdx;
    this.canProject = false;
    this.isOnSegmentPt = false;
    this.distance = 0d;
    // TODO Gerer eps.
    this.eps = 0.1d;
  }
  
  public void compute()
  {
    if (this.segmentIdx >= 0)
    {
      this.testProjectionOnSegment(this.segmentIdx);
    }
    else
    {
      for (int i = 0; i < this.line.getNbSegment(); i++)
      {
        this.testProjectionOnSegment(i);
      }
    }
  }
  
  private void testProjectionOnSegment(int segmentIdxToTest)
  {
    GISSegment segment = this.line.getSegment(segmentIdxToTest);
    
    if (segment == null)
    {
      return;
    }
    
    Coordinate tempPoint = this.getProjectedPoint(segment.getPt1(), segment.getPt2(), this.point);
    
    if (tempPoint == null)
    {
      return;
    }

    if ((!this.canProject) || (CtuluLibGeometrie.getDistanceXY(this.point, tempPoint) < this.distance))
    {
      this.distance = this.point.distance(tempPoint);
      this.projectedPoint = tempPoint;
      this.segmentOfProjectionIdx = segmentIdxToTest;
      this.canProject = true;
      this.isOnSegmentPt = ((this.projectedPoint.distance(segment.getPt1()) < eps) || (this.projectedPoint.distance(segment.getPt2()) < eps));
    }
  }
  
  private Coordinate getProjectedPoint(Coordinate segmentPt1, Coordinate segmentPt2, Coordinate point)
  {
    double p = (point.x - segmentPt1.x) * (segmentPt2.x - segmentPt1.x) + (point.y - segmentPt1.y) * (segmentPt2.y - segmentPt1.y);
    // si le point est "en dehors" du segment ,on prend la distance X P1
    if (p < 0)
    {
      return null;
    }

    // le produit scalaire/norme de B
    p = p / ((segmentPt2.x - segmentPt1.x) * (segmentPt2.x - segmentPt1.x) + (segmentPt2.y - segmentPt1.y) * (segmentPt2.y - segmentPt1.y));
    // si p>1 p est en dehors du segment
    if (p > 1)
    {
      return null;
    }
    
    return new Coordinate((p * (segmentPt2.x - segmentPt1.x)) + segmentPt1.x, (p * (segmentPt2.y - segmentPt1.y)) + segmentPt1.y);
  }

  public boolean canProject()
  {
    return this.canProject;
  }
  
  public int getSegmentOfProjectionIdx()
  {
    return this.segmentOfProjectionIdx;
  }
  
  public Coordinate getProjectedPoint()
  {
    return this.projectedPoint;
  }
  
  public boolean isOnSegmentPt()
  {
    return this.isOnSegmentPt;
  }
}
