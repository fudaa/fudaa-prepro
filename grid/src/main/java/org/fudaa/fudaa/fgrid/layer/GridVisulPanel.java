package org.fudaa.fudaa.fgrid.layer;

import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;

/**
 * TODO a voir pour la traduction.
 * <p lang="fr">
 * Le visupanel contenant toutes les donn�es. Le contexte {@link #getGeometryContext()} permet de recuperer le context
 * et de naviguer entre les diff�rents calques.{@linkplain #getGroupGIS()} renvoie le groupe des calques altim�triques.
 * </p>
 * 
 * @author deniger
 */
@SuppressWarnings("serial")
public class GridVisulPanel extends FSigVisuPanel {

  final ContextMainGeometryModule ctx;

  /**
   * @return the ctx used for the geometry module
   */
  public ContextMainGeometryModule getGeometryContext() {
    return ctx;
  }

  /**
   * @param ctx le context contenant le tout et utilis�e pour construire les calques.
   */
  public GridVisulPanel(ContextMainGeometryModule ctx) {
    super(ctx.getMainGroup(), ctx.getVisuController());
    this.ctx = ctx;
    ctx.install(this);
  }
  
  @Override
  public void setActive(boolean active) {}

  /**
   * @return altimetric group
   */
  @Override
  public FSigLayerGroup getGroupGIS() {
    return ctx.getGroupAltimetry();
  }

}
