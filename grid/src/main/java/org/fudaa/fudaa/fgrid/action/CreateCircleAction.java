package org.fudaa.fudaa.fgrid.action;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.fgrid.layer.ContextGeometricLayersImpl;
import org.fudaa.fudaa.fgrid.model.GeometricPointModel;
import org.fudaa.fudaa.fgrid.model.MultiGroupPointFindResult;
import org.fudaa.fudaa.fgrid.model.PointFindResult;

import org.locationtech.jts.geom.Coordinate;

@SuppressWarnings("serial")
public class CreateCircleAction extends EbliActionSimple
{
  final ContextGeometricLayersImpl context;
  final CtuluCommandContainer cmdContainer;
  
  public CreateCircleAction(final ContextGeometricLayersImpl context, CtuluCommandContainer cmdContainer)
  {
    super(Messages.getString("createCircleAction.actionText"), EbliResource.EBLI.getToolIcon("draw-add-pt"), "CREATE_CIRCLE"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    this.context = context;
    this.cmdContainer = cmdContainer;
  }

  @Override
  public void actionPerformed(final ActionEvent _e)
  {
    BCalque layer = this.context.getScene().getCalqueActif();
    
    if (!(layer instanceof ZCalqueLigneBriseeEditable))
    {
      JOptionPane.showMessageDialog(null, Messages.getString("createCircleAction.layerErrorText"), Messages.getString("createCircleAction.layerErrorTitle"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
      
      return;
    }
    
    Coordinate center = new Coordinate();

    MultiGroupPointFindResult result = this.context.getSelectionInPointLayers();
    
    if (result.getNbObjects() == 1)
    {
      PointFindResult pointFound = result.getPointFindResult(0);
      
      GeometricPointModel model = (GeometricPointModel)pointFound.layer.getModelEditable();
      
      center.x = model.getX(pointFound.idx);
      center.y = model.getY(pointFound.idx);
    }
    
    this.doAction((ZCalqueLigneBriseeEditable)layer, center);
  }

  private void doAction(ZCalqueLigneBriseeEditable layer, Coordinate center)
  {
    GISPolygone polygone = CircleCreator.createCircleWithDialog(center);
    
    if (polygone != null)
    {
      layer.modeleDonnees().addGeometry(polygone, this.cmdContainer, this.context.getUi(), null);
    }
  }
}
