package org.fudaa.fudaa.fgrid.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.fgrid.layer.ContextGeometricLayersImpl;
import org.fudaa.fudaa.fgrid.model.LineFindResult;
import org.fudaa.fudaa.fgrid.model.MultiGroupLineFindResult;
import org.fudaa.fudaa.fgrid.model.SeaWall;

@SuppressWarnings("serial")
public class CreateSeaWallAction extends EbliActionSimple
{
  final ContextGeometricLayersImpl context;
  final CtuluCommandContainer cmdContainer;
  
  public CreateSeaWallAction(final ContextGeometricLayersImpl context, CtuluCommandContainer cmdContainer)
  {
    super(Messages.getString("createSeaWallAction.actionText"), EbliResource.EBLI.getToolIcon("draw-add-pt"), "CREATE_SEA_WALL"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    this.context = context;
    this.cmdContainer = cmdContainer;
  }

  @Override
  public void actionPerformed(final ActionEvent _e)
  {
    MultiGroupLineFindResult lines = this.context.getSelectionInLineLayers();
    
    if (lines.isEmpty() || (lines.getNbObjects() != 1))
    {
      return;
    }
    
    LineFindResult line = lines.getLineFindResult(0);
    SeaWall seaWall = SeaWallCreator.createSeaWall(line, 20, 15);

    ZCalqueLigneBriseeEditable layer = line.layer;
    
    if (seaWall != null)
    {
      layer.modeleDonnees().addGeometry(seaWall.getRidge(), this.cmdContainer, this.context.getUi(), null);
      layer.modeleDonnees().addGeometry(seaWall.getFoot1(), this.cmdContainer, this.context.getUi(), null);
      layer.modeleDonnees().addGeometry(seaWall.getFoot2(), this.cmdContainer, this.context.getUi(), null);
    }

  }
}
