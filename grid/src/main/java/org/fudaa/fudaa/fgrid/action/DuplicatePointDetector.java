package org.fudaa.fudaa.fgrid.action;

import java.util.List;

import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.calque.edition.ZModelePointEditableInterface;
import org.fudaa.fudaa.fgrid.model.MultiGroupPointFindResult;

import org.locationtech.jts.geom.Coordinate;

public class DuplicatePointDetector
{
  private List<ZCalquePointEditable> layers;
  private MultiGroupPointFindResult result;
  private double eps;
  
  public DuplicatePointDetector(List<ZCalquePointEditable> layers)
  {
    super();
    this.layers = layers;
    this.result = new MultiGroupPointFindResult();
    // TODO Gerer eps.
    this.eps = 0.1d;
  }
  
  public void process()
  {
    for (int i = 0; i < this.layers.size(); i++)
    {
      ZModelePointEditableInterface model1 = (ZModelePointEditableInterface)this.layers.get(i).getModelEditable();
      
      for (int j = 0; j < model1.getNombre(); j++)
      {
        if (this.result.containsObject(this.layers.get(i), j))
        {
          continue;
        }

        Coordinate pts1 = new Coordinate(model1.getX(j), model1.getY(j));
        
        for (int k = i; k < this.layers.size(); k++)
        {
          ZModelePointEditableInterface model2 = (ZModelePointEditableInterface)this.layers.get(k).getModelEditable();

          for (int l = ((i == k) ? j + 1 : 0); l < model2.getNombre(); l++)
          {
            Coordinate pts2 = new Coordinate(model2.getX(l), model2.getY(l));
            
            if (pts1.distance(pts2) <= eps)
            {
              this.result.addObject(this.layers.get(k), l);
            }
          }
        }
      }
    }
  }

  public MultiGroupPointFindResult getResult()
  {
    return this.result;
  }
}
