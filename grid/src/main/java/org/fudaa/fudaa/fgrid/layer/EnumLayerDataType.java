package org.fudaa.fudaa.fgrid.layer;

/**
 * Type des diff�rents calques utilis�es. A completer par la suite.
 * @author deniger
 *
 */
public enum EnumLayerDataType {

  GEOMETRIC_POINT, GEOMETRIC_POINT_DUR, GEOMETRIC_LINE, GEOMETRIC_LINE_DUR

}
