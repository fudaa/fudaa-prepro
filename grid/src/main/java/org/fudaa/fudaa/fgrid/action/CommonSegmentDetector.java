package org.fudaa.fudaa.fgrid.action;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.ctulu.gis.GISLigneBrisee;
import org.fudaa.ctulu.gis.GISSegment;
import org.fudaa.fudaa.fgrid.model.GeometricLigneBriseeModel;

public class CommonSegmentDetector
{
  public static class CommonSegment
  {
    private long line1;
    private long line2;
    private GISSegment segment;
    
    public CommonSegment(long line1, long line2, GISSegment segment)
    {
      super();
      this.line1 = line1;
      this.line2 = line2;
      this.segment = segment;
    }

    public long getLine1()
    {
      return line1;
    }

    public long getLine2()
    {
      return line2;
    }

    public GISSegment getSegment()
    {
      return segment;
    }
  }
  
  private GeometricLigneBriseeModel[] models;
  private CommonSegment[] commonSegments;
  
  public CommonSegmentDetector(GeometricLigneBriseeModel[] models)
  {
    super();
    this.models = models;
    this.commonSegments = new CommonSegment[0];
  }
  
  public void process()
  {
    List<CommonSegment> segments = new ArrayList<CommonSegment>();
    
    for (int i = 0; i < this.models.length; i++)
    {
      for (int j = 0; j < this.models[i].getNombre(); j++)
      {
        for (int k = i; k < this.models.length; k++)
        {
          for (int l = ((i == k) ? j + 1 : 0); l < this.models[k].getNombre(); l++)
          {
            segments.addAll(this.getCommonSegment(this.models[i], j, this.models[k], l));
          }
        }
      }
    }
    
    this.commonSegments = segments.toArray(new CommonSegment[0]);
  }
  
  private List<CommonSegment> getCommonSegment(GeometricLigneBriseeModel model1, int lineIdx1, GeometricLigneBriseeModel model2, int lineIdx2)
  {
    List<CommonSegment> segments = new ArrayList<CommonSegment>();
    
    GISLigneBrisee line1 = (GISLigneBrisee)model1.getGeomData().getGeometry(lineIdx1);
    GISLigneBrisee line2 = (GISLigneBrisee)model2.getGeomData().getGeometry(lineIdx2);
    
    for (int i = 0; i < line1.getNbSegment(); i++)
    {
      GISSegment segment = line1.getSegment(i);
      
      for (int j = 0; j < line2.getNbSegment(); j++)
      {
        if (segment.equals(line2.getSegment(j)))
        {
          segments.add(new CommonSegment(model1.getLineId(lineIdx1), model2.getLineId(lineIdx2), segment));
        }
      }
    }
        
    return segments;
  }

  public CommonSegment[] getCommonSegments()
  {
    return commonSegments;
  }
}
