package org.fudaa.fudaa.fgrid.model;

import gnu.trove.TLongIntHashMap;

import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModelLongInterface;
import org.fudaa.ctulu.gis.GISCollection;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneListener;
import org.fudaa.ctulu.gis.GISZoneListenerDispatcher;

/**
 * A default indexer for ID. Used to give quickly the position from an ID.
 * 
 * @author deniger
 */
public class GeometricDefaultIndexer implements GISZoneListener {

  /**
   * Mapping between node id and its positions in the collection.
   */
  final TLongIntHashMap idForPosition = new TLongIntHashMap();

  boolean mustUpdate = true;
  final GISZoneCollection collection;

  /**
   * @param collection
   */
  public GeometricDefaultIndexer(GISZoneCollection collection) {
    super();
    this.collection = collection;
    ((GISZoneListenerDispatcher) collection.getListener()).addListener(this);
  }

  public void attributeChanged(GISCollection src, GISAttributeInterface att) {
    if (att == AttributeConstants.ID) {
      mustUpdate = true;
    }
  }
  
  


  /**
   * Empty method that can be overload: called by the index method.
   */
  protected void postIndex() {

  }

  public void index() {
    if (mustUpdate) {
      mustUpdate = false;
      idForPosition.clear();
      int nbGeom = collection.getNbGeometries();
      GISAttributeModelLongInterface ids = (GISAttributeModelLongInterface) collection.getDataModel(collection
          .getIndiceOf(AttributeConstants.ID));
      for (int i = 0; i < nbGeom; i++) {
        idForPosition.put(ids.getValue(i), i);
      }
      postIndex();
    }
  }

  public int getPositionOfId(long id) {
    index();
    if (idForPosition.contains(id)) { return idForPosition.get(id); }
    return -1;
  }

  /**
   * @param objectId l'ID of the point
   * @return if contains by this collection.
   */
  public boolean isIdUsed(long objectId) {
    index();
    return idForPosition.contains(objectId);
  }

  @Override
  public void attributeAction(Object source, int indexAtt, GISAttributeInterface att, int action) {}

  @Override
  public void attributeValueChangeAction(Object source, int indexAtt, GISAttributeInterface att, int indexObj,
                                         Object newValue) {}

  @Override
  public void objectAction(Object source, int indexObj, Object obj, int action) {}

}
