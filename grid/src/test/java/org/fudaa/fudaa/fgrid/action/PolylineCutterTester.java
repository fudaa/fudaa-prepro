package org.fudaa.fudaa.fgrid.action;

import gnu.trove.TIntHashSet;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLigneBrisee;
import org.fudaa.ctulu.gis.GISLigneBriseeComparator;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.fudaa.fgrid.action.PolylineCutter.PolylineCutterResult;
import org.fudaa.fudaa.fgrid.model.LineFind;
import org.fudaa.fudaa.fgrid.model.MultiGroupLineFindResult;

import org.locationtech.jts.geom.Coordinate;

public class PolylineCutterTester extends TestCase
{
  private static final GISLigneBriseeComparator comparator = new GISLigneBriseeComparator();

  public void testPoylineCutter()
  {
    ContextForTesting context = this.createContext1();
    ZCalqueLigneBriseeEditable layer = context.getLineLayers().iterator().next();
    
    MultiGroupLineFindResult lines = null;
    PolylineCutter cutter = new PolylineCutter(lines, null);
    cutter.process();
    
    assertTrue("Aucun ligne ne devrait �tre s�lectionn�e", cutter.getResult() == PolylineCutterResult.NOT_LINE_SELECTED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine()}, this.getAllLines(layer)));

    lines = new MultiGroupLineFindResult();
    cutter = new PolylineCutter(lines, null);
    cutter.process();
    
    assertTrue("Aucun ligne ne devrait �tre s�lectionn�e", cutter.getResult() == PolylineCutterResult.NOT_LINE_SELECTED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine()}, this.getAllLines(layer)));

    lines.addObject(layer, new LineFind(0));
    lines.addObject(layer, new LineFind(1));

    cutter = new PolylineCutter(lines, null);
    cutter.process();
    
    assertTrue("Trop des lignes devrait �tre s�lectionn�es", cutter.getResult() == PolylineCutterResult.TOO_MANY_LINE_SELECTED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine()}, this.getAllLines(layer)));

    lines = new MultiGroupLineFindResult();
    lines.addObject(layer, new LineFind(0));

    cutter = new PolylineCutter(lines, null);
    cutter.process();
    
    assertTrue("Aucun point ou segment ne devrait �tre s�lectionn�", cutter.getResult() == PolylineCutterResult.NOT_POINT_OR_SEGMENT_SELECTED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine()}, this.getAllLines(layer)));

    lines = new MultiGroupLineFindResult();
    lines.addObject(layer, new LineFind(0, 1, true));
    lines.addObject(layer, new LineFind(0, 1, false));

    cutter = new PolylineCutter(lines, null);
    cutter.process();
    
    assertTrue("Des points et des segments devraient �tre s�lectionn�s", cutter.getResult() == PolylineCutterResult.POINT_AND_SEGMENT_SELECTED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine()}, this.getAllLines(layer)));

    lines = new MultiGroupLineFindResult();
    lines.addObject(layer, new LineFind(0, 1, true));
    lines.addObject(layer, new LineFind(0, 2, true));
    lines.addObject(layer, new LineFind(0, 3, true));

    cutter = new PolylineCutter(lines, null);
    cutter.process();
    
    assertTrue("Trop de points devraient �tre s�lectionn�s", cutter.getResult() == PolylineCutterResult.TOO_MANY_POINT_SELECTED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine()}, this.getAllLines(layer)));

    lines = new MultiGroupLineFindResult();
    lines.addObject(layer, new LineFind(0, 1, false));
    lines.addObject(layer, new LineFind(0, 2, false));

    cutter = new PolylineCutter(lines, null);
    cutter.process();
    
    assertTrue("Trop de segments devraient �tre s�lectionn�s", cutter.getResult() == PolylineCutterResult.TOO_MANY_SEGMENT_SELECTED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine()}, this.getAllLines(layer)));

    lines = new MultiGroupLineFindResult();
    lines.addObject(layer, new LineFind(0, 0, true));

    cutter = new PolylineCutter(lines, null);
    cutter.process();
    
    assertTrue("La coupure devrait r�ussir", cutter.getResult() == PolylineCutterResult.SUCCEEDED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine()}, this.getAllLines(layer)));

    context = this.createContext1();
    layer = context.getLineLayers().iterator().next();
    lines = new MultiGroupLineFindResult();
    lines.addObject(layer, new LineFind(0, 3, true));

    cutter = new PolylineCutter(lines, null);
    cutter.process();
    
    assertTrue("La coupure devrait r�ussir", cutter.getResult() == PolylineCutterResult.SUCCEEDED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine(0, 4), this.createLine(3, 5)}, this.getAllLines(layer)));

    context = this.createContext1();
    layer = context.getLineLayers().iterator().next();
    lines = new MultiGroupLineFindResult();
    lines.addObject(layer, new LineFind(0, 3, true));
    lines.addObject(layer, new LineFind(0, 4, true));

    cutter = new PolylineCutter(lines, null);
    cutter.process();
    
    assertTrue("La coupure devrait r�ussir", cutter.getResult() == PolylineCutterResult.SUCCEEDED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine(0, 4), this.createLine(4, 4)}, this.getAllLines(layer)));

    context = this.createContext1();
    layer = context.getLineLayers().iterator().next();
    lines = new MultiGroupLineFindResult();
    lines.addObject(layer, new LineFind(0, 3, true));
    lines.addObject(layer, new LineFind(0, 5, true));

    cutter = new PolylineCutter(lines, null);
    cutter.process();
    
    assertTrue("La coupure devrait r�ussir", cutter.getResult() == PolylineCutterResult.SUCCEEDED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine(0, 4), this.createLine(5, 3)}, this.getAllLines(layer)));

    //TODO Ce test ne devrait-il pas marcher?
/*    context = this.createContext1();
    layer = context.getLineLayers().iterator().next();
    lines = new MultiGroupLineFindResult();
    lines.addObject(layer, new LineFind(0, 0, false));

    cutter = new PolylineCutter(lines, null);
    cutter.process();
    
    assertTrue("La coupure devrait r�ussir", cutter.getResult() == PolylineCutterResult.SUCCEEDED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine(1, 7)}, this.getAllLines(layer)));
*/
    //TODO Ce test DOIT marcher.
/*    context = this.createContext1();
    layer = context.getLineLayers().iterator().next();
    lines = new MultiGroupLineFindResult();
    lines.addObject(layer, new LineFind(0, 3, false));

    cutter = new PolylineCutter(lines, null);
    cutter.process();
    
    assertTrue("La coupure devrait r�ussir", cutter.getResult() == PolylineCutterResult.SUCCEEDED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine(0, 4), this.createLine(4, 4)}, this.getAllLines(layer)));
*/
    context = this.createContext2();
    layer = context.getLineLayers().iterator().next();
    lines = new MultiGroupLineFindResult();
    lines.addObject(layer, new LineFind(0, 3, false));

    cutter = new PolylineCutter(lines, null);
    cutter.process();
    
    assertTrue("La ligne s�lectionn�e ne devrait pas �tre une polyligne", cutter.getResult() == PolylineCutterResult.CAN_CUT_ONLY_POLYLINE);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createPolygone()}, this.getAllLines(layer)));
 }
  
  private Coordinate[] createCoordinates(int idxFirst, int nb)
  {
    Coordinate[] originalCoords = new Coordinate[]{new Coordinate(3, 3), new Coordinate(8, 7), new Coordinate(13, 3), new Coordinate(18, 7), new Coordinate(24, 12), new Coordinate(24, 6), new Coordinate(29, 9), new Coordinate(30, 4), new Coordinate(3, 3)};
    
    if ((idxFirst < 0) || (nb < 2) || ((idxFirst + nb) > 8))
    {
      return originalCoords;
    }
    
    Coordinate[] coords = new Coordinate[nb];
    
    for (int i = 0; i < nb; i++)
    {
      coords[i] = originalCoords[i + idxFirst];
    }
    
    return coords;
  }
  
  private GISPolyligne createLine(int idxFirst, int nb)
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.createCoordinates(idxFirst, nb));
  }
  
  private GISPolyligne createLine()
  {
    return this.createLine(0, 8);
  }
  
  private GISPolygone createPolygone()
  {
    return (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(this.createCoordinates(-1, -1));
  }

  private ContextForTesting createContext1()
  {
    ContextForTesting context = new ContextForTesting();
    context.createLinesLayer().modeleDonnees().addGeometry(this.createLine(), null, null, null);
    
    return context;
  }

  private ContextForTesting createContext2()
  {
    ContextForTesting context = new ContextForTesting();
    context.createLinesLayer().modeleDonnees().addGeometry(this.createPolygone(), null, null, null);
    
    return context;
  }

  private boolean isSameLines(GISLigneBrisee[] lines1, GISLigneBrisee[] lines2)
  {
    if (lines1.length != lines2.length)
    {
      return false;
    }
    
    TIntHashSet findedLines = new TIntHashSet();
    
    for (int i = 0; i < lines1.length; i++)
    {
      for (int j = 0; j < lines2.length; j++)
      {
        if (!findedLines.contains(j))
        {
          if (comparator.compare(lines1[i], lines2[j]) == 0)
          {
            findedLines.add(j);
            
            break;
          }
        }
      }
    }
    
    return findedLines.size() == lines1.length;
  }
  
  private GISLigneBrisee[] getAllLines(ZCalqueLigneBriseeEditable layer)
  {
    List<GISLigneBrisee> listLines = new ArrayList<GISLigneBrisee>();
    
    ZModeleLigneBriseeEditable model = layer.modeleDonnees();
    int nbLines = model.getNombre();
    
    for (int j = 0; j < nbLines; j++)
    {
      listLines.add((GISLigneBrisee)model.getGeomData().getGeometry(j));
    }
    
    GISLigneBrisee[] lines = new GISLigneBrisee[0];

    lines = listLines.toArray(lines);
    
    return lines;
  }
}
