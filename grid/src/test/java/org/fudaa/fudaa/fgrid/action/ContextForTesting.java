package org.fudaa.fudaa.fgrid.action;

import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gis.GISZoneListenerDispatcher;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.fudaa.fgrid.layer.ContextGeometricLayersImpl;
import org.fudaa.fudaa.fgrid.layer.RepereContainerImpl;
import org.fudaa.fudaa.fgrid.model.AttributeConstants;
import org.fudaa.fudaa.fgrid.model.GeometricLigneBriseeModel;
import org.fudaa.fudaa.fgrid.model.GeometricPointModel;
import org.fudaa.fudaa.sig.layer.FSigLayerLineEditable;
import org.fudaa.fudaa.sig.layer.FSigLayerPointEditable;

class ContextForTesting extends ContextGeometricLayersImpl
{
  public ContextForTesting()
  {
    super(null, new RepereContainerImpl(), null);
    final GISZoneCollectionPoint createCollectionPoint = this.createCollectionPoint();
    
    this.register(new FSigLayerPointEditable(new GeometricPointModel(createCollectionPoint, this), null), true);
  }

  public ZCalqueLigneBriseeEditable createLinesLayer()
  {
    ZCalqueLigneBriseeEditable layer = new FSigLayerLineEditable(new GeometricLigneBriseeModel(this, this.createCollectionLine()), null);
    
    this.register(layer);

    return layer;
  }

  public FSigLayerPointEditable createPointsLayer()
  {
    FSigLayerPointEditable layer = new FSigLayerPointEditable(new GeometricPointModel(this.createCollectionPoint(), this), null);
    
    this.register(layer);
    
    return layer;
  }
  
  private GISZoneCollectionLigneBrisee createCollectionLine()
  {
    GISZoneCollectionLigneBrisee collection = new GISZoneCollectionLigneBrisee(new GISZoneListenerDispatcher());
    collection.setAttributes(new GISAttribute[] { AttributeConstants.ID, AttributeConstants.POINT_ID, AttributeConstants.GRID_PRECISION }, null);
    
    return collection;
  }

  private GISZoneCollectionPoint createCollectionPoint()
  {
    GISZoneCollectionPoint collection = new GISZoneCollectionPoint(new GISZoneListenerDispatcher());
    collection.setAttributes(new GISAttribute[] { AttributeConstants.ID, GISAttributeConstants.BATHY, AttributeConstants.GRID_PRECISION }, null);
    
    return collection;
  }
}
