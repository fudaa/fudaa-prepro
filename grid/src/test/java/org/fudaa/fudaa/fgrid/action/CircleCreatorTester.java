package org.fudaa.fudaa.fgrid.action;

import junit.framework.TestCase;

import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLigneBriseeComparator;
import org.fudaa.ctulu.gis.GISPolygone;

import org.locationtech.jts.geom.Coordinate;

public class CircleCreatorTester extends TestCase
{
  public void testCircleCreator()
  {
    Coordinate center = new Coordinate(23, -13);
    double rayon = 23.0d;
    double startDegree = 13.0d;
    int nbPts = 27;
    
    GISPolygone circle = CircleCreator.createCircle(center, rayon, startDegree, nbPts);
    GISPolygone orignalCircle = this.createCircle(center, rayon, startDegree, nbPts);
    
    assertTrue("Les cercles sont différents", (new GISLigneBriseeComparator().compare(orignalCircle, circle)) == 0);
  }
  
  private GISPolygone createCircle(Coordinate center, double rayon, double startDegree, int nbPts)
  {
    Coordinate[] polygone = new Coordinate[]{new Coordinate(45.4105,-7.8261),
                                             new Coordinate(43.6133,-2.7974),
                                             new Coordinate(40.7047,1.6814),
                                             new Coordinate(36.8417,5.3686),
                                             new Coordinate(32.2325,8.0656),
                                             new Coordinate(27.1256,9.6270),
                                             new Coordinate(21.7963,9.9685),
                                             new Coordinate(16.5318,9.0718),
                                             new Coordinate(11.6161,6.9852),
                                             new Coordinate(7.3140,3.8211),
                                             new Coordinate(3.8576,-0.2497),
                                             new Coordinate(1.4332,-5.0079),
                                             new Coordinate(0.1714,-10.1970),
                                             new Coordinate(0.1404,-15.5372),
                                             new Coordinate(1.3417,-20.7406),
                                             new Coordinate(3.7106,-25.5267),
                                             new Coordinate(7.1194,-29.6375),
                                             new Coordinate(11.3843,-32.8513),
                                             new Coordinate(16.2755,-34.9950),
                                             new Coordinate(21.5291,-35.9529),
                                             new Coordinate(26.8621,-35.6734),
                                             new Coordinate(31.9868,-34.1716),
                                             new Coordinate(36.6271,-31.5284),
                                             new Coordinate(40.5327,-27.8864),
                                             new Coordinate(43.4932,-23.4418),
                                             new Coordinate(45.3488,-18.4343),
                                             new Coordinate(45.9996,-13.1338),
                                             new Coordinate(45.4105,-7.8261)};
      
    return (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(polygone);
  }
}
