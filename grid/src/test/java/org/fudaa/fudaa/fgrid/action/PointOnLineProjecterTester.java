package org.fudaa.fudaa.fgrid.action;

import junit.framework.TestCase;

import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLigneBrisee;
import org.fudaa.ctulu.gis.GISLigneBriseeComparator;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.fgrid.action.PointOnLineProjecter.PointOnLineProjecterResult;
import org.fudaa.fudaa.fgrid.model.LineFindResult;
import org.fudaa.fudaa.fgrid.model.PointFindResult;

import org.locationtech.jts.geom.Coordinate;

public class PointOnLineProjecterTester extends TestCase
{
  private static final GISLigneBriseeComparator comparator = new GISLigneBriseeComparator();
  
  public void testPointOnLineProjecter()
  {
    ContextForTesting context = this.createContext1();
    LineFindResult line = null;
    PointFindResult point = null;
    
    PointOnLineProjecter projecter = new PointOnLineProjecter(line, point, null);
    projecter.process();
    
    assertTrue("La ligne devrait �tre incorrect", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_LINE);

    line = new LineFindResult();
    
    projecter = new PointOnLineProjecter(line, point, null);
    projecter.process();
    
    assertTrue("La ligne devrait �tre incorrect", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_LINE);

    line.layer = context.getLineLayers().iterator().next();
    line.posOfLine = 0;

    projecter = new PointOnLineProjecter(line, point, null);
    projecter.process();
    
    assertTrue("Le point devrait �tre incorrect", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_POINT);

    point = new PointFindResult();

    projecter = new PointOnLineProjecter(line, point, null);    
    projecter.process();
    
    assertTrue("Le point devrait �tre incorrect", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_POINT);
    
    point.layer = context.getSupportPointLayer();
    point.idx = 3;

    projecter = new PointOnLineProjecter(line, point, null);
    projecter.process();
    
    assertFalse("La ligne devrait �tre correct", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_LINE);
    assertFalse("Le point devrait �tre correct", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_POINT);
    assertFalse("La projection devrait �tre possible", projecter.getResult() == PointOnLineProjecterResult.CANT_DO_PROJECTION);
    assertFalse("Le point projet� ne devrait pas d�j� exister", projecter.getResult() == PointOnLineProjecterResult.PROJECTION_POINT_ALREADY_EXIST);
    assertTrue("La projection devrait �tre r�ussie", projecter.getResult() == PointOnLineProjecterResult.SUCCEEDED);
    assertEquals("Il devrait y avoir 4 points", 4, context.getSupportModelPoint().getNombre());
    assertTrue("Les lignes devrait �tre �gales", comparator.compare(this.createLine(new Coordinate(12, 15), 2), this.getLine(line)) == 0);

    context = this.createContext1();
    line.layer = context.getLineLayers().iterator().next();
    line.posOfLine = 0;
    line.idxOfSegment = 0;
    point.layer = context.getSupportPointLayer();
    point.idx = 3;

    projecter = new PointOnLineProjecter(line, point, null);
    projecter.process();
    
    assertFalse("La ligne devrait �tre correct", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_LINE);
    assertFalse("Le point devrait �tre correct", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_POINT);
    assertFalse("La projection devrait �tre possible", projecter.getResult() == PointOnLineProjecterResult.CANT_DO_PROJECTION);
    assertFalse("Le point projet� ne devrait pas d�j� exister", projecter.getResult() == PointOnLineProjecterResult.PROJECTION_POINT_ALREADY_EXIST);
    assertTrue("La projection devrait �tre r�ussie", projecter.getResult() == PointOnLineProjecterResult.SUCCEEDED);
    assertEquals("Il devrait y avoir 4 points", 4, context.getSupportModelPoint().getNombre());
    assertTrue("Les lignes devrait �tre �gales", comparator.compare(this.createLine(new Coordinate(10, 7), 1), this.getLine(line)) == 0);

    context = this.createContext2();
    line.layer = context.getLineLayers().iterator().next();
    line.posOfLine = 0;
    line.idxOfSegment = -1;
    point.layer = context.getSupportPointLayer();
    point.idx = 3;

    projecter = new PointOnLineProjecter(line, point, null);
    projecter.process();
    
    assertFalse("La ligne devrait �tre correct", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_LINE);
    assertFalse("Le point devrait �tre correct", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_POINT);
    assertTrue("La projection ne devrait pas �tre possible", projecter.getResult() == PointOnLineProjecterResult.CANT_DO_PROJECTION);
    assertFalse("Le point projet� ne devrait pas d�j� exister", projecter.getResult() == PointOnLineProjecterResult.PROJECTION_POINT_ALREADY_EXIST);
    assertFalse("La projection ne devrait pas �tre r�ussie", projecter.getResult() == PointOnLineProjecterResult.SUCCEEDED);
    assertEquals("Il devrait y avoir 4 points", 4, context.getSupportModelPoint().getNombre());
    assertTrue("Les lignes devrait �tre �gales", comparator.compare(this.createLine(), this.getLine(line)) == 0);

    context = this.createContext3();
    line.layer = context.getLineLayers().iterator().next();
    line.posOfLine = 0;
    line.idxOfSegment = -1;
    point.layer = context.getSupportPointLayer();
    point.idx = 3;

    projecter = new PointOnLineProjecter(line, point, null);
    projecter.process();
    
    assertFalse("La ligne devrait �tre correct", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_LINE);
    assertFalse("Le point devrait �tre correct", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_POINT);
    assertFalse("La projection devrait �tre possible", projecter.getResult() == PointOnLineProjecterResult.CANT_DO_PROJECTION);
    assertFalse("Le point projet� ne devrait pas d�j� exister", projecter.getResult() == PointOnLineProjecterResult.PROJECTION_POINT_ALREADY_EXIST);
    assertTrue("La projection devrait �tre r�ussie", projecter.getResult() == PointOnLineProjecterResult.SUCCEEDED);
    assertEquals("Il devrait y avoir 4 points", 4, context.getSupportModelPoint().getNombre());
    assertTrue("Les lignes devrait �tre �gales", comparator.compare(this.createLine(new Coordinate(8, 5), 1), this.getLine(line)) == 0);

    context = this.createContext3();
    line.layer = context.getLineLayers().iterator().next();
    line.posOfLine = 0;
    line.idxOfSegment = 1;
    point.layer = context.getSupportPointLayer();
    point.idx = 3;

    projecter = new PointOnLineProjecter(line, point, null);
    projecter.process();
    
    assertFalse("La ligne devrait �tre correct", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_LINE);
    assertFalse("Le point devrait �tre correct", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_POINT);
    assertTrue("La projection ne devrait pas �tre possible", projecter.getResult() == PointOnLineProjecterResult.CANT_DO_PROJECTION);
    assertFalse("Le point projet� ne devrait pas d�j� exister", projecter.getResult() == PointOnLineProjecterResult.PROJECTION_POINT_ALREADY_EXIST);
    assertFalse("La projection ne devrait pas �tre r�ussie", projecter.getResult() == PointOnLineProjecterResult.SUCCEEDED);
    assertEquals("Il devrait y avoir 4 points", 4, context.getSupportModelPoint().getNombre());
    assertTrue("Les lignes devrait �tre �gales", comparator.compare(this.createLine(), this.getLine(line)) == 0);

    context = this.createContext4();
    line.layer = context.getLineLayers().iterator().next();
    line.posOfLine = 0;
    line.idxOfSegment = -1;
    point.layer = context.getSupportPointLayer();
    point.idx = 3;

    projecter = new PointOnLineProjecter(line, point, null);
    projecter.process();
    
    assertFalse("La ligne devrait �tre correct", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_LINE);
    assertFalse("Le point devrait �tre correct", projecter.getResult() == PointOnLineProjecterResult.INCORRECT_POINT);
    assertFalse("La projection devrait �tre possible", projecter.getResult() == PointOnLineProjecterResult.CANT_DO_PROJECTION);
    assertTrue("Le point projet� devrait d�j� exister", projecter.getResult() == PointOnLineProjecterResult.PROJECTION_POINT_ALREADY_EXIST);
    assertFalse("La projection ne devrait pas �tre r�ussie", projecter.getResult() == PointOnLineProjecterResult.SUCCEEDED);
    assertEquals("Il devrait y avoir 4 points", 4, context.getSupportModelPoint().getNombre());
    assertTrue("Les lignes devrait �tre �gales", comparator.compare(this.createLine(), this.getLine(line)) == 0);
}
  
  private GISLigneBrisee getLine(LineFindResult line)
  {
    return (GISLigneBrisee)line.layer.modeleDonnees().getGeomData().getGeometry(line.posOfLine);
  }
  
  private GISPolyligne createLine()
  {
    return this.createLine(null, -1);
  }

  private GISPolyligne createLine(Coordinate coord, int idx)
  {
    Coordinate[] originalCoords = new Coordinate[]{new Coordinate(6, 3), new Coordinate(12, 9), new Coordinate(12, 20)};
    
    if ((coord == null) || (idx < 0) || (idx > 3))
    {
      return GISGeometryFactory.INSTANCE.createLineString(originalCoords);
    }
    
    Coordinate[] coords = new Coordinate[4];
    int originalIdx = 0;
    
    for (int i = 0; i < coords.length; i++)
    {
      if (i == idx)
      {
        coords[i] = coord;
      }
      else
      {
        coords[i] = originalCoords[originalIdx++];
      }
    }

    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords);
  }
  
  private ContextForTesting createContext1()
  {
    ContextForTesting context = new ContextForTesting();
    context.createLinesLayer().modeleDonnees().addGeometry(this.createLine(), null, null, null);
    context.getSupportModelPoint().addPoint(new GrPoint(new Coordinate(2, 15)), null, null);
    
    return context;
  }
  
  private ContextForTesting createContext2()
  {
    ContextForTesting context = new ContextForTesting();
    context.createLinesLayer().modeleDonnees().addGeometry(this.createLine(), null, null, null);
    context.getSupportModelPoint().addPoint(new GrPoint(new Coordinate(3, 1)), null, null);
    
    return context;
  }
  
  private ContextForTesting createContext3()
  {
    ContextForTesting context = new ContextForTesting();
    context.createLinesLayer().modeleDonnees().addGeometry(this.createLine(), null, null, null);
    context.getSupportModelPoint().addPoint(new GrPoint(new Coordinate(12, 1)), null, null);
    
    return context;
  }
  
  private ContextForTesting createContext4()
  {
    ContextForTesting context = new ContextForTesting();
    context.createLinesLayer().modeleDonnees().addGeometry(this.createLine(), null, null, null);
    context.getSupportModelPoint().addPoint(new GrPoint(new Coordinate(22, 9)), null, null);
    
    return context;
  }
}
