package org.fudaa.fudaa.fgrid.action;

import gnu.trove.TIntHashSet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import junit.framework.TestCase;

import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLigneBrisee;
import org.fudaa.ctulu.gis.GISLigneBriseeComparator;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.fudaa.fgrid.action.PolylineJoiner.PolylineJoinerResult;
import org.fudaa.fudaa.fgrid.model.LineFind;
import org.fudaa.fudaa.fgrid.model.MultiGroupLineFindResult;

import org.locationtech.jts.geom.Coordinate;

public class PolylineJoinerTester extends TestCase
{
  private static final GISLigneBriseeComparator comparator = new GISLigneBriseeComparator();

  public void testPolylineJoiner()
  {
    ContextForTesting context = this.createContext1();
    
    List<ZCalqueLigneBriseeEditable> layers = new ArrayList<ZCalqueLigneBriseeEditable>();
    Iterator<ZCalqueLigneBriseeEditable> iterator = context.getLineLayers().iterator();
    layers.add(iterator.next());
    layers.add(iterator.next());
    
    MultiGroupLineFindResult lines = null;

    PolylineJoiner joiner = new PolylineJoiner(lines, null);
    joiner.process();
    
    assertTrue("Aucun ligne ne devrait �tre s�lectionn�e", joiner.getResult() == PolylineJoinerResult.NOT_LINE_SELECTED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine1(), this.createLine2()}, this.getAllLines(layers)));

    lines = new MultiGroupLineFindResult();
    
    joiner = new PolylineJoiner(lines, null);
    joiner.process();
    
    assertTrue("Aucun ligne ne devrait �tre s�lectionn�e", joiner.getResult() == PolylineJoinerResult.NOT_LINE_SELECTED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine1(), this.createLine2()}, this.getAllLines(layers)));

    lines.addObject(layers.get(0), new LineFind(0, 3, true));
    lines.addObject(layers.get(1), new LineFind(0, 0, true));
    
    joiner = new PolylineJoiner(lines, null);
    joiner.process();
    
    assertTrue("Les lignes ne devraient pas �tre dans le m�me calque", joiner.getResult() == PolylineJoinerResult.LINES_MUST_BE_IN_SAME_LAYER);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine1(), this.createLine2()}, this.getAllLines(layers)));

    context = this.createContext2();
    
    layers = new ArrayList<ZCalqueLigneBriseeEditable>();
    layers.add(context.getLineLayers().iterator().next());
    
    lines = new MultiGroupLineFindResult();
    lines.addObject(layers.get(0), new LineFind(0, 3, true));
    lines.addObject(layers.get(0), new LineFind(1, 0, true));
    lines.addObject(layers.get(0), new LineFind(2, 0, true));

    joiner = new PolylineJoiner(lines, null);
    joiner.process();

    assertTrue("Trop de lignes devraient �tre s�lectionn�es", joiner.getResult() == PolylineJoinerResult.TOO_MANY_LINE_SELECTED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine1(), this.createLine2(), this.createPolygone1()}, this.getAllLines(layers)));

    lines = new MultiGroupLineFindResult();
    lines.addObject(layers.get(0), new LineFind(0, 3, true));

    joiner = new PolylineJoiner(lines, null);
    joiner.process();

    assertTrue("Pas assez de point devrait �tre s�lectionn�", joiner.getResult() == PolylineJoinerResult.NOT_ENOUGH_POINT_SELECTED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine1(), this.createLine2(), this.createPolygone1()}, this.getAllLines(layers)));

    lines = new MultiGroupLineFindResult();
    lines.addObject(layers.get(0), new LineFind(0, 3, true));
    lines.addObject(layers.get(0), new LineFind(1, 0, true));
    lines.addObject(layers.get(0), new LineFind(1, 3, true));

    joiner = new PolylineJoiner(lines, null);
    joiner.process();

    assertTrue("Trop de points devraient �tre s�lectionn�s", joiner.getResult() == PolylineJoinerResult.TOO_MANY_POINT_SELECTED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine1(), this.createLine2(), this.createPolygone1()}, this.getAllLines(layers)));

    lines = new MultiGroupLineFindResult();
    lines.addObject(layers.get(0), new LineFind(0, 3, true));
    lines.addObject(layers.get(0), new LineFind(1, 0, true));
    lines.addObject(layers.get(0), new LineFind(1, 1, false));

    joiner = new PolylineJoiner(lines, null);
    joiner.process();

    assertTrue("Un segment devrait �tre s�lectionn�", joiner.getResult() == PolylineJoinerResult.SEGMENT_MUST_NOT_BE_SELECTED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine1(), this.createLine2(), this.createPolygone1()}, this.getAllLines(layers)));

    lines = new MultiGroupLineFindResult();
    lines.addObject(layers.get(0), new LineFind(0, 3, true));
    lines.addObject(layers.get(0), new LineFind(1, 1, true));

    joiner = new PolylineJoiner(lines, null);
    joiner.process();

    assertTrue("Un point int�rieur devrait �tre s�lectionn�", joiner.getResult() == PolylineJoinerResult.ONLY_EXTREMITY_POINT_MUST_BE_SELECTED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine1(), this.createLine2(), this.createPolygone1()}, this.getAllLines(layers)));

    lines = new MultiGroupLineFindResult();
    lines.addObject(layers.get(0), new LineFind(0, 3, true));
    lines.addObject(layers.get(0), new LineFind(2, 0, true));

    joiner = new PolylineJoiner(lines, null);
    joiner.process();

    assertTrue("Un polygone devrait �tre s�lectionn�", joiner.getResult() == PolylineJoinerResult.CAN_JOIN_ONLY_POLYLINE);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine1(), this.createLine2(), this.createPolygone1()}, this.getAllLines(layers)));

    lines = new MultiGroupLineFindResult();
    lines.addObject(layers.get(0), new LineFind(0, 3, true));
    lines.addObject(layers.get(0), new LineFind(1, 0, true));

    joiner = new PolylineJoiner(lines, null);
    joiner.process();

    assertTrue("La jointure devrait r�ussir", joiner.getResult() == PolylineJoinerResult.SUCCEEDED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine3(), this.createPolygone1()}, this.getAllLines(layers)));

    context = this.createContext2();
    
    layers = new ArrayList<ZCalqueLigneBriseeEditable>();
    layers.add(context.getLineLayers().iterator().next());

    lines = new MultiGroupLineFindResult();
    lines.addObject(layers.get(0), new LineFind(0, 3, true));
    lines.addObject(layers.get(0), new LineFind(0, 0, true));

    joiner = new PolylineJoiner(lines, null);
    joiner.process();

    assertTrue("La jointure devrait r�ussir", joiner.getResult() == PolylineJoinerResult.SUCCEEDED);
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(new GISLigneBrisee[]{this.createLine2(), this.createPolygone1(), this.createPolygone2()}, this.getAllLines(layers)));
  }
  
  private ContextForTesting createContext1()
  {
    ContextForTesting context = new ContextForTesting();
    context.createLinesLayer().modeleDonnees().addGeometry(this.createLine1(), null, null, null);
    context.createLinesLayer().modeleDonnees().addGeometry(this.createLine2(), null, null, null);
    
    return context;
  }

  private ContextForTesting createContext2()
  {
    ContextForTesting context = new ContextForTesting();
    ZModeleLigneBriseeEditable model = context.createLinesLayer().modeleDonnees();
    model.addGeometry(this.createLine1(), null, null, null);
    model.addGeometry(this.createLine2(), null, null, null);
    model.addGeometry(this.createPolygone1(), null, null, null);
    
    return context;
  }

  private Coordinate[] createCoordinatesLine1()
  {
    return new Coordinate[]{new Coordinate(3, 3), new Coordinate(8, 7), new Coordinate(13, 3), new Coordinate(18, 7)};
  }

  private Coordinate[] createCoordinatesLine2()
  {
    return new Coordinate[]{new Coordinate(24, 12), new Coordinate(24, 6), new Coordinate(29, 9), new Coordinate(30, 4)};
  }
  
  private GISPolyligne createLine1()
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.createCoordinatesLine1());
  }
  
  private GISPolyligne createLine2()
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.createCoordinatesLine2());
  }
  
  private GISPolyligne createLine3()
  {
    Coordinate[] coords1 = this.createCoordinatesLine1();
    Coordinate[] coords2 = this.createCoordinatesLine2();
    Coordinate[] coords = new Coordinate[coords1.length + coords2.length];
    
    for (int i = 0; i < coords1.length; i++)
    {
      coords[i] = coords1[i];
    }
    
    for (int i = 0; i < coords2.length; i++)
    {
      coords[i + coords1.length] = coords2[i];
    }
    
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords);
  }
  
  private GISPolygone createPolygone1()
  {
    return (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(new Coordinate[]{new Coordinate(15, 9), new Coordinate(20, 14), new Coordinate(18, 19), new Coordinate(12, 17), new Coordinate(10, 12), new Coordinate(15, 9)});
  }

  private GISPolygone createPolygone2()
  {
    Coordinate[] coords1 = this.createCoordinatesLine1();
    Coordinate[] coords = new Coordinate[coords1.length + 1];
    
    for (int i = 0; i < coords1.length; i++)
    {
      coords[i] = coords1[i];
    }

    coords[coords1.length] = new Coordinate(coords1[0]);
    
    return (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(coords);
  }

  private boolean isSameLines(GISLigneBrisee[] lines1, GISLigneBrisee[] lines2)
  {
    if (lines1.length != lines2.length)
    {
      return false;
    }
    
    TIntHashSet findedLines = new TIntHashSet();
    
    for (int i = 0; i < lines1.length; i++)
    {
      for (int j = 0; j < lines2.length; j++)
      {
        if (!findedLines.contains(j))
        {
          if (comparator.compare(lines1[i], lines2[j]) == 0)
          {
            findedLines.add(j);
            
            break;
          }
        }
      }
    }
    
    return findedLines.size() == lines1.length;
  }
  
  private GISLigneBrisee[] getAllLines(List<ZCalqueLigneBriseeEditable> layers)
  {
    List<GISLigneBrisee> listLines = new ArrayList<GISLigneBrisee>();
    
    for (int i = 0; i < layers.size(); i++)
    {
      ZModeleLigneBriseeEditable model = layers.get(i).modeleDonnees();
      int nbLines = model.getNombre();
      
      for (int j = 0; j < nbLines; j++)
      {
        listLines.add((GISLigneBrisee)model.getGeomData().getGeometry(j));
      }
    }
    
    GISLigneBrisee[] lines = new GISLigneBrisee[0];

    lines = listLines.toArray(lines);
    
    return lines;
  }
}
