package org.fudaa.fudaa.fgrid.action;

import gnu.trove.TIntHashSet;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLigneBrisee;
import org.fudaa.ctulu.gis.GISLigneBriseeComparator;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;

import org.locationtech.jts.geom.Coordinate;

public class IntersectionCreaterTester extends TestCase
{
  private static final GISLigneBriseeComparator comparator = new GISLigneBriseeComparator();

  public void testIntersectionCreater()
  {
    ContextForTesting context = new ContextForTesting();
    
    ZCalqueLigneBriseeEditable layer1 = context.createLinesLayer();
    ZCalqueLigneBriseeEditable layer2 = context.createLinesLayer();
    
    List<ZCalqueLigneBriseeEditable> layers = new ArrayList<ZCalqueLigneBriseeEditable>();
    layers.add(layer1);
    layers.add(layer2);
    
    ZModeleLigneBriseeEditable model1 = layer1.modeleDonnees();
    ZModeleLigneBriseeEditable model2 = layer2.modeleDonnees();
    
    model1.addGeometry(this.createLine1(), null, null, null);
    model1.addGeometry(this.createLine3(), null, null, null);
    model1.addGeometry(this.createLine5(), null, null, null);
    model1.addGeometry(this.createPolygone1(), null, null, null);

    model2.addGeometry(this.createLine2(), null, null, null);
    model2.addGeometry(this.createLine4(), null, null, null);
    model2.addGeometry(this.createLine6(), null, null, null);
    model2.addGeometry(this.createPolygone2(), null, null, null);
    
    IntersectionCreater creater = new IntersectionCreater(layers, context.getSupportModelPoint(), null);
    creater.process();
    
    assertEquals("Le nombre de point ajout� est incorrect", 9, creater.getNbPointsAdded());
    assertEquals("Le nombre de point ins�r� est incorrect", 19, creater.getNbPointsInserted());

    GISLigneBrisee[] realLines = new GISLigneBrisee[]{this.createLine1WithIntersections(),
                                                      this.createLine2WithIntersections(),
                                                      this.createLine3WithIntersections(),
                                                      this.createLine4WithIntersections(),
                                                      this.createLine5WithIntersections(),
                                                      this.createLine6WithIntersections(),
                                                      this.createPolygone1WithIntersections(),
                                                      this.createPolygone2WithIntersections()};
    
    assertTrue("Les lignes devraient �tre identiques", this.isSameLines(realLines, this.getAllLines(layers)));
  }
  
  private Coordinate[] insertCoordinates(Coordinate[] originalCoord, Coordinate[] coordsToInsert, int[] idxToInsert)
  {
    Coordinate[] coords = new Coordinate[originalCoord.length + coordsToInsert.length];
    int originalIdx = 0;
    int idx = 0;
    
    for (int i = 0; i < coords.length; i++)
    {
      if ((idx < idxToInsert.length) && (idxToInsert[idx] == i))
      {
        coords[i] = coordsToInsert[idx++];
      }
      else
      {
        coords[i] = originalCoord[originalIdx++];
      }
    }
    
    return coords;
  }
  
  private Coordinate[] createCoordinatesLine1()
  {
    return new Coordinate[]{new Coordinate(2, 24),
                            new Coordinate(5, 31),
                            new Coordinate(16, 31),
                            new Coordinate(13, 26),
                            new Coordinate(2, 37),
                            new Coordinate(13, 37)};
  }
  
  private Coordinate[] createCoordinatesLine2()
  {
    return new Coordinate[]{new Coordinate(9, 41),
                            new Coordinate(9, 34),
                            new Coordinate(2, 34),
                            new Coordinate(0, 29)};
  }
  
  private Coordinate[] createCoordinatesLine3()
  {
    return new Coordinate[]{new Coordinate(40, 29),
                            new Coordinate(33, 35),
                            new Coordinate(22, 24),
                            new Coordinate(12, 22)};
  }
  
  private Coordinate[] createCoordinatesLine4()
  {
    return new Coordinate[]{new Coordinate(16, 27),
                            new Coordinate(20, 25),
                            new Coordinate(31, 25),
                            new Coordinate(34, 28),
                            new Coordinate(26, 36)};
  }
  
  private Coordinate[] createCoordinatesLine5()
  {
    return new Coordinate[]{new Coordinate(27, 22),
                            new Coordinate(22, 17),
                            new Coordinate(16, 16),
                            new Coordinate(22, 14),
                            new Coordinate(33, 14)};
  }
  
  private Coordinate[] createCoordinatesLine6()
  {
    return new Coordinate[]{new Coordinate(21, 6),
                            new Coordinate(24, 11),
                            new Coordinate(31, 18),
                            new Coordinate(39, 21)};
  }
  
  private Coordinate[] createCoordinatesPolygone1()
  {
    return new Coordinate[]{new Coordinate(16, 36),
                            new Coordinate(23, 33),
                            new Coordinate(29, 27),
                            new Coordinate(37, 27),
                            new Coordinate(43, 28),
                            new Coordinate(39, 38),
                            new Coordinate(32, 43),
                            new Coordinate(16, 36)};
  }
  
  private Coordinate[] createCoordinatesPolygone2()
  {
    return new Coordinate[]{new Coordinate(9, 9),
                            new Coordinate(12, 17),
                            new Coordinate(21, 20),
                            new Coordinate(32, 9),
                            new Coordinate(22, 2),
                            new Coordinate(9, 9)};
  }
  
  private GISPolyligne createLine1()
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.createCoordinatesLine1());
  }

  private GISPolyligne createLine1WithIntersections()
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.insertCoordinates(this.createCoordinatesLine1(), new Coordinate[]{new Coordinate(8, 31), new Coordinate(8, 31), new Coordinate(5, 34), new Coordinate(9, 37)}, new int[]{2, 5, 6, 8}));
  }
  
  private GISPolyligne createLine2()
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.createCoordinatesLine2());
  }

  private GISPolyligne createLine2WithIntersections()
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.insertCoordinates(this.createCoordinatesLine2(), new Coordinate[]{new Coordinate(9, 37), new Coordinate(5, 34)}, new int[]{1, 3}));
  }
  
  private GISPolyligne createLine3()
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.createCoordinatesLine3());
  }

  private GISPolyligne createLine3WithIntersections()
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.insertCoordinates(this.createCoordinatesLine3(), new Coordinate[]{new Coordinate(30, 32), new Coordinate(27, 29), new Coordinate(23, 25)}, new int[]{2, 3, 4}));
  }
  
  private GISPolyligne createLine4()
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.createCoordinatesLine4());
  }

  private GISPolyligne createLine4WithIntersections()
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.insertCoordinates(this.createCoordinatesLine4(), new Coordinate[]{new Coordinate(23, 25), new Coordinate(33, 27), new Coordinate(30, 32)}, new int[]{2, 4, 6}));
  }
  
  private GISPolyligne createLine5()
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.createCoordinatesLine5());
  }

  private GISPolyligne createLine5WithIntersections()
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.insertCoordinates(this.createCoordinatesLine5(), new Coordinate[]{new Coordinate(23, 18), new Coordinate(27, 14)}, new int[]{1, 5}));
  }
  
  private GISPolyligne createLine6()
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.createCoordinatesLine6());
  }

  private GISPolyligne createLine6WithIntersections()
  {
    return (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(this.insertCoordinates(this.createCoordinatesLine6(), new Coordinate[]{new Coordinate(27, 14)}, new int[]{2}));
  }
  
  private GISPolygone createPolygone1()
  {
    return (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(this.createCoordinatesPolygone1());
  }

  private GISPolygone createPolygone1WithIntersections()
  {
    return (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(this.insertCoordinates(this.createCoordinatesPolygone1(), new Coordinate[]{new Coordinate(27, 29), new Coordinate(33, 27)}, new int[]{2, 4}));
  }
  
  private GISPolygone createPolygone2()
  {
    return (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(this.createCoordinatesPolygone2());
  }

  private GISPolygone createPolygone2WithIntersections()
  {
    return (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(this.insertCoordinates(this.createCoordinatesPolygone2(), new Coordinate[]{new Coordinate(23, 18), new Coordinate(27, 14)}, new int[]{3, 4}));
  }

  private boolean isSameLines(GISLigneBrisee[] lines1, GISLigneBrisee[] lines2)
  {
    if (lines1.length != lines2.length)
    {
      return false;
    }
    
    TIntHashSet findedLines = new TIntHashSet();
    
    for (int i = 0; i < lines1.length; i++)
    {
      for (int j = 0; j < lines2.length; j++)
      {
        if (!findedLines.contains(j))
        {
          if (comparator.compare(lines1[i], lines2[j]) == 0)
          {
            findedLines.add(j);
            
            break;
          }
        }
      }
    }
    
    return findedLines.size() == lines1.length;
  }
  
  private GISLigneBrisee[] getAllLines(List<ZCalqueLigneBriseeEditable> layers)
  {
    List<GISLigneBrisee> listLines = new ArrayList<GISLigneBrisee>();
    
    for (int i = 0; i < layers.size(); i++)
    {
      ZModeleLigneBriseeEditable model = layers.get(i).modeleDonnees();
      int nbLines = model.getNombre();
      
      for (int j = 0; j < nbLines; j++)
      {
        listLines.add((GISLigneBrisee)model.getGeomData().getGeometry(j));
      }
    }
    
    GISLigneBrisee[] lines = new GISLigneBrisee[0];

    lines = listLines.toArray(lines);
    
    return lines;
  }
}
