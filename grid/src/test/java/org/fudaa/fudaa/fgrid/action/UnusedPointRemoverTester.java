package org.fudaa.fudaa.fgrid.action;

import gnu.trove.TIntHashSet;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.fgrid.model.GeometricPointModel;
import org.fudaa.fudaa.sig.layer.FSigLayerPointEditable;

import org.locationtech.jts.geom.Coordinate;

//TODO See to manage eps.
public class UnusedPointRemoverTester extends TestCase
{
  private static final double eps = 0.1d;

  public void testUnusedPointRemover()
  {
    ContextForTesting context = new ContextForTesting();

    ZModeleLigneBriseeEditable linesModel = context.createLinesLayer().modeleDonnees();
    
    FSigLayerPointEditable pointsLayer1 = context.getSupportPointLayer();
    FSigLayerPointEditable pointsLayer2 = context.createPointsLayer();
    
    GeometricPointModel pointsModel1 = (GeometricPointModel)pointsLayer1.getModelEditable();
    GeometricPointModel pointsModel2 = (GeometricPointModel)pointsLayer2.getModelEditable();
    
    List<ZCalquePointEditable> layers = new ArrayList<ZCalquePointEditable>();
    layers.add(pointsLayer1);
    layers.add(pointsLayer2);
    
    pointsModel1.addPoint(new GrPoint(new Coordinate(5, 12)), null, null);
    pointsModel2.addPoint(new GrPoint(new Coordinate(12, 17)), null, null);

    linesModel.addGeometry(GISGeometryFactory.INSTANCE.createLineString(this.createCoordinatesLine1()), null, null, null);
    
    pointsModel1.addPoint(new GrPoint(new Coordinate(18, 19)), null, null);
    pointsModel2.addPoint(new GrPoint(new Coordinate(14, 8)), null, null);

    linesModel.addGeometry(GISGeometryFactory.INSTANCE.createLineString(this.createCoordinatesLine2()), null, null, null);

    pointsModel1.addPoint(new GrPoint(new Coordinate(20, 3)), null, null);
    pointsModel2.addPoint(new GrPoint(new Coordinate(27, 5)), null, null);
    
    UnusedPointRemover remover = new UnusedPointRemover(layers, null, context);
    remover.process();
    
    assertEquals("Le nombre de point �limin� est incorrect", 6, remover.getNbPointsRemoved());
    assertTrue("Les point ne sont pas identiques", this.isSamePoints(this.getAllUsedPoints(), this.getAllPoints(layers)));
  }

  private Coordinate[] createCoordinatesLine1()
  {
    return new Coordinate[]{new Coordinate(3, 3), new Coordinate(8, 7), new Coordinate(13, 3), new Coordinate(18, 7)};
  }

  private Coordinate[] createCoordinatesLine2()
  {
    return new Coordinate[]{new Coordinate(24, 12), new Coordinate(24, 6), new Coordinate(29, 9), new Coordinate(30, 4)};
  }

  private Coordinate[] getAllUsedPoints()
  {
    Coordinate[] coords1 = this.createCoordinatesLine1();
    Coordinate[] coords2 = this.createCoordinatesLine2();
    Coordinate[] coords = new Coordinate[coords1.length + coords2.length];
    
    for (int i = 0; i < coords1.length; i++)
    {
      coords[i] = coords1[i];
    }
    
    for (int i = 0; i < coords2.length; i++)
    {
      coords[i + coords1.length] = coords2[i];
    }
    
    return coords;
  }
  
  private boolean isSamePoints(Coordinate[] points1, Coordinate[] points2)
  {
    if (points1.length != points2.length)
    {
      return false;
    }
    
    TIntHashSet findedPoints = new TIntHashSet();
    
    for (int i = 0; i < points1.length; i++)
    {
      for (int j = 0; j < points2.length; j++)
      {
        if (!findedPoints.contains(j))
        {
          if (points1[i].distance(points2[j]) <= eps)
          {
            findedPoints.add(j);
            
            break;
          }
        }
      }
    }
    
    return findedPoints.size() == points1.length;
  }

  private Coordinate[] getAllPoints(List<ZCalquePointEditable> layers)
  {
    List<Coordinate> listPoints = new ArrayList<Coordinate>();
    
    for (int i = 0; i < layers.size(); i++)
    {
      GeometricPointModel model = (GeometricPointModel)layers.get(i).getModelEditable();
      int nbPoints = model.getNombre();
      
      for (int j = 0; j < nbPoints; j++)
      {
        listPoints.add(new Coordinate(model.getX(j), model.getY(j)));
      }
    }
    
    Coordinate[] points = new Coordinate[0];

    points = listPoints.toArray(points);
    
    return points;
  }
}
