package org.fudaa.fudaa.fgrid.layer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;

import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gui.CtuluUIDialog;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BCalquePaletteInfo;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.fudaa.fgrid.action.ClearDuplicateLineAction;
import org.fudaa.fudaa.fgrid.action.ClearDuplicatePointAction;
import org.fudaa.fudaa.fgrid.action.CreateCircleAction;
import org.fudaa.fudaa.fgrid.action.CreateIntersectionAction;
import org.fudaa.fudaa.fgrid.action.CreateSeaWallAction;
import org.fudaa.fudaa.fgrid.action.CutPolylineAction;
import org.fudaa.fudaa.fgrid.action.JoinPolylineAction;
import org.fudaa.fudaa.fgrid.action.ProjectPointOnLineAction;
import org.fudaa.fudaa.fgrid.action.RemoveUnusedPointAction;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;

import com.memoire.bu.BuTabbedPane;
import org.locationtech.jts.geom.Coordinate;

public class ExampleGridVisuPanel {

  static JComponent buildComponent(Collection<EbliActionPaletteAbstract> acts) {
    final BuTabbedPane tb = new BuTabbedPane();
    for (final EbliActionPaletteAbstract pals : acts) {
      final JComponent component = pals.getPaletteContent();
      if (component instanceof BCalquePaletteInfo) {
        ((BCalquePaletteInfo) component).setAvailable(true);
        ((BCalquePaletteInfo) component).updateState();
      }
      pals.setEnabled(true);
      pals.updateBeforeShow();
      tb.addTab(pals.getTitle(), pals.getIcon(), component);
    }
    return tb;
  }

  public static void main(final String[] args) {
    final JPanel main = new JPanel();
    final CtuluUIDialog ui = new CtuluUIDialog(main);
    final ContextMainGeometryModule ctx = new ContextMainGeometryModule(ui);
    final FSigVisuPanel pn = new GridVisulPanel(ctx);
    // le contexte geometrique permet de r�cuperer les diff�rents calques.
    ZCalqueLigneBriseeEditable geometricLine = ctx.getGeometricContext().findFirstLineLayerOfType(
        EnumLayerDataType.GEOMETRIC_LINE);

    final GISPolyligne createLineString = createDefaultLine();
    geometricLine.modeleDonnees().addGeometry(createLineString, null, ui, null);
    // final ZCalqueLigneBrisee layerAct = pn.getGroupGIS().addLigneBriseeLayerAct("Test", testModel);
    main.setLayout(new BorderLayout());
    main.add(pn);
    pn.setPreferredSize(new Dimension(800, 300));
    pn.restaurer();
    final JFrame fr = new JFrame();
    fr.setPreferredSize(new Dimension(1100, 400));
    fr.setContentPane(main);
    fr.pack();
    final BArbreCalque arbre = new BArbreCalque(pn.getArbreCalqueModel());
    main.add(new JScrollPane(arbre), BorderLayout.EAST);
    // les actions join et cut sont dans la toolbar.
    final JToolBar tb = createToolBar(pn, ctx.getGeometricContext());

    main.add(tb, BorderLayout.NORTH);
    pn.getScene().setCalqueActif(geometricLine);
    JComponent palette = buildComponent(pn.getController().getTabPaletteAction());
    
    
    main.add(palette, BorderLayout.WEST);
    fr.setVisible(true);
    pn.restaurer();

    fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  private static JToolBar createToolBar(final FSigVisuPanel pn, ContextGeometricLayersImpl context) {
    final List<EbliActionInterface> actions = pn.getController().getActions();
    final JToolBar tb = new JToolBar();
    for (final Iterator<EbliActionInterface> iterator = actions.iterator(); iterator.hasNext();) {
      final EbliActionInterface ebliActionInterface = iterator.next();
      if (ebliActionInterface != null) {
        tb.add(ebliActionInterface.buildToolButton(EbliComponentFactory.INSTANCE));
      }
    }
    final ClearDuplicateLineAction clearLineAction = new ClearDuplicateLineAction(context, null);
    tb.add(clearLineAction.buildToolButton(EbliComponentFactory.INSTANCE));
    final ClearDuplicatePointAction clearPtsAction = new ClearDuplicatePointAction(context, null);
    tb.add(clearPtsAction.buildToolButton(EbliComponentFactory.INSTANCE));
    final CreateCircleAction createCircleAction = new CreateCircleAction(context, null);
    tb.add(createCircleAction.buildToolButton(EbliComponentFactory.INSTANCE));    
    final CutPolylineAction cutAction = new CutPolylineAction(context, null);
    tb.add(cutAction.buildToolButton(EbliComponentFactory.INSTANCE));
    final CreateIntersectionAction detectInterAction = new CreateIntersectionAction(context, null);
    tb.add(detectInterAction.buildToolButton(EbliComponentFactory.INSTANCE));    
    final JoinPolylineAction joinAction = new JoinPolylineAction(context, null);
    tb.add(joinAction.buildToolButton(EbliComponentFactory.INSTANCE));    
    final ProjectPointOnLineAction projectPtsAction = new ProjectPointOnLineAction(context, null);
    tb.add(projectPtsAction.buildToolButton(EbliComponentFactory.INSTANCE));    
    final RemoveUnusedPointAction removePtsAction = new RemoveUnusedPointAction(context, null);
    tb.add(removePtsAction.buildToolButton(EbliComponentFactory.INSTANCE));
    final CreateSeaWallAction createSeaWallAction = new CreateSeaWallAction(context, null);
    tb.add(createSeaWallAction.buildToolButton(EbliComponentFactory.INSTANCE));

    return tb;
  }

  private static GISPolyligne createDefaultLine() {
 //   final GISZoneCollectionLigneBrisee testModel = new GISZoneCollectionLigneBrisee();
    final GISPolyligne createLineString = (GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(new Coordinate[] {
        new Coordinate(12, 13), new Coordinate(32, 33), new Coordinate(57, 24), new Coordinate(86, 42),
        new Coordinate(112, 13) });
    return createLineString;
  }
}
