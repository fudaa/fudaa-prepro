package org.fudaa.fudaa.fgrid.action;

import gnu.trove.TIntHashSet;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLigneBrisee;
import org.fudaa.ctulu.gis.GISLigneBriseeComparator;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;

import org.locationtech.jts.geom.Coordinate;

// TODO See to manage eps.
public class DuplicateLineCleanerTester extends TestCase
{
  private static final GISLigneBriseeComparator comparator = new GISLigneBriseeComparator();

  public void testDuplicateLineCleaner()
  {
    // Coords 1, 2, 3 equal.
    Coordinate[] coords1 = new Coordinate[]{new Coordinate(3, 3), new Coordinate(8, 7), new Coordinate(13, 3), new Coordinate(18, 7)};
    Coordinate[] coords2 = new Coordinate[]{new Coordinate(3, 3), new Coordinate(8, 7), new Coordinate(13, 3), new Coordinate(18, 7)};
    Coordinate[] coords3 = new Coordinate[]{new Coordinate(18, 7), new Coordinate(13, 3), new Coordinate(8, 7), new Coordinate(3, 3)};
    // Coords 4 unique.
    Coordinate[] coords4 = new Coordinate[]{new Coordinate(8, 7), new Coordinate(13, 3), new Coordinate(18, 7)};
    // Coords 5 unique.
    Coordinate[] coords5 = new Coordinate[]{new Coordinate(3, 3), new Coordinate(8, 7), new Coordinate(13, 3)};
    // Coords 6 unique.
    Coordinate[] coords6 = new Coordinate[]{new Coordinate(3, 3), new Coordinate(8, 7), new Coordinate(13, 3), new Coordinate(18, 7), new Coordinate(25, 12)};
    // Coords 7 unique.
    Coordinate[] coords7 = new Coordinate[]{new Coordinate(24, 12), new Coordinate(24, 6), new Coordinate(29, 9), new Coordinate(30, 4)};
    // Coords 8, 9, 10 equal.
    Coordinate[] coords8 = new Coordinate[]{new Coordinate(15, 9), new Coordinate(20, 14), new Coordinate(18, 19), new Coordinate(12, 17), new Coordinate(10, 12), new Coordinate(15, 9)};
    Coordinate[] coords9 = new Coordinate[]{new Coordinate(18, 19), new Coordinate(12, 17), new Coordinate(10, 12), new Coordinate(15, 9), new Coordinate(20, 14), new Coordinate(18, 19)};
    Coordinate[] coords10 = new Coordinate[]{new Coordinate(10, 12), new Coordinate(12, 17), new Coordinate(18, 19), new Coordinate(20, 14), new Coordinate(15, 9), new Coordinate(10, 12)};
    // Coords 11 unique.
    Coordinate[] coords11 = new Coordinate[]{new Coordinate(18, 19), new Coordinate(12, 17), new Coordinate(15, 14), new Coordinate(15, 9), new Coordinate(20, 14), new Coordinate(18, 19)};
    // Coords 12 unique.
    Coordinate[] coords12 = new Coordinate[]{new Coordinate(20, 14), new Coordinate(18, 19), new Coordinate(12, 17)};

    // Lines 1, 2, 3 equal.
    GISPolyligne line1 = (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords1);
    GISPolyligne line2 = (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords2);
    GISPolyligne line3 = (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords3);
    // Line 4 unique.
    GISPolyligne line4 = (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords4);
    // Line 5 unique.
    GISPolyligne line5 = (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords5);
    // Line 6 unique.
    GISPolyligne line6 = (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords6);
    // Line 7 unique.
    GISPolyligne line7 = (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords7);
    // Line 8 unique.
    GISPolyligne line8 = (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords12);

    // Polygons 1, 2, 3 equal.
    GISPolygone poly1 = (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(coords8);
    GISPolygone poly2 = (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(coords9);
    GISPolygone poly3 = (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(coords10);
    // Polygon 4 unique.
    GISPolygone poly4 = (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(coords11);

    ContextForTesting context = new ContextForTesting();
    
    ZCalqueLigneBriseeEditable layer1 = context.createLinesLayer();
    ZCalqueLigneBriseeEditable layer2 = context.createLinesLayer();
    ZModeleLigneBriseeEditable model1 = layer1.modeleDonnees();
    ZModeleLigneBriseeEditable model2 = layer2.modeleDonnees();

    model1.addGeometry(line1, null, null, null);
    model1.addGeometry(line4, null, null, null);
    model1.addGeometry(line7, null, null, null);
    model1.addGeometry(line8, null, null, null);
    model1.addGeometry(poly2, null, null, null);
    model1.addGeometry(poly3, null, null, null);

    model2.addGeometry(line2, null, null, null);
    model2.addGeometry(line3, null, null, null);
    model2.addGeometry(line5, null, null, null);
    model2.addGeometry(line6, null, null, null);
    model2.addGeometry(poly1, null, null, null);
    model2.addGeometry(poly4, null, null, null);
    
    List<ZCalqueLigneBriseeEditable> layers = new ArrayList<ZCalqueLigneBriseeEditable>();
    layers.add(layer1);
    layers.add(layer2);
    
    DuplicateLineCleaner cleaner = new DuplicateLineCleaner(layers, null);
    cleaner.process();

    assertEquals("Pas le bon nombre de lignes supprim�es", 4, cleaner.getNbLinesCleaned());

    GISLigneBrisee[] uniqueLines = new GISLigneBrisee[]{(GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords1),
                                                        (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords4),
                                                        (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords5),
                                                        (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords6),
                                                        (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords7),
                                                        (GISPolyligne)GISGeometryFactory.INSTANCE.createLineString(coords12),
                                                        (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(coords8),
                                                        (GISPolygone)GISGeometryFactory.INSTANCE.createLinearRing(coords11)};
    GISLigneBrisee[] lines = this.getAllLines(layers);
    assertEquals("Pas le bon nombre de lignes", uniqueLines.length, lines.length);
    assertTrue("Les lignes ne sont pas les m�me", this.isSameLines(uniqueLines, lines));
  }
  
  private boolean isSameLines(GISLigneBrisee[] lines1, GISLigneBrisee[] lines2)
  {
    if (lines1.length != lines2.length)
    {
      return false;
    }
    
    TIntHashSet findedLines = new TIntHashSet();
    
    for (int i = 0; i < lines1.length; i++)
    {
      for (int j = 0; j < lines2.length; j++)
      {
        if (!findedLines.contains(j))
        {
          if (comparator.compare(lines1[i], lines2[j]) == 0)
          {
            findedLines.add(j);
            
            break;
          }
        }
      }
    }
    
    return findedLines.size() == lines1.length;
  }
  
  private GISLigneBrisee[] getAllLines(List<ZCalqueLigneBriseeEditable> layers)
  {
    List<GISLigneBrisee> listLines = new ArrayList<GISLigneBrisee>();
    
    for (int i = 0; i < layers.size(); i++)
    {
      ZModeleLigneBriseeEditable model = layers.get(i).modeleDonnees();
      int nbLines = model.getNombre();
      
      for (int j = 0; j < nbLines; j++)
      {
        listLines.add((GISLigneBrisee)model.getGeomData().getGeometry(j));
      }
    }
    
    GISLigneBrisee[] lines = new GISLigneBrisee[0];

    lines = listLines.toArray(lines);
    
    return lines;
  }
}
